#!/usr/bin/python
    #http://docs.python.org/library/sqlite3.html
    #http://www.sqlite.org/docs.html
    #http://www.sqlite.org/lang_aggfunc.html
    #http://www.sqlite.org/lang_indexedby.html
    #http://www.sqlite.org/optoverview.html
    #http://www.sqlite.org/fileformat2.html

import sqlite3, os
#import progress


#conn = sqlite3.connect('kmers.db')
conn = sqlite3.connect(":memory:")
conn.isolation_level = None

c = conn.cursor()

# Create table
c.execute('''CREATE TABLE IF NOT EXISTS kmers (kmer INT PRIMARY KEY, org TEXT)''')
c.execute('''CREATE UNIQUE INDEX kmeri ON kmers (kmer)''')
c = conn.cursor()



# Insert a row of data
t = ('1','org1')


c.execute('insert into kmers values (?,?)', t)


t   = 0
#max = 2 ** 31
#max = 2 147 483 648
max = 2147483648


while t <= max:
    if t % 100000 == 0:
        print str(t) + "... ",
    

    val = "SEQ "+str(t)
    c.execute('UPDATE kmers SET kmer = ?, org = org || ";" || ? WHERE kmer = ?', (t, val, t))
    if c.rowcount == 0:
        c.execute('INSERT OR IGNORE INTO kmers VALUES (?, ?)',     (t, val))
    else:
        print "should update again to reduce the data, or not"
    t += 1
    #   c.execute('IF EXISTS (SELECT * FROM kmers WHERE kmer = ?) BEGIN UPDATE kmers SET org = org || ";" || ? WHERE kmer = ? END ELSE BEGIN INSERT OR IGNORE INTO kmers VALUES (?, ?) END', (t, val, t, t, val))
    #   c.execute('CASE (SELECT kmer FROM kmers WHERE kmer = ?) UPDATE kmers SET org  || ";" || ? WHERE kmer = ? INSERT OR REPLACE INTO kmers (kmer, org) VALUES (?, org || ";" || ?)',     (t, "SEQ "+str(t)))
    #   c.execute('insert into kmers values (?,?)', (t, "SEQ "+str(t)))
    #   c.execute('UPDATE kmers SET org = org + ? WHERE kmer = ?', ("SEQ "+str(t), t))
    #   c.execute('INSERT OR REPLACE INTO kmers (kmer, org) VALUES (?, group_concat(org,?))', (t, "SEQ "+str(t)))
    #   c.execute('UPDATE kmers SET org = @org + ? WHERE kmer = ? IF @@ROWCOUNT=0 INSERT INTO rows VALUES (?,?)', ("SEQ "+str(t), t, t, "SEQ "+str(t)) )
    #   c.execute('REPLACE INTO kmers (kmer,org) VALUES (?, ?)', (t, "SEQ "+str(t)));
    #   c.execute("""INSERT INTO kmers VALUES SET (kmer,org) = ('?','?') ON DUPLICATE KEY UPDATE kmer = 'kmer', org = (org + ?)""", (t, "SEQ "+str(t)), "SEQ "+str(t)))


# Save (commit) the changes
conn.commit()



c = conn.cursor()
c.execute('select * from kmers order by kmer')
for row in c:
    print row


# Convert file existing_db.db to SQL dump file dump.sql
conn = sqlite3.connect('existing_db.db')
with open('dump.sql', 'w') as f:
    for line in conn.iterdump():
        f.write('%s\n' % line)


# Copy from memory : http://stackoverflow.com/questions/3826552/in-python-how-can-i-load-a-sqlite-db-completely-to-memory-before-connecting-to-i
connb = sqlite3.connect('kmers.db')
with connb.backup("kmers", ":memory:", "kmers") as backup:
    backup.step() # copy whole database in one go


# We can also close the cursor if we are done with it
c.close()

#sqlite3 example.db "SELECT * FROM stocks"


#    db = SQLite3::Database.new( 'sqlite' )
#    db.execute( %{
#      CREATE TABLE foo
#      (key varchar(100) PRIMARY KEY,
#      value varchar(1000),
#      modified timestamp(20))
#    } )

