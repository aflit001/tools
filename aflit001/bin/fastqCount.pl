#!/usr/bin/perl -w
use strict;
use File::Basename;
use Getopt::Long;

my $inFile;
my $outFile;
my $force;

my $printEvery = 100_000;

GetOptions(
           'infile:s'      => \$inFile,
           'i:s'           => \$inFile,
           'outfile:s'     => \$outFile,
           'o:s'           => \$outFile,
           'force:s'       => \$force,
           'f:s'           => \$force,
);


die if !    $inFile;
die if ! -f $inFile;
die if      $inFile !~ /\.fastq$|\.fastq.gz/i;
$force = $force ? 1 : 0;


my $size = -s $inFile;


my $fileB = basename($inFile);

if ( defined $outFile )
{
	if (( -f "$outFile" ) && ( ! $force ))
	{
		print " FILE $outFile EXISTS. TRY WITH FORCE :: $0 -i $inFile -o $outFile --force\n";
		exit 0;
	}
} else {
	if (( -f "./$fileB.nfo" ) && ( ! $force ))
	{
		print " FILE ./$fileB.nfo EXISTS. TRY WITH FORCE :: $0 -i $inFile --force\n";
		exit 0;
	} else {
		$outFile = $fileB . ".nfo";
	}
}


print "IN FILE $inFile OUT FILE $outFile SIZE $size\n";




#==> EAS67_0098_PEFC6189WAAXX_s_3_2_sequence.fastq <==
#@EAS67:3:1:3:415#0/2
#GGTAACTGGGGTAAATAATTTAATTAAANNANATNANNACNANANNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
#+EAS67:3:1:3:415#0/2
#aFD^aT`_LbM^\L]Y]a^I^YBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
#@EAS67:3:1:3:726#0/2
#GGGTCCCCCTAAAACTCCAAGGACTTGGNNCNAGNTNNTANANCNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
#+EAS67:3:1:3:726#0/2
#ab`\GGU__BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
#@EAS67:3:1:3:1562#0/2
#GATTTTTTTTATCATGGGAAAACATAGGNNTNTTNGNNCTNCNGNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN


my $mer            = 31;
my $fileSize       = 0;
my $readSize       = 0;
my $readCount      = 0;
my $readTotal      = 0;
my $readTotalClean = 0;
my $kmer           = 0;

my $FASTQ;

if ( $inFile =~ /\.fastq\.gz$/ )
{
    use IO::Uncompress::Gunzip qw(gunzip $GunzipError);
    $FASTQ = new IO::Uncompress::Gunzip $inFile or (print "IO::Uncompress::Gunzip failed: $GunzipError\n"  && exit 180 );
    $size *= 5;
} else {
    open ($FASTQ, "<$inFile") or ( print "COULD NOT OPEN $inFile: $!" && exit 190 );
}

my $start = time;

my $name;
my $seq;
my $plus;
my $qual;
my $cleanSeq;

while ($name = <$FASTQ>)
{
	$seq  = <$FASTQ>;
    $plus = <$FASTQ>;
    $qual = <$FASTQ>;

	$fileSize += length($name) + length($seq) + length($plus) + length($qual);
    
	if ( ! ($readCount % $printEvery) )
	{
		my $elap   = time - $start + 1;
		my $spee   = int ($readCount         / $elap);
		my $speeB  = int ($fileSize          / $elap);
		my $eta    = int(($size - $fileSize) / $speeB);
		my $curPer = int(($fileSize          / $size)*100);
		printf " %10d [%6d reads/s - %10d bytes/s - %3d %% - eta %4d s] :: $inFile\n",
			$readCount, $spee,   $speeB,       $curPer,     $eta;
		printf "         READSIZE %3d READCOUNT %12d READTOTAL %12d READTOTALCLEAN %12d N %12d KMER %12d KMERLEN %12d\n\n",
			$readSize,   $readCount,    $readTotal,    $readTotalClean,    ($readTotal - $readTotalClean), $kmer, ($kmer * $mer);
	}

	chomp $name;
	chomp $seq;
	chomp $plus;
	chomp $qual;

	$cleanSeq        = $seq;
	$cleanSeq        =~ s/N//gi;
	my $seqLeng      = length($seq);
	my $cleanSeqLeng = length($cleanSeq);

	#print "LINE\n";
	#print "\tNAME '$name'\n";
	#print "\tSEQ  '$seq'\n";
	#print "\tSEQC '$cleanSeq'\n";
	#print "\tPLUS '$plus'\n";
	#print "\tQUAL '$qual'\n";
	#print "\n\n\n";




	$readCount++;
	$readSize        = $seqLeng if $readSize == 0;
	$readTotal      += $seqLeng;
	$readTotalClean += $cleanSeqLeng;

	if ( $cleanSeqLeng >= $mer )
	{
		#print "\nCHECKING KMER $mer '$seq'\n";
		for ( my $s = 0; $s <= ($seqLeng - $mer); $s++ )
		{
			my $sub = substr($seq, $s, $mer);
            my $p   = index($sub, "N");
			#print "  S $s SUB $sub\n";
			if ( $p  == -1 )
			{
				$kmer++;
			#} else {
            #    $s += index($sub, "N")+1;
            }
		}
	}
}


if ( $inFile =~ /\.fastq\.gz$/ )
{
    print "gz\n";
    if ( ! eof($FASTQ) )
    {
        print "ended with error. NOT EOF\n";
        exit 200;
    } else {
        print "eof\n";
        print "closing file. success\n";
        close $FASTQ or ( print "COULD NOT CLOSE FASTQ: $!" && exit 210 );
    }
} else {
    print "closing file. success\n";
    close $FASTQ or ( print "COULD NOT CLOSE FASTQ: $!" && exit 220 );
}


open  FASTR, ">$outFile" or ( print "COULD NOT OPEN $outFile: $!" && exit 230 );
print FASTR  "INFILE:$inFile|OUTFILE:$outFile|SIZE:$size|READSIZE:$readSize|READCOUNT:$readCount|READTOTAL:$readTotal|READTOTALCLEAN:$readTotalClean|N:" . ($readTotal - $readTotalClean) . "|KMER:$kmer|KMERLENGTH:" . ($kmer * $mer);
print        " INFILE $inFile OUTFILE $outFile SIZE $size READSIZE $readSize READCOUNT $readCount READTOTAL $readTotal READTOTALCLEAN $readTotalClean N " . ($readTotal - $readTotalClean) . " KMER $kmer KMERLENGTH " . ($kmer * $mer). " \n";
close FASTR or ( print "COULD NOT CLOSE $outFile: $!" && exit 240 );
print "exiting\n";
#print "\n";

exit 0;

1;



