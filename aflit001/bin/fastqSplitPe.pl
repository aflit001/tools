#!/usr/bin/perl -w
use strict;

my $in = $ARGV[0];

die "NO INPUT FILE DEFINED" if ! defined $in;
die "INPUT FILE $in DOES NOT EXISTS" if ! -f $in;
die "INPUT FILE $in DOES NOT ENDS IN FASTQ" if $in !~ /\.fastq$/i;

my $ou1 = $in;
my $ou2 = $in;
my $ou3 = $in;
my $ou4 = $in;
$ou1 =~ s/\.fastq$/_1\.fastq/;
$ou2 =~ s/\.fastq$/_2\.fastq/;
$ou3 =~ s/\.fastq$/_n\.fastq/;

print "
IN    $in
OUT 1 $ou1
OUT 2 $ou2
OUT n $ou3
";

if ( -f $ou1 || -f $ou2 || $ou3 ) {
    print "output files $ou1, $ou2 and $ou3 exists. skipping\n\n";
    exit 0;
}

my $c = 0;
my @reg;
open IN,  "<$in"  or die $!;
open OU1, ">$ou1" or die $!;
open OU2, ">$ou2" or die $!;
open OU3, ">$ou3" or die $!;


while ( my $line = <IN> )
{
    #print "LINE $line";
    push(@reg, $line);
    if ( scalar @reg == 8 )
    {
        #print "CONTENT: ", @reg;
        place(\@reg);
    }
#    print $line;
}

#place(\@reg);

close IN;
close OU1;
close OU2;
close OU3;



sub getNameStatus()
{
    my $name   = shift;
    my $status = undef;
    my $read   = undef;
    if    ( $name =~ /(.+)\/1$/ || $name =~ /(.+)\.f$/ ) { $status = 1;  $read = $1 }
    elsif ( $name =~ /(.+)\/2$/ || $name =~ /(.+)\.r$/ ) { $status = 2;  $read = $1 } else {
        $status = 3;
    }
    return ($status, $read);
}

sub place
{
    my $arr = shift;

    if ( defined $arr && scalar @$arr == 8 )
    {
        my @st        = @{$arr}[0..3];
        my @nd        = @{$arr}[4..7];
        #print "ST ", @st;
        #print "ND ", @nd;
           @$arr      = ();

        my $st_name   = $st[0];
        my $nd_name   = $nd[0];

        my $st_status = 0;
        my $nd_status = 0;
        my $st_read   = '';
        my $nd_read   = '';

        ($st_status, $st_read) = &getNameStatus($st_name);
        ($nd_status, $nd_read) = &getNameStatus($nd_name);

        if        ( $st_status == 3  || $nd_status == 3 )
        {
            if    ( $st_status == 3 )
            {
                print OU3 @st;
            } else {
                push(@$arr, @st);
            }

            if    ( $nd_status == 3 )
            {
                print OU3 @nd;
            } else {
                push(@$arr, @nd);
            }

        } else {
            die "READS OUT OF SYNCHRONY '$st_read' vs '$nd_read'\n" if ( $st_read ne $nd_read );

            if    ( $st_status == 1 && $nd_status == 2 )
            {
                print OU1 @st;
                print OU2 @nd;
            }
            elsif ( $st_status == 2 && $nd_status == 1 )
            {
                print OU1 @nd;   
                print OU2 @st; 
            } else {
                die "COULD NOT FIGURE ORDER OF SEQUENCES " . @st . " " . @nd;
            }
        }
    } else {
        print "ARRAY NOT DEFINED OR SMALLER THAN 8 (",scalar @$arr,")\n";
    }
}
