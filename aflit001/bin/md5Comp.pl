#!/usr/bin/perl -w
use strict;

#cat home_files.md5 | perl -ane 'BEGIN {my %m; } END { foreach my $M ( sort keys %m ) { my $a = $m{$M}; my $c = scalar @$a; if ($c != 2 ) { print "$M ($c):\n\t", join("\n\t", @$a), "\n\n"; } } } chomp; push(@{$m{$F[0]}}, $F[1]);'
my %m;

foreach my $line ( <STDIN> )
{
    chomp $line; 
    my @F = split(/\s+/, $line);
    push(@{$m{$F[0]}}, $F[1]);
}

foreach my $M ( sort keys %m ) { 
    my $a = $m{$M}; 
    my $c = scalar @$a;
    print "$M ($c):\n\t", join("\n\t", @$a), "\n\n" if ( $c != 2 );
} 
