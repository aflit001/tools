#!/bin/bash
#rm sizes_fastq*.lst 2>/dev/null
#rm sizes_sff*.lst 2>/dev/null

for folder in `find . -maxdepth 1 -mindepth 1 -type d`; do
    folder=`basename $folder`
    echo "ANALYZING FOLDER $folder"
    FAQF="sizes_"$folder"_fastq.lst"
    SFFF="sizes_"$folder"_sff.lst"

    if [[ ! -f "$FAQF" ]]; then
        echo "  COUNTING FASTQ TO $FAQF"
        find $folder -type f -name '*.fastq' -exec  bash -c 'echo "{} "`sed -n "2~4p" {} | tr -d "\n" | wc -c` | tee -a '$FAQF' &' \;
        DOTOTALFASTQ=1
    else
        echo "  FASTQ SIZE $FAQF EXISTS"
    fi

    if [[ ! -f "$SFFF" ]]; then
        echo "  COUNTING SFF TO $SFFF"
        find $folder -type f -name '*.sff'   -exec  bash -c 'sffCount "{}"                                     | tee -a '$SFFF' &' \;
        DOTOTALSFF=1
    else
        echo "  SFF SIZE $SFFF EXISTS"
    fi

    still=`pgrep ^tr$ | wc -l`
    echo "    FASTQ $still"

    while [ "$still" -gt "0" ]; do
        sleep 2
        still=`pgrep ^tr$ | wc -l`
        echo "    FASTQ $still"
    done


    still=`pgrep ^sffCount$ | wc -l`
    echo "    SFF $still"

    while [ "$still" -gt "0" ]; do
        sleep 2
        still=`pgrep ^sffCount$ | wc -l`
        echo "    SFF $still"
    done

    #sort sizes_fastq_$folder.lst > sizes_fastq_$folder.lst
    #sort sizes_sff_$folder.lst   > sizes_sff_$folder.lst

    if [[ ! -z "$DOTOTALFASTQ" ]]; then
        echo "  DOING TOTAL FASTQ"
        cat $FAQF | perl -nae 'BEGIN{ $s = 0 } END{ print "TOTAL $s\n" } $s+=$F[1];' | tee -a $FAQF
    fi

    if [[ ! -z "$DOTOTALSFF" ]]; then
        echo "  DOING TOTAL SFF"
        cat $SFFF | perl -nae 'BEGIN{ $s = 0 } END{ print "TOTAL $s\n" } $s+=$F[7];' | tee -a $SFFF
    fi
done

head -10000 sizes_*_{fastq,sff}.lst > sizes_global.lst
