#!/usr/bin/perl -w
use strict;
use File::Basename;
#Cuts a specified piece of a sequence in a fasta file
# needs fasta name, chromossome, start position, end position and sequence output name
# prints to stdout
my $fasta  = $ARGV[0];
my $chrom  = $ARGV[1];
my $start  = $ARGV[2];
my $finish = $ARGV[3];
my $name   = $ARGV[4];


my $verbose = 0;
die "NO INPUT FASTA"                     if ! defined $fasta;
die "INPUT FASTA $fasta DOES NOT EXISTS" if ! -f $fasta;
die "INPUT FASTA $fasta IS EMPTY"        if ! -s $fasta;
die "NO CHROMOSOME DEFINED"              if ! defined $chrom;
die "NO START DEFINED"                   if ! defined $start;
die "NO FINISH DEFINED"                  if ! defined $finish;
die "NO NAME DEFINED"                    if ! defined $name;

die "START $start NOT NUMERIC"           if $start  !~ /^\d+$/;
die "END $finish NOT NUMERIC"            if $finish !~ /^\d+$/;

print STDERR "
    INFASTA '$fasta'
    CHR     '$chrom'
    START   '$start'
    FINISH  '$finish'
    NAME    '$name'
";

my $gene = &readFasta($fasta, $chrom, $start, $finish, $name);
#&returner($gene, $start, $end, $name);

# R265_c_neoformans.fasta
# gpd1 824481 823935 supercontig_1.04_of_Cryptococcus_neoformans_Serotype_B
# tef1 288429 287730 supercontig_1.13_of_Cryptococcus_neoformans_Serotype_B
# 
# WM276GBFF10_06.fasta
# gpd1 29 576   CP000291_CGB_F2310W_Glyceraldehyde_3_phosphate_dehydrogenase_476378_478056
# tef1 673 1371 CP000298_CGB_M1520C_Translation_elongation_factor_EF1_alpha__putative_323075_324730

# ./cut.pl R265_c_neoformans.fasta supercontig_1.04_of_Cryptococcus_neoformans_Serotype_B 823935 824481 gpd1 > gpd1.265.txt
# ./cut.pl R265_c_neoformans.fasta supercontig_1.13_of_Cryptococcus_neoformans_Serotype_B 287730 288429 tef1 > tef1.265.txt
# ./cut.pl WM276GBFF10_06.fasta CP000291_CGB_F2310W_Glyceraldehyde_3_phosphate_dehydrogenase_476378_478056 29 576 gpd1 > gpd1.276.txt
# ./cut.pl WM276GBFF10_06.fasta CP000298_CGB_M1520C_Translation_elongation_factor_EF1_alpha__putative_323075_324730 673 1371 tef1 > tef1.276.txt



sub readFasta
{
    my $inputFA  = shift;
    my $chrom    = shift;
    my $begin    = shift;
    my $end      = shift;
    my $name     = shift;

    my @gene;
    my $chromName;
    my $desiredLine;

    undef @gene; $gene[0] = "";
    open FILE, "<$inputFA" or die "COULD NOT OPEN $inputFA: $!";

    my $current;
    my $chromo;
    my $chromoName;
    my $fastaName = basename($inputFA);
    print ">$fastaName\_$chrom\_$begin\_$end\_$name\n";

    my $on   = 0;
    my $pos  = 0;
    my $line = 1;
    my $seq;
    my $totalSeqLen = 0;
    my $found       = 0;

    while (my $line = <FILE>)
    {
            chomp $line;
            if (( $line =~ /^\>/) && ($on))
            {
                $on   = 0;
            }
    
            if ($on)
            {
                my $seqLen = length($line);
                my $seqBeg = $pos + 1;
                my $seqEnd = $pos + $seqLen;
                $totalSeqLen += $seqLen;
                $pos         += $seqLen;

                my $cut1   = -1;
                my $cut2   = -1;
                if ( ! $seq && $seqBeg <= $begin && $seqEnd >= $begin ) {
                    $cut1 = $begin - $seqBeg;
                    print STDERR sprintf ("subtracting CUT1 %3d - %3d = %d\n", $seqBeg, $begin, $cut1) if $verbose;
                    print STDERR "START POSITION $begin FOUND\n";
                } else {
                    #print STDERR "NOT YET ($seqBeg < $begin)\n";
                }

                if ( $end >= $seqBeg && $seqEnd >= $end ) {
                    $cut2 = $end - $seqBeg + 1;
                    #printf STDERR "subtracting CUT2 %3d - %3d = %d\n", $end, $seqBeg, $cut2;
                    printf STDERR "subtracting CUT2 %3d - %3d = %d\n", $end, $seqBeg, $cut2 if $verbose;
                    print STDERR "END POSITION $end FOUND\n";
                }

                printf STDERR "LINE %80s LENGTH %2d POS %5d END POS %5d CUT1 %3d CUT2 %3d ", $_, $seqLen, $seqBeg, $seqEnd, $cut1, $cut2 if $verbose;
                if    ( $cut1 != -1 && $cut2 == -1 ) { if ($verbose) { printf STDERR "T1 %3d - %3d ", $cut1, $seqEnd           } ; $line = substr( $line, $cut1                  ) } # get from end
                elsif ( $cut1 == -1 && $cut2 != -1 ) { if ($verbose) { printf STDERR "T2   0 - %3d ", $cut2                    } ; $line = substr( $line, 0    ,  $cut2          ) } # get from begining 
                elsif ( $cut1 != -1 && $cut2 != -1 ) { if ($verbose) { printf STDERR "T3 %3d - %3d ", $cut1, ( $cut2 - $cut1 ) } ; $line = substr( $line, $cut1, ($cut2 - $cut1) ) } # indside
                else                                 { if ($verbose) { print  STDERR "T4 ALL $line"                            } ;                                               } # all


                if    ( $seqBeg < $begin && $seqEnd < $begin ) { if ($verbose) { print STDERR " NEXTING ($seqBeg < $seqEnd < $begin)\n"; } next };

                if ( ! defined $line ) {
                    die "NO LINE\n";
                }
                $seq .= $line;
                print STDERR "$line\n" if $verbose;
                #print STDERR "$line\n";

                last if $seqEnd >= $end;
            }
    
            if ($line =~ /^\>$chrom/)
            {
                print STDERR "CHROMOSOME $chrom FOUND\n";
                $on         = 1;
                $found      = 1;
            }
    }
    print "$begin $seq $end (" , length($seq), "/",($end - $begin + 1),")\n" if ($verbose);
    if ( length($seq) != ($end - $begin + 1) ) {
        print STDERR "COULD NOT EXTRACT FULL SEQUENCE LENGTH\n";
        print STDERR "  REQUESTED SIZE   ", ($end - $begin + 1)  , "\n";
        print STDERR "  OBSERVED SIZE    ", length($seq)         , "\n";
        print STDERR "  TOTAL SEQ LENGTH ", $totalSeqLen         , "\n";
        print STDERR "  FOUND CHROMOSOME ", ($found ? 'yes' : 'no') , "\n";
        exit 2;
    } else {
        print STDERR "SUCCESSFULLY EXTRACT FULL SEQUENCE LENGTH\n";
        print STDERR "  REQUESTED SIZE   ", ($end - $begin + 1)  , "\n";
        print STDERR "  OBSERVED SIZE    ", length($seq)         , "\n";
        print STDERR "  TOTAL SEQ LENGTH ", $totalSeqLen         , "\n";
        print STDERR "  FOUND CHROMOSOME ", ($found ? 'yes' : 'no') , "\n";
    }

    if ( $seq ) {
        $seq =~ s/(.{60})/$1\n/g;
        print $seq, "\n";
    } else {
        die "NOT ABLE TO EXTRACT SEQUENCE\n";
    }
#   print "\n\n";
    undef $chromo;
    close FILE;
#    print "GOT GENE\n";
    #return \@gene;
}
