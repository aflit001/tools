Installation instructions and user guide for EULER V2.0
Updated July 29, 2004

**********************************************************************

1. Introduction

EULER V2.0 implements the FragmentGluer algorithm
described in the paper:

Pavel A. Pevzner, Haixu Tang and Glenn Tesler,
De novo Repeat Classification and Fragment Assembly,
RECOMB 2004, March 27-31, 2004, San Diego.

EULER V2.0, Copyright (c) 2001-2004,
The Regents of the University of California.
All Rights Reserved.

**********************************************************************

2. Compiling and installing EULER V2.0

If you downloaded the source code:
  In the main directory, type "make clean", and type "make".
  It will produce several executable programs for assembly.
  All the executable files will be produced in the same directory
  as the source codes. This is the main directory for EULER.

If you downloaded the executable code only, you should skip the
above step and go directly to the step below.

You should edit the first lines of each perl script
(consens.pl, create_alignment.pl, errcorr-lapper-pair-mem.pl,
eubatch.pl, euindex.pl, realign2contig.pl, seqcount.pl)
to specify the path for perl on your system
if it is not /usr/bin/perl.

Also near the top of eubatch.pl, you may edit the line
with the definition of $pgms::EULERBIN to specify the
directory where you install the euler binaries.

**********************************************************************

3. Other software recommended for use with EULER

EULER V2.0 has the option of using components of Phrap and
ReAligner, which are supplied by other sources.
EULER V2.0 produces some graph files (with extension .gvz)
suitable for the graphviz package.

We recommend use of all of these programs.

To use these other programs, you will have to download and install
them separately.  You should cite appropriate papers for these
programs separately if required.


* Phrap (by Philip Green)
  You may use a module in Phrap to compute overlaps,
  instead of the corresponding module supplied in EULER.
  The Phrap module is recommended if you have Phred
  quality values along with your reads.

  Please go to http://www.phrap.org/ to get Phrap
  for academic use.

  Install cross_match at the location $EULERBIN/phrap/cross_match
  where $EULERBIN is the directory with EULER binaries.

* ReAligner (by Eric Anson and Gene Myers)
  You may use ReAligner to compute consensus sequences
  of the contigs, instead of the corresponding module
  supplied in EULER.  This is highly recommended.

  Please download it from ftp://ftp.cs.arizona.edu/realigner/

  ReAligner has to be modified for use with EULER.
  Copy these files from the EULER source directory
  into the realigner source directory:
      Patch_ReAligner
      Patch_converter

  Apply the patches:
     patch -n -b ReAligner.c < Patch_ReAligner
     patch -n -b converter.c < Patch_converter

  (The originals will be saved as ReAligner.c.orig
  and converter.c.orig.)

  Then run "make" to compile it.

  Install the binaries as
    $EULERBIN/realigner/ReAligner
    $EULERBIN/realigner/converter
  where $EULERBIN is the directory with EULER binaries.

* GraphViz (AT&T)
  EULER V2.0 produces a set of graph files (with extension .gvz).
  A large suite of software is available to draw graphs from these
  files in major graphics formats such as PostScript, tiff, jpeg, etc.
  http://www.research.att.com/sw/tools/graphviz/
  http://www.graphviz.org

  Download and install the appropriate ones according to their directions.
  These do not have to be in $EULERBIN, you should install them
  separately.

  The downloadable version of EULER V2.0 does not directly run
  the GraphViz programs.
  The GraphViz graph viewer used by the EULER web server is "dot".
  We usually use "dot" or "neato" to produce illustrations.
  GraphViz has other programs for processing these files as well.

  Note that GraphViz documentation uses ".dot" as the file extension,
  but we use ".gvz" due to other well-established use of
  the extension ".dot".

**********************************************************************

4. Running an assembly 

You need to set an environment variable
EULERBIN to the directory containing executables for
the EULER program before you can run EULER.
You should add this directory to your PATH variable
or explicitly include when running eubatch.pl, below.

Change to an independent directory having
the proper input files.

Run the script eubatch.pl:

eubatch.pl -i fasta_file [-q quality_file] [options]

I. Input files:
(a) FASTA Files 

(b) Quality Value Files 
(typically same name as the FASTA file plus ".qual")

These are the names of the input reads files and
the corresponding Phred quality files, respectively.

The read file is required, while the quality file is optional. 

We emphasize that the reads must be previously
vector screened, which can be either cut off
or masked with X's in the reads file.

If the user doesn't have a quality file available, he may
not use this option. The read file and
the quality file must be consistent with each other:
the number of reads in each file should be equal,
and their names and order should be the same.

(c) Mate-Pair Naming Rules File
(typically "name.rul")

This is the names of the mate-pair file.
This file should be in the same directory
as running the job and must be the name "name.rul".
A sample of name.rul file can be found at
the main directory of euler2, like

/*                      Names are: Library_Plate_Well.primer
                                primer  library plate   length range    */

Finishing reads:                f       ALL     ALL
Single reads:                   s       ALL     ALL
Double-barreled reads:          x y     ALL     1-200     1500 3500
Double-barreled reads:          x y     ALL     201-400   3000 10000

Please note the definition of the sources of the reads
in the mate-pair files are very important and the wrong
definition may cause errors in the assembly of EULER-DB and EULER-SF. 

--------------------------------------------------

II. Overlap 


Overlap method
   -v [phrap|direct]          (default: direct)

EULER V2.0 has an initial step to determine the overlaps between input
reads.  We provide two methods to do this.

"phrap" uses the overlap detection procedure in Phrap,
which cuts off overlaps based on log-likelihood ratio score (LLR).

"direct" detects overlaps with a straightforward
Smith-Waterman local alignment algorithm.

We recommend that users use the Phrap overlap detection
procedure if they have the corresponding read quality file
as input. Otherwise we recommend the users use the
second overlap detection method. 

------------------------------

Phrap LLR score cutoff 1 
Phrap LLR score cutoff 2
   -c1 cutoff_1               (default: 15)
   -c2 cutoff_2               (default: 5)

These two parameters are required when the user
chooses Phrap as the overlap detection method.
They are the cutoffs of the LLR score to determine
the overlaps from Phrap output. EULER V2.0 only
takes into account the overlaps between two reads if

  (1) their LLR score is above cutoff_1; or

  (2) their LLR score is above cutoff_2 and neither of the reads
      are involved in any repeat region.

Therefore, for a reasonable parameter setup, cutoff_2 should
be always smaller than cutoff_1.
In order to get more stringent assemblies, the user should
increase the value of both cutoffs. 

------------------------------

Minimum overlap length 
Minimum alignment identity 
   -ol overlap_length         (default: 20)
   -id minimum_identity       (default: 0.95)

These two parameters are required when the user chooses the
"direct" overlap detection method. They are the cutoffs
for the length and identity to decide whether or not
the alignment from the overlap detection should be chosen.
EULER V2.0 only takes into account the overlaps between
two reads if their alignment length is longer than
the minimum overlap length and the alignment identity
higher than the minimum alignment identity.
In order to get stringent assemblies, the user 
should increase either or both of these two parameters. 

------------------------------

Maximum whirl size 
Maximum multiplicity for chimeric reads 
   -ws whirl_size             (default: 50)
   -co minimum_coverage       (default: 1)

These two parameters are use for
the A-Bruijn graph algorithm in EULER V2.0.
They are used with both overlap detection
methods.

"whirls" that have a smaller size than
"maximum whirl size" will be removed
in the FragmentGluer algorithm.

Edges that have smaller multiplicity
than the parameter "Maximum multiplicity
for chimeric reads" are determined to be caused
by chimeric reads and will be removed in
FragmentGluer algorithm.

------------------------------

Consensus method
   -realigner

The user can set this option to use
realigner to generate contig consensus.
If this option is not specified, the default
is for EULER V2.0 to use its own consensus generator.

--------------------------------------------------

III. Output files

The output files are the following (xxxx represents the
name of the reads file): 

******* outputs for EULER: details of all stages
(1) EULER-report.html 
The report of EULER for this assembly job, in html format. 

(2) index.html: 
An HTML file with links to all of these files, with brief descriptions. 

******* outputs for EULER: overlap detection & post-processing
(3) xxxx.single 
(4) xxxx.single.qual: 
The reads that do not overlap any other reads.

(5) xxxx.fin: 
The trimmed reads that will be used in assembly (FASTA format). 

(6) xxxx.fin.qual: 
The Phred quality values of the reads used. 

(7) xxxx.fin.edge 
(8) xxxx.fin.graph: 
The A-Bruijn graph representation of the assembly after
FragmentGluer with reads. 

(9) xxxx.fin.intv: 
The read intervals that form each edge in the above graph. 

(10) xxxx.fin.et.gvz: 
The GraphViz format output of the A-Bruijn graph
(see the report file EULER-report.html for details)
after FragmentGluer. For components that are
reverse complements of each other, only one component is generated. 

(11) xxxx.fin.contig: 
The contigs after FragmentGluer with reads. 

(12) xxxx.fin.contig.ace: 
The ace file in the same format as CONSED for each contig. 

(13) xxxx.fin.single: 
The single-read contigs generated by FragmentGluer. 

******* outputs for EULER-ET:
(14) xxxx.fin.et.edge 
(15) xxxx.fin.et.graph
(16) xxxx.fin.et.intv: 
The A-Bruijn graph representation and assembly read intervals
after equivalent transformation with reads. 

(17) xxxx.fin.et.gvz: 
The graphviz format output of the A-Bruijn graph
(see the report file EULER-report.html for details)
after equivalent transformation with reads. For
components that are reverse-complements of each
other, only one component is generated. 

(18) xxxx.fin.et.contig: 
The contigs after equivalent transformation with reads. 

******* outputs for EULER-DB:
(19) xxxx.fin.pair: 
Table of all mate-pairs found in the reads used
in the assembly. Each line in this file represents
the name of a clone, the index of the 1st and 2nd
read of that clone in the read file xxxx.fin and
the type of clone (corresponding to the description
in the mate-pair naming rule file). 

(20) xxxx.fin.db.edge 
(21) xxxx.fin.db.graph
(22) xxxx.fin.db.intv: 
The A-Bruijn graph representation & read intervals
after equivalent transformation with mate-pairs. 

(23) xxxx.fin.db.gvz: 
The graphviz format output of the above graph
(see the report file EULER-report.html for details)
after equivalent transformation with mate-pairs. 

(24) xxxx.fin.db.contig: 
The contigs after equivalent transformation with mate-pairs. 

(25) xxxx.fin.db.contig.sf.gvz: 
The possible connections between contigs after
EULER-DB. Each node in this graph represents
a contig in the input file xxxx.fin.db.contig
and each edge indicates these two contigs might
be positioned closely, suggested by some mate-pairs. 

******* outputs for EULER-Consensus:
(26) xxxx.fin.et.contig.con
(27) xxxx.fin.db.contig.con: 
The contigs after consensus with majority rule,
corresponding to the contigs after EULER-ET and EULER-DB,
respectively. 

--------------------------------------------------

IV. GraphViz package

EULER V2.0 produces a set of graph files (with extension
.gvz), which are in "GraphViz" format.
These files can be used to produce graphs in major
graphics formats such as PostScript, tiff, jpeg, etc.
Information on how to obtain GraphViz is provided earlier in
this document.

**********************************************************************

5. Contact

For questions about using EULER V2.0, please send email to

   Haixu Tang
   htang@cs.ucsd.edu.

For potential collaboration in fragment assembly,
please send email to

   Pavel Pevzner
   ppevzner@cs.ucsd.edu
