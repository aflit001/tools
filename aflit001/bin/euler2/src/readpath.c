/***************************************************************************
* Title:          readpath.c
* Author:         Haixu Tang
* Created:        Jun. 2002
* Last modified:  May. 2004
*
* Copyright (c) 2001-2004 The Regents of the University of California
* All Rights Reserved
* See file LICENSE for details.
***************************************************************************/

#include <stdinc.h>
#include <extvab.h>
#include <extfunc.h>

void remove_readinterval(EDGE *edge, int index);
int filter_path_read(PATH *path, int num, int num_path);
int filter_path_path(PATH *path, int num, int num_path);
void allocpath(PATH *path, int len);
void findpath(PATH *path, EDGE *edge, int *len_seq, int num_seq, char *label);
void backpath(PATH *path, NODES *vertex, int reads, int end, int *len_seq);
void forpath(PATH *path, NODES *vertex, int reads, int begin, int *len_seq);
int readpath(NODES **vertex, int *num, PATH *path, int *len_seq, int num_seq, int *chim, int *num_chim);
int comppath(PATH *path1, PATH *path2);

int readpath(NODES **vertex, int *num, PATH *path, int *len_seq, int num_seq, int *chim, int *num_chim)
{
	int	i, j, k, l, m, n;
	int	num_path;
	int	nch;
	int	num_vertex;
	int	reads;
	char	*label;
	EDGE	*edge, *bal_edge;

	num_vertex = *num;
	label = (char *) ckalloc(2 * num_seq * sizeof(char));

/*	Define read paths	*/

	for(i = 0; i < num_vertex; i ++)	{
		for(j = 0; j < vertex[i] -> num_nextedge; j ++)	{
			edge = vertex[i] -> nextedge[j];
			findpath(path, edge, len_seq, num_seq, label);
		}
	}

	for(i = 0; i < num_seq; i ++)	{
		if(path[i].len_path != path[i + num_seq].len_path)	{
			printf("i %d len_path %d %d\n", i, path[i].len_path, path[i + num_seq].len_path);
			printf("PATH %d ", i);
			for(j = 0; j < path[i].len_path; j ++)	{
				printf("%d(%d) ", path[i].edge[j] -> length, path[i].edge[j] -> multip);
			}
			printf("\n");
			printf("PATH %d ", i + num_seq);
			for(j = 0; j < path[i + num_seq].len_path; j ++)	{
				printf("%d(%d) ", path[i + num_seq].edge[j] -> length, path[i + num_seq].edge[j] -> multip);
			}
			printf("\n");
			printf("len_seq %d %d\n", len_seq[i], len_seq[i + num_seq]);
			for(m = 0; m < path[i].len_path; m ++)	{
				remove_readinterval(path[i].edge[m], i);
			}
			for(m = 0; m < path[i + num_seq].len_path; m ++)	{
				remove_readinterval(path[i + num_seq].edge[m], i + num_seq);
			}
			path[i].len_path = path[i + num_seq].len_path = 0;
/*
			getchar();
*/
		}
	}

	*num = num_vertex;
	free((void *) label);
	return(2 * num_seq);
}

void remove_readinterval(EDGE *edge, int index)
{
	int	i, j, k, l;

	i = 0;
	while(i < edge -> multip)	{
		if(edge -> readinterval[i].eq_read == index)	{
			edge -> readinterval[i] = edge -> readinterval[edge -> multip - 1];
			edge -> multip --;
		} else	{
			i ++;
		}
	}
}

void findpath(PATH *path, EDGE *edge, int *len_seq, int num_seq, char *label)
{
	int i, j, k, l, reads, m;
	int	i1, i2;
	READINTERVAL *readinterval;
	PATH path1, path2;

	for(i = 0; i < edge -> multip; i ++)	{
		readinterval = &(edge -> readinterval[i]);
		reads = readinterval -> eq_read;
		if(label[reads] == 1)	{
			continue;
		}
		label[reads] = 1;
		path1.len_path = path2.len_path = 0;
		allocpath(&path1, len_seq[reads]);
		allocpath(&path2, len_seq[reads]);

		path1.begin_length = readinterval -> offset;
		path2.end_length   = edge -> length - (readinterval -> offset + readinterval -> length);
		//		path2.begin_length = edge -> length - readinterval -> offset + 1;
		//		path1.end_length = readinterval -> offset + readinterval -> length - 1;

		if(readinterval -> begin > 0)	{
			backpath(&path1, edge -> begin, reads, readinterval -> begin, len_seq);
		}
		forpath(&path2, edge -> end, reads, readinterval -> begin + readinterval -> length - VERTEX_SIZE, len_seq);
/*
		printf("reads %d readinterval -> begin %d length %d\n", reads, readinterval -> begin, readinterval -> length,
			path[reads].len_path);
*/

		path[reads].len_path = path1.len_path + path2.len_path + 1;
		allocpath(&path[reads], path[reads].len_path);

		k = 0;
		for(m = 0; m < path1.len_path; m ++)	{
			path[reads].edge[k ++] = path1.edge[path1.len_path - 1 - m];
		}
		path[reads].edge[k ++] = edge;
		for(m = 0; m < path2.len_path; m ++)	{
			path[reads].edge[k ++] = path2.edge[m];
		}
		path[reads].len_path = k;
		path[reads].begin_length = path1.begin_length;
		path[reads].end_length = path2.end_length;

		if(path[reads].len_path == 0)	free((void **) path[reads].edge);
		free((void **) path1.edge);
		free((void **) path2.edge);
	}
}

void backpath(PATH *path, NODES *vertex, int reads, int end, int *len_seq)
{
	int	i, j, k, l;
	READINTERVAL *readinterval;

	for(i = 0; i < vertex -> num_lastedge; i ++)	{
		for(j = 0; j < vertex -> lastedge[i] -> multip; j ++)	{
			readinterval = &(vertex -> lastedge[i] -> readinterval[j]);
			if(readinterval -> eq_read == reads &&
			   readinterval -> begin + readinterval -> length - VERTEX_SIZE - end == 0)	{
				path -> edge[path -> len_path ++] = vertex -> lastedge[i];
				path -> begin_length = readinterval -> offset;
		//		path -> begin_length = vertex -> lastedge[i] -> length - readinterval -> offset + 1;
				if(readinterval -> begin > 0)	{
					backpath(path, vertex -> lastedge[i] -> begin, readinterval -> eq_read,
					 	 readinterval -> begin, len_seq);
				}
				return;
			}
		}
	}
}

void forpath(PATH *path, NODES *vertex, int reads, int begin, int *len_seq)
{
	int	i, j, k, l;
	int	cut;
	READINTERVAL *readinterval;

	for(i = 0; i < vertex -> num_nextedge; i ++)	{
		for(j = 0; j < vertex -> nextedge[i] -> multip; j ++)	{
			readinterval = &(vertex -> nextedge[i] -> readinterval[j]);
			if(readinterval -> eq_read == reads && 
			   readinterval -> begin - begin == 0)	{
				path -> edge[path -> len_path ++] = vertex -> nextedge[i];
				path -> end_length   = vertex -> nextedge[i] -> length - (readinterval -> offset + readinterval -> length);
//				path -> end_length = readinterval -> offset + readinterval -> length + 1;
				forpath(path, vertex -> nextedge[i] -> end, readinterval -> eq_read,
				 	 readinterval -> begin + readinterval -> length - VERTEX_SIZE, len_seq);
				return;
			}
		}
	}
}

void	allocpath(PATH *path, int len)
{
	path -> edge = (EDGE **) ckalloc(len * sizeof(EDGE *));
}

int comppath(PATH *path1, PATH *path2)
{
	int	i, j, k, l, m;

	l = min(path1 -> len_path, path2 -> len_path);
	if(path1 -> len_path == path2 -> len_path)	{
		for(k = 0; k < l; k ++)	{
			if(path1 -> edge[k] != path2 -> edge[k])	{
				break;
			}
		}
		if(k == l)	{
			return(0);
		}
	} else if(path1 -> len_path > path2 -> len_path)	{
		for(i = 0; i <= path1 -> len_path - path2 -> len_path; i ++)	{
			for(k = 0; k < l; k ++)	{
				if(path1 -> edge[i + k] != path2 -> edge[k])	{
					break;
				}
			}
			if(k == l)	{
				return(2);
			}
		}
	} else	{
		for(i = 0; i <= path2 -> len_path - path1 -> len_path; i ++)	{
			for(k = 0; k < l; k ++)	{
				if(path2 -> edge[i + k] != path1 -> edge[k])	{
					break;
				}
			}
			if(k == l)	{
				return(1);
			}
		}
	}
	return(-1);
}

int filter_path_read(PATH *path, int num, int num_path)
{
	int	i, j, k, m, n, l;
	int	*label;

/*	Reorder paths by removing single read edges paths with length 0 (reads in a single edge)	*/

	n = 0;
	for(i = 0; i < num_path; i ++)	{
		if(path[i].len_path > 1)	{
			path[n] = path[i];
			if(i < num)	{
				path[n].readindex = i + 1;
			}
			n ++;
		} else if(path[i].len_path > 0)	{
			free((void **) path[i].edge);
		}
	}
	num_path = n;

/*	Filter low coverage paths	*/

	label = (int *) ckalloc(num_path * sizeof(int));
	for(i = 0; i < num_path; i ++)	{
		label[i] = 1;
	}
	for(i = 0; i < num_path; i ++)	{
		if(path[i].len_path == 0)	continue;
		for(j = i + 1; j < num_path; j ++)	{
			if(path[j].len_path == 0)	continue;
			k = comppath(&path[i], &path[j]);
			if(k == 0)	{
				label[i] ++;
				label[j] ++;
			} else if(k == 1)	{
				label[i] ++;
			} else if(k == 2)	{
				label[j] ++;
			}
		}
	}

	k = 0;
	for(i = 0; i < num_path; i ++)	{
		if(path[i].len_path >= 2 && label[i] <= LOW_COV_PATH)	{
			for(m = 0; m < path[i].len_path; m ++)	{
				remove_readinterval(path[i].edge[m], path[i].readindex - 1);
			}
			free((void **) path[i].edge);
			path[i].len_path = 0;
			k ++;
		}
	}
	free((void *) label);

/*	Again, order paths by removing single read edges paths with length 0 (reads in a single edge)	*/

	n = 0;
	for(i = 0; i < num_path; i ++)	{
		if(path[i].len_path > 1)	{
			path[n] = path[i];
			n ++;
		} else if(path[i].len_path > 0)	{
			free((void **) path[i].edge);
		}
	}
	num_path = n;
	return(num_path);
}

int filter_path_mate(PATH *path, int num, int num_path)
{
	int	i, j, k, m, n, l;
	int	*label;

/*	Reorder paths by removing single read edges paths with length 0 (reads in a single edge)	*/

	n = 0;
	for(i = 0; i < num_path; i ++)	{
		if(path[i].len_path > 1)	{
			path[n] = path[i];
			if(i < num)	{
				path[n].readindex = i + 1;
			}
			n ++;
		} else if(path[i].len_path > 0)	{
			free((void **) path[i].edge);
		}
	}
	num_path = n;

/*	Filter low coverage paths	*/

	label = (int *) ckalloc(num_path * sizeof(int));
	for(i = 0; i < num_path; i ++)	{
		label[i] = 1;
	}
	for(i = 0; i < num_path; i ++)	{
		if(path[i].len_path == 0)	continue;
		for(j = i + 1; j < num_path; j ++)	{
			if(path[j].len_path == 0)	continue;
			k = comppath(&path[i], &path[j]);
			if(k == 0)	{
				label[i] ++;
				label[j] ++;
			} else if(k == 1)	{
				label[i] ++;
			} else if(k == 2)	{
				label[j] ++;
			}
		}
	}

	k = 0;
	for(i = 0; i < num_path; i ++)	{
/*	Only mate-paths should be filtered here	(therefore path[i].readinex == 0 */
		if(path[i].len_path >= 2 && label[i] <= LOW_COV_PATH)	{
			if(path[i].readindex == 0)	{
				for(m = 0; m < path[i].len_path; m ++)	{
					remove_readinterval(path[i].edge[m], path[i].readindex - 1);
				}
				free((void **) path[i].edge);
				path[i].len_path = 0;
				k ++;
			}
		}
	}
	free((void *) label);

/*	Again, order paths by removing single read edges paths with length 0 (reads in a single edge)	*/

	n = 0;
	for(i = 0; i < num_path; i ++)	{
		if(path[i].len_path > 1)	{
			path[n] = path[i];
			n ++;
		} else if(path[i].len_path > 0)	{
			free((void **) path[i].edge);
		}
	}
	num_path = n;
	return(num_path);
}


void free_path(int num_path, PATH *path)
{
  int i;

  for (i = 0; i < num_path; i ++) {
    if(path[i].edge && path[i].edge > 0)	{
    	free((void **) path[i].edge);
    }
  }
  free((void *) path);
}
