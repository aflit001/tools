#include <stdinc.h>
#include <extvab.h>
#include <extfunc.h>

int vertex_run(NODES **vertex, int num_vertex, PATH *path, int num_path, int small_cover, int num_seq);
int run1vertex(NODES *vertex_now, NODES **vertex, int num_vertex, PATH *path, int num_path, EDGE *inedge, int num_seq);
int createvertex(NODES **vertex, int num_vertex, NODES *vertex_now, PATH *path, int num_path);
int chkcross(NODES *vertex_now, PATH *path, int num_path);
int chkforbid(EDGE *edge1, EDGE *edge2, PATH *path);

int vertex_run(NODES **vertex, int num_vertex, PATH *path, int num_path, int small_cover, int num_seq)
{
	int	i, j, k, l, m, n, q;
	int	tot_edge, tot_edge_old;
	int	**num_pa;
	int	n1, n2;
	NODES	*begin, **start_vertex;
	EDGE	*edge, *edge1, *edge2;

	num_pa = (int **) ckalloc(MAX_BRA * sizeof(int *));
	for(i = 0; i < MAX_BRA; i ++)	{
		num_pa[i] = (int *) ckalloc(MAX_BRA * sizeof(int));
	}

	start_vertex = (NODES **) ckalloc(num_vertex * sizeof(NODES *));
	do	{
		tot_edge_old = tot_edge;
		n = 0;
		for(i = 0; i < num_vertex; i ++)	{
			if(vertex[i] -> num_lastedge == 0)	{
				start_vertex[n ++] = vertex[i];
			}
		}
		for(i = 0; i < n; i ++)	{
			for(j = 0; j < start_vertex[i] -> num_nextedge; j ++)	{
				edge = start_vertex[i] -> nextedge[j];
				begin = edge -> end;
				if(begin == start_vertex[i])	continue;
				while(begin -> num_nextedge > 0)	{
					n1 = begin -> num_lastedge;
					n2 = begin -> num_nextedge;
					num_vertex = run1vertex(begin, vertex, num_vertex, path, num_path, edge, num_seq);
					if(n1 == begin -> num_lastedge || n2 == begin -> num_nextedge)	break;
				}
			}
		}
		for(i = 0; i < num_vertex; i ++)	{
			if(vertex[i] -> num_lastedge == 0 || vertex[i] -> num_nextedge == 0)	continue;
			for(j = 0; j < vertex[i] -> num_lastedge; j ++)	{
				num_vertex = run1vertex(vertex[i], vertex, num_vertex, path, num_path,
					 vertex[i] -> lastedge[j], num_seq);
			}
		}
		num_vertex = merge_graph_path(vertex, num_vertex, path, num_path, small_cover);
		tot_edge = count_edge_simp(vertex, num_vertex, num_pa);
	} while(tot_edge_old != tot_edge);

	for(i = 0; i < MAX_BRA; i ++)	{
		free((void *) num_pa[i]);
	}
	free((void **) num_pa);
	free((void **) start_vertex);

	return(num_vertex);
}

int run1vertex(NODES *vertex_now, NODES **vertex, int num_vertex, PATH *path, int num_path, EDGE *inedge, int num_seq)
{
	int	i, j, k, l, m, n, n1, n2, c, q, p, t, f_edge[2];
	int	num_nextedge, num_lastedge;
	int	*match;
	EDGE	*edge, *edge1, *edge2, *lastedge, *nextedge, *edge01, *edge02;
	NODES	*vertex0;

	if(vertex_now -> num_lastedge == 0 || vertex_now -> num_nextedge == 0)	return(num_vertex);
	l = rm_edge(vertex_now, path, num_path);

	n = chkcross(vertex_now, path, num_path);
	if(n > 0)	{
		return(num_vertex);
	}

	match = (int *) ckalloc(MAX_BRA * sizeof(int));

	num_lastedge = vertex_now -> num_lastedge;
	num_nextedge = vertex_now -> num_nextedge;
	if(num_lastedge == 1 && num_nextedge == 1)	{
		lastedge = vertex_now -> lastedge[0];
		vertex0 = lastedge -> bal_edge -> begin;
		nextedge = vertex_now -> nextedge[0];
		edge01 = lastedge -> bal_edge;
		edge02 = nextedge -> bal_edge;
		edge1 = merge_vertex_path(vertex_now, path, num_path);
		if(!edge1)	{
			return(num_vertex);
		}
		if(vertex0 == vertex_now)	{
			edge1 -> bal_edge = edge1;
		} else if(vertex0 -> lastedge[0] != vertex0 -> nextedge[0])	{
			edge2 = merge_vertex_path(vertex0, path, num_path);
			if(edge01 == lastedge || edge02 == nextedge)	{
				edge2 -> bal_edge = edge2;
			} else	{
				edge1 -> bal_edge = edge2;
				edge2 -> bal_edge = edge1;
			}
		} else	{
			edge1 -> bal_edge = edge1;
		}
	} else	{
		edge = inedge;
		n = searchnext(vertex_now, edge, path, num_path, match);
		for(k = 0; k < n; k ++)	{
			nextedge = vertex_now -> nextedge[match[k]];
			m = chkforbid(edge, nextedge, path);
			if(m)	continue;
			edge01 = nextedge -> bal_edge;
			edge02 = edge -> bal_edge;
			edge1 = detach_bal(edge, nextedge, path, num_path, f_edge, num_seq);
			if(edge1)	{
				if(edge02 == nextedge && edge01 == edge)	{
					edge1 -> bal_edge = edge1;
				} else	{
					if(edge01 != nextedge && edge02 != edge)	{
						vertex0 = edge01 -> end;
						if(vertex0 == vertex_now && vertex0 -> num_lastedge == 1 &&
						   vertex0 -> num_nextedge == 1)	{
							edge2 = merge_vertex_path(vertex0, path, num_path);
						} else	{
							edge2 = detach_bal(edge01, edge02, path, num_path, f_edge, num_seq);
						}
						edge1 -> bal_edge = edge2;
						edge2 -> bal_edge = edge1;
					} else if(edge01 == nextedge)	{
						if(f_edge[1])	{
							vertex0 = edge1 -> end;
							if(vertex0 -> num_lastedge == 1 &&
							   vertex0 -> num_nextedge == 1)	{
								edge2 = merge_vertex_path(vertex0, path, num_path);
							} else	{
								edge2 = detach_bal(edge1, edge02, path, num_path, f_edge, num_seq);
							}
							edge2 -> bal_edge = edge2;
						} else	{
							n1 = countmatch(edge1, edge02, path, num_path);
							n2 = countmatch(nextedge, edge02, path, num_path);
							if(n1 > 0)	{
							   edge2 = detach_bal(edge1, edge02, path, num_path, f_edge, num_seq);
							   edge2 -> bal_edge = edge2;
							}
							if(n2 > 0)	{
								edge2 = detach_bal(nextedge, edge02, path, num_path, f_edge, num_seq);
								edge1 -> bal_edge = edge2;
								edge2 -> bal_edge = edge1;
							}
						}
					} else if(edge02 == edge)	{
						if(f_edge[0])	{
							vertex0 = edge01 -> end;
							if(vertex0 -> num_lastedge == 1 &&
							   vertex0 -> num_nextedge == 1)	{
								edge2 = merge_vertex_path(vertex0, path, num_path);
							} else	{
								edge2 = detach_bal(edge01, edge1, path, num_path, f_edge, num_seq);
							}
							edge2 -> bal_edge = edge2;
						} else	{
							n1 = countmatch(edge01, edge1, path, num_path);
							n2 = countmatch(edge01, edge, path, num_path);
							if(n1 > 0)	{
							   edge2 = detach_bal(edge01, edge1, path, num_path, f_edge, num_seq);
							   edge2 -> bal_edge = edge2;
							}
							if(n2 > 0)	{
								edge2 = detach_bal(edge01, edge, path, num_path, f_edge, num_seq);
								edge1 -> bal_edge = edge2;
								edge2 -> bal_edge = edge1;
							}
						}
					}
				}
				break;
			}
		}
	}

	free((void *) match);
	return(num_vertex);
}

int createvertex(NODES **vertex, int num_vertex, NODES *vertex_now, PATH *path, int num_path)
{
	int	i, j, k, l, m, n, q;
	EDGE	*edge, *edge0;

	for(i = 0; i < vertex_now -> num_nextedge; i ++)	{
		k = crossedge(vertex_now -> nextedge[i], path);
		if(k == 0)	{
			edge = vertex_now -> nextedge[i];
			edge0 = edge -> bal_edge;
			num_vertex = newvertex(vertex, num_vertex, edge, path);
			if(edge != edge0)	{
				num_vertex = newvertex(vertex, num_vertex, edge0, path);
			}
		}
	}
	for(i = 0; i < vertex_now -> num_lastedge; i ++)	{
		k = crossedge(vertex_now -> lastedge[i], path);
		if(k == 0)	{
			edge = vertex_now -> lastedge[i];
			edge0 = edge -> bal_edge;
			num_vertex = newvertex(vertex, num_vertex, edge, path);
			if(edge != edge0)	{
				num_vertex = newvertex(vertex, num_vertex, edge0, path);
			}
		}
	}

	return(num_vertex);
}

int chkcross(NODES *vertex_now, PATH *path, int num_path)
{
	int	i, j, k, l, n, n1, n2;
	int	**match, *match1, *match2;

	if((vertex_now -> num_lastedge > 1  || vertex_now -> lastedge[0] -> length < 500)
	 && (vertex_now -> num_nextedge > 1 || vertex_now -> nextedge[0] -> length < 500))	return(0);

	match1 = (int *) ckalloc(vertex_now -> num_lastedge * sizeof(int));
	match2 = (int *) ckalloc(vertex_now -> num_nextedge * sizeof(int));
	match = (int **) ckalloc(vertex_now -> num_lastedge * sizeof(int *));
	for(j = 0; j < vertex_now -> num_lastedge; j ++)	{
		match[j] = (int *) ckalloc(vertex_now -> num_nextedge * sizeof(int));
	}

	for(i = 0; i < vertex_now -> num_path; i ++)	{
		j = vertex_now -> path_index[i];
		k = vertex_now -> path_pos[i];
		if(k > 0 && k < path[j].len_path)	{
			n1 = searcherase(vertex_now -> lastedge, path[j].edge[k - 1], vertex_now -> num_lastedge);
			n2 = searcherase(vertex_now -> nextedge, path[j].edge[k], vertex_now -> num_nextedge);
			match[n1][n2] ++;
		}
	}

	for(j = 0; j < vertex_now -> num_lastedge; j ++)	{
		for(i = 0; i < vertex_now -> num_nextedge; i ++)	{
			match1[j] += match[j][i];
			match2[i] += match[j][i];
		}
	}

	n = 0;
	for(i = 0; i < vertex_now -> num_lastedge; i ++)	{
		if(match1[i] == 0)	{
			n ++;
		}
	}
	for(i = 0; i < vertex_now -> num_nextedge; i ++)	{
		if(match2[i] == 0)	{
			n ++;
		}
	}

	for(j = 0; j < vertex_now -> num_lastedge; j ++)	{
		free((void *) match[j]);
	}
	free((void **) match);
	free((void *) match1);
	free((void *) match2);

	return(n);
}

int chkforbid(EDGE *edge1, EDGE *edge2, PATH *path)
{
	int	i, j, k, l, n1, n2;
	NODES	*vertex;
	EDGE	*edge;

	if(edge1 -> end != edge2 -> begin)	{
		printf("Edges are not linked!. %d(%d-%d) %d(%d-%d)\n", edge1, edge1 -> begin,
			edge1 -> end, edge2, edge2 -> begin, edge2 -> end);
		exit(0);
	}
	if(edge1 -> begin == edge1 -> end || edge2 -> begin == edge2 -> end)	return(0);
	if(edge1 -> length > SMALL_EDGE)	{
		vertex = edge1 -> end;
		edge = (EDGE *) NULL;
		n1 = 0;
		for(i = 0; i < vertex -> num_path; i ++)	{
			if(vertex -> path_pos[i] == 0 ||
			   path[vertex -> path_index[i]].edge[vertex -> path_pos[i] - 1] != edge1)	{
				continue;
			}
			if(vertex -> path_pos[i] < path[vertex -> path_index[i]].len_path
			   && vertex -> path_pos[i] > 1)	{
				n1 = 0;
				break;
			} else if(vertex -> path_pos[i] == 1 && 
			   vertex -> path_pos[i] < path[vertex -> path_index[i]].len_path)	{
				if(!edge)	{
					edge = path[vertex -> path_index[i]].edge[vertex -> path_pos[i]];
				} else if(edge != path[vertex -> path_index[i]].edge[vertex -> path_pos[i]])	{
					n1 = 1;
				}
			}
		}
	} else {
		n1 = 0;
	}
	if(edge2 -> length > SMALL_EDGE)	{
		vertex = edge2 -> begin;
		edge = (EDGE *) NULL;
		n2 = 0;
		for(i = 0; i < vertex -> num_path; i ++)	{
			if(vertex -> path_pos[i] == path[vertex -> path_index[i]].len_path ||
			   path[vertex -> path_index[i]].edge[vertex -> path_pos[i]] != edge2)	{
				continue;
			}
			if(vertex -> path_pos[i] < path[vertex -> path_index[i]].len_path - 1
			   && vertex -> path_pos[i] > 0)	{
				n2 = 0;
				break;
			} else if(vertex -> path_pos[i] > 0 && 
			   vertex -> path_pos[i] == path[vertex -> path_index[i]].len_path - 1)	{
				if(!edge)	{
					edge = path[vertex -> path_index[i]].edge[vertex -> path_pos[i] - 1];
				} else if(edge != path[vertex -> path_index[i]].edge[vertex -> path_pos[i] - 1]) {
					n2 = 1;
				}
			}
		}
	} else {
		n2 = 0;
	}

	return(n1 + n2);
}
