/***************************************************************************
 * Title:          rule.h
 * Author:         Haixu Tang
 * Created:        Jun. 2002
 * Last modified:  May. 2004
 *
 * Copyright (c) 2001-2004 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
char librule[500][100];
char namerule[2][10];
char pairrule[500][2];
int ntyperule[2], ntypepair;
int pairrange[500][2];
int platerule[500][2];
int matetype;
