#!/usr/bin/python
import twitter
import pprint
import os
import sys
import platform
import re
import subprocess

home               = os.environ["HOME"]
cfgFile            = home + "/.twitterDone"

user               = os.environ["USER"]
uname              = platform.uname()[1]
msgHeader          = "[" + user + "@" + uname + "] "

directto           = "sauloal"
consumer_key       = os.environ["TWITTER_CONSUMER_KEY"]
consumer_secret    = os.environ["TWITTER_CONSUMER_SECRET"]
oauth_token        = os.environ["TWITTER_ACCESS_TOLKEN"]
oauth_token_secret = os.environ["TWITTER_ACCESS_TOLKEN_SECRET"]

#pydoc twitter.Api
# Do a test API call using our new credentials
api = twitter.Api(consumer_key        = consumer_key,
                  consumer_secret     = consumer_secret,
                  access_token_key    = oauth_token,
                  access_token_secret = oauth_token_secret)



def twitterSend(text):
    message = msgHeader + text
    status = api.PostDirectMessage(directto, message)
    print "MESSAGE SENT : " + status.text


def markAsDone(id):
    don = []
    don.append(id)
    
    if os.path.exists(cfgFile): 
        file = open(cfgFile)
    
        while 1:
            line = file.readline().rstrip()
            
            if not line:
                break
            else:
                don.append(line)
    
    file = open(cfgFile, "w")
    dlen = len(don)
    
    if dlen > 20:
        dlen = 20
        
    for i in range ( 0, dlen ):
        file.write(don[i] + "\n")


def readDone():
    don = {}
    
    if os.path.exists(cfgFile): 
        file = open(cfgFile)
    
        while 1:
            line = file.readline().rstrip()
            #print "LINE '" + line + "'"
            
            if not line:
                break
            else:
                don[line] = 1;
    else:
        return don
    return don


def checkIfDone(mid):
    for d in done.iterkeys():
        #print "DONE: '" + d + "' vs ID '" + mid + "'"
        if d == mid:
            return True
    
    return False


def twitterCommands(id, text):
    #print "  checking text " + text
    
    p = re.compile('CMD: (.+)', re.IGNORECASE)
    m = p.match(text)
    
    if m:
        #print "    match : " + m.group(1)
        cmd  = m.groups()[0]
        print "    match : '" + cmd + "'"
        cmds = cmd.split(" ")
        #cmds[0] = "arew"
        
        res = ["", ""]
        try:
            proc = subprocess.Popen(cmds, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=False)
            res  = proc.communicate()
        except OSError, e:
            res  = [str(e), "ERR"]
        
        out  = res[0]
        err  = res[1]
        
        if not out:
            out = ""
        if not err:
            err = ""
        
        if err == "":
            markAsDone(id)
        
        text = err + " :: " + out
        
        #print "      Res: " + text
        msg  = text[0:(140 - len(msgHeader))]
        print "      Msg: " + msg
        twitterSend(msg)
    else:
        pass
        #print "    no match"
    #message = "[" + user + "@" + uname + "] " + "".join(sys.argv[1:])
    #status = api.PostDirectMessage(directto, message)
    #print status.text
    
    
def twitterReadMessages():
    #GetDirectMessages(self, since=None, since_id=None, page=None)
    # A sequence of twitter.DirectMessage instances
    # https://dev.twitter.com/docs/api/1/get/direct_messages
    #{"created_at": "Fri Nov 04 18:19:36 +0000 2011", "id": 132522396904202242, "recipient_id": 404947567, "recipient_screen_name": "sauloalassembly", "sender_id": 40236747, "sender_screen_name": "sauloal", "text": "message 01"}
    messages = api.GetDirectMessages()
    for m in messages:
        print "[" + str(m.id) + "]" + m.sender_screen_name + ": " + m.text
        if not checkIfDone(str(m.id)):
            twitterCommands(str(m.id), m.text)
    #save message to list of complied messages

	
def twitterReadTimeline():
    user_timeline = api.GetUserTimeline()
    for s in user_timeline:
        print s.user.name + ": " + s.text
        twitterCommands(s.text)
    #pp            = pprint.PrettyPrinter(indent=4)
    #pp.pprint(user_timeline)




lenArgv = len(sys.argv)

if ( lenArgv == 1 ):
    #twitterReadTimeline()
    done = readDone()
    twitterReadMessages()
else:
    twitterSend(" ".join(sys.argv[1:]))







#user_timeline = api.GetUserTimeline()
#pp            = pprint.PrettyPrinter(indent=4)
#pp.pprint(user_timeline)

#statuses = api.GetPublicTimeline()
#print [s.user.name for s in statuses]

#users = api.GetFriends()
#print [u.name for u in users]

#status = api.PostUpdate("I love python-twitter!")
#print status.text

#status = api.PostDirectMessage(directto, "direct 3")
#print status.text


#GetDirectMessages(self, since=None, since_id=None, page=None)
# A sequence of twitter.DirectMessage instances

#GetMentions(self, since_id=None, max_id=None, page=None)
# A sequence of twitter.Status instances, one for each mention of the user.

#GetReplies(self, since=None, since_id=None, page=None)
# A sequence of twitter.Status instances, one for each reply to the user.

#PostUpdate(self, status, in_reply_to_status_id=None)
#in_reply_to_status_id:
# |          The ID of an existing status that the status to be posted is
# |          in reply to.  This implicitly sets the in_reply_to_user_id
# |          attribute of the resulting status to the user ID of the
# |          message being replied to.  Invalid/missing status IDs will be
# |          ignored. [Optional]
# |      Returns:
# |        A twitter.Status instance representing the message posted.

