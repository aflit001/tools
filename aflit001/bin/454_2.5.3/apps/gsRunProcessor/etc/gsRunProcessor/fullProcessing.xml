
<!-- This file controls the parameters running the gsRunProcessor "pipeline".
     Consult the software manual for information on modifying these files. -->

<!-- This pipeline is for version 2.5.3 of gsRunProcessor only. -->

<parameters>

  <info>
    <id>FullProcessing</id>
    <gsRunProcessorVersion>2.5.3</gsRunProcessorVersion>
    <displayText>Full processing for Shotgun or Paired End</displayText>
    <description lang="en">
			This pipeline takes raw images and performs image
			processing as well as signal processing, filtering and
			base calling for non-amplicons runs.  All processing
			is executed locally.
		</description>
    <displaySortOrder>40</displaySortOrder>
  </info>
  
  <!-- Pipeline setup 
	    The order of the main elements between the <pipeline> ... </pipeline> elements 
	    is critical and shouldn't be changed. However, parameters within the sub-elements 
	    may be modified for different experiments. 	     
	     
		Users should treat these files as templates.  Instead 
	      of modifying these files, it is recommended that these files be 
	      copied to the user's home directory (or other working directory)
	      and modified there.  The user can then use the pipe option of 
		  runAnalysisPipe to select the modified file.
	      -->
  <pipeline>
  
    <cameraClassifier/>
    
    <wellFinder/>
    
    <wellBuilder>
      <if test="(starts-with(InstrumentModel,GSJUNIOR_A)) OR (InstrumentModel = GSFLX_1K)">
        <usePpiInterpolation>false</usePpiInterpolation>
      </if>
    </wellBuilder>
    
    <metricsGenerator>
      <generateRigStatisticsOnly>true</generateRigStatisticsOnly>
    </metricsGenerator>
    
    <datawriter/>
    
    <nukeSignalStrengthBalancer/>
    
    <blowByCorrector/>
    
    <nucValveEventBalancer/>
    
    <externalInterface>
      <enable>false</enable>
      <if test="(starts-with(InstrumentModel,'GSJUNIOR_A')) OR (InstrumentModel = 'GSFLX_1K')">
        <!-- Only use this module for GS-Junior/GSFLX-1K runs. -->
        <enable>true</enable>
      </if>
      <command>extractWellSignalsCWF preTMC 0</command>
    </externalInterface>
    
    <cafieCorrector/>
    
    <externalInterface>
      <enable>false</enable>
      <if test="(starts-with(InstrumentModel,'GSJUNIOR_A')) OR (InstrumentModel = 'GSFLX_1K')">
        <!-- Only use this module for GS-Junior/GSFLX-1K runs. -->
        <enable>true</enable>
      </if>
      <command>TMC267Cafie2C docleanup 0</command>
    </externalInterface>
    
    <metricsGenerator>
      <generateDroopMetricOnly>true</generateDroopMetricOnly>
    </metricsGenerator>
    
    <globalDroopCorrector/>
    
    <individualWellScaler/>
    
    <mostLikelyErrorSubtractor/>
    
    <wellScreener>
      <!-- This is set to match 2.5p1 behavior -->
      <if test="starts-with(InstrumentModel,'GSJUNIOR_A')">
        <enable>false</enable>
      </if>
      <!-- Do not run on non Titanium PTP's -->
      <if test="Run.PTP.WellSize=50">
        <enable>false</enable>
      </if>
    </wellScreener>
    
    <metricsGenerator/>
    
    <qualityFilter>
      <if test="(starts-with(InstrumentModel,'GSJUNIOR_A')) OR (InstrumentModel = 'GSFLX_1K')">
        <vfTrimBackScaleFactor>1.6</vfTrimBackScaleFactor>
      </if>
    </qualityFilter>
    
    <baseCaller>
      <doQScoreTrim>true</doQScoreTrim>
      <errorQscoreWindowTrim>0.010</errorQscoreWindowTrim>
      <QScoreTrimNukeWindowSize>40</QScoreTrimNukeWindowSize>
      <if test="starts-with(InstrumentModel,'GSJUNIOR_A')">
            <carryOverPeaks>true</carryOverPeaks>
      </if>
      <if test="(starts-with(InstrumentModel,'GSJUNIOR_A')) OR (InstrumentModel = 'GSFLX_1K')">
        <QScoreTrimBackScaleFactor>1.2</QScoreTrimBackScaleFactor>
      </if>
    </baseCaller>
    
  </pipeline>
  <!-- Report generation.
			 The gsReporter tool can optionally be run when the processing is 
			 complete and before the post analysis script is run.
		-->
  <gsReporter>
    <enable>true</enable>
    <options>--454BaseCallerMetrics.txt --454BaseCallerMetrics.csv --454RuntimeMetricsAll.txt --454RuntimeMetricsAll.csv --454QualityFilterMetrics.txt --454QualityFilterMetrics.csv --454AllControlMetrics.txt --454AllControlMetrics.csv  --fna --qual</options>
  </gsReporter>
  <!-- Post analysis script. 
		  The program named below will be run after the pipeline completes 
		  and the results are written to disk.
		  
		  The following environmental variables are available to the script:
		  $SOURCE_DIR  - The directory from which the source data was read.
		  $PIPELINE_NAME - The pipeline name (the 'id' field above).
		  $SW_REVISION - The revision number of the pipeline.
		  $GSREPORTER_BIN 	 - The full path of the gsReporter executable.
		  $RUN_NAME	 - The run name (e.g. "R_2008..." )
		  $ON_THE_FLY - Set to 1 if the run was requested as part of a sequencing run.
		  
		  The full path of the directory containing the output files (the -o parameter
		  to gsRunProcessor) is passed on the command line, then the
		  name of each generated file relative to the output path is passed in the
		  following order:  CWF files, SFF files, .fna/.qual files, log/error files, then
		  any reports or legacy files.  
		  
		  NOTE:  If legacy file generation is enabled, the names of the legacy files
		  are passed as well. Therefore, the number of files on the command line 
		  could be large.
		  
		  See the software manual for information about setting up automated
		  processing.  
		  -->
  <postAnalysisScript>postAnalysisScript.sh</postAnalysisScript>
</parameters>
