
<!-- This file controls the parameters running the gsRunProcessor "pipeline".
     Consult the software manual for information on modifying these files. -->

<!-- This pipeline is for version 2.5.3 of gsRunProcessor only. -->

<parameters>
  <info>
    <id>FullProcessingAmplicons</id>
    <gsRunProcessorVersion>2.5.3</gsRunProcessorVersion>
    <displayText>Full processing for Amplicons</displayText>
    <description lang="en">
      This pipeline takes raw images and performs image
      processing as well as signal processing, filtering and
      base calling.  This pipeline defines additional
      screening steps for amplicons experiments.
    </description>
    <displaySortOrder>50</displaySortOrder>
  </info>
  
  <!-- Pipeline setup 
       The order of the main elements between the <pipeline> ... </pipeline> elements 
       is critical and shouldn't be changed. However, parameters within the sub-elements 
       may be modified for different experiments. 	     
       
       Users should treat these files as templates.  Instead 
       of modifying these files, it is recommended that these files be 
       copied to the user's home directory (or other working directory)
       and modified there.  The user can then use the pipe option of 
       runAnalysisPipe to select the modified file.
    -->
  <pipeline>
    
    <cameraClassifier/>
    
    <wellFinder>
      <if test="starts-with(InstrumentModel,'GSJUNIOR_A')">
        <!-- This setting helps offset the brighter signals of Amplicons runs. -->
        <minConsensusSignal>60</minConsensusSignal>
      </if>
    </wellFinder>
    
    <wellBuilder>
      <!-- PPi/ATP image interpolation isn't required on GS Junior instruments. -->
      <if test="(starts-with(InstrumentModel,GSJUNIOR_A)) OR (InstrumentModel = GSFLX_1K)">
        <usePpiInterpolation>false</usePpiInterpolation>
      </if>
    </wellBuilder>
    
    <metricsGenerator>
      <generateRigStatisticsOnly>true</generateRigStatisticsOnly>
    </metricsGenerator>
    
    <datawriter/>
    
    <wellScreener>
      <if test="Run.PTP.WellSize = 35" >
        <!-- Do not modify the following line for 
	     amplicons experiments. -->
        <enable>false</enable>
      </if>
      <!-- useStdDevThresholding must be "false" for FLX amplicons experiments. -->
      <useStdDevThresholding>false</useStdDevThresholding>
    </wellScreener>
    
    <nukeSignalStrengthBalancer/>
    
    <blowByCorrector/>
    
    <externalInterface>
      
      <enable>false</enable>
      <if test="(starts-with(InstrumentModel,'GSJUNIOR_A')) OR (InstrumentModel = 'GSFLX_1K')">
        <!-- Only use this module for GS-Junior/GSFLX-1K runs. -->
        <enable>true</enable>
      </if>
      <command>extractWellSignalsCWF preTMC 0</command>
    </externalInterface>
    
    <cafieCorrector/>
    
    <externalInterface>
      <enable>false</enable>
      <if test="(starts-with(InstrumentModel,'GSJUNIOR_A')) OR (InstrumentModel = 'GSFLX_1K')">
        <!-- Only use this module for GS-Junior/GSFLX-1K runs. -->
        <enable>true</enable>
      </if>
      <command>TMC267Cafie2C docleanup 0</command>
    </externalInterface>
    
    <nucValveEventBalancer>
      <!-- useEctfOnly must be "true" for amplicons experiments. -->
      <useEctfOnly>true</useEctfOnly>
    </nucValveEventBalancer>
    
    <nukeSignalStrengthBalancer/>
    
    <metricsGenerator>
      <generateDroopMetricOnly>true</generateDroopMetricOnly>
    </metricsGenerator>
    
    <individualWellScaler/>
    
    <mostLikelyErrorSubtractor>
      <!-- This should be 201 for Amplicons experiments.  -->
      <maxNucCount>201</maxNucCount>
    </mostLikelyErrorSubtractor>
    
    <wellScreener>
      <if test="Run.PTP.WellSize = 35" >
        <!-- Do not modify the following line for 
	     amplicons experiments. -->
        <enable>false</enable>
      </if>
      <!-- useStdDevThresholding must be "false" for FLX amplicons experiments. -->
      <useStdDevThresholding>false</useStdDevThresholding>
    </wellScreener>
    
    <metricsGenerator/>
    
    <qualityFilter>
      <doValleyFilterTrimBack>false</doValleyFilterTrimBack>
      <useAmpliconsPrimers>true</useAmpliconsPrimers>
      <vfScanAllFlows>tiOnly</vfScanAllFlows>
      <vfTrimBackScaleFactor>2.0</vfTrimBackScaleFactor>
      <vfScanLimit>700</vfScanLimit>
      <if test="starts-with(InstrumentModel,'GSJUNIOR_A')">
        <vfTrimBackScaleFactor>3.0</vfTrimBackScaleFactor>
      </if>
    </qualityFilter>
    
    <clusterBuilder/>
    
    <baseCaller>
      <doQScoreTrim>false</doQScoreTrim>
      <carryOverPeaks>true</carryOverPeaks>
      <if test="Run.PTP.WellSize = 35" >
        <flowRadius>32</flowRadius>
        <useCorrectionGlobalLimit>true</useCorrectionGlobalLimit>
      </if>
    </baseCaller>
    
  </pipeline>
  <!-- Report generation.
       The gsReporter tool can optionally be run when the processing is 
       complete and before the post analysis script is run.
    -->
  <gsReporter>
    <enable>true</enable>
    <options>--454BaseCallerMetrics.txt --454BaseCallerMetrics.csv --454RuntimeMetricsAll.txt --454RuntimeMetricsAll.csv --454QualityFilterMetrics.txt --454QualityFilterMetrics.csv --454AllControlMetrics.txt --454AllControlMetrics.csv --fna --qual</options>
  </gsReporter>
  <!-- Post analysis script. 
       The program named below will be run after the pipeline completes 
       and the results are written to disk.
       
       The following environmental variables are available to the script:
       $SOURCE_DIR  - The directory from which the source data was read.
       $PIPELINE_NAME - The pipeline name (the 'id' field above).
       $SW_REVISION - The revision number of the pipeline.
       $GSREPORTER_BIN 	 - The full path of the gsReporter executable.
       $RUN_NAME	 - The run name (e.g. "R_2008..." )
       $ON_THE_FLY - Set to 1 if the run was requested as part of a sequencing run.
       
       The full path of the directory containing the output files (the -o parameter
       to gsRunProcessor) is passed on the command line, then the
       name of each generated file relative to the output path is passed in the
       following order:  CWF files, SFF files, .fna/.qual files, log/error files, then
       any reports or legacy files.  
       
       NOTE:  If legacy file generation is enabled, the names of the legacy files
       are passed as well. Therefore, the number of files on the command line 
       could be large.
       
       See the software manual for information about setting up automated
       processing.  
    -->
  <postAnalysisScript>postAnalysisScript.sh</postAnalysisScript>
</parameters>
