/*
 * What is 'goldenretriever' and why does it have suid?
 *
 * goldenretriever is a small program that allows the gsSupportTool to access
 * a small number of log files in restricted areas of the GS Junior Attending PC,
 * FLX instrument or Titanium Cluster.
 *
 * This allows remote service staff to be able to diagnose issues with the instrument
 * even if the user does not have read access to files in the /var/log directory.
 *
 * The list of files this program can access is listed below:
 * /var/log/messages*  - To allow access to critical system errors
 * /var/log/secure    - To be able to check for security /intrusion attempts
 * /var/log/maillog   - To be ablt to assist in mail server configuration issues
 * /var/log/gsRunProcessorManager.log - To assist in processing job launching issues.
 * /var/log/boot.log* - To assist in machine startup issues
 * /etc/arno-iptables-firewall/firewall.conf, 
 *   /etc/arno-iptables-firewall/custom-rules
 *   /etc/arno-iptables-firewall/plugins/ssh-brute-force-protection.conf
 *	 To assist in firewall configuration.
 *
 * Users concerned about security with a suid process have three options.
 *
 * 1) Delete the 'goldenretriever' program from /opt/454/apps/gsSupportTool/libexec . This
 *  may prevent the support staff from assisting in all issues, but is the most secure.
 * 2) Remove the suid permissions from the program and setup sudo such that the 'adminrig'
 *  user can run the goldenretriver program without a password.
 *       chmod -s /opt/454/apps/gsSupportTool/libexec/goldenretriever
 *       visudo
 *         and add the following line:
 *   adminrig  ALL = (root) NOPASSWD: /opt/454/apps/gsSupportTool/libexec/goldenretriever
 * 3) This file contains the full source code to the goldenretriever program.  Users may choose
 *  to audit the code below, compile it, and replace the provided goldenretriever with their
 *  own copy.
 *
 *      mv What_is_goldenretriever.txt What_is_goldenretriever.c
 *      gcc -o goldenretriever What_is_goldenretriever.c
 *      (as root)  chown root:root goldenretriever && chmod +s goldenretriever
 *
 * SOURCE CODE TO THE GOLDENRETRIEVER PROGRAM FOLLOWS *
 *
 * Copyright (c) [2009, 2010] 454 Life Sciences Corporation.
 * All Rights Reserved.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.
 *
 * IN NO EVENT SHALL 454 LIFE SCIENCES CORPORATION  BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE.
 *****************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

const char programName[] = "goldenretriever";
const char programVersion[] = "1.0";
const char programLegal[] = "(c) 454 Life Sciences 2010. All rights reserved.";
const char * fileList[] = {  "/var/log/messages.2",
                             "/var/log/messages.1",
                             "/var/log/messages",
                             "/var/log/secure.2",
                             "/var/log/secure.1",
                             "/var/log/secure",
                             "/var/log/maillog",
                             "/var/log/gsRunProcessorManager.log.1",
                             "/var/log/gsRunProcessorManager.log",
                             "/var/log/anaconda",
                             "/var/log/boot.log",
                             "/var/log/boot.log.1",
                             "/etc/arno-iptables-firewall/firewall.conf",
                             "/etc/arno-iptables-firewall/custom-rules",
                             "/etc/arno-iptables-firewall/plugins/ssh-brute-force-protection.conf" };

const int numFiles =  sizeof( fileList )/sizeof( fileList[0] );

void
printHelp()
{
    int i= 0;
    fprintf( stderr, "%s %s\n%s\n\n", programName, programVersion, programLegal );
    fprintf( stderr, "This program is a helper application that allows the gsSupportTool \n"
                     "to securely access the following files:\n" );

    for( i = 0; i < numFiles; ++i ) fprintf( stderr, "   %s\n", fileList[i] );

    fprintf( stderr, "\nRefer to 'What_is_golden_retriever.txt' for details.\n" );

}

int
main( int argc, const char * const* argv )
{
    int i = 0;
    int found = -1;
    int fd = -1;
    char realName[1024] = {0};
    char iobuffer[4096] = {0};
    size_t len_of_realname = 0;
    struct stat stat_buf;
    ssize_t bytes_read = 0;
    static uid_t euid, ruid;

    ruid = getuid();
    euid = geteuid();

    /* Ensure the program hasn't been renamed. Don't trust argv[0], look at the /proc system. */
    len_of_realname = readlink( "/proc/self/exe", realName, sizeof( realName ) );
    realName[1023] = '\0';

    /* Drop the root privileges until we ABSOLUTELY need them. */
    setreuid( euid, ruid );

    if( len_of_realname < sizeof( programName )  || ( len_of_realname > sizeof( programName ) && 
        strcmp( programName, &( realName[len_of_realname-strlen( programName )] ) ) != 0  ) )
    {
        fprintf( stderr, "This program has had its name changed.\n" );
        return ( 1 );
    }

    /* Check for command line parameters.
     * This program only supports one, the name of the file to print or --version. */
    if( argc != 2 )
    {
        printHelp();
        return( 1 );
    }

    /* Check for the --version flag. */
    if( strncmp( "--version", argv[1], strlen( fileList[i] ) ) == 0 )
    {
        fprintf( stderr, "%s %s\n%s\n\n", programName, programVersion, programLegal );
        return ( 0 );
    }

    /* Check the file name is one of the safe ones. */
    for( i = 0; i < numFiles; ++i )
    {
        if( strncmp( fileList[i], argv[1], strlen( fileList[i] ) ) == 0 )
        {
            found = i;
            break;
        }
    }
    if( found == -1 )
    {
        printHelp();
        return( 1 );
    }

    /* Re-enable the root privileges.  Note we open by the 'safe' filename and not
     * the one the user provided. */
    setreuid( ruid, euid );
    fd = open( fileList[found], O_RDONLY );

    /* Drop the root privileges - they are only needed to open the file, not read it. */
    setreuid( euid, ruid );

    if( fd == -1 )
    {
        return( 1 );
    }

    /* Ensure the file we are opening is a real, regular file. */
    if( fstat( fd, &stat_buf ) != 0 || !S_ISREG( stat_buf.st_mode ) )
    {
        close( fd );
        return( 1 );
    }

    /* Read the file a block at a time and dump the file to stdout. */
    while( ( bytes_read = read( fd, iobuffer, sizeof( iobuffer ) ) ) > 0 )
    {
        write( STDOUT_FILENO, iobuffer, bytes_read );
    }

    close( fd );

    return ( 0 );
}

