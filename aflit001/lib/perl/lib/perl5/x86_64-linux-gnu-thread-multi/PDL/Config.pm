
# AUTOMATICALLY GENERATED BY THE PDL TOPLEVEL Makefile.PL.
# DO NOT HAND-EDIT - CHANGES WILL BE LOST UPON YOUR NEXT
#  'perl Makefile.PL'!!!
package PDL;
use File::Spec;
%PDL::Config = (
	BADVAL_PER_PDL	=>	"0",
	WITH_PROJ	=>	"0",
	PDL_CONFIG_VERSION	=>	"0\.005",
	POSIX_THREADS_INC	=>	undef,
	FFTW_TYPE	=>	"double",
	PDL_BUILD_DIR	=>	"\/home\/aflit001\/\.cpanm\/work\/1330016910\.74751\/PDL\-2\.4\.10",
	FFTW_LIBS	=>	undef,
	WITH_FFTW	=>	"0",
	GSL_LIBS	=>	undef,
	WITH_IO_BROWSER	=>	"0",
	PROJ_INC	=>	undef,
	WHERE_PLPLOT_INCLUDE	=>	undef,
	HTML_DOCS	=>	"1",
	SKIP_KNOWN_PROBLEMS	=>	"0",
	WHERE_PLPLOT_LIBS	=>	undef,
	WITH_3D	=>	"0",
	WITH_POSIX_THREADS	=>	"1",
	POGL_VERSION	=>	"0\.65",
	FFTW_INC	=>	undef,
	HIDE_TRYLINK	=>	"1",
	HDF_INC	=>	undef,
	WITH_HDF	=>	"0",
	POGL_WINDOW_TYPE	=>	"glut",
	WITH_GD	=>	"1",
	WITH_BADVAL	=>	"1",
	FITS_LEGACY	=>	"1",
	WITH_SLATEC	=>	"0",
	BADVAL_USENAN	=>	"0",
	WITH_DEVEL_REPL	=>	"1",
	TEMPDIR	=>	"\/tmp",
	PROJ_LIBS	=>	undef,
	USE_POGL	=>	"0",
	PDL_BUILD_VERSION	=>	"2\.4\.10",
	GD_LIBS	=>	undef,
	GSL_INC	=>	undef,
	GD_INC	=>	undef,
	WITH_GSL	=>	"0",
	OPTIMIZE	=>	undef,
	PDLDOC_IGNORE_AUTOLOADER	=>	"0",
	HDF_LIBS	=>	undef,
	POSIX_THREADS_LIBS	=>	undef,
	MALLOCDBG	=>	{},
	WITH_MINUIT	=>	"0",
	WITH_PLPLOT	=>	"0",
	MINUIT_LIB	=>	undef,
);
1;