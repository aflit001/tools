### R code from vignette source 'qrqc.Rnw'

###################################################
### code chunk number 1: qrqc.Rnw:45-47
###################################################
library(qrqc)
s.fastq <- readSeqFile(system.file('extdata', 'test.fastq', package='qrqc'))


###################################################
### code chunk number 2: qrqc.Rnw:49-50
###################################################
s.fastq@filename <- 'test.fastq' # otherwise a long temp dir will be here


###################################################
### code chunk number 3: qrqc.Rnw:64-65
###################################################
s.fastq


###################################################
### code chunk number 4: figPlotQuals
###################################################
plotQuals(s.fastq)


###################################################
### code chunk number 5: figPlotQuals-trimmed
###################################################
s.trimmed.fastq <- readSeqFile(system.file('extdata', 'test-trimmed.fastq', package='qrqc'))
plotQuals(s.trimmed.fastq)


###################################################
### code chunk number 6: figPlotBase-freqs
###################################################
plotBases(s.fastq, type="freq")


###################################################
### code chunk number 7: figPlotBase-prop
###################################################
plotBases(s.fastq, type="prop")


###################################################
### code chunk number 8: figPlotSeqLengths
###################################################
plotSeqLengths(s.trimmed.fastq)


###################################################
### code chunk number 9: figPlotGC
###################################################
plotGC(s.fastq)


###################################################
### code chunk number 10: qrqc.Rnw:249-250
###################################################
sessionInfo()


