###################################################
### chunk number 15: batchplot
###################################################
#line 195 "/tmp/RtmpzlF3B6/R.INSTALL66c19eff/Rolexa/inst/doc/Rolexa-vignette.Rnw"
par(mfrow=c(2,2),cex=.8,mar=c(4, 4, 2, 1)+.1)
BatchAnalysis(run=rolenv,seq=res2$sread,scores=res2$entropy,what="length",main="Length distribution")
BatchAnalysis(run=rolenv,seq=res$sread,scores=res$entropy,what="ratio",main="Complementary bases ratio")
BatchAnalysis(run=rolenv,seq=res$sread,scores=res$entropy,what="iupac",main="IUPAC codes")
BatchAnalysis(run=rolenv,seq=res2$sread,scores=res2$entropy,what="information",main="Base quality")


