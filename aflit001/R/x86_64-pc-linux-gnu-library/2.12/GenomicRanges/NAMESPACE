useDynLib(GenomicRanges)

import(methods)
import(IRanges)

exportClasses(
    Seqinfo,
    GenomicRanges, GRanges, GRangesList,
    GappedAlignments, SummarizedExperiment
)

export(
    seqnames, "seqnames<-", seqlengths, "seqlengths<-",
    strand, "strand<-",
    isCircular, "isCircular<-",
    isCircularWithKnownLength,
    Seqinfo, seqinfo,
    GRanges, GRangesList,
    validCigar, cigarOpTable,
    cigarToQWidth, cigarToWidth, cigarQNarrow, cigarNarrow,
    cigarToIRanges, cigarToIRangesListByAlignment, cigarToIRangesListByRName,
    queryLoc2refLoc, queryLocs2refLocs,
    splitCigar, cigarToRleList, cigarToCigarTable, summarizeCigarTable,
    rname, "rname<-", cigar, qwidth, grglist, grg, rglist, qnarrow,
    readGappedAlignments, GappedAlignments,
    SummarizedExperiment,
    assays, "assays<-", assay, "assay<-", rowData, "rowData<-",
    colData, "colData<-", exptData, "exptData<-"
)

exportMethods(
    length,
    names, "names<-",
    coerce, as.data.frame, unlist,
    seqnames, ranges, strand, seqlengths, elementMetadata,
    "seqnames<-", "ranges<-", "strand<-", "seqlengths<-", "elementMetadata<-",
    isCircular, "isCircular<-",
    isCircularWithKnownLength,
    start, end, width, "start<-", "end<-", "width<-",
    "[", "[<-", c, rev, seqselect, "seqselect<-",
    split, window,
    flank, resize, shift,
    disjoin, gaps, range, reduce,
    coverage,
    union, intersect, setdiff,
    punion, pintersect, psetdiff,
    findOverlaps, countOverlaps, match, "%in%",
    show,
    rname, "rname<-", cigar, qwidth, grglist, grg, rglist, qnarrow,
    dimnames, "dimnames<-"
)

