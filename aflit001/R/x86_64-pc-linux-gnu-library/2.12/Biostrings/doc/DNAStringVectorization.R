###################################################
### chunk number 1: a1
###################################################
#line 69 "/tmp/Rtmpyo0QvT/R.INSTALL5caf62e7/Biostrings/inst/doc/DNAStringVectorization.Rnw"
d <- DNAString("TTGAAAA-CTC-N")
is(d, "XString")


###################################################
### chunk number 2: b1
###################################################
#line 99 "/tmp/Rtmpyo0QvT/R.INSTALL5caf62e7/Biostrings/inst/doc/DNAStringVectorization.Rnw"
v <- XStringViews(c("TTGAAAA-C", "TC-N"), "DNAString")
v


###################################################
### chunk number 3: c1
###################################################
#line 110 "/tmp/Rtmpyo0QvT/R.INSTALL5caf62e7/Biostrings/inst/doc/DNAStringVectorization.Rnw"
library(hgu95av2probe)


###################################################
### chunk number 4: c2
###################################################
#line 114 "/tmp/Rtmpyo0QvT/R.INSTALL5caf62e7/Biostrings/inst/doc/DNAStringVectorization.Rnw"
system.time(z <- XStringViews(hgu95av2probe$sequence, "DNAString"))
z


###################################################
### chunk number 5: d1
###################################################
#line 144 "/tmp/Rtmpyo0QvT/R.INSTALL5caf62e7/Biostrings/inst/doc/DNAStringVectorization.Rnw"
file <- system.file("extdata", "someORF.fa", package="Biostrings")
orf <- read.XStringViews(file, subjectClass="DNAString")
orf
names(orf)


###################################################
### chunk number 6: e1
###################################################
#line 158 "/tmp/Rtmpyo0QvT/R.INSTALL5caf62e7/Biostrings/inst/doc/DNAStringVectorization.Rnw"
orf2 <- XStringViews(orf, "RNAString")


###################################################
### chunk number 7: e2
###################################################
#line 163 "/tmp/Rtmpyo0QvT/R.INSTALL5caf62e7/Biostrings/inst/doc/DNAStringVectorization.Rnw"
subject(orf)@shared
subject(orf2)@shared


