Defaults.Mclust         List of values controlling defaults for some
                        MCLUST functions.
Mclust                  Model-Based Clustering
adjustedRandIndex       Adjusted Rand Index
bic                     BIC for Parameterized Gaussian Mixture Models
bicEMtrain              Select models in discriminant analysis using
                        BIC
cdens                   Component Density for Parameterized MVN Mixture
                        Models
cdensE                  Component Density for a Parameterized MVN
                        Mixture Model
chevron                 Simulated minefield data
clPairs                 Pairwise Scatter Plots showing Classification
classError              Classification error.
coordProj               Coordinate projections of multidimensional data
                        modeled by an MVN mixture.
cross                   Simulated Cross Data
cv1EMtrain              Select discriminant models using cross
                        validation
decomp2sigma            Convert mixture component covariances to matrix
                        form.
defaultPrior            Default conjugate prior for Gaussian mixtures.
dens                    Density for Parameterized MVN Mixtures
densityMclust           Density Estimation via Model-Based Clustering
diabetes                Diabetes data
em                      EM algorithm starting with E-step for
                        parameterized Gaussian mixture models.
emControl               Set control values for use with the EM
                        algorithm.
emE                     EM algorithm starting with E-step for a
                        parameterized Gaussian mixture model.
estep                   E-step for parameterized Gaussian mixture
                        models.
estepE                  E-step in the EM algorithm for a parameterized
                        Gaussian mixture model.
hc                      Model-based Hierarchical Clustering
hcE                     Model-based Hierarchical Clustering
hclass                  Classifications from Hierarchical Agglomeration
hypvol                  Aproximate Hypervolume for Multivariate Data
imputeData              Missing Data Imputation via the mix package
imputePairs             Pairwise Scatter Plots showing Missing Data
                        Imputations
map                     Classification given Probabilities
mapClass                Correspondence between classifications.
mclust1Dplot            Plot one-dimensional data modeled by an MVN
                        mixture.
mclust2Dplot            Plot two-dimensional data modelled by an MVN
                        mixture.
mclustBIC               BIC for Model-Based Clustering
mclustDA                MclustDA discriminant analysis.
mclustDAtest            MclustDA Testing
mclustDAtrain           MclustDA Training
mclustModel             Best model based on BIC.
mclustModelNames        MCLUST Model Names
mclustOptions           Set default values for use with MCLUST.
mclustVariance          Template for variance specification for
                        parameterized Gaussian mixture models.
me                      EM algorithm starting with M-step for
                        parameterized MVN mixture models.
meE                     EM algorithm starting with M-step for a
                        parameterized Gaussian mixture model.
mstep                   M-step for parameterized Gaussian mixture
                        models.
mstepE                  M-step for a parameterized Gaussian mixture
                        model.
mvn                     Univariate or Multivariate Normal Fit
mvnX                    Univariate or Multivariate Normal Fit
nVarParams              Number of Variance Parameters in Gaussian
                        Mixture Models
partconv                Numeric Encoding of a Partitioning
partuniq                Classifies Data According to Unique
                        Observations
plot.Mclust             Plot Model-Based Clustering Results
plot.densityMclust      Plot Univariate Mclust Density
plot.mclustBIC          BIC Plot
plot.mclustDA           Plotting method for MclustDA discriminant
                        analysis.
plot.mclustDAtrain      Plot mclustDA training models.
priorControl            Conjugate Prior for Gaussian Mixtures.
randProj                Random projections of multidimensional data
                        modeled by an MVN mixture.
sigma2decomp            Convert mixture component covariances to
                        decomposition form.
sim                     Simulate from Parameterized MVN Mixture Models
simE                    Simulate from a Parameterized MVN Mixture Model
summary.mclustBIC       Summary Function for model-based clustering.
summary.mclustDAtest    Classification and posterior probability from
                        mclustDAtest.
summary.mclustDAtrain   Models and classifications from mclustDAtrain
summary.mclustModel     Summary Function for MCLUST Models
surfacePlot             Density or uncertainty surface for bivariate
                        mixtures.
uncerPlot               Uncertainty Plot for Model-Based Clustering
unmap                   Indicator Variables given Classification
wreath                  Data Simulated from a 14-Component Mixture
