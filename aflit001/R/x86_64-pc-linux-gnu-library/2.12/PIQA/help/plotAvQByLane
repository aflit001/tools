plotAvgQBaseCallsLaneX         package:PIQA         R Documentation

_P_l_o_t _t_h_e _a_v_e_r_a_g_e _q_u_a_l_i_t_y _o_f _b_a_s_e-_c_a_l_l_s _p_e_r _t_i_l_e

_D_e_s_c_r_i_p_t_i_o_n:

     Plot the average quality of base-calls per tile for a given lane
     in a Illumina 1G Sequencer experiment

_U_s_a_g_e:

     plotAvgQBaseCallsLaneX(D, X, lines = FALSE, legend = TRUE)

_A_r_g_u_m_e_n_t_s:

       D: A summary data set as produced by the function PIQA_rpt()
          that has been loaded into R's name space 

       X: An integer representing a lane number that is contained in
          the data set D 

   lines: A Boolean indicating whether or not the predicted linear
          regression lines should be plotted  

  legend: A Boolean indicating whether or not the plot should include a
          legend 

_D_e_t_a_i_l_s:

     For each tile, the average solexa quality of each base-call
     (A,C,G,T) is taken across all of the cycles of the experiment.

     The data set D is usually automatically loaded into R's name space
     if the PIQA_rpt() function is run. Alternatively, the user can
     generate and load D using the PIQA_rpt() function.

     Setting lines to TRUE might help to visually confirm tendencies in
     the quality along the flow cell. The "lines" parameter divides the
     flow cell in three segments of 100 tiles each and then plots the
     regression line for each segment (the partition into three
     segments is a result of the flow cell of the Illumina 1G sequencer
     physically having three rows of 100 tiles each).

_V_a_l_u_e:

     The function returns a data frame containing the data to generate
     the plot.

_A_u_t_h_o_r(_s):

     Antonio Martinez-Alcantara et. al., questions:  
     antonio@bioinfo.uh.edu

_R_e_f_e_r_e_n_c_e_s:

     PIQA: Pipeline for Illumina G1 Genome Analyzer Data Quality
     Assessment, A. Martinez et al., Bioinformatics Vol. 00 no. 00 2009

_S_e_e _A_l_s_o:

     PIQA_rpt()

_E_x_a_m_p_l_e_s:

     dataSet<-read.csv("http://bioinfo.uh.edu/PIQA/SummaryExample.csv",header=TRUE)
     ##Get the lane number in SummaryExample.csv
     lane <- unique(dataSet$lane)
     ##Produce the plot. m holds the data that generates the plot
     m<-plotAvgQBaseCallsLaneX(dataSet, lane)

