### Name: plotPropBaseCallsByCycleAllTiles
### Title: Plot the proportion of base-calls for a particular cycle
### Aliases: plotPropBaseCallsByCycleAllTiles
### Keywords: Illumina G1 Sequencer, quality control

### ** Examples

dataSet<-read.csv("http://bioinfo.uh.edu/PIQA/SummaryExample.csv",header=TRUE)
##Get the lane number in SummaryExample.csv
lane <- unique(dataSet$lane)
##Produce the plot for cycle number 5. m holds the data that generates the plot
m<-plotPropBaseCallsByCycleAllTiles(dataSet, lane, 5)



