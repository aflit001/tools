### Name: plotAvgQBaseCallsByCycleLaneX
### Title: Plot Average Quality of Base-Calls per Cycle
### Aliases: plotAvgQBaseCallsByCycleLaneX
### Keywords: Illumina G1 Sequencer, quality control

### ** Examples

dataSet<-read.csv("http://bioinfo.uh.edu/PIQA/SummaryExample.csv",header=TRUE)
##Get the lane number in SummaryExample.csv
lane <- unique(dataSet$lane)
##Produce the plot. m holds the data that generates the plot
m<-plotAvgQBaseCallsByCycleLaneX(dataSet, lane)



