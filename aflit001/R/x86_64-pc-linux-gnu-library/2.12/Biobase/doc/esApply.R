###################################################
### chunk number 1: R.hide
###################################################
#line 56 "/tmp/RtmplyP8rE/R.INSTALL4003b6a5/Biobase/inst/doc/esApply.Rnw"
library(Biobase)
data(sample.ExpressionSet)


###################################################
### chunk number 2: R
###################################################
#line 66 "/tmp/RtmplyP8rE/R.INSTALL4003b6a5/Biobase/inst/doc/esApply.Rnw"
print(sample.ExpressionSet)
print(exprs(sample.ExpressionSet)[1,])
print(pData(sample.ExpressionSet)[1:2,1:3])


###################################################
### chunk number 3: R
###################################################
#line 74 "/tmp/RtmplyP8rE/R.INSTALL4003b6a5/Biobase/inst/doc/esApply.Rnw"
print(rbind(exprs(sample.ExpressionSet[1,]),
sex <- t(pData(sample.ExpressionSet))[1,]))


###################################################
### chunk number 4: R
###################################################
#line 82 "/tmp/RtmplyP8rE/R.INSTALL4003b6a5/Biobase/inst/doc/esApply.Rnw"
medContr <- function( y, x ) {
 ys <- split(y,x)
 median(ys[[1]]) - median(ys[[2]])
}


###################################################
### chunk number 5: R
###################################################
#line 92 "/tmp/RtmplyP8rE/R.INSTALL4003b6a5/Biobase/inst/doc/esApply.Rnw"
print(apply(exprs(sample.ExpressionSet[1,,drop=F]), 1,
  medContr, pData(sample.ExpressionSet)[["sex"]]))


###################################################
### chunk number 6: R
###################################################
#line 101 "/tmp/RtmplyP8rE/R.INSTALL4003b6a5/Biobase/inst/doc/esApply.Rnw"
medContr1 <- function(y) {
 ys <- split(y,sex)
 median(ys[[1]]) - median(ys[[2]])
}

print(esApply( sample.ExpressionSet, 1, medContr1)[1])


###################################################
### chunk number 7: 
###################################################
#line 127 "/tmp/RtmplyP8rE/R.INSTALL4003b6a5/Biobase/inst/doc/esApply.Rnw"
sessionInfo()


