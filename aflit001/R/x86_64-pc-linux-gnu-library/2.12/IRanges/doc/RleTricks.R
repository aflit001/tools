###################################################
### chunk number 1: options
###################################################
#line 39 "/tmp/RtmpgCSQZ7/R.INSTALL2bc00c72/IRanges/inst/doc/RleTricks.Rnw"
options(width=60)


###################################################
### chunk number 2: Rle-rollmean
###################################################
#line 43 "/tmp/RtmpgCSQZ7/R.INSTALL2bc00c72/IRanges/inst/doc/RleTricks.Rnw"
rollmeanRle <- function (x, k)
{
  n <- length(x)
  cumsum(c(Rle(sum(window(x, 1, k))), window(x, k + 1, n) - window(x, 1, n - k))) / k
}


###################################################
### chunk number 3: Rle-rollvar
###################################################
#line 52 "/tmp/RtmpgCSQZ7/R.INSTALL2bc00c72/IRanges/inst/doc/RleTricks.Rnw"
rollvarRle <- function(x, k)
{
  n <- length(x)
  means <- rollmeanRle(x, k)
  nextMean <- window(means, 2, n - k + 1)
  cumsum(c(Rle(sum((window(x, 1, k) - means[1])^2)),
  k * diff(means)^2 -
  (window(x, 1, n - k) - nextMean)^2 +
  (window(x, k + 1, n) - nextMean)^2)) / (k - 1)
}


###################################################
### chunk number 4: Rle-rollcov
###################################################
#line 66 "/tmp/RtmpgCSQZ7/R.INSTALL2bc00c72/IRanges/inst/doc/RleTricks.Rnw"
rollcovRle <- function(x, y, k)
{
  n <- length(x)
  meanX <- rollmeanRle(x, k)
  meanY <- rollmeanRle(y, k)
  nextMeanX <- window(meanX, 2, n - k + 1)
  nextMeanY <- window(meanY, 2, n - k + 1)
  cumsum(c(Rle(sum((window(x, 1, k) - meanX[1]) * (window(y, 1, k) - meanY[1]))),
  k * diff(meanX) * diff(meanY) -
  (window(x, 1, n - k) - nextMeanX) * (window(y, 1, n - k) - nextMeanY) +
  (window(x, k + 1, n) - nextMeanX) * (window(y, k + 1, n) - nextMeanY))) / (k - 1)
}


###################################################
### chunk number 5: Rle-rollsd
###################################################
#line 82 "/tmp/RtmpgCSQZ7/R.INSTALL2bc00c72/IRanges/inst/doc/RleTricks.Rnw"
rollsdRle <- function(x, k)
{
  sqrt(rollvarRle(x, k))
}


###################################################
### chunk number 6: Rle-rollcor
###################################################
#line 90 "/tmp/RtmpgCSQZ7/R.INSTALL2bc00c72/IRanges/inst/doc/RleTricks.Rnw"
rollcorRle <- function(x, y, k)
{
  rollcovRle(x, y, k) / (rollsdRle(x, k) * rollsdRle(y, k))
}


