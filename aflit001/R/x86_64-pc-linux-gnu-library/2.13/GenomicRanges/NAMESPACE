useDynLib(GenomicRanges)

import(methods)
import(IRanges)

exportClasses(
    Seqinfo,
    GenomicRanges, GRanges, GRangesList,
    GappedAlignments, SummarizedExperiment
)

export(
    validCigar, cigarOpTable,
    cigarToQWidth, cigarToWidth, cigarQNarrow, cigarNarrow,
    cigarToIRanges, cigarToIRangesListByAlignment, cigarToIRangesListByRName,
    queryLoc2refLoc, queryLocs2refLocs,
    splitCigar, cigarToRleList, cigarToCigarTable, summarizeCigarTable,

    seqinfo, "seqinfo<-",
    seqnames, "seqnames<-",
    seqlevels, "seqlevels<-",
    seqlengths, "seqlengths<-",
    isCircular, "isCircular<-",
    isCircularWithKnownLength,
    Seqinfo,
    strand, "strand<-",
    GRanges, GRangesList,
    rname, "rname<-", cigar, qwidth, grglist, granges, grg, rglist, qnarrow,
    readGappedAlignments, GappedAlignments,
    SummarizedExperiment,
    assays, "assays<-", assay, "assay<-", rowData, "rowData<-",
    colData, "colData<-", exptData, "exptData<-"
)

exportMethods(
    length,
    names, "names<-",
    seqinfo, "seqinfo<-",
    seqnames, "seqnames<-",
    seqlevels, "seqlevels<-",
    seqlengths, "seqlengths<-",
    isCircular, "isCircular<-",
    isCircularWithKnownLength,
    merge,
    coerce, as.data.frame, unlist,
    ranges, strand, elementMetadata,
    "ranges<-", "strand<-", "elementMetadata<-",
    start, end, width, "start<-", "end<-", "width<-",
    "[", "[<-", c, seqselect, "seqselect<-",
    split, window,
    flank, resize, shift,
    disjoin, gaps, range, reduce,
    coverage,
    duplicated, unique, order, sort, rank, "==", "<=", ">=", "<", ">",
    union, intersect, setdiff,
    punion, pintersect, psetdiff,
    findOverlaps, countOverlaps, match, "%in%",
    show,
    rname, "rname<-", cigar, qwidth, grglist, granges, grg, rglist, ngap,
    qnarrow,
    dimnames, "dimnames<-", precede, follow,
    countGenomicOverlaps
)

