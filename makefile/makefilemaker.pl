#!/usr/bin/perl -w
use strict;
use Data::Dumper;
use Panotools::Makefile;
use Panotools::Makefile::Utils qw(quoteshell);
#make --recon -f mk_Pennellii.mk
## MAKEFILE GENERATOR FOR QC CHECK ON ILLUMINA DATA
## 17/09/2011
## V0.1
## SAULO AFLITOS
## PLANT RESEARCH INTERNATIONAL / WAGENINEGN UNIVERSITY / CENTRE FOR BIOSYSTEMS GENOMICS
## RELASED UNDER GPLv3
##
## This program takes a folder name (under BASE) and scans it recursively
## until it finds fastq files. Then, it groups them into runs, libs, and finally
## specie (folder passed)
## At each level it performs several actions. Each level needs all the output
## from the previous level in order to be performed. 


##############
## SETUP
##############
#####
# FOLDERS
#####
my $home                = '/home/aflit001';
my $makeGraph           = 1;                            # export graphics (0 for no)
my $BASE                = $home.'/nobackup/Data';       # base folder where the search will begin
my $OUTF                = $home.'/nobackup/Data_1_mer'; # output folder

#####
# AUXILIARY SCRIPTS
#####
my $MKPLOT              = $home.'/nobackup/filter/mkplot'; # program to create plot
my $FASTQCOUNT          = $home.'/bin/fastqCount';         # program to count kmers in fastq
my $JELLY               = $home.'/bin/jellyfish';

#####
# JELLYFISH PARAMETERS
#####
my $THREADS             = 16;        # NUMBER OF THREADS
my $HASHSIZE            = 800000000; # HASH SIZE
my $MERLEN              = 31;        # KMER LENGTH
my $COUNTERLEN          = 7;         # COUNTER LENGTH

#####
# FASTQ LEVEL PARAMETERS
#####
my $JELLYDELETETEMPMER  = 1;  # DELETE TEMPORARY FILES
my $JELLYGRAPHLENGTH    = 50; # LENGTH OF x FOR GRAPHIC
my $JELLYHISTOHIGH      = $JELLYGRAPHLENGTH - 1;

#####
# RUN LEVEL PARAMETERS
#####
my $RUNDELETETEMPMER    = 0;
my $RUNGRAPHLENGTH      = 100;
my $RUNHISTOHIGH        = $RUNGRAPHLENGTH-1;

#####
# LIBRARY LEVEL PARAMETERS
#####
my $LIBDELETETEMPMER    = 0;
my $LIBGRAPHLENGTH      = 100;
my $LIBHISTOHIGH        = $LIBGRAPHLENGTH-1;

#####
# PARENT (SPECIE) LEVEL PARAMETERS
#####
my $PARENTDELETETEMPMER = 1;
my $PARENTGRAPHLENGTH   = 200;
my $PARENTHISTOHIGH     = $PARENTGRAPHLENGTH - 1;

#####
# SHELL
#####
my $SHELL               = "/bin/bash";

my $INFOLDER            = $ARGV[0];

if ( ! defined $INFOLDER ) {
	print "PLEASE INFORM WHICH FOLDER WOULD YOU LIKE TO BE ANALYZED. ERROR 01";
	print "E.G.: $0 Pennellii";
	exit 1;
}


my $folderData = &readFolder($INFOLDER);    # READ FOLDER, SEARCH FOR FASTQ FILES AND CREATE OUTPUT FILE NAMES
my $maketext   = &parseFolder($folderData); # PARSE FOLDER STRUCTURE AND GENERATE MAKEFILE
my $outFile    = "mk_$INFOLDER.mk";         # OUTPUT FILE
unlink($outFile);                           # DELETE OUTPUT FILE
&makeGraph($outFile, $maketext, [$MKPLOT, $FASTQCOUNT, $JELLY]) if $makeGraph; # CREATE GRAPHIC

# EXPORT MAKEFILE TO OUTPUT FILE
my    $fh;
open  $fh, ">$outFile" or die;
print $fh $maketext;
close $fh;



sub parseFolder {
	my $parent = shift;
	# HOLDS INFORMATION TO CUSTOM BASH SCRIPTS TO BE INSERTED
	#   AFTER PANOTOOLS EXPORTS THE MAKEFILE
	#   PANOTOOLS DOES NOT ALLOWS CUSTOM (MULTILINE, WITH VARIABLES)
	#   SCRIPTS TO BE INSERTED. TO CIRCUMVENT IT WE ADD A NAMED MARKER
	#   ON THE MAKEFILE AS A COMMENT AND, AFTER EXPORTING, REPLACE THE
	#   MARKER WITH THE INTENDED CODE. FOR BETTER READBILITY THE PLACEHOLDER
	#   IS DEFINED IN THE VICINITY OF THE PLACE WHERE IT SHOULD BE INSERTED.
	
	my %placeholder; 
	my $makefile = new Panotools::Makefile;

	$makefile->Comment('######################################');
	$makefile->Comment('CONSTANTS');
	$makefile->Comment('######################################');

	my $var_default = $makefile->Variable('.DEFAULT_GOAL');
	   $var_default->Values('all');

	my $var_base = $makefile->Variable('BASE');
	   $var_base->Values($BASE);

	my $var_outf = $makefile->Variable('OUTF');
	   $var_outf->Values($OUTF);

	my $var_mkpl = $makefile->Variable('MKPLOT');
	   $var_mkpl->Values($MKPLOT);

	my $var_fcount = $makefile->Variable('FCOUNT');
	   $var_fcount->Values($FASTQCOUNT);

	my $var_jelly = $makefile->Variable('JELLY');
	   $var_jelly->Values($JELLY);

	#$(eval whereami=$(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST))) 
	my $var_whoami = $makefile->Variable('<PLACEHOLDER ID=who-am-i FILE=root>');
	#   $var_whoami->Values('PLACEHOLDER');
	$placeholder{'who-am-i'}{'root'}[0] = '
where-am-i := $(CURDIR)/$(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST))
THIS_MAKEFILE := $(call where-am-i)';

	$makefile->Comment('######################################');
	$makefile->Comment('VARIABLES');
	$makefile->Comment('######################################');

	my $var_shel = $makefile->Variable('SHELL');
	   $var_shel->Values($SHELL);
	my $var_thre = $makefile->Variable('THREADS');
	   $var_thre->Values($THREADS);
	my $var_hash = $makefile->Variable('HASHSIZE');
	   $var_hash->Values($HASHSIZE);
	my $var_merl = $makefile->Variable('MERLEN');
	   $var_merl->Values($MERLEN);
	my $var_coun = $makefile->Variable('COUNTERLEN');
	   $var_coun->Values($COUNTERLEN);


	my $var_jdeltmpmer = $makefile->Variable('JELLYDELETETEMPMER');
	   $var_jdeltmpmer->Values($JELLYDELETETEMPMER);
	my $var_jgraphLen  = $makefile->Variable('JELLYGRAPHLENGTH');
	   $var_jgraphLen->Values($JELLYGRAPHLENGTH);
	my $var_jhistoHigh = $makefile->Variable('JELLYHISTOHIGH');
	   $var_jhistoHigh->Values($JELLYHISTOHIGH);

	my $var_rdeltmpmer = $makefile->Variable('RUNDELETETEMPMER');
	   $var_rdeltmpmer->Values($RUNDELETETEMPMER);
	my $var_rgraphLen  = $makefile->Variable('RUNGRAPHLENGTH');
	   $var_rgraphLen->Values($RUNGRAPHLENGTH);
	my $var_rhistoHigh = $makefile->Variable('RUNHISTOHIGH');
	   $var_rhistoHigh->Values($RUNHISTOHIGH);

	my $var_pdeltmpmer = $makefile->Variable('LIBDELETETEMPMER');
	   $var_pdeltmpmer->Values($LIBDELETETEMPMER);
	my $var_pgraphLen  = $makefile->Variable('LIBGRAPHLENGTH');
	   $var_pgraphLen->Values($LIBGRAPHLENGTH);
	my $var_phistoHigh = $makefile->Variable('LIBHISTOHIGH');
	   $var_phistoHigh->Values($LIBHISTOHIGH);

	my $var_Pdeltmpmer = $makefile->Variable('PARENTDELETETEMPMER');
	   $var_Pdeltmpmer->Values($PARENTDELETETEMPMER);
	my $var_PgraphLen  = $makefile->Variable('PARENTGRAPHLENGTH');
	   $var_PgraphLen->Values($PARENTGRAPHLENGTH);
	my $var_PhistoHigh = $makefile->Variable('PARENTHISTOHIGH');
	   $var_PhistoHigh->Values($PARENTHISTOHIGH);

    #$placeholder{error}{all}[1] = ' ERR = $(error found an error!)'."\n\n";

#		@if %ERRORLEVEL% 1; then  \

	# UNIVERSAL PLACEHOLDER TO CHECK IF THE PROGRAM EXITS WITH ERROR OR NOT
	$placeholder{'checkerror'}{'all'}[1] = 
        '   @if [[ $$? != 0 ]]; then  \
			echo FAILED. REASON: $$? ;\
			exit $$?; \
		else \
			echo FINISHED SUCCESSFULLY;\
		fi;\\';
	# UNIVERSAL PLACEHOLDER TO CHECK IF THE PROGRAM EXITS WITH ERROR OR NOT
	#   THIS VARIANTE IS INTENDED TO BE PLACED INSIDE A LOOP, THEREFORE
	#   DOES NOT CONTAIN THE '@' TO SILENT THE LINE FROM BEING
	#   PRINTED TO THE SCREEN. 
	$placeholder{'checkerrorInside'}{'all'}[1] = 
        '   if [[ $$? != 0 ]]; then  \
			echo FAILED. REASON: $$? ;\
			exit $$?; \
		else \
			echo FINISHED SUCCESSFULLY;\
		fi;\\';

	# UNIVERSAL PLACEHOLDER TO CHECK IF THE OUTPUT FILE (VARIABLE $@)
	#   WAS SUCCESSFULLY GENERATED
    $placeholder{outexists}{all}[1] = "\t".'if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\\'."\n\t\t".'exit 1;\\'."\n\t\t".' fi;\\';


	#my $var_vpath1 = $makefile->Variable('vpath');
	#   $var_vpath1->Values('%.jf', '$OUTF');
	#my $var_vpath2 = $makefile->Variable('vpath');
	#   $var_vpath2->Values('%.stat', '$OUTF');
	#my $var_vpath3 = $makefile->Variable('vpath');
	#   $var_vpath3->Values('%.histo', '$OUTF');
	#my $var_vpath4 = $makefile->Variable('vpath');
	#   $var_vpath4->Values('%.png', '$OUTF');
	#my $var_vpath5 = $makefile->Variable('vpath');
	#   $var_vpath5->Values('%.nfo', '$OUTF');

	# STORES THE PARENT DATABASES NAMES (ACTUALLY, JUST ONE ONCE THE PARENT IS THE SPECIE NAME)
	my @parentJfs;
	
	# STORES ALL THE .nfo FILE NAMES TO PUT AS PREREQUISITE TO THE TARGET 'all'
	#   WITH THAT EFECTEVELY FORCING IT TO MAKE ALL .nfo FILES OF THE SPECIE
	my @parentNfo;
	
	# STORES ALL THE OUTPUT FILE NAMES FOR THE PARENT
	my @parentAlls;
	
	# STORES ALL THE PARENT FILE BASE NAMES (SHOULD BE ONLY ONE)
	my @parentNames;
	
	# STORES ALL THE PARENT FILE PHONY NAMES WHICH CONTAIN AS PREREQUISITE
	#   ALL THE PARENT OUTPUT FILES. THIS IS USED AS PREREQUISITE TO THE 'all2'
	#   TARGET FORCING IT TO GENERATE ALL THE PARENT FILES
	my @parentPhonies;
	
	# FOREACH PARENT
	# TODO: CLEAN VARIABLES
	foreach my $RFOLDER ( sort keys %$parent ) {
		my $parentSRC   = $parent->{$RFOLDER}{'PARENT_SRC'};
		my @libJfs;
		
		my $parentMerge = $parent->{$RFOLDER}{'PARENT_MERGE'};
		my $parentStats = $parent->{$RFOLDER}{'PARENT_STATS'};
		my $parentHisto = $parent->{$RFOLDER}{'PARENT_HISTO'};
		my $parentHisPn = $parent->{$RFOLDER}{'PARENT_HISTO_PNG'};
		# PARENT PHONY NAME IS THE SPECIE NAME
		my $parentName  = $RFOLDER;
		# STORES ALL THE PHONY TARGETS OF ALL THE LIBRARIES
		#   EACH LIB PHONY TARGET CONTAIN ALL LIB OUTPUT FILES AS PREREQUISITE
		my @libPhonies;


		
		# FOREACH LIBRARY
		foreach my $LFOLDERSHORT ( sort keys %$parentSRC ) {
			my $librarySRC   = $parentSRC->{$LFOLDERSHORT}{'PROJECT_SRC'};
			
			# LIBRARY PHONE NAME IS THE LIBRARY NAME
			my $libName      = $LFOLDERSHORT;
			
			# HOLDS ALL DATABASE FILE NAMES FROM THE RUNS TO BE MERGED
			#    INTO THE SPECIE DATABASE
			my @runJfs;
			
			# HOLDS THE NAMES OFF ALL THE PHONY RUN TARGETS EACH CONTAINING
			#    ALL THE RUN OUTPUT FILES TO BE USED AS PREREQUISITE TO
			#    THE LIBRARY
			my @runPhonies;
			
			# FOREACH RUN
			foreach my $runName (sort keys %$librarySRC ) {
				my $runSRC   = $librarySRC->{$runName}{'RUN_SRC'};
				
				# STORES THE NAMES OF ALL FASTQ DATABASES TO BE MERGED INTO
				#    THE RUN DATABASE
				my @fastqJfs;
				
				# STORES ALL THE JELLY PHONY TARGETS TO BE USED AS PREREQUISITE
				#    FOR THE RUN TARGET
				my @jellyPhonies;
				
				# FOREACH FILE
				foreach my $fastq (sort keys %$runSRC ) {
					my $fastqNfo     = $runSRC->{$fastq}{'JELLY_NFO'};
					my $fastqNfoKmer = $runSRC->{$fastq}{'JELLY_KMER'};
					my $fastqJout    = $runSRC->{$fastq}{'JELLY_JOUT'};   # _counts
					my $fastqMer     = $runSRC->{$fastq}{'JELLY_MER'};    # _counts_0
					my $fastqMerAll  = $runSRC->{$fastq}{'JELLY_MERALL'}; # _counts_*
					my $fastqJF      = $runSRC->{$fastq}{'JELLY_JF'};
					my $fastqStats   = $runSRC->{$fastq}{'JELLY_STATS'};
					my $fastqHisto   = $runSRC->{$fastq}{'JELLY_HISTO'};
					my $fastqHisPn   = $runSRC->{$fastq}{'JELLY_HISTO_PNG'};
					
                    print "    FASTQ NFO             $fastqNfo\n";
                    print "    FASTQ NFO KER         $fastqNfoKmer\n";
                    print "    FASTQ J OUT           $fastqJout\n";
                    print "    FASTQ MER             $fastqMer\n";
                    print "    FASTQ MER ALL         $fastqMerAll\n";
                    print "    FASTQ JF              $fastqJF\n";
                    print "    FASTQ STATS           $fastqStats\n";
                    print "    FASTQ HISTO           $fastqHisto\n";
                    print "    FASTQ HIS IMG         $fastqHisPn\n";

					# FASTQ PHONY NAME IS THE FILENAME BASENAME STRIPPED FROM
					#    ITS EXTENSION
					my $fastqName    = $fastq;
					   $fastqName    =~ s/.+\/(.+?)\.fastq$/$1/i;
					   
					# FASTQSHORT IF THE FASTQ FILENAME BASENAME REPLACING '.' AND '-' BY '_'
					my $fastqShort   = substr($fastq, rindex($fastq, "/")+1);
                       $fastqShort   =~ s/\.|\-/_/g;

					$makefile->Comment('######################################');
					$makefile->Comment('RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM '.$RFOLDER.' LIBRARY '.$LFOLDERSHORT.' RUN '.$runName.' FASTQ '.$fastq );
					$makefile->Comment('######################################');

                    #$makefile->Comment ('@/bin/echo', '<PLACEHOLDER ID=error FILE=all>');


					$makefile->Comment('MANUALLY COUNTING KMER FOR FASTQ FILE '.$fastq);
					my $rule_jnfo = $makefile->Rule;
					   $rule_jnfo->Targets($fastqNfo);
					   $rule_jnfo->Prerequisites($fastq);
					   $rule_jnfo->Command ('@/bin/echo', 'MANUALLY COUNTING KMER FOR FASTQ FILE '.$fastq.' TO ','$@');
					   $rule_jnfo->Command ('$(FCOUNT)', '-i',$fastq,'-o',$fastqNfo);
					   $rule_jnfo->Command ('@/bin/echo', '<PLACEHOLDER ID=checkerror FILE=all>');
					   $rule_jnfo->Command ('@/bin/echo', '<PLACEHOLDER ID=outexists FILE=all>');



					$makefile->Comment('STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE '.$fastqNfo);
					my $rule_kmer = $makefile->Rule;
					   $rule_kmer->Targets($fastqNfoKmer);
					   $rule_kmer->Prerequisites($fastqNfo);
					   $rule_kmer->Command ('@/bin/echo', 'STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE '.$fastqNfo);

					   #$(eval whereami=$(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST))) 

					   $rule_kmer->Command ('@/bin/echo', '<PLACEHOLDER ID=jellynfokmer FILE='.$fastqNfoKmer.'>');
					   
					   # IF THE NUMBER OF KMERS MANUALLY COUNTED IS BIGGER THAN 0,
					   #   GO AHEAD AND RE-CALL MAKE ASKING FOR THE COMPLETION OF
					   #   THE TASK (CALL JELLY).
					   #   IT CALLS THE PHONY PARENTNAME TARGET WHICH CONTAINS ALL THE
					   #   DESIRED FINAL OUTPUT FILES AS PREREQUISITES, FORCING THE
					   #   RECURSIVE RUN OF THE WHOLE TREE
					   #TODO: FIX IT. SHOULN'T CALL THE WHOLE TREE, ONLY THE APROVED ONE
					   
						$placeholder{'jellynfokmer'}{$fastqNfoKmer}[1] = 
                            '   $(eval '.uc($fastqShort). '_KMERC = '                                    .
                            '$(shell cat "'.$fastqNfo.'" | perl -ne ' . "'" . 'if (/KMER:(\\d+)/) { print $$1 }' . "'" . '))' . "\n\t" .
							'   @/bin/echo KAMERS $('.uc($fastqShort).'_KMERC); '             . "\n\t"         .
							'   @if [ $('.uc($fastqShort).'_KMERC) == "0" ]; then \\'         . "\n\t"         .
							'	    /bin/echo NO KMER; \\'                                    . "\n\t"         .
							'   else \\'                                                      . "\n\t"         .
							'	    /bin/echo HAS KMER; \\'                                   . "\n\t"         .
							'	    /bin/echo I AM $(THIS_MAKEFILE) ; \\'                     . "\n\t"         .
							'	    /bin/echo $(KMERS'.$fastqShort.') > '.$fastqNfoKmer.';\\' . "\n\t"         .
							#'	$(MAKE) -f $(THIS_MAKEFILE) '.$fastqMer.';\\'                 . "\n\t"         .
							#'	$(MAKE) -f $(THIS_MAKEFILE) '.$parentMerge.';\\'              . "\n\t"         .
							#'	$(MAKE) -f $(THIS_MAKEFILE) '.$parentStats.';\\'              . "\n\t"         .
							#'	$(MAKE) -f $(THIS_MAKEFILE) '.$parentHisto.';\\'              . "\n\t"         .
							#'	$(MAKE) -f $(THIS_MAKEFILE) '.$parentHisPn.';\\'              . "\n\t"         .
							#'	$(MAKE) -f $(THIS_MAKEFILE) '.$parentName  .';\\'             . "\n\t"         .
							#'	$(MAKE) -f $(THIS_MAKEFILE) '.$fastqName   .';\\'             . "\n\t"         .
							#'	$(MAKE) -f $(THIS_MAKEFILE) '.$fastqName   .';\\'             . "\n\t"         .
							#'	/bin/echo <PLACEHOLDER ID=checkerrorInside FILE=all>; \\'     . "\n\t"         .
							'   fi;';
					   $rule_kmer->Command ('@/bin/echo', '<PLACEHOLDER ID=checkerror FILE=all>');
					   $rule_kmer->Command ('@/bin/echo', '<PLACEHOLDER ID=outexists FILE=all>');


					push(@parentNfo, $fastqNfoKmer);

					$makefile->Comment('ADDING JELLYFISH KMER COUNT FOR FASTQ FILE '.$fastq);
					my $rule_jmer = $makefile->Rule;
					   $rule_jmer->Targets($fastqMer);
					   $rule_jmer->Prerequisites($fastq, $fastqNfo, $fastqNfoKmer);
					   $rule_jmer->Command ('@/bin/echo', 'ADDING JELLYFISH KMER COUNT FOR FASTQ FILE '.$fastq);
					   $rule_jmer->Command ('$(JELLY)', 'count', '--mer-len=$(MERLEN)', '--threads=$(THREADS)', '--counter-len=$(COUNTERLEN)', '--size=$(HASHSIZE)', '--both-strands', '-o', $fastqJout,  $fastq);
					   $rule_jmer->Command ('@/bin/echo', '<PLACEHOLDER ID=checkerror FILE=all>');
					   $rule_jmer->Command ('@/bin/echo', '<PLACEHOLDER ID=outexists FILE=all>');



					$makefile->Comment('MERGING JELLYFISH KMER COUNT FOR '.$fastqMer.' INTO SINGLE DATABASE FILE '.$fastqJF);
					my $rule_jjf = $makefile->Rule;
					   $rule_jjf->Targets($fastqJF);
					   $rule_jjf->Prerequisites($fastqMer);
					   $rule_jjf->Command ('@/bin/echo', 'MERGING JELLYFISH KMER COUNT FOR '.$fastqMer.' INTO SINGLE DATABASE FILE '.$fastqJF);
					   $rule_jjf->Command ('$(JELLY)', 'merge', '--buffer-size=$(HASHSIZE)', '-o', $fastqJF, $fastqMerAll);
					   $rule_jjf->Command ('@/bin/echo', '<PLACEHOLDER ID=checkerror FILE=all>');
					   $rule_jjf->Command ('@/bin/echo', '<PLACEHOLDER ID=outexists FILE=all>');

					push(@fastqJfs, $fastqJF);


					$makefile->Comment('GENERATING STATISTICS '.$fastqStats.' FOR SINGLE FILE DB '.$fastqJF);
					my $rule_jsta  = $makefile->Rule;
					   $rule_jsta->Targets($fastqStats);
					   $rule_jsta->Prerequisites($fastqJF);
					   $rule_jsta->Command ('@/bin/echo', 'GENERATING STATISTICS '.$fastqStats.' FOR SINGLE FILE DB '.$fastqJF);
					   $rule_jsta->Command ('$(JELLY)', 'stats', '-o', $fastqStats, $fastqJF);
					   $rule_jsta->Command ('@/bin/echo', '<PLACEHOLDER ID=checkerror FILE=all>');
					   $rule_jsta->Command ('@/bin/echo', '<PLACEHOLDER ID=outexists FILE=all>');


					$makefile->Comment('GENERATING HISTOGRAM '.$fastqHisto.' FOR SINGLE FILE DB '.$fastqJF);
					my $rule_jhis  = $makefile->Rule;
					   $rule_jhis->Targets($fastqHisto);
					   $rule_jhis->Prerequisites($fastqJF);
					   $rule_jhis->Command ('@/bin/echo', 'GENERATING HISTOGRAM '.$fastqHisto.' FOR SINGLE FILE DB '.$fastqJF);
					   $rule_jhis->Command ('$(JELLY)', 'histo', '--low=1', '--high=$(JELLYHISTOHIGH)', '--increment=1', '--buffer-size=$(HASHSIZE)', '--threads=$(THREADS)', '-o', $fastqHisto, $fastqJF);
					   $rule_jhis->Command ('@/bin/echo', '<PLACEHOLDER ID=checkerror FILE=all>');
					   $rule_jhis->Command ('@/bin/echo', '<PLACEHOLDER ID=outexists FILE=all>');


					$makefile->Comment('GENERATING HISTOGRAM GRAPHIC '.$fastqHisPn.' FOR HISTOGRAM '.$fastqHisto);
					my $rule_jhiP  = $makefile->Rule;
					   $rule_jhiP->Targets($fastqHisPn);
					   $rule_jhiP->Prerequisites($fastqHisto);
					   $rule_jhiP->Command ('@/bin/echo', 'GENERATING HISTOGRAM GRAPHIC '.$fastqHisPn.' FOR HISTOGRAM '.$fastqHisto);
					   $rule_jhiP->Command ('export', 'MKPLOTLINES=$(JELLYGRAPHLENGTH)');
					   $rule_jhiP->Command ('$(MKPLOT)', $fastqHisto);
					   $rule_jhiP->Command ('@/bin/echo', '<PLACEHOLDER ID=checkerror FILE=all>');
					   $rule_jhiP->Command ('@/bin/echo', '<PLACEHOLDER ID=outexists FILE=all>');
					   
					   
			
					#$makefile->Comment('.PHONY target isn\'t strictly necessary in this case');
					#my $rule_jelly_phony = $makefile->Rule;
					#   $rule_jelly_phony->Targets('.PHONY');
					#   $rule_jelly_phony->Prerequisites($fastqName);
					   
					#my  $rule_jelly = $makefile->Rule($fastqName);
					#	$rule_jelly->Prerequisites($fastqNfo, $fastqNfoKmer, $fastqMer, $fastqJF, $fastqStats, $fastqHisto, $fastqHisPn);
					#	$rule_jelly->Command ('@/bin/echo', 'GENERATING RUN');
					#	$rule_jelly->Command ('@/bin/echo', 'COMPLETED SUCCESSFULLY');
					#	#$rule_all->Command ('echo', '$(USER_SHELL)', '>', 'My File.txt');

					push(@jellyPhonies, $fastqNfo, $fastqNfoKmer, $fastqMer, $fastqJF, $fastqStats, $fastqHisto, $fastqHisPn);
				}
				
				
				
				my $runMerge = $librarySRC->{$runName}{'RUN_MERGE'};
				my $runStats = $librarySRC->{$runName}{'RUN_STATS'};
				my $runHisto = $librarySRC->{$runName}{'RUN_HISTO'};
				my $runHisPn = $librarySRC->{$runName}{'RUN_HISTO_PNG'};

                print "      RUN MERGE           $runMerge\n";
                print "      RUN STATS           $runStats\n";
                print "      RUN HISTO           $runHisto\n";
                print "      RUN HISTO IMG       $runHisPn\n";


				$makefile->Comment('######################################');
				$makefile->Comment('RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM '.$RFOLDER.' LIBRARY '.$LFOLDERSHORT. ' RUN '.$runName);
				$makefile->Comment('######################################');

				$makefile->Comment('MERGING SINGLE FILES DATABASES '.join(", ", @fastqJfs).' ['.@fastqJfs.'] INTO RUN\'s SINGLE DATABASE FILE '.$runMerge);
				my $rule_rmer  = $makefile->Rule;
				   $rule_rmer->Targets($runMerge);
				   #$rule_rmer->Prerequisites(@fastqJfs);
				   $rule_rmer->Prerequisites(@jellyPhonies);
				   $rule_rmer->Command ('@/bin/echo', 'MERGING SINGLE FILES DATABASES '.join(", ", @fastqJfs).' ['.@fastqJfs.'] INTO RUN\'s SINGLE DATABASE FILE '.$runMerge);
				   $rule_rmer->Command ('$(JELLY)', 'merge', '--buffer-size=$(HASHSIZE)', '-o', $runMerge, @fastqJfs);
				   $rule_rmer->Command ('@/bin/echo', '<PLACEHOLDER ID=checkerror FILE=all>');
				   $rule_rmer->Command ('@/bin/echo', '<PLACEHOLDER ID=outexists FILE=all>');

				#jellyfish merge --buffer-size=$HASHSIZE -o $OUTF/$JF $OUTF/$LFOLDERSHORT*.jf
				push(@runJfs, $runMerge);


				$makefile->Comment('GENERATING STATISTICS '.$runStats.' FOR RUN DB '.$runMerge);
				my $rule_rsta  = $makefile->Rule;
				   $rule_rsta->Targets($runStats);
				   $rule_rsta->Prerequisites($runMerge);
				   $rule_rsta->Command ('@/bin/echo', 'GENERATING STATISTICS '.$runStats.' FOR RUN DB '.$runMerge);
				   $rule_rsta->Command ('$(JELLY)', 'stats', '-o', $runStats, $runMerge);
				   $rule_rsta->Command ('@/bin/echo', '<PLACEHOLDER ID=checkerror FILE=all>');
				   $rule_rsta->Command ('@/bin/echo', '<PLACEHOLDER ID=outexists FILE=all>');
				#jellyfish stats -o $OUTF/$LFOLDERSHORT.stats $OUTF/$JF


				$makefile->Comment('GENERATING HISTOGRAM '.$runHisto.' FOR RUN DB '.$runMerge);
				my $rule_rhis  = $makefile->Rule;
				   $rule_rhis->Targets($runHisto);
				   $rule_rhis->Prerequisites($runMerge);
				   $rule_rhis->Command ('@/bin/echo', 'GENERATING HISTOGRAM '.$runHisto.' FOR RUN DB '.$runMerge);
				   $rule_rhis->Command ('$(JELLY)', 'histo', '--low=1', '--high=$(RUNHISTOHIGH)', '--increment=1', '--buffer-size=$(HASHSIZE)', '--threads=$(THREADS)', '-o', $runHisto, $runMerge);
				   $rule_rhis->Command ('@/bin/echo', '<PLACEHOLDER ID=checkerror FILE=all>');
				   $rule_rhis->Command ('@/bin/echo', '<PLACEHOLDER ID=outexists FILE=all>');
				#jellyfish histo --low=1 --high=$PROJHISTOHIGH --increment=1 --buffer-size=$HASHSIZE --threads=$THREADS -o $OUTF/$LFOLDERSHORT.histo $OUTF/$JF";


				$makefile->Comment('GENERATING HISTOGRAM GRAPHIC '.$runHisPn.' FOR RUN HISTOGRAM '.$runHisto);
				my $rule_rhiP  = $makefile->Rule;
				   $rule_rhiP->Targets($runHisPn);
				   $rule_rhiP->Prerequisites($runHisto);
				   $rule_rhiP->Command ('@/bin/echo', 'GENERATING HISTOGRAM GRAPHIC '.$runHisPn.' FOR RUN HISTOGRAM '.$runHisto);
				   $rule_rhiP->Command ('export', 'MKPLOTLINES=$(RUNGRAPHLENGTH)');
				   $rule_rhiP->Command ('$(MKPLOT)', $runHisto);
				   $rule_rhiP->Command ('@/bin/echo', '<PLACEHOLDER ID=checkerror FILE=all>');
				   $rule_rhiP->Command ('@/bin/echo', '<PLACEHOLDER ID=outexists FILE=all>');
               #$MKPLOT $OUTF/$LFOLDERSHORT.histo


		
				#$makefile->Comment('.PHONY target isn\'t strictly necessary in this case');
				#my $rule_run_phony = $makefile->Rule;
				#   $rule_run_phony->Targets('.PHONY');
				#   $rule_run_phony->Prerequisites($runName);			   
			   
				#my  $rule_run = $makefile->Rule($runName);
				#	$rule_run->Prerequisites($runMerge, $runStats, $runHisto, $runHisPn);
				#	$rule_run->Command ('@/bin/echo', 'GENERATING RUN');
				#	$rule_run->Command ('@/bin/echo', 'COMPLETED SUCCESSFULLY');
				#	#$rule_all->Command ('echo', '$(USER_SHELL)', '>', 'My File.txt');
				
				push(@runPhonies, $runMerge, $runStats, $runHisto, $runHisPn);
			}
			
			
			
			
			my $libMerge = $parentSRC->{$LFOLDERSHORT}{'PROJECT_MERGE'};
			my $libStats = $parentSRC->{$LFOLDERSHORT}{'PROJECT_STATS'};
			my $libHisto = $parentSRC->{$LFOLDERSHORT}{'PROJECT_HISTO'};
			my $libHisPn = $parentSRC->{$LFOLDERSHORT}{'PROJECT_HISTO_PNG'};

            print "        LIB MERGE         $libMerge\n";
            print "        LIB STATS         $libStats\n";
            print "        LIB HISTO         $libHisto\n";
            print "        LIB HISTO IMG     $libHisPn\n";


			$makefile->Comment('######################################');
			$makefile->Comment('RUNNING RUNS AND GROUPING THEM INTO LIBRARIES TO ORGANISM '.$RFOLDER.' LIBRARY '.$LFOLDERSHORT);
			$makefile->Comment('######################################');

			$makefile->Comment('MERGING RUN DATABASES '.join(", ", @runJfs).' ['.@runJfs.'] INTO LIBRARY SINGLE DATABASE FILE '.$libMerge);
			my $rule_lmer  = $makefile->Rule;
			   $rule_lmer->Targets($libMerge);
			   #$rule_lmer->Prerequisites(@runJfs);
			   $rule_lmer->Prerequisites(@runPhonies);
			   $rule_lmer->Command ('@/bin/echo', 'MERGING RUN DATABASES '.join(", ", @runJfs).' ['.@runJfs.'] INTO LIBRARY SINGLE DATABASE FILE '.$libMerge);
			   $rule_lmer->Command ('$(JELLY)', 'merge', '--buffer-size=$(HASHSIZE)', '-o', $libMerge, @runJfs);
			   $rule_lmer->Command ('@/bin/echo', '<PLACEHOLDER ID=checkerror FILE=all>');
			   $rule_lmer->Command ('@/bin/echo', '<PLACEHOLDER ID=outexists FILE=all>');

			push(@libJfs, $libMerge);


			$makefile->Comment('GENERATING STATISTICS '.$libStats.' FOR LIBRARY DB '.$libMerge);
			my $rule_lsta  = $makefile->Rule;
			   $rule_lsta->Targets($libStats);
			   $rule_lsta->Prerequisites($libMerge);
			   $rule_lsta->Command ('@/bin/echo', 'GENERATING STATISTICS '.$libStats.' FOR LIBRARY DB '.$libMerge);
			   $rule_lsta->Command ('$(JELLY)', 'stats', '-o', $libStats, $libMerge);
			   $rule_lsta->Command ('@/bin/echo', '<PLACEHOLDER ID=checkerror FILE=all>');
			   $rule_lsta->Command ('@/bin/echo', '<PLACEHOLDER ID=outexists FILE=all>');


			$makefile->Comment('GENERATING HISTOGRAM '.$libHisto.' FOR LIBRARY DB '.$libMerge);
			my $rule_lhis  = $makefile->Rule;
			   $rule_lhis->Targets($libHisto);
			   $rule_lhis->Prerequisites($libMerge);
			   $rule_lhis->Command ('@/bin/echo', 'GENERATING HISTOGRAM '.$libHisto.' FOR LIBRARY DB '.$libMerge);
			   $rule_lhis->Command ('$(JELLY)', 'histo', '--low=1', '--high=$(LIBHISTOHIGH)', '--increment=1', '--buffer-size=$(HASHSIZE)', '--threads=$(THREADS)', '-o', $libHisto, $libMerge);
			   $rule_lhis->Command ('@/bin/echo', '<PLACEHOLDER ID=checkerror FILE=all>');
			   $rule_lhis->Command ('@/bin/echo', '<PLACEHOLDER ID=outexists FILE=all>');


			$makefile->Comment('GENERATING HISTOGRAM GRAPHIC '.$libHisPn.' FOR LIBRARY HISTOGRAM '.$libHisto);
			my $rule_lhiP  = $makefile->Rule;
			   $rule_lhiP->Targets($libHisPn);
			   $rule_lhiP->Prerequisites($libHisto);
			   $rule_lhiP->Command ('@/bin/echo', 'GENERATING HISTOGRAM GRAPHIC '.$libHisPn.' FOR LIBRARY HISTOGRAM '.$libHisto);
			   $rule_lhiP->Command ('export', 'MKPLOTLINES=$(LIBGRAPHLENGTH)');
			   $rule_lhiP->Command ('$(MKPLOT)', $libHisto);
			   $rule_lhiP->Command ('@/bin/echo', '<PLACEHOLDER ID=checkerror FILE=all>');
			   $rule_lhiP->Command ('@/bin/echo', '<PLACEHOLDER ID=outexists FILE=all>');
			   
	
			#$makefile->Comment('.PHONY target isn\'t strictly necessary in this case');
			#my $rule_lib_phony = $makefile->Rule;
			#   $rule_lib_phony->Targets('.PHONY');
			#   $rule_lib_phony->Prerequisites($libName);

			#my  $rule_lib = $makefile->Rule($libName);
			#	$rule_lib->Prerequisites($libMerge, $libStats, $libHisto, $libHisPn);
			#	$rule_lib->Command ('@/bin/echo', 'GENERATING LIBRARY');
			#	$rule_lib->Command ('@/bin/echo', 'COMPLETED SUCCESSFULLY');
			#	#$rule_all->Command ('echo', '$(USER_SHELL)', '>', 'My File.txt');
			   
			push(@libPhonies, $libMerge, $libStats, $libHisto, $libHisPn);
		}
		
		
		

		$makefile->Comment('######################################');
		$makefile->Comment('RUNNING LIBRARIES GROUPING THEM INTO A SINGLE ORGANISM '.$RFOLDER);
		$makefile->Comment('######################################');


        print "          PARENT MERGE    $parentMerge\n";
        print "          PARENT STATS    $parentStats\n";
        print "          PARENT HISTO    $parentHisto\n";
        print "          PARENT HIS PNG  $parentHisPn\n";
        print "          PARENT NAME     $parentName\n\n";


		$makefile->Comment('MERGING LIBRARIES DATABASES '.join(", ", @libJfs).' ['.@libJfs.'] INTO PARENT SINGLE DATABASE FILE '.$parentMerge);
		my $rule_pmer  = $makefile->Rule;
		   $rule_pmer->Targets($parentMerge);
		   #$rule_pmer->Prerequisites(@libJfs);
		   $rule_pmer->Prerequisites(@libPhonies);
		   $rule_pmer->Command ('@/bin/echo', 'MERGING LIBRARIES DATABASES '.join(", ", @libJfs).' ['.@libJfs.'] INTO PARENT SINGLE DATABASE FILE '.$parentMerge);
		   $rule_pmer->Command ('$(JELLY)', 'merge', '--buffer-size=$(HASHSIZE)', '-o', $parentMerge, @libJfs);
		   $rule_pmer->Command ('@/bin/echo', '<PLACEHOLDER ID=checkerror FILE=all>');
		   $rule_pmer->Command ('@/bin/echo', '<PLACEHOLDER ID=outexists FILE=all>');

		push(@parentJfs,   $parentMerge);
		push(@parentAlls,  $parentMerge);
		push(@parentAlls,  $parentStats);
		push(@parentAlls,  $parentHisto);
		push(@parentAlls,  $parentHisPn);
		push(@parentNames, $parentName);


		$makefile->Comment('GENERATING STATISTICS '.$parentStats.' FOR PARENT DB '.$parentMerge);
		my $rule_psta  = $makefile->Rule;
		   $rule_psta->Targets($parentStats);
		   $rule_psta->Prerequisites($parentMerge);
		   $rule_psta->Command ('@/bin/echo', 'GENERATING STATISTICS '.$parentStats.' FOR PARENT DB '.$parentMerge);
		   $rule_psta->Command ('$(JELLY)', 'stats', '-o', $parentStats, $parentMerge);
		   $rule_psta->Command ('@/bin/echo', '<PLACEHOLDER ID=checkerror FILE=all>');
		   $rule_psta->Command ('@/bin/echo', '<PLACEHOLDER ID=outexists FILE=all>');


		$makefile->Comment('GENERATING HISTOGRAM '.$parentHisto.' FOR PARENT DB '.$parentMerge);
		my $rule_phis  = $makefile->Rule;
		   $rule_phis->Targets($parentHisto);
		   $rule_phis->Prerequisites($parentMerge);
		   $rule_phis->Command ('@/bin/echo', 'GENERATING HISTOGRAM '.$parentHisto.' FOR PARENT DB '.$parentMerge);
		   $rule_phis->Command ('$(JELLY)', 'histo', '--low=1', '--high=$(LIBHISTOHIGH)', '--increment=1', '--buffer-size=$(HASHSIZE)', '--threads=$(THREADS)', '-o', $parentHisto, $parentMerge);
		   $rule_phis->Command ('@/bin/echo', '<PLACEHOLDER ID=checkerror FILE=all>');
		   $rule_phis->Command ('@/bin/echo', '<PLACEHOLDER ID=outexists FILE=all>');


		$makefile->Comment('GENERATING HISTOGRAM GRAPHIC '.$parentHisPn.' FOR PARENT HISTOGRAM '.$parentHisto);
		my $rule_phiP  = $makefile->Rule;
		   $rule_phiP->Targets($parentHisPn);
		   $rule_phiP->Prerequisites($parentHisto);
		   $rule_phiP->Command ('@/bin/echo', 'GENERATING HISTOGRAM GRAPHIC '.$parentHisPn.' FOR PARENT HISTOGRAM '.$parentHisto);
		   $rule_phiP->Command ('export', 'MKPLOTLINES=$(PARENTGRAPHLENGTH)');
		   $rule_phiP->Command ('$(MKPLOT)', $parentHisto);
		   $rule_phiP->Command ('@/bin/echo', '<PLACEHOLDER ID=checkerror FILE=all>');
		   $rule_phiP->Command ('@/bin/echo', '<PLACEHOLDER ID=outexists FILE=all>');



		#$makefile->Comment('.PHONY target isn\'t strictly necessary in this case');
		#my $rule_par_phony = $makefile->Rule;
		#   $rule_par_phony->Targets('.PHONY');
		#   $rule_par_phony->Prerequisites($parentName);		   
		   
		#my  $rule_par = $makefile->Rule($parentName);
		#	$rule_par->Prerequisites($parentMerge, $parentStats, $parentHisto, $parentHisPn);
		#	$rule_par->Command ('@/bin/echo', 'GENERATING PARENT');
		#	$rule_par->Command ('@/bin/echo', 'COMPLETED SUCCESSFULLY');
		#	#$rule_all->Command ('echo', '$(USER_SHELL)', '>', 'My File.txt');
		   
		push(@parentPhonies, $parentMerge, $parentStats, $parentHisto, $parentHisPn);
	}




	#$makefile->Comment('.PHONY target isn\'t strictly necessary in this case');
	#my $rule_phony = $makefile->Rule;
	#   $rule_phony->Targets('.PHONY');
	#   $rule_phony->Prerequisites('all');

	#my $rule_all = $makefile->Rule('all');
	#   $rule_all->Prerequisites(@parentNfo);
	#   $rule_all->Command ('@/bin/echo', 'GENERATING MANUAL KMER COUNT');
	#   $rule_all->Command ('@/bin/echo', 'COMPLETED SUCCESSFULLY');
	#   #$rule_all->Command ('echo', '$(USER_SHELL)', '>', 'My File.txt');




	#$makefile->Comment('.PHONY target isn\'t strictly necessary in this case');
	my $rule_phony2 = $makefile->Rule;
	   $rule_phony2->Targets('.PHONY');
	   $rule_phony2->Prerequisites('all');
	   
	my $rule_all2 = $makefile->Rule('all');
	   #$rule_all2->Prerequisites(@parentJfs);
	   #$rule_all2->Prerequisites(@parentAlls);
	   #$rule_all2->Prerequisites(@parentNames);
	   $rule_all2->Prerequisites(@parentPhonies);
	   $rule_all2->Command ('@/bin/echo', 'GENERATING JELLY KMER COUNT AND MERGING');
	   $rule_all2->Command ('@/bin/echo', 'COMPLETED SUCCESSFULLY ALL');
	   #$rule_all->Command ('echo', '$(USER_SHELL)', '>', 'My File.txt');


	# ASSEMBLE THE MAKEFILE
	my $string1 = $makefile->Assemble;
	# CHECK FOR PLACEHOLDERS
	my $string2 = placeholder(\%placeholder, $string1);
	# CHECK FOR NESTED PLACEHOLDERS
	my $string3 = placeholder(\%placeholder, $string2);
	# REPLACE ':' WITH '::' TO AVOID 'duplicated target' ERROR
	my $string4 = $string3;
	   #$string4 =~ s/\.PHONY \:/\.PHONY \:\:/g;
	
	# RETURN MAKEFILE TEXT
	return $string4;
}


sub placeholder {
	my $placeholder = shift;
	my $string      = shift;
	my @string;

	foreach my $line (split("\n", $string)){
		#$rule_jnfo->Command ('@/bin/echo', '<PLACEHOLDER ID=jellynfo FILE='.$fastqNfo.'>');
		#@/bin/echo \<PLACEHOLDER\ ID=jellynfo\ FILE=/home/aflit001/nobackup/Data_0_mer/Pennellii_matepair_5000_matepair_5000_s_6_1_sequence.fastq.nfo\>
		#$placeholder{'jellynfo'}{$fastqNfo} = 

		#print "CHECKING LINE $line\n";
		if ( $line =~ /\<PLACEHOLDER\\* ID=(\S+?)\\* FILE=(\S+?)\\*>\_SHELL/ ) {
			#print "  SKIPPING LINE\n";
		}
		elsif ( $line =~ /\<PLACEHOLDER\\* ID=(\S+?)\\* FILE=(\S+?)\\*>/ ) {
			my $id   = $1;
			my $file = $2;
			#print "  PLACEHOLDER FOUND\n";
			my $codeArr = $placeholder->{$id}{$file};
			for ( my $i = 0; $i < @$codeArr; $i++ )
			{
				if ( defined $codeArr->[$i] ) {
					my $code = $codeArr->[$i];
					my $tab  = "\t"x$i;
					#print "    CONVERTING LINE $line TO $code\n";
					foreach my $codeL (split("\n", $code)) {
						$codeL =~ s/^\s+/$tab/;
						push(@string, $codeL);
					}
				} else {
					#print "    ID $id FILE $file NOW FOUND IN HASH\n";
				}
			}
		} else {
			push(@string, $line);
		}
	}
	my $string2 = join("\n", @string);
	return $string2;
}



sub readFolder {
	my $RFOLDER = shift;
	#SOURCES=`ls $OUTF/"$PARENT"_*.jf | grep -v ".fastq.jf" | perl -ne 'chomp; print; print " "'`
	my %parent;
	
	# CHECKS IF SPECIES FOLDER EXISTS
	if ( -d "$BASE/$RFOLDER" ) {
		print "RFOLDER $RFOLDER\n";
		my %project;
		 # CHECKS IF ILLUMINA FOLDER EXISTS
		if ( -d "$BASE/$RFOLDER/$RFOLDER\_Illumina" ) {
			
			# LIST ALL CHILDREN FOLDERS (LIBRARIES)
			my @FOLDERS = glob("$BASE/$RFOLDER/$RFOLDER\_Illumina/$RFOLDER\_Illumina*");
			
			# FOREACH LIBRARY
			foreach my $LFOLDER (@FOLDERS) {
				print "  LFOLDER $LFOLDER\n";
				
				if ( -d "$LFOLDER" ) {
					print "    LFOLDERSHORTB $LFOLDER\n";
					# GENERATE SHORT NAME
					my $LFOLDERSHORT = substr($LFOLDER, rindex($LFOLDER, "/$RFOLDER\_Illumina")+length("/$RFOLDER\_Illumina")+1);
					print "    LFOLDERSHORTA $LFOLDERSHORT\n";

					# LIST ALL FASTQ FILES
					#TODO: SEARCH FOR .FASTQ.GZ
					my @FASTQFILES   = glob("$LFOLDER/*.fastq");

					my @pairs;
					# FOREACH PAIR OF FASTQ FILES
					#  BEING ORGANIZED ALPHABETICALLY, PAIRS WILL ALWAYS BE
					#  TOGETHER, OTHERWISE ONE LANE IS MISSING AND THE OTHER
					#  SHOULD BE REMOVED (MANUALLY)
					for (my $i = 0; $i < @FASTQFILES; $i += 2) {
						my $p1 = $FASTQFILES[$i+0];
						my $p2 = $FASTQFILES[$i+1];
						print "      P1 $p1\n";
						print "      P2 $p2\n";

						my $runName;
						my $runLane;
						# GET FILE BASE NAME
						my $p1Name   = substr($p1, rindex($p1, "/")+1);
						print "      P1NAME $p1Name\n";

						# EXTRACT RUN NAME AND LANE NUMBER
						if    ( $p1Name =~ /(\S*?_*s_(\d+))_\d+_sequence.fastq/ ) {
							$runName  = defined $1 && $1 ne '' ? $1 : $LFOLDERSHORT;
							$runLane  = $2;
							print "        [PATH 1]\n";
						} 
						elsif ( $p1Name =~ /(\S*?_*s_(\d+))_\d+_.+.fastq/ ) {
							$runName  = defined $1 && $1 ne '' ? $1 : $LFOLDERSHORT;
							$runLane  = $2;
							print "        [PATH 2]\n";
						} 
						elsif ( $p1Name =~ /(sequence_(\d+))_\d+.fastq/i ) {
							$runName  = defined $1 && $1 ne '' ? $1 : $LFOLDERSHORT;
							$runLane  = $2;
							print "        [PATH 3]\n";
						} 
						elsif ( $p1Name =~ /(\S*?_*(\d+))_[f|r].fastq/ ) {
							$runName  = defined $1 && $1 ne '' ? $1 : $LFOLDERSHORT;
							$runLane  = $2;
							print "        [PATH 4]\n";
						} 
						elsif    ( $p1Name =~ /(\S+-(\d+))_\d+_sequence.fastq/ ) {
							$runName  = defined $1 && $1 ne '' ? $1 : $LFOLDERSHORT;
							$runLane  = $2;
							print "        [PATH 5]\n";
						} else {
							# DIES IF NOT ABLE TO EXTRACT INFORMATION
                            #Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq  
							print "  FILE HAS INVALID NAME: ", $p1, "\n";
							exit 1;
						}

						print "      RUN NAME $runName\n";
						print "      RUN LANE $runLane\n";

						# IF PAIR EXISTS, CONTINUE, OTHERWISE DIE
						if    (( $p1 =~ /s_$runLane\_/ ) && ( $p2 =~ /s_$runLane\_/ )) {
							push(@pairs, [$runName, $p1, $p2]);
							print "        PAIR P1 $p1\n";
							print "        PAIR P2 $p2\n";
							print "        RUN NAME $runName LANE $runLane\n";
						}
						elsif (( $p1 =~ /-$runLane\_/ ) && ( $p2 =~ /-$runLane\_/ )) {
							push(@pairs, [$runName, $p1, $p2]);
							print "        PAIR P1 $p1\n";
							print "        PAIR P2 $p2\n";
							print "        RUN NAME $runName LANE $runLane\n";
						}
						elsif (( $p1 =~ /$runLane\_\d/ ) && ( $p2 =~ /$runLane\_\d/ )) {
							push(@pairs, [$runName, $p1, $p2]);
							print "        PAIR P1 $p1\n";
							print "        PAIR P2 $p2\n";
							print "        RUN NAME $runName LANE $runLane\n";
						}
						elsif (( $p1 =~ /_$runLane\_[f|r]/ ) && ( $p2 =~ /_$runLane\_[f|r]/ )) {
							push(@pairs, [$runName, $p1, $p2]);
							print "        PAIR P1 $p1\n";
							print "        PAIR P2 $p2\n";
							print "        RUN NAME $runName LANE $runLane\n";
						} else {
							print "  FASTQ DOES NOT HAVE A PAIR: ", $p1, " AND \n", " "x30, $p2, "\n";
							exit;
						}
					}


					####
					## GENERATE OUTPUT FILENAMES
					####
					my %run;
					print "      PROJECT $LFOLDERSHORT\n";
					foreach my $pair ( @pairs ) {
						my $rn  = $pair->[0];
						my $p1  = $pair->[1];
						my $p2  = $pair->[2];
						my $p1s = substr($p1, rindex($p1, "/")+1); # EXTRACT FILE BASE NAME
						my $p2s = substr($p2, rindex($p2, "/")+1);
						print "        RUN NAME $rn\n";
						print "          P1 $p1 P1S $p1s\n";
						print "          P2 $p2 P2S $p2s\n";

						my %jelly;
						$jelly{$p1}{'JELLY_NFO'}       = "$OUTF/$RFOLDER\_$LFOLDERSHORT\_$rn\_$p1s.nfo";
						$jelly{$p1}{'JELLY_KMER'}      = "$OUTF/$RFOLDER\_$LFOLDERSHORT\_$rn\_$p1s.nfo.KMER";
						$jelly{$p1}{'JELLY_JOUT'}      = "$OUTF/$RFOLDER\_$LFOLDERSHORT\_$rn\_$p1s\_mer_counts";
						$jelly{$p1}{'JELLY_MER'}       = "$OUTF/$RFOLDER\_$LFOLDERSHORT\_$rn\_$p1s\_mer_counts_0";
						$jelly{$p1}{'JELLY_MERALL'}    = "$OUTF/$RFOLDER\_$LFOLDERSHORT\_$rn\_$p1s\_mer_counts_*";
						$jelly{$p1}{'JELLY_JF'}        = "$OUTF/$RFOLDER\_$LFOLDERSHORT\_$rn\_$p1s.jf";
						$jelly{$p1}{'JELLY_STATS'}     = "$OUTF/$RFOLDER\_$LFOLDERSHORT\_$rn\_$p1s.stats";
						$jelly{$p1}{'JELLY_HISTO'}     = "$OUTF/$RFOLDER\_$LFOLDERSHORT\_$rn\_$p1s.histo";
						$jelly{$p1}{'JELLY_HISTO_PNG'} = "$OUTF/$RFOLDER\_$LFOLDERSHORT\_$rn\_$p1s.histo.png";

						$jelly{$p2}{'JELLY_NFO'}       = "$OUTF/$RFOLDER\_$LFOLDERSHORT\_$rn\_$p2s.nfo";
						$jelly{$p2}{'JELLY_KMER'}      = "$OUTF/$RFOLDER\_$LFOLDERSHORT\_$rn\_$p2s.nfo.KMER";
						$jelly{$p2}{'JELLY_JOUT'}      = "$OUTF/$RFOLDER\_$LFOLDERSHORT\_$rn\_$p2s\_mer_counts";
						$jelly{$p2}{'JELLY_MER'}       = "$OUTF/$RFOLDER\_$LFOLDERSHORT\_$rn\_$p2s\_mer_counts_0";
						$jelly{$p2}{'JELLY_MERALL'}    = "$OUTF/$RFOLDER\_$LFOLDERSHORT\_$rn\_$p2s\_mer_counts_*";
						$jelly{$p2}{'JELLY_JF'}        = "$OUTF/$RFOLDER\_$LFOLDERSHORT\_$rn\_$p2s.jf";
						$jelly{$p2}{'JELLY_STATS'}     = "$OUTF/$RFOLDER\_$LFOLDERSHORT\_$rn\_$p2s.stats";
						$jelly{$p2}{'JELLY_HISTO'}     = "$OUTF/$RFOLDER\_$LFOLDERSHORT\_$rn\_$p2s.histo";
						$jelly{$p2}{'JELLY_HISTO_PNG'} = "$OUTF/$RFOLDER\_$LFOLDERSHORT\_$rn\_$p2s.histo.png";

						$run{$rn}{'RUN_MERGE'}         = "$OUTF/$RFOLDER\_$LFOLDERSHORT\_$rn.jf";
						$run{$rn}{'RUN_STATS'}         = "$OUTF/$RFOLDER\_$LFOLDERSHORT\_$rn.stats";
						$run{$rn}{'RUN_HISTO'}         = "$OUTF/$RFOLDER\_$LFOLDERSHORT\_$rn.histo";
						$run{$rn}{'RUN_HISTO_PNG'}     = "$OUTF/$RFOLDER\_$LFOLDERSHORT\_$rn.histo.png";
						# INSERT JELLYFISH RUNS FOR INDIVIDUAL FILES INTO THEIR RESPECTIVE RUN NAME
						$run{$rn}{'RUN_SRC'}           = \%jelly; 
					}
					$project{$LFOLDERSHORT}{'PROJECT_MERGE'}     = "$OUTF/$RFOLDER\_$LFOLDERSHORT.jf";
					$project{$LFOLDERSHORT}{'PROJECT_STATS'}     = "$OUTF/$RFOLDER\_$LFOLDERSHORT.stats";
					$project{$LFOLDERSHORT}{'PROJECT_HISTO'}     = "$OUTF/$RFOLDER\_$LFOLDERSHORT.histo";
					$project{$LFOLDERSHORT}{'PROJECT_HISTO_PNG'} = "$OUTF/$RFOLDER\_$LFOLDERSHORT.histo.png";
					# INSERT EACH INDIVIDUAL RUN INTO THEIR RESPECTIVE LIBRARY
					$project{$LFOLDERSHORT}{'PROJECT_SRC'}       = \%run;
				} else {
					print "FOLDER $LFOLDER DOES NOT EXISTS";
					exit;
				}
			}
		} else {
			print "NO ILLUMINA FOLDER : "."$BASE/$RFOLDER/$RFOLDER\_Illumina";
			exit;
		}

		print "      PARENT $RFOLDER\n";
		$parent{$RFOLDER}{'PARENT_MERGE'}     = "$OUTF/$RFOLDER.jf";
		$parent{$RFOLDER}{'PARENT_STATS'}     = "$OUTF/$RFOLDER.stats";
		$parent{$RFOLDER}{'PARENT_HISTO'}     = "$OUTF/$RFOLDER.histo";
		$parent{$RFOLDER}{'PARENT_HISTO_PNG'} = "$OUTF/$RFOLDER.histo.png";
		# INSERT EACH LIBRARY INTO THEIR RESPECTIVE SPECIE
		$parent{$RFOLDER}{'PARENT_SRC'}       = \%project;
		#SOURCES=`ls $OUTF/"$PARENT"_*.jf | grep -v ".fastq.jf" | perl -ne 'chomp; print; print " "'`
	} else {
		print "NO ILLUMINA FOLDER : "."$BASE/$RFOLDER";
		exit;
	}

	#my $d = Data::Dumper->new([\%parent], ['parent']);
	#$d->Purity(1)->Terse(1)->Deepcopy(1);
	#print $d->Dump;

	# RETURN DATA STRUCTURE
	return \%parent;
}


sub makeGraph {
	my $outputName = shift;
	my $text       = shift;
	my $extProgs   = shift;
	use Makefile::GraphViz;

	print "MAKING GRAPHIC\n";
	#unlink("mk_$projName.png");
	#unlink("mk_$projName.dot");
	#unlink("mk_$projName.svg");
	#print "LEN ", length($text), "\n";
	
	# CLEANING FOR CLARIFICATION

    #my $MKPLOT              = $home.'/nobackup/filter/mkplot'; # program to create plot
    #my $FASTQCOUNT          = $home.'/bin/fastqCount';         # program to count kmers in fastq
    #my $JELLY               = $home.'/bin/jellyfish';

    $text =~ s/$MKPLOT/mkplot/gm;
    $text =~ s/$FASTQCOUNT/fastqCount/gm;
    $text =~ s/$JELLY/jellyfish/gm;

	$text =~ s/$OUTF\///g; # STRIP NAME OF OUTPUT FOLDER FROM TEXT 
	$text =~ s/$BASE\///g; # STRIP NAME OF BASE FOLDER FROM TEXT
	$text =~ s/$home\///g; # STRIP NAME OF BASE FOLDER FROM TEXT

    #cat mk_AllRound.mk.tmp | grep -vE "\\\s*$" | grep -vE "\s+if|fi" | grep -v "#" | grep -v "^\s*$" | less
    $text =~ s/^.+\\\s*$//gm;
    $text =~ s/^\s+if|fi.+$//gm;
    $text =~ s/^\#+.+//gm;
    $text =~ s/^\t+$//gm;
    $text =~ s/\n+/\n/g;
    $text =~ s/\t+\n//g;
    $text =~ s/\t+$//gm;
    $text =~ s/\n+/\n/g;

	#$text =~ s/\/home\/aflit001\/bin\///g;
	#$text =~ s/\/home\/aflit001\/nobackup\/filter\///g;
	
	foreach my $ext ( @$extProgs ) {
		# REDUCE THE FILENAME OF EXTERNAL PROGRAMS
		$ext = substr($ext, 0, rindex($ext, '/'));
		$text =~ s/$ext//g;	
	}
	
	# DELETE ECHOs
	$text =~ s/\@*\/bin\/echo.+//g;
	
	#print "LEN ", length($text), "\n";
	#print $text;

	# EXPORT TO TEMPORARY FILE
	my $fh;
	open  $fh, ">$outputName.tmp" or die;
	print $fh $text;
	close $fh;

	# PARSE THE TEMPORARY FILE
	my $parser = Makefile::GraphViz->new;
	$parser->parse("$outputName.tmp");
	unlink("$outputName.tmp");
	
	# GENERATE GRAPHIC FOR PHONY TARGETS ALL AND ALL2
	#foreach my $target ('all', 'all2')
	foreach my $target ('all')
	{
		print "  MAKING $target\n";
		my $gv = $parser->plot($target);  # A GraphViz object returned.
		$gv->as_png("$outputName.$target.png");
		#$gv->as_text("mk_$projName\_$target.dot");
		#$gv->as_svg("mk_$projName\_$target.svg");
	}

	# GENERATE GRAPHIC FOR ALL OTHER MAIN TARGETS (ENDING IN .jf)
	foreach my $target ( $parser->targets )
	{
		if ( $target =~ /\.jf$/ && $target !~ /fastq/ ) {
			print "  MAKING $target\n";
			my $gv = $parser->plot($target);  # A GraphViz object returned.
			$gv->as_png("$outputName.$target.png");
			#$gv->as_text("mk_$projName\_$target.dot");
			#$gv->as_svg("mk_$projName\_$target.svg");
		} else {
			#print "  NOT PRINTING $target\n";
		}
	}

	# MAKE GLOBAL PLOT
	print "  MAKING ALL\n";
	my $gvAll = $parser->plot_all;  # A GraphViz object returned.
	$gvAll->as_png("$outputName.png");
	#$gvAll->as_text("mk_$projName.dot");
	#$gvAll->as_svg("mk_$projName.svg");

	print "GRAPHIC EXPORTED\n";
}



#my $makefile = new Panotools::Makefile;

#my $var_user = $makefile->Variable('USER');
#   $var_user->Values("Dr. Largio d'Apalansius (MB)");

#my $rule_all = $makefile->Rule('all');
#   $rule_all->Prerequisites('various');
#   $rule_all->Command ('echo', '$(USER_SHELL)', '>', 'My File.txt');

#$makefile->Comment('.PHONY target isn\'t strictly necessary in this case');
#my $rule_phony = $makefile->Rule;
#   $rule_phony->Targets('.PHONY');
#   $rule_phony->Prerequisites('all');

#my $string = $makefile->Assemble;
#print $string;
#exit;


1;
