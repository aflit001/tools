# Created by Panotools::Script 0.26


# ######################################

# CONSTANTS

# ######################################
.DEFAULT_GOAL = all
.DEFAULT_GOAL_SHELL = all
BASE = /home/aflit001/nobackup/Data
BASE_SHELL = /home/aflit001/nobackup/Data
OUTF = /home/aflit001/nobackup/Data_1_mer
OUTF_SHELL = /home/aflit001/nobackup/Data_1_mer
MKPLOT = /home/aflit001/nobackup/filter/mkplot
MKPLOT_SHELL = /home/aflit001/nobackup/filter/mkplot
FCOUNT = /home/aflit001/bin/fastqCount
FCOUNT_SHELL = /home/aflit001/bin/fastqCount
JELLY = /home/aflit001/bin/jellyfish
JELLY_SHELL = /home/aflit001/bin/jellyfish

where-am-i := $(CURDIR)/$(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST))
THIS_MAKEFILE := $(call where-am-i)

# ######################################

# VARIABLES

# ######################################
SHELL = /bin/bash
SHELL_SHELL = /bin/bash
THREADS = 16
THREADS_SHELL = 16
HASHSIZE = 800000000
HASHSIZE_SHELL = 800000000
MERLEN = 31
MERLEN_SHELL = 31
COUNTERLEN = 7
COUNTERLEN_SHELL = 7
JELLYDELETETEMPMER = 1
JELLYDELETETEMPMER_SHELL = 1
JELLYGRAPHLENGTH = 50
JELLYGRAPHLENGTH_SHELL = 50
JELLYHISTOHIGH = 49
JELLYHISTOHIGH_SHELL = 49
RUNDELETETEMPMER = 0
RUNDELETETEMPMER_SHELL = 0
RUNGRAPHLENGTH = 100
RUNGRAPHLENGTH_SHELL = 100
RUNHISTOHIGH = 99
RUNHISTOHIGH_SHELL = 99
LIBDELETETEMPMER = 0
LIBDELETETEMPMER_SHELL = 0
LIBGRAPHLENGTH = 100
LIBGRAPHLENGTH_SHELL = 100
LIBHISTOHIGH = 99
LIBHISTOHIGH_SHELL = 99
PARENTDELETETEMPMER = 1
PARENTDELETETEMPMER_SHELL = 1
PARENTGRAPHLENGTH = 200
PARENTGRAPHLENGTH_SHELL = 200
PARENTHISTOHIGH = 199
PARENTHISTOHIGH_SHELL = 199

# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Pimpinelifolium LIBRARY lib RUN sequence_2 FASTQ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_2_1.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_2_1.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.nfo : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_2_1.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_2_1.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_2_1.fastq -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.nfo
	$(eval SEQUENCE_2_1_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(SEQUENCE_2_1_FASTQ_KMERC); 
	@if [ $(SEQUENCE_2_1_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSsequence_2_1_fastq) > /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_2_1.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_2_1.fastq /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_2_1.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq_mer_counts /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_2_1.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Pimpinelifolium LIBRARY lib RUN sequence_2 FASTQ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_2_2.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_2_2.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.nfo : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_2_2.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_2_2.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_2_2.fastq -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.nfo
	$(eval SEQUENCE_2_2_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(SEQUENCE_2_2_FASTQ_KMERC); 
	@if [ $(SEQUENCE_2_2_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSsequence_2_2_fastq) > /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_2_2.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_2_2.fastq /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_2_2.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq_mer_counts /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_2_2.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Pimpinelifolium LIBRARY lib RUN sequence_2

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.jf : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_1.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2_sequence_2_2.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.stats : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.histo : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.histo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.histo.png : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Pimpinelifolium LIBRARY lib RUN sequence_3 FASTQ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_3_1.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_3_1.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.nfo : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_3_1.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_3_1.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_3_1.fastq -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.nfo
	$(eval SEQUENCE_3_1_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(SEQUENCE_3_1_FASTQ_KMERC); 
	@if [ $(SEQUENCE_3_1_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSsequence_3_1_fastq) > /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_3_1.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_3_1.fastq /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_3_1.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq_mer_counts /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_3_1.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Pimpinelifolium LIBRARY lib RUN sequence_3 FASTQ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_3_2.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_3_2.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.nfo : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_3_2.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_3_2.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_3_2.fastq -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.nfo
	$(eval SEQUENCE_3_2_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(SEQUENCE_3_2_FASTQ_KMERC); 
	@if [ $(SEQUENCE_3_2_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSsequence_3_2_fastq) > /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_3_2.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_3_2.fastq /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_3_2.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq_mer_counts /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_3_2.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Pimpinelifolium LIBRARY lib RUN sequence_3

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.jf : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_1.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3_sequence_3_2.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.stats : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.histo : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.histo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.histo.png : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Pimpinelifolium LIBRARY lib RUN sequence_4 FASTQ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_4_1.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_4_1.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.nfo : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_4_1.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_4_1.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_4_1.fastq -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.nfo
	$(eval SEQUENCE_4_1_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(SEQUENCE_4_1_FASTQ_KMERC); 
	@if [ $(SEQUENCE_4_1_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSsequence_4_1_fastq) > /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_4_1.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_4_1.fastq /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_4_1.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq_mer_counts /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_4_1.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Pimpinelifolium LIBRARY lib RUN sequence_4 FASTQ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_4_2.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_4_2.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.nfo : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_4_2.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_4_2.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_4_2.fastq -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.nfo
	$(eval SEQUENCE_4_2_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(SEQUENCE_4_2_FASTQ_KMERC); 
	@if [ $(SEQUENCE_4_2_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSsequence_4_2_fastq) > /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_4_2.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_4_2.fastq /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_4_2.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq_mer_counts /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_4_2.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Pimpinelifolium LIBRARY lib RUN sequence_4

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.jf : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_1.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4_sequence_4_2.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.stats : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.histo : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.histo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.histo.png : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Pimpinelifolium LIBRARY lib RUN sequence_5 FASTQ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_5_1.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_5_1.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.nfo : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_5_1.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_5_1.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_5_1.fastq -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.nfo
	$(eval SEQUENCE_5_1_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(SEQUENCE_5_1_FASTQ_KMERC); 
	@if [ $(SEQUENCE_5_1_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSsequence_5_1_fastq) > /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_5_1.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_5_1.fastq /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_5_1.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq_mer_counts /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_5_1.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Pimpinelifolium LIBRARY lib RUN sequence_5 FASTQ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_5_2.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_5_2.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.nfo : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_5_2.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_5_2.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_5_2.fastq -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.nfo
	$(eval SEQUENCE_5_2_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(SEQUENCE_5_2_FASTQ_KMERC); 
	@if [ $(SEQUENCE_5_2_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSsequence_5_2_fastq) > /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_5_2.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_5_2.fastq /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_5_2.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq_mer_counts /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_5_2.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Pimpinelifolium LIBRARY lib RUN sequence_5

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.jf : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_1.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5_sequence_5_2.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.stats : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.histo : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.histo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.histo.png : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Pimpinelifolium LIBRARY lib RUN sequence_6 FASTQ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_6_1.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_6_1.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.nfo : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_6_1.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_6_1.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_6_1.fastq -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.nfo
	$(eval SEQUENCE_6_1_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(SEQUENCE_6_1_FASTQ_KMERC); 
	@if [ $(SEQUENCE_6_1_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSsequence_6_1_fastq) > /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_6_1.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_6_1.fastq /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_6_1.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq_mer_counts /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_6_1.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Pimpinelifolium LIBRARY lib RUN sequence_6 FASTQ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_6_2.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_6_2.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.nfo : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_6_2.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_6_2.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_6_2.fastq -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.nfo
	$(eval SEQUENCE_6_2_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(SEQUENCE_6_2_FASTQ_KMERC); 
	@if [ $(SEQUENCE_6_2_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSsequence_6_2_fastq) > /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_6_2.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_6_2.fastq /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_6_2.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq_mer_counts /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_6_2.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Pimpinelifolium LIBRARY lib RUN sequence_6

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.jf : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_1.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6_sequence_6_2.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.stats : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.histo : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.histo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.histo.png : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Pimpinelifolium LIBRARY lib RUN sequence_7 FASTQ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_7_1.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_7_1.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.nfo : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_7_1.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_7_1.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_7_1.fastq -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.nfo
	$(eval SEQUENCE_7_1_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(SEQUENCE_7_1_FASTQ_KMERC); 
	@if [ $(SEQUENCE_7_1_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSsequence_7_1_fastq) > /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_7_1.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_7_1.fastq /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_7_1.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq_mer_counts /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_7_1.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Pimpinelifolium LIBRARY lib RUN sequence_7 FASTQ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_7_2.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_7_2.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.nfo : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_7_2.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_7_2.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_7_2.fastq -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.nfo
	$(eval SEQUENCE_7_2_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(SEQUENCE_7_2_FASTQ_KMERC); 
	@if [ $(SEQUENCE_7_2_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSsequence_7_2_fastq) > /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_7_2.fastq
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_7_2.fastq /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_7_2.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq_mer_counts /home/aflit001/nobackup/Data/Pimpinelifolium/Pimpinelifolium_Illumina/Pimpinelifolium_Illumina_lib/sequence_7_2.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Pimpinelifolium LIBRARY lib RUN sequence_7

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.jf : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_1.fastq.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7_sequence_7_2.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.stats : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.histo : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.histo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.histo.png : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING RUNS AND GROUPING THEM INTO LIBRARIES TO ORGANISM Pimpinelifolium LIBRARY lib

# ######################################

# MERGING RUN DATABASES /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.jf, /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.jf, /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.jf, /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.jf, /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.jf, /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.jf [6] INTO LIBRARY SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.jf : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.histo.png /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.histo.png /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.histo.png /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.histo.png /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.histo.png /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.histo.png
	@/bin/echo MERGING\ RUN\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.jf,\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.jf,\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.jf,\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.jf,\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.jf,\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.jf\ [6]\ INTO\ LIBRARY\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_2.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_3.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_4.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_5.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_6.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib_sequence_7.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.stats FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.stats : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.stats\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.histo FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.histo : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.histo\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.jf
	$(JELLY) histo --low=1 --high=$(LIBHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.histo.png FOR LIBRARY HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.histo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.histo.png : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.histo.png\ FOR\ LIBRARY\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.histo
	export MKPLOTLINES=$(LIBGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING LIBRARIES GROUPING THEM INTO A SINGLE ORGANISM Pimpinelifolium

# ######################################

# MERGING LIBRARIES DATABASES /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.jf [1] INTO PARENT SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.jf : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.histo.png
	@/bin/echo MERGING\ LIBRARIES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.jf\ [1]\ INTO\ PARENT\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium_lib.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.stats FOR PARENT DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.stats : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.stats\ FOR\ PARENT\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.histo FOR PARENT DB /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.jf
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.histo : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.histo\ FOR\ PARENT\ DB\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.jf
	$(JELLY) histo --low=1 --high=$(LIBHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.histo.png FOR PARENT HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.histo
/home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.histo.png : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.histo.png\ FOR\ PARENT\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.histo
	export MKPLOTLINES=$(PARENTGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\

.PHONY : all

all : /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.jf /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.stats /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.histo /home/aflit001/nobackup/Data_1_mer/Pimpinelifolium.histo.png
	@/bin/echo GENERATING\ JELLY\ KMER\ COUNT\ AND\ MERGING
	@/bin/echo COMPLETED\ SUCCESSFULLY\ ALL