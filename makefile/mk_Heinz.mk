# Created by Panotools::Script 0.26


# ######################################

# CONSTANTS

# ######################################
.DEFAULT_GOAL = all
.DEFAULT_GOAL_SHELL = all
BASE = /home/aflit001/nobackup/Data
BASE_SHELL = /home/aflit001/nobackup/Data
OUTF = /home/aflit001/nobackup/Data_1_mer
OUTF_SHELL = /home/aflit001/nobackup/Data_1_mer
MKPLOT = /home/aflit001/nobackup/filter/mkplot
MKPLOT_SHELL = /home/aflit001/nobackup/filter/mkplot
FCOUNT = /home/aflit001/bin/fastqCount
FCOUNT_SHELL = /home/aflit001/bin/fastqCount
JELLY = /home/aflit001/bin/jellyfish
JELLY_SHELL = /home/aflit001/bin/jellyfish

where-am-i := $(CURDIR)/$(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST))
THIS_MAKEFILE := $(call where-am-i)

# ######################################

# VARIABLES

# ######################################
SHELL = /bin/bash
SHELL_SHELL = /bin/bash
THREADS = 16
THREADS_SHELL = 16
HASHSIZE = 800000000
HASHSIZE_SHELL = 800000000
MERLEN = 31
MERLEN_SHELL = 31
COUNTERLEN = 7
COUNTERLEN_SHELL = 7
JELLYDELETETEMPMER = 1
JELLYDELETETEMPMER_SHELL = 1
JELLYGRAPHLENGTH = 50
JELLYGRAPHLENGTH_SHELL = 50
JELLYHISTOHIGH = 49
JELLYHISTOHIGH_SHELL = 49
RUNDELETETEMPMER = 0
RUNDELETETEMPMER_SHELL = 0
RUNGRAPHLENGTH = 100
RUNGRAPHLENGTH_SHELL = 100
RUNHISTOHIGH = 99
RUNHISTOHIGH_SHELL = 99
LIBDELETETEMPMER = 0
LIBDELETETEMPMER_SHELL = 0
LIBGRAPHLENGTH = 100
LIBGRAPHLENGTH_SHELL = 100
LIBHISTOHIGH = 99
LIBHISTOHIGH_SHELL = 99
PARENTDELETETEMPMER = 1
PARENTDELETETEMPMER_SHELL = 1
PARENTGRAPHLENGTH = 200
PARENTGRAPHLENGTH_SHELL = 200
PARENTHISTOHIGH = 199
PARENTHISTOHIGH_SHELL = 199

# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY matepair_2000 RUN 3018DAAXX_1 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/3018DAAXX_1_f.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/3018DAAXX_1_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/3018DAAXX_1_f.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/3018DAAXX_1_f.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/3018DAAXX_1_f.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.nfo
	$(eval 3018DAAXX_1_F_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(3018DAAXX_1_F_FASTQ_KMERC); 
	@if [ $(3018DAAXX_1_F_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS3018DAAXX_1_f_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/3018DAAXX_1_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/3018DAAXX_1_f.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/3018DAAXX_1_f.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/3018DAAXX_1_f.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY matepair_2000 RUN 3018DAAXX_1 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/3018DAAXX_1_r.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/3018DAAXX_1_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/3018DAAXX_1_r.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/3018DAAXX_1_r.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/3018DAAXX_1_r.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.nfo
	$(eval 3018DAAXX_1_R_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(3018DAAXX_1_R_FASTQ_KMERC); 
	@if [ $(3018DAAXX_1_R_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS3018DAAXX_1_r_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/3018DAAXX_1_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/3018DAAXX_1_r.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/3018DAAXX_1_r.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/3018DAAXX_1_r.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Heinz LIBRARY matepair_2000 RUN 3018DAAXX_1

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1_3018DAAXX_1_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY matepair_2000 RUN 30THBAAXX_1 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/30THBAAXX_1_f.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/30THBAAXX_1_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/30THBAAXX_1_f.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/30THBAAXX_1_f.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/30THBAAXX_1_f.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.nfo
	$(eval 30THBAAXX_1_F_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(30THBAAXX_1_F_FASTQ_KMERC); 
	@if [ $(30THBAAXX_1_F_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS30THBAAXX_1_f_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/30THBAAXX_1_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/30THBAAXX_1_f.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/30THBAAXX_1_f.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/30THBAAXX_1_f.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY matepair_2000 RUN 30THBAAXX_1 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/30THBAAXX_1_r.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/30THBAAXX_1_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/30THBAAXX_1_r.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/30THBAAXX_1_r.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/30THBAAXX_1_r.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.nfo
	$(eval 30THBAAXX_1_R_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(30THBAAXX_1_R_FASTQ_KMERC); 
	@if [ $(30THBAAXX_1_R_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS30THBAAXX_1_r_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/30THBAAXX_1_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/30THBAAXX_1_r.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/30THBAAXX_1_r.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_2000/30THBAAXX_1_r.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Heinz LIBRARY matepair_2000 RUN 30THBAAXX_1

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1_30THBAAXX_1_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING RUNS AND GROUPING THEM INTO LIBRARIES TO ORGANISM Heinz LIBRARY matepair_2000

# ######################################

# MERGING RUN DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.jf [2] INTO LIBRARY SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.histo.png
	@/bin/echo MERGING\ RUN\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.jf\ [2]\ INTO\ LIBRARY\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_3018DAAXX_1.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000_30THBAAXX_1.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.stats FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.stats\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.histo FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.histo\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.jf
	$(JELLY) histo --low=1 --high=$(LIBHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.histo.png FOR LIBRARY HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.histo.png\ FOR\ LIBRARY\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.histo
	export MKPLOTLINES=$(LIBGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY matepair_3000 RUN 3018DAAXX_2 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/3018DAAXX_2_f.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/3018DAAXX_2_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/3018DAAXX_2_f.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/3018DAAXX_2_f.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/3018DAAXX_2_f.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.nfo
	$(eval 3018DAAXX_2_F_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(3018DAAXX_2_F_FASTQ_KMERC); 
	@if [ $(3018DAAXX_2_F_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS3018DAAXX_2_f_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/3018DAAXX_2_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/3018DAAXX_2_f.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/3018DAAXX_2_f.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/3018DAAXX_2_f.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY matepair_3000 RUN 3018DAAXX_2 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/3018DAAXX_2_r.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/3018DAAXX_2_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/3018DAAXX_2_r.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/3018DAAXX_2_r.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/3018DAAXX_2_r.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.nfo
	$(eval 3018DAAXX_2_R_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(3018DAAXX_2_R_FASTQ_KMERC); 
	@if [ $(3018DAAXX_2_R_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS3018DAAXX_2_r_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/3018DAAXX_2_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/3018DAAXX_2_r.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/3018DAAXX_2_r.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/3018DAAXX_2_r.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Heinz LIBRARY matepair_3000 RUN 3018DAAXX_2

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2_3018DAAXX_2_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY matepair_3000 RUN 30THBAAXX_2 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/30THBAAXX_2_f.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/30THBAAXX_2_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/30THBAAXX_2_f.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/30THBAAXX_2_f.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/30THBAAXX_2_f.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.nfo
	$(eval 30THBAAXX_2_F_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(30THBAAXX_2_F_FASTQ_KMERC); 
	@if [ $(30THBAAXX_2_F_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS30THBAAXX_2_f_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/30THBAAXX_2_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/30THBAAXX_2_f.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/30THBAAXX_2_f.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/30THBAAXX_2_f.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY matepair_3000 RUN 30THBAAXX_2 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/30THBAAXX_2_r.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/30THBAAXX_2_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/30THBAAXX_2_r.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/30THBAAXX_2_r.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/30THBAAXX_2_r.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.nfo
	$(eval 30THBAAXX_2_R_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(30THBAAXX_2_R_FASTQ_KMERC); 
	@if [ $(30THBAAXX_2_R_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS30THBAAXX_2_r_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/30THBAAXX_2_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/30THBAAXX_2_r.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/30THBAAXX_2_r.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_3000/30THBAAXX_2_r.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Heinz LIBRARY matepair_3000 RUN 30THBAAXX_2

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2_30THBAAXX_2_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING RUNS AND GROUPING THEM INTO LIBRARIES TO ORGANISM Heinz LIBRARY matepair_3000

# ######################################

# MERGING RUN DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.jf [2] INTO LIBRARY SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.histo.png
	@/bin/echo MERGING\ RUN\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.jf\ [2]\ INTO\ LIBRARY\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_3018DAAXX_2.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000_30THBAAXX_2.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.stats FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.stats\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.histo FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.histo\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.jf
	$(JELLY) histo --low=1 --high=$(LIBHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.histo.png FOR LIBRARY HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.histo.png\ FOR\ LIBRARY\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.histo
	export MKPLOTLINES=$(LIBGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY matepair_4000 RUN 3018DAAXX_3 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/3018DAAXX_3_f.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/3018DAAXX_3_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/3018DAAXX_3_f.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/3018DAAXX_3_f.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/3018DAAXX_3_f.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.nfo
	$(eval 3018DAAXX_3_F_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(3018DAAXX_3_F_FASTQ_KMERC); 
	@if [ $(3018DAAXX_3_F_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS3018DAAXX_3_f_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/3018DAAXX_3_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/3018DAAXX_3_f.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/3018DAAXX_3_f.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/3018DAAXX_3_f.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY matepair_4000 RUN 3018DAAXX_3 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/3018DAAXX_3_r.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/3018DAAXX_3_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/3018DAAXX_3_r.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/3018DAAXX_3_r.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/3018DAAXX_3_r.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.nfo
	$(eval 3018DAAXX_3_R_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(3018DAAXX_3_R_FASTQ_KMERC); 
	@if [ $(3018DAAXX_3_R_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS3018DAAXX_3_r_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/3018DAAXX_3_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/3018DAAXX_3_r.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/3018DAAXX_3_r.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/3018DAAXX_3_r.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Heinz LIBRARY matepair_4000 RUN 3018DAAXX_3

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3_3018DAAXX_3_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY matepair_4000 RUN 30THBAAXX_3 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/30THBAAXX_3_f.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/30THBAAXX_3_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/30THBAAXX_3_f.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/30THBAAXX_3_f.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/30THBAAXX_3_f.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.nfo
	$(eval 30THBAAXX_3_F_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(30THBAAXX_3_F_FASTQ_KMERC); 
	@if [ $(30THBAAXX_3_F_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS30THBAAXX_3_f_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/30THBAAXX_3_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/30THBAAXX_3_f.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/30THBAAXX_3_f.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/30THBAAXX_3_f.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY matepair_4000 RUN 30THBAAXX_3 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/30THBAAXX_3_r.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/30THBAAXX_3_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/30THBAAXX_3_r.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/30THBAAXX_3_r.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/30THBAAXX_3_r.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.nfo
	$(eval 30THBAAXX_3_R_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(30THBAAXX_3_R_FASTQ_KMERC); 
	@if [ $(30THBAAXX_3_R_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS30THBAAXX_3_r_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/30THBAAXX_3_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/30THBAAXX_3_r.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/30THBAAXX_3_r.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_4000/30THBAAXX_3_r.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Heinz LIBRARY matepair_4000 RUN 30THBAAXX_3

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3_30THBAAXX_3_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING RUNS AND GROUPING THEM INTO LIBRARIES TO ORGANISM Heinz LIBRARY matepair_4000

# ######################################

# MERGING RUN DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.jf [2] INTO LIBRARY SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.histo.png
	@/bin/echo MERGING\ RUN\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.jf\ [2]\ INTO\ LIBRARY\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_3018DAAXX_3.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000_30THBAAXX_3.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.stats FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.stats\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.histo FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.histo\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.jf
	$(JELLY) histo --low=1 --high=$(LIBHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.histo.png FOR LIBRARY HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.histo.png\ FOR\ LIBRARY\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.histo
	export MKPLOTLINES=$(LIBGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY matepair_5000 RUN 3018DAAXX_4 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/3018DAAXX_4_f.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/3018DAAXX_4_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/3018DAAXX_4_f.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/3018DAAXX_4_f.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/3018DAAXX_4_f.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.nfo
	$(eval 3018DAAXX_4_F_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(3018DAAXX_4_F_FASTQ_KMERC); 
	@if [ $(3018DAAXX_4_F_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS3018DAAXX_4_f_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/3018DAAXX_4_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/3018DAAXX_4_f.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/3018DAAXX_4_f.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/3018DAAXX_4_f.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY matepair_5000 RUN 3018DAAXX_4 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/3018DAAXX_4_r.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/3018DAAXX_4_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/3018DAAXX_4_r.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/3018DAAXX_4_r.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/3018DAAXX_4_r.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.nfo
	$(eval 3018DAAXX_4_R_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(3018DAAXX_4_R_FASTQ_KMERC); 
	@if [ $(3018DAAXX_4_R_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS3018DAAXX_4_r_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/3018DAAXX_4_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/3018DAAXX_4_r.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/3018DAAXX_4_r.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/3018DAAXX_4_r.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Heinz LIBRARY matepair_5000 RUN 3018DAAXX_4

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4_3018DAAXX_4_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY matepair_5000 RUN 30THBAAXX_4 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/30THBAAXX_4_f.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/30THBAAXX_4_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/30THBAAXX_4_f.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/30THBAAXX_4_f.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/30THBAAXX_4_f.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.nfo
	$(eval 30THBAAXX_4_F_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(30THBAAXX_4_F_FASTQ_KMERC); 
	@if [ $(30THBAAXX_4_F_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS30THBAAXX_4_f_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/30THBAAXX_4_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/30THBAAXX_4_f.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/30THBAAXX_4_f.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/30THBAAXX_4_f.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY matepair_5000 RUN 30THBAAXX_4 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/30THBAAXX_4_r.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/30THBAAXX_4_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/30THBAAXX_4_r.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/30THBAAXX_4_r.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/30THBAAXX_4_r.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.nfo
	$(eval 30THBAAXX_4_R_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(30THBAAXX_4_R_FASTQ_KMERC); 
	@if [ $(30THBAAXX_4_R_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS30THBAAXX_4_r_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/30THBAAXX_4_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/30THBAAXX_4_r.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/30THBAAXX_4_r.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_matepair_5000/30THBAAXX_4_r.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Heinz LIBRARY matepair_5000 RUN 30THBAAXX_4

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4_30THBAAXX_4_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING RUNS AND GROUPING THEM INTO LIBRARIES TO ORGANISM Heinz LIBRARY matepair_5000

# ######################################

# MERGING RUN DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.jf [2] INTO LIBRARY SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.histo.png
	@/bin/echo MERGING\ RUN\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.jf\ [2]\ INTO\ LIBRARY\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_3018DAAXX_4.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000_30THBAAXX_4.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.stats FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.stats\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.histo FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.histo\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.jf
	$(JELLY) histo --low=1 --high=$(LIBHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.histo.png FOR LIBRARY HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.histo.png\ FOR\ LIBRARY\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.histo
	export MKPLOTLINES=$(LIBGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_330 RUN 303UPAAXX_1 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_1_f.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_1_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_1_f.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_1_f.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_1_f.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.nfo
	$(eval 303UPAAXX_1_F_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(303UPAAXX_1_F_FASTQ_KMERC); 
	@if [ $(303UPAAXX_1_F_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS303UPAAXX_1_f_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_1_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_1_f.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_1_f.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_1_f.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_330 RUN 303UPAAXX_1 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_1_r.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_1_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_1_r.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_1_r.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_1_r.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.nfo
	$(eval 303UPAAXX_1_R_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(303UPAAXX_1_R_FASTQ_KMERC); 
	@if [ $(303UPAAXX_1_R_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS303UPAAXX_1_r_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_1_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_1_r.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_1_r.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_1_r.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Heinz LIBRARY pairedend_330 RUN 303UPAAXX_1

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1_303UPAAXX_1_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_330 RUN 303UPAAXX_2 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_2_f.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_2_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_2_f.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_2_f.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_2_f.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.nfo
	$(eval 303UPAAXX_2_F_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(303UPAAXX_2_F_FASTQ_KMERC); 
	@if [ $(303UPAAXX_2_F_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS303UPAAXX_2_f_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_2_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_2_f.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_2_f.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_2_f.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_330 RUN 303UPAAXX_2 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_2_r.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_2_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_2_r.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_2_r.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_2_r.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.nfo
	$(eval 303UPAAXX_2_R_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(303UPAAXX_2_R_FASTQ_KMERC); 
	@if [ $(303UPAAXX_2_R_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS303UPAAXX_2_r_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_2_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_2_r.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_2_r.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_2_r.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Heinz LIBRARY pairedend_330 RUN 303UPAAXX_2

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2_303UPAAXX_2_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_330 RUN 303UPAAXX_3 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_3_f.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_3_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_3_f.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_3_f.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_3_f.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.nfo
	$(eval 303UPAAXX_3_F_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(303UPAAXX_3_F_FASTQ_KMERC); 
	@if [ $(303UPAAXX_3_F_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS303UPAAXX_3_f_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_3_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_3_f.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_3_f.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_3_f.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_330 RUN 303UPAAXX_3 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_3_r.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_3_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_3_r.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_3_r.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_3_r.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.nfo
	$(eval 303UPAAXX_3_R_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(303UPAAXX_3_R_FASTQ_KMERC); 
	@if [ $(303UPAAXX_3_R_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS303UPAAXX_3_r_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_3_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_3_r.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_3_r.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_3_r.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Heinz LIBRARY pairedend_330 RUN 303UPAAXX_3

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3_303UPAAXX_3_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_330 RUN 303UPAAXX_4 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_4_f.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_4_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_4_f.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_4_f.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_4_f.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.nfo
	$(eval 303UPAAXX_4_F_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(303UPAAXX_4_F_FASTQ_KMERC); 
	@if [ $(303UPAAXX_4_F_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS303UPAAXX_4_f_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_4_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_4_f.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_4_f.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_4_f.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_330 RUN 303UPAAXX_4 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_4_r.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_4_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_4_r.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_4_r.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_4_r.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.nfo
	$(eval 303UPAAXX_4_R_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(303UPAAXX_4_R_FASTQ_KMERC); 
	@if [ $(303UPAAXX_4_R_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS303UPAAXX_4_r_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_4_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_4_r.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_4_r.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/303UPAAXX_4_r.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Heinz LIBRARY pairedend_330 RUN 303UPAAXX_4

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4_303UPAAXX_4_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_330 RUN 3050CAAXX_6 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_6_f.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_6_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_6_f.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_6_f.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_6_f.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.nfo
	$(eval 3050CAAXX_6_F_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(3050CAAXX_6_F_FASTQ_KMERC); 
	@if [ $(3050CAAXX_6_F_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS3050CAAXX_6_f_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_6_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_6_f.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_6_f.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_6_f.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_330 RUN 3050CAAXX_6 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_6_r.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_6_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_6_r.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_6_r.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_6_r.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.nfo
	$(eval 3050CAAXX_6_R_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(3050CAAXX_6_R_FASTQ_KMERC); 
	@if [ $(3050CAAXX_6_R_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS3050CAAXX_6_r_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_6_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_6_r.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_6_r.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_6_r.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Heinz LIBRARY pairedend_330 RUN 3050CAAXX_6

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6_3050CAAXX_6_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_330 RUN 3050CAAXX_7 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_7_f.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_7_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_7_f.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_7_f.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_7_f.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.nfo
	$(eval 3050CAAXX_7_F_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(3050CAAXX_7_F_FASTQ_KMERC); 
	@if [ $(3050CAAXX_7_F_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS3050CAAXX_7_f_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_7_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_7_f.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_7_f.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_7_f.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_330 RUN 3050CAAXX_7 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_7_r.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_7_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_7_r.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_7_r.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_7_r.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.nfo
	$(eval 3050CAAXX_7_R_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(3050CAAXX_7_R_FASTQ_KMERC); 
	@if [ $(3050CAAXX_7_R_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS3050CAAXX_7_r_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_7_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_7_r.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_7_r.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_7_r.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Heinz LIBRARY pairedend_330 RUN 3050CAAXX_7

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7_3050CAAXX_7_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_330 RUN 3050CAAXX_8 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_8_f.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_8_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_8_f.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_8_f.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_8_f.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.nfo
	$(eval 3050CAAXX_8_F_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(3050CAAXX_8_F_FASTQ_KMERC); 
	@if [ $(3050CAAXX_8_F_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS3050CAAXX_8_f_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_8_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_8_f.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_8_f.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_8_f.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_330 RUN 3050CAAXX_8 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_8_r.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_8_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_8_r.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_8_r.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_8_r.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.nfo
	$(eval 3050CAAXX_8_R_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(3050CAAXX_8_R_FASTQ_KMERC); 
	@if [ $(3050CAAXX_8_R_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS3050CAAXX_8_r_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_8_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_8_r.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_8_r.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/3050CAAXX_8_r.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Heinz LIBRARY pairedend_330 RUN 3050CAAXX_8

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8_3050CAAXX_8_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_330 RUN 426TUAAXX_2 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/426TUAAXX_2_f.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/426TUAAXX_2_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/426TUAAXX_2_f.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/426TUAAXX_2_f.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/426TUAAXX_2_f.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.nfo
	$(eval 426TUAAXX_2_F_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(426TUAAXX_2_F_FASTQ_KMERC); 
	@if [ $(426TUAAXX_2_F_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS426TUAAXX_2_f_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/426TUAAXX_2_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/426TUAAXX_2_f.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/426TUAAXX_2_f.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/426TUAAXX_2_f.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_330 RUN 426TUAAXX_2 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/426TUAAXX_2_r.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/426TUAAXX_2_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/426TUAAXX_2_r.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/426TUAAXX_2_r.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/426TUAAXX_2_r.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.nfo
	$(eval 426TUAAXX_2_R_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(426TUAAXX_2_R_FASTQ_KMERC); 
	@if [ $(426TUAAXX_2_R_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS426TUAAXX_2_r_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/426TUAAXX_2_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/426TUAAXX_2_r.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/426TUAAXX_2_r.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_330/426TUAAXX_2_r.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Heinz LIBRARY pairedend_330 RUN 426TUAAXX_2

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2_426TUAAXX_2_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING RUNS AND GROUPING THEM INTO LIBRARIES TO ORGANISM Heinz LIBRARY pairedend_330

# ######################################

# MERGING RUN DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.jf [8] INTO LIBRARY SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.histo.png
	@/bin/echo MERGING\ RUN\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.jf\ [8]\ INTO\ LIBRARY\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_1.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_2.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_3.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_303UPAAXX_4.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_6.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_7.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_3050CAAXX_8.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330_426TUAAXX_2.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.stats FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.stats\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.histo FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.histo\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.jf
	$(JELLY) histo --low=1 --high=$(LIBHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.histo.png FOR LIBRARY HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.histo.png\ FOR\ LIBRARY\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.histo
	export MKPLOTLINES=$(LIBGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_368 RUN 303UPAAXX_6 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_6_f.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_6_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_6_f.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_6_f.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_6_f.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.nfo
	$(eval 303UPAAXX_6_F_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(303UPAAXX_6_F_FASTQ_KMERC); 
	@if [ $(303UPAAXX_6_F_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS303UPAAXX_6_f_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_6_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_6_f.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_6_f.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_6_f.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_368 RUN 303UPAAXX_6 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_6_r.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_6_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_6_r.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_6_r.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_6_r.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.nfo
	$(eval 303UPAAXX_6_R_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(303UPAAXX_6_R_FASTQ_KMERC); 
	@if [ $(303UPAAXX_6_R_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS303UPAAXX_6_r_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_6_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_6_r.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_6_r.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_6_r.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Heinz LIBRARY pairedend_368 RUN 303UPAAXX_6

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6_303UPAAXX_6_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_368 RUN 303UPAAXX_7 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_7_f.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_7_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_7_f.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_7_f.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_7_f.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.nfo
	$(eval 303UPAAXX_7_F_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(303UPAAXX_7_F_FASTQ_KMERC); 
	@if [ $(303UPAAXX_7_F_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS303UPAAXX_7_f_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_7_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_7_f.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_7_f.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_7_f.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_368 RUN 303UPAAXX_7 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_7_r.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_7_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_7_r.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_7_r.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_7_r.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.nfo
	$(eval 303UPAAXX_7_R_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(303UPAAXX_7_R_FASTQ_KMERC); 
	@if [ $(303UPAAXX_7_R_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS303UPAAXX_7_r_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_7_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_7_r.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_7_r.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_7_r.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Heinz LIBRARY pairedend_368 RUN 303UPAAXX_7

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7_303UPAAXX_7_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_368 RUN 303UPAAXX_8 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_8_f.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_8_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_8_f.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_8_f.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_8_f.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.nfo
	$(eval 303UPAAXX_8_F_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(303UPAAXX_8_F_FASTQ_KMERC); 
	@if [ $(303UPAAXX_8_F_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS303UPAAXX_8_f_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_8_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_8_f.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_8_f.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_8_f.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_368 RUN 303UPAAXX_8 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_8_r.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_8_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_8_r.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_8_r.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_8_r.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.nfo
	$(eval 303UPAAXX_8_R_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(303UPAAXX_8_R_FASTQ_KMERC); 
	@if [ $(303UPAAXX_8_R_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS303UPAAXX_8_r_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_8_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_8_r.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_8_r.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/303UPAAXX_8_r.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Heinz LIBRARY pairedend_368 RUN 303UPAAXX_8

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8_303UPAAXX_8_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_368 RUN 3050CAAXX_3 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_3_f.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_3_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_3_f.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_3_f.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_3_f.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.nfo
	$(eval 3050CAAXX_3_F_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(3050CAAXX_3_F_FASTQ_KMERC); 
	@if [ $(3050CAAXX_3_F_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS3050CAAXX_3_f_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_3_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_3_f.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_3_f.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_3_f.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_368 RUN 3050CAAXX_3 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_3_r.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_3_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_3_r.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_3_r.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_3_r.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.nfo
	$(eval 3050CAAXX_3_R_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(3050CAAXX_3_R_FASTQ_KMERC); 
	@if [ $(3050CAAXX_3_R_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS3050CAAXX_3_r_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_3_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_3_r.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_3_r.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_3_r.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Heinz LIBRARY pairedend_368 RUN 3050CAAXX_3

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3_3050CAAXX_3_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_368 RUN 3050CAAXX_4 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_4_f.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_4_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_4_f.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_4_f.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_4_f.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.nfo
	$(eval 3050CAAXX_4_F_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(3050CAAXX_4_F_FASTQ_KMERC); 
	@if [ $(3050CAAXX_4_F_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS3050CAAXX_4_f_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_4_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_4_f.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_4_f.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_4_f.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_368 RUN 3050CAAXX_4 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_4_r.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_4_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_4_r.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_4_r.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_4_r.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.nfo
	$(eval 3050CAAXX_4_R_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(3050CAAXX_4_R_FASTQ_KMERC); 
	@if [ $(3050CAAXX_4_R_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS3050CAAXX_4_r_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_4_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_4_r.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_4_r.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/3050CAAXX_4_r.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Heinz LIBRARY pairedend_368 RUN 3050CAAXX_4

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4_3050CAAXX_4_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_368 RUN 426TUAAXX_1 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/426TUAAXX_1_f.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/426TUAAXX_1_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/426TUAAXX_1_f.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/426TUAAXX_1_f.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/426TUAAXX_1_f.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.nfo
	$(eval 426TUAAXX_1_F_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(426TUAAXX_1_F_FASTQ_KMERC); 
	@if [ $(426TUAAXX_1_F_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS426TUAAXX_1_f_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/426TUAAXX_1_f.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/426TUAAXX_1_f.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/426TUAAXX_1_f.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/426TUAAXX_1_f.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Heinz LIBRARY pairedend_368 RUN 426TUAAXX_1 FASTQ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/426TUAAXX_1_r.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/426TUAAXX_1_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.nfo : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/426TUAAXX_1_r.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/426TUAAXX_1_r.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/426TUAAXX_1_r.fastq -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.nfo
	$(eval 426TUAAXX_1_R_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(426TUAAXX_1_R_FASTQ_KMERC); 
	@if [ $(426TUAAXX_1_R_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS426TUAAXX_1_r_fastq) > /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/426TUAAXX_1_r.fastq
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/426TUAAXX_1_r.fastq /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/426TUAAXX_1_r.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq_mer_counts /home/aflit001/nobackup/Data/Heinz/Heinz_Illumina/Heinz_Illumina_pairedend_368/426TUAAXX_1_r.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Heinz LIBRARY pairedend_368 RUN 426TUAAXX_1

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_f.fastq.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1_426TUAAXX_1_r.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING RUNS AND GROUPING THEM INTO LIBRARIES TO ORGANISM Heinz LIBRARY pairedend_368

# ######################################

# MERGING RUN DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.jf [6] INTO LIBRARY SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.histo.png
	@/bin/echo MERGING\ RUN\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.jf\ [6]\ INTO\ LIBRARY\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_6.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_7.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_303UPAAXX_8.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_3.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_3050CAAXX_4.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368_426TUAAXX_1.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.stats FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.stats : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.stats\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.histo FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.jf
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.histo : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.histo\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.jf
	$(JELLY) histo --low=1 --high=$(LIBHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.histo.png FOR LIBRARY HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.histo
/home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.histo.png\ FOR\ LIBRARY\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.histo
	export MKPLOTLINES=$(LIBGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING LIBRARIES GROUPING THEM INTO A SINGLE ORGANISM Heinz

# ######################################

# MERGING LIBRARIES DATABASES /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.jf, /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.jf [6] INTO PARENT SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Heinz.jf
/home/aflit001/nobackup/Data_1_mer/Heinz.jf : /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.stats /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.histo /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.histo.png /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.stats /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.histo /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.histo.png
	@/bin/echo MERGING\ LIBRARIES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.jf,\ /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.jf\ [6]\ INTO\ PARENT\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Heinz.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Heinz.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_2000.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_3000.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_4000.jf /home/aflit001/nobackup/Data_1_mer/Heinz_matepair_5000.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_330.jf /home/aflit001/nobackup/Data_1_mer/Heinz_pairedend_368.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Heinz.stats FOR PARENT DB /home/aflit001/nobackup/Data_1_mer/Heinz.jf
/home/aflit001/nobackup/Data_1_mer/Heinz.stats : /home/aflit001/nobackup/Data_1_mer/Heinz.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Heinz.stats\ FOR\ PARENT\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Heinz.stats /home/aflit001/nobackup/Data_1_mer/Heinz.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz.histo FOR PARENT DB /home/aflit001/nobackup/Data_1_mer/Heinz.jf
/home/aflit001/nobackup/Data_1_mer/Heinz.histo : /home/aflit001/nobackup/Data_1_mer/Heinz.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz.histo\ FOR\ PARENT\ DB\ /home/aflit001/nobackup/Data_1_mer/Heinz.jf
	$(JELLY) histo --low=1 --high=$(LIBHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Heinz.histo /home/aflit001/nobackup/Data_1_mer/Heinz.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Heinz.histo.png FOR PARENT HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Heinz.histo
/home/aflit001/nobackup/Data_1_mer/Heinz.histo.png : /home/aflit001/nobackup/Data_1_mer/Heinz.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Heinz.histo.png\ FOR\ PARENT\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Heinz.histo
	export MKPLOTLINES=$(PARENTGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Heinz.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\

.PHONY : all

all : /home/aflit001/nobackup/Data_1_mer/Heinz.jf /home/aflit001/nobackup/Data_1_mer/Heinz.stats /home/aflit001/nobackup/Data_1_mer/Heinz.histo /home/aflit001/nobackup/Data_1_mer/Heinz.histo.png
	@/bin/echo GENERATING\ JELLY\ KMER\ COUNT\ AND\ MERGING
	@/bin/echo COMPLETED\ SUCCESSFULLY\ ALL