# Created by Panotools::Script 0.26


# ######################################

# CONSTANTS

# ######################################
.DEFAULT_GOAL = all
.DEFAULT_GOAL_SHELL = all
BASE = /home/aflit001/nobackup/Data
BASE_SHELL = /home/aflit001/nobackup/Data
OUTF = /home/aflit001/nobackup/Data_1_mer
OUTF_SHELL = /home/aflit001/nobackup/Data_1_mer
MKPLOT = /home/aflit001/nobackup/filter/mkplot
MKPLOT_SHELL = /home/aflit001/nobackup/filter/mkplot
FCOUNT = /home/aflit001/bin/fastqCount
FCOUNT_SHELL = /home/aflit001/bin/fastqCount
JELLY = /home/aflit001/bin/jellyfish
JELLY_SHELL = /home/aflit001/bin/jellyfish

where-am-i := $(CURDIR)/$(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST))
THIS_MAKEFILE := $(call where-am-i)

# ######################################

# VARIABLES

# ######################################
SHELL = /bin/bash
SHELL_SHELL = /bin/bash
THREADS = 16
THREADS_SHELL = 16
HASHSIZE = 800000000
HASHSIZE_SHELL = 800000000
MERLEN = 31
MERLEN_SHELL = 31
COUNTERLEN = 7
COUNTERLEN_SHELL = 7
JELLYDELETETEMPMER = 1
JELLYDELETETEMPMER_SHELL = 1
JELLYGRAPHLENGTH = 50
JELLYGRAPHLENGTH_SHELL = 50
JELLYHISTOHIGH = 49
JELLYHISTOHIGH_SHELL = 49
RUNDELETETEMPMER = 0
RUNDELETETEMPMER_SHELL = 0
RUNGRAPHLENGTH = 100
RUNGRAPHLENGTH_SHELL = 100
RUNHISTOHIGH = 99
RUNHISTOHIGH_SHELL = 99
LIBDELETETEMPMER = 0
LIBDELETETEMPMER_SHELL = 0
LIBGRAPHLENGTH = 100
LIBGRAPHLENGTH_SHELL = 100
LIBHISTOHIGH = 99
LIBHISTOHIGH_SHELL = 99
PARENTDELETETEMPMER = 1
PARENTDELETETEMPMER_SHELL = 1
PARENTGRAPHLENGTH = 200
PARENTGRAPHLENGTH_SHELL = 200
PARENTHISTOHIGH = 199
PARENTHISTOHIGH_SHELL = 199

# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM AllRound LIBRARY GOG17L2_WGS RUN 110126_SN132_B_s_2 FASTQ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L2_WGS/110126_SN132_B_s_2_1_seq_GOG-17.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L2_WGS/110126_SN132_B_s_2_1_seq_GOG-17.fastq
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.nfo : /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L2_WGS/110126_SN132_B_s_2_1_seq_GOG-17.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L2_WGS/110126_SN132_B_s_2_1_seq_GOG-17.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L2_WGS/110126_SN132_B_s_2_1_seq_GOG-17.fastq -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.nfo
	$(eval 110126_SN132_B_S_2_1_SEQ_GOG_17_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(110126_SN132_B_S_2_1_SEQ_GOG_17_FASTQ_KMERC); 
	@if [ $(110126_SN132_B_S_2_1_SEQ_GOG_17_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS110126_SN132_B_s_2_1_seq_GOG_17_fastq) > /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L2_WGS/110126_SN132_B_s_2_1_seq_GOG-17.fastq
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L2_WGS/110126_SN132_B_s_2_1_seq_GOG-17.fastq /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.nfo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L2_WGS/110126_SN132_B_s_2_1_seq_GOG-17.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq_mer_counts /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L2_WGS/110126_SN132_B_s_2_1_seq_GOG-17.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.jf : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.stats : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.histo : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.histo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM AllRound LIBRARY GOG17L2_WGS RUN 110126_SN132_B_s_2 FASTQ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L2_WGS/110126_SN132_B_s_2_2_seq_GOG-17.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L2_WGS/110126_SN132_B_s_2_2_seq_GOG-17.fastq
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.nfo : /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L2_WGS/110126_SN132_B_s_2_2_seq_GOG-17.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L2_WGS/110126_SN132_B_s_2_2_seq_GOG-17.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L2_WGS/110126_SN132_B_s_2_2_seq_GOG-17.fastq -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.nfo
	$(eval 110126_SN132_B_S_2_2_SEQ_GOG_17_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(110126_SN132_B_S_2_2_SEQ_GOG_17_FASTQ_KMERC); 
	@if [ $(110126_SN132_B_S_2_2_SEQ_GOG_17_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS110126_SN132_B_s_2_2_seq_GOG_17_fastq) > /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L2_WGS/110126_SN132_B_s_2_2_seq_GOG-17.fastq
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L2_WGS/110126_SN132_B_s_2_2_seq_GOG-17.fastq /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.nfo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L2_WGS/110126_SN132_B_s_2_2_seq_GOG-17.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq_mer_counts /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L2_WGS/110126_SN132_B_s_2_2_seq_GOG-17.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.jf : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.stats : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.histo : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.histo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM AllRound LIBRARY GOG17L2_WGS RUN 110126_SN132_B_s_2

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.jf, /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.jf : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.nfo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.nfo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_1_seq_GOG-17.fastq.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2_110126_SN132_B_s_2_2_seq_GOG-17.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.stats : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.histo : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.histo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.histo.png : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING RUNS AND GROUPING THEM INTO LIBRARIES TO ORGANISM AllRound LIBRARY GOG17L2_WGS

# ######################################

# MERGING RUN DATABASES /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.jf [1] INTO LIBRARY SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.jf : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.histo.png
	@/bin/echo MERGING\ RUN\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.jf\ [1]\ INTO\ LIBRARY\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS_110126_SN132_B_s_2.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.stats FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.stats : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.stats\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.histo FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.histo : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.histo\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.jf
	$(JELLY) histo --low=1 --high=$(LIBHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.histo.png FOR LIBRARY HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.histo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.histo.png : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.histo.png\ FOR\ LIBRARY\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.histo
	export MKPLOTLINES=$(LIBGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM AllRound LIBRARY GOG17L5_WGS RUN 110127_SN365_B_s_5 FASTQ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L5_WGS/110127_SN365_B_s_5_1_seq_GOG-17.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L5_WGS/110127_SN365_B_s_5_1_seq_GOG-17.fastq
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.nfo : /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L5_WGS/110127_SN365_B_s_5_1_seq_GOG-17.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L5_WGS/110127_SN365_B_s_5_1_seq_GOG-17.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L5_WGS/110127_SN365_B_s_5_1_seq_GOG-17.fastq -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.nfo
	$(eval 110127_SN365_B_S_5_1_SEQ_GOG_17_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(110127_SN365_B_S_5_1_SEQ_GOG_17_FASTQ_KMERC); 
	@if [ $(110127_SN365_B_S_5_1_SEQ_GOG_17_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS110127_SN365_B_s_5_1_seq_GOG_17_fastq) > /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L5_WGS/110127_SN365_B_s_5_1_seq_GOG-17.fastq
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L5_WGS/110127_SN365_B_s_5_1_seq_GOG-17.fastq /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.nfo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L5_WGS/110127_SN365_B_s_5_1_seq_GOG-17.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq_mer_counts /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L5_WGS/110127_SN365_B_s_5_1_seq_GOG-17.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.jf : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.stats : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.histo : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.histo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM AllRound LIBRARY GOG17L5_WGS RUN 110127_SN365_B_s_5 FASTQ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L5_WGS/110127_SN365_B_s_5_2_seq_GOG-17.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L5_WGS/110127_SN365_B_s_5_2_seq_GOG-17.fastq
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.nfo : /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L5_WGS/110127_SN365_B_s_5_2_seq_GOG-17.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L5_WGS/110127_SN365_B_s_5_2_seq_GOG-17.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L5_WGS/110127_SN365_B_s_5_2_seq_GOG-17.fastq -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.nfo
	$(eval 110127_SN365_B_S_5_2_SEQ_GOG_17_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(110127_SN365_B_S_5_2_SEQ_GOG_17_FASTQ_KMERC); 
	@if [ $(110127_SN365_B_S_5_2_SEQ_GOG_17_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS110127_SN365_B_s_5_2_seq_GOG_17_fastq) > /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L5_WGS/110127_SN365_B_s_5_2_seq_GOG-17.fastq
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L5_WGS/110127_SN365_B_s_5_2_seq_GOG-17.fastq /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.nfo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L5_WGS/110127_SN365_B_s_5_2_seq_GOG-17.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq_mer_counts /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L5_WGS/110127_SN365_B_s_5_2_seq_GOG-17.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.jf : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.stats : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.histo : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.histo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM AllRound LIBRARY GOG17L5_WGS RUN 110127_SN365_B_s_5

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.jf, /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.jf : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.nfo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.nfo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_1_seq_GOG-17.fastq.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5_110127_SN365_B_s_5_2_seq_GOG-17.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.stats : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.histo : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.histo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.histo.png : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING RUNS AND GROUPING THEM INTO LIBRARIES TO ORGANISM AllRound LIBRARY GOG17L5_WGS

# ######################################

# MERGING RUN DATABASES /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.jf [1] INTO LIBRARY SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.jf : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.histo.png
	@/bin/echo MERGING\ RUN\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.jf\ [1]\ INTO\ LIBRARY\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS_110127_SN365_B_s_5.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.stats FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.stats : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.stats\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.histo FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.histo : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.histo\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.jf
	$(JELLY) histo --low=1 --high=$(LIBHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.histo.png FOR LIBRARY HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.histo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.histo.png : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.histo.png\ FOR\ LIBRARY\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.histo
	export MKPLOTLINES=$(LIBGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM AllRound LIBRARY GOG17L6_WGS RUN 110127_SN365_B_s_6 FASTQ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L6_WGS/110127_SN365_B_s_6_1_seq_GOG-17.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L6_WGS/110127_SN365_B_s_6_1_seq_GOG-17.fastq
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.nfo : /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L6_WGS/110127_SN365_B_s_6_1_seq_GOG-17.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L6_WGS/110127_SN365_B_s_6_1_seq_GOG-17.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L6_WGS/110127_SN365_B_s_6_1_seq_GOG-17.fastq -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.nfo
	$(eval 110127_SN365_B_S_6_1_SEQ_GOG_17_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(110127_SN365_B_S_6_1_SEQ_GOG_17_FASTQ_KMERC); 
	@if [ $(110127_SN365_B_S_6_1_SEQ_GOG_17_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS110127_SN365_B_s_6_1_seq_GOG_17_fastq) > /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L6_WGS/110127_SN365_B_s_6_1_seq_GOG-17.fastq
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L6_WGS/110127_SN365_B_s_6_1_seq_GOG-17.fastq /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.nfo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L6_WGS/110127_SN365_B_s_6_1_seq_GOG-17.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq_mer_counts /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L6_WGS/110127_SN365_B_s_6_1_seq_GOG-17.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.jf : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.stats : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.histo : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.histo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM AllRound LIBRARY GOG17L6_WGS RUN 110127_SN365_B_s_6 FASTQ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L6_WGS/110127_SN365_B_s_6_2_seq_GOG-17.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L6_WGS/110127_SN365_B_s_6_2_seq_GOG-17.fastq
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.nfo : /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L6_WGS/110127_SN365_B_s_6_2_seq_GOG-17.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L6_WGS/110127_SN365_B_s_6_2_seq_GOG-17.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L6_WGS/110127_SN365_B_s_6_2_seq_GOG-17.fastq -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.nfo
	$(eval 110127_SN365_B_S_6_2_SEQ_GOG_17_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(110127_SN365_B_S_6_2_SEQ_GOG_17_FASTQ_KMERC); 
	@if [ $(110127_SN365_B_S_6_2_SEQ_GOG_17_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS110127_SN365_B_s_6_2_seq_GOG_17_fastq) > /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L6_WGS/110127_SN365_B_s_6_2_seq_GOG-17.fastq
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L6_WGS/110127_SN365_B_s_6_2_seq_GOG-17.fastq /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.nfo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L6_WGS/110127_SN365_B_s_6_2_seq_GOG-17.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq_mer_counts /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG17L6_WGS/110127_SN365_B_s_6_2_seq_GOG-17.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.jf : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.stats : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.histo : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.histo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM AllRound LIBRARY GOG17L6_WGS RUN 110127_SN365_B_s_6

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.jf, /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.jf : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.nfo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.nfo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_1_seq_GOG-17.fastq.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6_110127_SN365_B_s_6_2_seq_GOG-17.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.stats : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.histo : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.histo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.histo.png : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING RUNS AND GROUPING THEM INTO LIBRARIES TO ORGANISM AllRound LIBRARY GOG17L6_WGS

# ######################################

# MERGING RUN DATABASES /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.jf [1] INTO LIBRARY SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.jf : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.histo.png
	@/bin/echo MERGING\ RUN\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.jf\ [1]\ INTO\ LIBRARY\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS_110127_SN365_B_s_6.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.stats FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.stats : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.stats\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.histo FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.histo : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.histo\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.jf
	$(JELLY) histo --low=1 --high=$(LIBHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.histo.png FOR LIBRARY HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.histo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.histo.png : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.histo.png\ FOR\ LIBRARY\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.histo
	export MKPLOTLINES=$(LIBGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM AllRound LIBRARY GOG19_matepair_2000 RUN 110401_SN365_A_s_3 FASTQ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG19_matepair_2000/110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG19_matepair_2000/110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo : /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG19_matepair_2000/110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG19_matepair_2000/110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG19_matepair_2000/110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo
	$(eval 110401_SN365_A_S_3_1_SEQ_GOG_19_RD30_NOTEMPTY_NOTLINK_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(110401_SN365_A_S_3_1_SEQ_GOG_19_RD30_NOTEMPTY_NOTLINK_FASTQ_KMERC); 
	@if [ $(110401_SN365_A_S_3_1_SEQ_GOG_19_RD30_NOTEMPTY_NOTLINK_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS110401_SN365_A_s_3_1_seq_GOG_19_RD30_NotEmpty_NotLink_fastq) > /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG19_matepair_2000/110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG19_matepair_2000/110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG19_matepair_2000/110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq_mer_counts /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG19_matepair_2000/110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.stats : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM AllRound LIBRARY GOG19_matepair_2000 RUN 110401_SN365_A_s_3 FASTQ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG19_matepair_2000/110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG19_matepair_2000/110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo : /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG19_matepair_2000/110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG19_matepair_2000/110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG19_matepair_2000/110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo
	$(eval 110401_SN365_A_S_3_2_SEQ_GOG_19_RD30_NOTEMPTY_NOTLINK_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(110401_SN365_A_S_3_2_SEQ_GOG_19_RD30_NOTEMPTY_NOTLINK_FASTQ_KMERC); 
	@if [ $(110401_SN365_A_S_3_2_SEQ_GOG_19_RD30_NOTEMPTY_NOTLINK_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERS110401_SN365_A_s_3_2_seq_GOG_19_RD30_NotEmpty_NotLink_fastq) > /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG19_matepair_2000/110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG19_matepair_2000/110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG19_matepair_2000/110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq_mer_counts /home/aflit001/nobackup/Data/AllRound/AllRound_Illumina/AllRound_Illumina_GOG19_matepair_2000/110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.stats : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM AllRound LIBRARY GOG19_matepair_2000 RUN 110401_SN365_A_s_3

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf, /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.jf : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_1_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3_110401_SN365_A_s_3_2_seq_GOG-19.RD30.NotEmpty.NotLink.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.stats : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.histo : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.histo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.histo.png : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING RUNS AND GROUPING THEM INTO LIBRARIES TO ORGANISM AllRound LIBRARY GOG19_matepair_2000

# ######################################

# MERGING RUN DATABASES /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.jf [1] INTO LIBRARY SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.jf : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.histo.png
	@/bin/echo MERGING\ RUN\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.jf\ [1]\ INTO\ LIBRARY\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000_110401_SN365_A_s_3.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.stats FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.stats : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.stats\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.histo FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.jf
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.histo : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.histo\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.jf
	$(JELLY) histo --low=1 --high=$(LIBHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.histo.png FOR LIBRARY HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.histo
/home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.histo.png : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.histo.png\ FOR\ LIBRARY\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.histo
	export MKPLOTLINES=$(LIBGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING LIBRARIES GROUPING THEM INTO A SINGLE ORGANISM AllRound

# ######################################

# MERGING LIBRARIES DATABASES /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.jf, /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.jf, /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.jf, /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.jf [4] INTO PARENT SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/AllRound.jf
/home/aflit001/nobackup/Data_1_mer/AllRound.jf : /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.histo.png /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.histo.png /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.histo.png /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.stats /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.histo /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.histo.png
	@/bin/echo MERGING\ LIBRARIES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.jf,\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.jf,\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.jf,\ /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.jf\ [4]\ INTO\ PARENT\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/AllRound.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/AllRound.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L2_WGS.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L5_WGS.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG17L6_WGS.jf /home/aflit001/nobackup/Data_1_mer/AllRound_GOG19_matepair_2000.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/AllRound.stats FOR PARENT DB /home/aflit001/nobackup/Data_1_mer/AllRound.jf
/home/aflit001/nobackup/Data_1_mer/AllRound.stats : /home/aflit001/nobackup/Data_1_mer/AllRound.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/AllRound.stats\ FOR\ PARENT\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/AllRound.stats /home/aflit001/nobackup/Data_1_mer/AllRound.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound.histo FOR PARENT DB /home/aflit001/nobackup/Data_1_mer/AllRound.jf
/home/aflit001/nobackup/Data_1_mer/AllRound.histo : /home/aflit001/nobackup/Data_1_mer/AllRound.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound.histo\ FOR\ PARENT\ DB\ /home/aflit001/nobackup/Data_1_mer/AllRound.jf
	$(JELLY) histo --low=1 --high=$(LIBHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/AllRound.histo /home/aflit001/nobackup/Data_1_mer/AllRound.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/AllRound.histo.png FOR PARENT HISTOGRAM /home/aflit001/nobackup/Data_1_mer/AllRound.histo
/home/aflit001/nobackup/Data_1_mer/AllRound.histo.png : /home/aflit001/nobackup/Data_1_mer/AllRound.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/AllRound.histo.png\ FOR\ PARENT\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/AllRound.histo
	export MKPLOTLINES=$(PARENTGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/AllRound.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\

.PHONY : all

all : /home/aflit001/nobackup/Data_1_mer/AllRound.jf /home/aflit001/nobackup/Data_1_mer/AllRound.stats /home/aflit001/nobackup/Data_1_mer/AllRound.histo /home/aflit001/nobackup/Data_1_mer/AllRound.histo.png
	@/bin/echo GENERATING\ JELLY\ KMER\ COUNT\ AND\ MERGING
	@/bin/echo COMPLETED\ SUCCESSFULLY\ ALL