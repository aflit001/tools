# Created by Panotools::Script 0.26


# ######################################

# CONSTANTS

# ######################################
.DEFAULT_GOAL = all
.DEFAULT_GOAL_SHELL = all
BASE = /home/aflit001/nobackup/Data
BASE_SHELL = /home/aflit001/nobackup/Data
OUTF = /home/aflit001/nobackup/Data_1_mer
OUTF_SHELL = /home/aflit001/nobackup/Data_1_mer
MKPLOT = /home/aflit001/nobackup/filter/mkplot
MKPLOT_SHELL = /home/aflit001/nobackup/filter/mkplot
FCOUNT = /home/aflit001/bin/fastqCount
FCOUNT_SHELL = /home/aflit001/bin/fastqCount
JELLY = /home/aflit001/bin/jellyfish
JELLY_SHELL = /home/aflit001/bin/jellyfish

where-am-i := $(CURDIR)/$(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST))
THIS_MAKEFILE := $(call where-am-i)

# ######################################

# VARIABLES

# ######################################
SHELL = /bin/bash
SHELL_SHELL = /bin/bash
THREADS = 16
THREADS_SHELL = 16
HASHSIZE = 800000000
HASHSIZE_SHELL = 800000000
MERLEN = 31
MERLEN_SHELL = 31
COUNTERLEN = 7
COUNTERLEN_SHELL = 7
JELLYDELETETEMPMER = 1
JELLYDELETETEMPMER_SHELL = 1
JELLYGRAPHLENGTH = 50
JELLYGRAPHLENGTH_SHELL = 50
JELLYHISTOHIGH = 49
JELLYHISTOHIGH_SHELL = 49
RUNDELETETEMPMER = 0
RUNDELETETEMPMER_SHELL = 0
RUNGRAPHLENGTH = 100
RUNGRAPHLENGTH_SHELL = 100
RUNHISTOHIGH = 99
RUNHISTOHIGH_SHELL = 99
LIBDELETETEMPMER = 0
LIBDELETETEMPMER_SHELL = 0
LIBGRAPHLENGTH = 100
LIBGRAPHLENGTH_SHELL = 100
LIBHISTOHIGH = 99
LIBHISTOHIGH_SHELL = 99
PARENTDELETETEMPMER = 1
PARENTDELETETEMPMER_SHELL = 1
PARENTGRAPHLENGTH = 200
PARENTGRAPHLENGTH_SHELL = 200
PARENTHISTOHIGH = 199
PARENTHISTOHIGH_SHELL = 199

# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Sativa LIBRARY pairedend_250 RUN Medicinalgenomics.com_Prep7_1-1 FASTQ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.nfo : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.nfo
	$(eval MEDICINALGENOMICS_COM_PREP7_1_1_1_SEQUENCE_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(MEDICINALGENOMICS_COM_PREP7_1_1_1_SEQUENCE_FASTQ_KMERC); 
	@if [ $(MEDICINALGENOMICS_COM_PREP7_1_1_1_SEQUENCE_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSMedicinalgenomics_com_Prep7_1_1_1_sequence_fastq) > /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq_mer_counts /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Sativa LIBRARY pairedend_250 RUN Medicinalgenomics.com_Prep7_1-1 FASTQ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.nfo : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.nfo
	$(eval MEDICINALGENOMICS_COM_PREP7_1_1_2_SEQUENCE_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(MEDICINALGENOMICS_COM_PREP7_1_1_2_SEQUENCE_FASTQ_KMERC); 
	@if [ $(MEDICINALGENOMICS_COM_PREP7_1_1_2_SEQUENCE_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSMedicinalgenomics_com_Prep7_1_1_2_sequence_fastq) > /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq_mer_counts /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Sativa LIBRARY pairedend_250 RUN Medicinalgenomics.com_Prep7_1-1

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.jf : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_1_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1_Medicinalgenomics.com_Prep7_1-1_2_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.stats : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.histo : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.histo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.histo.png : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Sativa LIBRARY pairedend_250 RUN Medicinalgenomics.com_Prep7_1-2 FASTQ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.nfo : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.nfo
	$(eval MEDICINALGENOMICS_COM_PREP7_1_2_1_SEQUENCE_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(MEDICINALGENOMICS_COM_PREP7_1_2_1_SEQUENCE_FASTQ_KMERC); 
	@if [ $(MEDICINALGENOMICS_COM_PREP7_1_2_1_SEQUENCE_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSMedicinalgenomics_com_Prep7_1_2_1_sequence_fastq) > /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq_mer_counts /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Sativa LIBRARY pairedend_250 RUN Medicinalgenomics.com_Prep7_1-2 FASTQ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.nfo : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.nfo
	$(eval MEDICINALGENOMICS_COM_PREP7_1_2_2_SEQUENCE_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(MEDICINALGENOMICS_COM_PREP7_1_2_2_SEQUENCE_FASTQ_KMERC); 
	@if [ $(MEDICINALGENOMICS_COM_PREP7_1_2_2_SEQUENCE_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSMedicinalgenomics_com_Prep7_1_2_2_sequence_fastq) > /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq_mer_counts /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Sativa LIBRARY pairedend_250 RUN Medicinalgenomics.com_Prep7_1-2

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.jf : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_1_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2_Medicinalgenomics.com_Prep7_1-2_2_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.stats : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.histo : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.histo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.histo.png : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Sativa LIBRARY pairedend_250 RUN Medicinalgenomics.com_Prep7_1-3 FASTQ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.nfo : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.nfo
	$(eval MEDICINALGENOMICS_COM_PREP7_1_3_1_SEQUENCE_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(MEDICINALGENOMICS_COM_PREP7_1_3_1_SEQUENCE_FASTQ_KMERC); 
	@if [ $(MEDICINALGENOMICS_COM_PREP7_1_3_1_SEQUENCE_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSMedicinalgenomics_com_Prep7_1_3_1_sequence_fastq) > /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq_mer_counts /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Sativa LIBRARY pairedend_250 RUN Medicinalgenomics.com_Prep7_1-3 FASTQ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.nfo : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.nfo
	$(eval MEDICINALGENOMICS_COM_PREP7_1_3_2_SEQUENCE_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(MEDICINALGENOMICS_COM_PREP7_1_3_2_SEQUENCE_FASTQ_KMERC); 
	@if [ $(MEDICINALGENOMICS_COM_PREP7_1_3_2_SEQUENCE_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSMedicinalgenomics_com_Prep7_1_3_2_sequence_fastq) > /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq_mer_counts /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Sativa LIBRARY pairedend_250 RUN Medicinalgenomics.com_Prep7_1-3

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.jf : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_1_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3_Medicinalgenomics.com_Prep7_1-3_2_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.stats : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.histo : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.histo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.histo.png : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Sativa LIBRARY pairedend_250 RUN Medicinalgenomics.com_Prep7_1-5 FASTQ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.nfo : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.nfo
	$(eval MEDICINALGENOMICS_COM_PREP7_1_5_1_SEQUENCE_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(MEDICINALGENOMICS_COM_PREP7_1_5_1_SEQUENCE_FASTQ_KMERC); 
	@if [ $(MEDICINALGENOMICS_COM_PREP7_1_5_1_SEQUENCE_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSMedicinalgenomics_com_Prep7_1_5_1_sequence_fastq) > /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq_mer_counts /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Sativa LIBRARY pairedend_250 RUN Medicinalgenomics.com_Prep7_1-5 FASTQ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.nfo : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.nfo
	$(eval MEDICINALGENOMICS_COM_PREP7_1_5_2_SEQUENCE_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(MEDICINALGENOMICS_COM_PREP7_1_5_2_SEQUENCE_FASTQ_KMERC); 
	@if [ $(MEDICINALGENOMICS_COM_PREP7_1_5_2_SEQUENCE_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSMedicinalgenomics_com_Prep7_1_5_2_sequence_fastq) > /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq_mer_counts /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Sativa LIBRARY pairedend_250 RUN Medicinalgenomics.com_Prep7_1-5

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.jf : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_1_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5_Medicinalgenomics.com_Prep7_1-5_2_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.stats : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.histo : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.histo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.histo.png : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Sativa LIBRARY pairedend_250 RUN Medicinalgenomics.com_Prep7_1-6 FASTQ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.nfo : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.nfo
	$(eval MEDICINALGENOMICS_COM_PREP7_1_6_1_SEQUENCE_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(MEDICINALGENOMICS_COM_PREP7_1_6_1_SEQUENCE_FASTQ_KMERC); 
	@if [ $(MEDICINALGENOMICS_COM_PREP7_1_6_1_SEQUENCE_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSMedicinalgenomics_com_Prep7_1_6_1_sequence_fastq) > /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq_mer_counts /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Sativa LIBRARY pairedend_250 RUN Medicinalgenomics.com_Prep7_1-6 FASTQ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.nfo : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.nfo
	$(eval MEDICINALGENOMICS_COM_PREP7_1_6_2_SEQUENCE_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(MEDICINALGENOMICS_COM_PREP7_1_6_2_SEQUENCE_FASTQ_KMERC); 
	@if [ $(MEDICINALGENOMICS_COM_PREP7_1_6_2_SEQUENCE_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSMedicinalgenomics_com_Prep7_1_6_2_sequence_fastq) > /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq_mer_counts /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Sativa LIBRARY pairedend_250 RUN Medicinalgenomics.com_Prep7_1-6

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.jf : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_1_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6_Medicinalgenomics.com_Prep7_1-6_2_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.stats : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.histo : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.histo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.histo.png : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Sativa LIBRARY pairedend_250 RUN Medicinalgenomics.com_Prep7_1-7 FASTQ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.nfo : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.nfo
	$(eval MEDICINALGENOMICS_COM_PREP7_1_7_1_SEQUENCE_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(MEDICINALGENOMICS_COM_PREP7_1_7_1_SEQUENCE_FASTQ_KMERC); 
	@if [ $(MEDICINALGENOMICS_COM_PREP7_1_7_1_SEQUENCE_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSMedicinalgenomics_com_Prep7_1_7_1_sequence_fastq) > /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq_mer_counts /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Sativa LIBRARY pairedend_250 RUN Medicinalgenomics.com_Prep7_1-7 FASTQ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.nfo : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.nfo
	$(eval MEDICINALGENOMICS_COM_PREP7_1_7_2_SEQUENCE_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(MEDICINALGENOMICS_COM_PREP7_1_7_2_SEQUENCE_FASTQ_KMERC); 
	@if [ $(MEDICINALGENOMICS_COM_PREP7_1_7_2_SEQUENCE_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSMedicinalgenomics_com_Prep7_1_7_2_sequence_fastq) > /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq_mer_counts /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Sativa LIBRARY pairedend_250 RUN Medicinalgenomics.com_Prep7_1-7

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.jf : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_1_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7_Medicinalgenomics.com_Prep7_1-7_2_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.stats : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.histo : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.histo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.histo.png : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Sativa LIBRARY pairedend_250 RUN Medicinalgenomics.com_Prep7_1-8 FASTQ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.nfo : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.nfo
	$(eval MEDICINALGENOMICS_COM_PREP7_1_8_1_SEQUENCE_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(MEDICINALGENOMICS_COM_PREP7_1_8_1_SEQUENCE_FASTQ_KMERC); 
	@if [ $(MEDICINALGENOMICS_COM_PREP7_1_8_1_SEQUENCE_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSMedicinalgenomics_com_Prep7_1_8_1_sequence_fastq) > /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq_mer_counts /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTQ FILES FOR ORGANISM Sativa LIBRARY pairedend_250 RUN Medicinalgenomics.com_Prep7_1-8 FASTQ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq

# ######################################

# MANUALLY COUNTING KMER FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.nfo : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq
	@/bin/echo MANUALLY\ COUNTING\ KMER\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq\ TO\  $@
	$(FCOUNT) -i /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.nfo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# STORING AND CHECKING TOTAL NUMBER OF KMERS ON NFO FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.nfo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.nfo.KMER : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.nfo
	@/bin/echo STORING\ AND\ CHECKING\ TOTAL\ NUMBER\ OF\ KMERS\ ON\ NFO\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.nfo
	$(eval MEDICINALGENOMICS_COM_PREP7_1_8_2_SEQUENCE_FASTQ_KMERC = $(shell cat "/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.nfo" | perl -ne 'if (/KMER:(\d+)/) { print $$1 }'))
	@/bin/echo KAMERS $(MEDICINALGENOMICS_COM_PREP7_1_8_2_SEQUENCE_FASTQ_KMERC); 
	@if [ $(MEDICINALGENOMICS_COM_PREP7_1_8_2_SEQUENCE_FASTQ_KMERC) == "0" ]; then \
	/bin/echo NO KMER; \
	else \
	/bin/echo HAS KMER; \
	/bin/echo I AM $(THIS_MAKEFILE) ; \
	/bin/echo $(KMERSMedicinalgenomics_com_Prep7_1_8_2_sequence_fastq) > /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.nfo.KMER;\
	fi;
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ADDING JELLYFISH KMER COUNT FOR FASTQ FILE /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq_mer_counts_0 : /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.nfo.KMER
	@/bin/echo ADDING\ JELLYFISH\ KMER\ COUNT\ FOR\ FASTQ\ FILE\ /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq
	$(JELLY) count --mer-len=$(MERLEN) --threads=$(THREADS) --counter-len=$(COUNTERLEN) --size=$(HASHSIZE) --both-strands -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq_mer_counts /home/aflit001/nobackup/Data/Sativa/Sativa_Illumina/Sativa_Illumina_pairedend_250/Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# MERGING JELLYFISH KMER COUNT FOR /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq_mer_counts_0 INTO SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.jf : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq_mer_counts_0
	@/bin/echo MERGING\ JELLYFISH\ KMER\ COUNT\ FOR\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq_mer_counts_0\ INTO\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq_mer_counts_*
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.stats FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.stats : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.stats\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.histo FOR SINGLE FILE DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.histo : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.histo\ FOR\ SINGLE\ FILE\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.jf
	$(JELLY) histo --low=1 --high=$(JELLYHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.histo.png FOR HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.histo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.histo.png : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.histo.png\ FOR\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.histo
	export MKPLOTLINES=$(JELLYGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING INDIVIDUAL FASTAQ FILES DBS INTO RUNS, GROUPING TWO BY TWO TO ORGANISM Sativa LIBRARY pairedend_250 RUN Medicinalgenomics.com_Prep7_1-8

# ######################################

# MERGING SINGLE FILES DATABASES /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.jf, /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.jf [2] INTO RUN's SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.jf : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.histo.png /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.nfo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.nfo.KMER /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq_mer_counts_0 /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.histo.png
	@/bin/echo MERGING\ SINGLE\ FILES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.jf,\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.jf\ [2]\ INTO\ RUN\'s\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_1_sequence.fastq.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8_Medicinalgenomics.com_Prep7_1-8_2_sequence.fastq.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.stats FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.stats : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.stats\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.histo FOR RUN DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.histo : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.histo\ FOR\ RUN\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.jf
	$(JELLY) histo --low=1 --high=$(RUNHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.histo.png FOR RUN HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.histo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.histo.png : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.histo.png\ FOR\ RUN\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.histo
	export MKPLOTLINES=$(RUNGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING RUNS AND GROUPING THEM INTO LIBRARIES TO ORGANISM Sativa LIBRARY pairedend_250

# ######################################

# MERGING RUN DATABASES /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.jf, /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.jf, /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.jf, /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.jf, /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.jf, /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.jf, /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.jf [7] INTO LIBRARY SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.jf : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.histo.png /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.histo.png /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.histo.png /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.histo.png /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.histo.png /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.histo.png /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.histo.png
	@/bin/echo MERGING\ RUN\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.jf,\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.jf,\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.jf,\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.jf,\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.jf,\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.jf,\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.jf\ [7]\ INTO\ LIBRARY\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-1.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-2.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-3.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-5.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-6.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-7.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250_Medicinalgenomics.com_Prep7_1-8.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.stats FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.stats : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.stats\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.histo FOR LIBRARY DB /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.jf
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.histo : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.histo\ FOR\ LIBRARY\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.jf
	$(JELLY) histo --low=1 --high=$(LIBHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.histo.png FOR LIBRARY HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.histo
/home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.histo.png : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.histo.png\ FOR\ LIBRARY\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.histo
	export MKPLOTLINES=$(LIBGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# ######################################

# RUNNING LIBRARIES GROUPING THEM INTO A SINGLE ORGANISM Sativa

# ######################################

# MERGING LIBRARIES DATABASES /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.jf [1] INTO PARENT SINGLE DATABASE FILE /home/aflit001/nobackup/Data_1_mer/Sativa.jf
/home/aflit001/nobackup/Data_1_mer/Sativa.jf : /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.stats /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.histo /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.histo.png
	@/bin/echo MERGING\ LIBRARIES\ DATABASES\ /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.jf\ [1]\ INTO\ PARENT\ SINGLE\ DATABASE\ FILE\ /home/aflit001/nobackup/Data_1_mer/Sativa.jf
	$(JELLY) merge --buffer-size=$(HASHSIZE) -o /home/aflit001/nobackup/Data_1_mer/Sativa.jf /home/aflit001/nobackup/Data_1_mer/Sativa_pairedend_250.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING STATISTICS /home/aflit001/nobackup/Data_1_mer/Sativa.stats FOR PARENT DB /home/aflit001/nobackup/Data_1_mer/Sativa.jf
/home/aflit001/nobackup/Data_1_mer/Sativa.stats : /home/aflit001/nobackup/Data_1_mer/Sativa.jf
	@/bin/echo GENERATING\ STATISTICS\ /home/aflit001/nobackup/Data_1_mer/Sativa.stats\ FOR\ PARENT\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa.jf
	$(JELLY) stats -o /home/aflit001/nobackup/Data_1_mer/Sativa.stats /home/aflit001/nobackup/Data_1_mer/Sativa.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa.histo FOR PARENT DB /home/aflit001/nobackup/Data_1_mer/Sativa.jf
/home/aflit001/nobackup/Data_1_mer/Sativa.histo : /home/aflit001/nobackup/Data_1_mer/Sativa.jf
	@/bin/echo GENERATING\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa.histo\ FOR\ PARENT\ DB\ /home/aflit001/nobackup/Data_1_mer/Sativa.jf
	$(JELLY) histo --low=1 --high=$(LIBHISTOHIGH) --increment=1 --buffer-size=$(HASHSIZE) --threads=$(THREADS) -o /home/aflit001/nobackup/Data_1_mer/Sativa.histo /home/aflit001/nobackup/Data_1_mer/Sativa.jf
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\


# GENERATING HISTOGRAM GRAPHIC /home/aflit001/nobackup/Data_1_mer/Sativa.histo.png FOR PARENT HISTOGRAM /home/aflit001/nobackup/Data_1_mer/Sativa.histo
/home/aflit001/nobackup/Data_1_mer/Sativa.histo.png : /home/aflit001/nobackup/Data_1_mer/Sativa.histo
	@/bin/echo GENERATING\ HISTOGRAM\ GRAPHIC\ /home/aflit001/nobackup/Data_1_mer/Sativa.histo.png\ FOR\ PARENT\ HISTOGRAM\ /home/aflit001/nobackup/Data_1_mer/Sativa.histo
	export MKPLOTLINES=$(PARENTGRAPHLENGTH)
	$(MKPLOT) /home/aflit001/nobackup/Data_1_mer/Sativa.histo
	@if [[ $$? != 0 ]]; then  \
	echo FAILED. REASON: $$? ;\
	exit $$?; \
	else \
	echo FINISHED SUCCESSFULLY;\
	fi;\
	if [[ ! -f "$@" ]]; then /bin/echo "OUTPUT FILE \"$@\" NOT CREATED. QUITTING";\
	exit 1;\
	fi;\

.PHONY : all

all : /home/aflit001/nobackup/Data_1_mer/Sativa.jf /home/aflit001/nobackup/Data_1_mer/Sativa.stats /home/aflit001/nobackup/Data_1_mer/Sativa.histo /home/aflit001/nobackup/Data_1_mer/Sativa.histo.png
	@/bin/echo GENERATING\ JELLY\ KMER\ COUNT\ AND\ MERGING
	@/bin/echo COMPLETED\ SUCCESSFULLY\ ALL