#!/usr/bin/python
import sys

#sed -i -e 's/|\| /_/g' data/Solanum_lycopersicum_chloroplast.fasta

def getTitle (line):
    line = line.lstrip("#")
    titleA = line.split("\t")
    titleD = {}
    for i in range(len(titleA)):
        val         = titleA[i]
        titleD[val] = i
    
    return titleA, titleD

def parseLine (line):
    cols = line.split("\t")
    return cols

def printLine (titleA, titleD, line):
    for c in range(len(line)):
        val   = line[c]
        title = titleA[c]
        print "%s=%s |" % (title.upper(), val),
    print ""

def printData (titleA, titleD, data):
    for line in data:
        printLine(titleA, titleD, line)





def fixZpos (posi):
    #print posi
    #posi = []
    #posi.
    for item in posi:
        #print str(item)
        for k in item.keys():
            v = item[k]
            #print v
            #v = ""
            if v.isdigit:
                return int(v)+1
            else:
                print "ERROR. TRYING TO FIX Z POSITION '"+v+"' BUT IT IS NOT A NUMBER"
                sys.exit(1)

def genEmptyRes ():
    res = []
    for i in range(len(formater)):
        res.append('')
    return res

def genGffFormater ():
    formater = {}
    for key in correspondence:
        corrVal = correspondence.get(key)
        name = corrVal[0]
        formater[name] = corrVal
    return formater

def genGffPlacer ():
    placer = {}
    for key in correspondence:
        if key == "": pass
        val          = correspondence.get(key)[0]
        placer[key] = val
    return placer

def getGffFields (fields):
    res = genEmptyRes()
    
    for key in formater.keys():
        vals   = fields.get(key)
        format = formater[key]
        pos    = format[1]
        method = format[2]

        #print "KEY "+key+" METHOD "+method+" POS "+str(pos)
        #print key,
        #print vals
        
        if   method == 'value':
            #print "  ADDING VALUE '"+str(vals)+"' TO KEY '"+key+"' AT POS '"+str(pos)+"'"
            for valDic in vals:
                for valKey in valDic:
                    valVal = valDic[valKey]
                    res[pos] = str(valVal)
                    #print "    ADDING SINGLE VALUE:"+str(valVal)
        elif method == 'default':
            #print "  ADDING VALUE '"+format[3]+"' TO KEY '"+key+"' AT POS '"+str(pos)+"'"
            res[pos] = str(format[3])
        elif method == 'function':
            #print "  ADDING VALUE '"+str(vals)+"' TO KEY '"+key+"' AT POS '"+str(pos)+"'"
            function = format[3]
            value    = function(vals)
            #print "    ADDING SINGLE VALUE:"+str(value)
            res[pos] = str(value)
        elif method == 'merge':
            #print "  ADDING VALUE '"+str(vals)+"' TO KEY '"+key+"' AT POS '"+str(pos)+"'"
            values = []
            for valDic in vals:
                #print "    DICT VALLUE:"+str(valDic)
                for valKey in valDic:
                    valVal = valDic[valKey]
                    #print "      VALUE KEY:"+valKey+" VALUE VALUE:"+str(valVal)
                    value = valKey+"='"+valVal+"'"
                    #print "        VALUE:"+value
                    values.append(value)
            #print "  VALUES"+str(values)
            valueStr = ";".join(values)
            #print "  VALUE STR:"+valueStr
            res[pos] = str(valueStr)
            #print "    ADDING SINGLE VALUE:"+valueStr
    #print "  INPUT:"+str(fields)
    #print "  OUTPUT:"+str(res)+"\n\n\n"
    return res

def placeGffFields (fields, value, name):
    if name in placer.keys():
        corrKey = placer.get(name)
        #print "    PLACE GFF FIELDS: NAME="+name+" VALUE="+value+" CORRECT KEY="+corrKey
        if corrKey in fields:
            fields[corrKey].append({name: value})
        else:
            fields[corrKey] = [{name: value}]
    else:
        #print "    PLACE GFF FIELDS: NAME="+name+" VALUE="+value+" CORRECT KEY=META"
        if "group" in fields:
            fields["group"].append({name: value})
        else:
            fields["group"] = [{name: value}]


def printGffLine (titleA, titleD, line):
    fields = {}

    for c in range(len(line)):
        val   = line[c]
        title = titleA[c]
        placeGffFields(fields, val, title)
    
    gffFields = getGffFields(fields)
    print "\t".join(gffFields)

def printGff (titleA, titleD, data):
    for line in data:
        printGffLine(titleA, titleD, line)



lineCount = 0
titleA    = []
titleD    = {}
data      = []

correspondence  =   {   "name1":    ["seqname", 0, "value"],
                        "name2":    ["feature", 1, "value"],
                        "zstart1":  ["start",   2, "function", fixZpos],
                        "end1":     ["end",     3, "function", fixZpos],
                        "score":    ["score",   4, "value"],
                        "strand2":  ["strand",  5, "value"],
                        "frame":    ["frame",   6, "default", "."],
                        "META":     ["group",   7, "merge"]
                    }

formater = genGffFormater()
placer   = genGffPlacer()


for line in sys.stdin:
    #print "  LINE "+line
    line = line.rstrip("\n")
    if lineCount == 0:
        print "  TITLE "+line
        titleA, titleD = getTitle(line)
    else:
        #print "  LINE "+line
        cols = parseLine(line)
        data.append(cols)
    lineCount += 1
        
#printData(titleA, titleD, data)
printGff(titleA, titleD, data)



    

#GFF
#http://genome.ucsc.edu/FAQ/FAQformat.html#format3
#http://gmod.org/wiki/GFF2#The_GFF2_File_Format
    #GFF (General Feature Format) lines are based on the GFF standard file format. GFF lines have nine required fields that must be tab-separated. If the fields are separated by spaces instead of tabs, the track will not display correctly. For more information on GFF format, refer to http://www.sanger.ac.uk/resources/software/gff.
    #
    #Here is a brief description of the GFF fields:
    #
    #    seqname - The name of the sequence. Must be a chromosome or scaffold.
    #    source - The program that generated this feature.
    #    feature - The name of this type of feature. Some examples of standard feature types are "CDS", "start_codon", "stop_codon", and "exon".
    #    start - The starting position of the feature in the sequence. The first base is numbered 1.
    #    end - The ending position of the feature (inclusive).
    #    score - A score between 0 and 1000. If the track line useScore attribute is set to 1 for this annotation data set, the score value will determine the level of gray in which this feature is displayed (higher numbers = darker gray). If there is no score value, enter ".".
    #    strand - Valid entries include '+', '-', or '.' (for don't know/don't care).
    #    frame - If the feature is a coding exon, frame should be a number between 0-2 that represents the reading frame of the first base. If the feature is not a coding exon, the value should be '.'.
    #    group - All lines with the same group are linked together into a single item. 
    #
    #Example:
    #Here's an example of a GFF-based track. Click here for a copy of this example that can be pasted into the browser without editing. NOTE: Paste operations on some operating systems will replace tabs with spaces, which will result in an error when the GFF track is uploaded. You can circumvent this problem by pasting the URL of the above example (http://genome.ucsc.edu/goldenPath/help/regulatory.txt) instead of the text itselfinto the custom annotation track text box.
    #
    #track name=regulatory description="TeleGene(tm) Regulatory Regions"
    #chr22  TeleGene enhancer  1000000  1001000  500 +  .  touch1
    #chr22  TeleGene promoter  1010000  1010100  900 +  .  touch1
    #chr22  TeleGene promoter  1020000  1020000  800 -  .  touch2

#GFF
#http://www.synbrowse.org/pub_docs/GFF.txt
    #Description of the GFF file (version 2)
    #
    #            (Adapted from GBrowse documentation written by Lincoln Stein)
    #
    #
    #
    #A. The GFF 2 file format
    #
    #The SynBrowse, like GBrowse is based around the GFF file format, which stands 
    #for "Gene Finding Format" and was invented at the Sanger Centre
    #(http://www.sanger.ac.uk/Software/formats/GFF/). The GFF format is a flat 
    #tab-delimited file, each line of which corresponds to an annotation, or 
    #feature. Each line has nine columns and looks like this:
    #
    #1	curated	CDS	20715643	20715694	.	+	.	mRNA At1g55475.1 ; Note "expressed protein"
    #
    #  1. reference sequence
    #
    #  This is the ID of the sequence that is used to establish the coordinate 
    #  system of the annotation.  In the example above, the reference sequence 
    #  is 1 (chromosome 1).
    #
    #  2. source
    #
    #  The source of the annotation.  This field describes how the annotation 
    #  was derived.  In the example above, the source is "curated" to indicate 
    #  that the feature is curated feature. The names and versions of software 
    #  programs are often used for the source field.
    #
    #  3. method
    #
    #  The annotation method.  This field describes the type of the annotation, 
    #  such as "CDS".  Together the method and source describe the annotation 
    #  type.
    #
    #  4. start position
    #
    #  The start of the annotation relative to the reference sequence.
    #
    #  5. stop position
    #
    #  The stop of the annotation relative to the reference sequence.
    #  Start is always less than or equal to stop.
    #
    #  6. score
    #
    #  For annotations that are associated with a numeric score (for
    #  example, a sequence similarity), this field describes the score.
    #  The score units are completely unspecified, but for sequence
    #  similarities, it is typically percent identity.  Annotations that
    #  don't have a score can use "."
    #
    #  7. strand
    #
    #  For those annotations which are strand-specific, this field is the
    #  strand on which the annotation resides.  It is "+" for the forward
    #  strand, "-" for the reverse strand, or "." for annotations that are
    #  not stranded.
    #
    #  8. phase
    #
    #  For annotations that are linked to proteins, this field describes
    #  the phase of the annotation on the codons.  It is a number from 0 to
    #  2, or "." for features that have no phase.
    #
    #  9. group
    #
    #  GFF provides a simple way of generating annotation hierarchies ("is
    #  composed of" relationships) by providing a group field.  The group
    #  field contains the class and ID of an annotation which is the
    #  logical parent of the current one.  
    #
    #  The group field is also used to store information about the target
    #  of sequence similarity hits, and miscellaneous notes.  
    #
    #The sequences used to establish the coordinate system for annotations
    #can correspond to sequenced clones, clone fragments, contigs or
    #super-contigs.
    #
    #In addition to a group ID, the GFF format allows annotations to have a
    #group class.  This makes sure that all groups are unique even if they
    #happen to share the same name.  For example, you can have a GenBank
    #accession named AP001234 and a clone named AP001234 and distinguish
    #between them by giving the first one a class of Accession and the
    #second a class of Clone.
    #
    #You should use double-quotes around the group name or class if it
    #contains white space.
    #
    #
    #B. Sequence alignments
    #
    #There are several cases in which an annotation indicates the
    #relationship between two sequences. A common one is a similarity
    #hit, where the annotation indicates an alignment.  A second common
    #case is a map assembly, in which the annotation indicates that a
    #portion of a larger sequence is built up from one or more smaller
    #ones. Both cases are indicated by using the Target tag in the group 
    #field.
    #
    #For the sequence alignments for SynBrowse, there are additional 
    #requirements for the source and method columns. In each similarity 
    #hit record, we specify the entry in the method column as "similarity"
    #and the entry in the source column as "gene" or "coding" (for 
    #entirely conserved genes or single exons, respectively) appended 
    #with a similarity or identity percentage in deciles for protein 
    #alignments and as "align" or "conserved" (for gapped alignements 
    #or gap-free alignments, respectively) appended with a similarity 
    #or identity percentage in deciles for nucleotide alignments. The 
    #value of similarity or identity is shown in the score column. 
    # 
    #For example in SynBrowse requirement, a typical similarity hit 
    #will look like this:
    #
    #AC123572	coding50	similarity	18950	19093	0.597	+	.	Target "Sequence:1" 25440931 25441074
    #
    #Here, the source is "coding50" to indicate that the feature is an alignment 
    #of coding region which have approximately 50% identity percentage. This value
    #can be calculated from the score "0.597" by timing 100 and then taking the 
    #decile value. The method is similarity. The group field contains the Target 
    #tag, followed by an identifier for the biological object. The GFF format uses 
    #the notation Class:Name for the biological object, and even though this is 
    #stylistically inconsistent, that's the way it's done.  The object identifier 
    #is followed by two integers indicating the start and stop of the alignment 
    #on the target sequence. Note that each item in the group field be separated 
    #by a white space.
    #
    #The previous example indicates that the the section of AC123572 from 18,950 to 
    #19,093 aligns to Arabidopsis Chromosome 1 starting at position 25440931 and 
    #extending to position 25441074 with forward strand. 
    #
    #Unlike the main start and stop columns for the standard GFF described in A,
    #for a sequence alignment, it is possible for both the main and target start 
    #to be greater than the end. 
    #
    #
    #C. Loading the GFF file into the database
    #
    #Use the BioPerl script utilities bp_bulk_load_gff.pl, bp_load_gff.pl or 
    #bp_fast_load_gff.pl to load the GFF file into the database. For example, if 
    #your database is a MySQL database on the local host named "dicty", you can 
    #load it into an empty database using bp_bulk_load_gff.pl like this:
    #
    #  bp_bulk_load_gff.pl -c -d dicty my_data.gff
    #
    #To update existing databases, use either bp_load_gff.pl or bp_fast_load_gff.pl. 
    #The latter is somewhat experimental, so use with care.


#LASTZ
    #target[[start..end]]   spec/file containing target sequence (fasta, nib, 2bit
    #                       or hsx);  [start..end] defines a subrange of the file
    #                       (use --help=files for more details)
    #query[[start..end]]    spec/file containing query sequences;  if absent,
    #                       queries come from stdin (if needed)
    #--self                 the target sequence is also the query
    #--seed=match<length>   use a word with no gaps instead of a seed pattern
    #--seed=half<length>    use space-free half-weight word instead of seed pattern
    #--[no]transition[=2]   allow one or two transitions in a seed hit
    #                       (by default a transition is allowed)
    #--word=<bits>          set max bits for word hash;  use this to trade time for
    #                       memory, eliminating thrashing for heavy seeds
    #                       (default is 28 bits)
    #--filter=<T>,<M>       filter seed hits, requiring at least M matches and
    #                       allowing no more than T transversions
    #                       (default is no filtering)
    #--notwins              require just one seed hit
    #--twins=<min>..<maxgap> require two nearby seed hits on the same diagonal
    #                       (default is twins aren't required)
    #--seedqueue=<entries>  set number of entries in seed hit queue
    #                       (default is 262144)
    #--segments=<file>      read anchor segments from a file, instead of
    #                       discovering anchors via seeding
    #--norecoverseeds       don't recover hash-collision seed hits
    #--recoverseeds         recover hash-collision seed hits
    #                       (default is not to recover seed hits)
    #--step=<length>        set step length (default is 1)
    #--strand=both          search both strands
    #--strand=plus          search + strand only (matching strand of query spec)
    #--strand=minus         search - strand only (opposite strand of query spec)
    #                       (by default both strands are searched)
    #--ambiguous=n          treat N as an ambiguous nucleotide
    #                       (by default N is treated as a sequence splicing
    #                        character)
    #--ambiguous=iupac      treat any ambiguous IUPAC-IUB character as a
    #                       completely ambiguous nucleotide
    #                       (by default any sequence file with B,D,H,K,M,R,S,V,W,Y
    #                        is rejected)
    #--[no]gfextend         perform gap-free extension of seed hits to HSPs
    #                       (by default extension is performed)
    #--[no]chain            perform chaining
    #--chain=<diag,anti>    perform chaining with given penalties for diagonal and
    #                       anti-diagonal
    #                       (by default no chaining is performed)
    #--[no]gapped           perform gapped alignment (instead of gap-free)
    #                       (by default gapped alignment is performed)
    #--notrivial            do not output a trivial self-alignment block if the
    #                       target and query happen to be identical
    #--scores=<file>        read substitution scores from a file
    #                       (default is HOXD70)
    #--match=<R>,<P>        scores are +R/-P for match/mismatch
    #--gap=<open,extend>    set gap open and extend penalties (default is 400,30)
    #--xdrop=<score>        set x-drop threshold (default is 10*sub[A][A])
    #--ydrop=<score>        set y-drop threshold (default is open+300extend)
    #--noxtrim              if x-drop extension encounters end of sequence, don't
    #                       trim back to peak score (use this for short reads)
    #--noytrim              if y-drop extension encounters end of sequence, don't
    #                       trim back to peak score (use this for short reads)
    #--infer=<control>      infer scores from the sequences, then use them
    #--inferonly=<control>  infer scores but don't use them (requires --infscores)
    #                       all inference options are read from the control file
    #--infscores[=<file>]   write inferred scores to a file
    #--hspthresh=<score>    set threshold for high scoring pairs (default is 3000)
    #                       ungapped extensions scoring lower are discarded
    #                       <score> can also be a percentage or base count
    #--exact=<length>       set threshold for exact matches
    #                       if specified, exact matches are found rather than high
    #                       scoring pairs (replaces --hspthresh)
    #--mismatch=<N>,<length> set threshold for mismatches
    #                       if specified, N-mismatch segments are found rather
    #                       than high scoring pairs (replaces --hspthresh)
    #--inner=<score>        set threshold for HSPs during interpolation
    #                       (default is no interpolation)
    #--gappedthresh=<score> set threshold for gapped alignments
    #                       gapped extensions scoring lower are discarded
    #                       <score> can also be a percentage or base count
    #                       (default is to use same value as --hspthresh)
    #--ball=<score>[%]      set minimum score required of words 'in' a quantum ball
    #--[no]entropy          involve entropy in filtering high scoring pairs
    #                       (default is "entropy")
    #--nomirror             don't report mirror-image alignments when using --self
    #                       (default is to skip processing them, but recreate them
    #                       in the output)
    #--traceback=<bytes>    space for trace-back information
    #                       (default is 80.0M)
    #--maxwordcount=<limit>[%] limit seed word-repeats in target
    #                       words occurring too often are not used in seed hits
    #                       (default is no word-repeat limit)
    #--masking=<count>      mask any position in target hit this many times
    #                       zero indicates no masking
    #                       (default is no masking)
    #--[no]census[=<file>]  count/report how many times each target base aligns
    #                       (default is to not report census)
    #--identity=<min>[..<max>] filter alignments by percent identity
    #                       0<=min<=max<=100;  blocks (or HSPs) outside min..max
    #                       are discarded
    #                       (default is no identity filtering)
    #--coverage=<min>[..<max>] filter alignments by percentage of query covered
    #                       0<=min<=max<=100;  blocks (or HSPs) outside min..max
    #                       are discarded
    #                       (default is no query coverage filtering)
    #--continuity=<min>[..<max>] filter alignments by percent continuity
    #                       0<=min<=max<=100;  blocks (or HSPs) outside min..max
    #                       are discarded
    #                       (default is no continuity filtering)
    #--matchcount=<min> filter alignments by match-count
    #                       0<min;  blocks (or HSPs) with fewer than min matched
    #                       bases are discarded
    #                       (default is no match-count filtering)
    #--output=<file>        specify output alignment file;  otherwise alignments
    #                       are written to stdout
    #--format=<type>        specify output format; one of lav, axt, maf, cigar,
    #                       rdotplot, text or general
    #                       (use --help=formats for more details)
    #                       (by default output is LAV)
    #--markend              Write a comment at the end of the output file
    #--rdotplot=<file>      create an output file suitable for plotting in R.
    #--verbosity=<level>    set info level (0 is minimum, 10 is everything)
    #                       (default is 0)
    #--[no]runtime          report runtime in the output file
    #                       (default is to not report runtime)
    #--tableonly[=count]    just produce the target position table, don't
    #                       search for seeds
    #--writesegments=<file> just produce the anchor segments table, don't
    #                       perform gapped alignment
    #--writecapsule=<file>  write the target and seed word table to a file
    #--targetcapsule=<file> read the target seed word table from a file
    #                       (this replaces the target specifier)
    #--progress=<n>         report processing of every nth query
    #--version              report the program version and quit
    #--help                 list all options
    #--help=files           list information about file specifiers
    #--help=formats         list information about output file formats
    #--help=shortcuts       list blastz-compatible shortcuts
    #--help=yasra           list yasra-specific shortcuts
  
  
    #score Score of the alignment block. The scale and meaning of this number will vary, depending
    #       on the final stage performed and other command-line options.
    #name1 Name of the target sequence.
    #strand1 Target sequence strand, either "+" or "?".
    #size1 Size of the entire target sequence.
    #start1 Starting position of the alignment block in the target, origin-one.
    #zstart1 Starting position of the alignment block in the target, origin-zero.
    #end1 Ending position of the alignment block in the target, expressed either as origin-one
    #       closed or origin-zero half-open (the ending value is the same in both systems).
    #length1 Length of the alignment block in the target (excluding gaps).
    #text1 Aligned characters in the target, including gap characters. align1 can be used as a
    #       synonym for text1.
    #name2 Name of the query sequence.
    #strand2 Query sequence strand, either "+" or "?".
    #size2 Size of the entire query sequence.
    #start2 Starting position of the alignment block in the query, origin-one.
    #zstart2 Starting position of the alignment block in the query, origin-zero.
    #end2 Ending position of the alignment block in the query, expressed either as origin-one
    #       closed or origin-zero half-open (the ending value is the same in both systems).
    #start2+ Starting position of the alignment block in the query, counting along the query
    #       sequence's positive strand (regardless of which query strand was aligned), origin-one. Note
    #       that if strand2 is "?", then this is the other end of the block from start2.
    #zstart2+ Starting position of the alignment block in the query, counting along the query
    #       sequence's positive strand (regardless of which query strand was aligned), origin-zero. Note
    #       that if strand2 is "?", then this is the other end of the block from zstart2.
    #end2+ Ending position of the alignment block in the query, counting along the query
    #       sequence's positive strand (regardless of which query strand was aligned), expressed either
    #       as origin-one closed or origin-zero half-open (the ending value is the same in both systems).
    #       Note that if strand2 is "?", then this is the other end of the block from end2.
    #length2 Length of the alignment block in the query (excluding gaps).
    #text2 Aligned characters in the query, including gap characters. align2 can be used as a
    #       synonym for text2.
    #nmatch Number of aligned bases in the block that are matches.
    #nmismatch Number of aligned bases in the block that are mismatches (substitutions).
    #ngap Number of gaps in the block, counting each run of gapped columns as a single gap.
    #cgap Number of gaps in the block, counting each gapped column as a separate gap.
    #diff Differences between what would be written for text1 and text2. Matches are written as .
    #(period), transitions as : (colon), transversions as X, and gaps as - (hyphen).
    #cigar
    #A CIGAR-like representation of the alignment's path through the DP matrix. This is the short
    #representation, without spaces, described in the Ensembl CIGAR specification.
    #
    #For more information, see the section about CIGAR and its example.
    #cigarx
    #Same as cigar, but uses a newer syntax that distinguishes matches from substitutions and
    #omits the run length when it is 1.
    #
    #For more information, see the section about CIGAR and its example.
    #identity Fraction of aligned bases in the block that are matches (see Identity). This is
    #written as two fields. The first field is a fraction, written as <n>/<d>. The second field
    #contains the same value, computed as a percentage.
    #continuity Rate of non-gaps (non-indels) in the alignment block (see Continuity). This is
    #written as two fields. The first field is a fraction, written as <n>/<d>. The second field
    #contains the same value, computed as a percentage.
    #coverage Fraction of the entire input sequence (target or query, whichever is shorter) that
    #is covered by the alignment block (see Coverage). This is written as two fields. The first
    #field is a fraction, written as <n>/<d>. The second field contains the same value, computed
    #as a percentage.
    #diagonal The diagonal of the start of the alignment block in the DP matrix, expressed as an
    #identifying number start1-start2.
    #shingle A measurement of the shingle overlap between the target and the query. This is
    #intended for the case where both the target and query are relatively short, and their ends
    #are expected to overlap.

    #LAV
    #    LAV format is the format that blastz produced, and is the default.  It
    #    reports alignment blocks grouped by 'contig' and strand, and describes the
    #    blocks by listing the coordinates of ungapped segments.  It does not display
    #    the nucleotides.  For more deatils see the lastz readme file.
    #
    #    The option --format=lav+text adds a textual display of each alignment
    #    block, intermixed with the lav format.  Such files are unlikely to be
    #    recognized by any lav-reading program.
    #
    #AXT
    #    AXT format is a pairwise alignment format.  As of Jan/2009, a spec for AXT
    #    files can be found at
    #        genome.ucsc.edu/goldenPath/help/axt.html
    #
    #    The option --format=axt+ displays additional statistics with each block,
    #    in the form of comments.  The exact content of these comment lines may
    #    change in future releases of lastz.
    #
    #MAF
    #    MAF format is a multiple alignment format.  As of Jan/2009, a spec for MAF
    #    files can be found at
    #        genome.ucsc.edu/FAQ/FAQformat#format5
    #    The MAF files produced by lastz have exactly two sequences per block.  The
    #    first sequence always comes from the target sequence file, the second from
    #    the query.
    #
    #    The option --format=maf+ displays additional statistics with each block,
    #    in the form of comments.  The exact content of these comment lines may
    #    change in future releases of lastz.
    #
    #    The option --format=maf- inhibits the maf header and any comments.  This
    #    makes it suitable for catenating output from multiple runs.
    #
    #SAM
    #    SAM format is a pairwise alignment format used primarily for short-read
    #    mapping.  It is imperative that the query sequence(s) be short reads.  By
    #    default "hard clipping" is used when alignments don't reach the end of a
    #    query (see the SAM spec for what that means).  The option --format=softsam
    #    will use "soft clipping" instead.  As of Oct/2009, a spec for SAM files
    #    can be found at
    #        samtools.sourceforge.net/SAM1.pdf
    #
    #    The option --format=sam- inhibits the sam header lines.  This makes it
    #    suitable for catenating output from multiple runs.
    #
    #CIGAR
    #    CIGAR format is a pairwise alignment format that describes alignment blocks
    #    in a run-length format.  As of Jan/2009, a spec for CIGAR files can be
    #    found at
    #        may2005.archive.ensembl.org/Docs/wiki/html/EnsemblDocs/CigarFormat.html
    #
    #segments
    #    Output anchor segments, for reprocessing with --segments=<file>.
    #
    #rdotplot
    #    R output creates a file that can be plotted in the statistical package R.
    #    After creating the file like this:
    #        lastz ... --format=rdotplot > rdots.dat
    #    ask R to plot it using an R command like this:
    #        plot(read.table("rdots.dat",header=T),type="l")
    #    The separate option --rdotplot=<file> can be used to create a dot plot file
    #    at the same time as creating alignment output in another format.
    #
    #text
    #    Textual output is intended to be human readable.  Each alignment block is
    #    displayed with gap characters and a row of match/transition characters.
    #    Lines are wrapped at some reasonable width to allow printing to paper.
    #    The exact format of textual output may change in future releases of lastz.
    #
    #general
    #    General output creates a tab-delimited table with one line per alignment
    #    block.  The user can specify which fields are written (and in what order).
    #    This format is well-suited for use with spreadsheets and the R statistical
    #    package.
    #
    #    The format of the general output option is one of these:
    #        --format=general
    #        --format=general:fields
    #    where fields is a comma-separated list of field names.  If this list is
    #    absent all fields are printed.  The recognized field names are shown below.
    #    See the lastz readme file for more details.
    #
    #    The option --format=general- (with or without fields) inhibits the header
    #    lines.  This makes it suitable for catenating output from multiple runs.
    #        name1, strand1, size1, start1, zstart1, end1, length1, align1, text1,
    #        name2, strand2, size2, start2, zstart2, start2+, zstart2+, end2, end2+,
    #        length2, align2, text2, nmatch, nmismatch, ngap, cgap, diff, cigar,
    #        cigar-, cigarx, cigarx-, diagonal, shingle, score, identity, coverage,
    #        continuity, gaprate
    #
    #The option --markend can be useful in cases (such as batch servers) in which
    #there may be a question as to whether or not lastz completed successfully.  The
    #line "# lastz end-of-file" is written to output as the last line.  Note that
    #in some formats this is *not* a legal line;  the user must remove it before any
    #downstream processsing.


    #aflit001@dev1:~/cyrille2$ ./lastz --strand=both --format=general --rdotplot=plot.dat Solanum_lycopersicum_chloroplast.fasta Solanum_tuberisum_chloroplast.fasta | column | tee res.tab && echo 'plot(read.table("plot.dat",header=T),type="l");quit()' | R --no-save
    ##score	name1	strand1	size1	zstart1	end1	name2	strand2	size2	zstart2	end2	identity	idPct	coverage	covPct
    #14342141	gi	+	155461	2	155442	gi	+	155296	27	155296	153730/154643	99.4%	155269/155296	100.0%
    #28791	gi	+	155461	38248	40222	gi	+	155296	40604	42566	1141/1852	61.6%	1962/155296	1.3%
    #28579	gi	+	155461	40463	42425	gi	+	155296	38389	40363	1137/1854	61.3%	1974/155296	1.3%
    #4217	gi	+	155461	44491	44540	gi	+	155296	121521	121570	46/49	93.9%	49/155296	0.0%
    #3598	gi	+	155461	44501	44540	gi	+	155296	99923	99962	38/39	97.4%	39/155296	0.0%
    #3625	gi	+	155461	100059	100113	gi	+	155296	121518	121570	45/52	86.5%	52/155296	0.0%
    #3598	gi	+	155461	100074	100113	gi	+	155296	44646	44685	38/39	97.4%	39/155296	0.0%
    #3439	gi	+	155461	121679	121730	gi	+	155296	83226	83277	43/51	84.3%	51/155296	0.0%
    #4003	gi	+	155461	121681	121730	gi	+	155296	44636	44685	45/49	91.8%	49/155296	0.0%
    #3780	gi	+	155461	121689	121730	gi	+	155296	99921	99962	40/41	97.6%	41/155296	0.0%
    #5182	gi	+	155461	30146	30226	gi	-	155296	124922	125002	66/80	82.5%	80/155296	0.1%
    #3598	gi	+	155461	44501	44540	gi	-	155296	14186	14225	38/39	97.4%	39/155296	0.0%
    #3494	gi	+	155461	47572	47644	gi	-	155296	101697	101769	50/72	69.4%	72/155296	0.0%
    #3494	gi	+	155461	53680	53752	gi	-	155296	107521	107593	50/72	69.4%	72/155296	0.0%
    #3582	gi	+	155461	58199	58280	gi	-	155296	97152	97243	63/81	77.8%	91/155296	0.1%
    #8933	gi	+	155461	64963	65058	gi	-	155296	90374	90469	95/95	100.0%	95/155296	0.1%
    #4584	gi	+	155461	76204	76252	gi	-	155296	79207	79255	48/48	100.0%	48/155296	0.0%
    #5058	gi	+	155461	78626	78680	gi	-	155296	76768	76822	54/54	100.0%	54/155296	0.0%
    #2408674	gi	+	155461	85901	111505	gi	-	155296	0	25608	25499/25537	99.9%	25608/155296	16.5%
    #3780	gi	+	155461	121689	121730	gi	-	155296	14184	14225	40/41	97.6%	41/155296	0.0%
    #4579	gi	+	155461	124028	124105	gi	-	155296	105650	105734	69/77	89.6%	84/155296	0.1%
    #2410127	gi	+	155461	129854	155461	gi	-	155296	43970	69578	25506/25540	99.9%	25608/155296	16.5%
    #3598	gi	+	155461	141230	141269	gi	-	155296	110611	110650	38/39	97.4%	39/155296	0.0%
    #3625	gi	+	155461	141230	141284	gi	-	155296	33726	33778	45/52	86.5%	52/155296	0.0%