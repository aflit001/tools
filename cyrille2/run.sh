IN1=data/Solanum_lycopersicum_chloroplast.fasta
IN2=data/Solanum_tuberisum_chloroplast.fasta
DAT=res/plot.dat
TAB=res/res.tab

rm $DAT   2>/dev/null
rm $RES    2>/dev/null
rm res/Rplots.pdf 2>/dev/null

CMD="./lastz --strand=both --format=general --rdotplot=$DAT $IN1 $IN2 | column > $TAB && echo 'plot(read.table(\"$DAT\",header=T),type=\"l\");quit()' | R --no-save >/dev/null; cat $TAB"
echo $CMD
eval $CMD

mv Rplots.pdf res/ 2>/dev/null
