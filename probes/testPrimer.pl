#!/usr/bin/perl -w
use strict;
use File::Basename;

my $replace    = 1;
my $maxProduct = 5_000;

my $envscript = "make_setup.sh";
die "NO ENVIRNMENT SCRIPT $envscript FOUND" if ! -f $envscript;
#system "source $envscript";
if ( ! defined $ENV{'SOURCED_IT'} || $ENV{'SOURCED_IT'} != 1 )
{
    print "NOT SOURCED\n";

    my $selfCmd = "/bin/bash -c 'SOURCED_IT=1; export SOURCED_IT; source $envscript;$0 @ARGV 2>&1' |";
    #print $selfCmd, "\n";

    $|++;
    open(SELF, $selfCmd) or die "Can't make graphic: $!";
    while (<SELF>){ print };
    close SELF;

    print "finished self\n";

    exit 0;
    #exec("SOURCED_IT=1; export SOURCED_IT; ./$envscript; $0 @ARGV");
} else {
    print "SOURCED\n";
}

my $name         = shift @ARGV;
my $p1           = shift @ARGV;
my $p2           = shift @ARGV;

die   "NO NAME DEFINED"       if ! defined $name;
die   "NO PRIMER 1 DEFINED"   if ! defined $p1;
die   "NO PRIMER 2 DEFINED"   if ! defined $p2;
die   "NOT ENOUGH ARGUMENTS. NEEDS. NAME PRIMER1 PRIMER2" if (( ! defined $name ) || ( ! defined $p1 ) || ( ! defined $p2 ));
die   "P1 NOT A DNA SEQUENCE" if $p1 !~ /^[A|C|G|T]+$/;
die   "P2 NOT A DNA SEQUENCE" if $p2 !~ /^[A|C|G|T]+$/;
print "NAME          : $name\n";
print "PRIMER 1      : $p1\n";
print "PRIMER 2      : $p2\n";




my $outFolder    = $ENV{'PROBEFOLDER'};
my $inTabFolder  = $ENV{'TABFOLDER'};
my $dataFolder   = $ENV{'INFOLDER'};
my $baseFolder   = $ENV{'OUTFOLDER'};
my $dbFolder     = $ENV{'EBWTFOLDER'};
my $agpFolder    = $ENV{'AGPFOLDER'};
my $nFolder      = $ENV{'NFOLDER'};

die "OUT  FOLDER NOT DEFINED" if ! defined $outFolder   || $outFolder   eq '';
die "IN   FOLDER NOT DEFINED" if ! defined $inTabFolder || $inTabFolder eq '';
die "DATA FOLDER NOT DEFINED" if ! defined $dataFolder  || $dataFolder  eq '';
die "BASE FOLDER NOT DEFINED" if ! defined $baseFolder  || $baseFolder  eq '';
die "AGP  FOLDER NOT DEFINED" if ! defined $agpFolder   || $agpFolder   eq '';
die "DB   FOLDER NOT DEFINED" if ! defined $dbFolder    || $dbFolder    eq '';
die "Ns   FOLDER NOT DEFINED" if ! defined $nFolder     || $nFolder     eq '';

die "IN   FOLDER '$inTabFolder' DOES NOT EXISTS" if ! -d $inTabFolder;
die "DATA FOLDER '$dataFolder'  DOES NOT EXISTS" if ! -d $dataFolder;
die "BASE FOLDER '$baseFolder'  DOES NOT EXISTS" if ! -d $baseFolder;
die "AGP  FOLDER '$agpFolder'   DOES NOT EXISTS" if ! -d $agpFolder;
if ( ! -d $dbFolder ) {
    print "DB   FOLDER '$dbFolder'    DOES NOT EXISTS. CREATING";
    mkdir($dbFolder) or die "$!";
}
die "Ns   FOLDER '$dbFolder'    DOES NOT EXISTS" if ! -d $nFolder;

print "OUT FOLDER    : $outFolder\n";
print "IN TAB FOLDER : $inTabFolder\n";
print "IN FOLDER     : $dataFolder\n";
print "BASE FOLDER   : $baseFolder\n";
print "EBWT FOLDER   : $dbFolder\n";
print "AGP FOLDER    : $agpFolder\n";
print "Ns FOLDER     : $nFolder\n";



die "BOWTIE DB NAME  NOT DEFINED" if ! defined $ENV{'DBNAME'}      || $ENV{'DBNAME'}      eq '';
die "CONTIG MAP      NOT DEFINED" if ! defined $ENV{'CONTIGMAP'}   || $ENV{'CONTIGMAP'}   eq '';
die "MAP AGP         NOT DEFINED" if ! defined $ENV{'MAPAGP'}      || $ENV{'MAPAGP'}      eq '';
die "BOWTIE DB FASTA NOT DEFINED" if ! defined $ENV{'DBFASTAFILE'} || $ENV{'DBFASTAFILE'} eq '';
die "INFASTA ENV     NOT DEFINED" if ! defined $ENV{'INFASTA'}     || $ENV{'INFASTA'}     eq '';
die "DATA AGP        NOT DEFINED" if ! defined $ENV{'MAPAGP'}      || $ENV{'MAPAGP'}      eq '';


my $inFasta      = $ENV{'INFASTA'};
my $dbFastaFile  = $ENV{'DBFASTAFILE'};
my $contig       = $ENV{'CONTIGMAP'};
my $dataAgp      = $ENV{'MAPAGP'};
my $dbName       = $ENV{'DBNAME'};
my $noneTabFile  = "$dbFastaFile\_NONE.tab";
my $baseFile     = "$inFasta\_$dbName.sam";
my $dbNameFull   = "$dbFolder/$dbName";



print "IN FASTA      : $inFasta\n";
print "DB FASTA FILE : $dbFastaFile\n";
print "CONTIG        : $contig\n";
print "DATA AGP      : $dataAgp\n";
print "DB NAME       : $dbName\n";
print "NONE TAB FILE : $noneTabFile\n";
print "BASE FILE     : $baseFile\n";
print "DB NAME FULL  : $dbNameFull\n";




die "INFASTA  FILE '$dataFolder/$inFasta'     DOES NOT EXISTS" if ! -f "$dataFolder/$inFasta";
die "DB FASTA FILE '$dataFolder/$dbFastaFile' DOES NOT EXISTS" if ! -f "$dataFolder/$dbFastaFile";
die "CONTIG   FILE '$dataFolder/$contig'      DOES NOT EXISTS" if ! -f "$dataFolder/$contig";
die "DATA     FILE '$dataFolder/$dataAgp'     DOES NOT EXISTS" if ! -f "$dataFolder/$dataAgp";
die "NONE TAB FILE '$nFolder/$noneTabFile'    DOES NOT EXISTS" if ! -f "$nFolder/$noneTabFile";
die "BASE     FILE '$baseFolder/$baseFile'    DOES NOT EXISTS" if ! -f "$baseFolder/$baseFile";
die "DB       FILE '$dbNameFull.1.ebwt'       DOES NOT EXISTS" if ! -f "$dbNameFull.1.ebwt";


if ( ! -d $outFolder ) {
    mkdir($outFolder);
}


my $cmd0= "bowtie-build --offrate $dataFolder/$dbFastaFile $dbNameFull";
if ( ! -f "$dbNameFull.1.ebwt" ) {
    open(CMD0, $cmd0) or die "$!";
    while (<CMD0>){ print };
    die "$!" if $?;
    close CMD0;
}


my $cmd1 = "bowtie -n 3 -a $dbNameFull -c $p1,$p2";

my @bRes = `$cmd1 2>\&1`;
#0       +       SL2.40sc04057   23797776        GAGTCCGACAAGCCCTGAATG   IIIIIIIIIIIIIIIIIIIII   0
#1       -       SL2.40sc04057   23801944        GCTGCCTCTAAACAAATGGTGC  IIIIIIIIIIIIIIIIIIIIII  0

print @bRes;
my %res;
foreach my $res ( @bRes )
{
    if ( $res =~ /([+|-])\s+(\S+)\s+(\d+)\s+(\S+)/ )
    {
        my $frame = $1;
        my $sc    = $2;
        my $pos   = $3;
        my $seq   = $4;
        my $rcSeq = &rc($seq);
        print " GOT NAME $name SEQ $seq SCAFFOLD $sc POSITION $pos (RC $rcSeq) FRAME $frame\n";
        die "NAME $name SEQ $seq DOES NOT MATCH ANYWHERE" if (($seq ne $p1) && ($seq ne $p2) && ($rcSeq ne $p1) && ($rcSeq ne $p2));
        if (( $seq eq $p1 ) || ( $seq eq $p2 ) )
        {
            $res{$seq}{$sc}{$pos}   = $frame;
        } else {
            $res{$rcSeq}{$sc}{$pos} = $frame;
        }
    }
}

die "NO RESULT TO NAME $name PRIMER 1: $p1" if ! exists $res{$p1};
die "NO RESULT TO NAME $name PRIMER 2: $p2" if ! exists $res{$p2};

warn "MORE THAN ONE SCAFFOLD MAP TO NAME $name PRIMER 1: $p1" if scalar keys %{$res{$p1}} > 1;
warn "MORE THAN ONE SCAFFOLD MAP TO NAME $name PRIMER 2: $p2" if scalar keys %{$res{$p2}} > 1;


my %pair;
for my $scaf1 ( sort keys %{$res{$p1}} ) {
    my $pos1 = $res{$p1}{$scaf1};
    die if ! defined $pos1;

    if ( exists ${$res{$p2}}{$scaf1} ) {
        my $pos2 = $res{$p2}{$scaf1};
        die if ! defined $pos2;
        push( @{$pair{$scaf1}}, ( $pos1, $pos2 ));
    }
}


die "NO SINGLE PCR PRODUCT" if ( scalar keys %pair ) < 1;


for my $scaf ( sort keys %pair ) {
    print "  SCAFFOLD $scaf\n";

    my $poses = $pair{$scaf};
    my $pos1  = $poses->[0];
    my $pos2  = $poses->[1];
    print "    POS1 '",join(", ", keys %{$pos1}),"' POS2 '",join(", ", keys %{$pos2}),"'\n";

    warn "NAME $name PRIMER 1 MAPPED IN MORE THAN ONE POSITION AT SCAFFOLD $scaf ",join(", ", keys %{$pos1}),"\n" if ( scalar (keys %{$pos1}) > 1 );
    warn "NAME $name PRIMER 2 MAPPED IN MORE THAN ONE POSITION AT SCAFFOLD $scaf ",join(", ", keys %{$pos2}),"\n" if ( scalar (keys %{$pos2}) > 1 );


    my @prods;
    for my $pos1pos     ( sort {$a <=> $b} keys %$pos1 ) {
        my $frame1 = $pos1->{$pos1pos};
        for my $pos2pos ( sort {$a <=> $b} keys %$pos2 ) {
            my $frame2 = $pos2->{$pos2pos};
            my $start  = $pos1pos < $pos2pos ? $pos1pos : $pos2pos;
            my $end    = $pos1pos < $pos2pos ? $pos2pos : $pos1pos;
            if ( abs($start - $end) < $maxProduct) {
                if ( $frame1 ne $frame2 ) {
                    print "    $start ($frame1) <=> $end ($frame2)\n";
                    push( @prods, [ $start, $end ] );
                } else {
                    print "    $start ($frame1) <=> $end ($frame2) does not anplifies\n";
                }
            }
        }
    }

    foreach my $prod ( @prods )
    {
        my $start   = $prod->[0];
        my $end     = $prod->[1];
        my $prodLen = $end - $start + 1;
        print "        START $start END $end LENGTH $prodLen\n";


        if ( $start > $end )
        {
            warn "      NAME $name START ($start) SMALLER THAN END ($end). TRY INVERTING THE ORDER";
            my $t     = $start;
               $start = $end;
               $end   = $t;
        }

        my $leng = $end-$start;
        printf "\tNAME\t%10s\tSCAF\t%10s\tSTART\t%8d\tEND\t%8d\tLENGTH\t%8d\n", $name,$scaf,$start,$end,$leng;


        die "COULD NOT FIND DATA AGP $dataFolder/$dataAgp" if ! -f "$dataFolder/$dataAgp";
        my $cmd2  = "cat $dataFolder/$dataAgp | grep '$scaf' | gawk '{print \$1}' | sort | uniq | head -1";
        my $chrom = `$cmd2`;
        chomp($chrom);
        print "  CHROM $chrom\n";
        die "NO CHROMOSSOME FOUND" if $chrom eq "";
        my $baseName    = "$outFolder/$baseFile.$chrom.$scaf.$name." . sprintf("%012d",$start) . "-" . sprintf("%012d",$end) . "";




        my $INFASTA      = "data/S_lycopersicum_scaffolds.fa";
        my $DBFASTA      = "data/S_lycopersicum_chromosomes.fa";
        my $DBNAME       = "tomato";
        my $PAR          = '-gapopen 2 -gapextend 1 -num_threads 40 -word_size 6';
        my $OUTBLASTNAME = "$baseName\_Product";
        
        if ( ! -f "db/$DBNAME.nsq" ) {
           print "MAKING BLAST DATABASE\n";
           print `formatdb -t $DBNAME -n db/$DBNAME -V -p F -i $DBFASTA` or die "$? : $!";
            if ( ! -f "db/$DBNAME.nsq" ) { die "error running mkdb " };
        }
        
        print "CUTTING PCR PRODUCT\n";
        if ( ! -f "$OUTBLASTNAME.fasta" ) {
            print `fastaCut.pl $INFASTA $scaf $start $end $OUTBLASTNAME > $OUTBLASTNAME.fasta` or die "$? : $!";
            if ( ! -f "$OUTBLASTNAME.fasta" ) { die "error cutting product" };
        }
        
        print "RUNNING BLAST\n";
        if ( ! -f "$OUTBLASTNAME.fasta.blast" ) {
            print `blastn -outfmt 2 -evalue 1e-1 -db db/$DBNAME -out $OUTBLASTNAME.fasta.blast -query $OUTBLASTNAME.fasta $PAR` or die "$? : $!";
            if ( ! -f "$OUTBLASTNAME.fasta.blast" ) { die "error blasting" };
        }

        print "MAKING BLAST COVERAGE\n";
        my $blastCov = "$OUTBLASTNAME.fasta.blast.cov";
        if ( ! -f "$blastCov" ) {
            print `mapping/blast/blast2align.pl $OUTBLASTNAME.fasta.blast` or die "$? : $!";
            if ( ! -f "$blastCov" ) { die "error creating blast coverage" };
        }




        my $contigMap   = $agpFolder . "/" . $contig . ".$scaf.agp.contig.agp.cov";
        my $gapMap      = $agpFolder . "/" . $contig . ".$scaf.agp.gap.agp.cov";
        my $unknownMap  = $agpFolder . "/" . $contig . ".$scaf.agp.unknown.agp.cov";
        my $otherMap    = $agpFolder . "/" . $contig . ".$scaf.agp.other.agp.cov";
        my $nMap        = "$nFolder/$noneTabFile.$chrom.tab.$scaf.tab.cov";
        my $prop        = "$baseFolder/$baseFile.$chrom.sam.$scaf.sam.cov.prop.cov";
        my $mappingCov  = "mapping/out/S_lycopersicum_chromosomes.fa_S_lycopersicum_chromosomes.pos.$chrom.pos.cov.$scaf.cov";
        my $outFile     = $baseName;


        print "CONTIG  MAP   : $contigMap\n";
        print "GAP     MAP   : $gapMap\n";
        print "UNKNOWN MAP   : $unknownMap\n";
        print "OTHER   MAP   : $otherMap\n";
        print "Ns      MAP   : $nMap\n";
        print "PROP          : $prop\n";

        die "NO CONTIG  MAP ($contigMap)  FOUND" if ! -f $contigMap;
        die "NO GAP     MAP ($gapMap)     FOUND" if ! -f $gapMap;
        die "NO UNKNOWN MAP ($unknownMap) FOUND" if ! -f $unknownMap;
        die "NO OTHER   MAP ($otherMap)   FOUND" if ! -f $otherMap;
        die "NO Ns      MAP ($nMap)       FOUND" if ! -f $nMap;
        die "NO PROP        ($prop)       FOUND" if ! -f $prop;
        die "NO MAPPING COV ($mappingCov) FOUND" if ! -f $mappingCov;


        foreach my $inCut (\$contigMap, \$gapMap, \$unknownMap, \$otherMap, \$nMap, \$prop, \$mappingCov ) {
            my $basein = basename($$inCut);
            my $outCut = "$baseName.$basein";
            if ( ! -f $outCut ) {
                my $cutCmd = "./rangeCut.pl --input $$inCut --cut --start $start --end $end -r > $outCut";
                open (CUT, "$cutCmd|") or die "$!";
                while (<CUT>) { print };
                if ( $? ) {
                    print "$!";
                    unlink($outCut);
                    die;
                }
                close CUT;
            } else {
                print "CUT FILE $outCut ALAREADY EXISTS. SKIPPING\n";
            }
            $$inCut = $outCut;
        }

        print "CONTIG  MAP CUT  : $contigMap\n";
        print "GAP     MAP CUT  : $gapMap\n";
        print "UNKNOWN MAP CUT  : $unknownMap\n";
        print "OTHER   MAP CUT  : $otherMap\n";
        print "Ns      MAP CUT  : $nMap\n";
        print "PROP        CUT  : $prop\n";
        print "MAPPING COV CUT  : $mappingCov\n";
        print "BAST    COV      : $blastCov\n";


        die "NO CONTIG  MAP CUT ($contigMap)  FOUND" if ! -f $contigMap;
        die "NO GAP     MAP CUT ($gapMap)     FOUND" if ! -f $gapMap;
        die "NO UNKNOWN MAP CUT ($unknownMap) FOUND" if ! -f $unknownMap;
        die "NO OTHER   MAP CUT ($otherMap)   FOUND" if ! -f $otherMap;
        die "NO Ns      MAP CUT ($nMap)       FOUND" if ! -f $nMap;
        die "NO PROP        CUT ($prop)       FOUND" if ! -f $prop;
        die "NO MAPPING COV CUT ($mappingCov) FOUND" if ! -f $mappingCov;

        print "MAPPING COV   : $mappingCov\n";
        print "OUT FILE      : $outFile\n";



        print "  GENERATING GRAPHIC $outFile\n";

        if ( -f "$outFile.png" ) {
            if ( ! $replace )
            {
                print "  OUTPUT $outFile.png EXISTS. SKIPPING\n";
                exit 0;
            } else {
                unlink("$outFile.png");
            }
        }







        print "  MAKING GRAPHIC\n";
        my $cmd3      = "./mkplot.pl "              .
            "--input=$prop "                        .
            "--iname='CHROM \"$chrom\" SCAF \"$scaf\" PROBE NAME \"$name\" START \"$start\" END \"$end\"' " .
            "--output=$outFile.png "                .
            "--contig=$contigMap  --cname=CONTIG "  .
            "--contig=$gapMap     --cname=GAP "     .
            "--contig=$unknownMap --cname=UNK "     .
            "--contig=$otherMap   --cname=OTHER "   .
            "--contig=$nMap       --cname=Ns "      .
            "--range=$mappingCov  --rname=COVERAGE ".
            "--range=$blastCov    --rname=BLAST";
        print "    CMD: $cmd3\n";



        open(GRA1, "$cmd3 |") or die "Can't make graphic normal: $!";
        while (<GRA1>) { print };
        if ( $? ) {
            print "$!";
            unlink("$outFile.png");
            unlink("$outFile.plot");
            die;
        }
        close GRA1;


        if ( ! -f "$outFile.png" )
        {
            die "  ERROR CREATING OUTPUT :: $outFile.png";
        } else {
            print "  SUCCESS CREATING OUTPUT $outFile.png\n\n";
        }
    } # for my prod
} # for my scaf



sub rc
{
    my $s = shift;
    $s =~ tr/ACGT/TGCA/;
    $s = reverse($s);
    return $s;
}
