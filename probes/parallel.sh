#!/bin/bash

#http://snippets.dzone.com/posts/show/7807
# Echo to stderr
#
debug() {    
  echo "$@" >&2
}

# check_process PID
#
check_process() { kill -0 $1 2>/dev/null; }

# run_parallel NPROCESS FACTORY
#
# Run N parallel proceses, wait until EOF and all running processes are finished
#
#
# FACTORY is a process/function that returns (using stdout) the command to run
# 
# The status code of FACTORY is interpreted like this:
#
#   0: There is more data
#   != 0: There is no more data to process
#
run_parallel() {
  NPROCESS=$1  
  shift
  
  PIDS=()  
  EOF=0
  while test $EOF = 0 -0 ${#PIDS[*]} -ne 0; do
    if test $EOF = 0 -a ${#PIDS[*]} -lt $NPROCESS; then
      if ! COMMAND=$("$@"); then
        debug "EOF"
        EOF=1
      fi
      if test "$COMMAND"; then 
        $COMMAND &
        PID=$!
        debug "starting process: $@ ($PID)"
        PIDS[$PID]=$PID
      fi
    fi
    
    debug "active processes: ${PIDS[*]}"
    sleep 0.5
     
    for PID in ${PIDS[*]}; do      
      if ! check_process $PID; then
        wait $PID 
        unset PIDS[$PID]
        debug "process finished: $PID"
      fi
    done        
  done
}