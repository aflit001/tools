#!/bin/bash
set -e
set -o pipefail



LC_ALL=C

source make_setup.sh
SCRIPT_NAME=`basename ${BASH_SOURCE}`
DSC=" :: MAKE GFF"

######################################################
## FILTERING EXTRA GFF CONVERTER
######################################################
export EXTRAGFF_CONVERTER=SL2.40ch_from_sc.agp
export EXTRAGFF_CONVERTER_GREP=''
export EXTRAGFF_CONVERTER_GREN='\S+\s+\S+\s+\S+\s+\S+\s+U'

if [[ "$EXTRAGFF_CONVERTER_GREN" == "" ]]; then
    EXTRAGFF_CONVERTER_GREN="0x0"
fi

#echoMsg  "["`date`" :: $SECONDS :: "`basename ${BASH_SOURCE}`" :: $LINENO]  EXTRA GFF CONVERTER '$EXTRAGFF_CONVERTER' GREP POS '$EXTRAGFF_CONVERTER_GREP' GREP NEG '$EXTRAGFF_CONVERTER_GREN'"
echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "EXTRA GFF CONVERTER '$EXTRAGFF_CONVERTER' GREP POS '$EXTRAGFF_CONVERTER_GREP' GREP NEG '$EXTRAGFF_CONVERTER_GREN'"
export EXTRAGFF_CONVERTER_EXT="${EXTRAGFF_CONVERTER##*.}"


IN="${INFOLDER}/$EXTRAGFF_CONVERTER"
OUT="${EXTRAGFF}/$EXTRAGFF_CONVERTER.filtered.$EXTRAGFF_CONVERTER_EXT"
export EXTRAGFF_CONVERTER_CONVERTED=$OUT
if [[ ! -f "$OUT" ]]; then
    #cat finished_bacs.all.gff3 | grep bacs > finished_bacs.all.gff3.bacs.gff3
    CMD="grep -Ev '^#' $IN | grep -Ev '$EXTRAGFF_CONVERTER_GREN' | grep '$EXTRAGFF_CONVERTER_GREP' > $OUT"
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    $CMD"
    eval time "$CMD" 2>&1 | tee -a err.log
    #echo "PIPESTATUS = $PIPESTATUS"
    
    if [[ ! -f "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR GENERATING $OUT ***" 
        rm $OUT &>/dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " GENERATING $OUT SUCCEEDED" 
    fi
    
    if [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR GENERATING $OUT. SIZE 0 ***"  
        rm $OUT &>/dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " GENERATING $OUT WITH SIZE GT 0" 
    fi
    
    DIFFS=`diff $IN $OUT | wc -l`
    NUMLI=`cat $IN | wc -l`
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    $DIFFS LINES FILTERED OUT OF $NUMLI"
else
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  $OUT EXISTS. SKIPPING"
fi




######################################################
## FILTERING EXTRA GFF
######################################################
echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " FILTERING EXTRA GFFs"
for p in $(seq 0 $((${#EXTRAGFF_TO_CONVERT_FILE[@]} - 1))); do
    GFF_FILE=${EXTRAGFF_TO_CONVERT_FILE[$p]}
    GFF_NAME=${EXTRAGFF_TO_CONVERT_NAME[$p]}
    GFF_GREP=${EXTRAGFF_TO_CONVERT_GREP[$p]}
    GFF_GREN=${EXTRAGFF_TO_CONVERT_GREN[$p]}
    
    if [[ "$GFF_GREN" == "" ]]; then
        GFF_GREN="0x0"
    fi
    
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  GFF FILE '$GFF_FILE' NAME '$GFF_NAME' GREP POS '$GFF_GREP' GREP NEG '$GFF_GREN'"
    IN="${INFOLDER}/$GFF_FILE"
    OUT="${EXTRAGFF}/$GFF_FILE.filtered.gff3"
    if [[ ! -f "$OUT" ]]; then
        #cat finished_bacs.all.gff3 | grep bacs > finished_bacs.all.gff3.bacs.gff3
        CMD="grep -Ev '^#' $IN | grep -Ev '$GFF_GREN' | grep '$GFF_GREP' > $OUT"
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    $CMD"
        eval $CMD 2>&1 | tee -a err.log
        if [[ ! -f "$OUT" ]]; then
            echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR GENERATING $OUT ***" 
            rm $OUT &>/dev/null
            exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
        fi
        if [[ ! -s "$OUT" ]]; then
            echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR GENERATING $OUT. SIZE 0 ***" 
            rm $OUT &>/dev/null
            exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
        fi
        DIFFS=`diff $IN $OUT | wc -l`
        NUMLI=`cat $IN | wc -l`
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    $DIFFS LINES FILTERED OUT OF $NUMLI"
    else
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  $OUT EXISTS. SKIPPING"
    fi
done



######################################################
## CONVERTING EXTRA GFF
######################################################
echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " CONVERTING EXTRA GFFs"
for p in $(seq 0 $((${#EXTRAGFF_TO_CONVERT_FILE[@]} - 1))); do
    GFF_FILE=${EXTRAGFF_TO_CONVERT_FILE[$p]}
    GFF_NAME=${EXTRAGFF_TO_CONVERT_NAME[$p]}
    
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  GFF FILE '$GFF_FILE' NAME '$GFF_NAME'"
    IN="${EXTRAGFF}/$GFF_FILE.filtered.gff3"
    OUT="${EXTRAGFF}/$GFF_FILE.filtered.gff3.converted.gff3"
    
    if [[ ! -f "$OUT" ]]; then
        #cat finished_bacs.all.gff3 | grep bacs > finished_bacs.all.gff3.bacs.gff3
        CMD="./chrom2scaf.pl $IN $EXTRAGFF_CONVERTER_CONVERTED 2>$OUT.log > $OUT"
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    $CMD"
        eval $CMD 2>&1 | tee -a err.log
        if [[ ! -f "$OUT" ]]; then
            echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR GENERATING $OUT ***" 
            rm $OUT &>/dev/null
            exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
        fi
        if [[ ! -s "$OUT" ]]; then
            echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR GENERATING $OUT. SIZE 0 ***" 
            rm $OUT &>/dev/null
            exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
        fi
    else
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  $OUT EXISTS. SKIPPING"
    fi
done

