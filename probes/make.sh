#!/bin/bash
set -o pipefail
set -e

LC_ALL=C

source make_setup.sh
#echoMsg  ${PIPESTATUS[1]}

if [[ -f "err.log" ]]; then 
    rm err.log &>/dev/null
fi
SCRIPT_NAME=`basename ${BASH_SOURCE}`
DSC=" :: BASE"

echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " JELLY THREADS $JELLYTHREADS"
echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " BOWTIE THREADS $BOWTIETHREADS"

######################################################
## JELLYFISH
######################################################

OUT="$KMERFOLDER/${INFASTA}_mer_counts_0"
CMD="jellyfish count $INFOLDER/$INFASTA --mer-len=$JELLYMERLEN \
    --threads=$JELLYTHREADS --output=$KMERFOLDER/${INFASTA}_mer_counts \
    --both-strands --size=$JELLYHASHSIZE"
export JELLYDBFILE=$OUT
echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " RUNNING JELLY COUNT"

if [[ ! -s "$OUT" ]]; then
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  RUNNING '$CMD'"
    eval time "$CMD" 2>&1 | tee -a err.log
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CMD $CMD ***"
        rm ${INFASTA}_mer_counts* &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    $OUT CREATED"
    fi
else
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  $OUT EXISTS. SKIPPING"
fi




OUT="$KMERFOLDER/${INFASTA}.kmer.fa"
export JELLYFASTA=$OUT

#  -L, --lower-count=LONG  Don't output k-mer with count < lower-count
#  -U, --upper-count=LONG  Don't output k-mer with count > upper-count
JU=" --upper-count=$JELLYUPPERCOUNT"
if [[ "$JELLYUPPERCOUNT" == -1 ]]; then
    JU=""
fi

JL=" --lower-count=$JELLYLOWERCOUNT"
if [[ "$JELLYLOWERCOUNT" == -1 ]]; then
    JL=""
fi

CMD="jellyfish dump $JL $JU --upper-count=1 \
    $KMERFOLDER/${INFASTA}_mer_counts* > $OUT"

echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " RUNNING JELLY DUMP TO FASTA"

if [[ ! -s "$OUT" ]]; then
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  RUNNING '$CMD'"
    eval time "$CMD"  2>&1 | tee -a err.log
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CMD $CMD ***" 
        rm $OUT &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    $OUT CREATED"
    fi

    #twit.py finished jellyfish ${INFASTA}.kmer.fa \| starting bowtie-build $BOWTIEDBFASTAFILE
else
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  $OUT EXISTS. SKIPPING"
fi




OUT="$KMERFOLDER/${INFASTA}.histo"
export JELLYHISTO=$OUT
CMD="jellyfish histo $KMERFOLDER/${INFASTA}_mer_counts* > $OUT"

echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " RUNNING JELLY DUMP TO HISTO"

if [[ ! -s "$OUT" ]]; then
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  RUNNING '$CMD'"
    eval time "$CMD"  2>&1 | tee -a err.log
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CMD $CMD ***" 
        rm $OUT &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    $OUT CREATED"
    fi

    #twit.py finished jellyfish ${INFASTA}.kmer.fa \| starting bowtie-build $BOWTIEDBFASTAFILE
else
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  $OUT EXISTS. SKIPPING"
fi


OUT="$KMERFOLDER/${INFASTA}.stats"
export JELLYSTATS=$OUT
CMD="jellyfish stats $KMERFOLDER/${INFASTA}_mer_counts* > $OUT"

echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " RUNNING JELLY STATS"

if [[ ! -s "$OUT" ]]; then
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  RUNNING '$CMD'"
    eval time "$CMD"  2>&1 | tee -a err.log
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CMD $CMD ***" 
        rm $OUT &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    $OUT CREATED"
    fi

    #twit.py finished jellyfish ${INFASTA}.kmer.fa \| starting bowtie-build $BOWTIEDBFASTAFILE
else
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  $OUT EXISTS. SKIPPING"
fi


OUT="$KMERFOLDER/${INFASTA}.histo.png"
export JELLYHISTOIMG=$OUT
CMD="../../filter/mkplot $JELLYHISTO"

echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " RUNNING JELLY TO IMAGE HISTOGRAM"

if [[ ! -s "$OUT" ]]; then
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  RUNNING '$CMD'"
    eval time "$CMD"  2>&1 | tee -a err.log
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CMD $CMD ***" 
        rm $OUT &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    $OUT CREATED"
    fi

    #twit.py finished jellyfish ${INFASTA}.kmer.fa \| starting bowtie-build $BOWTIEDBFASTAFILE
else
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  $OUT EXISTS. SKIPPING"
fi

######################################################
## BOWTIE
######################################################
if [[ $USEBOWTIE -eq "1" ]]; then
    OUT="$EBWTFOLDER/$DBNAME.1.ebwt"
    export DB=$OUT
    CMD="bowtie-build --offrate $BOWTIEOFFRATE  \
        $INFOLDER/$DBFASTAFILE \
        $EBWTFOLDER/$DBNAME"
        # mark bw each 2 ^ 1  = 2 rows

    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " RUNNING BOWTIE BUILD"

    if [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  RUNNING '$CMD'"
        eval time "$CMD"  2>&1 | tee -a err.log
        wait ${!}
        if [[ ! -s "$OUT" ]]; then
            echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CMD $CMD ***" 
            rm $EBWTFOLDER/${DBNAME}* &> /dev/null
            exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
        else
            echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    $OUT CREATED"
        fi
    else
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  $OUT EXISTS. SKIPPING"
    fi




    OUT="$OUTFOLDER/${INFASTA}_$DBNAME.map"
    export BOWTIEMAP=$OUT
    export MAPPINGOUT=$OUT
    BT=" -m $BOWTIESUPRESSBIGGERTHAN"

    if [[ "$BOWTIESUPRESSBIGGERTHAN" == -1 ]]; then
        BT=""
    fi


    CMD="bowtie -S --mm --offrate $BOWTIEOFFRATE --chunkmbs $BOWTIECHUNKMBS  \
        $BT --time   --threads $BOWTIETHREADS --seedlen $BOWTIESEEDLEN    \
        -v $BOWTIEMISMATCHES      \
        --al  $BOWTIEMAP.all      \
        --un  $BOWTIEMAP.unmapped \
        --max $BOWTIEMAP.max      \
        -f $EBWTFOLDER/$DBNAME $JELLYFASTA > $OUT"

    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " RUNNING BOWTIE MAP"

    if [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  RUNNING '$CMD'"
        #twit.py finished bowtie-build \| starting bowtie $OUT
        eval time "$CMD"  2>&1 | tee -a err.log
        wait ${!}
        if [[ ! -s "$OUT" ]]; then
            echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CMD $CMD ***" 
            rm ${OUT} &> /dev/null
            exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
        else
            echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    $OUT CREATED"
        fi
        #twit.py finished bowtie \| starting filtering
    else
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  $OUT EXISTS. SKIPPING"
    fi
fi














######################################################
## BWA
######################################################
if [[ $USEBWA -eq "1" ]]; then
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " USING BWA"
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " DBNAME        $DBNAME"
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " DB FASTA FILE $DBFASTAFILE"

    OUT="$BWAFOLDER/$DBNAME.pac"
    export DB=$OUT
    export DBNAMEFULL=$BWAFOLDER/$DBNAME
    CMD="bwa index -p $DBNAMEFULL -a is $INFOLDER/$DBFASTAFILE"
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " RUNNING BWA INDEX"
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " DB NAME FULL $DBNAMEFULL"

    if [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  RUNNING '$CMD'"
        eval time "$CMD"  2>&1 | tee -a err.log
        wait ${!}
        if [[ ! -s "$OUT" ]]; then
            echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CMD $CMD ***" 
            rm $BWAFOLDER/${DBNAME}* &> /dev/null
            exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
        else
            echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    $OUT CREATED"
        fi
    else
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  $OUT EXISTS. SKIPPING"
    fi




    OUT="$OUTFOLDER/${INFASTA}_$DBNAME.sai"
    export BWASAI=$OUT
    CMD="bwa aln $DBNAMEFULL -t $BWATHREADS        -n $BWAALN_MAXEDIT -R 999999 -N \
                             -o $BWAALN_MAXGAPOPEN -e $BWAALN_MAXGAPEXT \
                             -O $BWAALN_GAPOPEN    -E $BWAALN_GAPEXTENT \
                             -k $BWAALN_MAXEDITSEED $JELLYFASTA > $OUT"

    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " RUNNING BWA MAP"

    if [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  RUNNING '$CMD'"
        #twit.py finished bowtie-build \| starting bowtie $OUT
        #eval time "$CMD"  2>&1 | tee -a err.log
        echo "$CMD" > $OUT.log
        eval time $CMD | tee -a arr.log
        wait ${!}
        if [[ ! -s "$OUT" ]]; then
            echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CMD $CMD ***" 
            rm ${OUT} &> /dev/null
            exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
        else
            echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    $OUT CREATED"
        fi
        #twit.py finished bowtie \| starting filtering
    else
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  $OUT EXISTS. SKIPPING"
    fi



    #export BWAEXP_MAX=1000  #-n 
    #export BWAEXP_MAX=1     #-n 
    OUT="$OUTFOLDER/${INFASTA}_$DBNAME.sam"
    export BWASAM=$OUT
    export MAPPINGOUT=$OUT
    CMD="bwa samse   -n $BWAEXP_MAX $DBNAMEFULL $BWASAI $JELLYFASTA | grep -e 'X1:i:0' | grep -v 'XA:' > $OUT"

    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " RUNNING BWA EXPORT SAM"

    if [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  RUNNING '$CMD'"
        #twit.py finished bowtie-build \| starting bowtie $OUT
        echo "$CMD" > $OUT.log
        eval time "$CMD"  2>&1 | tee -a err.log
        wait ${!}
        if [[ ! -s "$OUT" ]]; then
            echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CMD $CMD ***" 
            rm ${OUT} &> /dev/null
            exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
        else
            echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    $OUT CREATED"
        fi
        #twit.py finished bowtie \| starting filtering
    else
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  $OUT EXISTS. SKIPPING"
    fi
fi


######################################################
## MAPPING Ns
######################################################
source ./make_N.sh
if [[ $? -ne "0" ]]; then
    DSC=" :: BASE"
    SCRIPT_NAME=`basename ${BASH_SOURCE}`
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    *** RUNNING ./make_N.sh FAILED ***"
    exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
fi

######################################################
## GFF CONVERTER
######################################################
source ./make_GFF.sh
if [[ $? -ne "0" ]]; then
    DSC=" :: BASE"
    SCRIPT_NAME=`basename ${BASH_SOURCE}`    
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    *** RUNNING ./make_GFF.sh $CHROMNAME FAILED ***"
    exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
fi
DSC=" :: BASE"
SCRIPT_NAME=`basename ${BASH_SOURCE}`


######################################################
## MAKING CHROMOSOMES
######################################################
CHROMPIDS=()
PIDNAMES=()
for element in $(seq 0 $((${#DESIREDCHROMOSSOME[@]} - 1)))
do
    CHROMNUM=${DESIREDCHROMOSSOME[$element]}
    CHROMNAME=$SEQEPITOPE$COL1EPITOPE${DESIREDCHROMOSSOME[$element]}

    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " RUNNING CHROMOSOME $CHROMNAME"
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "   CHROMOSSOME POS    #"$((element+1))" / "${#DESIREDCHROMOSSOME[@]}
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "   CHROMOSSOME NUMBER #"$CHROMNUM
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "   CHROMOSSOME NAME   #"$CHROMNAME
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "     RUNNING ./make_chrom.sh $CHROMNAME"

    ./make_chrom.sh $CHROMNAME &
    CHROMPIDS[$element]=$!
    PIDNAMES[$!]=$CHROMNAME
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " CHROMNAME $CHROMNAME PID $!"
done



LOOPCOUNT=0
WAITALL_DELAY=3
while [[ ! -z `jobs -p` ]]; do
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " LOOP $LOOPCOUNT"
    LOOPCOUNT=$((LOOPCOUNT+1))
    
    while :; do
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " Processes remaining: "`jobs -p`""
        
        for element in $(seq $((${#CHROMPIDS[@]} - 1)) -1 0); do
            pid=${CHROMPIDS[$element]}
            PNAME=${PIDNAMES[$pid]}
            
            echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  checking pid $pid [$PNAME]"

            if [[ -z "$pid" ]]; then 
                #echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  *** PID $pid WAS NOT FOUND ***"
                continue
                #exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
            fi

            if [[ -z "$PNAME" ]]; then 
                echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  *** PID $pid HAS NO NAME [$PNAME] ***"
                exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
            fi
            
            if kill -0 "$pid" 2>/dev/null; then
                echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    $pid [$PNAME] is still alive"
            elif wait "$pid"; then
                CHROMPIDS=(${CHROMPIDS[@]:0:$element} ${CHROMPIDS[@]:$(($element + 1))})
                echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    $pid [$PNAME] exited with zero exit status"
                echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      SUCCESS ANALYZING CHROMOSOME $PNAME"
                continue
            else
                echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    *** $pid [$PNAME] exited with non zero exit status ***"
                echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    ***   JOB $PNAME HAS FAILED ***"
                echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    ***     ERROR ANALYZING CHROMOSOME $PNAME ***"
                exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
            fi
        done
        
        JOBS=`jobs -p`
        if [[ -z "$JOBS" ]]; then
            echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      NO MORE JOBS. BREAKING"
            break
        fi
        # TODO: how to interrupt this sleep when a child terminates?
        sleep ${WAITALL_DELAY:-1}
    done
done

wait
sleep 5
echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " ALL CHROMOSOMES ANALYZED SUCCESSFULLY"


echoMsg  "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "#################################"
echoMsg  "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "####### TESTING PRIMERS #########"
echoMsg  "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "#################################"
echoMsg  "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " TESTING PRIMERS"
#./sander_primers.sh   2>&1 | tee -a err.log
echoMsg  "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " FINISHED TESTING PRIMERS"
echoMsg  "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " ANALYSIS FINISHED. QUITTING"
echo ""
#twit.py finished making probe

