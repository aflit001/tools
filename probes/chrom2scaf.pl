#!/usr/bin/perl -w
use strict;
my $verbose  = 0;
my $chromGff = shift @ARGV;
my $scafGff  = shift @ARGV;

die "NO CHROMOSOME GFF DEFINED" if ! defined $chromGff;
die "NO SCAFFOLD   GFF DEFINED" if ! defined $scafGff;

die "CHROMOSOME GFF '$chromGff' DOES NOT EXISTS" if ! -f $chromGff;
die "SCAFFOLD   GFF '$scafGff'  DOES NOT EXISTS" if ! -f $scafGff;

die "CHROMOSOME GFF '$chromGff' HAS SIZE 0" if ! -s $chromGff;
die "SCAFFOLD   GFF '$scafGff'  HAS SIZE 0" if ! -s $scafGff;



#CHROM
	###gff-version 3
	###feature-ontology http://song.cvs.sourceforge.net/*checkout*/song/ontology/sofa.obo?revision=1.93
	###sequence-region SL2.40ch00 1 21805821
	#SL2.40ch00      ITAG_eugene     gene    16437   18189   .       +       .       Alias=Solyc00g005000;ID=gene:Solyc00g005000.2;Name=Solyc00g005000.2;from_BOGAS=1;length=1753
	#SL2.40ch00      ITAG_eugene     mRNA    16437   18189   .       +       .       ID=mRNA:Solyc00g005000.2.1;Name=Solyc00g005000.2.1;Note=Aspartic proteinase nepenthesin I (AHRD V1 **-- A9ZMF9_NEPAL)%3B contains Interpro domain(s)  IPR001461  Peptidase A1 ;Ontology_term=GO:0006508;Parent=gene:Solyc00g005000.2;from_BOGAS=1;interpro2go_term=GO:0006508;length=1753;nb_exon=2
#SCAF
	#CHROM           START           END             #       TYPE    NAME            
	#SL2.40ch01      1               32987597        1       W       SL2.40sc04133   1       32987597        -
	#SL2.40ch01      32987598        32987697        2       U       100     contig  no
	#SL2.40ch01      32987698        35621724        3       W       SL2.40sc03666   1       2634027 0

my %sca;
open SCA, "<$scafGff"  or die $!;
while (my $scaLine = <SCA>)
{
    chomp $scaLine;
	my @scaFields = split(/\t/, $scaLine);
    print STDERR "  LOADING CHROMOSOME $scaFields[0] SCAFFOLD $scaFields[5]\n";
    $sca{$scaFields[0]}{$scaFields[5]} = [ $scaFields[1], $scaFields[2], $scaFields[4] ];
}
close SCA;

#my @chr = split(<CHR>);
open CHR, "<$chromGff" or die $!;
while (my $chrLine = <CHR>)
{
    chomp $chrLine;
    next if substr($chrLine, 0, 1) eq "#";
    my @chrFields = split(/\t/, $chrLine);
    my $chr       = $chrFields[0];
    my $start     = $chrFields[3];
    my $end       = $chrFields[4];
    
    if ( ! exists $sca{$chr} )
    {
        warn "COULD NOT FIND CHROMOSOME $chr IN SCAFFOLD LIST" ;
        next;
    }
    
    my $scaf  = $sca{$chr};
    my $found = 0;
    my $lines = '';
    warn $chrLine, "\n" if $verbose;
    
    foreach my $scafName ( keys %$scaf )
    {
        my $nfo          = $scaf->{$scafName};
        my $scafStartPos = $nfo->[0];
        my $scafEndPos   = $nfo->[1];
        my $scafType     = $nfo->[2];
        my $line         = '';
        
        if ( $scafStartPos <= $end && $scafEndPos >= $start )
        {
            my $startNew     = $start;
            my $endNew       = $end;
            
            if ( $scafStartPos <= $start )
            { # inside
                $startNew     = $start - $scafStartPos + 1;
                warn sprintf("  SCAFFOLD %s (%12d:%12d) START POS %12d %2s %12d (START NEW %12d)\n", $scafName, $scafStartPos, $scafEndPos, $scafStartPos, "<=", $start, $startNew) if $verbose;
                $found++;
            }
            else
            {
                $startNew     = $scafStartPos;
                warn sprintf("  SCAFFOLD %s (%12d:%12d) START POS %12d %2s %12d (START NEW %12d)\n", $scafName, $scafStartPos, $scafEndPos, $scafStartPos, ">", $start, $startNew) if $verbose;
                $found++;
            }
            
            if ( $scafEndPos <= $end )
            { # inside
                $endNew       = $scafEndPos;
                warn sprintf("  SCAFFOLD %s (%12d:%12d) END   POS %12d %2s %12d (END   NEW %12d)\n", $scafName, $scafStartPos, $scafEndPos, $scafEndPos, "<=", $end, $endNew) if $verbose;
                $found++;
            }
            else
            {
                $endNew       = $end - $scafStartPos + 1;
                warn sprintf("  SCAFFOLD %s (%12d:%12d) END   POS %12d %2s %12d (END   NEW %12d)\n", $scafName, $scafStartPos, $scafEndPos, $scafEndPos, ">", $end, $endNew) if $verbose;
                $found++;
            }
            
            $chrFields[0] = $scafName;
            $chrFields[3] = $startNew;
            $chrFields[4] = $endNew;
            $line         = join("\t", @chrFields) . "\n";
            warn sprintf("  PLACED CHR %s START %12d END %12d ON SCAFFOLD %s (%12d:%12d)\n\n",
                         $chr,$start,$end,$scafName,$scafStartPos,$scafEndPos)  if $verbose;
        } else {
            next;
        }

        $lines .= $line;
    }
    
    
    if ( ! $found )
    {
        warn "COULD NOT PLACE CHR $chr START $start END $end ON ANY SCAFFOLD\n\n";
        next;
    }
    elsif ( $lines ne '' )
    {
        print $lines;
    }
}
close CHR;
