#!/bin/bash
INGFF=$1

if [[ -f "$INGFF" ]]; then
	if [[ ! -f "$INGFF.cov" ]]; then
	##gff-version 3
		#SL2.40ch00      SL2.40_assembly ultracontig     1       21805821        .       .       .       ID=SL2.40ch00;Name=SL2.40ch00
		grep -vE "^#" $INGFF | gawk '{print $4"\t"$5}' | ../range > $INGFF.cov
	else
		echo "OUTPUT $INGFF.cov EXISTS. SKIPPING"
		exit 0
	fi
else
	echo "INPUT GFF $INGFF DOES NOT EXISTS"
	exit 1
fi
