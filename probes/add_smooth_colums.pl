#!/usr/bin/perl -w
use strict;
my $verbose      = 0;
my $printSum     = 0;
my $tabDelimited = 1;

use constant SIZE    => 0;
use constant METHOD  => 1;
use constant NAME    => 2;
use constant COLPOS  => 3;
use constant CURRPOS => 4;

#print STDERR "MAX WINDOW : ", $maxWindow, "\n" if $verbose;

#my @cols  = (
#    [  5, "mid",    "5kbp"        ],
#    [ 10, "mid",    "10kbp"       ],
#    [  5, "before", "5kbp before" ],
#    [  5, "after",  "5kbp after"  ],
#);

my @cols  = (
    [       500, "mid",    "500 bp"       ],
    [     2_500, "mid",    "2.5 kbp"      ],
    [     5_000, "mid",    "5 kbp"        ],
#    [    10_000, "mid",    "10 kbp"       ],
    [    50_000, "mid",    "50 kbp"       ],
#    [   100_000, "mid",    "100 kbp"      ],
#    [   500_000, "mid",    "500 kbp"      ],
    [ 1_000_000, "mid",    "1 Mbp"        ],
    [     5_000, "before", "5 kbp before" ],
    [     5_000, "after",  "5 kbp after"  ],
);


my $maxWindow  = 1;
my $numCols    = 0;
my $maxNameLen = 0;

for my $ob ( @cols ) {
    #$header        .= "\t" if defined $header;
    $ob->[COLPOS]   = 2 + $numCols++; #col pos
    $ob->[CURRPOS]  = 0;          #last pos
    my $nameLen     = length($ob->[NAME]);
    $maxNameLen     = $nameLen if $nameLen > $maxNameLen;
    
    if ( ! ( $ob->[SIZE] % 2 ) ) { #even
        print STDERR "SIZE $ob->[SIZE] IS EVEN\n" if $verbose;
        $ob->[SIZE]++;    #last pos
    }
    $maxWindow      = $ob->[SIZE] if $ob->[SIZE] > $maxWindow;
}

$maxNameLen++;
print STDERR "MAX NAME LENGTH $maxNameLen\n" if $verbose;

my $maxWindowBuffer = $maxWindow + 1;

if ( $tabDelimited ) {
    print join("\t", ("#Position","Coverage"));
    map { print "\t", $_->[NAME] } @cols;
    if ( $printSum ) {
        print "\tSum\n";
    } else {
        print "\n";
    }
} else {
    printf "%-".$maxNameLen."s%-".$maxNameLen."s", "#Position","Coverage";
    map { printf "%-".$maxNameLen."s", $_->[NAME] } @cols;
    
    if ( $printSum ) {
        printf "%-".$maxNameLen."s\n","Sum";
    } else {
        print "\n";
    }
}


my @openWindow;
#print STDERR "INITIALIZING EMPTY BUFFER\n";
#$openWindow[$maxWindowBuffer] = [];
#for ( my $i = 0; $i < $maxWindowBuffer; $i++ ) {
#    $openWindow[$i] = [undef, undef, undef,undef, undef, undef,undef, undef, undef,undef, undef, undef];
#}
#print STDERR "EMPTY BUFFER INITIALIZED\n";

my $linesRead      = 0;
my $linesWritten   = 0;
my $currPos        = 0;
my $globalSumSoFar = 0;

while ( my $line = <STDIN> ) {
    if ( $line =~ /^(\d+)\s+(\d+(\.\d+)*)/)
    { # [+-]?(\d+\.\d+|\d+\.|\.\d+)
        print STDERR $line if $verbose;
        my $pos   = $1;
        my $count = $2;
        $linesRead++;
        print STDERR $linesRead, " .. " if ! ( $linesRead % 100_000 );
        #print "\n\n", $line;
        &appendList(\$pos, \$count);
    } else {
        print STDERR "INCORRECT LINE $line" if $verbose;
        die;
    }
}

print STDERR "DATA HAS ENDED\n";
&empty();



if ( $linesRead != $linesWritten ) {
    print STDERR "LINES READ   : $linesRead\n";
    print STDERR "LINES WRITTEN: $linesWritten\n";
    print STDERR "ERROR. NUMBER OF LINES DIVERGE. QUITTING WITH ERROR\n";
    exit 1;
}

sub appendList {
    my $pos             = shift;
    my $count           = shift;
       $globalSumSoFar += $$count;
    my $virtPos         = $currPos % $maxWindowBuffer;
    print STDERR "ADDING TO OPENWINDOW POS " , ($currPos % $maxWindow), " VALUES :: POS $$pos COUNT $$count :: GLOBAL SUM SO FAR $globalSumSoFar [VIRT POS $virtPos]\n" if $verbose;


    #my @colVals = ($pos, $count);
    #$colVals[$numCols + 2] = \$lGlobalSumSoFar;
    #print @colVals;
    $openWindow[$virtPos][0]            = $pos;
    $openWindow[$virtPos][1]            = $count;
    for ( my $i = 2; $i <= $numCols + 2; $i++ ) {
        $openWindow[$virtPos][$i] = undef;
    }

    my $lGlobalSumSoFar = $globalSumSoFar;
    $openWindow[$virtPos][$numCols + 2] = \$lGlobalSumSoFar;

    #print "CURR WINDOW SIZE ", scalar @openWindow, " MAX WINDOW $maxWindow\n";



    for my $col ( @cols )
    {
        &processCols($col); # call itself and let itself knows who it is and its constraints
    }
    
    

    if ( scalar @openWindow >= $maxWindow ) {
        my $prevPosVirt = ($currPos + 1) % $maxWindowBuffer;
        print STDERR "POS VIRT $virtPos PREV $prevPosVirt\n" if $verbose;
        printPos($currPos + 1);
    } else {
        print STDERR "[OPENWINDOW TOO SMALL (",scalar @openWindow,")]\n" if $verbose;
    }
    
    $currPos++;
}


sub empty      {
    my $currSPos = $currPos;
    my $currEPos = $currPos + $maxWindowBuffer - 2;
    
    if ( scalar @openWindow <= $maxWindow ) {
        print STDERR "ERROR: NUM OF SEQUENCES ",scalar @openWindow," IS SMALLER THAN THE BIGGEST WINDOW ($maxWindow)\n";
        exit 1;
    }
    
    &fillGaps();
    
    while ( $currSPos < $currEPos )
    {
        printPos( ++$currSPos );
    }
}

sub printPos   {
    my $pos     = shift;
    my $posVirt = ($pos + 1) % $maxWindowBuffer;
    $linesWritten++;
    
    my $lastCol = $numCols;
    $lastCol-- if ! $printSum;
    $lastCol += 3;
    #print STDERR "NUM COLS = $numCols LAST COL $lastCol\n";
    for ( my $colNum = 0; $colNum < $lastCol; $colNum++ ) {
        my $col = $openWindow[$posVirt][$colNum];
        $$col = -0 if ! defined $$col;
        
        if ( $tabDelimited ) {
            if ( $colNum > 0 ) {
                print "\t", $$col;
            } else {
                print $$col;
            }
        } else {
            if ( $colNum > 1 ) {
                printf "%-".$maxNameLen."s", sprintf("%6.2f", $$col);
            } else {
                printf "%-".$maxNameLen."d", $$col;
            }
        }
    }
    print "\n";
}


sub fillGaps   {

    for my $col ( @cols )
    {
        my $maxSize    = $col->[SIZE];     # size of smooth
        my $method     = $col->[METHOD];   # mid, begin or end
        my $name       = $col->[NAME];     # col name
        my $colNum     = $col->[COLPOS];   # column position
        my $currArrPos = $col->[CURRPOS];  # last position
        
        for ( my $pos = $currPos - $maxWindowBuffer; $pos < $currPos; $pos++ ) {
            my $data    = $openWindow[$pos % $maxWindowBuffer];
            my $colData = $data->[$colNum];
            if ( ! defined $colData ) {
                #print "POS $pos NOT DEFINED FOR $name\n";
                if ( $method eq "mid" ) {
                    $col->[SIZE] -= 2;
                } else {
                    $col->[SIZE]--;
                }
                
                $col->[CURRPOS] = $pos;
                &processCols($col, 1);
            }
        }
    }
    $currPos++;
}

sub processCols {
    my $data           = shift;
    my $once           = shift;
    if ( $once ) {
        $once = 1;
    } else {
        $once = -1;
    }
    my $maxSize        =  $data->[SIZE];     # size of smooth
    my $method         =  $data->[METHOD];   # mid, begin or end
    my $name           =  $data->[NAME];     # col name
    my $colNum         =  $data->[COLPOS];   # column position
    my $currArrPos     = \$data->[CURRPOS];  # last position
    my $openWindowSize = scalar @openWindow;


    print STDERR "  APPENDING COLUMN (". sprintf("%-12s", $name) . "): MAXSIZE $maxSize CURR ARRAY POS $$currArrPos CURR POS $currPos\n" if $verbose;
    
    if    ( $method eq "mid"   ) {
        my $midSize = int($maxSize / 2);
        while ( ( $$currArrPos < ($currPos  - $midSize + 1) ) ) {
            my $begin      = $$currArrPos - $midSize;
            my $end        = $$currArrPos + $midSize;
            my $target     = $$currArrPos;
            my $tmpMaxSize = $maxSize;
            
            if ( $begin < 0 && $end > 0 ) {
                print STDERR "          DOING WHAT I CAN MATE $begin $target $end (MAX SIZE $tmpMaxSize)\n" if $verbose;
                $end   += $begin;
                $begin  = 0;
                $tmpMaxSize = $end - $begin + 1;
                print STDERR "          DID WHAT I COULD MATE $begin $target $end (MAX SIZE $tmpMaxSize)\n" if $verbose;
            }

            print STDERR "      APPENDING $begin $target $end (MAX SIZE $tmpMaxSize)\n" if $verbose;
            &updateVal("MID", $tmpMaxSize, $colNum, $target, $begin, $end) if $target >= 0;
            $$currArrPos++;
            last if $once == 1;
        }
    }
    else {
        while ( $$currArrPos < $currPos + 1 ) {
            if ( $method eq "after" ) {
                my $begin      = $$currArrPos - $maxSize + 1;
                my $end        = $$currArrPos;
                my $target     =  $begin;
                print STDERR "      APPENDING $begin $target $end (MAX SIZE $maxSize)\n" if $verbose;
                &updateVal("AFTER", $maxSize, $colNum, $target, $begin, $end) if $target >= 0;

            }
            elsif ( $method eq "before"   ) {
                my $begin      = $$currArrPos - $maxSize + 1;
                my $end        = $$currArrPos;
                my $target     = $end;
                my $tmpMaxSize = $maxSize;

                if ( $begin < 0 && $end > 0 ) {
                    print STDERR "          DOING WHAT I CAN MATE $begin $target $end (MAX SIZE $tmpMaxSize)\n" if $verbose;
                    $begin = 0;
                    $tmpMaxSize = $end - $begin + 1;
                    print STDERR "          DID WHAT I COULD MATE $begin $target $end (MAX SIZE $tmpMaxSize)\n" if $verbose;
                }
                
                print STDERR "      APPENDING $begin $target $end (MAX SIZE $tmpMaxSize)\n" if $verbose;
                &updateVal("BEFORE", $tmpMaxSize, $colNum, $target, $begin, $end) if $target >= 0;
            } else {
                die "UNKOWN METHOD $method\n";
            }
            $$currArrPos++;
            last if $once == 1;
        }
    }
}

sub updateVal  {
    my $name    = shift;
    my $maxSize = shift;
    my $colNum  = shift;
    my $target  = shift;
    my $begin   = shift;
    my $end     = shift;

    my $targetVirt = $target % $maxWindowBuffer;
    my $beginVirt  = $begin  % $maxWindowBuffer;
    my $endVirt    = $end    % $maxWindowBuffer;
    my $sumCol     = $numCols + 2;

    if ( $begin < 0 || $end < 0 ) {
        print STDERR "           SKIPPING. BEGIN < 0 OR END < 0\n" if $verbose;
        my $res = 0;
        $openWindow[$targetVirt][$sumCol] = \$res;
        return;
    }


    print STDERR "      $name   [ $begin { $beginVirt } .. $target { $targetVirt } .. $end { $endVirt }] (".scalar @openWindow.")\n" if $verbose;
    my $beginSum  = $openWindow[$beginVirt][$sumCol];
    my $endSum    = $openWindow[$endVirt  ][$sumCol];
    my $diffSum   = $$endSum - $$beginSum;
    my $avg       = $diffSum / $maxSize;
    
    if ( $avg < 0 ) {
        print STDERR "           SKIPPING. AVERAGE < 0 [($$endSum - $$beginSum) = $diffSum / $maxSize = $avg\n" if $verbose;
        return;
    }
    
    print STDERR "          BEGIN $$beginSum END $$endSum SUM SO FAR $diffSum TARGET $target MAX SIZE $maxSize AVG " . (sprintf("%4.2f", $avg)) . "\n" if $verbose;
    if ( ! defined ${$openWindow[$targetVirt]}[$colNum] )
    {
        $openWindow[$targetVirt][$colNum] = \$avg;
    } else {
        print STDERR "           SKIPPING. POSITION ALREADY DEFINED\n" if $verbose;
    }
}

