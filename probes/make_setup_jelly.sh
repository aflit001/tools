#!/bin/bash
#KMER
export JELLYMERLEN=17
export JELLYTHREADS=40
export JELLYTHREADS=`echo "$CORES * 0.8 / 1" | bc`
export JELLYHASHSIZE=10000000000
export JELLYLOWERCOUNT=-1
export JELLYUPPERCOUNT=1


#count
#    Count k-mers or qmers in fasta or fastq files
#    Usage: jellyfish count [OPTIONS]... [file.f[aq]]...
#        -h,- -help               Print help and exit
#        --full-help              Print help, including hidden options, and exit
#        -V,--version             Print version and exit
#        -m,--mer-len=INT         Length of mer (mandatory)
#        -s,--size=LONG           Hash size (mandatory)
#        -t,--threads=INT         Number of threads (default=1)
#        -o,--output=STRING       Output prefix (default=mer counts)
#        -c,--counter-len=Length  in bits Length of counting field (default=7)
#        --out-counter-len=Length in bytes Length of counter field in output (de-
#                                 fault=4)
#        -C,--both-strands        Count both strand, canonical representation
#                                (default=off)
#        -p,--reprobes=INT       Maximum number of reprobes (default=62)
#        -r,--raw                Write raw database (default=off)
#        -q,--quake              Quake compatibility mode (default=off)
#        - -quality-start=INT    Starting ASCII for quality values (default=64)
#        - -min-quality=INT      Minimum quality. A base with lesser quality
#                                becomes an N (default=0)
#        -L,--lower-count=LONG   Don’t output k-mer with count ¡ lower-count
#        -U,--upper-count=LONG   Don’t output k-mer with count ¿ upper-count
#        --matrix=Matrix         file Hash function binary matrix
#        --timing=Timing         file Print timing information
#
#
#histo
#    Create an histogram of k-mer occurrences
#    Usage: jellyfish histo [OPTIONS]... [database.jf]...
#        --help                  Print help and exit
#        -V,--version            Print version and exit
#        -s,--buffer-size=Buffer length Length in bytes of input buffer
#                                (default=10000000)
#        -l,--low=LONG           Low count value of histogram (default=1)
#        -h,--high=LONG          High count value of histogram (default=10000)
#        -i,--increment=LONG     Increment value for buckets (default=1)
#        -t,--threads=INT        Number of threads (default=1)
#        -o,--output=STRING      Output file (default=/dev/fd/1)
#
#dump
#    Dump k-mer counts
#    Usage: jellyfish stats [OPTIONS]... [database.jf]...
#        -h,--help               Print help and exit
#        -V,--version            Print version and exit
#        -c,--column             Column format (default=off)
#        -t,--tab                Tab separator (default=off)
#        -L,--lower-count=LONG   Don’t output k-mer with count ¡ lower-count
#        -U,--upper-count=LONG   Don’t output k-mer with count ¿ upper-count
#        -o,--output=STRING      Output file (default=/dev/fd/1)
#
#stats
#    Statistics
#    Usage: jellyfish stats [OPTIONS]... [database.jf]...
#        -h,--help               Print help and exit
#        --full-help             Print help, including hidden options, and exit
#        -V,--version            Print version and exit
#        -L,--lower-count=LONG   Don’t output k-mer with count ¡ lower-count
#        -U,--upper-count=LONG   Don’t output k-mer with count ¿ upper-count
#        -v,--verbose            Verbose (default=off)
#        -o,--output=STRING      Output file (default=/dev/fd/1)
#    
#merge
#    Merge jellyfish databases
#    Usage: jellyfish merge [OPTIONS]... [database.jf]...
#        -h,--help               Print help and exit
#        -V,--version            Print version and exit
#        -s,--buffer-size=Buffer length Length in bytes of input buffer
#                                (default=10000000)
#        -o,--output=STRING      Output file (default=mer counts merged.jf)
#        --out-counter-len=INT   Length (in bytes) of counting field in output
#                                (de-fault=4)
#        --out-buffer-size=LONG  Size of output buffer per thread
#                                (default=10000000)
#        -v,--verbose            Be verbose (default=off)
