#!/bin/bash

source make_setup.sh

######################################################
## FILTERING
######################################################
#DESIREDCHROMOSSOME=(02 12)
#CHROMOSSOMEEPITOPE=SL2.40ch
#cat src/S_lycopersicum_chromosomes_from_scaffolds.2.40.agp | grep SL2.40ch12

CHROMNAME=$1
SCRIPT_NAME=`basename ${BASH_SOURCE}`
DSC=" :: $CHROMNAME"

echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  CHROMOSSOME NAME   #"$CHROMNAME

DID=0



#EXTRA=(finished_bacs.all.gff3 finished_bacs.all.gff3.bacs.gff3 finished_bacs_repeatmasked.seq finished_bacs.seq ITAG2.3_assembly.gff3 ITAG2.3_gene_models.gff3 I$
#for f in $EXTRA; do
    #./gff2range.sh $f
#done


OUT="$AGPFOLDER/$MAPAGP.$CHROMNAME.agp"
export CHROMAGP=$OUT
CMD="grep $CHROMNAME $INFOLDER/$MAPAGP > $OUT"

echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    FILTERING AGP FOR CHROMOSSOME $CHROMNAME TO $OUT"

if [[ ! -s "$OUT" ]]; then
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      RUNNING '$CMD'"
    #twit.py starting filtering chromossome $CHROMNAME
    eval time "$CMD"  2>&1 | tee -a err.log
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CHROM $CHROMNAME :: CMD $CMD ***" 
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        DID=$((DID+1))
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        $OUT CREATED"
    fi
else
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      $OUT EXISTS. SKIPPING"
fi




OUT="$CHROMAGP.list"
export CHROMAGPLIST=$OUT
CMD="grep $COL2EPITOPE $CHROMAGP | gawk '{print \$6}' | sort > $OUT"

echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    GENERATING LIST FOR CHRMOSSOME $CHROMNAME TO $OUT"

if [[ ! -s "$OUT" ]]; then
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      RUNNING '$CMD'"
    eval time "$CMD" 2>&1 | tee -a err.log
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CHROM $CHROMNAME :: CMD $CMD ***" 
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        DID=$((DID+1))
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        $OUT CREATED"
    fi
else
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      $OUT EXISTS. SKIPPING"
fi




OUT="$MAPPINGOUT.$CHROMNAME.$MAPEXTENSION"
export MAPPINGOUT_CHROM=$OUT
CMD="grep -f $CHROMAGPLIST $MAPPINGOUT > $OUT"

echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    FILTERING MAP FOR CHROMOSSOME $CHROMNAME TO $OUT"

if [[ ! -s "$OUT" ]]; then
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      RUNNING '$CMD'"
    eval time "$CMD" 2>&1 | tee -a err.log
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CHROM $CHROMNAME :: CMD $CMD ***" 
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        DID=$((DID+1))
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        $OUT CREATED"
    fi
else
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      $OUT EXISTS. SKIPPING"
fi




OUT="$MAPN.$CHROMNAME.tab"
export MAPN_CHROM=$OUT
CMD="grep -f $CHROMAGPLIST $MAPN > $OUT"

echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    FILTERING Ns MAP FOR CHROMOSSOME $CHROMNAME TO $OUT"

if [[ ! -s "$OUT" ]]; then
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      RUNNING '$CMD'"
    eval time "$CMD" 2>&1 | tee -a err.log
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CHROM $CHROMNAME :: CMD $CMD ***" 
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        DID=$((DID+1))
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        $OUT CREATED"
    fi
else
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      $OUT EXISTS. SKIPPING"
fi



NUMAGP=`cat $CHROMAGPLIST | wc -l`
NUMAGPNOW=0
SCAFPIDS=()
PIDCHRNAMES=()
PIDSCANAMES=()
#
# CHECKING EACH SCAFFOLD
#
for SCA in `cat $CHROMAGPLIST`
do
    let NUMAGPNOW=NUMAGPNOW+1

#    if [[ $USEBOWTIE -eq "1" ]]; then
#        echoMsg  "    FILTERING SCAFFOLD $SCA [$NUMAGPNOW / $NUMAGP] :: ./make_scaf_bowtie.sh $CHROMNAME $SCA"
#        echoMsg  "      RUNNING CHROMOSOME $CHROMNAME SCAFFOLD $SCA UNDER BOWTIE"
##        ./make_scaf_bowtie.sh $CHROMNAME $SCA
#    fi
#    if [[ $USEBWA -eq "1" ]]; then
#        echoMsg  "    FILTERING SCAFFOLD $SCA [$NUMAGPNOW / $NUMAGP] :: ./make_scaf_bwa.sh $CHROMNAME $SCA"
#        echoMsg  "      RUNNING CHROMOSOME $CHROMNAME SCAFFOLD $SCA UNDER BWA"
#        ./make_scaf_bwa.sh $CHROMNAME $SCA
#        if [[ $? -ne "0" ]]; then
#            echoMsg  "      RUNNING CHROMOSOME $CHROMNAME SCAFFOLD $SCA UNDER BWA FAILED"
#            exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
#        fi
#    fi
    
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    FILTERING SCAFFOLD $SCA [$NUMAGPNOW / $NUMAGP] :: ./make_scaf.sh $CHROMNAME $SCA"
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      RUNNING CHROMOSOME $CHROMNAME SCAFFOLD $SCA PID $!"

    ./make_scaf.sh $CHROMNAME $SCA
    SCAFPIDS[$NUMAGPNOW]=$!
    PIDCHRNAMES[$!]=$CHROMNAME
    PIDSCANAMES[$!]=$SCA

    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " CHROM NAME $CHROMNAME SCAFFOLD NAME $SCANAME PID $! IS RUNNING"
done

LOOPCOUNT=0
WAITALL_DELAY=3
while [[ ! -z `jobs -p` ]]; do
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " LOOP $LOOPCOUNT"
    LOOPCOUNT=$((LOOPCOUNT+1))

    while :; do
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " Processes remaining: "`jobs -p`""

        for element in $(seq $((${#SCAFPIDS[@]} - 1)) -1 0); do
            pid=${SCAFPIDS[$element]}
            PCNAME=${PIDCHRNAMES[$pid]}
            PSNAME=${PIDSCANAMES[$pid]}

            echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  checking pid $pid ['$PCNAME' :: '$PSNAME' ($element)]"

            if [[ -z "$pid" ]]; then
                #echoMsg  "["`date`" :: $SECONDS :: $0 :: $LINENO]  *** PID $pid WAS NOT FOUND ***"
                continue
                #exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
            fi

            if [[ -z "$PCNAME" ]]; then
                echoMsg  "["`date`" :: $SECONDS :: $0 :: $LINENO]  *** PID $pid HAS NO CHROMOSOME NAME [$PCNAME] ***"
                exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
            fi

            if [[ -z "$PSNAME" ]]; then
                echoMsg  "["`date`" :: $SECONDS :: $0 :: $LINENO]  *** PID $pid HAS NO NAME SCAFFOLD [$PSNAME] ***"
                exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
            fi

            if kill -0 "$pid" 2>/dev/null; then
                echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    $pid [$PCNAME :: $PSNAME] is still alive"
            elif wait "$pid"; then
                SCAFPIDS=(${SCAFPIDS[@]:0:$element} ${SCAFPIDS[@]:$(($element + 1))})
                echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    $pid [$PCNAME :: $PSNAME] exited with zero exit status"
                echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      SUCCESS ANALYZING CHROMOSOME $PCNAME SCAFFOLD $PSNAME"
                continue
            else
                echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    *** $pid [$PCNAME :: $PSNAME] exited with non zero exit status ***"
                echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    ***  JOB $PCNAME :: $PSNAME HAS FAILED ***"
                echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    ***    ERROR ANALYZING CHROMOSOME $PCNAME SCAFFOLD $PSNAME ***"
                exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
            fi
        done

        JOBS=`jobs -p`
        if [[ -z "$JOBS" ]]; then
            echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      NO MORE JOBS. BREAKING"
            break
        fi
        # TODO: how to interrupt this sleep when a child terminates?
        sleep ${WAITALL_DELAY:-1}
    done
    sleep $WAITALL_DELAY
done


for sca in `cat $AGPFOLDER/$MAPAGP.$CHROMNAME.agp.list`
do
    if [[ ! -s "$OUTFOLDER/${INFASTA}_$DBNAME.$MAPEXTENSION.$CHROMNAME.$MAPEXTENSION.$sca.$MAPEXTENSION.cov.prop.cov.png" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR MAKING SCAFFOLD $sca OF CHROMOSSOME $CHROMNAME. COULD NOT FIND '$OUTFOLDER/${INFASTA}_$DBNAME.$MAPEXTENSION.$CHROMNAME.$MAPEXTENSION.$sca.$MAPEXTENSION.cov.prop.cov.png' ***"
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    SUCCESSFULLY ANALYZED SCAFFOLD $sca FROM CHROMOSSOME $CHROMNAME"
    fi
done


if [[ "$DID" -gt 0 ]]; then
    #twit.py finished filtering chromossome $CHROMNAME
    echoMsg  ""
fi

echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  FINISHED MAKING CHROMOSOME $CHROMNAME"

