#!/usr/bin/perl -w

use lib "/h/ablumer/Graph-0.91/lib";

use Graph::Directed;
my $g = Graph::Directed->new;

print "How many vertices? ";
chomp( $V = <STDIN> );
print "How many edges? ";
chomp( $E = <STDIN> );

for ($i=1; $i<=$V; $i++) {
  $g->add_vertex($i);
  $color[$i] = 'W';
  $parent[$i] = 0;
  $discover[$i] = 0;
  $finish[$i] = 0;
}
$numedges = 0;
while ( $numedges < $E ) {
  $i = 1 + int rand $V;
  $j = 1 + int rand $V;
  if (($i != $j) && (!$g->has_edge( $i, $j )) ) {
    $g->add_edge($i, $j);
    $numedges++;
  }
}

print "The graph is $g\n";

$time = 0;
for ($i=1; $i<=$V; $i++) {
  if ( $color[$i] eq 'W' ) {
    print "New root at ", $i, "\n";
    DFSVisit( $i );
  }
}

sub DFSVisit {
my($u) = $_[0];

$time++;
$discover[$u] = $time;
$color[$u] = 'G';
for ($v=1; $v<=$V; $v++) {
  if ($g->has_edge($u, $v) && ($color[$v] eq 'W')) {
    $parent[$v] = $u;
    print "Parent of ", $v, " is ", $u, "\n";
    DFSVisit( $v );
  }
}
$color[$u] = 'B';
$time++;
$finish[$u] = $time;
print "Label of ", $u, " is ", $discover[$u], " - ", $finish[$u], "\n";
}

