#!/bin/bash

source make_setup.sh

######################################################
## FILTERING
######################################################
#DESIREDCHROMOSSOME=(02 12)
#CHROMOSSOMEEPITOPE=SL2.40ch
#cat src/S_lycopersicum_chromosomes_from_scaffolds.2.40.agp | grep SL2.40ch12

CHROMNAME=$1
echo "  CHROMOSSOME NAME   #"$CHROMNAME

DID=0


OUT="${INFOLDER}/${BOWTIEDBFASTAFILE}_NONE.tab.$CHROMNAME.tab"
CMD="cat ${INFOLDER}/${BOWTIEDBFASTAFILE}_NONE.tab | \
	grep -f $MAPAGP.$CHROMNAME.agp.list \
	> $OUT"

echo "    FILTERING Ns MAP FOR CHROMOSSOME $CHROMNAME"
if [[ ! -s "$OUT" ]]; then
    echo "      RUNNING '$CMD'"
    eval time $CMD
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echo "ERROR CREATING $OUT :: CHROM $CHROMNAME :: CMD $CMD"
        echo "ERROR CREATING $OUT :: CHROM $CHROMNAME :: CMD $CMD" >> err.log
        rm ${OUT} &> /dev/null
        exit 1
    else
        DID=$((DID+1))
        echo "        $OUT CREATED"
    fi
else
    echo "      $OUT EXISTS. SKIPPING"
fi









OUT="$MAPAGP.$CHROMNAME.agp"
CMD="cat $INFOLDER/$MAPAGP | grep $CHROMNAME > $MAPAGP.$CHROMNAME.agp"
echo "    FILTERING AGP FOR CHROMOSSOME $CHROMNAME"
if [[ ! -s "$OUT" ]]; then
    echo "      RUNNING '$CMD'"
    #twit.py starting filtering chromossome $CHROMNAME
    eval time $CMD
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echo "ERROR CREATING $OUT :: CHROM $CHROMNAME :: CMD $CMD"
        echo "ERROR CREATING $OUT :: CHROM $CHROMNAME :: CMD $CMD" >> err.log
        rm ${OUT} &> /dev/null
        exit 1
    else
        DID=$((DID+1))
        echo "        $OUT CREATED"
    fi
else
    echo "      $OUT EXISTS. SKIPPING"
fi

OUT="$MAPAGP.$CHROMNAME.agp.list"
CMD="cat $MAPAGP.$CHROMNAME.agp | grep $COL2EPITOPE | gawk '{print \$6}' |  \
sort > $MAPAGP.$CHROMNAME.agp.list"
echo "    GENERATING LIST FOR CHRMOSSOME $CHROMNAME"
if [[ ! -s "$OUT" ]]; then
    echo "      RUNNING '$CMD'"
    eval time $CMD
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echo "ERROR CREATING $OUT :: CHROM $CHROMNAME :: CMD $CMD"
        echo "ERROR CREATING $OUT :: CHROM $CHROMNAME :: CMD $CMD" >> err.log
        rm ${OUT} &> /dev/null
        exit 1
    else
        DID=$((DID+1))
        echo "        $OUT CREATED"
    fi
else
    echo "      $OUT EXISTS. SKIPPING"
fi



OUT="${INFASTA}_$BOWTIEDBNAME.map.$CHROMNAME.map"
CMD="cat ${INFASTA}_$BOWTIEDBNAME.map | grep -f $MAPAGP.$CHROMNAME.agp.list \
 > $OUT"

echo "    FILTERING MAP FOR CHROMOSSOME $CHROMNAME"
if [[ ! -s "$OUT" ]]; then
    echo "      RUNNING '$CMD'"
    eval time $CMD
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echo "ERROR CREATING $OUT :: CHROM $CHROMNAME :: CMD $CMD"
        echo "ERROR CREATING $OUT :: CHROM $CHROMNAME :: CMD $CMD" >> err.log
        rm ${OUT} &> /dev/null
        exit 1
    else
        DID=$((DID+1))
        echo "        $OUT CREATED"
    fi
else
    echo "      $OUT EXISTS. SKIPPING"
fi



NUMAGP=`cat $MAPAGP.$CHROMNAME.agp.list | wc -l`
NUMAGPNOW=0
#
# CHECKING EACH SCAFFOLD
#
for sca in `cat $MAPAGP.$CHROMNAME.agp.list`
do
    NUMAGPNOW=$((NUMAPGNOW + 1))
    echo "    FILTERING SCAFFOLD $sca [$NUMAGPNOW / NUMAGP] :: ./make_scaf.sh $CHROMNAME $sca"

    ./make_scaf.sh $CHROMNAME $sca &
done

wait

for sca in `cat $MAPAGP.$CHROMNAME.agp.list`
do
    if [[ ! -s "${INFASTA}_$BOWTIEDBNAME.map.$CHROMNAME.map.$sca.map.cov.prop.png" ]]; then
        echo "ERROR MAKING SCAFFOLD $sca OF CHROMOSSOME $CHROMNAME"
        echo "ERROR MAKING SCAFFOLD $sca OF CHROMOSSOME $CHROMNAME" >> err.log

        exit 1
    else
        echo "    SUCCESSFULLY ANALYZED SCAFFOLD $sca FROM CHROMOSSOME $CHROMNAME"
    fi
done


if [[ "$DID" -gt 0 ]]; then
    #twit.py finished filtering chromossome $CHROMNAME
    echo ""
fi
