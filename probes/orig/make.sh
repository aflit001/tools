#!/bin/bash

source make_setup.sh



echo "JELLY THREADS $JELLYTHREADS"
echo "BOWTIE THREADS $BOWTIETHREADS"
rm err.log &>/dev/null
######################################################
## JELLYFISH
######################################################

OUT="${INFASTA}_mer_counts_0"
CMD="jellyfish count $INFOLDER/$INFASTA --mer-len=$JELLYMERLEN \
--threads=$JELLYTHREADS --output=${INFASTA}_mer_counts \
--both-strands --size=$JELLYHASHSIZE"
echo "RUNNING JELLY COUNT"
if [[ ! -s "$OUT" ]]; then
    echo "  RUNNING '$CMD'"
    eval time $CMD
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echo "ERROR CREATING $OUT :: CMD $CMD"
        echo "ERROR CREATING $OUT :: CMD $CMD" >> err.log
        rm ${INFASTA}_mer_counts* &> /dev/null
        exit 1
    else
        echo "    $OUT CREATED"
    fi
else
    echo "  $OUT EXISTS. SKIPPING"
fi


OUT="${INFASTA}.kmer.fa"
JU=" --upper-count=$JELLYUPPERCOUNT"

if [[ "$JELLYUPPERCOUNT" == -1 ]]; then
    JU=""
fi

#CMD="jellyfish dump --column --tab --lower-count=$JELLYLOWERCOUNT \
#--upper-count=$JELLYUPPERCOUNT ${INFASTA}_mer_counts* > $OUT"
CMD="jellyfish dump --lower-count=$JELLYLOWERCOUNT $JU \
${INFASTA}_mer_counts* > $OUT"
echo "RUNNING JELLY DUMP"
if [[ ! -s "$OUT" ]]; then
    echo "  RUNNING '$CMD'"
    eval time $CMD
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echo "ERROR CREATING $OUT :: CMD $CMD"
        echo "ERROR CREATING $OUT :: CMD $CMD" >> err.log
        rm $OUT &> /dev/null
        exit 1
    else
        echo "    $OUT CREATED"
    fi

    twit.py finished jellyfish ${INFASTA}.kmer.fa \| starting bowtie-build $BOWTIEDBFASTAFILE
else
    echo "  $OUT EXISTS. SKIPPING"
fi

######################################################
## BOWTIE
######################################################


OUT="$BOWTIEDBNAME.1.ebwt"
#CMD="bowtie-build --offrate 5 --ftabchars 10 \
# src/$BOWTIEDBFASTAFILE $BOWTIEDBNAME"

# mark bw each 2 ^ 5  = 32 rows (340 Mb for human genome)
#CMD="bowtie-build --offrate $BOWTIEOFFRATE --ftabchars \
# $BOWTIEFTABCHARS src/$BOWTIEDBFASTAFILE $BOWTIEDBNAME"

CMD="bowtie-build --offrate $BOWTIEOFFRATE  \
$INFOLDER/$BOWTIEDBFASTAFILE $BOWTIEDBNAME"
# mark bw each 2 ^ 1  = 2 rows
echo "RUNNING BOWTIE BUILD"
if [[ ! -s "$OUT" ]]; then
    echo "  RUNNING '$CMD'"
    eval time $CMD
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echo "ERROR CREATING $OUT :: CMD $CMD"
        echo "ERROR CREATING $OUT :: CMD $CMD" >> err.log
        rm ${BOWTIEDBNAME}* &> /dev/null
        exit 1
    else
        echo "    $OUT CREATED"
    fi
else
    echo "  $OUT EXISTS. SKIPPING"
fi






OUT="${INFASTA}_$BOWTIEDBNAME.map"
BT=" -m $BOWTIESUPPERSBIGGERTHAN"

if [[ "$BOWTIESUPPERSBIGGERTHAN" == -1 ]]; then
    BT=""
fi


CMD="bowtie --mm --offrate $BOWTIEOFFRATE --chunkmbs $BOWTIECHUNKMBS \
 $BT --time --threads $BOWTIETHREADS --seedlen $BOWTIESEEDLEN \
 --al ${INFASTA}_$BOWTIEDBNAME.map.all \
 --un ${INFASTA}_$BOWTIEDBNAME.map.unmapped \
 --max ${INFASTA}_$BOWTIEDBNAME.map.max \
 -f $BOWTIEDBNAME ${INFASTA}.kmer.fa > $OUT"
echo "RUNNING BOWTIE MAP"
if [[ ! -s "$OUT" ]]; then
    echo "  RUNNING '$CMD'"
    twit.py finished bowtie-build \| starting bowtie $OUT
    eval time $CMD
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echo "ERROR CREATING $OUT :: CMD $CMD"
        echo "ERROR CREATING $OUT :: CMD $CMD" >> err.log
        rm ${OUT} &> /dev/null
        exit 1
    else
        echo "    $OUT CREATED"
    fi
    twit.py finished bowtie \| starting filtering
else
    echo "  $OUT EXISTS. SKIPPING"
fi



#src/S_lycopersicum_scaffolds.fa_NONE.tab
OUT="${INFOLDER}/${BOWTIEDBFASTAFILE}_NONE.tab"
CMD="./mapN.pl ${INFOLDER}/${BOWTIEDBFASTAFILE}"
echo "RUNNING Ns MAP"
if [[ ! -s "$OUT" ]]; then
    echo "  RUNNING '$CMD'"
    #twit.py finished bowtie-build \| starting bowtie $OUT
    eval time $CMD
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echo "ERROR CREATING $OUT :: CMD $CMD"
        echo "ERROR CREATING $OUT :: CMD $CMD" >> err.log
        rm ${OUT} &> /dev/null
        exit 1
    else
        echo "    $OUT CREATED"
    fi
    #twit.py finished bowtie \| starting filtering
else
    echo "  $OUT EXISTS. SKIPPING"
fi




######################################################
## FILTERING
######################################################
#DESIREDCHROMOSSOME=(02 12)
#CHROMOSSOMEEPITOPE=SL2.40ch
#cat src/S_lycopersicum_chromosomes_from_scaffolds.2.40.agp | grep SL2.40ch12

for element in $(seq 0 $((${#DESIREDCHROMOSSOME[@]} - 1)))
do
    CHROMNUM=${DESIREDCHROMOSSOME[$element]}
    CHROMNAME=$SEQEPITOPE$COL1EPITOPE${DESIREDCHROMOSSOME[$element]}

    echo "  CHROMOSSOME POS    #$element"
    echo "  CHROMOSSOME NUMBER #"$CHROMNUM
    echo "  CHROMOSSOME NAME   #"$CHROMNAME
    echo "    RUNNING ./make_chrom.sh $CHROMNAME"

    ./make_chrom.sh $CHROMNAME &
done


wait

for element in $(seq 0 $((${#DESIREDCHROMOSSOME[@]} - 1)))
do
    if [[ ! -s "${INFASTA}_$BOWTIEDBNAME.map.$CHROMNAME.map" ]]; then
        echo "ERROR ANALYZING CHROMOSOME $CHROMNAME"
        echo "ERROR ANALYZING CHROMOSOME $CHROMNAME" >> err.log
        exit 1
    else
        echo "  SUCCESS ANALYZING CHROMOSOME $CHROMNAME"
    fi
done


echo #################################
echo ##########  ERRORS ##############
echo #################################

if [[ -f "err.log" ]]; then
    cat err.log 2>/dev/null
else
    echo "NO ERRORS REPORTED"
fi


#twit.py finished making probe
