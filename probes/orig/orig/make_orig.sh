#!/bin/bash

INFASTA=S_lycopersicum_chromosomes.fa
#INFASTA=S_lycopersicum_chromosomes.fa.small.fa
CORES=80
DESIREDCHROMOSSOME=(02 12)
SEQEPITOPE=SL2.40
COL1EPITOPE=ch
COL2EPITOPE=sc
MAPAGP=S_lycopersicum_chromosomes_from_scaffolds.2.40.agp

#KMER
JELLYMERLEN=21
JELLYTHREADS=60
JELLYTHREADS=`echo "$CORES * 0.8 / 1" | bc`
JELLYHASHSIZE=100000000000
JELLYLOWERCOUNT=1
JELLYUPPERCOUNT=-1

#MAPPING
BOWTIEDBFASTAFILE=S_lycopersicum_scaffolds.fa
#BOWTIEDBFASTAFILE=S_lycopersicum_scaffolds.fa.small.fa
BOWTIEDBNAME=S_lycopersicum_scaffolds
BOWTIECHUNKMBS=1024
BOWTIESUPPERSBIGGERTHAN=1
#suppress all alignments if > <int> exist (def: no limit)
BOWTIETHREADS=60
BOWTIETHREADS=`echo "$CORES * 0.8 / 1" | bc`
BOWTIESEEDLEN=15
# mark bw each 2 ^ 1  = 2 rows
BOWTIEOFFRATE=1



echo "JELLY THREADS $JELLYTHREADS"
echo "BOWTIE THREADS $BOWTIETHREADS"


######################################################
## JELLYFISH
######################################################

OUT="${INFASTA}_mer_counts_0"
CMD="jellyfish count src/$INFASTA --mer-len=$JELLYMERLEN --threads=$JELLYTHREADS \
--output=${INFASTA}_mer_counts --both-strands --size=$JELLYHASHSIZE"
echo "RUNNING JELLY COUNT"
if [[ ! -s "$OUT" ]]; then
    echo "RUNNING '$CMD'"
    eval time $CMD
    if [[ ! -s "$OUT" ]]; then
        echo "ERROR CREATING $OUT"
        rm ${INFASTA}_mer_counts* &> /dev/null
        exit 1
    else
        echo "$OUT CREATED"
    fi
else
    echo "$OUT EXISTS. SKIPPING"
fi


OUT="${INFASTA}.kmer.fa"
JU=" --upper-count=$JELLYUPPERCOUNT"

if [[ "$JELLYUPPERCOUNT" == -1 ]]; then
    JU=""
fi

#CMD="jellyfish dump --column --tab --lower-count=$JELLYLOWERCOUNT \
#--upper-count=$JELLYUPPERCOUNT ${INFASTA}_mer_counts* > $OUT"
CMD="jellyfish dump --lower-count=$JELLYLOWERCOUNT $JU \
${INFASTA}_mer_counts* > $OUT"
echo "RUNNING JELLY DUMP"
if [[ ! -s "$OUT" ]]; then
    echo "RUNNING '$CMD'"
    eval time $CMD
    if [[ ! -s "$OUT" ]]; then
        echo "ERROR CREATING $OUT"
        rm $OUT &> /dev/null
        exit 1
    else
        echo "$OUT CREATED"
    fi

    twit.py finished jellyfish ${INFASTA}.kmer.fa \| starting bowtie-build $BOWTIEDBFASTAFILE
else
    echo "$OUT EXISTS. SKIPPING"
fi

######################################################
## BOWTIE
######################################################


OUT="$BOWTIEDBNAME.1.ebwt"
#CMD="bowtie-build --offrate 5 --ftabchars 10 \
# src/$BOWTIEDBFASTAFILE $BOWTIEDBNAME"

# mark bw each 2 ^ 5  = 32 rows (340 Mb for human genome)
#CMD="bowtie-build --offrate $BOWTIEOFFRATE --ftabchars \
# $BOWTIEFTABCHARS src/$BOWTIEDBFASTAFILE $BOWTIEDBNAME"

CMD="bowtie-build --offrate $BOWTIEOFFRATE  \
src/$BOWTIEDBFASTAFILE $BOWTIEDBNAME"
# mark bw each 2 ^ 1  = 2 rows
echo "RUNNING BOWTIE BUILD"
if [[ ! -s "$OUT" ]]; then
    echo "RUNNING '$CMD'"
    eval time $CMD
    if [[ ! -s "$OUT" ]]; then
        echo "ERROR CREATING $OUT"
        rm ${BOWTIEDBNAME}* &> /dev/null
        exit 1
    else
        echo "$OUT CREATED"
    fi
else
    echo "$OUT EXISTS. SKIPPING"
fi






OUT="${INFASTA}_$BOWTIEDBNAME.map"
BT=" -m $BOWTIESUPPERSBIGGERTHAN"

if [[ "$BOWTIESUPPERSBIGGERTHAN" == -1 ]]; then
    BT=""
fi


CMD="bowtie --mm --offrate $BOWTIEOFFRATE --chunkmbs $BOWTIECHUNKMBS \
 $BT --time --threads $BOWTIETHREADS --seedlen $BOWTIESEEDLEN \
 --al ${INFASTA}_$BOWTIEDBNAME.map.all \
 --un ${INFASTA}_$BOWTIEDBNAME.map.unmapped \
 --max ${INFASTA}_$BOWTIEDBNAME.map.max \
 -f $BOWTIEDBNAME ${INFASTA}.kmer.fa > $OUT"
echo "RUNNING BOWTIE MAP"
if [[ ! -s "$OUT" ]]; then
    echo "RUNNING '$CMD'"
    twit.py finished bowtie-build \| starting bowtie $OUT
    eval time $CMD
    if [[ ! -s "$OUT" ]]; then
        echo "ERROR CREATING $OUT"
        rm ${OUT} &> /dev/null
        exit 1
    else
        echo "$OUT CREATED"
    fi
    twit.py finished bowtie \| starting filtering
else
    echo "$OUT EXISTS. SKIPPING"
fi





######################################################
## FILTERING
######################################################
#DESIREDCHROMOSSOME=(02 12)
#CHROMOSSOMEEPITOPE=SL2.40ch
#cat src/S_lycopersicum_chromosomes_from_scaffolds.2.40.agp | grep SL2.40ch12

for element in $(seq 0 $((${#DESIREDCHROMOSSOME[@]} - 1)))
do
    CHROMNUM=${DESIREDCHROMOSSOME[$element]}
    CHROMNAME=$SEQEPITOPE$COL1EPITOPE${DESIREDCHROMOSSOME[$element]}

    echo "CHROMOSSOME POS    #$element"
    echo "CHROMOSSOME NUMBER #"$CHROMNUM
    echo "CHROMOSSOME NAME   #"$CHROMNAME


    OUT="$MAPAGP.$CHROMNAME.agp"
    CMD="cat src/$MAPAGP | grep $CHROMNAME > $MAPAGP.$CHROMNAME.agp"
    echo "  FILTERING AGP FOR CHROMOSSOME $CHROMNAME"
    if [[ ! -s "$OUT" ]]; then
        echo "    RUNNING '$CMD'"
        eval time $CMD
        if [[ ! -s "$OUT" ]]; then
            echo "ERROR CREATING $OUT"
            rm ${OUT} &> /dev/null
            exit 1
        else
            echo "      $OUT CREATED"
        fi
        twit.py starting filtering chromossome $CHROMNAME
    else
        echo "      $OUT EXISTS. SKIPPING"
    fi


    OUT="$MAPAGP.$CHROMNAME.agp.list"
    CMD="cat $MAPAGP.$CHROMNAME.agp | grep $COL2EPITOPE | gawk '{print \$6}' |  \
    sort > $MAPAGP.$CHROMNAME.agp.list"
    echo "  GENERATING LIST FOR CHRMOSSOME $CHROMNAME"
    if [[ ! -s "$OUT" ]]; then
        echo "    RUNNING '$CMD'"
        eval time $CMD
        if [[ ! -s "$OUT" ]]; then
            echo "ERROR CREATING $OUT"
            rm ${OUT} &> /dev/null
            exit 1
        else
            echo "      $OUT CREATED"
        fi
    else
        echo "       $OUT EXISTS. SKIPPING"
    fi



    OUT="${INFASTA}_$BOWTIEDBNAME.map.$CHROMNAME.map"
    CMD="cat ${INFASTA}_$BOWTIEDBNAME.map | grep -f $MAPAGP.$CHROMNAME.agp.list \
     > $OUT"

    echo "  FILTERING MAP FOR CHROMOSSOME $CHROMNAME"
    if [[ ! -s "$OUT" ]]; then
        echo "    RUNNING '$CMD'"
        eval time $CMD
        if [[ ! -s "$OUT" ]]; then
            echo "ERROR CREATING $OUT"
            rm ${OUT} &> /dev/null
            exit 1
        else
            echo "      $OUT CREATED"
        fi
    else
        echo "      $OUT EXISTS. SKIPPING"
    fi

    NUMAGP=`cat $MAPAGP.$CHROMNAME.agp.list | wc -l`
    NUMAGPNOW=0
    #
    # CHECKING EACH SCAFFOLD
    #
    for sca in `cat $MAPAGP.$CHROMNAME.agp.list`
    do
        NUMAGPNOW=$((NUMAPGNOW + 1))
        echo "    FILTERING SCAFFOLD $sca [$NUMAGPNOW / NUMAGP]"

        OUT="${INFASTA}_$BOWTIEDBNAME.map.$CHROMNAME.map.$sca.map"
        CMD="cat ${INFASTA}_$BOWTIEDBNAME.map.$CHROMNAME.map | grep $sca \
            > $OUT"
        echo "    GENERATING MAP FOR CHROMOSSOME $CHROMNAME SCAFFOLD $sca"

        if [[ ! -s "$OUT" ]]; then
            echo "      RUNNING '$CMD'"
            eval time $CMD
            if [[ ! -s "$OUT" ]]; then
                echo "ERROR CREATING $OUT"
                rm ${OUT} &> /dev/null
                exit 1
            else
                twit.py starting filtering chromossome $CHROMNAME scaffold $sca
                echo "        $OUT CREATED"
            fi
        else
            echo "        $OUT EXISTS. SKIPPING"
        fi



        OUT="${INFASTA}_$BOWTIEDBNAME.map.$CHROMNAME.map.$sca.map.cov"
        CMD="cat ${INFASTA}_$BOWTIEDBNAME.map.$CHROMNAME.map.$sca.map | perl -nae 'chomp; \
            \$size = '$JELLYMERLEN';   \$begin = 0;                                       \
            \$end  = 0; if ( \$F[1] eq \"+\" )                                            \
            { \$begin = \$F[3];          \$end   = \$F[3] + \$size; } else                \
            { \$begin = \$F[3] - \$size; \$end   = \$F[3];          };                    \
            print \"\$begin\t\$end\t\$F[2]\t\$F[1]\t\$size\n\"'                           \
            | ./range > $OUT"


        CMD="cat ${INFASTA}_$BOWTIEDBNAME.map.$CHROMNAME.map.$sca.map | perl -nae 'chomp; \
            \$size = '$JELLYMERLEN';   \$begin = \$F[3]; \$end = \$F[3] + \$size - 1;     \
            print \"\$begin\t\$end\t\$F[2]\t\$F[1]\t\$size\n\"'                           \
            | ./range > $OUT"


        echo "    GENERATING RANGES FOR CHROMOSSOME $CHROMNAME SCAFFOLD $sca"
        if [[ ! -s "$OUT" ]]; then
            echo "      RUNNING '$CMD'"
            eval time $CMD
            if [[ ! -s "$OUT" ]]; then
                echo "ERROR CREATING $OUT"
                rm ${OUT} &> /dev/null
                exit 1
            else
                echo "        $OUT CREATED"
            fi
        else
            echo "        $OUT EXISTS. SKIPPING"
        fi


        OUT="${INFASTA}_$BOWTIEDBNAME.map.$CHROMNAME.map.$sca.map.png"
        CMD="./mkplot.pl ${INFASTA}_$BOWTIEDBNAME.map.$CHROMNAME.map.$sca.map.cov"

        echo "    GENERATING GRAPHICS FOR CHROMOSSOME $CHROMNAME SCAFFOLD $sca"
        if [[ ! -s "$OUT" ]]; then
            echo "      RUNNING '$CMD'"
            eval time $CMD
            if [[ ! -s "$OUT" ]]; then
                echo "ERROR CREATING $OUT"
                rm ${OUT} &> /dev/null
                exit 1
            else
                echo "        $OUT CREATED"
            fi
        else
            echo "        $OUT EXISTS. SKIPPING"
        fi


        OUT="${INFASTA}_$BOWTIEDBNAME.map.$CHROMNAME.map.$sca.map.cov.prop.cov"
        CMD="cat ${INFASTA}_$BOWTIEDBNAME.map.$CHROMNAME.map.$sca.map.cov | perl -nae 'chomp; \
             \$size = '$JELLYMERLEN'; \$pos = \$F[0]; \$prop = \$F[1] / \$size;     \
             print \"\$pos\t\$prop\n\"' > $OUT"

        echo "    GENERATING RANGES PROPORTIONAL FOR CHROMOSSOME $CHROMNAME SCAFFOLD $sca"
        if [[ ! -s "$OUT" ]]; then
            echo "      RUNNING '$CMD'"
            eval time $CMD
            if [[ ! -s "$OUT" ]]; then
                echo "ERROR CREATING $OUT"
                rm ${OUT} &> /dev/null
                exit 1
            else
                echo "        $OUT CREATED"
            fi
        else
            echo "        $OUT EXISTS. SKIPPING"
        fi



        OUT="${INFASTA}_$BOWTIEDBNAME.map.$CHROMNAME.map.$sca.map.cov.prop.png"
        CMD="./mkplot.pl ${INFASTA}_$BOWTIEDBNAME.map.$CHROMNAME.map.$sca.map.cov.prop.cov"

        echo "    GENERATING GRAPHICS PROPORTIONAL FOR CHROMOSSOME $CHROMNAME SCAFFOLD $sca"
        if [[ ! -s "$OUT" ]]; then
            echo "      RUNNING '$CMD'"
            eval time $CMD
            if [[ ! -s "$OUT" ]]; then
                echo "ERROR CREATING $OUT"
                rm ${OUT} &> /dev/null
                exit 1
            else
                echo "        $OUT CREATED"
            fi

            #twit.py finished filtering chromossome $CHROMNAME
        else
            echo "        $OUT EXISTS. SKIPPING"
        fi
    done
done

#twit.py finished making probe
