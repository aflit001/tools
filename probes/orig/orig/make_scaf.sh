#!/bin/bash

source make_setup.sh

#
# CHECKING EACH SCAFFOLD
#
CHROMNAME=$1
SCAF=$2
DID=0

SCAFSIZE=`cat ${INFOLDER}/${BOWTIEDBFASTAFILE}.idx | grep $SCAF | perl -ane 'print $F[1]'`
echo "      SCAFFOLD SIZE $SCAFSIZE"



OUT="${INFASTA}_$BOWTIEDBNAME.map.$CHROMNAME.map.$SCAF.map"
CMD="cat ${INFASTA}_$BOWTIEDBNAME.map.$CHROMNAME.map | grep $SCAF \
    > $OUT"
echo "      GENERATING MAP FOR CHROMOSSOME $CHROMNAME SCAFFOLD $SCAF"

if [[ ! -s "$OUT" ]]; then
    echo "        RUNNING '$CMD'"
    #twit.py starting filtering chromossome $CHROMNAME scaffold $SCAF
    eval time $CMD
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echo "ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD"
        echo "ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD" >> err.log
        rm ${OUT} &> /dev/null
        exit 1
    else
        DID=$((DID+1))
        echo "          $OUT CREATED"
    fi
else
    echo "        $OUT EXISTS. SKIPPING"
fi


OUT="${INFOLDER}/${BOWTIEDBFASTAFILE}_NONE.tab.$CHROMNAME.tab.$SCAF.tab"
CMD="cat ${INFOLDER}/${BOWTIEDBFASTAFILE}_NONE.tab.$CHROMNAME.tab | grep "$SCAF" > $OUT"
echo "      GENERATING Ns MAP FOR CHROMOSSOME $CHROMNAME SCAFFOLD $SCAF"
if [[ ! -s "$OUT" ]]; then
    echo "        RUNNING '$CMD'"
    #twit.py starting filtering chromossome $CHROMNAME scaffold $SCAF
    eval time $CMD
    wait ${!}
    if [[ ! -f "$OUT" ]]; then
        echo "ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD"
        echo "ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD" >> err.log
        rm ${OUT} &> /dev/null
        exit 1
    else
        DID=$((DID+1))
        echo "          $OUT CREATED"
    fi
else
    echo "        $OUT EXISTS. SKIPPING"
fi


IN=$OUT
OUT="${IN}.cov"
CMD="cat ${IN} | perl -ane '\$s=\$F[2]; \$e=\$F[3]; print \
	( \$s > \$e ? \"\$e\t\$s\n\" : \"\$s\t\$e\n\")' | ./range ${SCAFSIZE} > $OUT"
echo "      GENERATING Ns COVERAGE FOR CHROMOSSOME $CHROMNAME SCAFFOLD $SCAF"
if [[ ! -s "$OUT" ]]; then
    echo "        RUNNING '$CMD'"
    #twit.py starting filtering chromossome $CHROMNAME scaffold $SCAF
    eval time $CMD
    wait ${!}
    if [[ ! -f "$OUT" ]]; then
        echo "ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD"
        echo "ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD" >> err.log
        rm ${OUT} &> /dev/null
        exit 1
    else
        DID=$((DID+1))
        echo "          $OUT CREATED"
    fi
else
    echo "        $OUT EXISTS. SKIPPING"
fi





OUT="${CONTIGMAP}.${SCAF}.agp.contig.agp"
OUT2="${CONTIGMAP}.${SCAF}.agp.gap.agp"
OUT3="${CONTIGMAP}.${SCAF}.agp.unknown.agp"
CMD="cat ${INFOLDER}/${CONTIGMAP} | grep \"$SCAF\" | tee \
>(perl -ane 'print if \$F[4] eq \"W\"' > $OUT)           \
>(perl -ane 'print if \$F[4] eq \"N\"' > $OUT2)          \
>(perl -ane 'print if \$F[4] eq \"U\"' > $OUT3) >/dev/null"

echo "      FILTERING CONTIGS FOR $CHROMNAME SCAFFOLD $SCAF"

if [[ ! -s "$OUT" ]]; then
    echo "        RUNNING '$CMD'"
    #twit.py starting filtering chromossome $CHROMNAME scaffold $SCAF
    eval time $CMD
    wait ${!}
    if [[ ! -f "$OUT" ]]; then
        echo "ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD"
        echo "ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD" >> err.log
        rm ${OUT}  &> /dev/null
		rm ${OUT2} &> /dev/null
		rm ${OUT3} &> /dev/null
        exit 1
    else
        DID=$((DID+1))
        echo "          $OUT CREATED"
    fi
else
    echo "        $OUT EXISTS. SKIPPING"
fi



IN=$OUT
OUT="${IN}.cov"
CMD="cat ${IN} | perl -ane '\$s=\$F[1]; \$e=\$F[2]; print \
	( \$s > \$e ? \"\$e\t\$s\n\" : \"\$s\t\$e\n\")' | ./range  ${SCAFSIZE} > $OUT"
echo "    CONVERTING CONTIGS FROM AGP ($IN) TO COV ($OUT) FROM $CHROMNAME SCAFFOLD $SCAF"

if [[ ! -s "$OUT" ]]; then
    echo "      RUNNING '$CMD'"
    #twit.py starting filtering chromossome $CHROMNAME scaffold $SCAF
    eval time $CMD
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echo "ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD"
        echo "ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD" >> err.log
        rm ${OUT} &> /dev/null
        exit 1
    else
        DID=$((DID+1))
        echo "        $OUT CREATED"
    fi
else
    echo "        $OUT EXISTS. SKIPPING"
fi


IN=$OUT2
OUT="${IN}.cov"
CMD="cat ${IN} | perl -ane '\$s=\$F[1]; \$e=\$F[2]; print \
	( \$s > \$e ? \"\$e\t\$s\n\" : \"\$s\t\$e\n\")' | ./range  ${SCAFSIZE} g > $OUT"
echo "      CONVERTING GAPS FROM AGP ($IN) TO COV ($OUT) FROM $CHROMNAME SCAFFOLD $SCAF"

if [[ ! -s "$OUT" ]]; then
    echo "        RUNNING '$CMD'"
    #twit.py starting filtering chromossome $CHROMNAME scaffold $SCAF
    eval time $CMD
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echo "ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD"
        echo "ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD" >> err.log
        rm ${OUT} &> /dev/null
        exit 1
    else
        DID=$((DID+1))
        echo "          $OUT CREATED"
    fi
else
    echo "        $OUT EXISTS. SKIPPING"
fi




IN=$OUT3
OUT="${IN}.cov"
CMD="cat ${IN} | perl -ane '\$s=\$F[1]; \$e=\$F[2]; print \
	( \$s > \$e ? \"\$e\t\$s\n\" : \"\$s\t\$e\n\")' | ./range  ${SCAFSIZE} > $OUT"
echo "      CONVERTING UNKNOWN GAPS FROM AGP ($IN) TO COV ($OUT) FROM $CHROMNAME SCAFFOLD $SCAF"

if [[ ! -s "$OUT" ]]; then
    echo "        RUNNING '$CMD'"
    #twit.py starting filtering chromossome $CHROMNAME scaffold $SCAF
    eval time $CMD
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echo "ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD"
        echo "ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD" >> err.log
        rm ${OUT} &> /dev/null
        exit 1
    else
        DID=$((DID+1))
        echo "          $OUT CREATED"
    fi
else
    echo "        $OUT EXISTS. SKIPPING"
fi





OUT="${INFASTA}_$BOWTIEDBNAME.map.$CHROMNAME.map.$SCAF.map.cov"
CMD="cat ${INFASTA}_$BOWTIEDBNAME.map.$CHROMNAME.map.$SCAF.map | perl -nae 'chomp; \
    \$size = '$JELLYMERLEN';   \$begin = \$F[3]; \$end = \$F[3] + \$size - 1;     \
    print \"\$begin\t\$end\t\$F[2]\t\$F[1]\t\$size\n\"'                           \
    | ./range  ${SCAFSIZE} > $OUT"


echo "      GENERATING RANGES FOR CHROMOSSOME $CHROMNAME SCAFFOLD $SCAF"
if [[ ! -s "$OUT" ]]; then
    echo "        RUNNING '$CMD'"
    eval time $CMD
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echo "ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD"
        echo "ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD" >> err.log
        rm ${OUT} &> /dev/null
        exit 1
    else
        DID=$((DID+1))
        echo "          $OUT CREATED"
    fi
else
    echo "        $OUT EXISTS. SKIPPING"
fi




IN=$OUT
OUT="${INFASTA}_$BOWTIEDBNAME.map.$CHROMNAME.map.$SCAF.map.cov.prop.cov"
CMD="cat $IN | grep -v '#' | perl -nae 'chomp; \$size = '$JELLYMERLEN'; \
     \$pos = \$F[0]; \$prop = \$F[1] / \$size;     \
     print \"\$pos\t\$prop\n\"' | ./add_smooth_colums.pl > $OUT"

echo "      GENERATING RANGES PROPORTIONAL FOR CHROMOSSOME $CHROMNAME SCAFFOLD $SCAF"
if [[ ! -s "$OUT" ]]; then
    echo "        RUNNING '$CMD'"
    eval time $CMD
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echo "ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD"
        echo "ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD" >> err.log
        rm ${OUT} &> /dev/null
        exit 1
    else
        DID=$((DID+1))
        echo "          $OUT CREATED"
    fi
else
    echo "        $OUT EXISTS. SKIPPING"
fi



IN=$OUT
OUT="${INFASTA}_$BOWTIEDBNAME.map.$CHROMNAME.map.$SCAF.map.cov.prop.png"
CMD="./mkplot_2.pl --input=${IN} \
--contig=${CONTIGMAP}.${SCAF}.agp.contig.agp.cov  \
--contig=${CONTIGMAP}.${SCAF}.agp.gap.agp.cov     \
--contig=${CONTIGMAP}.${SCAF}.agp.unknown.agp.cov \
--contig=${INFOLDER}/${BOWTIEDBFASTAFILE}_NONE.tab.$CHROMNAME.tab.$SCAF.tab.cov"

echo "      GENERATING GRAPHICS PROPORTIONAL FOR CHROMOSSOME $CHROMNAME SCAFFOLD $SCAF"
if [[ ! -s "$OUT" ]]; then
    echo "        RUNNING '$CMD'"
    eval time $CMD
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echo "ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD"
        echo "ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD" >> err.log
        rm ${OUT} &> /dev/null
        exit 1
    else
        DID=$((DID+1))
        echo "          $OUT CREATED"
    fi
else
    echo "        $OUT EXISTS. SKIPPING"
fi



#nano S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.map.SL2.40ch12.map.SL2.40sc06147.map.cov.smooth.plot
#gnuplot S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.map.SL2.40ch12.map.SL2.40sc06147.map.cov.smooth.plot
#cat S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.map.SL2.40ch12.map.SL2.40sc06147.map.cov | ./add_smooth_colums.pl > S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.map.SL2.40ch12.map.SL2.40sc06147.map.cov.smooth.cov



if [[ "$DID" -gt 0 ]]; then
    echo "finished filtering chromossome $CHROMNAME scaffold $SCAF"
    #twit.py finished filtering chromossome $CHROMNAME scaffold $SCAF
fi


