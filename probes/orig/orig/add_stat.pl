#!/usr/bin/perl -w

my $inName  = shift(@ARGV);
my $outName = &mkName($inName);
my @stats;
my @colNames;
my $count = 0;

&readFile($inName);
&genRes();

sub mkName
{
  my $inFile = $_[0];
  print "ANALIZING $inFile\n";

  die if ! defined $inFile;
  die if ! -f $inFile;
  die if ! -s $inFile;

  my $outFile   = $inFile;
     $outFile   =~ s/\.cov$/.stat/;

  my $fileSize = `wc -l $inFile`;

  if ($fileSize =~ /^(\d+)/)
  {
    $fileSize = $1;
  } else {
    die;
  }

  return $outFile;
}

sub getEmptyStat
{
    return [    "max"   => 0, 
                "min"   => 99_999_999_999_999,
                "sum"   => 0,
                "avg"   => -99_999_999_999_999,
                "mean2" => 0 ];
}


sub readFile
{
  my $inFile  = shift;
  my $numCols = 0;

  open IN, "<$inFile" or die;
  while (my $line = <IN>)
  {
    ++$count;
    chomp $line;
    my @cols = split("\t", $line);
    if ( substr($line, 0, 1) eq "#")
    {
      if ($numCols == 0 && @colNames == 0)
      {
        @colNames    = @cols;
        $colNames[0] = substr($colNames[0], 1);
        $numCols     = scalar @cols;
        #print "cols ", scalar @cols, "\n";

        for ( my $s = 1; $s < $numCols; $s++)
        {
            push(@stats, &getEmptyStat());
        }

        die "LESS THAN 2 COLUMNS" if $numCols < 2;
        die "HAS TITLE BUT TITLE HAS DIFFERENT LENGTH" if @colNames < $numCols;
      }
    } else {
      if ($numCols == 0)
      {
        $numCols = scalar @cols;
        die "LESS THAN 2 COLUMNS" if $numCols < 2;
      }
        print "numcols ", scalar @numCols, "\n";
      for ( my $s = 1; $s < @numCols; $s++ )
      { 
        my $stat = $stats[$s];     
        my $val  = $cols[$s];
      
        print "stat for col $s\n", $stat, "\n";
        if ( $stat->{"avg"} == -99_999_999_999_999 )
        {
            print "first avg for col $s is $val\n";
            $stat->{"avg"} = $val;
        }
  
        #print "POS $1 COV 2 $2\n";
        $stat->{"maxCov"}    = $val if ( $val > $stat->{"maxCov"} );
        $stat->{"minCov"}    = $val if ( $val < $stat->{"minCov"} );
        $stat->{"sumCov"}   += $val;
        my $delta  = $val - $stat->{"avg"};           # DIFF FROM AVERAGE
        $stat->{"avg"}      += $delta / ($count > 1 ? $count - 1 : 1 );
        $stat->{"mean2"}    += $delta * ( $val - $stat->{"avg"}); # SQUARE MEAN
        #if ( $2 <= $minGapCov ) { $gaps{$inFile}->insert($1); }; # print "ADDING \"$inFile\" \"$line\" \"$1\"\n";
      }
    }
  }
  close IN;
}

sub genRes
{
  for ( my $s = 1; $s < @numCols; $s++ )
  {
    my $stat      = \$stats[$s];
       $stat->{"var"}       = $stat->{"mean2"} / $count;
       $stat->{"stdDev"}    = sqrt($stat->{"var"});
       $stat->{"sdProp"}    = ($stat->{"stdDev"} / ($stat->{"avg"} || 1)) * 100 || 0;
       $stat->{"sdMax"}     = $stat->{"sdProp"} > 100 ? $stat->{"avg"} + ( 2*$stat->{"stdDev"} ) : $stat->{"avg"} + ( 3*$stat->{"stdDev"} );
       $stat->{"oldMaxCov"} = $stat->{"maxCov"};
    if ($stat->{"sdMax"} > ( 3 * $stat->{"avg"} )) { $stat->{"sdMax"} = (3 * $stat->{"avg"}) };

    print "SD          " . $stat->{"stdDev"}    . "\n";
    print "SD PROP     " . $stat->{"sdProp"}    . "\n";
    print "SD MAX      " . $stat->{"sdMax"}     . "\n";
    print "OLD MAX COV " . $stat->{"oldMaxCov"} . "\n";
    print "MAX COV     " . $stat->{"maxCov"}    . "\n";
    print "AVG COV     " . $stat->{"avg"}       . "\n";
    print "MIN COV     " . $stat->{"minCov"}    . "\n";

    $stat->{"maxCov"} += 1;
    $stat->{"maxCov"}  = int($stat->{"maxCov"});
    $stat->{"minCov"}  = int($stat->{"minCov"});

    print "MAX COV     " . $stat->{"maxCov"}     . "\n";
    print "MIN COV     " . $stat->{"minCov"}     . "\n";

  }
}




sub genPlotFile
{
  my $inFile        = $_[0];
  my $outPlotFile   = $_[1];
  my $outImgFile    = $_[2];
  my $title         = $_[3];
  my $xSize         = $_[4];
  my $ySize         = $_[5];
  my $avg           = $_[6];
  my $stdDev        = $_[7];
  my $max           = $_[8];
  my $min           = $_[9];
  my $propMainGraph = $_[10];
  my $propSecGraph  = $_[11];

  my $sdProp       = int(($stdDev / ($avg || 1)) * 100) || 0;
  my $avgPlusSdCov =     ($avg    + $stdDev)  || 0;
  my $avgMinSdCov  =     ($avg    - $stdDev)  || 0;
  my $split        = int( $ySize  / 2);


  print "    EXPORTING $inFile\n";
  print "    NUM COLS: $numCols\n";
  print "      TO $outPlotFile\n";
  print "      TO $outImgFile\n";

  open OUT, ">$outPlotFile" or die;


my $graphWidth   = 4096;
my $graphHeight  = $graphWidth * 1.6182339887;
#   $graphHeight *= $numCols - 1;


my $sizeProp     = ( 1 / ( $numCols      + 0.10 ) );
my $sizePropMain =         $sizeProp     + 0.03;
my $originMain   =   1 -   $sizePropMain - 0.02;

$title =~ s/\./\\./g;
$title =~ s/_/\\_/g;
my $numColNames  = scalar @colNames;
my $mainName     = 'Frequency';
   $mainName     = $colNames[1] if ( $numColNames > 0 && defined $colNames[1] && $colNames[1] ne "");

my $plot1 = "
set origin 0, $originMain
set size   1, $sizePropMain
plot '$inFile' using 1:2 lt rgb \"#".&getColor(0)."\" with filledcurves title '$mainName'
";

my $plot2;

my $nextOrigin = $originMain - $sizeProp - 0.005 - 0.005;

for ( my $i = 2; $i < $numCols; $i++ )
{
  my $colName = "Column " . ($i + 1);
  $colName = $colNames[$i] if ( $numColNames > 0 && defined $colNames[$i] && $colNames[$i] ne "");
  $plot2 .= "
set origin 0, $nextOrigin
set size   1, $sizeProp
plot '$inFile' using 1:" . ($i+1) . " lc rgb \"#".&getColor($i-1)."\" with filledcurves title '$colName bp'

  ";

  $nextOrigin = $nextOrigin - $sizeProp - 0.005;
}

print OUT <<CONF
set title  '$title'
set ylabel 'Coverage Depth'
set xlabel 'Position Reference'
set xrange [0:$xSize]
set yrange [0:$ySize]
set bars large

set label "AVG COV     : $avg"               at screen 0.05,0.995 front left
set label "STD DEV COV : $stdDev ($sdProp%)" at screen 0.05,0.990 front left
set label "MAX COV     : $max"               at screen 0.05,0.985 front left
set label "MIN COV     : $min"               at screen 0.05,0.980 front left
set label "LENG REF    : $xSize"             at screen 0.05,0.975 front left

set style fill transparent solid 1.0 noborder
set key inside left top vertical Left reverse enhanced autotitles nobox
set key noinvert samplen 1 spacing 1 width 0 height 0
set style data filledcurves y1=0 
unset colorbox

set grid
set palette model RGB
set pointsize 0.5
set ytics 
set xtics 
set mxtics 2
set mytics 2
#set format x "10^\%L"
set bmargin 0

#set terminal png enhanced size $graphHeight,$graphWidth large font "/usr/share/fonts/default/ghostscript/putr.pfa,16"
set terminal png enhanced size $graphWidth,$graphHeight large font "Courier" 14
set output "$outImgFile"

set multiplot

$plot1

unset label 1
unset label 2
unset label 3
unset label 4
unset label 5
unset title
unset xlabel
set ylabel '   '
$plot2

unset multiplot

exit

CONF
;




  close OUT;
  print "  EXPORTED\n";
}


sub getColor
{
  my $pos = shift;

  my @colors = (
"FF0000",
"FF7F00",
"FFFF00",
"7FFF00",
"00FF00",
"00FF7F",
"00FFFF",
"007FFF",
"0000FF",
"7F00FF",
"FF00FF",
"FF007F",
"000000",
"808080",
"005C5C",
"B22222"
);
  while ( $pos >= @colors )
  {
    $pos = $pos - @colors;
  }
  
  return $colors[$pos];
}
  

