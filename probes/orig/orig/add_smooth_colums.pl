#!/usr/bin/perl -w
use strict;


#print STDERR "MAX WINDOW : ", $maxWindow, "\n";

my $func  = \&appendCols;
my $func2 = \&readAhead;
my $func3 = \&readBehind;
my @cols  = (
    [$func,       500, "mid"],
    [$func,     2_500, "mid"],
    [$func,     5_000, "mid"],
    [$func,    10_000, "mid"],
    [$func,    50_000, "mid"],
    [$func,   100_000, "mid"],
    [$func,   500_000, "mid"],
    [$func, 1_000_000, "mid"],
    [$func ,    5_000, "begin"],
    [$func,     5_000, "end"],
);


my $maxWindow = 1;
my $c         = 0;
my $header;
for my $ob ( @cols )
{
    $header .= "\t" if defined $header;
    $header .= $ob->[1];
    $maxWindow = $ob->[1] if $ob->[1] > $maxWindow;
    $ob->[3] = $c++;
    $ob->[4] = 0;
}
print "#Position\tCoverage\t$header\n";

my @openWindow;

while ( my $line = <STDIN> )
{
    if ( $line =~ /(\d+)\s+(\d+(\.\d+)*)/)
    {
        my $pos   = $1;
        my $count = $2;
        #print "\n\n", $line;
        appendAndGet($pos, $count);
    }
}

#print "\n\nEMPTY STDIN\n\n";
&empty();

sub appendAndGet
{
    my $pos   = shift;
    my $count = shift;
    
    my @colVals = ($pos, $count);
    push(@openWindow, \@colVals);

    for my $col ( @cols )
    {
        my $avg = $col->[0]($col, 0);
    }
    
    &evaluate(0);
}


sub evaluate()
{
    my $crop = shift;
    
    if (( scalar @openWindow == $maxWindow ) || ( $crop ))
    {
        my $val = shift @openWindow;
        print join("\t" , @$val), " \n";
        #print "[cropping]\n";
    } else {
        #print "[OPENWINDOW TOO SMALL (",scalar @openWindow,")]\n";
    }
}

sub empty()
{
    while ( scalar @openWindow )
    {
        for my $col ( @cols )
        {
            my $avg = $col->[0]($col, 1);
        }
        
        &evaluate(1);
    }
}




sub appendCols
{
    my $data           = shift;
    my $crop           = shift;
    my $maxSize        = $data->[1];
    my $method         = $data->[2];
    my $colNum         = $data->[3];
    my $sumSoFar       = \$data->[4];
    my $openWindowSize = scalar @openWindow;
    my $max            = $maxSize;
    
    #print " APPENDING COLUMN: MAXSIZE $maxSize (#$colNum)\n";
    #print "  MAX             : ", $max, "\n";
    $max               = $openWindowSize if ( $openWindowSize < $maxSize );
    #print "  MAX             : ", $max, "\n";
    #print "  OPEN WINDOW SIZE: $openWindowSize\n";

    my $pos;
    if ( $method eq "mid" ) {
        $pos = int( ( $max ) / 2 );
    }
    elsif ( $method eq "begin" ) {
        $pos = 0;
    }
    elsif ( $method eq "end" ) {
        $pos = $max - 1;
    } else {
        die "ERROR IN CONFIGURATION";
    }

    my $avg = 0;
    my $sum = 0;
    
   
    if (( ! defined $openWindow[$pos] ) || ( ! defined ${$openWindow[$pos]}[$colNum + 2] ) || ( $openWindowSize < $maxSize ))
    {
        #METHOD 1
        if ( $openWindowSize < $maxSize )
        {
            my $valAdd  = $openWindow[$max - 1][1];
            my $valSub  = 0;

            if ( $crop )
            {
                $valAdd  = 0;
                $valSub  = $openWindow[0][1];
            }

            $$sumSoFar += $valAdd;
            $$sumSoFar -= $valSub;
            $avg        = $$sumSoFar / $max;
            #print "   OPEN < MAX && ", ($crop ? " crop " : " ! crop " ) , " : OPEN = $openWindowSize SUM = ", $$sumSoFar, " MAX = $max AVG = $avg (+$valAdd -$valSub)\n";

        } else {
            my $valAdd  = $openWindow[$max - 1][1];
            my $valSub  = $openWindow[0][1];
            $$sumSoFar += $valAdd;
            $$sumSoFar -= $valSub;
            $avg        = $$sumSoFar / $max;
            #print "   OPEN >  MAX: OPEN = $openWindowSize SUM = ", $$sumSoFar, " MAX = $max AVG = $avg (+$valAdd -$valSub)\n";
        }
    
        #METHOD 2
        #for ( my $p = 0; $p < $max; ++$p )
        #{
        #    $sum += $openWindow[$p][1];
        #}
        #$avg = $sum / $max;


        $openWindow[$pos][$colNum + 2] = $avg;
        #print "   POS $pos COL $colNum SUM $sum AVG $avg\n";
        #print "   POS $pos COL $colNum SUM " ,$$sumSoFar," AVG $avg\n";
    } else {
        #print "    POS $pos ALREADY DECIDED\n";
    }
}



