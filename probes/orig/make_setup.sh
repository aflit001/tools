#!/bin/bash

INFOLDER=data
INFASTA=S_lycopersicum_chromosomes.fa
#INFASTA=S_lycopersicum_chromosomes.fa.small.fa
CORES=80
DESIREDCHROMOSSOME=(02 12)
SEQEPITOPE=SL2.40
COL1EPITOPE=ch
COL2EPITOPE=sc
MAPAGP=S_lycopersicum_chromosomes_from_scaffolds.2.40.agp
CONTIGMAP=S_lycopersicum_scaffolds_from_contigs.2.40.agp

#KMER
JELLYMERLEN=21
JELLYTHREADS=60
JELLYTHREADS=`echo "$CORES * 0.8 / 1" | bc`
JELLYHASHSIZE=100000000000
JELLYLOWERCOUNT=1
JELLYUPPERCOUNT=-1

#MAPPING
BOWTIEDBFASTAFILE=S_lycopersicum_scaffolds.fa
#BOWTIEDBFASTAFILE=S_lycopersicum_scaffolds.fa.small.fa
BOWTIEDBNAME=S_lycopersicum_scaffolds
BOWTIECHUNKMBS=1024
BOWTIESUPPERSBIGGERTHAN=1
#suppress all alignments if > <int> exist (def: no limit)
BOWTIETHREADS=60
BOWTIETHREADS=`echo "$CORES * 0.8 / 1" | bc`
BOWTIESEEDLEN=15
# mark bw each 2 ^ 1  = 2 rows
BOWTIEOFFRATE=1



