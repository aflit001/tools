#!/usr/bin/perl -w
use strict;
use lib './';

use Algorithm::Dependency;
use Algorithm::Dependency::Ordered;
use Algorithm::Dependency::Source::File;
use Algorithm::Dependency::Source::String;

my $str         = 'Bin|Foo|Bar';
my $data_source = Algorithm::Dependency::Source::String->new($str);
#my $data_source = Algorithm::Dependency::Source::File->new('dep.txt');

my $dep = Algorithm::Dependency::Ordered->new(
      source   => $data_source,
#      selected => [ 'This', 'That' ]
      ) or die 'Failed to set up dependency algorithm';

query('Foo');
my $scheduleAll = $dep->schedule_all();
print "SCHEDULE ALL $scheduleAll\n";

sub query
{
    my $qry  = shift;
    my $also = $dep->depends( $qry );
    print $also
        ? "By selecting '$qry', you are also selecting the following items: "
                . join( ', ', @$also ) . "\n"
        : "Nothing else to select for '$qry'\n";

    my $schedule = $dep->schedule( $qry );
    print "SCHEDULE '$qry' ", $schedule, "\n";
    my $depends = $dep->depends($qry);
    print "DEPENDS  '$qry' ", $depends, "\n";
}

print "ok\n";


1;

