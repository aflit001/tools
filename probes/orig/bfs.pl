#!/usr/bin/perl -w

use lib "/h/ablumer/Graph-0.91/lib";

use Graph::Directed;
my $g = Graph::Directed->new;

print "How many vertices? ";
chomp( $V = <STDIN> );
print "How many edges? ";
chomp( $E = <STDIN> );

$g->add_vertex(1);
$color[1] = 'G';
$depth[1] = 0;
$parent[1] = 0;
for ($i=2; $i<=$V; $i++) {
  $g->add_vertex($i);
  $color[$i] = 'W';
  $depth[$i] = 2*$V;
  $parent[$i] = 0;
}
$numedges = 0;
while ( $numedges < $E ) {
  $i = 1 + int rand $V;
  $j = 1 + int rand $V;
  if (($i != $j) && (!$g->has_edge( $i, $j )) ) {
    $g->add_edge($i, $j);
    $numedges++;
  }
}

print "The graph is $g\n";

@queue = (1);
while (@queue > 0) {
  $u = shift @queue;
  for ($v=1; $v<=$V; $v++) {
    if ($g->has_edge($u,$v) && ($color[$v] eq 'W')) {
      $color[$v] = 'G';
      $depth[$v] = $depth[$u]+1;
      $parent[$v] = $u;
      push @queue, $v;
      print "Visited ", $v, " at depth ", $depth[$v],
         " parent is ", $u, "\n";
    }
  }
  $color[$u] = 'B';
}

