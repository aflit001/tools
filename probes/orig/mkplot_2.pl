#!/usr/bin/perl -w
#https://raw.github.com/sauloal/perlscripts/master/Bio/progs_parsers/mkplot.pl
use strict;
use Getopt::Long;

my $minimum        = 0;
my $fixBySd        = 1;  #fix output by 3xsd instead of maximum value
my $autoRun        = 1;  #execute gnuplot
my $mkPlot         = 1;  #read Files
my $mkIntermediary = 1;  #mk images
my $mkResume       = 0;  #mk resume graphic
my $correctResume  = 1;  #correct resume by 3x sd instead of maximum value
my $mkGaps         = 0;  #mk gaps output
my $minGapCov      = 20; #min gap coverage to merge in gap
my $resumeName     = 'resume';
my $transparent    = 0; #make background transparent or not
my $propMainGraph  = 0.85; #porportion between main graphic and overview

my $numCols        = 0;
my $transparentStr = '' ;
if ( $transparent ) { $transparentStr = 'transparent' };

my $propSecGraph   = 1 - $propMainGraph - .03;


#my $lineType = 'impulses';
my $lineType = 'lines';
  #dots
  #lines
  #steps
  #impulses
my $smooth = 'frequency';
  #unique
  #frequency
  #csplines
  #bezier
  #sbezier

my %resume;
my %gaps;
my @colNames;



sub usage()
{
    print STDERR <<EOF
    usage: $0 --input file.cov [--contig contig.cov]+ [--output out.png]* [--start 0]* [--end 100]*
		--help   |-h    : this (help) message
		--input  |-i    : input coverage file
		--output |-o    : ouput png file
		--contig |-c    : input contig coverage file
		--start  |-s    : start position
		--end    |-e    : end position
EOF
;
    exit;
}

my $inFile  = '';
my $outFile = '';
my $start   = 0;
my $end     = 0;
my @contigF = ();
GetOptions( 
		'input=s'  => \$inFile,
		'output=s' => \$outFile,
		'contig=s' => \@contigF,
		'start=i'  => \$start,
		'end=i'    => \$end,
		'help!'    => \&usage
		) or usage();


die "NOT DEFINED IMPUT FILE"             if ( !  defined $inFile );
die "INPUT FILE NAME IS EMPTY"           if (      $inFile eq '');
die "INPUT FILE $inFile DOES NOT EXISTS" if ( ! -f $inFile );
die "INPUT FILE $inFile IS EMPTY"        if ( ! -s $inFile ); # ;

die if ( index($inFile, ".cov") == -1 );

if ( @contigF )
{
	foreach my $contigF ( @contigF )
	{
		if ( defined $contigF && $contigF ne '' )
		{
			print "  CHECKING CONTIG $contigF\n";
			die "COULD NOT FIND CONTIG FILE '$contigF'"        if ( ! -f $contigF );
			die "CONTIG FILE '$contigF' IS EMPTY"              if ( ! -s $contigF ); # ;
			die "CONTIG FILE '$contigF' DOES NOT ENDS IN .cov" if ( index($contigF, ".cov") == -1 );
		}
	}
}


my $outPlot   = $inFile;
   $outPlot   =~ s/\.cov$/.plot/;
my $outImg    = $inFile;
   $outImg    =~ s/\.cov$/.png/;
   
if ( defined $outFile && $outFile ne '' )
{
	die if ( index($outFile, ".png") == -1 );
	$outPlot   = $outFile;
	$outPlot   =~ s/\.png$/.plot/;
	$outImg    = $outFile;
}


if ( $start !~ /^\d+$/ )
{
	die "UNKNOWN SECOND PARAMETER $start. SHOULD BE START POSITION\n";
}

if ( $end !~ /^\d+$/ )
{
	die "UNKNOWN THIRD PARAMETER $end. SHOULD BE END POSITION\n";
}

if (( $end != 0 ) && ( $end <= $start ))
{
	die "END $end SMALLER OR EQUAL TO START $start\n";
}

print "IN COV FILE : $inFile\n";
print "OUT PLOT    : $outPlot\n";
print "OUT PNG     : $outImg\n";
print "START       : $start\n";
print "END         : $end\n";

&mkPlotFile($inFile, $outPlot, $outImg, $start, $end) if ( $mkPlot );




sub mkPlotFile
{
  my $inFile   = shift;
  my $outFile  = shift;
  my $outImg   = shift;
  my $startPos = shift;
  my $endPos   = shift;
  print "ANALIZING $inFile\n";

  my $title     = $inFile;
     $title     =~ s/\.cov$//;

  my $fileSize = `wc -l $inFile`;

  if ($fileSize =~ /^(\d+)/)
  {
    $fileSize = $1;
  } else {
    die;
  }

  die "COV FILE IS EMPTY" if ! $fileSize;
  print "FILESIZE " . $fileSize . "\n";

  my $maxCov = 0;
  my $minCov = 99_999_999_999_999;
  my $sumCov = 0;
  my $avg    = -99_999_999_999_999;
  my $count  = 0;
  my $mean2  = 0;  
  
  
  open IN, "<$inFile" or die;
  while (my $line = <IN>)
  {
    ++$count;
    chomp $line;
    my @cols = split("\t", $line);
    
    if ( substr($line, 0, 1) eq "#")
    {
      if ($numCols == 0 && @colNames == 0)
      {
        @colNames    = @cols;
        $colNames[0] = substr($colNames[0], 1);
        $numCols     = scalar @cols;
        die "LESS THAN 2 COLUMNS" if $numCols < 2;
        die "HAS TITLE BUT TITLE HAS DIFFERENT LENGTH" if @colNames < $numCols;
      }
    } else {
      if ($numCols == 0)
      {
        $numCols = scalar @cols;
        die "LESS THAN 2 COLUMNS" if $numCols < 2;
      }

      my $val = $cols[2];
      
      if ( $avg == -99_999_999_999_999 )
      {
        $avg = $val;
      }
 

      next if (( $startPos != 0 ) && ( ( $count - 1 ) < $startPos ));
      last if (( $endPos   != 0 ) && ( ( $count - 1 ) > $endPos   ));

      #print "POS $1 COV 2 $2\n";
      $maxCov    = $val if ( $val > $maxCov );
      $minCov    = $val if ( $val < $minCov );
      $sumCov   += $val;
      my $delta  = $val - $avg;           # DIFF FROM AVERAGE
      $avg      += $delta / ($count > 1 ? $count - 1 : 1 );
      $mean2    += $delta * ( $val - $avg); # SQUARE MEAN
      #if ( $2 <= $minGapCov ) { $gaps{$inFile}->insert($1); }; # print "ADDING \"$inFile\" \"$line\" \"$1\"\n";
    }
  }
  close IN;


  my $var       = $mean2 / $count;
  my $stdDev    = sqrt($var);
  my $sdProp    = ($stdDev / ($avg || 1)) * 100 || 0;
  my $sdMax     = $sdProp > 100 ? $avg + ( 2*$stdDev ) : $avg + ( 3*$stdDev );
  if ($sdMax > ( 3 * $avg )) { $sdMax = (3 * $avg) };
  my $oldMaxCov = $maxCov;

  print "SD          " . $stdDev    . "\n";
  print "SD PROP     " . $sdProp    . "\n";
  print "SD MAX      " . $sdMax     . "\n";
  print "OLD MAX COV " . $oldMaxCov . "\n";
  print "MAX COV     " . $maxCov    . "\n";
  print "AVG COV     " . $avg       . "\n";
  print "MIN COV     " . $minCov    . "\n";

  if ($fixBySd && ($maxCov > $sdMax))
  {
    $maxCov = $sdMax > $maxCov ? $sdMax : $maxCov;
  }

  $maxCov += 1;
  $maxCov  = int($maxCov);
  $minCov  = int($minCov);

  print "MAX COV     " . $maxCov     . "\n";
  print "MIN COV     " . $minCov     . "\n";
  #if    ($maxCov < $minimum ) { $maxCov = $minimum; };
  #if    ($maxCov < 100      ) { $maxCov = 100 }
  #elsif ($maxCov < 1000     ) { while ( $maxCov % 100  ) { $maxCov++ } }
  #elsif ($maxCov < 10000    ) { while ( $maxCov % 1000 ) { $maxCov++ } }
  #elsif ($maxCov < 100000   ) { while ( $maxCov % 1000 ) { $maxCov++ } }

  $resume{$title}{min}   = $minCov;
  $resume{$title}{max}   = $oldMaxCov;
  $resume{$title}{sd}    = $stdDev;
  $resume{$title}{sdMax} = $sdMax;
  $resume{$title}{sum}   = $sumCov;
  $resume{$title}{avg}   = $avg;
  $resume{$title}{size}  = $fileSize;

  print "  NAME \"$inFile\" \n\tSIZE \"$fileSize\" ORIGMAX \"$oldMaxCov\" \n\tNEWMAX \"$maxCov\" MIN \"$minCov\" \n\tSUM \"$sumCov\" AVG \"$avg\" \n\tSTDDEV \"$stdDev\" 3SDMAX \"$sdMax\"\n";

  if ($mkIntermediary)
  {
    print "    EXPORTING $inFile\n";
    &genPlotFile($inFile, $outFile, $outImg, $title, $fileSize, $maxCov, $avg, $stdDev, $oldMaxCov, $minCov, $propMainGraph, $propSecGraph, $startPos, $endPos);
    if ($autoRun)
    {
      if ( -f $outFile )
      {
        print "      RUNNING GNUPLOT\n";
        print `gnuplot $outFile`;
		
		if ( ! -f $outFile )
		{
			die " GNUPLOT FAILED TO CREATE IMAGE. ERROR ON GNUPLOT";
		} else {
			print "        GNUPLOT SUCCEEDED TO CREATE IMAGE\n\n";
		}
      }
    }
  }
}




sub genPlotFile
{
  my $inFile        = $_[0];
  my $outPlotFile   = $_[1];
  my $outImgFile    = $_[2];
  my $title         = $_[3];
  my $fileSize      = $_[4];
  my $ySize         = $_[5];
  my $avg           = $_[6];
  my $stdDev        = $_[7];
  my $max           = $_[8];
  my $min           = $_[9];
  my $propMainGraph = $_[10];
  my $propSecGraph  = $_[11];
  my $startPos      = $_[12];
  my $endPos        = $_[13];

  my $sdProp        = int(($stdDev / ($avg || 1)) * 100) || 0;
  my $avgPlusSdCov  =     ($avg    + $stdDev)  || 0;
  my $avgMinSdCov   =     ($avg    - $stdDev)  || 0;
  my $split         = int( $ySize  / 2);

  my $finalEndPos   = $endPos == 0 ? $fileSize : $endPos;
  my $len           = $endPos - $startPos + 1;
  #my $xSize         = $_[4];

  print "    EXPORTING $inFile\n";
  print "    NUM COLS: $numCols\n";
  print "      TO COV  $outPlotFile\n";
  print "      TO IMG  $outImgFile\n";
  print "      START   $startPos\n";
  print "      END     $endPos\n";
  print "      LENGTH  $len\n";

  open OUT, ">$outPlotFile" or die;


my $graphWidth   = 4096;
my $graphHeight  = $graphWidth * 1.6182339887;
#   $graphHeight *= $numCols - 1;


my $sizePropMain = 0.150;
my $titleSpace   = 0.025;
my $spacing      = 0.005;
my $originMain   = 1 - $sizePropMain - $titleSpace;
my $spaceLeft    = $originMain - ( $spacing * ( $numCols - 1 ));

$title =~ s/\./\\./g;
$title =~ s/_/\\_/g;

my $numColNames  = scalar @colNames;
my $mainName     = 'Frequency';
   $mainName     = $colNames[1] if ( $numColNames > 0 && defined $colNames[1] && $colNames[1] ne "");


my $plot1 = "
set origin 0, $originMain
set size   1, $sizePropMain
plot '$inFile' using 1:2 lt rgb \"#".&getColor(0)."\" with filledcurves title '$mainName'
";

my $contigs = "";
if ( @contigF )
{
	foreach my $contigF ( @contigF )
	{
		if ( defined $contigF && $contigF ne '' )
		{
			if ( defined $contigF && $contigF ne '' )
			{
				my $contigSize = 0.015;
				
				$contigs       .= &getContigs($contigF, $startPos, $finalEndPos, $ySize, ($originMain - $contigSize));
				$contigs       .= "\n\n";
				
				#$sizePropMain -= $contigSize;
				$originMain    -= $contigSize;
				$spaceLeft      = $originMain - ( $spacing * ( $numCols - 1 )) - 0.005 - 0.005;
			}
		}
	}
}

my $sizeProp     = ( $spaceLeft / ( $numCols - 2 ) );

my $plot2;

my $nextOrigin = $originMain - $sizeProp - 0.005;

for ( my $i = 2; $i < $numCols; $i++ )
{
  my $colName = "Column " . ($i + 1);
  $colName = $colNames[$i] if ( $numColNames > 0 && defined $colNames[$i] && $colNames[$i] ne "");
  $plot2 .= "
set origin 0, $nextOrigin
set size   1, $sizeProp
plot '$inFile' using 1:" . ($i+1) . " lc rgb \"#".&getColor($i-1)."\" with filledcurves title '$colName bp'

  ";

  $nextOrigin = $nextOrigin - $sizeProp - 0.005;
}





print OUT <<CONF
set title  '$title'
set ylabel 'Coverage Depth'
set xlabel 'Position Reference'
set xrange [$startPos:$finalEndPos]
set yrange [0:$ySize]
set bars large

set label "AVG COV     : $avg"                               at screen 0.05,0.995 front left
set label "STD DEV COV : $stdDev ($sdProp%)"                 at screen 0.05,0.990 front left
set label "MAX COV     : $max"                               at screen 0.05,0.985 front left
set label "MIN COV     : $min"                               at screen 0.05,0.980 front left
set label "LENG REF    : $fileSize"                          at screen 0.05,0.975 front left
set label "START       : $startPos END: $finalEndPos ($len)" at screen 0.05,0.970 front left

set style fill transparent solid 1.0 noborder
set key inside left top vertical Left reverse enhanced autotitles nobox
set key noinvert samplen 1 spacing 1 width 0 height 0
set style data filledcurves y1=0 
unset colorbox

set grid
set palette model RGB
set pointsize 0.5
set ytics  out
set xtics 
set mxtics 2
set mytics 2
#set format x "10^\%L"
set bmargin 0

#set terminal png enhanced size $graphHeight,$graphWidth large font "/usr/share/fonts/default/ghostscript/putr.pfa,16"
set terminal png enhanced size $graphWidth,$graphHeight large font "Courier" 14
set output "$outImgFile"

set multiplot

$plot1

set xrange [$startPos:$finalEndPos]
set yrange [0:$ySize]

unset label 1
unset label 2
unset label 3
unset label 4
unset label 5
unset label 6
unset title
unset xlabel
unset ylabel

#CONTIGS
$contigs


$plot2

unset multiplot

exit

CONF
;




  close OUT;
  print "  EXPORTED\n";
}


sub getContigs()
{
	my $contigF     = shift;
	my $startPos    = shift;
	my $finalEndPos = shift;
	my $ySize       = shift;
	my $origin      = shift;
	my $contigs     = "";
	my $yPos        = $ySize  * 0.980;
	
	print "        PLOTTING CONTIG $contigF\n";
	
	if ( defined $contigF && $contigF ne '' )
	{
		$contigs  .= "plot '$contigF' using 1:2 lt rgb \"black\" with filledcurves title 'contig'";
	}
	
	if ( $contigs ne "" )
	{
		$contigs =  "
set style line 99 lt rgb 'black' lw 6
unset ytics 
unset xtics 
unset mxtics
unset mytics
unset key
unset border
unset grid
set ylabel ' '
set ytics  out
set xrange [$startPos:$finalEndPos]
set yrange [0:1]
set mytics 0

set origin 0, $origin
set size   1, 0.005

$contigs

set grid
set border
set key
set ytics 
set xtics 
set mxtics 2
set mytics 2
";
	}
	
	return $contigs;
}


sub getColor
{
  my $pos = shift;

  my @colors = (
"FF0000",
"FF7F00",
"FFFF00",
"7FFF00",
"00FF00",
"00FF7F",
"00FFFF",
"007FFF",
"0000FF",
"7F00FF",
"FF00FF",
"FF007F",
"000000",
"808080",
"005C5C",
"B22222"
);
  while ( $pos >= @colors )
  {
    $pos = $pos - @colors;
  }
  
  return $colors[$pos];
}
  

sub mkResume
{
  my $hash = $_[0];
  my $name = $_[1];
  print "MAKING RESUME $name\n";

  my $gAvg    = 0;
  my $gSum    = 0;
  my $gMin    = 9999999;
  my $gMax    = 0;
  my $gSd     = 0;
  my $gSdMax  = 0;
  my $gSize   = 0;
  my $gAvgMin = 0;
  my $gAvgMax = 0;

  my $plotStr;
  my $c = 1;

  foreach my $title (sort keys %$hash)
  {
    my $min   = $hash->{$title}{min};
    my $max   = $hash->{$title}{max};
    my $sd    = $hash->{$title}{sd};
    my $sdMax = $hash->{$title}{sdMax};
    my $sum   = $hash->{$title}{sum};
    my $avg   = $hash->{$title}{avg};
    my $size  = $hash->{$title}{size};

    my $maxD  = $correctResume ? $sdMax : $max;
    $gMin     = $min if $min < $gMin;
    $gMax     = $max if $max > $gMax;
    $gAvgMin += $min;
    $gAvgMax += $max;
    $gSd     += $sd;
    $gSdMax  += $sdMax;
    $gSum    += $avg;
    $gSize   += $size;

    print "\tCHROM \"$title\" MIN \"$min\" MAX \"$max\" SD \"$sd\" SDMAX \"$sdMax\" SUM \"$sum\" AVG \"$avg\" SIZE \"$size\"\n";

    $plotStr .= "$c " . ($avg-$sd) . " $min " . $maxD . " " . ($avg+$sd) . " $avg $title\n";
    $c++;
  }

  my $total = $c - 1;

  $gAvg    = $total ? int(($gSum    / $total)+.5) : 0;
  $gSd     = $total ? int(($gSd     / $total)+.5) : 0;
  $gSdMax  = $total ? int(($gSdMax  / $total)+.5) : 0;
  $gAvgMin = $total ? int(($gAvgMin / $total)+.5) : 0;
  $gAvgMax = $total ? int(($gAvgMax / $total)+.5) : 0;

  if ($plotStr)
  {
    open OUTDAT, ">$name.dat" or die;
    print OUTDAT $plotStr;
    close OUTDAT;

    open OUTPLOT, ">$name.plot" or die;
    print OUTPLOT <<PLOT
set bars     4.0
set style    fill empty
set title   "RESUME"
set xlabel  "chromossome"
set ylabel  "coverage"
set xtics   1

set label "AVG COV         : $gAvg"      at graph 0.05,0.95 front left
set label "AVG STD DEV COV : $gSd"       at graph 0.05,0.92 front left
set label "MAX COV         : $gMax"      at graph 0.05,0.89 front left
set label "AVG MAX COV     : $gAvgMax"   at graph 0.05,0.86 front left
set label "MIN COV         : $gMin"      at graph 0.05,0.83 front left
set label "AVG MIN COV     : $gAvgMin"   at graph 0.05,0.80 front left
set label "LENG REF        : $gSize"     at graph 0.05,0.77 front left

set terminal png size 1024,768 large font "/usr/share/fonts/default/ghostscript/putr.pfa,12" $transparentStr
set output "$name.png"

plot  "$name.dat" [0:$total] using 1:2 notitle with lines, \\
      "$name.dat" [0:$total] using 1:6 notitle with lines, \\
      "$name.dat" [0:$total] using 1:5 notitle with lines, \\
      "$name.dat" [0:$total] using 1:2:3:4:5 notitle with candlesticks

PLOT
;
    close OUTPLOT;

    print `gnuplot $name.plot`;
  }
  #print "echo -e 'set boxwidth -2;\\nplot $plotStr notitle with boxerrorbars' | gnuplot\n";
}




1;

