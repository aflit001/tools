#!/usr/bin/perl -w
use strict;

my @filters = (
    [\&perfectCov],
    [\&perfectNeighbour, 6, 0.1],
    [\&perfectNeighbour, 5, 0.2],
    [\&perfectNeighbour, 4, 0.3],
    [\&perfectNeighbour, 3, 0.3],
    [\&perfectNeighbour, 2, 0.3],
);

my $numFilters = scalar @filters;

while ( my $line = <STDIN> )
{
    chomp $line;
    next if substr($line, 0, 1) eq "#";
    my @cols = split("\t", $line);
    my $fRes = 0;
    for my $filterFunc ( @filters )
    {
        #print "FILTER FUNC ", $filterFunc, "\n";
        #print "  FUNC ", $filterFunc->[0], "\n";
        $fRes += $filterFunc->[0]->($filterFunc, \@cols);
    }
    
    print $line, "\n" if (( $fRes != 0 ) && ( $fRes == $numFilters ))
}

sub perfectCov()
{
    my $func = shift;
    my $vals = shift;
    return 1 if $vals->[1] == 1;
}

sub perfectNeighbour()
{
    my $func = shift;
    my $vals = shift;
    #print "POS ", $func->[1], " MIN ", $func->[2], "\n";
    return 1 if ( $vals->[$func->[1]] >= $func->[2] )
}


