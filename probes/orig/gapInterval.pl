#!/usr/bin/perl -w
use strict;
use Set::IntSpan::Fast;

my $interval = Set::IntSpan::Fast->new();

while (<STDIN>)
{
    if ( (/(\d+)\s+(\d+)/) )
    {
        $interval->add_range($1, $2);
    }
}


print $interval->as_string(), "\n"
