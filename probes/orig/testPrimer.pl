#!/usr/bin/perl -w
use strict;

my $name   = shift @ARGV;
my $p1     = shift @ARGV;
my $p2     = shift @ARGV;
my $db     = "S_lycopersicum_scaffolds";
my $contig = 'S_lycopersicum_scaffolds_from_contigs.2.40.agp';

die "NO NAME DEFINED"       if ! defined $name;
die "NO PRIMER 1 DEFINED"   if ! defined $p1;
die "NO PRIMER 2 DEFINED"   if ! defined $p2;
die "NOT ENOUGH ARGUMENTS. NEEDS. NAME PRIMER1 PRIMER2" if (( ! defined $name ) || ( ! defined $p1 ) || ( ! defined $p2 ));
die "P1 NOT A DNA SEQUENCE" if $p1 !~ /^[A|C|G|T]+$/;
die "P2 NOT A DNA SEQUENCE" if $p2 !~ /^[A|C|G|T]+$/;

my $cmd1 = "bowtie $db -c $p1,$p2";
    
my @bRes = `$cmd1 2>\&1`;
#0       +       SL2.40sc04057   23797776        GAGTCCGACAAGCCCTGAATG   IIIIIIIIIIIIIIIIIIIII   0
#1       -       SL2.40sc04057   23801944        GCTGCCTCTAAACAAATGGTGC  IIIIIIIIIIIIIIIIIIIIII  0

print @bRes;
my %res;
foreach my $res ( @bRes )
{
    if ( $res =~ /[+|-]\s+(\S+)\s+(\d+)\s+(\S+)/ )
    {
        my $sc    = $1;
        my $pos   = $2;
        my $seq   = $3;
        my $rcSeq = &rc($seq);
        print " GET SEQ $seq SCAFFOLD $sc POSITION $pos (RC $rcSeq)\n";
        die "SEQ $seq DOES NOT MATCH" if (($seq ne $p1) && ($seq ne $p2) && ($rcSeq ne $p1) && ($rcSeq ne $p2));
        if (( $seq eq $p1 ) || ( $seq eq $p2 ) )
        {
            $res{$seq}{$sc}{$pos}++;
        } else {
            $res{$rcSeq}{$sc}{$pos}++;
        }
    }
}

die "NO RESULT TO PRIMER 1: $p1" if ! exists $res{$p1};
die "NO RESULT TO PRIMER 2: $p2" if ! exists $res{$p2};

die "MORE THAN ONE SCAFFOLD MAP TO PRIMER 1: $p1" if scalar keys %{$res{$p1}} > 1;
die "MORE THAN ONE SCAFFOLD MAP TO PRIMER 1: $p1" if scalar keys %{$res{$p2}} > 1;




die "DIFFERENT SCAFFOLDS BETWEEN PRIMERS" if (keys %{$res{$p1}})[0] ne (keys %{$res{$p1}})[0];
my $scaf  = (keys %{$res{$p1}})[0];
print "  SCAFFOLD $scaf\n";
die "PRIMER 1 MAPPED IN MORE THAN ONE POSITION AT SCAFFOLD $scaf" if scalar (keys %{$res{$p1}{$scaf}}) > 1;
die "PRIMER 2 MAPPED IN MORE THAN ONE POSITION AT SCAFFOLD $scaf" if scalar (keys %{$res{$p1}{$scaf}}) > 1;




my $start = (keys %{$res{$p1}{$scaf}})[0];
my $end   = (keys %{$res{$p2}{$scaf}})[0];
print "  START $start\n";
print "  END   $end\n";
if ( $start > $end )
{
	warn "      START ($start) SMALLER THAN END ($end). TRY INVERTING THE ORDER";
	my $t  = $start;
	$start = $end;
	$end   = $t;
}





my $cmd2  = "cat data/S_lycopersicum_chromosomes_from_scaffolds.2.40.agp | grep '$scaf' | gawk '{print \$1}' | sort | uniq | head -1";
my $chrom = `$cmd2`;
chomp($chrom);
print "  CHROM $chrom\n";





my $base       = "S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.map";
my $contigMap  = $contig . ".$scaf.agp.contig.agp.cov";
my $gapMap     = $contig . ".$scaf.agp.gap.agp.cov";
my $unknownMap = $contig . ".$scaf.agp.unknown.agp.cov";
#S_lycopersicum_chromosomes.fa_NONE.tab.$CHROMNAME.tab.$SCAF.tab.cov

my $nMap      = "data/S_lycopersicum_scaffolds.fa_NONE.tab." . $chrom . ".tab.$scaf.tab.cov";
my $prop      = "$base.$chrom.map.$scaf.map.cov.prop";
print "  GENERATING GRAPHIC $prop.png\n";
print "  GENERATING GRAPHIC $prop.$name.$start-$end.png\n";

if ( -f "$prop.$name.$start-$end.png" )
{
	print "  OUTPUT EXISTS. SKIPPING\n";
	exit 0;
}



#if ( -f "$prop.png" )
#{
#    print "  MOVING $prop.png TO TMP FILE $prop.tmp.png\n";
#    print `mv $prop.png $prop.tmp.png`;
#}

print "  MAKING GRAPHIC\n";
my $cmd3      = "./mkplot_2.pl --input=$prop.cov " .
"--output=$prop.$name.$start-$end.png " .
"--start=$start --end=$end " .
"--contig=$contigMap " .
"--contig=$gapMap " .
"--contig=$unknownMap " .
"--contig=$nMap";
print "    CMD: $cmd3\n";

open(GRA, "$cmd3 |") or die "Can't make graphic: $!";
while (<GRA>){ print };
close GRA;
#print `$cmd3`;


if ( ! -f "$prop.$name.$start-$end.png" )
{
    die "  ERROR CREATING OUTPUT :: $prop.$name.$start-$end.png";
} else {
	print "  SUCCESS CREATING OUTPUT $prop.$name.$start-$end.png\n\n";
}



print "  MAKING GRAPHIC SIDE\n";
my $start2 = $start - 1000;
my $end2   = $end   + 1000;

   $start2 = 0 if $start2 <= 0;

my $cmd4      = "./mkplot_2.pl --input=$prop.cov " .
"--output=$prop.$name.SIDE.$start-$end.png " .
"--start=$start2 --end=$end2 " .
"--contig=$contigMap " .
"--contig=$gapMap " .
"--contig=$unknownMap " .
"--contig=$nMap";
print "    CMD: $cmd3\n";

open(GRA2, "$cmd4 |") or die "Can't make graphic: $!";
while (<GRA2>){ print };
close GRA2;
#print `$cmd3`;


if ( ! -f "$prop.$name.SIDE.$start2-$end2.png" )
{
    die "  ERROR CREATING OUTPUT :: $prop.$name.SIDE.$start2-$end2.png";
} else {
	print "  SUCCESS CREATING OUTPUT $prop.$name.SIZE.$start2-$end2.png\n\n";
}


sub rc
{
    my $s = shift;
    $s =~ tr/ACGT/TGCA/;
    $s = reverse($s);
    return $s;
}
