#!/bin/bash
#BOWTIE MAPPING
export BOWTIECHUNKMBS=1024
export BOWTIESUPRESSBIGGERTHAN=1
export BOWTIETHREADS=60
export BOWTIETHREADS=`echo "$CORES * 0.8 / 1" | bc`
export BOWTIESEEDLEN=15
export BOWTIEOFFRATE=1
export BOWTIEMISMATCHES=0


#bowtie [options]* <ebwt> {-1 <m1> -2 <m2> | --12 <r> | <s>} [<hit>]
#
#Main arguments
#    <ebwt>
#        The basename of the index to be searched. The basename is the name of any of the
#        index files up to but not including the final .1.ebwt / .rev.1.ebwt / etc.
#        bowtie looks for the specified index first in the current directory, then in the
#        indexes subdirectory under the directory where the bowtie executable is located,
#        then looks in the directory specified in the BOWTIE_INDEXES environment variable.
#    
#    <m1>
#        Comma-separated list of files containing the #1 mates (filename usually includes
#        _1), or, if -c is specified, the mate sequences themselves. E.g., this might be
#        flyA_1.fq,flyB_1.fq, or, if -c is specified, this might be GGTCATCCT,ACGGGTCGT.
#        Sequences specified with this option must correspond file-for-file and read-for
#        -read with those specified in <m2>. Reads may be a mix of different lengths. If
#        - is specified, bowtie will read the #1 mates from the "standard in" filehandle.
#    
#    <m2>
#        Comma-separated list of files containing the #2 mates (filename usually includes
#        _2), or, if -c is specified, the mate sequences themselves. E.g., this might be
#        flyA_2.fq,flyB_2.fq, or, if -c is specified, this might be GGTCATCCT,ACGGGTCGT.
#        Sequences specified with this option must correspond file-for-file and read-for
#        -read with those specified in <m1>. Reads may be a mix of different lengths. If
#        - is specified, bowtie will read the #2 mates from the "standard in" filehandle.
#    
#    <r>
#        Comma-separated list of files containing a mix of unpaired and paired-end reads
#        in Tab-delimited format. Tab-delimited format is a 1-read-per-line format where
#        unpaired reads consist of a read name, sequence and quality string each separated
#        by tabs. A paired-end read consists of a read name, sequnce of the #1 mate,
#        quality values of the #1 mate, sequence of the #2 mate, and quality values of
#        the #2 mate separated by tabs. Quality values can be expressed using any of the
#        scales supported in FASTQ files. Reads may be a mix of different lengths and
#        paired-end and unpaired reads may be intermingled in the same file. If - is
#        specified, bowtie will read the Tab-delimited reads from the "standard in"
#        filehandle.
#    
#    <s>
#        A comma-separated list of files containing unpaired reads to be aligned, or, if
#        -c is specified, the unpaired read sequences themselves. E.g., this might be
#        lane1.fq,lane2.fq,lane3.fq,lane4.fq, or, if -c is specified, this might be
#        GGTCATCCT,ACGGGTCGT. Reads may be a mix of different lengths. If - is specified,
#        Bowtie gets the reads from the "standard in" filehandle.
#    
#    <hit>
#        File to write alignments to. By default, alignments are written to the "standard
#        out" filehandle (i.e. the console).
#
#Options
#    Input
#        -q
#            The query input files (specified either as <m1> and <m2>, or as <s>) are FASTQ
#            files (usually having extension .fq or .fastq). This is the default.
#            See also: --solexa-quals and --integer-quals.
#        
#        -f
#            The query input files (specified either as <m1> and <m2>, or as <s>) are FASTA
#            files (usually having extension .fa, .mfa, .fna or similar). All quality values
#            are assumed to be 40 on the Phred quality scale.
#        
#        -r
#            The query input files (specified either as <m1> and <m2>, or as <s>) are Raw
#            files: one sequence per line, without quality values or names. All quality
#            values are assumed to be 40 on the Phred quality scale.
#        
#        -c
#            The query sequences are given on command line. I.e. <m1>, <m2> and <singles> are
#            comma-separated lists of reads rather than lists of read files.
#        
#        -C/--color
#            Align in colorspace. Read characters are interpreted as colors. The index s
#            pecified must be a colorspace index (i.e. built with bowtie-build -C, or bowtie
#            will print an error message and quit. See Colorspace alignment for more details.
#        
#        -Q/--quals <files>
#            Comma-separated list of files containing quality values for corresponding
#            unpaired CSFASTA reads. Use in combination with -C and -f. --integer-quals is
#            set automatically when -Q/--quals is specified.
#        
#        --Q1 <files>
#            Comma-separated list of files containing quality values for corresponding
#            CSFASTA #1 mates. Use in combination with -C, -f, and -1. --integer-quals is set
#            automatically when --Q1 is specified.
#        
#        --Q2 <files>
#            Comma-separated list of files containing quality values for corresponding
#            CSFASTA #2 mates. Use in combination with -C, -f, and -2. --integer-quals is set
#            automatically when --Q2 is specified.
#        
#        -s/--skip <int>
#            Skip (i.e. do not align) the first <int> reads or pairs in the input.
#        
#        -u/--qupto <int>
#            Only align the first <int> reads or read pairs from the input (after the
#            -s/--skip reads or pairs have been skipped). Default: no limit.
#        
#        -5/--trim5 <int>
#            Trim <int> bases from high-quality (left) end of each read before alignment
#            (default: 0).
#        
#        -3/--trim3 <int>
#            Trim <int> bases from low-quality (right) end of each read before alignment
#            (default: 0).
#        
#        --phred33-quals
#            Input qualities are ASCII chars equal to the Phred quality plus 33. Default: on.
#        
#        --phred64-quals
#            Input qualities are ASCII chars equal to the Phred quality plus 64. Default: off.
#        
#        --solexa-quals
#            Convert input qualities from Solexa (which can be negative) to Phred (which
#            can't). This is usually the right option for use with (unconverted) reads
#            emitted by GA Pipeline versions prior to 1.3. Default: off.
#        
#        --solexa1.3-quals
#            Same as --phred64-quals. This is usually the right option for use with
#            (unconverted) reads emitted by GA Pipeline version 1.3 or later. Default: off.
#        
#        --integer-quals
#            Quality values are represented in the read input file as space-separated ASCII
#            integers, e.g., 40 40 30 40..., rather than ASCII characters, e.g., II?I....
#            Integers are treated as being on the Phred quality scale unless --solexa-quals
#            is also specified. Default: off.
#        
#    Alignment
#        -v <int>
#            Report alignments with at most <int> mismatches. -e and -l options are ignored
#            and quality values have no effect on what alignments are valid. -v is mutually
#            exclusive with -n.
#        
#        -n/--seedmms <int>
#            Maximum number of mismatches permitted in the "seed", i.e. the first L base
#            pairs of the read (where L is set with -l/--seedlen). This may be 0, 1, 2 or 3
#            and the default is 2. This option is mutually exclusive with the -v option.
#        
#        -e/--maqerr <int>
#            Maximum permitted total of quality values at all mismatched read positions
#            throughout the entire alignment, not just in the "seed". The default is 70.
#            Like Maq, bowtie rounds quality values to the nearest 10 and saturates at 30;
#            rounding can be disabled with --nomaqround.
#        
#        -l/--seedlen <int>
#            The "seed length"; i.e., the number of bases on the high-quality end of the read
#            to which the -n ceiling applies. The lowest permitted setting is 5 and the
#            default is 28. bowtie is faster for larger values of -l.
#        
#        --nomaqround
#            Maq accepts quality values in the Phred quality scale, but internally rounds
#            values to the nearest 10, with a maximum of 30. By default, bowtie also rounds
#            this way. --nomaqround prevents this rounding in bowtie.
#        
#        -I/--minins <int>
#            The minimum insert size for valid paired-end alignments. E.g. if -I 60 is
#            specified and a paired-end alignment consists of two 20-bp alignments in the
#            appropriate orientation with a 20-bp gap between them, that alignment is
#            considered valid (as long as -X is also satisfied). A 19-bp gap would not be
#            valid in that case. If trimming options -3 or -5 are also used, the -I
#            constraint is applied with respect to the untrimmed mates. Default: 0.
#        
#        -X/--maxins <int>
#            The maximum insert size for valid paired-end alignments. E.g. if -X 100 is
#            specified and a paired-end alignment consists of two 20-bp alignments in the
#            proper orientation with a 60-bp gap between them, that alignment is considered
#            valid (as long as -I is also satisfied). A 61-bp gap would not be valid in that
#            case. If trimming options -3 or -5 are also used, the -X constraint is applied
#            with respect to the untrimmed mates, not the trimmed mates. Default: 250.
#        
#        --fr/--rf/--ff
#            The upstream/downstream mate orientations for a valid paired-end alignment
#            against the forward reference strand. E.g., if --fr is specified and there is a
#            candidate paired-end alignment where mate1 appears upstream of the reverse
#            complement of mate2 and the insert length constraints are met, that alignment is
#            valid. Also, if mate2 appears upstream of the reverse complement of mate1 and
#            all other constraints are met, that too is valid. --rf likewise requires that an
#            upstream mate1 be reverse-complemented and a downstream mate2 be
#            forward-oriented. --ff requires both an upstream mate1 and a downstream mate2 to
#            be forward-oriented. Default: --fr when -C (colorspace alignment) is not
#            specified, --ff when -C is specified.
#        
#        --nofw/--norc
#            If --nofw is specified, bowtie will not attempt to align against the forward
#            reference strand. If --norc is specified, bowtie will not attempt to align
#            against the reverse-complement reference strand. For paired-end reads using
#            --fr or --rf modes, --nofw and --norc apply to the forward and reverse-
#            complement pair orientations. I.e. specifying --nofw and --fr will only find
#            reads in the R/F orientation where mate 2 occurs upstream of mate 1 with respect
#            to the forward reference strand.
#        
#        --maxbts
#            The maximum number of backtracks permitted when aligning a read in -n 2 or -n 3
#            mode (default: 125 without --best, 800 with --best). A "backtrack" is the
#            introduction of a speculative substitution into the alignment. Without this
#            limit, the default parameters will sometimes require that bowtie try 100s or
#            1,000s of backtracks to align a read, especially if the read has many
#            low-quality bases and/or has no valid alignments, slowing bowtie down
#            significantly. However, this limit may cause some valid alignments to be missed.
#            Higher limits yield greater sensitivity at the expensive of longer running times.
#            See also: -y/--tryhard.
#        
#        --pairtries <int>
#            For paired-end alignment, this is the maximum number of attempts bowtie will
#            make to match an alignment for one mate up with an alignment for the opposite
#            mate. Most paired-end alignments require only a few such attempts, but pairs
#            where both mates occur in highly repetitive regions of the reference can
#            require significantly more. Setting this to a higher number allows bowtie to
#            find more paired- end alignments for repetitive pairs at the expense of speed.
#            The default is 100. See also: -y/--tryhard.
#        
#        -y/--tryhard
#            Try as hard as possible to find valid alignments when they exist, including
#            paired-end alignments. This is equivalent to specifying very high values for the
#            --maxbts and --pairtries options. This mode is generally much slower than the
#            default settings, but can be useful for certain problems. This mode is slower
#            when (a) the reference is very repetitive, (b) the reads are low quality, or (c)
#            not many reads have valid alignments.
#        
#        --chunkmbs <int>
#            The number of megabytes of memory a given thread is given to store path
#            descriptors in --best mode. Best-first search must keep track of many paths at
#            once to ensure it is always extending the path with the lowest cumulative cost.
#            Bowtie tries to minimize the memory impact of the descriptors, but they can
#            still grow very large in some cases. If you receive an error message saying that
#            chunk memory has been exhausted in --best mode, try adjusting this parameter up
#            to dedicate more memory to the descriptors. Default: 64.
#    
#    
#    Reporting
#        -k <int>
#            Report up to <int> valid alignments per read or pair (default: 1). Validity of
#            alignments is determined by the alignment policy (combined effects of -n, -v,
#            -l, and -e). If more than one valid alignment exists and the --best and
#            --strata options are specified, then only those alignments belonging to the best
#            alignment "stratum" will be reported. Bowtie is designed to be very fast for
#            small -k but bowtie can become significantly slower as -k increases. If you
#            would like to use Bowtie for larger values of -k, consider building an index
#            with a denser suffix-array sample, i.e. specify a smaller -o/--offrate when
#            invoking bowtie-build for the relevant index (see the Performance tuning section
#            for details).
#        
#        -a/--all
#            Report all valid alignments per read or pair (default: off). Validity of
#            alignments is determined by the alignment policy (combined effects of -n, -v, -l,
#            and -e). If more than one valid alignment exists and the --best and --strata
#            options are specified, then only those alignments belonging to the best alignment
#            "stratum" will be reported. Bowtie is designed to be very fast for small -k but
#            bowtie can become significantly slower if -a/--all is specified. If you would
#            like to use Bowtie with -a, consider building an index with a denser
#            suffix-array sample, i.e. specify a smaller -o/--offrate when invoking
#            bowtie-build for the relevant index (see the Performance tuning section for
#            details).
#        
#        -m <int>
#            Suppress all alignments for a particular read or pair if more than <int>
#            reportable alignments exist for it. Reportable alignments are those that would
#            be reported given the -n, -v, -l, -e, -k, -a, --best, and --strata options.
#            Default: no limit. Bowtie is designed to be very fast for small -m but bowtie
#            can become significantly slower for larger values of -m. If you would like to
#            use Bowtie for larger values of -k, consider building an index with a denser
#            suffix-array sample, i.e. specify a smaller -o/--offrate when invoking
#            bowtie-build for the relevant index (see the Performance tuning section for
#            details).
#        
#        -M <int>
#            Behaves like -m except that if a read has more than <int> reportable alignments,
#            one is reported at random. In default output mode, the selected alignment's 7th
#            column is set to <int>+1 to indicate the read has at least <int>+1 valid
#            alignments. In -S/--sam mode, the selected alignment is given a MAPQ (mapping
#            quality) of 0 and the XM:I field is set to <int>+1. This option requires --best;
#            if specified without --best, --best is enabled automatically.
#        
#        --best
#            Make Bowtie guarantee that reported singleton alignments are "best" in terms of
#            stratum (i.e. number of mismatches, or mismatches in the seed in the case of -n
#            mode) and in terms of the quality values at the mismatched position(s). Stratum
#            always trumps quality; e.g. a 1-mismatch alignment where the mismatched position
#            has Phred quality 40 is preferred over a 2-mismatch alignment where the
#            mismatched positions both have Phred quality 10. When --best is not specified,
#            Bowtie may report alignments that are sub-optimal in terms of stratum and/or
#            quality (though an effort is made to report the best alignment). --best mode
#            also removes all strand bias. Note that --best does not affect which alignments
#            are considered "valid" by bowtie, only which valid alignments are reported by
#            bowtie. When --best is specified and multiple hits are allowed (via -k or -a),
#            the alignments for a given read are guaranteed to appear in best-to-worst order
#            in bowtie's output. bowtie is somewhat slower when --best is specified.
#        
#        --strata
#            If many valid alignments exist and are reportable (e.g. are not disallowed via
#            the -k option) and they fall into more than one alignment "stratum", report only
#            those alignments that fall into the best stratum. By default, Bowtie reports all
#            reportable alignments regardless of whether they fall into multiple strata. When
#            --strata is specified, --best must also be specified.
#        
#    Output
#        -t/--time
#            Print the amount of wall-clock time taken by each phase.
#        
#        -B/--offbase <int>
#            When outputting alignments, number the first base of a reference sequence as
#            <int>. Default: 0.
#        
#        --quiet
#            Print nothing besides alignments.
#        
#        --refout
#            Write alignments to a set of files named refXXXXX.map, where XXXXX is the
#            0-padded index of the reference sequence aligned to. This can be a useful way to
#            break up work for downstream analyses when dealing with, for example, large
#            numbers of reads aligned to the assembled human genome. If <hits> is also
#            specified, it will be ignored.
#        
#        --refidx
#            When a reference sequence is referred to in a reported alignment, refer to it by
#            0-based index (its offset into the list of references that were indexed) rather
#            than by name.
#        
#        --al <filename>
#            Write all reads for which at least one alignment was reported to a file with
#            name <filename>. Written reads will appear as they did in the input, without any
#            of the trimming or translation of quality values that may have taken place
#            within bowtie. Paired-end reads will be written to two parallel files with _1
#            and _2 inserted in the filename, e.g., if <filename> is aligned.fq, the #1 and
#            #2 mates that fail to align will be written to aligned_1.fq and aligned_2.fq
#            respectively.
#        
#        --un <filename>
#            Write all reads that could not be aligned to a file with name <filename>.
#            Written reads will appear as they did in the input, without any of the trimming
#            or translation of quality values that may have taken place within Bowtie.
#            Paired-end reads will be written to two parallel files with _1 and _2 inserted
#            in the filename, e.g., if <filename> is unaligned.fq, the #1 and #2 mates that
#            fail to align will be written to unaligned_1.fq and unaligned_2.fq respectively.
#            Unless --max is also specified, reads with a number of valid alignments
#            exceeding the limit set with the -m option are also written to <filename>.
#        
#        --max <filename>
#            Write all reads with a number of valid alignments exceeding the limit set with
#            the -m option to a file with name <filename>. Written reads will appear as they
#            did in the input, without any of the trimming or translation of quality values
#            that may have taken place within bowtie. Paired-end reads will be written to
#            two parallel files with _1 and _2 inserted in the filename, e.g., if <filename>
#            is max.fq, the #1 and #2 mates that exceed the -m limit will be written to
#            max_1.fq and max_2.fq respectively. These reads are not written to the file
#            specified with --un.
#        
#        --suppress <cols>
#            Suppress columns of output in the default output mode. E.g. if --suppress 1,5,6
#            is specified, the read name, read sequence, and read quality fields will be
#            omitted. See Default Bowtie output for field descriptions. This option is
#            ignored if the output mode is -S/--sam.
#        
#        --fullref
#            Print the full refernce sequence name, including whitespace, in alignment
#            output. By default bowtie prints everything up to but not including the first
#            whitespace.
#    
#    
#    Colorspace
#        --snpphred <int>
#            When decoding colorspace alignments, use <int> as the SNP penalty. This should
#            be set to the user's best guess of the true ratio of SNPs per base in the
#            subject genome, converted to the Phred quality scale. E.g., if the user expects
#            about 1 SNP every 1,000 positions, --snpphred should be set to 30 (which is also
#            the default). To specify the fraction directly, use --snpfrac.
#        
#        --snpfrac <dec>
#            When decoding colorspace alignments, use <dec> as the estimated ratio of SNPs
#            per base. For best decoding results, this should be set to the user's best guess
#            of the true ratio. bowtie internally converts the ratio to a Phred quality, and
#            behaves as if that quality had been set via the --snpphred option.
#            Default: 0.001.
#        
#        --col-cseq
#            If reads are in colorspace and the default output mode is active, --col-cseq
#            causes the reads' color sequence to appear in the read-sequence column (column
#            5) instead of the decoded nucleotide sequence. See the Decoding colorspace
#            alignments section for details about decoding. This option is ignored in
#            -S/--sam mode.
#        
#        --col-cqual
#            If reads are in colorspace and the default output mode is active, --col-cqual
#            causes the reads' original (color) quality sequence to appear in the quality
#            column (column 6) instead of the decoded qualities. See the Colorspace alignment
#            section for details about decoding. This option is ignored in -S/--sam mode.
#        
#        --col-keepends
#            When decoding colorpsace alignments, bowtie trims off a nucleotide and quality
#            from the left and right edges of the alignment. This is because those
#            nucleotides are supported by only one color, in contrast to the middle
#            nucleotides which are supported by two. Specify --col-keepends to keep the
#            extreme-end nucleotides and qualities.
#            SAM
#        
#        -S/--sam
#            Print alignments in SAM format. See the SAM output section of the manual for
#            details. To suppress all SAM headers, use --sam-nohead in addition to -S/--sam.
#            To suppress just the @SQ headers (e.g. if the alignment is against a very large
#            number of reference sequences), use --sam-nosq in addition to -S/--sam. bowtie
#            does not write BAM files directly, but SAM output can be converted to BAM on the
#            fly by piping bowtie's output to samtools view. -S/--sam is not compatible with
#            --refout.
#        
#        --mapq <int>
#            If an alignment is non-repetitive (according to -m, --strata and other options)
#            set the MAPQ (mapping quality) field to this value. See the SAM Spec for details
#            about the MAPQ field Default: 255.
#        
#        --sam-nohead
#            Suppress header lines (starting with @) when output is -S/--sam. This must be
#            specified in addition to -S/--sam. --sam-nohead is ignored unless -S/--sam is
#            also specified.
#        
#        --sam-nosq
#            Suppress @SQ header lines when output is -S/--sam. This must be specified in
#            addition to -S/--sam. --sam-nosq is ignored unless -S/--sam is also specified.
#        
#        --sam-RG <text>
#            Add <text> (usually of the form TAG:VAL, e.g. ID:IL7LANE2) as a field on the @RG
#            header line. Specify --sam-RG multiple times to set multiple fields. See the SAM
#            Spec for details about what fields are legal. Note that, if any @RG fields are
#            set using this option, the ID and SM fields must both be among them to make the
#            @RG line legal according to the SAM Spec. --sam-RG is ignored unless -S/--sam is
#            also specified.
#    
#    
#    Performance
#        -o/--offrate <int>
#            Override the offrate of the index with <int>. If <int> is greater than the
#            offrate used to build the index, then some row markings are discarded when the
#            index is read into memory. This reduces the memory footprint of the aligner but
#            requires more time to calculate text offsets. <int> must be greater than the
#            value used to build the index.
#        
#        -p/--threads <int>
#            Launch <int> parallel search threads (default: 1). Threads will run on separate
#            processors/cores and synchronize when parsing reads and outputting alignments.
#            Searching for alignments is highly parallel, and speedup is fairly close to
#            linear. This option is only available if bowtie is linked with the pthreads
#            library (i.e. if BOWTIE_PTHREADS=0 is not specified at build time).
#        
#        --mm
#            Use memory-mapped I/O to load the index, rather than normal C file I/O.
#            Memory-mapping the index allows many concurrent bowtie processes on the same
#            computer to share the same memory image of the index (i.e. you pay the memory
#            overhead just once). This facilitates memory-efficient parallelization of
#            bowtie in situations where using -p is not possible.
#        
#        --shmem
#            Use shared memory to load the index, rather than normal C file I/O. Using shared
#            memory allows many concurrent bowtie processes on the same computer to share
#            the same memory image of the index (i.e. you pay the memory overhead just once).
#            This facilitates memory-efficient parallelization of bowtie in situations where
#            using -p is not desirable. Unlike --mm, --shmem installs the index into shared
#            memory permanently, or until the user deletes the shared memory chunks manually.
#            See your operating system documentation for details on how to manually list and
#            remove shared memory chunks (on Linux and Mac OS X, these commands are ipcs and
#            ipcrm). You may also need to increase your OS's maximum shared-memory chunk size
#            to accomodate larger indexes; see your OS documentation.
#
#
#    Other
#        --seed <int>
#            Use <int> as the seed for pseudo-random number generator.
#        
#        --verbose
#            Print verbose output (for debugging).
#        
#        --version
#            Print version information and quit.
#        
#        -h/--help
#            Print usage information and quit.
#



#bowtie-build [options]* <reference_in> <ebwt_base>
#
#Main arguments
#    <reference_in>
#        A comma-separated list of FASTA files containing the reference sequences
#        to be aligned to, or, if -c is specified, the sequences themselves.
#        E.g., <reference_in> might be chr1.fa,chr2.fa,chrX.fa,chrY.fa, or,
#        if -c is specified, this might be GGTCATCCT,ACGGGTCGT,CCGTTCTATGCGGCTTA.
#    <ebwt_base>
#        The basename of the index files to write. By default, bowtie-build
#        writes files named NAME.1.ebwt, NAME.2.ebwt, NAME.3.ebwt, NAME.4.ebwt,
#        NAME.rev.1.ebwt, and NAME.rev.2.ebwt, where NAME is <ebwt_base>.
#
#    Options
#        -f
#            The reference input files (specified as <reference_in>) are FASTA
#            files (usually having extension .fa, .mfa, .fna or similar).
#        
#        -c
#            The reference sequences are given on the command line.
#            I.e. <reference_in> is a comma-separated list of sequences rather
#            than a list of FASTA files.
#        
#        -C/--color
#            Build a colorspace index, to be queried using bowtie -C.
#        
#        -a/--noauto
#            Disable the default behavior whereby bowtie-build automatically
#            selects values for the --bmax, --dcv and --packed parameters
#            according to available memory. Instead, user may specify values for
#            those parameters. If memory is exhausted during indexing, an error
#            message will be printed; it is up to the user to try new parameters.
#        
#        -p/--packed
#            Use a packed (2-bits-per-nucleotide) representation for DNA strings.
#            This saves memory but makes indexing 2-3 times slower. Default: off.
#            This is configured automatically by default; use -a/--noauto to
#            configure manually.
#        
#        --bmax <int>
#            The maximum number of suffixes allowed in a block. Allowing more
#            suffixes per block makes indexing faster, but increases peak memory
#            usage. Setting this option overrides any previous setting for
#            --bmax, or --bmaxdivn. Default (in terms of the --bmaxdivn
#            parameter) is --bmaxdivn 4. This is configured automatically by
#            default; use -a/--noauto to configure manually.
#        
#        --bmaxdivn <int>
#            The maximum number of suffixes allowed in a block, expressed as a
#            fraction of the length of the reference. Setting this option
#            overrides any previous setting for --bmax, or --bmaxdivn.
#            Default: --bmaxdivn 4. This is configured automatically by
#            default; use -a/--noauto to configure manually.
#        
#        --dcv <int>
#            Use <int> as the period for the difference-cover sample. A larger
#            period yields less memory overhead, but may make suffix sorting
#            slower, especially if repeats are present. Must be a power of 2 no
#            greater than 4096. Default: 1024. This is configured automatically
#            by default; use -a/--noauto to configure manually.
#        
#        --nodc
#            Disable use of the difference-cover sample. Suffix sorting becomes
#            quadratic-time in the worst case (where the worst case is an
#            extremely repetitive reference). Default: off.
#        
#        -r/--noref
#            Do not build the NAME.3.ebwt and NAME.4.ebwt portions of the index,
#            which contain a bitpacked version of the reference sequences and
#            are used for paired-end alignment.
#        
#        -3/--justref
#            Build only the NAME.3.ebwt and NAME.4.ebwt portions of the index,
#            which contain a bitpacked version of the reference sequences and
#            are used for paired-end alignment.
#        
#        -o/--offrate <int>
#            To map alignments back to positions on the reference sequences,
#            it's necessary to annotate ("mark") some or all of the
#            Burrows-Wheeler rows with their corresponding location on the
#            genome. -o/--offrate governs how many rows get marked: the indexer
#            will mark every 2^<int> rows. Marking more rows makes
#            reference-position lookups faster, but requires more memory to hold
#            the annotations at runtime. The default is 5 (every 32nd row is
#            marked; for human genome, annotations occupy about 340 megabytes).
#        
#        -t/--ftabchars <int>
#            The ftab is the lookup table used to calculate an initial
#            Burrows-Wheeler range with respect to the first <int> characters of
#            the query. A larger <int> yields a larger lookup table but faster
#            query times. The ftab has size 4^(<int>+1) bytes. The default
#            setting is 10 (ftab is 4MB).
#        
#        --ntoa
#            Convert Ns in the reference sequence to As before building the
#            index. By default, Ns are simply excluded from the index and bowtie
#            will not report alignments that overlap them.
#        
#        --big --little
#            Endianness to use when serializing integers to the index file.
#            Default: little-endian (recommended for Intel- and AMD-based
#            architectures).
#        
#        --seed <int>
#            Use <int> as the seed for pseudo-random number generator.
#        
#        --cutoff <int>
#            Index only the first <int> bases of the reference sequences
#            (cumulative across sequences) and ignore the rest.
#        
#        -q/--quiet
#            bowtie-build is verbose by default. With this option bowtie-build
#            will print only error messages.
#        
#        -h/--help
#            Print usage information and quit.
#        
#        --version
#            Print version information and quit.
