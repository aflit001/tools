set terminal png size 1024,768 large font 'Courier New,12'
set output 'S_lycopersicum_scaffolds.fa.SL2.40sc05611.cov.probe.png'
set logscale y
set yrange [0.1:]
#plot [967164:970727] 'S_lycopersicum_scaffolds.fa.SL2.40sc05611.cov' using 3  axis x1y1 w impulses
plot 'S_lycopersicum_scaffolds.fa.SL2.40sc05611.cov.probe.cov' using 3  axis x1y1 w impulses
