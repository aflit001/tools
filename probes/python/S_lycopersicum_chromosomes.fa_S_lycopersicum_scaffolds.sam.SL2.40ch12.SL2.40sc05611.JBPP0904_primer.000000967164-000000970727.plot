
#set terminal png enhanced size 6628.2864177152,4096 large font "/usr/share/fonts/default/ghostscript/putr.pfa,16"
#set terminal png enhanced size 4096,6628.2864177152 font "/home/aflit001/temptive/probes/Courier.ttf,30"
set terminal png enhanced size 4096,6628.2864177152 font "Courier" 28
set output "tmp/run_BWA_JKL_17_JU_1_PROBES/S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.SL2.40sc05611.JBPP0904_primer.000000967164-000000970727.png"

set multiplot

#MAIN PLOT

set bars large

set style fill transparent solid 1.0 noborder
set key inside left top vertical Left reverse enhanced autotitles nobox
set key noinvert samplen 1 spacing 1 width 0 height 0
set style data filledcurves y1=0

unset colorbox
set grid
set palette model RGB
set pointsize 0.5
set ytics  out
set xtics
set mxtics 2
set mytics 2
#set format x "10^%L"
set bmargin 0



set title  'CHROM "SL2.40ch12" SCAF "SL2.40sc05611" PROBE NAME "JBPP0904\_primer" START "967164" END "970727"'
set ylabel 'Coverage Depth'
set xlabel 'Position Reference'
set xtics
set ytics
set label  'AVG COV     : 0.0581547113631249'                               at screen 0.05,0.995 front left
set label  'STD DEV COV : 0.0213822974428993 (36%)'                 at screen 0.05,0.988 front left
set label  'MAX COV     : 0.111776447106282'                               at screen 0.05,0.981 front left
set label  'MIN COV     : 0'                               at screen 0.05,0.974 front left
set label  'LENG REF    : 3565'                          at screen 0.05,0.967 front left
set label  'START       : 0 END: 3564 (3565)' at screen 0.05,0.960 front left


set xrange [0:3564]
set yrange [0:1]
set origin 0, 0.81
set size   1, 0.15
plot 'tmp/run_BWA_JKL_17_JU_1_PROBES/S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.SL2.40sc05611.JBPP0904_primer.000000967164-000000970727.S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.sam.SL2.40sc05611.sam.cov.prop.cov' using 1:2 lt rgb "#FF0000" with filledcurves title 'Coverage'

unset xrange
unset yrange
unset origin
unset size
unset title
unset xlabel
unset ylabel
unset label 1
unset label 2
unset label 3
unset label 4
unset label 5
unset label 6

unset style fill
unset style data
unset key
unset xtics
unset ytics
unset mxtics
unset mytics





#RANGES

set style line 99 lt rgb 'black' lw 6
set format y '%1.0te%T'
set ylabel 'COVERAGE'
set ytics  out
set xrange [0:3564]
set yrange [0:*]
#set mytics

set origin 0.005,  0.715
set size   0.995, 0.05

plot 'tmp/run_BWA_JKL_17_JU_1_PROBES/S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.SL2.40sc05611.JBPP0904_primer.000000967164-000000970727.S_lycopersicum_chromosomes.fa_S_lycopersicum_chromosomes.pos.SL2.40ch12.pos.cov.SL2.40sc05611.cov' using 1:2 lt rgb "black" with filledcurves title 'COVERAGE'

unset ylabel
unset ytics
unset xrange
unset yrange
unset origin
unset size



set style line 99 lt rgb 'black' lw 6
set format y '%1.0te%T'
set ylabel 'BLAST'
set ytics  out
set xrange [0:3564]
set yrange [0:*]
#set mytics

set origin 0.005,  0.665
set size   0.995, 0.05

plot 'tmp/run_BWA_JKL_17_JU_1_PROBES/S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.SL2.40sc05611.JBPP0904_primer.000000967164-000000970727_Product.fasta.blast.cov' using 1:2 lt rgb "black" with filledcurves title 'BLAST'

unset ylabel
unset ytics
unset xrange
unset yrange
unset origin
unset size




#CONTIGS

set style line 99 lt rgb 'black' lw 6
set ylabel 'CONTIG' font "Courier,16"
#set ytics  out
set xrange [0:3564]
set yrange [0:1]
#unset mytics

set origin 0.042 , 0.775
set size   0.958, 0.015

plot 'tmp/run_BWA_JKL_17_JU_1_PROBES/S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.SL2.40sc05611.JBPP0904_primer.000000967164-000000970727.S_lycopersicum_scaffolds_from_contigs.2.40.agp.SL2.40sc05611.agp.contig.agp.cov' using 1:2 lt rgb "black" with filledcurves title 'CONTIG'

unset ylabel
unset xrange
unset yrange
unset origin
unset size



set style line 99 lt rgb 'black' lw 6
set ylabel 'GAP' font "Courier,16"
#set ytics  out
set xrange [0:3564]
set yrange [0:1]
#unset mytics

set origin 0.042 , 0.77
set size   0.958, 0.015

plot 'tmp/run_BWA_JKL_17_JU_1_PROBES/S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.SL2.40sc05611.JBPP0904_primer.000000967164-000000970727.S_lycopersicum_scaffolds_from_contigs.2.40.agp.SL2.40sc05611.agp.gap.agp.cov' using 1:2 lt rgb "black" with filledcurves title 'GAP'

unset ylabel
unset xrange
unset yrange
unset origin
unset size



set style line 99 lt rgb 'black' lw 6
set ylabel 'UNK' font "Courier,16"
#set ytics  out
set xrange [0:3564]
set yrange [0:1]
#unset mytics

set origin 0.042 , 0.765
set size   0.958, 0.015

plot 'tmp/run_BWA_JKL_17_JU_1_PROBES/S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.SL2.40sc05611.JBPP0904_primer.000000967164-000000970727.S_lycopersicum_scaffolds_from_contigs.2.40.agp.SL2.40sc05611.agp.unknown.agp.cov' using 1:2 lt rgb "black" with filledcurves title 'UNK'

unset ylabel
unset xrange
unset yrange
unset origin
unset size



set style line 99 lt rgb 'black' lw 6
set ylabel 'OTHER' font "Courier,16"
#set ytics  out
set xrange [0:3564]
set yrange [0:1]
#unset mytics

set origin 0.042 , 0.76
set size   0.958, 0.015

plot 'tmp/run_BWA_JKL_17_JU_1_PROBES/S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.SL2.40sc05611.JBPP0904_primer.000000967164-000000970727.S_lycopersicum_scaffolds_from_contigs.2.40.agp.SL2.40sc05611.agp.other.agp.cov' using 1:2 lt rgb "black" with filledcurves title 'OTHER'

unset ylabel
unset xrange
unset yrange
unset origin
unset size



set style line 99 lt rgb 'black' lw 6
set ylabel 'Ns' font "Courier,16"
#set ytics  out
set xrange [0:3564]
set yrange [0:1]
#unset mytics

set origin 0.042 , 0.755
set size   0.958, 0.015

plot 'tmp/run_BWA_JKL_17_JU_1_PROBES/S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.SL2.40sc05611.JBPP0904_primer.000000967164-000000970727.S_lycopersicum_scaffolds.fa_NONE.tab.SL2.40ch12.tab.SL2.40sc05611.tab.cov' using 1:2 lt rgb "black" with filledcurves title 'Ns'

unset ylabel
unset xrange
unset yrange
unset origin
unset size





#OTHER PLOTS

set bars large

set style fill transparent solid 1.0 noborder
set key inside left top vertical Left reverse enhanced autotitles nobox
set key noinvert samplen 1 spacing 1 width 0 height 0
set style data filledcurves y1=0

unset colorbox
set grid
set palette model RGB
set pointsize 0.5
set ytics  out
set xtics
set mxtics 2
set mytics 2
#set format x "10^%L"
set bmargin 0


#set title '500 bp'
set origin 0, 0.577857142857143
set size   1, 0.0821428571428571
set xtics
set ytics
set ylabel 'Coverage Depth'
set xlabel 'Position Reference'
plot 'tmp/run_BWA_JKL_17_JU_1_PROBES/S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.SL2.40sc05611.JBPP0904_primer.000000967164-000000970727.S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.sam.SL2.40sc05611.sam.cov.prop.cov' using 1:3 lc rgb "#FF7F00" with filledcurves title '500 bp bp'
unset title
unset origin
unset size
unset ylabel
unset xlabel
unset style fill
unset style data
unset key
unset xtics
unset ytics
unset mxtics
unset mytics


  
set bars large

set style fill transparent solid 1.0 noborder
set key inside left top vertical Left reverse enhanced autotitles nobox
set key noinvert samplen 1 spacing 1 width 0 height 0
set style data filledcurves y1=0

unset colorbox
set grid
set palette model RGB
set pointsize 0.5
set ytics  out
set xtics
set mxtics 2
set mytics 2
#set format x "10^%L"
set bmargin 0


#set title '2.5 kbp'
set origin 0, 0.490714285714286
set size   1, 0.0821428571428571
set xtics
set ytics
set ylabel 'Coverage Depth'
set xlabel 'Position Reference'
plot 'tmp/run_BWA_JKL_17_JU_1_PROBES/S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.SL2.40sc05611.JBPP0904_primer.000000967164-000000970727.S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.sam.SL2.40sc05611.sam.cov.prop.cov' using 1:4 lc rgb "#FFFF00" with filledcurves title '2.5 kbp bp'
unset title
unset origin
unset size
unset ylabel
unset xlabel
unset style fill
unset style data
unset key
unset xtics
unset ytics
unset mxtics
unset mytics


  
set bars large

set style fill transparent solid 1.0 noborder
set key inside left top vertical Left reverse enhanced autotitles nobox
set key noinvert samplen 1 spacing 1 width 0 height 0
set style data filledcurves y1=0

unset colorbox
set grid
set palette model RGB
set pointsize 0.5
set ytics  out
set xtics
set mxtics 2
set mytics 2
#set format x "10^%L"
set bmargin 0


#set title '5 kbp'
set origin 0, 0.403571428571428
set size   1, 0.0821428571428571
set xtics
set ytics
set ylabel 'Coverage Depth'
set xlabel 'Position Reference'
plot 'tmp/run_BWA_JKL_17_JU_1_PROBES/S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.SL2.40sc05611.JBPP0904_primer.000000967164-000000970727.S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.sam.SL2.40sc05611.sam.cov.prop.cov' using 1:5 lc rgb "#7FFF00" with filledcurves title '5 kbp bp'
unset title
unset origin
unset size
unset ylabel
unset xlabel
unset style fill
unset style data
unset key
unset xtics
unset ytics
unset mxtics
unset mytics


  
set bars large

set style fill transparent solid 1.0 noborder
set key inside left top vertical Left reverse enhanced autotitles nobox
set key noinvert samplen 1 spacing 1 width 0 height 0
set style data filledcurves y1=0

unset colorbox
set grid
set palette model RGB
set pointsize 0.5
set ytics  out
set xtics
set mxtics 2
set mytics 2
#set format x "10^%L"
set bmargin 0


#set title '50 kbp'
set origin 0, 0.316428571428571
set size   1, 0.0821428571428571
set xtics
set ytics
set ylabel 'Coverage Depth'
set xlabel 'Position Reference'
plot 'tmp/run_BWA_JKL_17_JU_1_PROBES/S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.SL2.40sc05611.JBPP0904_primer.000000967164-000000970727.S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.sam.SL2.40sc05611.sam.cov.prop.cov' using 1:6 lc rgb "#00FF00" with filledcurves title '50 kbp bp'
unset title
unset origin
unset size
unset ylabel
unset xlabel
unset style fill
unset style data
unset key
unset xtics
unset ytics
unset mxtics
unset mytics


  
set bars large

set style fill transparent solid 1.0 noborder
set key inside left top vertical Left reverse enhanced autotitles nobox
set key noinvert samplen 1 spacing 1 width 0 height 0
set style data filledcurves y1=0

unset colorbox
set grid
set palette model RGB
set pointsize 0.5
set ytics  out
set xtics
set mxtics 2
set mytics 2
#set format x "10^%L"
set bmargin 0


#set title '1 Mbp'
set origin 0, 0.229285714285714
set size   1, 0.0821428571428571
set xtics
set ytics
set ylabel 'Coverage Depth'
set xlabel 'Position Reference'
plot 'tmp/run_BWA_JKL_17_JU_1_PROBES/S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.SL2.40sc05611.JBPP0904_primer.000000967164-000000970727.S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.sam.SL2.40sc05611.sam.cov.prop.cov' using 1:7 lc rgb "#00FF7F" with filledcurves title '1 Mbp bp'
unset title
unset origin
unset size
unset ylabel
unset xlabel
unset style fill
unset style data
unset key
unset xtics
unset ytics
unset mxtics
unset mytics


  
set bars large

set style fill transparent solid 1.0 noborder
set key inside left top vertical Left reverse enhanced autotitles nobox
set key noinvert samplen 1 spacing 1 width 0 height 0
set style data filledcurves y1=0

unset colorbox
set grid
set palette model RGB
set pointsize 0.5
set ytics  out
set xtics
set mxtics 2
set mytics 2
#set format x "10^%L"
set bmargin 0


#set title '5 kbp before'
set origin 0, 0.142142857142857
set size   1, 0.0821428571428571
set xtics
set ytics
set ylabel 'Coverage Depth'
set xlabel 'Position Reference'
plot 'tmp/run_BWA_JKL_17_JU_1_PROBES/S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.SL2.40sc05611.JBPP0904_primer.000000967164-000000970727.S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.sam.SL2.40sc05611.sam.cov.prop.cov' using 1:8 lc rgb "#00FFFF" with filledcurves title '5 kbp before bp'
unset title
unset origin
unset size
unset ylabel
unset xlabel
unset style fill
unset style data
unset key
unset xtics
unset ytics
unset mxtics
unset mytics


  
set bars large

set style fill transparent solid 1.0 noborder
set key inside left top vertical Left reverse enhanced autotitles nobox
set key noinvert samplen 1 spacing 1 width 0 height 0
set style data filledcurves y1=0

unset colorbox
set grid
set palette model RGB
set pointsize 0.5
set ytics  out
set xtics
set mxtics 2
set mytics 2
#set format x "10^%L"
set bmargin 0


#set title '5 kbp after'
set origin 0, 0.055
set size   1, 0.0821428571428571
set xtics
set ytics
set ylabel 'Coverage Depth'
set xlabel 'Position Reference'
plot 'tmp/run_BWA_JKL_17_JU_1_PROBES/S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.SL2.40sc05611.JBPP0904_primer.000000967164-000000970727.S_lycopersicum_chromosomes.fa_S_lycopersicum_scaffolds.sam.SL2.40ch12.sam.SL2.40sc05611.sam.cov.prop.cov' using 1:9 lc rgb "#007FFF" with filledcurves title '5 kbp after bp'
unset title
unset origin
unset size
unset ylabel
unset xlabel
unset style fill
unset style data
unset key
unset xtics
unset ytics
unset mxtics
unset mytics


  


unset multiplot

exit

