#!/usr/bin/python
import cPickle as pickle
import sys,os,time,datetime
from   Bio.Seq                import Seq
from   Bio.Alphabet           import IUPAC
from   struct                 import *

kmer = 32

#perl -ne 'chomp; print if substr($_, 0, 1) ne ">"' keys2.fasta | perl -ne 'for (my $i = 0; $i < length($_); $i++) { $s= substr($_,$i,32) . "\n"; print $s if length($s) == 33 }' > kmers2.keys
#./queryDb.py /run/shm/reader/S_lycopersicum_scaffolds.fa.pickle $(cat kmers2.keys)

def genkeys():
    dnak = {}
    dnav = []
    voc  = ['A', 'C','G','T']
    for st in voc:
        for nd in voc:
            for rd in voc:
                for th in voc:
                    key = st+nd+rd+th
                    dnav.append(key)
                    dnak[key] = chr(len(dnav) - 1)
    return [dnav, dnak]

def rc(seq):
    revco = seq.reverse_complement()
    #print "    COMPARING",seq,"AGAINST",revco
    if str(revco) < str(seq):
        #print "      REVCO"
        return [True, revco]
    else:
        #print "      ORIG"
        return [False, seq]

def convert(seq, keys, vals):
    res = ""
    start = 0

    while start < len(seq):
        s      = seq[start:start + 4]
        #print "    START",start,"SEQ",s
        res   += keys[str(s).upper()]
        start += 4

    #pp.pprint(res)
    res1 = res
    #while len(res1) < 8: res1 += "\x00"

    #print "seq  (",sys.getsizeof(seq ),")",seq
    #print "res  (",sys.getsizeof(res ),")",
    #pp.pprint(res)

    #print "res1 (",sys.getsizeof(res1),")",
    #pp.pprint(res1)

    res2 = unpack('Q', res1)[0]
    #print "res2 (",sys.getsizeof(res2),")",res2,sys.getsizeof(str(res2))

    #res3 = pack  ('Q',   res2)
    #print "res3 (",sys.getsizeof(res3),")",
    #pp.pprint(res3)

    #res4 = ''
    #for char in res3:
        #res4 += dnav[ord(char)]
    #res4 = res4[:kmer]
    #print "res4 (",sys.getsizeof(res4),")",res4

    #sys.exit(0)
    return res2
    #return res

def seconds2str(seconds):
    timeStr   = str(datetime.timedelta(seconds=seconds))
    pointpos  = timeStr.rfind('.')
    return timeStr[:pointpos]


def deconvert(keys, seq):
    conv = pack  ('Q',   seq)
    #print "conv (",sys.getsizeof(conv),")",
    #pp.pprint(conv)

    res = ''
    for char in conv:
        res += dnav[ord(char)]
    res = res[:kmer]
    #print "res (",sys.getsizeof(res),")",res

    return res

def timer(lastTime):
    newTime   = time.time()
    timeDiff  = newTime - lastTime
    return seconds2str(timeDiff)
    #timeHours = timediff / 60.0 / 60.0 / 60.0
    #timeMinut = (timediff / 60.0 / 60.0) - int(timeHours *
    #timeSecon = timediff / 60.0
    #
    #timeUnity = "s"
    #if timediff > 60:
    #    timediff  = timediff / 60.0 # min
    #    timeUnity = "m"
    #if timediff > 60:
    #    timediff  = timediff / 60.0 # min
    #    timeUnity = "h"

    #return [timediff, timeUnity]
    pass


def pickleLoad(ob, fi):
    if os.path.exists(fi):
        lastTime = time.time()
        print "LOADING",fi,
        sys.stdout.flush()
        file = open(fi, 'rb')
        ob   = pickle.load(file)
        file.close()
        timeStr = timer(lastTime)
        print "LOADED IN %s" % (timeStr)
    return ob

def pickleDump(ob, fi):
    lastTime = time.time()
    print "PICKLING",
    sys.stdout.flush()
    file = open(fi+'.tmp', 'wb')
    pickle.dump(ob, file, protocol=pickle.HIGHEST_PROTOCOL)
    file.close()
    if os.path.exists(fi):
        os.remove(fi)
    os.rename(fi+'.tmp', fi)
    timeStr = timer(lastTime)
    print "PICKLED IN %s" % (timeStr)





if __name__ == '__main__':
    outPickleFile = None
    if len(sys.argv) < 3:
        sys.stderr.write("NO FILE NAME GIVEN NOR SEQUENCE TO SEARCH\n")
        sys.exit(1)
    else:
        outPickleFile = sys.argv[1]
        seqs          = sys.argv[2:]

    dnav, dnak = genkeys()


    if not os.path.exists(outPickleFile):
        sys.stderr.write("INPUT FILE %s DOES NOT EXISTS\n" % outPickleFile)
        sys.exit(2)

    sequencesUniquenes = {}
    if os.path.exists(outPickleFile):
        print "  DUMP EXISTS. READING"
        sequencesUniquenes = pickleLoad(sequencesUniquenes, outPickleFile)
        print "  DUMP LOADED"

    uniquenessLength = len(sequencesUniquenes)
    if uniquenessLength == 0:
        sys.stderr.write("ERROR CREATING UNIQUENESS DB. LENGTH 0\n")
        sys.exit(1)

    print "  UNIQUENESS DB CREATED. TOTAL: %d" % (uniquenessLength)





    for seq in seqs:
        #print "SUB",sub
        if len(seq) == kmer:
            sub = Seq(seq.upper(), IUPAC.unambiguous_dna)
            if sub.find('N') == -1:
                conv2    = convert(sub, dnak, dnav)
                rev, sub = rc(sub)
                conv     = convert(sub, dnak, dnav)


                if  sequencesUniquenes.has_key(conv):
                    data = sequencesUniquenes[conv]
                    #print "  FILE %s CHRNUM %d LENGTH %d SUB %s CONV %s DATA %s" %(fileName, chrNum, length, sub, conv, pp.pformat(data))
                    dSum   = data[0]
                    dCount = data[1]
                    dAvg   = data[2]
                    #file.write("\t%d\t%d\t%s\t%d\t%d\t%d\n" % (pos, pos+kmer, sub, dSum, dCount, dAvg))
                    print "O %s\t%s\t%.1f\t%d\t%.1f" % (seq,sub, dSum, dCount, dAvg)
                if conv != conv2 and sequencesUniquenes.has_key(conv2):
                    data = sequencesUniquenes[conv2]
                    #print "  FILE %s CHRNUM %d LENGTH %d SUB %s CONV %s DATA %s" %(fileName, chrNum, length, sub, conv, pp.pformat(data))
                    dSum   = data[0]
                    dCount = data[1]
                    dAvg   = data[2]
                    #file.write("\t%d\t%d\t%s\t%d\t%d\t%d\n" % (pos, pos+kmer, sub, dSum, dCount, dAvg))
                    print "M %s\t%s\t%.1f\t%d\t%.1f" % (seq,seq, dSum, dCount, dAvg)

            else:
                pass
