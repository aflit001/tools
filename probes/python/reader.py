#!/usr/bin/python
from   Bio                    import SeqIO
from   Bio.Seq                import Seq
from   Bio.Alphabet           import IUPAC
from   Bio.Blast.Applications import NcbiblastnCommandline
from   multiprocessing        import Process, Manager
from   struct                 import *
from   subprocess             import call
import pipes
import pprint
import cPickle as pickle
import base64
import sys,os,time,datetime

#import pickle

blastPartsKb           = 100
kmer                   = 30
printEvery             = 1
stepSize               = kmer / 2
#tmpDir                = './tmp/'
tmpDir                 = '/run/shm/'

removeTemps            = False
removeTempsAll         = False

overwriteKeys          = False
overwriteKeysSort      = False
overwriteSort          = False
overwriteMerged        = False
overwriteBlast         = True
DoNotRunBlastSeparator = False
DoNotRunBlast          = False
DoNotRunBlastMerge     = False
overwritePickle        = True
overwriteCov           = True
overwritePng           = True



tmpDir            = os.path.join(tmpDir, 'reader')
if not os.path.exists(tmpDir):
    os.makedirs(tmpDir)


while kmer % 4 != 0: kmer += 1
#if kmer > 32: kmer = 32
minScore = int(kmer * 0.8)
print "KMERLENGTH",kmer


def genkeys():
    dnak = {}
    dnav = []
    voc  = ['A', 'C','G','T']
    for st in voc:
        for nd in voc:
            for rd in voc:
                for th in voc:
                    key = st+nd+rd+th
                    dnav.append(key)
                    dnak[key] = chr(len(dnav) - 1)
    return [dnav, dnak]
def pickleLoad(ob, fi):
    if os.path.exists(fi):
        lastTime = time.time()
        print "LOADING",fi,
        sys.stdout.flush()
        file = open(fi, 'rb')
        ob   = pickle.load(file)
        file.close()
        timeStr = timer(lastTime)
        print "LOADED IN %s" % (timeStr)
    return ob

def pickleDump(ob, fi):
    lastTime = time.time()
    print "PICKLING",
    sys.stdout.flush()
    file = open(fi+'.tmp', 'wb')
    pickle.dump(ob, file, protocol=pickle.HIGHEST_PROTOCOL)
    file.close()
    if os.path.exists(fi):
        os.remove(fi)
    os.rename(fi+'.tmp', fi)
    timeStr = timer(lastTime)
    print "PICKLED IN %s" % (timeStr)

def seconds2str(seconds):
    timeStr   = str(datetime.timedelta(seconds=seconds))
    pointpos  = timeStr.rfind('.')
    return timeStr[:pointpos]

def timer(lastTime):
    newTime   = time.time()
    timeDiff  = newTime - lastTime
    return seconds2str(timeDiff)
    #timeHours = timediff / 60.0 / 60.0 / 60.0
    #timeMinut = (timediff / 60.0 / 60.0) - int(timeHours *
    #timeSecon = timediff / 60.0
    #
    #timeUnity = "s"
    #if timediff > 60:
    #    timediff  = timediff / 60.0 # min
    #    timeUnity = "m"
    #if timediff > 60:
    #    timediff  = timediff / 60.0 # min
    #    timeUnity = "h"

    #return [timediff, timeUnity]
    pass


def calcEta(initialTime, amountDone, amountTotal):
    timeTaken     = time.time() - initialTime
    timePerAmount = timeTaken * 1.0 / amountDone * 1.0
    amountLeft    = amountTotal     - amountDone
    timeLeft      = timePerAmount   * amountLeft
    return seconds2str(timeLeft)



class posData():
    def __init__(self, chrNum, posChr, rev, seqConv):
        self.chrNum   = chrNum
        self.posChr   = posChr
        self.rev      = rev
        self.seqConv  = seqConv




def convert(seq, keys, vals):
    res = ""
    start = 0

    while start < len(seq):
        s      = seq[start:start + 4]
        #print "    START",start,"SEQ",s
        res   += keys[str(s).upper()]
        start += 4

    #pp.pprint(res)
    res1 = res
    #while len(res1) < 8: res1 += "\x00"

    #print "seq  (",sys.getsizeof(seq ),")",seq
    #print "res  (",sys.getsizeof(res ),")",
    #pp.pprint(res)

    #print "res1 (",sys.getsizeof(res1),")",
    #pp.pprint(res1)

    res2 = unpack('Q', res1)[0]
    #print "res2 (",sys.getsizeof(res2),")",res2,sys.getsizeof(str(res2))

    #res3 = pack  ('Q',   res2)
    #print "res3 (",sys.getsizeof(res3),")",
    #pp.pprint(res3)

    #res4 = ''
    #for char in res3:
        #res4 += dnav[ord(char)]
    #res4 = res4[:kmer]
    #print "res4 (",sys.getsizeof(res4),")",res4

    #sys.exit(0)
    return res2
    #return res

def deconvert(keys, seq):
    conv = pack  ('Q',   seq)
    #print "conv (",sys.getsizeof(conv),")",
    #pp.pprint(conv)

    res = ''
    for char in conv:
        res += dnav[ord(char)]
    res = res[:kmer]
    #print "res (",sys.getsizeof(res),")",res

    return res

def rc(seq):
    revco = seq.reverse_complement()
    #print "    COMPARING",seq,"AGAINST",revco
    if str(revco) < str(seq):
        #print "      REVCO"
        return [True, revco]
    else:
        #print "      ORIG"
        return [False, seq]


def line_len(filename):
    print "  READING LENGTH OF",filename
    with open(filename) as f:
        for i, l in enumerate(f):
            pass
        print "  LENGTH OBTAINED:",i+1
        return i + 1




def readSeq(chrNum, length, seq):
    pos           = 0
    lastPercPrint = 0
    lastPercDump  = 0
    initialTime   = time.time()
    lastTime      = initialTime

    while pos < length - kmer:
        pos += stepSize
        #print "CHRNUM",chrNum,"LENGTH",length,"POS",pos
        perc = int(pos * 100/length)
        if perc % printEvery == 0 and perc != lastPercPrint:
            lastTimeStr  = timer(lastTime)
            totalTimeStr = timer(initialTime)
            eta          = calcEta(initialTime, pos, length)

            sys.stdout.write("  READSEQ CHROM %5d POS %12d / %12d %6.2f%% - LAST 1%% DONE IN %s - TOTAL TIME %s - ETA %s\n" % (chrNum, pos,length, perc, lastTimeStr, totalTimeStr, eta))

            lastPercPrint = perc
            lastTime = time.time()

        sub = Seq("".join(seq[pos:pos+kmer]).upper(), IUPAC.unambiguous_dna)
        yield [pos,sub]


def mapSeq(dnaV, dnaK, fileName, sequencesUniquenes, chrNum, length, seq):
    file          = open(fileName, 'w')
    for pair in readSeq(chrNum, length, seq):
        pos, sub = pair
        #print "SUB",sub
        if len(sub) == kmer:
            if sub.find('N') == -1:
                rev, sub = rc(sub)
                conv     = convert(sub, dnak, dnav)
                #print " CHROM",chrNum,"POS",pos,"SEQ",seq,"CONV",conv
                #data     = base64.b64encode(pack('HL?', chrNum, pos, rev))#;, {chrNum, pos, rev, conv}
                #print "CHROMNUM %3d POS %4d" % (chrNum, pos)
                data = [-1.0, 0.0, -1.0]
                if sequencesUniquenes.has_key(conv):
                    data = sequencesUniquenes[conv]
                    #print "  FILE %s CHRNUM %d LENGTH %d SUB %s CONV %s DATA %s" %(fileName, chrNum, length, sub, conv, pp.pformat(data))
                dSum   = data[0]
                dCount = data[1]
                dAvg   = data[2]
                #file.write("\t%d\t%d\t%s\t%d\t%d\t%d\n" % (pos, pos+kmer, sub, dSum, dCount, dAvg))
                file.write("%d\t%d\t%.1f\t%d\t%.1f\n" % (pos, pos+kmer-1, dSum, dCount, dAvg))
            else:
                pass
    file.close()



def doSeq(dnaV, dnaK, fileName, chrNum, length, seq):
    try:
        p = pipes.Template()
        p.append("sort --key=1 --compress-program=pigz --parallel=4 --temporary-directory=%(tmpdir)s" % { 'tmpdir':tmpDir }, '--')
        p.debug(True)

        try:
            outFile = p.open(fileName, 'w')

        except Exception as inst:
            print "DO SEQ ERR 1 :: TYPE",type(inst)
            print "DO SEQ ERR 1 :: ARGS",inst.args
            print "DO SEQ ERR 1 :: INST",inst

            sys.stderr.write("ERROR RUNNING DO SEQ ON FILE %s CHROM %s - ERROR OPENING\n" % (fileName, chrNum))
            #sys.exit(1)
            return 1
        #file          = open(fileName, 'w')
        for pair in readSeq(chrNum, length, seq):
            pos, sub = pair
            if len(sub) == kmer:
                if sub.find('N') == -1:
                    rev, sub = rc(sub)
                    #conv     = convert(sub, dnak, dnav)
                    #print " CHROM",chrNum,"POS",pos,"SEQ",seq,"CONV",conv
                    data     = base64.b64encode(pack('HL?', chrNum, pos, rev))#;, {chrNum, pos, rev, conv}
                    #print "CHROMNUM %3d POS %4d" % (chrNum, pos)
                    outFile.write("%s\t%s\n" % (sub, data))
        outFile.close()

    except Exception as inst:
        print "DO SEQ ERR 2 :: TYPE",type(inst)
        print "DO SEQ ERR 2 :: ARGS",inst.args
        print "DO SEQ ERR 2 :: INST",inst

        sys.stderr.write("ERROR RUNNING DO SEQ ON FILE %s CHROM %s\n" % (fileName, chrNum))
        #sys.exit(1)
        return 1

def sortFile(inFile, outFile):
    #--numeric-sort
    ret = call("sort --key=1 --compress-program=pigz --parallel=4 --temporary-directory=%(tmpdir)s \"%(input)s\"  | pv --progress --timer --eta --rate --average-rate --bytes --size %(size)d --cursor --name \"%(input)s\" > \"%(output)s\"" % {'input': inFile, 'output': outFile, 'size':os.path.getsize(inFile), 'tmpdir':tmpDir }, shell=True)

    if ret == 0:
        if os.path.getsize(inFile) != os.path.getsize(outFile):
            os.remove(outFile)
            sys.stderr.write("ERROR SORTING FILE %s TO %s. RETURN CODE %d. ERROR CREATING FILE\n" % (inFile, outFile, ret))
            #sys.exit(1)
            return 1
        else:
            if removeTemps:
                os.remove(inFile)
    else:
        sys.stderr.write("ERROR SORTING FILE %s TO %s. RETURN CODE %d. NOT 0\n" % (inFile, outFile, ret))
        #sys.exit(1)
        return 1


def splitFasta(fastaFileName, tmpDir, size):
    if os.path.exists(tmpDir):
        for f in os.listdir(tmpDir):
            os.remove(os.path.join(tmpDir, f))
    else:
        os.makedirs(tmpDir)

    counter      = 0
    totalLen     = 0
    outFileName  = os.path.join(tmpDir, "fasta_tmp_%07d.fasta" % (counter))
    fastaFile    = open(fastaFileName, 'r')
    outFile      = open(outFileName,   'w')

    for line in fastaFile:
        totalLen += len(line)
        if line[0] == '>':
            if totalLen >= size:
                outFile.close()
                counter     += 1
                outFileName  = os.path.join(tmpDir, "fasta_tmp_%07d.fasta" % (counter))
                outFile      = open(outFileName,   'w')
                totalLen     = 0
                totalLen    += len(line)
                outFile.write(line)
            else:
                outFile.write(line)
        else:
            outFile.write(line)
    outFile.close()
    fastaFile.close()

    if os.path.getsize(outFileName) == 0:
        os.remove(outFileName)

def mergeBlast(inputDir, outputFileName):
    if os.path.exists(outputFileName):
        os.remove(outputFileName)

    if os.path.exists(inputDir):
        outputFile = open(outputFileName, "w")
        sumSize    = 0
        files      = os.listdir(inputDir)
        numFiles   = len(files)

        timeStart    = time.time()
        lastTime     = timeStart
        fileCount    = 0

        for fileName in files:
            fileName   = os.path.join(inputDir, fileName)
            sumSize   += os.path.getsize(fileName)
            fileCount += 1

            if fileCount % ( numFiles / 100 ) == 0:
                perc      = (fileCount * 100.0/numFiles)
                timeBegin = timer(timeStart)
                timeLast  = timer(lastTime)
                eta       = calcEta(timeStart, fileCount, numFiles)

                print "  MERGEBLAST LINE %12d / %12d %6.2f%% - LAST 1%% DONE IN %s - TOTAL TIME %s - ETA %s" %( fileCount, numFiles, perc, timeLast, timeBegin, eta )
                lastTime = time.time()

            if not os.path.exists(fileName):
                sys.stderr.write("ERROR MERGING BLAST. FILE %s IN %s DOES NOT EXISTS\n" % (fileName, inputDir))
                sys.exit(1)
            else:
                with open(fileName, 'r') as f:
                    for line in f:
                        outputFile.write(line)
                if removeTemps:
                    os.remove(fileName)

        outputFile.close()
        outFileSize = os.path.getsize(outputFileName)
        if outFileSize != sumSize:
            sys.stderr.write("ERROR MERGING BLAST. OUTPUT FILE %s FROM %s HAS WRONG SIZE. %d INSTEAD OF %d\n" % (outputFile, inputDir, outFileSize, sumSize))
            #sys.exit(1)
            return 1
        else:
            print "    SIZES MATCH. PROCEDING"
    else:
        sys.stderr.write("ERROR MERGING BLAST. INPUT DIR %s DOES NOT EXISTS\n" % (inputDir))
        #sys.exit(1)
        return 1



if __name__ == '__main__':

    input = None
    print "argv",sys.argv,"len",len(sys.argv)
    if len(sys.argv) >= 2:
        print "argv",sys.argv
        input = sys.argv[1]
        print "lib",input
        if not os.path.exists(input):
            sys.stderr.write("INPUT FILE "+input+" DOES NOT EXISTS\n")
            sys.exit(1)

        while os.path.islink(input):
            input = os.path.abspath(input)

        if not os.path.isfile(input):
            sys.stderr.write("INPUT FILE "+input+" IS NOT A FILE\n")
            sys.exit(2)

        if os.path.getsize(input) == 0:
            sys.stderr.write("INPUT FILE "+input+" IS EMPTY\n")
            sys.exit(3)
    else:
        sys.stderr.write("NO INPUT FILE GIVEN\n")
        sys.exit(4)


    inputDb = None
    if len(sys.argv) == 3:
        inputDb = sys.argv[2]
        if not os.path.exists(inputDb):
            sys.stderr.write("INPUT DB FILE "+inputDb+" DOES NOT EXISTS\n")
            sys.exit(1)

        while os.path.islink(inputDb):
            inputDb = os.path.abspath(inputDb)

        if not os.path.isfile(inputDb):
            sys.stderr.write("INPUT DB FILE "+inputDb+" IS NOT A FILE\n")
            sys.exit(2)

        if os.path.getsize(inputDb) == 0:
            sys.stderr.write("INPUT DB FILE "+inputDb+" IS EMPTY\n")
            sys.exit(3)
    else:
        sys.stderr.write("NO INPUT DB FILE GIVEN. USING SAME FASTA TWICE\n")
        inputDb = input

    print "here"

    dnav, dnak = genkeys()
    pp = pprint.PrettyPrinter(indent=6)
    #pp.pprint( dnav )
    #pp.pprint( dnak )





    chroms       = {}
    chrNums      = []
    processes    = []
    baseInput    = os.path.basename(input)
    baseInputDb  = os.path.basename(inputDb)
    baseInputTmp = os.path.join(tmpDir, baseInput)

    print "CREATING INTERMEDIATE KEY FILES"
    for seq_record in SeqIO.parse(input, "fasta"):
        id           = seq_record.id
        length       = len(seq_record)
        chrNums.append(id)
        chrNum       = len(chrNums) - 1
        fileName     = "%s.%04d.keys" % (baseInputTmp, chrNum)
        chroms[id]   = { 'length': length, 'fileName': fileName, 'chrNum':chrNum }
        seq          = seq_record.seq
        print "  ID %s LEN %7d" % (id,length)

        if not os.path.exists(fileName) or overwriteKeys:
            processes.append(Process(target=doSeq, args=[ dnav, dnak, fileName, chrNum, length, seq ]))

    if len(processes) > 0:
        print "  CREATING KEY FILES :: SPAWNING %3d PROCESSES" % (len(processes))
        for process in processes:
            process.start()
        print "  CREATING KEY FILES :: PROCESSES STARTED"

        print "  CREATING KEY FILES :: JOINING PROCESSES"
        for process in processes:
            ec = process.exitcode
            if ec is not None and ec < 0:
                sys.stderr.write("SUBPROCESS RETURNED ERROR %d :: %s\n" % (ec, process.name))
                sys.exit(ec * -1)
            process.join()
        print "  CREATING KEY FILES :: PROCESSES JOINED"
    else:
        print "  CREATING KEY FILES :: INTERMEDIATE KEY FILES ALREADY EXISTS. SKIPPING"





    #processes = []
    #print "CREATING SORTED KEY FILES"
    #for chromId in chroms.keys():
    #    fileName     = chroms[chromId]['fileName']
    #    fileNameSort = fileName + ".sort.keys"
    #    chroms[chromId]['fileNameSort'] = fileNameSort
    #
    #    print "  CREATING SORTED KEY :: CHROM %s IN FILE %s TO %s" % (chromId, fileName, fileNameSort)
    #
    #    if os.path.exists(fileName):
    #        if not os.path.exists(fileNameSort) or overwriteKeysSort:
    #            processes.append(Process(target=sortFile, args=[ fileName , fileNameSort ]))
    #    else:
    #        sys.stdout.write("INPUT FILE %s DOES NOT EXISTS" % (fileName))
    #        sys.exit(1)
    #
    #if len(processes) > 0:
    #    print "  CREATING SORTED KEY :: SPAWNING %3d PROCESSES" % (len(processes))
    #    for process in processes:
    #        process.start()
    #    print "  CREATING SORTED KEY :: PROCESSES STARTED"
    #
    #    print "  CREATING SORTED KEY :: JOINING PROCESSES"
    #    for process in processes:
    #        process.join()
    #    print "  CREATING SORTED KEY :: PROCESSES JOINED"
    #else:
    #    print "  CREATING SORTED KEY :: INTERMEDIATE SORTED KEY FILES ALREADY EXISTS. SKIPPING"





    sumSizes         = 0
    batchSize        = 0
    inputSortedFiles = []
    print "CREATING MERGED SORTED FILE"
    for chromId in chroms.keys():
        sortedFile = chroms[chromId]['fileName']
        size       = os.path.getsize(sortedFile)
        if size == 0:
            sys.exit(1)
        sumSizes  += size
        batchSize += 1
        if len(inputSortedFiles) != (batchSize / 512) + 1:
            inputSortedFiles.append([0, "", []])

        inputSortedFiles[batchSize / 512][0] += size
        inputSortedFiles[batchSize / 512][1] += ' "' + sortedFile + '"'
        inputSortedFiles[batchSize / 512][2].append(sortedFile)


    print "  CREATING MERGED SORTED :: TOTAL FILES %5d TOTAL SIZE %15d" % (batchSize, sumSizes)
    outputSortedFile = baseInputTmp + ".keys"

    if not os.path.exists(outputSortedFile) or overwriteSort:
        if len(inputSortedFiles) > 1:
            print "    TOO MANY INPUTS. SPLITING"
            tmpCount = 0
            tmpFiles = []
            for pair in inputSortedFiles:
                size              = pair[0]
                inputSortedFile   = pair[1]
                files             = pair[2]
                outputSortedFiles = outputSortedFile + ".keys%05d.keys" % (tmpCount)
                batchSizes        = 512
                tmpFiles.append(outputSortedFiles)

                if (tmpCount + 1) * 512 > batchSize:
                    batchSizes = batchSize - (tmpCount * 512)

                print "    PAIR %4d / %4d - OUTPUT %s SIZE %12d - BATCH SIZE %5d" % (tmpCount + 1, len(inputSortedFiles), outputSortedFiles, size, batchSizes)

                ret = call("sort --key=1 --numeric-sort --compress-program=pigz --parallel=30 --merge --temporary-directory=%(tmpdir)s --batch-size=%(batchSize)d %(input)s | pv --progress --timer --eta --rate --average-rate --bytes --size %(size)d > \"%(output)s\"" % {'input': inputSortedFile, 'output': outputSortedFiles, 'batchSize': batchSizes, 'size': size, 'tmpdir':tmpDir }, shell=True)
                if ret != 0:
                    sys.stderr.write("ERROR SORTING FILE %s TO %s. RETURN CODE %d. NOT 0\n" % (inputSortedFiles, outputSortedFile, ret))
                    sys.exit(1)
                tmpCount += 1
                if removeTemps:
                    for tmpFile in files:
                        os.remove(tmpFile)

            inputMergedFiles = "\"" + "\" \"".join(tmpFiles) + "\""
            print "    MERGING TEMPORARY FILES TO %s SIZE %13d PIECES %5d FROM %s" % (outputSortedFile, sumSizes, len(tmpFiles), inputMergedFiles)
            ret = call("sort --key=1 --numeric-sort --compress-program=pigz --parallel=30 --merge --temporary-directory=%(tmpdir)s --batch-size=%(batchSize)d %(input)s | pv --progress --timer --eta --rate --average-rate --bytes --size %(size)d > \"%(output)s\"" % {'input': inputMergedFiles, 'output': outputSortedFile, 'batchSize': len(tmpFiles), 'size': sumSizes, 'tmpdir':tmpDir }, shell=True)
            if ret != 0:
                sys.stderr.write("ERROR SORTING FILE %s TO %s. RETURN CODE %d. NOT 0\n" % (inputSortedFiles, outputSortedFile, ret))
                sys.exit(1)

            if removeTemps:
                for tmpFile in tmpFiles:
                    os.remove(tmpFile)
        else:
            for inputSortedFile in inputSortedFiles:
                ret = call("sort --key=1 --numeric-sort --compress-program=pigz --parallel=30 --merge --temporary-directory=%(tmpdir)s --batch-size=%(batchSize)d %(input)s | pv --progress --timer --eta --rate --average-rate --bytes --size %(size)d > \"%(output)s\"" % {'input': inputSortedFile, 'output': outputSortedFile, 'batchSize': batchSize, 'size': sumSizes, 'tmpdir':tmpDir }, shell=True)
                if ret != 0:
                    sys.stderr.write("ERROR SORTING FILE %s TO %s. RETURN CODE %d. NOT 0\n" % (inputSortedFiles, outputSortedFile, ret))
                    sys.exit(1)
    else:
        print "  CREATING MERGED SORTED :: INTERMEDIATE MERGED SORTED FILE ALREADY EXISTS. SKIPPING"

    if os.path.getsize(outputSortedFile) != sumSizes:
        os.remove(outputSortedFile)
        sys.stderr.write("ERROR CREATING %s FROM %s\n" % (outputSortedFile, inputSortedFiles))
        sys.exit(1)




    outputMergedFile = baseInputTmp + ".merged.keys"
    tmpFastaFile     = baseInputTmp + '.kmers.fasta'
    print "CREATING FINAL REDUCED FILE", outputMergedFile,"AND TMP FASTA FILE",tmpFastaFile
    reducedLength    = 0

    if not os.path.exists(outputMergedFile) or not os.path.exists(tmpFastaFile) or overwriteMerged:
        lineOutCount = 0
        lineInCount  = 0
        inputSorted  = open(outputSortedFile, 'r')
        outputMerged = open(outputMergedFile, 'w')
        tmpFasta     = open(tmpFastaFile,     'w')
        lastKey      = None
        lastVal      = None
        timeStart    = time.time()
        lastTime     = timeStart
        inFileSize   = line_len(outputSortedFile)


        for line in inputSorted:
            lineInCount += 1
            line = line.strip()
            if lineInCount % ( inFileSize / 100 ) == 0:
                perc      = (lineInCount * 100.0/inFileSize)
                timeBegin = timer(timeStart)
                timeLast  = timer(lastTime)
                eta       = calcEta(timeStart, lineInCount, inFileSize)

                print "  FINAL REDUCE LINE %12d / %12d %6.2f%% - LAST 1%% DONE IN %s - TOTAL TIME %s - ETA %s" %( lineInCount, inFileSize, perc, timeLast, timeBegin, eta )
                lastTime = time.time()
            #print "LINE '"+line+"'"
            (k,v) = line.split()
            v     = base64.b64decode(v)
            #print "  FOR K",k
            if lastKey is None:
                #print "    LAST K IS NONE"
                lastKey = k
                lastVal = v
            else:
                if lastKey == k:
                    #print "    LAST K IS K. APPENDING"
                    lastVal += v
                else:
                    #print "    LAST K IS NOT K"
                    outputMerged.write("%s\t%s\n" % (lastKey, base64.b64encode(lastVal)))
                    lineOutCount += 1
                    #seq1 = base64.b64decode(seq)
                    #seq    = deconvert(dnav, int(lastKey))
                    seq    = lastKey
                    tmpFasta.write(">%s\n%s\n" % (seq,seq))
                    lastKey       = k
                    lastVal       = v
        #seq    = deconvert(dnav, int(lastKey))
        seq    = lastKey
        tmpFasta.write(">%s\n%s\n" % (seq,seq))
        outputMerged.write("%s\t%s\n" % (lastKey, base64.b64encode(lastVal)))
        inputSorted.close()
        outputMerged.close()
        tmpFasta.close()

        perc      = ( lineInCount * 100.0/inFileSize )
        timeBegin = timer(timeStart)
        timeLast  = timer(lastTime)
        eta       = calcEta(timeStart, lineInCount, inFileSize)
        print "  FINAL REDUCE LINE %12d / %12d %6.2f%% - LAST 1%% DONE IN %s - TOTAL TIME %s - ETA %s" %( lineInCount, inFileSize, perc, timeLast, timeBegin, eta )
        reducedLength = lineOutCount
        print "  CREATING FINAL REDUCED :: LENGTH %d" % (reducedLength)
    else:
        print "  CREATING FINAL REDUCED :: FINAL REDUCED FILE ALREADY EXISTS. SKIPPING"
        reducedLength = line_len(outputMergedFile)
        print "  CREATING FINAL REDUCED :: LENGTH %d" % (reducedLength)





    outputBlastFile = baseInputTmp + ".blast"
    dbName          = os.path.join(tmpDir, 'db')
    print "BLASTING KMERS TO %s" % outputBlastFile

    if not os.path.exists(outputBlastFile) or overwriteBlast:
        if     not os.path.exists(dbName+'.nhd') \
            or not os.path.exists(dbName+'.nhi') \
            or not os.path.exists(dbName+'.nhr') \
            or not os.path.exists(dbName+'.nin') \
            or not os.path.exists(dbName+'.nog') \
            or not os.path.exists(dbName+'.nsd') \
            or not os.path.exists(dbName+'.nsi') \
            or not os.path.exists(dbName+'.nsq'):
            print "  CREATING DB %s" % (dbName)

            ret = call("makeblastdb -dbtype nucl -in \"%(input)s\" -out %(dbname)s -title %(title)s -hash_index" % {'input': inputDb, 'dbname': dbName, 'title':os.path.splitext(baseInputDb)[1] }, shell=True)

            if ret != 0:
                sys.stderr.write("ERROR MAKING BLAST DB OF %s. RETURN CODE %d. NOT 0\n" % (inputDb, ret))
                sys.exit(1)
            print "  DB CREATED"
        else:
            print "  DB ALREADY EXISTS"


        #soft_masking="false",
        print "  RUNNING BLAST"
        print "    SEPARATING FASTA"
        tmpDirPar1    = os.path.join(tmpDir, 'par1')
        if not DoNotRunBlastSeparator:
            splitFasta(tmpFastaFile, tmpDirPar1, blastPartsKb * 1024)




        tmpDirPar2    = os.path.join(tmpDir, 'par2')
        print "    BLASTING"
        if not DoNotRunBlast:
            if os.path.exists(tmpDirPar2):
                for f in os.listdir(tmpDirPar2):
                    os.remove(os.path.join(tmpDirPar2, f))
            else:
                os.makedirs(tmpDirPar2)

            if os.path.exists(outputBlastFile):
                os.remove(outputBlastFile)

            tmpFastaFileSize = os.path.getsize(tmpFastaFile)
            pieces           = tmpFastaFileSize / (70*1024)
            print "  PIECES %d" % (pieces)

            opts = {    'query'  : tmpFastaFile,     \
                        'output' : outputBlastFile,  \
                        'size'   : tmpFastaFileSize, \
                        'tmpdir1': tmpDirPar1,       \
                        'tmpdir2': tmpDirPar2,       \
                        'dbname' : dbName            \
                        }

            tmpDirPar1Len = len(os.listdir(tmpDirPar1))
            #blastn_cline   = NcbiblastnCommandline(query=tmpFastaFile, out=outTmpFile, db="./db", outfmt=6, task='blastn-short', word_size=11, num_threads=40, dust="no", max_target_seqs=100000)
            #-word_size 11 -task blastn-short
            blastn_cline2 = "ls %(tmpdir1)s | parallel --progress --tmpdir %(tmpdir2)s --files -X --max-args 1 -j 30%% \
                            'blastn -task blastn-short -outfmt 6 -dust no -soft_masking false -max_target_seqs 100 -db %(dbname)s -num_threads 2 -query %(tmpdir1)s/{}; rm %(tmpdir1)s/{}'" % opts
            print "  CMD2 %s" % (blastn_cline2)
            ret2 = call(blastn_cline2, shell=True)
            if ret2 != 0:
                sys.stderr.write("ERROR RUNNING BLAST OF %s TO %s. RETURN CODE %d. NOT 0\n" % (tmpFastaFile, outputBlastFile, ret))
                sys.exit(1)

            tmpDirPar2Len = len(os.listdir(tmpDirPar2))
            if tmpDirPar1Len != tmpDirPar2Len:
                sys.stderr.write("ERROR RUNNING BLAST OF %s TO %s. NUMBER OF INPUT FILES %d NOT EQUAL TO NUMBER OF OUTPUT FILES %d\n" % (tmpFastaFile, outputBlastFile, tmpDirPar1Len, tmpDirPar2Len))
                sys.exit(1)



        print "    MERGING BLAST"
        if not DoNotRunBlastMerge:
            mergeBlast(tmpDirPar2, outputBlastFile)

        print "  BLAST RUN"
    else:
        print "  BLASTING FILE ALREADY EXISTS. SKIPPING"






    print "GENERATING UNIQUENESS DB"
    outPickleFile = baseInputTmp + '.pickle'
    sequencesUniquenes = {}
    if not os.path.exists(outPickleFile) or overwritePickle:
        if os.path.exists(outPickleFile):
            os.remove(outPickleFile)

        inFileSize  = line_len(outputBlastFile)
        lineInCount = 0
        timeStart   = time.time()
        lastTime    = timeStart

        blastres    = open(outputBlastFile, 'r')
        for line in blastres:
            lineInCount += 1

            if lineInCount % ( inFileSize / 100 ) == 0:
                perc      = (lineInCount * 100.0/inFileSize)
                timeBegin = timer(timeStart)
                timeLast  = timer(lastTime)
                eta       = calcEta(timeStart, lineInCount, inFileSize)

                print "  UNIQ DB GEN LINE %12d / %12d %6.2f%% - LAST 1%% DONE IN %s - TOTAL TIME %s - ETA %s" %( lineInCount, inFileSize, perc, timeLast, timeBegin, eta )
                lastTime = time.time()

            line = line.strip()
            res  = line.split()
            #print "  LINE '",line,"'"
            #AAAAAAAAAAGTGGCTACAATATTAATCGGGG        SL2.40ch11      100.00  32      0       0       1       32      25370532        25370501        5e-09   60.2

            if len(res) == 12:
                seq  = res[0]
                bits = float(res[11])
                #print "    SEQ %s BITS %.f" % (seq, bits)

                if len(seq) == kmer:
                    conv = convert(seq, dnak, dnav)
                    #print "      CONV",conv

                    if sequencesUniquenes.has_key(conv):
                        #print "  OLD SEQUENCE",seq
                        sequencesUniquenes[conv][0] += bits
                        sequencesUniquenes[conv][1] += 1
                        sequencesUniquenes[conv][2]  = sequencesUniquenes[conv][0] * 1.0 / sequencesUniquenes[conv][1]
                    else:
                        #print "  NEW SEQUENCE",seq
                        sequencesUniquenes[conv]     = [0.0, 0, 0.0]
                        sequencesUniquenes[conv][0]  = bits
                        sequencesUniquenes[conv][1]  = 1
                        sequencesUniquenes[conv][2]  = bits
                else:
                    print "  LINE '"+line+"'. SEQUENCE IS WRONG"
                    sys.stdout.flush()
            else:
                print "  LINE '"+line+"'. LENGTH IS WRONG"
                sys.stdout.flush()
        blastres.close()



        print "  FINISHED READING"
        print "  STANDARDIZING"
        uniMin    = 100000000000000.0
        uniMax    = 0.0
        uniMinU   = 100000000000000.0
        uniMaxU   = 0.0
        posCount  = 0
        posMax    = len(sequencesUniquenes)
        timeStart = time.time()
        lastTime  = timeStart

        for k in sequencesUniquenes.keys():
            posCount += 1

            if posCount % ( posMax / 100 ) == 0:
                perc      = (posCount * 100.0/posMax)
                timeBegin = timer(timeStart)
                timeLast  = timer(lastTime)
                eta       = calcEta(timeStart, posCount, posMax)

                print "  UNIQ DB READ LINE %12d / %12d %6.2f%% - LAST 1%% DONE IN %s - TOTAL TIME %s - ETA %s" %( posCount, posMax, perc, timeLast, timeBegin, eta )
                lastTime = time.time()

            data      = sequencesUniquenes[k]
            dSum      = data[0]
            dCount    = data[1]
            dAvg      = data[2]

            if uniMinU >= dSum: uniMinU = dSum
            if uniMaxU <= dSum: uniMaxU = dSum

            if dCount == 1:
                #print "DSUM %.1f DCOUNT %d DAVG %.1f KMER %d MIN %.1f MAX %.1f" % (dSum, dCount, dAvg, kmer, uniMin, uniMax)
                if uniMin >= dSum: uniMin = dSum
                if uniMax <= dSum: uniMax = dSum
        print "    UNIQUENESS DB: MIN UNI %.1f MAX UNI %.1f MIN %.1f MAX %.1f" % (uniMin, uniMax, uniMinU, uniMaxU)


        uniMinP   = 100000000000000.0
        uniMaxP   = 0.0
        posCount  = 0
        timeStart = time.time()
        lastTime  = timeStart

        for k in sequencesUniquenes.keys():
            posCount += 1

            if posCount % ( posMax / 100 ) == 0:
                perc      = (posCount * 100.0/posMax)
                timeBegin = timer(timeStart)
                timeLast  = timer(lastTime)
                eta       = calcEta(timeStart, posCount, posMax)

                print "  UNIQ DB FIX LINE %12d / %12d %6.2f%% - LAST 1%% DONE IN %s - TOTAL TIME %s - ETA %s" %( posCount, posMax, perc, timeLast, timeBegin, eta )
                lastTime = time.time()

            data   = sequencesUniquenes[k]
            dSum   = data[0] * 1.0 / uniMax * 1.0
            dCount = data[1]
            dAvg   = data[2] * 1.0 / uniMax * 1.0
            sequencesUniquenes[k] = [dSum, dCount, dAvg]

            if uniMinP >= dSum: uniMinP = dSum
            if uniMaxP <= dSum: uniMaxP = dSum
        print "    UNIQUENESS DB: MINP %.1f MAXP %.1f" % (uniMinP, uniMaxP)
        print "  STANDARDAZED"

        print "  DUMPING"
        pickleDump(sequencesUniquenes, outPickleFile)
        print "  DUMPED"
    else:
        print "  DUMP EXISTS. READING"
        sequencesUniquenes = pickleLoad(sequencesUniquenes, outPickleFile)
        print "  DUMP LOADED"

    uniquenessLength = len(sequencesUniquenes)
    if uniquenessLength == 0:
        sys.stderr.write("ERROR CREATING UNIQUENESS DB. LENGTH 0\n")
        sys.exit(1)

    elif uniquenessLength != reducedLength:
        sys.stderr.write("ERROR CREATING UNIQUENESS DB. LENGTH %d DIFFERENT FROM QUERY LENGTH %d\n" % (uniquenessLength, reducedLength))
        sys.exit(1)

    print "  UNIQUENESS DB CREATED. TOTAL: %d" % (uniquenessLength)






    processes  = []
    print "CREATING COVERAGE FILES"
    for seq_record in SeqIO.parse(input, "fasta"):
        id         = seq_record.id
        length     = chroms[id]['length']
        chrNum     = chroms[id]['chrNum']
        fileName   = "%s.%s.cov" % (baseInput, id)
        chroms[id] = { 'length': length, 'covName': fileName }
        seq        = seq_record.seq
        print "  ID" ,id,"LEN",length

        if not os.path.exists(fileName) or overwriteCov:
            if os.path.exists(fileName):
                os.remove(fileName)
            processes.append(Process(target=mapSeq, args=[ dnav, dnak, fileName, sequencesUniquenes, chrNum, length, seq ]))


    if len(processes) > 0:
        print "  CREATING COV FILES :: SPAWNING %3d PROCESSES" % (len(processes))
        for process in processes:
            process.start()
        print "  CREATING COV FILES :: PROCESSES STARTED"

        print "  CREATING COV FILES :: JOINING PROCESSES"
        for process in processes:

            ec = process.exitcode
            if ec is not None and ec < 0:
                sys.stderr.write("SUBPROCESS RETURNED ERROR %d :: %s\n" % (ec, process.name))
                sys.exit(ec * -1)

            process.join()
        print "  CREATING COV FILES :: PROCESSES JOINED"
    else:
        print "  CREATING COV FILES :: COV FILES ALREADY EXISTS. SKIPPING"




    covFiles    = os.listdir(os.curdir)
    covFilesLen = len(covFiles)
    covFileCurr = 1
    print "CREATING IMAGES %s (%d)" % (os.curdir, covFilesLen)

    for fileName in covFiles:
        print "  CHECKING FILE %s %12d / %12d" % (fileName, covFileCurr, covFilesLen)
        covFileCurr += 1

        if fileName[-4:] == ".cov":
            print "    COV"
            outPng  = fileName + ".png"
            outPlot = fileName + ".plot"
            print "    PNG %s" % outPng

            if not os.path.exists(outPng) or overwritePng:
                cmd    = "set terminal png size 1024,768 large font 'Courier New,12'\nset output '%(outpng)s'\nset logscale y\nplot '%(incov)s' using 3  axis x1y1 w impulses" % { 'incov': fileName, 'outpng':outPng }
                with open(outPlot, 'w') as f:
                    f.writelines(cmd)

                cmd    = "gnuplot %s" % (outPlot)
                print "  MAKING IMAGE %s" % (cmd)
                ret    = call(cmd, shell=True)
                if ret != 0:
                    sys.stderr.write("ERROR CREATING PNG %s FROM %s. RESPONSE %s\n" % (outPng, fileName, ret))
                    sys.exit(1)
                else:
                    print "  PNG CREATION FROM %s TO %s SUCCESSFULL" % (fileName, outPng)







    if removeTempsAll:
        #chroms[id]   = { 'length': length, 'fileName': fileName, 'chrNum':chrNum }
        for id in chroms.keys():
            fn = chroms[id]['fileName']
            fp = chroms[id]['covName']+'.plot'
            if os.path.exists(fn): os.remove(fn)
            if os.path.exists(fp): os.remove(fp)

        if os.path.exists(outputSortedFile): os.remove(outputSortedFile)
        if os.path.exists(outputMergedFile): os.remove(outputMergedFile)
        if os.path.exists(tmpFastaFile    ): os.remove(tmpFastaFile    )
        if os.path.exists(outputBlastFile ): os.remove(outputBlastFile )
        if os.path.exists(outPickleFile   ): os.remove(outPickleFile   )

        for ext in ['.nhd', '.nhi', '.nhr', '.nin', '.nog', '.nsd', '.nsi', '.nsq']:
            if os.path.exists(dbName + ext): os.remove(dbName + ext)








        #timeStart   = time.time()
        #lastTime    = timeStart
        #inFileSize  = line_len(outputMergedFile)
        #lineInCount = 0
        #from Bio.Blast.Applications import NcbiblastxCommandline
        #from Bio.Blast import NCBIXML
        #outputBlast = open(outputBlastFile,  'w')
        #outBlastFile = baseInput + '.blast'
        #outBlast     = open(outBlastFile, 'w')
        #if os.path.exists(outTmpFile):
        #    #print "  PARSING BLAST"
        #    tmpxml     = open(outTmpFile, 'r')
        #    totalScore = 0.0
        #    resCount   = 0
        #    for line in tmpxml:
        #        eval, score = line.split()[-2:]
        #        #print "  LINE %s EVAL %s SCORE %s" % (line.strip(), eval, score)
        #        resCount   += 1
        #        totalScore += float(score)
        #    tmpxml.close()
        #    #print "  SCORE",totalScore
        #    #print "  COUNT",resCount
        #    #print "  BLAST PARSED\n"
        #    outBlast.write("%d\t%d;%.f\n" % (k, resCount, totalScore))
        #else:
        #    print "NOT ABLE TO FIND OUTPUT TAB XML FILE %s" % (outTmpFile)
        #    sys.exit(1)
        #    os.remove(tmpFastaFile)
        #    os.remove(outTmpFile)
        #    #sys.exit(1)
        #outBlast.close()



#data = base64.b64decode(encoded)
#data = base64.b64encode(pack('HL?', chrNum, pos, rev))#;, {chrNum, pos, rev, conv}
