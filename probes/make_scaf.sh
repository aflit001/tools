#!/bin/bash
set -o pipefail
set -e

LC_ALL=C

source make_setup.sh

#
# CHECKING EACH SCAFFOLD
#
CHROMNAME=$1
SCAF=$2
DID=0
SCRIPT_NAME=`basename ${BASH_SOURCE}`
DSC=" :: $CHROMNAME :: $SCAF"

SCAFSIZE=`grep $SCAF ${INFOLDER}/${DBFASTAFILE}.idx | perl -ane 'print $F[1]'`
echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      SCAFFOLD SIZE $SCAFSIZE"



OUT="$MAPPINGOUT_CHROM.$SCAF.$MAPEXTENSION"
export MAPPINGOUT_CHROM_SCAF=$OUT
CMD="grep -P -e \"^\\d+\\s+\\d+\\s+$SCAF\" $MAPPINGOUT_CHROM | sort -k 4 -n --compress-program=gzip > $OUT"
ERRMSG="ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD"

echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      GENERATING MAP FOR CHROMOSSOME $CHROMNAME SCAFFOLD $SCAF"

if [[ ! -s "$OUT" ]]; then
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        RUNNING '$CMD'"
    #twit.py starting filtering chromossome $CHROMNAME scaffold $SCAF
    eval time "$CMD" 2>&1 | tee -a err.log
    RES=$?
    wait ${!}
    if [[ $RES -ne "0" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** $ERRMSG :: EXIT WITH ERROR: $RES ***" 
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    elif [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** $ERRMSG :: COULD NOT FIND OUTPUT FILE ***"
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        DID=$((DID+1))
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "          $OUT CREATED"
    fi
else
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        $OUT EXISTS. SKIPPING"
fi




OUT="$MAPN_CHROM.$SCAF.tab"
export MAPN_CHROM_SCAF=$OUT
CMD="grep $SCAF $MAPN_CHROM > $OUT"
ERRMSG="ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD"

echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      GENERATING Ns MAP FOR CHROMOSSOME $CHROMNAME SCAFFOLD $SCAF"

if [[ ! -s "$OUT" ]]; then
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        RUNNING '$CMD'"
    #twit.py starting filtering chromossome $CHROMNAME scaffold $SCAF
    
    eval time "$CMD"  2>&1 | tee -a err.log
    RES=$?
    wait ${!}
    if [[ $RES -ne "0" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** $ERRMSG :: EXIT WITH ERROR: $RES ***"  
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    elif [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** $ERRMSG :: COULD NOT FIND OUTPUT FILE ***"
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        DID=$((DID+1))
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "          $OUT CREATED"
    fi
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        $OUT EXISTS. SKIPPING"
fi





OUT="${MAPN_CHROM_SCAF}.cov"
export MAPN_CHROM_SCAF_COV=$OUT
CMD="cat ${MAPN_CHROM_SCAF} | perl -ane '\$s=\$F[2]; \$e=\$F[3]; print \
	( \$s > \$e ? \"\$e\t\$s\n\" : \"\$s\t\$e\n\")' | ./range ${SCAFSIZE} > $OUT"
ERRMSG="ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD"

echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      GENERATING Ns COVERAGE FOR CHROMOSSOME $CHROMNAME SCAFFOLD $SCAF"

if [[ ! -s "$OUT" ]]; then
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        RUNNING '$CMD'"
    #twit.py starting filtering chromossome $CHROMNAME scaffold $SCAF
    eval time "$CMD"  2>&1 | tee -a err.log
    RES=$?
    wait ${!}
    if [[ $RES -ne "0" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** $ERRMSG :: EXIT WITH ERROR: $RES ***"  
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    elif [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** $ERRMSG :: COULD NOT FIND OUTPUT FILE ***"  
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        DID=$((DID+1))
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "          $OUT CREATED"
    fi
else
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        $OUT EXISTS. SKIPPING"
fi





OUT1="$AGPFOLDER/${CONTIGMAP}.${SCAF}.agp.contig.agp"
OUT2="$AGPFOLDER/${CONTIGMAP}.${SCAF}.agp.gap.agp"
OUT3="$AGPFOLDER/${CONTIGMAP}.${SCAF}.agp.unknown.agp"
OUT4="$AGPFOLDER/${CONTIGMAP}.${SCAF}.agp.other.agp"
export CONTIGMAP_CONTIG=$OUT1
export CONTIGMAP_GAP=$OUT2
export CONTIGMAP_UNKNOWN=$OUT3
export CONTIGMAP_OTHER=$OUT4

CMD="grep \"$SCAF\" ${INFOLDER}/${CONTIGMAP}  | tee           \
    >(perl -ane 'print if  \$F[4] eq \"W\"' > $OUT1 )         \
    >(perl -ane 'print if  \$F[4] eq \"N\"' > $OUT2 )         \
    >(perl -ane 'print if  \$F[4] eq \"U\"' > $OUT3 )         \
    >(perl -ane 'print if (\$F[4] ne \"W\" && \$F[4] ne \"N\" \
    && \$F[4] ne \"U\")' > $OUT4 ) >/dev/null"

OUT=$OUT1
echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      FILTERING CONTIGS FOR $CHROMNAME SCAFFOLD $SCAF"


if [[ ! -s "$OUT1" ]] || [[ ! -f "$OUT2" ]] || [[ ! -f "$OUT3" ]] || [[ ! -f "$OUT4" ]]; then
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        RUNNING '$CMD'"
    #twit.py starting filtering chromossome $CHROMNAME scaffold $SCAF
    
    
    eval time "$CMD"  2>&1 | tee -a err.log
    RES=$?
    wait ${!}
    if [[ $RES -ne "0" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD :: EXIT WITH ERROR: $RES ***" 
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: SIZE $OUT1 *** "`ls -lh "$OUT1"`
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: SIZE $OUT2 *** "`ls -lh "$OUT2"`  
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: SIZE $OUT3 *** "`ls -lh "$OUT3"`   
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: SIZE $OUT4 *** "`ls -lh "$OUT4"`   
        rm ${OUT1} &> /dev/null
		rm ${OUT2} &> /dev/null
		rm ${OUT3} &> /dev/null
		rm ${OUT4} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    elif [[ ! -s "$OUT1" ]] || [[ ! -f "$OUT2" ]] || [[ ! -f "$OUT3" ]] || [[ ! -f "$OUT4" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD :: COULD NOT FIND OUTPUT FILE***" 
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: SIZE $OUT1 *** "`ls -lh "$OUT1"`        
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: SIZE $OUT2 *** "`ls -lh "$OUT2"`        
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: SIZE $OUT3 *** "`ls -lh "$OUT3"`        
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: SIZE $OUT4 *** "`ls -lh "$OUT4"`        
        rm ${OUT1} &> /dev/null
		rm ${OUT2} &> /dev/null
		rm ${OUT3} &> /dev/null
		rm ${OUT4} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        DID=$((DID+1))
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "          $OUT1 , $OUT2 , $OUT3 , $OUT4 CREATED"
    fi
else
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        $OUT EXISTS. SKIPPING"
fi




OUT="${CONTIGMAP_CONTIG}.cov"
export CONTIGMAP_CONTIG_COV=$OUT
CMD="cat ${CONTIGMAP_CONTIG} | perl -ane '\$s=\$F[1]; \$e=\$F[2]; print \
	( \$s > \$e ? \"\$e\t\$s\n\" : \"\$s\t\$e\n\")' \
    | ./range  ${SCAFSIZE} > $OUT"

ERRMSG="ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD"

echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      CONVERTING CONTIGS FROM AGP (${CONTIGMAP_CONTIG}) TO COV \
($OUT) FROM $CHROMNAME SCAFFOLD $SCAF"

if [[ ! -s "$OUT" ]]; then
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      RUNNING '$CMD'"
    #twit.py starting filtering chromossome $CHROMNAME scaffold $SCAF
    eval time "$CMD"  2>&1 | tee -a err.log
    RES=$?
    wait ${!}
    if [[ $RES -ne "0" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** $ERRMSG :: EXIT WITH ERROR: $RES ***"  
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    elif [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** $ERRMSG :: COULD NOT FIND OUTPUT FILE ***"  
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        DID=$((DID+1))
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "          $OUT CREATED"
    fi
else
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        $OUT EXISTS. SKIPPING"
fi




OUT="${CONTIGMAP_GAP}.cov"
export CONTIGMAP_GAP_COV=$OUT
CMD="cat ${CONTIGMAP_GAP} | perl -ane '\$s=\$F[1]; \$e=\$F[2]; print \
	( \$s > \$e ? \"\$e\t\$s\n\" : \"\$s\t\$e\n\")' \
    | ./range  ${SCAFSIZE} g > $OUT"
ERRMSG="ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD"

echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      CONVERTING GAPS FROM AGP (${CONTIGMAP_GAP}) TO COV \
($OUT) FROM $CHROMNAME SCAFFOLD $SCAF"

if [[ ! -s "$OUT" ]]; then
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        RUNNING '$CMD'"
    #twit.py starting filtering chromossome $CHROMNAME scaffold $SCAF
    eval time "$CMD"  2>&1 | tee -a err.log
    RES=$?
    wait ${!}
    if [[ $RES -ne "0" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** $ERRMSG :: EXIT WITH ERROR: $RES ***"  
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    elif [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** $ERRMSG :: COULD NOT FIND OUTPUT FILE ***"  
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        DID=$((DID+1))
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "          $OUT CREATED"
    fi
else
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        $OUT EXISTS. SKIPPING"
fi




OUT="${CONTIGMAP_UNKNOWN}.cov"
export CONTIGMAP_UNKNOWN_COV=$OUT
CMD="cat ${CONTIGMAP_UNKNOWN} | perl -ane '\$s=\$F[1]; \$e=\$F[2]; print \
	( \$s > \$e ? \"\$e\t\$s\n\" : \"\$s\t\$e\n\")' \
    | ./range  ${SCAFSIZE} > $OUT"
ERRMSG="ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD"

echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      CONVERTING UNKNOWN GAPS FROM AGP (${CONTIGMAP_UNKNOWN}) TO COV \
($OUT) FROM $CHROMNAME SCAFFOLD $SCAF"

if [[ ! -s "$OUT" ]]; then
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        RUNNING '$CMD'"
    #twit.py starting filtering chromossome $CHROMNAME scaffold $SCAF
    eval time "$CMD"  2>&1 | tee -a err.log
    RES=$?
    wait ${!}
    if [[ $RES -ne "0" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** $ERRMSG :: EXIT WITH ERROR: $RES ***"  
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    elif [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** $ERRMSG :: COULD NOT FIND OUTPUT FILE ***"  
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        DID=$((DID+1))
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "          $OUT CREATED"
    fi
else
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        $OUT EXISTS. SKIPPING"
fi




OUT="${CONTIGMAP_OTHER}.cov"
export CONTIGMAP_OTHER_COV=$OUT
CMD="cat ${CONTIGMAP_OTHER} | perl -ane '\$s=\$F[1]; \$e=\$F[2]; print \
	( \$s > \$e ? \"\$e\t\$s\n\" : \"\$s\t\$e\n\")' \
    | ./range  ${SCAFSIZE} > $OUT"
ERRMSG="ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD"

echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      CONVERTING OTHER GAPS FROM AGP (${CONTIGMAP_OTHER}) TO COV \
($OUT) FROM $CHROMNAME SCAFFOLD $SCAF"

if [[ ! -s "$OUT" ]]; then
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        RUNNING '$CMD'"
    #twit.py starting filtering chromossome $CHROMNAME scaffold $SCAF
    eval time "$CMD"  2>&1 | tee -a err.log
    RES=$?
    wait ${!}
    if [[ $RES -ne "0" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** $ERRMSG :: EXIT WITH ERROR: $RES ***" 
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    elif [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** $ERRMSG :: COULD NOT FIND OUTPUT FILE ***"  
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        DID=$((DID+1))
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "          $OUT CREATED"
    fi
else
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        $OUT EXISTS. SKIPPING"
fi








OUT="$MAPPINGOUT_CHROM_SCAF.cov"
OUT2="$MAPPINGOUT_CHROM_SCAF.pos"
export MAPPINGOUT_CHROM_SCAF_COV=$OUT
CMD="cat $MAPPINGOUT_CHROM_SCAF                                             \
    | perl -nae 'chomp; \$size = '$JELLYMERLEN';                            \
    if ( \$F[5] =~ /(\d+)M/ ) { \$size = \$1 };                             \
    my \$mm = 1; if (\$F[1] & 16) { \$mm = -1; } else { \$mm = 1; }         \
    \$begin = \$F[3]; \$end = (\$F[3] + \$size - 1);                        \
    if ( \$begin > \$end ) { \$t = \$end; \$end = \$begin, \$begin = \$t }; \
    print \"\$begin\t\$end\t\$F[2]\t\$F[1]\t\$size\t\",(\$mm == 1 ? 'fwd' : 'rev'),\"\n\" \
    if (substr(\$F[0],0,1) ne \"\@\")'                                      \
    | tee $OUT2 | ./range  ${SCAFSIZE} > $OUT"

#CMD="cat $MAPPINGOUT_CHROM_SCAF                                             \
#    | perl -nae 'chomp; \$size = '$JELLYMERLEN';                            \
#    if ( \$F[5] =~ /(\d+)M/ ) { \$size = \$1 };                             \
#    my \$mm = 1; if (\$F[1] & 16) { \$mm = -1; } else { \$mm = 1; }         \
#    \$begin = \$F[3]; \$end = (\$F[3] + ( \$size * \$mm ) - 1);             \
#    if ( \$begin > \$end ) { \$t = \$end; \$end = \$begin, \$begin = \$t }; \
#    print \"\$begin\t\$end\t\$F[2]\t\$F[1]\t\$size\t\",(\$mm == 1 ? 'fwd' : 'rev'),\"\n\" \
#    if (substr(\$F[0],0,1) ne \"\@\")'                                      \
#    | tee $OUT2 | ./range  ${SCAFSIZE} > $OUT"

ERRMSG="ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD"

echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      GENERATING RANGES FOR CHROMOSSOME $CHROMNAME SCAFFOLD $SCAF"

if [[ ! -s "$OUT" ]]; then
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        RUNNING '$CMD'"
    eval time "$CMD"  2>&1 | tee -a err.log
    RES=$?
    wait ${!}
    if [[ $RES -ne "0" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** $ERRMSG :: EXIT WITH ERROR: $RES ***"  
        rm ${OUT2} &> /dev/null
        rm ${OUT}  &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    elif [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** $ERRMSG :: COULD NOT FIND OUTPUT FILE ***"
        rm ${OUT2} &> /dev/null
        rm ${OUT}  &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        DID=$((DID+1))
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "          $OUT CREATED"
    fi
else
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        $OUT EXISTS. SKIPPING"
fi

###########################################
#################
#################
###########################################

OUT="$MAPPINGOUT_CHROM_SCAF_COV.prop.cov"
export MAPPINGOUT_CHROM_SCAF_COV_PROP=$OUT
CMD="grep -v '#' $MAPPINGOUT_CHROM_SCAF_COV | \
    perl -nae 'chomp; \$size = '$JELLYMERLEN'; \
     \$pos = \$F[0]; \$prop = \$F[1] / \$size;     \
     print \"\$pos\t\$prop\n\"' | ./add_smooth_colums.pl > $OUT"
ERRMSG="ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD"

echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      GENERATING RANGES PROPORTIONAL FOR CHROMOSSOME $CHROMNAME SCAFFOLD $SCAF"

if [[ ! -s "$OUT" ]]; then
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        RUNNING '$CMD'"
    eval time "$CMD"  2>&1 | tee -a err.log
    RES=$?
    wait ${!}
    if [[ $RES -ne "0" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** $ERRMSG :: EXIT WITH ERROR: $RES ***"  
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    elif [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** $ERRMSG :: COULD NOT FIND OUTPUT FILE ***"  
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        DID=$((DID+1))
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "          $OUT CREATED"
    fi
else
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        $OUT EXISTS. SKIPPING"
fi





MAPPINGCOV=mapping/out/S_lycopersicum_chromosomes.fa_S_lycopersicum_chromosomes.pos.$CHROMNAME.pos.cov
#mapping/out/S_lycopersicum_chromosomes.fa_S_lycopersicum_chromosomes.sam.pos.SL2.40ch00.pos.cov

if [[ ! -f "$MAPPINGCOV" ]]; then
    echo "MAPPING TO CHROMOSOME $CHROMNAME ($MAPPINGCOV) DOES NOT EXISTS"
    exit 1
fi
OUT="$MAPPINGCOV.$SCAF.cov"
export MAPPINGCOV_SCAF=$OUT
ERRMSG="ERROR CREATING $OUT :: CMD "
echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "GENOMIC COVERAGE"

if [[ ! -f "$OUT" ]]; then
    #SL2.40ch02      27495639        49918294        13      W       SL2.40sc03665   1       22422656
    INAGP="data/S_lycopersicum_chromosomes_from_scaffolds.2.40.agp"
    MIN=`cat $INAGP | grep $SCAF | perl -nae 'if (\$F[1] >= \$F[2]) { print "\$F[2]" } else { print "\$F[1]" }'`
    MAX=`cat $INAGP | grep $SCAF | perl -nae 'if (\$F[1] >= \$F[2]) { print "\$F[1]" } else { print "\$F[2]" }'`

    if [[ -z $MIN ]]; then
        echo "COULD NOT DETERMINE MINIMUM GENOMIC POSITION $MIN FOR SCAFFOLD $SCAF ON $INAGP";
        exit 1
    fi
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        GENOMIC MIN $MIN"



    if [[ -z $MAX ]]; then
        echo "COULD NOT DETERMINE MAXIMUM GENOMIC POSITION $MAX FOR SCAFFOLD $SCAF ON $INAGP";
        exit 1
    fi
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        GENOMIC MAX $MAX"



    CMD="./rangeCut.pl --input $MAPPINGCOV --cut --start $MIN --end $MAX -r > $MAPPINGCOV.$SCAF.cov"
    ERRMSG="$ERRMSG$CMD"

    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        RUNNING '$CMD'"
    eval time "$CMD"  2>&1 | tee -a err.log
    RES=$?
    wait ${!}
    if [[ $RES -ne "0" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** $ERRMSG :: EXIT WITH ERROR: $RES ***"
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    elif [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** $ERRMSG :: COULD NOT FIND OUTPUT FILE ***"
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        DID=$((DID+1))
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "          $OUT CREATED"
    fi


else
    echo "SCAFFOLD GENOMIC MAP COVERAGE ALREADY EXISTS"
fi












OUT="$MAPPINGOUT_CHROM_SCAF_COV_PROP.png"
CMD="./mkplot.pl                              \
    --input=${MAPPINGOUT_CHROM_SCAF_COV_PROP} \
    --contig=$CONTIGMAP_CONTIG_COV  --cname=CONTIG\
    --contig=$CONTIGMAP_GAP_COV     --cname=GAP\
    --contig=$CONTIGMAP_UNKNOWN_COV --cname=UNK\
    --contig=$CONTIGMAP_OTHER_COV   --cname=OTHER\
    --contig=$MAPN_CHROM_SCAF_COV   --cname=Ns\
    --range=$MAPPINGCOV_SCAF        --rname=COVERAGE"
ERRMSG="ERROR CREATING $OUT :: CHR $CHROMNAME :: SCA $SCAF :: $CMD"

echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "      GENERATING GRAPHICS PROPORTIONAL FOR CHROMOSSOME $CHROMNAME SCAFFOLD $SCAF"

if [[ ! -s "$OUT" ]]; then
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        RUNNING '$CMD'"
    eval time "$CMD"  2>&1 | tee -a err.log
    RES=$?
    wait ${!}
    if [[ $RES -ne "0" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** $ERRMSG :: EXIT WITH ERROR: $RES ***"  
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    elif [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** $ERRMSG :: COULD NOT FIND OUTPUT FILE ***"  
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        DID=$((DID+1))
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "          $OUT CREATED"
    fi
else
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "        $OUT EXISTS. SKIPPING"
fi




if [[ "$DID" -gt 0 ]]; then
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " finished filtering chromossome $CHROMNAME scaffold $SCAF"
    #twit.py finished filtering chromossome $CHROMNAME scaffold $SCAF
fi


