#!/usr/bin/perl -w
use strict;
use Getopt::Long;



my $c = 0;
my $cut;
my $merge;
my $refactor;
my $inFile;
my $start;
my $end;
my @mergeF   = ();
GetOptions( 
                'cut'      => \$cut,
                'merge'    => \$merge,
                'refactor' => \$refactor,

                'input=s'  => \$inFile,

                'start=i'  => \$start,
                'end=i'    => \$end,

                'file=s'   => \@mergeF,

                'help!'    => \&usage
                ) or usage();

if ( ! defined $cut && ! defined $merge && ! defined $refactor )
{
    print "NO COMMAND ISSUED\n\n";
    usage();
}
die "INPUT FILE $inFile DOES NOT EXISTS"          if ( defined $inFile   && ! -f $inFile );
die "INPUT FILE $inFile IS EMPTY"                 if ( defined $inFile   && ! -s $inFile ); # ;
die "CUT AND MERGE ARE NOT COMPATIBLE (YET)"      if ( defined $cut      &&   defined $merge );
die "REFACTOR AND MERGE ARE NOT COMPATIBLE (YET)" if ( defined $refactor &&   defined $merge );
die "CUT NEEDS A STARTING POINT"                  if ( defined $cut      && ! defined $start );
die "MERGE NEEDS TWO FILES AT LEAST"              if ( defined $merge    && @mergeF < 2 );
die "MERGE TAKES --file NOT --inFile"             if ( defined $merge    && defined $inFile );

print STDERR "CUT        : ", ( defined $cut      ? $cut      : ""), "\n";
print STDERR "MERGE      : ", ( defined $merge    ? $merge    : ""), "\n";
print STDERR "REFACTOR   : ", ( defined $refactor ? $refactor : ""), "\n";
print STDERR "INFILE     : ", ( defined $inFile   ? $inFile   : ""), "\n";
print STDERR "START      : ", ( defined $start    ? $start    : ""), "\n";
print STDERR "END        : ", ( defined $end      ? $end      : ""), "\n";
print STDERR "MERGE FILES: ", ( scalar  @mergeF   ? join(", ", @mergeF) : ""), "\n";






if ( $cut || $refactor )
{
    if ( $cut )
    {
        die "START $start NOT A NUMBER"   if $start !~ /^\d+$/;
        die "END NOT A NUMBER"            if defined $end && $end   !~ /^\d+$/;
        die "START $start < 0"            if $start < 0;
        die "END $end < 0"                if defined $end && $end   < 0;
        die "END ($end) < START ($start)" if defined $end && $end   < $start;
    }

    if ( defined $inFile )
    {
        open F, "<$inFile" or die "$!";
        while (<F>)
        {
            last if ( checkLine($_) );
        }
        close F;
    } else {
        while (<STDIN>)
        {
            last if ( checkLine($_) );
        }
    }

    die "NO POSITION FOUND"       if $c == 0;
    die "NOT ALL POSITIONS FOUND" if $c != ($end - $start + 1);
}
elsif ( $merge )
{
    my @arr;
    die "MERGE DOES NOT ACCEPTS START (YET)" if defined $start;
    die "MERGE DOES NOT ACCEPTS END (YET)"   if defined $end;
    foreach my $file ( @mergeF )
    {
        die "INPUT FILE $file DOES NOT EXISTS"    if ( ! -f $file );
        die "INPUT FILE $file IS EMPTY"           if ( ! -s $file ); # ;

        print STDERR "OPENING $file\n";
        open (my $f, "<$file") or die "$!";
        my $pos = @arr;
        $arr[$pos][0] = $file;
        $arr[$pos][1] = $f;
        $arr[$pos][2] = 0;
    }

    
    my $totalCols = 0;
    my $ln        = -1;
    while (1)
    {
        my $defineds  = 0;
        my @lines;

        ++$c;

        for my $file ( @arr )
        {
            my $fh      = $file->[1];
            my $numCols = $file->[2];
            my $line = <$fh>;

            if ( defined $line )
            {
                chomp($line);
                if ( $numCols == 0 )
                {
                    my @cols    = split(/\s+/, $line);
                    $totalCols += @cols - 1;
                    $file->[2]  = @cols - 1;
                    $ln         = $cols[0] if ( $ln == -1 );
                }
                $line = substr($line, index($line, "\t") + 1);
                ++$defineds;
            } else {
                $line = "\t0"x($file->[2]);
            }
            push(@lines, $line);
        }

        last if ( ! $defineds );
        #last if ( $c == 20 );
        print $ln++, "\t", join("\t", @lines), "\n";
    }

    for my $file ( @arr )
    {
        my $fn = $file->[0];
        my $fh = $file->[1];
        print STDERR "CLOSING $fn\n";
        close $fh or die "$!";
    }

    print "NOT INPLEMENTED YET\n";
}



sub checkLine
{
    my $line = shift;
    if ( $line =~ /^(\d+)/ )
    {
        my $pos = $1;

        if (( ! $cut ) || (( $pos >= $start ) && (( ! defined $end ) || ( $pos <= $end ))))
        {
            if ( $refactor )
            {
                if ( $line =~ /^$pos(\s+.+)/ )
                {
                    $line = $c . $1 . "\n";
                    ++$c;
                    print $line;
                } else {
                    die "oops";
                }
            } else {
                print $line;
            }
        }

        return 1 if ( defined $end && $pos > $end );
        return 0;
    } else {
        print;
        return 0;
    }
}


sub usage
{
    print STDERR <<EOF
    usage: $0 --input file.cov --CMD [--OPTIONS]
                --cut          |-c  : cut a coverage file
                    --input*   |-i  : input coverage file. if no input is given,
                                      STDIN will be used
                   --start     |-s  : start point
                   --end*      |-e  : end point (or nothing to rest)
                   --refactor* |-r  : instead of just printing, fix the first column
                --refactor     |-r  : reprint a coverage file fixing the first column
                    --input*   |-i  : input coverage file. if no input is given,
                                      STDIN will be used
                --merge        |-m  : merge several coverage files in the order given
                    --file     |-f  : two or more files to be merged
                   
                --help         |-h  : this (help) message

EOF
;
    exit 0;
}
