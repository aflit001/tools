#!/bin/bash
set -o pipefail
set -e

LC_ALL=C

source make_setup.sh
SCRIPT_NAME=`basename ${BASH_SOURCE}`
DSC=" :: MAKE Ns"

######################################################
## MAPPING Ns
######################################################
OUT="${NFOLDER}/${DBFASTAFILE}_NONE.tab"
export MAPN=$OUT
CMD="./mapN.pl ${INFOLDER}/${DBFASTAFILE} ${NFOLDER}"

echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " RUNNING Ns MAP"

if [[ ! -s "$OUT" ]]; then
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  RUNNING '$CMD'"
    #twit.py finished bowtie-build \| starting bowtie $OUT
    eval time "$CMD"  2>&1 | tee -a err.log
    wait ${!}
    if [[ ! -s "$OUT" ]]; then
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" " *** ERROR CREATING $OUT :: CMD $CMD ***" 
        rm ${OUT} &> /dev/null
        exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
    else
        echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "    $OUT CREATED"
    fi
    #twit.py finished bowtie \| starting filtering
else
    echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  $OUT EXISTS. SKIPPING"
fi
