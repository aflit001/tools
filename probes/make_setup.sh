#!/bin/bash
set -o pipefail
set -e

LC_ALL=C

export USEBWA=1
export USEBOWTIE=0

if [[ $USEBWA -eq "1" ]]; then
	export USEBOWTIE=0
fi
if [[ $USEBOWTIE -eq "1" ]]; then
	export USEBWA=0
fi

export TMPFOLDER=tmp
export INFOLDER=data
export AGPFOLDER=$TMPFOLDER/agp
export KMERFOLDER=$TMPFOLDER/kmer
export EBWTFOLDER=$TMPFOLDER/ebwt
export BWAFOLDER=$TMPFOLDER/bwa
export TABFOLDER=$TMPFOLDER/tab
export OUTFOLDER=$TMPFOLDER/run
export RAWINFOLDER=$TMPFOLDER/raw
export RAWOUTFOLDER=$TMPFOLDER/raw
export NFOLDER=$TMPFOLDER/Ns
export EXTRAGFF=$TMPFOLDER/extraGff


export EXTRAGFF_TO_CONVERT_FILE=(ITAG2.3_assembly.gff3     ITAG2.3_gene_models.gff3 ITAG2.3_genomic_reagents.gff3 ITAG2.3_repeats_aggressive.gff3 ITAG2.3_repeats.gff3)
export EXTRAGFF_TO_CONVERT_NAME=(assembly                  "gene model"             "reagents"                    "repeats agg"                   "repeats")
export EXTRAGFF_TO_CONVERT_GREP=(''                        "CDS"                    "BAC_clone"                   ""                              "")
export EXTRAGFF_TO_CONVERT_GREN=('supercontig|ultracontig' ""                       ""                            ""                              "")

export EXTRAGFF_CONVERTER=SL2.40ch_from_sc.agp
export EXTRAGFF_CONVERTER_GREP=''
export EXTRAGFF_CONVERTER_GREN='\S+\s+\S+\s+\S+\s+\S+\s+U'

export EXTRAAGP_TO_CONVERT_FILE=$()
export EXTRAAGP_TO_CONVERT_NAME=$()
export EXTRAGFF_FILE=$()
export EXTRAGFF_NAME=$()
export EXTRAAGP_FILE=$()
export EXTRAAGP_NAME=$()





export INFASTA=S_lycopersicum_chromosomes.fa
export DBFASTAFILE=S_lycopersicum_scaffolds.fa
export DBNAME=S_lycopersicum_scaffolds
export MAPAGP=S_lycopersicum_chromosomes_from_scaffolds.2.40.agp
export CONTIGMAP=S_lycopersicum_scaffolds_from_contigs.2.40.agp

#INFASTA=S_lycopersicum_chromosomes.fa.small.fa
export CORES=80
export DESIREDCHROMOSSOME=(02 12)
export SEQEPITOPE=SL2.40
export COL1EPITOPE=ch
export COL2EPITOPE=sc




source make_setup_jelly.sh
#export KMERFOLDER=$KMERFOLDER\_$JELLYMERLEN


if [[ $USEBOWTIE -eq "1" ]]; then
    source make_setup_bowtie.sh
    
    #SETUP1 - uniq jelly, match w/ error bowtie
    #run_JKL_21_JU_1_BSL_15_BMIS_3_BSBT_1
    #export BOWTIESUPRESSBIGGERTHAN=1
    #export BOWTIESEEDLEN=15
    #export BOWTIEMISMATCHES=3
    
    #SETUP2
    #run_JKL_21_JU_1_BSL_15_BMIS_0_BSBT_1
    #export BOWTIESUPRESSBIGGERTHAN=1
    #export BOWTIESEEDLEN=15
    #export BOWTIEMISMATCHES=0
    
    #SETUP3
    #run_JKL_21_JU_1_BSL_15_BMIS_2_BSBT_1
    #export BOWTIESUPRESSBIGGERTHAN=1
    #export BOWTIESEEDLEN=15
    #export BOWTIEMISMATCHES=2
    
    #SETUP4
    #run_JKL_21_JU_1_BSL_15_BMIS_1_BSBT_1
    #export BOWTIESUPRESSBIGGERTHAN=1
    #export BOWTIESEEDLEN=15
    #export BOWTIEMISMATCHES=1
    
    #SETUP5
    #run_JKL_15_JU_1_BSL_7_BMIS_1_BSBT_1
    #export BOWTIESUPRESSBIGGERTHAN=1
    #export BOWTIESEEDLEN=7
    #export BOWTIEMISMATCHES=1
    #export JELLYMERLEN=15
    
    #SETUP6
    #run_JKL_15_JU_1_BSL_7_BMIS_1_BSBT_1
    #export BOWTIESUPRESSBIGGERTHAN=1
    #export BOWTIESEEDLEN=7
    #export BOWTIEMISMATCHES=0
    #export JELLYMERLEN=15
    
    #run_bowtie_JKL_21_JU_1_BSL_7_BMIS_0_BSBT_1
    #SETUP7
    #run_JKL_15_JU_1_BSL_7_BMIS_1_BSBT_1
    export JELLYHASHSIZE=10000000000
    export BOWTIESUPRESSBIGGERTHAN=1
    export BOWTIESEEDLEN=7
    export BOWTIEMISMATCHES=0
    #export JELLYMERLEN=25

	export OUTFOLDER=$OUTFOLDER\_bowtie_JKL_$JELLYMERLEN\_JU_$JELLYUPPERCOUNT\_BSL_$BOWTIESEEDLEN\_BMIS_$BOWTIEMISMATCHES\_BSBT_$BOWTIESUPRESSBIGGERTHAN
	export RAWOUTFOLDER=$RAWOUTFOLDER\_bowtie_JKL_$JELLYMERLEN\_JU_$JELLYUPPERCOUNT\_BSL_$BOWTIESEEDLEN\_BMIS_$BOWTIEMISMATCHES\_BSBT_$BOWTIESUPRESSBIGGERTHAN
	export MAPEXTENSION=map
    export MAPEXTENSION=sam
	export DBNAME=$DBNAME
    
    if [[ ! -d "$EBWTFOLDER" ]]; then
        mkdir $EBWTFOLDER &>/dev/null
    fi
fi

if [[ $USEBWA -eq "1" ]]; then
    source make_setup_bwa.sh
    
    #export JELLYMERLEN=25
    
	export OUTFOLDER=$OUTFOLDER\_BWA_JKL_$JELLYMERLEN\_JU_$JELLYUPPERCOUNT
	export RAWOUTFOLDER=$RAWOUTFOLDER\_BWA_JKL_$JELLYMERLEN\_JU_$JELLYUPPERCOUNT
	export MAPEXTENSION=sam
	export DBNAME=$DBNAME
    
    if [[ ! -d "$BWAFOLDER" ]]; then
        mkdir $BWAFOLDER
    fi
fi


export KMERFOLDER=$KMERFOLDER\_$JELLYMERLEN
export PROBEFOLDER=$OUTFOLDER\_PROBES

if [[ ! -d "$INFOLDER" ]]; then
	mkdir $INFOLDER
fi

if [[ ! -d "$KMERFOLDER" ]]; then
	mkdir $KMERFOLDER
fi

if [[ ! -d "$AGPFOLDER" ]]; then
	mkdir $AGPFOLDER
fi

if [[ ! -d "$TABFOLDER" ]]; then
	mkdir $TABFOLDER
fi

if [[ ! -d "$OUTFOLDER" ]]; then
	mkdir $OUTFOLDER
fi

if [[ ! -d "$RAWOUTFOLDER" ]]; then
	mkdir $RAWOUTFOLDER
fi

if [[ ! -d "$NFOLDER" ]]; then
	mkdir $NFOLDER
fi

if [[ ! -d "$EXTRAGFF" ]]; then
	mkdir $EXTRAGFF
fi



function echoMsg () {
    SEC=$1
    NAM=$2
    PIP=$3
    LNO=$4
    DSC=$5
    shift; shift; shift; shift; shift
    MSG=$@
    printf "[%-20b :: pid %05d :: ln %04d (%06ds)%b] %s\n" "$NAM" "$PIP" "$LNO" "$SEC" "$DSC" "$MSG" | tee -a err.log
    #echo "[$SEC :: $NAM :: $LNO$DSC] $MSG" | tee -a err.log
}


function exitAll () {
    SEC=$1
    NAM=$2
    PIP=$3
    LNO=$4
    DSC=$5

    echoMsg "$SEC" "$NAM" "$PIP" "$LNO" "$DSC" "*** EXITALL CALLED ***"
    pkill -TERM -P $$
    #pkill -TERM -P $PPID
    exit 1
}


function runCmd () {
    OUT=$1
    CMD=$2
    ERRMSG=$3
    
    if [[ ! -s "$OUT" ]]; then
        echo "        RUNNING '$CMD'"
        eval time "$CMD"  2>&1 | tee -a err.log
        RES=$?
        wait ${!}
        if [[ $RES -ne "0" ]]; then
            echo "$ERRMSG :: EXIT WITH ERROR: $RES"  | tee -a err.log
            rm ${OUT} &> /dev/null
            echo ""
            exit 1
        elif [[ ! -s "$OUT" ]]; then
            echo "$ERRMSG :: COULD NOT FIND OUTPUT FILE"  | tee -a err.log
            rm ${OUT} &> /dev/null
            echo ""
            exit 1
        else
            DID=$((DID+1))
            echo "          $OUT CREATED"
        fi
    else
        echo "        $OUT EXISTS. SKIPPING"
    fi
}
