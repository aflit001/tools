#!/bin/bash
INHISTOFILE=$1
TITLE=$1
TITLE=`basename $TITLE`
TITLE=${TITLE%.histo}
TITLE=${TITLE%.fastq}
TITLE=${TITLE%_sequence}
TITLE=${TITLE//_/ }
TMPHISTO=in.histo

LINES=50
SKIPFIRST=0

if [ -n "$MKPLOTLINES" ]; then
	#echo "B LINES = $MKPLOTLINES ($LINES)"
	LINES=$MKPLOTLINES
	#echo "A LINES = $MKPLOTLINES ($LINES)"
fi

echo "LINES = $MKPLOTLINES ($LINES)"


XLABEL="KMER FREQUENCY FOR EACH NUMBER OF COPIES"
YLABEL="VOLUME OF COPIES (NUMBER x APPEARENCES)"
Y2LABEL="% OF TOTAL VOLUME"

OUTHISTOFILE=$1.png
HEAD=$LINES
TAIL=$(($LINES - $SKIPFIRST))
echo "IN HISTO FILE $INHISTOFILE"

#cat $INHISTOFILE      | head -n $HEAD | tail -n $TAIL > histo.tmp
#cat $INHISTOFILE      | head -n $HEAD | tail -n $TAIL | gawk '{print $1"\t"$1*$2}' > histo.tmp
#SIZE=`wc -l histo.tmp                                 | gawk '{print $1}'`
#SIZE=`wc -l histo.tmp                                 | gawk '{print $1}'`
#MAX=`cat histo.tmp                                    | gawk '{print $1*$2}' | sort -n | tail -1`
#MAX=`cat histo.tmp                                    | gawk '{print $2}' | sort -n | tail -1`

MINY=3
SIZE=`wc -l $INHISTOFILE                               | gawk '{print $1}'`
MAX=`cat $INHISTOFILE                                  | gawk '{print $1*$2}'                                | sort -n | tail -1`
SUM=`cat $INHISTOFILE                                  | gawk '{if ($1 > '$MINY') { x=x+($1*$2); print x}n}' | sort -n | tail -1`

MAX2=`echo "$MAX * 1.2" | bc -l | xargs printf "%1.0f"`

FIRST=$(($SKIPFIRST))
LAST=$LINES
LINELEN=`echo "$MAX2 / 10" | bc -l | xargs printf "%1.0f"`

Q=80
IQ=$((100-$Q))
#Q=$((100-$Q))   #100-70=30
#Q=$(($Q/100/2)) #30/100=0.3/2=0.15
#Q=$((1/$Q))     #1/0.15=0.667
SUM1=`echo "$SUM / (1/(((100-$Q)/100)/2))" | bc -l | xargs printf "%1.0f"` # 80 % of the volume
SUM2=`echo "$SUM / 2"                      | bc -l | xargs printf "%1.0f"` # 50 % of the volume
SUM3=`echo "$SUM - $SUM1"                  | bc -l | xargs printf "%1.0f"` # 20 % of the volume
echo "SUM $SUM SUM1 $SUM1 SUM2 $SUM2 SUM3 $SUM3"

V50=`cat $INHISTOFILE | \
        gawk '\
        BEGIN { s='$SUM'; miny='$MINY'};\
        { \
                v=($1*$2);\
		if ( $1 > miny )\
		{\
	                y=x+v; \
        	        if (( x<='$SUM1' ) && (y>='$SUM1')) { q1=$1; v1=x;    p1=x/s};\
	                if (( x<='$SUM2' ) && (y>='$SUM2')) { q2=$1; v2=y-v1; p2=y/s};\
        	        if (( x<='$SUM3' ) && (y>='$SUM3')) { q3=$1; v3=y-v1; p3=y/s};\
                	if (( x<='$SUM3' ) && (y>='$SUM1')) { auc=auc+v; };\
			if (( q1>0 ) && ( q2>0 ) && ( q3>0 ) && ( $1<=q3 ))\
			{\
				print "VOL:  "auc"\nVOL%: "int(((auc/s)+.005)*100)"\nQ'$IQ':   "q1"\nQ50:   "q2"\nQ'$Q':   "q3"";\
			};\
			x=y;\
		}\
	}'`
#print "SUM="'$SUM'" | SUM1="'$SUM1'" | SUM2="'$SUM2'" | SUM3="'$SUM3'""; \
#                printf "KMER="$1" | COUNT="$2" | X="x" | Y="y" ("y/'$SUM'") | auc="auc" " \
#                        "(c="auc/'$SUM'"%, q1="q1", v1="v1", p1="p1", q2="q2", v2="v2", p2="p2", q3="q3", v3="v3", p3="p3") | 1="$1"\n"; x=y}'\
#`
NFO=`echo -e "SIZE: $SIZE\nMAX: $MAX\nLINES: $LINES\nFIRST: $FIRST\n\nLAST: $LAST\nLINELEN: $LINELEN\n$V50\n" | perl -ne 'chomp; s/(\S+):(\s+)(\d+)(.*)/$count . $1 . "." x ((9-length($1))+(12-length($3))) . $3 . $4 . "\\\\n"/ge; print'`

echo "MAX   $MAX"
echo "SUM   $SUM"
echo "SUM   $SUM"
echo "SUM1  $SUM1"
echo "SUM2  $SUM2"
echo "SUM3  $SUM3"
echo -e "$V50"
echo -e "$NFO"
echo -e "$NFO" > $INHISTOFILE.desc


OLDEXT=histo
NEWEXT=stats
STATFILE=$INHISTOFILE
STATFILE="${STATFILE/%${OLDEXT}/${NEWEXT}}"
STATFILEN=`basename $STATFILE`
echo "STATFILE $STATFILEN"
#STAT=`cat $STATFILE | perl -ne 's/\\n/\\\\n/; print' | perl -ne 's/(?<=\:)(\s+)(?=\d+)/"." x (length($&))/ge; print'`
STAT=`cat $STATFILE | perl -ne 'chomp; s/\\n/\\\\n/; print' | perl -ne 's/(\S+):(\s+)(\d+)/uc($1) . "." x ((9-length($1))+(12-length($3))) . $3 . "\\\\n"/ge; print'`
echo -e "STAT\n$STAT"


Q1=`echo -e "$NFO" | grep "Q$IQ" | perl -ne 'if (/(\d+)$/) { print $1}'`
Q2=`echo -e "$NFO" | grep "Q50"  | perl -ne 'if (/(\d+)$/) { print $1}'`
Q3=`echo -e "$NFO" | grep "Q$Q"  | perl -ne 'if (/(\d+)$/) { print $1}'`

echo "Q$Q $Q1 Q50 $Q2 Q$IQ $Q3"

BOTS='
#set style  rect 1 fc lt -1 fs solid 0.10 noborder
#set style  rect 2 fc lt -1 fs solid 0.20 noborder

set object rect from '$Q1',0        to '$Q3','$MAX2'         behind fc lt -1 fs solid 0.10 noborder
set object rect from '$(($Q2-1))',0 to '$(($Q2+1))', '$MAX2' behind fc lt -1 fs solid 0.20 noborder
'

#echo BOTS $BOTS


TOPS=`cat $INHISTOFILE | head -n $LINES | perl -ane '

	BEGIN
	{
		@Y      = ();
		@min    = ();
		@max    = ();
		$pMax   = -100000000000000;
		$maxPos =  100000000000000;
		$pMin   =  100000000000000;
		$minPos;
	}
	END
	{
		#print "MAXIMUM = $pMax [$maxPos]\n";
		#foreach my $maxPair (@max)
		#{
		#	print "  MAX " . $maxPair->[0] . " = " . $maxPair->[1];
		#	print "(!)" if ( $maxPair->[1] == $pMax );
		#	print "\n";
		#}
		#
		#print "MINIMUM = $pMin [$minPos]\n";
		#foreach my $minPair (@min)
		#{
		#	print "  MIN " . $minPair->[0] . " = " . $minPair->[1];
		#	print "(!)" if ( $minPair->[1] == $pMin );
		#	print "\n";
		#}

		print "set arrow 1 from $minPos,"         . ($pMin+'$LINELEN')                  . " to $minPos,$pMin filled front ls 5\n";
		print "set arrow 2 from $maxPos,"         . ($pMax+'$LINELEN')                  . " to $maxPos,$pMax filled front ls 6\n";
		print "set label \"$minPos\" at $minPos," . ($pMin+'$LINELEN'+('$LINELEN'*0.1)) . "\n";
		print "set label \"$maxPos\" at $maxPos," . ($pMax+'$LINELEN'+('$LINELEN'*0.1)) . "\n";
	}
	$a=$F[0];
	$b=$F[1];
	$c=$a*$b;

	next if $a <= 3;
    next if $a >= '$LINES';
	if (( $c < $pMin ) && ( $a < $maxPos ))
	{
		push(@min, [$a,$c]);
		$pMin   = $c;
		$minPos = $a;
	}
	next if $a <= 3;
	if ( $c > $pMax )
	{
		push(@max, [$a,$c]);
		$pMax   = $c;
		$maxPos = $a;
	}

'`
#echo "$TOPS"




if [[ -z $MAX || $MAX == 0 ]]; then
	echo "NO MAX $MAX"
	exit 1
fi

if [[ -z $SIZE || $SIZE == 0 ]]; then
        echo "NO SUZE $SIZE"
        exit 1
fi

echo "EXPORTING GNUPLOT SCRIPT $TMPSHITO"
head -$LINES $INHISTOFILE | gawk 'BEGIN { v=0; cv=0; }; { v=($1*$2); if ($1 > '$MINY') { cv=cv+v; } print $1,$2,v,cv,((cv/'$SUM')*100) } ' > $TMPHISTO
#ln -s $INHISTOFILE in.histo

LINETYPE=impulses

echo "`cat <<ECHO

set title   "$TITLE"

set xlabel  "$XLABEL"
set ylabel  "$YLABEL"
set y2label "$Y2LABEL"

set xrange  [0:$LAST]
set yrange  [0:$MAX2]
set y2range [0:100]

set format y  "%2.0tx10^%L"
set format y2 "%3.0f"
set y2tics 0,25
set ytics nomirror
set y2tics out 

set label "STAT:\n$STAT"  at graph  0.74, graph  0.98
set label "INFO:\n$NFO"   at graph  0.74, graph  0.82

set bars large

set style line 1 lt rgb 'red'   lw 1
set style line 2 lt rgb 'green' lw 2
set style line 3 lt rgb 'blue'  lw 1

set style line 4 lt rgb 'pink'  lw 2

set style line 5 lt rgb 'red'   lw 3
set style line 6 lt rgb 'blue'  lw 3

$TOPS

set grid
set palette model RGB
set pointsize 0.5
set origin 0, 0

#set terminal png size 1024,768 large font "/usr/share/fonts/default/ghostscript/putr.pfa,12" $transparentStr
set terminal png size 1024,768 large font "Courier New,12" $transparentStr
set output "$OUTHISTOFILE"

$BOTS

plot '$TMPHISTO' [$FIRST:$LAST] using 1:3 with $LINETYPE ls 1 axis x1y1                 title "", \
     ''          [$FIRST:$LAST] using 1:3 with $LINETYPE ls 2 axis x1y1 smooth csplines title "", \
     ''          [$FIRST:$LAST] using 1:5 with $LINETYPE ls 3 axis x1y2 smooth csplines title ""

exit

ECHO`" > histo.plot

echo "RUNNING GNUPLOT"
gnuplot histo.plot

unlink $TMPHISTO
rm histo.plot



#rm histo.tmp

#set arrow 3 from 2,80000000  to 2,50000000  filled front ls 5
#set arrow 4 from 7,150000000 to 7,120000000 filled front ls 6


#set format x ""
#set size 1, 100
#set bmargin 0


#     ''             [$FIRST:$LAST] using 1:(\\$1*\\$2) with $LINETYPE ls 4 smooth kdensity title "$TITLE SMOOTH kdensity"
#     ''             [$FIRST:$LAST] using 1:(\\$1*\\$2) with $LINETYPE ls 2 smooth bezier   title "$TITLE SMOOTH bezier",   \


#a = 100
#f(x) = a + b*x + c*x**2 + d*x**3 + e*x**4 + f*x**5 + g*x**6 + h*x**7 + i*x**8 + j*x**9+ k*x**10 + l*x**11 + m*x**12 + n*x**13 
#fit [$FIRST:$LAST] f(x) "histo.tmp" via a,b,c,d,e,f,g,h,i,j,k,l,m,n
#spline f() 'histo.tmp'

#A=0; sa=1; sb=0; integ=0.0
#f(x) = a*x+b
#g(x) = (x>0?GPVAL_Y_MIN:GPVAL_Y_MAX)
##set xlabel 'Time [weeks]'
##set ylabel 'Crude oil price [$]'
#set grid front
#p 'histo.tmp' [$FIRST:$LAST] u 1:2

#xmax = GPVAL_DATA_X_MAX

#set output "$OUTHISTOFILE2"
#set print 'histo.tmp' append
#l 'derint_r.gnu'
#set print

#plot	'histo.tmp' [$FIRST:$LAST] u 1:2, \
#	'histo.tmp' [$FIRST:$LAST] u 1:(g(\\$2)) w filledcurve lc rgb "#000000"
#	'histo.tmp' u 1:2 w l
#plot 'histo.tmp' [$FIRST:$LAST] u 1:(g(2)) w filledcurve lc rgb "#eeeeee", 'histo.tmp' u 1:2 w l
