#!/bin/bash

BASE=/home/$USER/filter/Data
OUTF=/home/$USER/filter/out/Data_0_mer
MKPLOT=/home/$USER/filter/mkplot

SLEEPEVERY=2
WAITALL_DELAY=20

#THREADS
MAXNFO=20
MAXJELLY=5
MAXQC=10
MAXPROJ=2

function echoMsg () {
    FIL=$1
    LNO=$2
    DSC=$3
    shift; shift; shift
    MSG=$@
    printf "[%-20s :: %04d :: %b (%06d s)] %b\n" "$FIL" "$LNO" "$DSC" "$SECONDS" "$MSG"
    #echo "[$SEC :: $NAM :: $LNO$DSC] $MSG" | tee -a err.log
}


#JELLY
    DOKMER=0
    #NFO
        DONFO=1

    #JELLY
        DOJELLY=1
            REDOCOUNT=0
            REDOJELLY=0
            REDOJELLYMERGE=0
            REDOJELLYSTAT=0
            REDOJELLYHISTO=0
            REDOJELLYHISTOGRAPH=0

            JELLYDELETETEMPMER=1
            JELLYGRAPHLENGTH=50
            JELLYHISTOHIGH=$(($JELLYGRAPHLENGTH-1))
            JELLYTHREADS=8
            JELLYHASHSIZE=800000000
            JELLYBUFFERSIZE=100000000
            #HASHSIZE=8000000
            JELLYMERLEN=31
            JELLYCOUNTERLEN=7

    #PROJECT
        DOPROJECT=1
            REDOPROJMERGE=1
            REDOPROJSTAT=1
            REDOPROJHISTO=1
            REDOPROJHISTOGRAPH=1

            PROJDELETETEMPMER=0
            PROJGRAPHLENGTH=100
            PROJHISTOHIGH=$(($PROJGRAPHLENGTH-1))

    #PARENT
        DOPARENT=1
            REDOPARENTMERGE=1
            REDOPARENTSTAT=1
            REDOPARENTHISTO=1
            REDOPARENTHISTOGRAPH=1

            PARENTDELETETEMPMER=0
            PARENTGRAPHLENGTH=200
            PARENTHISTOHIGH=$(($PARENTGRAPHLENGTH-1))

#QC
     DOQC=1
        DOFASTQC=1
            REDOQC=0
            IGNOREQCERROR=0
            OUTQC="/home/aflit001/public_html_acessories/src/fastqc"
            QCP="/home/aflit001/bin/fqc2"
        DOSOLEXAQA=1
            REDOSOLQC=0
            SOLEXAQA="/home/aflit001/progs/solexaqa/SolexaQA.pl"
            OUTSOLEXAQA="/home/aflit001/public_html_acessories/src/solexaqa"
            SOLEXAQASAMPLESIZE=250000



if [[ $REDOJELLYMERGE -gt 0 && $REDOJELLY -eq 0 ]]; then
	echoMsg "$LINENO" "PRE" "IMPOSSIBLE TO REDO MERGE WITHOUTH REDO JELLY"
	exit 99
fi

function waitfor {
    FIL=$1
    LNO=$2
    DSC=$3
	PID=$4
    OUTNUM=$5
    shift; shift; shift; shift; shift;
    FILE=$@

    START=`date`
    BEGIN=`date +"%s"`

    DSC="$DSC (WAITING FOR)"
	echoMsg "$FIL" "$LNO" "$DSC" "PID $PID FILE $FILE"

    while :; do
        if kill -0 "$PID" 2>/dev/null; then
            NOW=`date +"%s"`
            ELA=$((($NOW-$BEGIN+1)/60))
            echoMsg "$FIL" "$LNO" "$DSC" "PID $PID FILE '$FILE' DSC '$DSC'\n\tSTARTED $START ELAPSED $ELA min"
        elif wait "$PID"; then
            echoMsg "$FIL" "$LNO" "$DSC" "    $PID exited with zero exit status $?"
            break
        else
            echoMsg "$FIL" "$LNO" "$DSC" "    $PID ($DSC) exited with NON-zero exit status $?"
            rm ${FILE[*]} 2>/dev/null
            exit $OUTNUM
        fi
        sleep $SLEEPEVERY
    done

}

function checkChildren {
    FIL=$1
    LNO=$2
    DSC=$3
    for element in `jobs -p`; do
        pid=$element

        #echoMsg "$LNO" "$DSC" "  checking pid $pid"

        if [[ -z "$pid" ]]; then
            #echoMsg "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC" "  *** PID $pid WAS NOT FOUND ***"
            continue
            #exitAll "$SECONDS" "$SCRIPT_NAME" "$$" "$LINENO" "$DSC"
        fi

        if kill -0 "$pid" 2>/dev/null; then
            #echoMsg "$LNO" "$DSC" "    $pid is still alive"
            echo -n ""
        elif wait "$pid"; then
            echoMsg "$FIL" "$LNO" "$DSC" "    $pid exited with zero exit status"
            continue
        else
            echoMsg "$FIL" "$LNO" "$DSC" "    **********************************************************************"
            echoMsg "$FIL" "$LNO" "$DSC" "    **********************************************************************"
            echoMsg "$FIL" "$LNO" "$DSC" "    ******** $pid exited with non zero exit status "`wait "$pid"`"********"
            echoMsg "$FIL" "$LNO" "$DSC" "    **********************************************************************"
            echoMsg "$FIL" "$LNO" "$DSC" "    **********************************************************************"
            exit `wait "$pid"`
        fi
    done
}

function waitall {
    FIL=$1
    LNO=$2
    DSC=$3
    LOOPCOUNT=0
    DSC="WAITING FOR ALL :: $DSC"
    #waitall LNO DSC

    echoMsg "$FIL" "$LNO" "$DSC" ""
    if [[ ! -z `jobs -p` ]]; then
        while [[ ! -z `jobs -p` ]]; do
            echoMsg "$FIL" "$LNO" "$DSC" " LOOP $LOOPCOUNT"
            LOOPCOUNT=$((LOOPCOUNT+1))

            while :; do
                echoMsg "$FIL" "$LNO" "$DSC" " Processes remaining: "`jobs -p`"\n\n"

                checkChildren "$FIL" "$LNO" "$DSC"

                JOBS=`jobs -p`
                if [[ -z "$JOBS" ]]; then
                    echoMsg "$FIL" "$LNO" "$DSC" "      NO MORE JOBS. BREAKING"
                    break
                fi
                # TODO: how to interrupt this sleep when a child terminates?
                sleep ${WAITALL_DELAY:-1}
            done
        done
    else
        echoMsg "$FIL" "$LNO" "$DSC" "NOTHING TO WAIT"
    fi
}

function runall {
    FIL=$1
    LNO=$2
    DSC=$3
    MAX=$4
    shift; shift; shift; shift;
    list=("$@")
    echoMsg "$FIL" "$LNO" "$DSC" "RUNALL :: MAX    $MAX"
    echoMsg "$FIL" "$LNO" "$DSC" "RUNALL :: LENGTH ${#list[@]}"
    echoMsg "$FIL" "$LNO" "$DSC" "RUNALL :: LIST   ${LIST[*]}"
    TOTAL=$(( ${#list[@]} - 1 ))

    for i in `seq 0 $TOTAL`; do
        JOBS=`jobs -p | wc -w`
        echoMsg "$FIL" "$LNO" "$DSC" "RUNALL ::   JOBS SO FAR $JOBS"

        while [[ ! -z "$JOBS" && "$JOBS" -gt "$MAX" ]]; do
            echoMsg       "$FIL" "$LNO" "$DSC" "RUNALL :: sleeping because $JOBS gt $MAX ($i/$TOTAL)"
            checkChildren "$FIL" "$LNO" "$DSC" "RUNALL :: sleeping ($i/$TOTAL)"
            sleep ${WAITALL_DELAY:-1}
            JOBS=`jobs -p | wc -w`
        done

        cmd=${list[i]}
        echoMsg "$FIL" "$LNO" "$DSC" "RUNALL ($i/$TOTAL) :: RUNNING $cmd"
        eval "$cmd &"
    done

    echoMsg "$FIL" "$LNO" "$DSC" "RUNALL :: FINISHED RUNNING ALL"
}


function runCmdSeveral {
    #runCmd "$SCRIPT_NAME" "$LINENO" "$IN" "$OUT" "$SUBTITLE" "$DESC" "$CMD" "$EXITCODE"
    SNAME=$1
    LNO=$2
    IN=$3
    OUT=$4
    SUBTITLE=$5
    DESC=$6
    CMD=$7
    EXITCODE=$8

    if [[ -f "$OUT" ]]; then
        rm "$OUT"
    fi


    echoMsg "$SNAME" "$LNO" "$SUBTITLE" "$DESC"
    eval "nohup $CMD &"
    PID=${!}
    waitfor "$SNAME" "$LNO" "$SUBTITLE" $PID $EXITCODE "CMD '$CMD' :: IN '$IN' OUT '$OUT'"
    echo -e "\n\n\n"


    if [[ ! -f "$OUT" ]]; then
        echoMsg "$SNAME" "$LNO" "$SUBTITLE" "FAILED TO CREATE '$OUT'. ERROR $EXITCODE ($CMD)"
        exit $EXITCODE
    fi


    if [[ ! -s "$OUT" ]]; then
        echoMsg "$SNAME" "$LNO" "$SUBTITLE" "FAILED TO CREATE '$OUT'. SIZE ZERO. ($CMD) ERROR $EXITCODE"
        exit $EXITCODE
    fi
}

function runCmd {
    SNAME=$1
    LNO=$2
    IN=$3
    OUT=$4
    SUBTITLE=$5
    DESC=$6
    CMD=$7
    EXITCODE=$8

    if [[ ! -f "$IN" || ! -s "$IN" ]]; then
        echoMsg "$SNAME" "$LNO" "$SUBTITLE" "INPUT '$IN' DOESNT EXISTS OR HAS SIZE 0. ERROR $EXITCODE"
        exit $EXITCODE
    fi

    runCmdSeveral "$SNAME" "$LNO" "$IN" "$OUT" "$SUBTITLE" "$DESC" "$CMD" "$EXITCODE"
}




