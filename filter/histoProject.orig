#!/bin/bash
#set -e

BASE=/home/$USER/filter/Data
OUTF=/home/$USER/filter/out/Data_0_mer
MKPLOT=/home/$USER/filter/mkplot

THREADS=16
HASHSIZE=800000000
#HASHSIZE=8000000
MERLEN=31
COUNTERLEN=7
SLEEPEVERY=30

if [[ -z "$1" ]]; then
	echo "PLEASE INFORM WHICH FOLDER WOULD YOU LIKE TO BE ANALYZED. ERROR 01"
	echo "E.G.: $0 Pennellii_Illumina_pairedend_300"
	exit 1
fi


if [[ -z "$2" ]]; then
	echo "RERUNING WITH TEE"
	NEWONE=`echo $1 | tr "/" "_"`
	mkdir log 2>/dev/null
	LOGNAME=log/$0"-"$NEWONE"-"`date -Iminutes | perl -ne "chomp; print"`".log"
	echo "LOGNAME: $LOGNAME"
	$0 $1 $LOGNAME | tee $LOGNAME
	STATUS=${PIPESTATUS[-2]}
	echo "EXITING STATUS $STATUS"
	exit $STATUS
else
	echo "ALREADY TEEDED. PROCEEDING"
	echo "PROJECT '$1' LOG '$2'"
fi

function waitfor {
	PID=$1
	FILE="$2"
        START=`date`
        BEGIN=`date +"%s"`

	echo "PID $PID FILE $FILE"

	while [ -d "/proc/$PID" ]
	do
		NOW=`date +"%s"`
        ELA=$((($NOW-$BEGIN+1)/60))
        echo "    STARTED $START :: ELAPSED $ELA min :: PID $PID"
        LSCMD="ls -l '$FILE' 2>/dev/null | gawk '{print $8\" \"$5/1024/1024/1024\"Gb\"}'"
        echo "LSCMD $LSCMD"
        eval "$LSCMD"
        sleep $SLEEPEVERY
    done
	ls -lh "$FILE"
}

RFOLDER=$1
LOGFILE=$2

#JELLY
    DOJELLY=1
        REDOCOUNT=0
        REDOJELLY=0
        REDOJELLYMERGE=0
        REDOJELLYSTAT=0
        REDOJELLYHISTO=0
        REDOJELLYHISTOGRAPH=0

        JELLYDELETETEMPMER=1
        JELLYGRAPHLENGTH=50
        JELLYHISTOHIGH=$(($JELLYGRAPHLENGTH-1))

#QC
     DOQC=1
        REDOQC=0
        IGNOREQCERROR=1
        OUTQC="/home/aflit001/public_html/fastqc"
        QCP="/home/aflit001/bin/fqc2"

#PROJECT
    DOPROJECT=1
        REDOPROJMERGE=0
        REDOPROJSTAT=0
        REDOPROJHISTO=0
        REDOPROJHISTOGRAPH=0

        PROJDELETETEMPMER=0
        PROJGRAPHLENGTH=100
        PROJHISTOHIGH=$(($PROJGRAPHLENGTH-1))

#PARENT
    DOPARENT=1
        REDOPARENTMERGE=0
        REDOPARENTSTAT=0
        REDOPARENTHISTO=0
        REDOPARENTHISTOGRAPH=0

        PARENTDELETETEMPMER=1
        PARENTGRAPHLENGTH=200
        PARENTHISTOHIGH=$(($PARENTGRAPHLENGTH-1))

if [[ $REDOJELLYMERGE -gt 0 && $REDOJELLY -eq 0 ]]; then
	echo "IMPOSSIBLE TO REDO MERGE WITHOUTH REDO JELLY"
	exit 99
fi


echo "HISTOHIGH $HISTOHIGH MKPLOTLINES $MKPLOTLINES"
#exit

if [ -d "$BASE/$RFOLDER" ]; then
    if [ -d "$BASE/$RFOLDER/"$RFOLDER"_Illumina" ]; then

        echo "ROOT :: $RFOLDER"
        for LFOLDER in $BASE/$RFOLDER/$RFOLDER\_Illumina/$RFOLDER\_Illumina*
        do
            if [ -d "$LFOLDER" ]; then
                LFOLDERSHORT=$LFOLDER
                LFOLDERSHORT=${LFOLDERSHORT##$BASE/$RFOLDER/$RFOLDER\_Illumina/}

                if [[ $DOJELLY -gt 0 ]]; then
                    echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDER"
                    echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: REQUEST FOLDER       "$RFOLDER
                    echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: LOCAL   FOLDER       "$LFOLDER
                    echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: LOCAL   FOLDER SHORT "$LFOLDERSHORT
                    echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: PWD                  "`pwd`
                    echo -e "\n\n\n"

                    for fastq in $LFOLDER/*.fastq
                    do
                        ###############################
                        ## ROOT :: JELLY
                        ###############################
                        ###############################
                        ## ROOT :: JELLY :: COUNT
                        ###############################
                        fastqBase=$LFOLDERSHORT"_"`basename $fastq`
                        echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase"
                        echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: COUNT"
                        echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: COUNT :: $fastq"

                        if [[ ! -f "$OUTF/$fastqBase.nfo" || $REDOCOUNT -gt 0 ]]; then
			                if [[ -f "$OUTF/$fastqBase.nfo" ]]; then
				                rm  "$OUTF/$fastqBase.nfo"
			                fi

                            echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: COUNT :: FASTQBASE $fastqBase"
                            FCOUNT="fastqCount -i $fastq -o $OUTF/$fastqBase.nfo"
                            echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: COUNT :: FCOUNT $FCOUNT"
                            eval "nohup $FCOUNT 2>> $LOGFILE &"
            			    waitfor $! "$OUTF/$fastqBase.nfo"
                            echo -e "\n\n\n"


                            if [[ ! -f "$OUTF/$fastqBase.nfo" ]]; then
                                echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: COUNT :: FAILED TO CREATE '$OUTF/$fastqBase.nfo'. ERROR 19"
                                exit 19
                            fi
                        else
                            echo -e "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: COUNT :: OUTPUT '$OUTF/$fastqBase.nfo' EXISTS. SKIPPING\n\n\n"
                        fi


                        ###############################
                        ## ROOT :: JELLY :: COUNT :: CHECK
                        ###############################
                        echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: COUNT :: CHECK"
                        echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: COUNT :: CHECK :: FASTQBASE $fastqBase"
                        KMERS=`cat $OUTF/$fastqBase.nfo | perl -ne 'if (/KMER:(\d+)/) { print $1 }'`

                        if [[ $KMERS == "0" ]]; then
                            echo -e "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: COUNT :: CHECK :: KMER COUNT ($KMERS) SHOWS NO RESULT. SKIPPING FILE\n\n\n"
                            continue
                        else
                            echo -e "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: COUNT :: CHECK :: KMER COUNT ($KMERS). PROCEDING\n\n\n"
                        fi



                        ###############################
                        ## ROOT :: JELLY :: MER_COUNT
                        ###############################
                        JOUT=$fastqBase"_mer_counts_0"
                        echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: MER_COUNT"
                        echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: MER_COUNT :: '$OUTF/$JOUT'"
                        if [[ ! -f "$OUTF/$JOUT"  || $REDOJELLY -gt 0 ]]; then
                            if [[ ! -f "$OUTF/$fastqBase.jf" || $REDOJELLY -gt 0 ]]; then
			                	if [[ -f "$OUTF/$JOUT" ]]; then
					                rm "$OUTF/"$fastqBase"_mer_"*
				                fi

                                if [[ -f "$OUTF/$fastqBase.jf" || $REDOJELLY -gt 0 || $REDOJELLYMERGE -gt 0 ]]; then
                                        rm "$OUTF/$fastqBase.jf"
                                fi

                                echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: MER_COUNT :: JELLY"
                                JELLY="jellyfish count --mer-len=$MERLEN --threads=$THREADS --counter-len=$COUNTERLEN --size=$HASHSIZE --both-strands -o $OUTF/"$fastqBase"_mer_counts $fastq"
                                echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: MER_COUNT :: JELLY :: $JELLY"
                                eval "nohup $JELLY  | tee $LOGFILE &"
				                waitfor $! "$OUTF/"$fastqBase"_mer_"*
                                echo -e "\n\n\n"

                                if [[ ! -f "$OUTF/$JOUT" ]]; then
                                    echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: MER_COUNT :: JELLY :: FAILED TO CREATE '$OUTF/$JOUT'. ERROR 18"
                                    exit 18
                                fi
                            else
                                echo -e "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: MER_COUNT :: JELLY :: MERGED OUTPUT '$OUTF/$fastqBase.jf' EXISTS. SKIPPING\n\n\n"
                            fi
                        else
                            echo -e "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: MER_COUNT :: JELLY :: OUTPUT '$OUTF/$JOUT' EXISTS. SKIPPING\n\n\n"
                        fi


                        ###############################
                        ## ROOT :: JELLY :: MER_COUNT :: MERGE
                        ###############################
                        echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: MER_COUNT :: MERGE"
                        if [[ ! -f "$OUTF/$fastqBase.jf" || $REDOJELLYMERGE -gt 0 ]]; then
                            if [[ -f "$OUTF/$JOUT"  ]]; then
				                if [[ -f "$OUTF/$fastqBase.jf"  ]]; then
					                rm "$OUTF/$fastqBase.jf"
				                fi

                                echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: MER_COUNT :: MERGE"
				                FILES=$OUTF/$fastqBase"_mer_counts_"*
				                #echo $FILES
				                #exit
                                JMERGE="jellyfish merge --buffer-size=$HASHSIZE -o $OUTF/$fastqBase.jf "`echo $FILES`
                                echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: MER_COUNT :: MERGE :: JELLY MERGE $JMERGE"
                                eval "nohup $JMERGE 2>> $LOGFILE &"
				                waitfor $! "$OUTF/$fastqBase.jf"
                                echo -e "\n\n\n"


                                if [[ ! -f "$OUTF/$fastqBase.jf" ]]; then
                                    echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: MER_COUNT :: MERGE :: FAILED TO CREATE '$OUTF/$fastqBase.jf'. ERROR 172"
                                    exit 172
                                fi


                                echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: MER_COUNT :: MERGE :: DELELTE TEMP MER"
                                if [[ $JELLYDELETETEMPMER -gt 0 ]]; then
                                        DELJELLYFILE="$OUTF/"$fastqBase"_mer_counts_"*
                                        echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: MER_COUNT :: MERGE :: DELELTE TEMP MER :: DELETING TEMP $DELJELLYFILE"
                                        rm $DELJELLYFILE 2>> $LOGFILE
                                else
                                        echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: MER_COUNT :: MERGE :: DELELTE TEMP MER :: NOT DELETING TEMP"
                                fi

                                if [[ ! -f "$OUTF/$fastqBase.jf" ]]; then
                                    echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: MER_COUNT :: MERGE :: FAILED TO CREATE '$OUTF/$fastqBase.jf'. ERROR 171"
                                    exit 171
                                fi

                            else
                                echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: MER_COUNT :: MERGE :: INPUT '$OUTF/$JOUT' DOES NOT EXISTS. ERROR 16"
                                exit 16
                            fi
                        else
                            echo -e "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: MER_COUNT :: MERGE :: OUTPUT '$OUTF/$fastqBase.jf' EXISTS. SKIPPING\n\n\n"
                        fi


                        #########################
                        ## ROOT :: JELLY :: STAT
                        #########################
                        echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: STATS"
                        if [[ ! -f "$OUTF/$fastqBase.stats" || $REDOJELLYSTAT -gt 0 ]]; then
			                if [[ -f "$OUTF/$fastqBase.stats" ]]; then
				                rm "$OUTF/$fastqBase.stats"
			                fi

                            echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: STATS :: '$OUTF/$fastqBase.stats'"
                            STAT="jellyfish stats -o $OUTF/$fastqBase.stats $OUTF/$fastqBase.jf"
                            echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: STATS :: $STAT"
                            eval "nohup $STAT 2>> $LOGFILE &"
			                waitfor $! "$OUTF/$fastqBase.stats"
                            echo -e "\n\n\n"

                            if [[ ! -f "$OUTF/$fastqBase.stats" ]]; then
                                echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: STATS :: FAILED TO CREATE '$OUTF/$fastqBase.stats'. ERROR 15"
                                exit 15
                            fi
                        else
                            echo -e "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: STATS :: OUTPUT '$OUTF/$fastqBase.stats' EXISTS. SKIPPING\n\n\n"
                        fi


                        #########################
                        ## ROOT :: JELLY :: HISTOGRAM
                        #########################
                        echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: HISTOGRAM"
                        if [[ ! -f "$OUTF/$fastqBase.histo" || $REDOJELLYHISTO -gt 0 ]]; then
			                if [[ -f "$OUTF/$fastqBase.histo"  ]]; then
				                rm "$OUTF/$fastqBase.histo"
			                fi
                            HIS="jellyfish histo --low=1 --high=$JELLYHISTOHIGH --increment=1 --buffer-size=$HASHSIZE --threads=$THREADS -o $OUTF/$fastqBase.histo $OUTF/$fastqBase.jf";
                            echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: HISTOGRAM ::  $HIS"
                            eval "nohup $HIS 2>> $LOGFILE &"
			                waitfor $! "$OUTF/$fastqBase.histo"
                            echo -e "\n\n\n"

                            if [[ ! -f "$OUTF/$fastqBase.histo" ]]; then
                                echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: HISTOGRAM :: FAILED TO CREATE '$OUTF/$fastqBase.histo'. ERROR 14"
                                exit 14
                            fi
                        else
                            echo -e  "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: HISTOGRAM :: OUTPUT '$OUTF/$fastqBase.histo' EXISTS. SKIPPING\n\n\n"
                        fi



                        #########################
                        ## ROOT :: JELLY :: HISTOGRAM :: GRAPH
                        #########################
                        export MKPLOTLINES=$JELLYGRAPHLENGTH
                        echo -e  "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: HISTOGRAM :: GRAPH"
                        if [[ ! -f "$OUTF/$fastqBase.histo.png" || $REDOJELLYHISTOGRAPH -gt 0 ]]; then
			                if [[ -f "$OUTF/$fastqBase.histo.png" ]]; then
				                rm "$OUTF/$fastqBase.histo.png"
			                fi
                            PLOT="$MKPLOT $OUTF/$fastqBase.histo"
                            echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: HISTOGRAM :: GRAPH :: MAKE PLOT $PLOT"
                            eval "nohup $PLOT 2>> $LOGFILE &"
			                waitfor $! "$OUTF/$fastqBase.histo.png"
                            echo -e "\n\n\n"


                            if [[ ! -f "$OUTF/$fastqBase.histo.png" ]]; then
                                echo "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: HISTOGRAM :: GRAPH :: FAILED TO CREATE '$OUTF/$fastqBase.histo.png'. ERROR 13"
                                exit 13
                            fi
                        else
                            echo -e "ROOT :: JELLY :: $RFOLDER :: $LFOLDERSHORT :: $fastqBase :: HISTOGRAM :: GRAPH :: OUTPUT '$OUTF/$fastqBase.histo.png' EXISTS. SKIPPING\n\n\n"
                        fi
                    done
                    #DONE for fastq in $LFOLDER/*.fastq
                fi




                if [[ $DOQC -gt 0 ]]; then
                    for fastq in $LFOLDER/*.fastq
                    do
                        ###############################
                        ## ROOT :: QC
                        ###############################
                        fastqShort=`basename $fastq`
                        fastqName=$fastqShort
                        fastqName=${fastqName%%.fastq}
                        fastqBase=$LFOLDERSHORT"_"$fastqShort
                        OUTQCDS="$OUTQC/$RFOLDER/$LFOLDERSHORT"
                        OUTQCFQ="$OUTQC/$RFOLDER/$LFOLDERSHORT/$fastqName""_fastqc"

                        echo "ROOT :: QC :: RFOLDER      = $RFOLDER"
                        echo "ROOT :: QC :: LFOLDERSHORT = $LFOLDERSHORT"
                        echo "ROOT :: QC :: FASTQ        = $fastq"
                        echo "ROOT :: QC :: FASTQ BASE   = $fastqBase"
                        echo "ROOT :: QC :: FASTQ SHORT  = $fastqShort"
                        echo "ROOT :: QC :: FASTQ NAME   = $fastqName"
                        echo "ROOT :: QC :: OUTQC        = $OUTQC"
                        echo "ROOT :: QC :: OUTQCDS      = $OUTQCDS"
                        echo "ROOT :: QC :: OUTQCFQ      = $OUTQCFQ"
                        echo "ROOT :: QC :: $RFOLDER"
                        echo "ROOT :: QC :: $RFOLDER :: $LFOLDERSHORT"
                        echo "ROOT :: QC :: $RFOLDER :: $LFOLDERSHORT :: $fastqName"
                        echo "ROOT :: QC :: $RFOLDER :: $LFOLDERSHORT :: $fastqName :: $fastq"

                        if [[ ! -d "$OUTQC" ]]; then
                            mkdir $OUTQC
                        fi
                        if [[ ! -d "$OUTQC/$RFOLDER" ]]; then
                            mkdir $OUTQC/$RFOLDER
                        fi
                        if [[ ! -d "$OUTQC/$RFOLDER/$LFOLDERSHORT" ]]; then
                            mkdir $OUTQC/$RFOLDER/$LFOLDERSHORT
                        fi


                        if [[ ! -d "$OUTQCFQ/" && ! -f $OUTQCFQ.broken || $REDOQC -gt 0 ]]; then
			                if [[ -d "$OUTQCFQ/" ]]; then
				                rm -rf "$OUTQCFQ"
				                rm $OUTQCFQ.zip
			                fi

                            echo "ROOT :: QC :: $RFOLDER :: $LFOLDERSHORT :: $fastqName :: QC :: DESTINY $OUTQCDS"
                            echo "ROOT :: QC :: $RFOLDER :: $LFOLDERSHORT :: $fastqName :: QC :: OUTPUT  $OUTQCFQ"
                            QCCMD="$QCP -o '$OUTQCDS' $fastq"
                            echo "ROOT :: QC :: $RFOLDER :: $LFOLDERSHORT :: $fastqName :: QC :: CMD $QCCMD"
                            eval "nohup $QCCMD 2>> $LOGFILE &"
            			    waitfor $! "$OUTQCFQ"


                            if [[ ! -d "$OUTQCFQ" ]]; then
                                if [[ $IGNOREQCERROR -eq 0 ]]; then
                                    echo "ROOT :: QC :: $RFOLDER :: $LFOLDERSHORT :: $fastqName :: QC :: FAILED TO CREATE '$OUTQCFQ'. ERROR 31"
                                    exit 31
                                else
                                    echo "ROOT :: QC :: $RFOLDER :: $LFOLDERSHORT :: $fastqName :: QC :: FAILED TO CREATE '$OUTQCFQ' BUT SET TO IGNORE. CONTINUING. ERROR 31b"
                                    rm $OUTQCFQ.zip
                                    touch $OUTQCFQ.broken
                                fi
                            fi
                            echo -e "\n\n\n"
                        else
                            echo -e "ROOT :: QC :: $RFOLDER :: $LFOLDERSHORT :: $fastqName :: QC :: OUTPUT '$OUTQCFQ' EXISTS. SKIPPING\n\n\n"
                        fi
                    done
                fi




                if [[ $DOPROJECT -gt 0 ]]; then
                    ###############################
                    ## ROOT :: PROJECT
                    ###############################
                    ###############################
                    ## ROOT :: PROJECT :: MERGE
                    ###############################
                    echo "ROOT :: PROJECT :: $RFOLDER :: $LFOLDERSHORT"
                    echo "ROOT :: PROJECT :: $RFOLDER :: $LFOLDERSHORT :: MERGE"

                    JF="$LFOLDERSHORT.jf"
                    if [[ ! -f "$OUTF/$JF" || $REDOPROJMERGE -gt 0 ]]; then
			            if [[ -f "$OUTF/$JF" ]]; then
				            rm "$OUTF/$JF"
			            fi

                        MERGE="jellyfish merge --buffer-size=$HASHSIZE -o $OUTF/$JF $OUTF/$LFOLDERSHORT*.jf"
                        echo "ROOT :: PROJECT :: $RFOLDER :: $LFOLDERSHORT :: MERGE :: BASENAME $LFOLDERSHORT"
                        eval "nohup $MERGE 2>> $LOGFILE &"
			            waitfor $! "$OUTF/$JF "
                        echo -e "\n\n\n"


                        if [[ ! -f "$OUTF/$JF" ]]; then
                            echo "ROOT :: PROJECT :: $RFOLDER :: $LFOLDERSHORT :: MERGE :: FAILED TO CREATE '$OUTF/$JF'. ERROR 122"
                            exit 122
                        fi


                        if [[ $PROJDELETETEMPMER -gt 0 ]]; then
                            DELPROJFILE="$OUTF/$LFOLDERSHORT*.jf"
                            echo "ROOT :: PROJECT :: $RFOLDER :: $LFOLDERSHORT :: MERGE :: PROJECT DELETE PROJ TEMP $DELPROJFILE"
                            rm $DELPROJFILE 2>> $LOGFILE
                        else
                            echo "ROOT :: PROJECT :: $RFOLDER :: $LFOLDERSHORT :: MERGE :: PROJECT NOT DELETING PROJ TEMP"
                        fi


                        if [[ ! -f "$OUTF/$JF" ]]; then
                            echo "ROOT :: PROJECT :: $RFOLDER :: $LFOLDERSHORT :: MERGE :: FAILED TO CREATE '$OUTF/$JF'. ERROR 121"
                            exit 121
                        fi
                    else
                        echo -e "ROOT :: PROJECT :: $RFOLDER :: $LFOLDERSHORT :: MERGE :: OUTPUT '$OUTF/$JF' EXISTS. SKIPPING\n\n\n"
                    fi


                    ###############################
                    ## ROOT :: PROJECT :: STATS
                    ###############################
                    echo "ROOT :: PROJECT :: $RFOLDER :: $LFOLDERSHORT :: MERGE :: STATS"
                    if [[ ! -f "$OUTF/$LFOLDERSHORT.stats"  || $REDOPROJSTAT -gt 0 ]]; then
			            if [[ -f "$OUTF/$LFOLDERSHORT.stats"  ]]; then
				            rm "$OUTF/$LFOLDERSHORT.stats"
			            fi
                        MSTAT="jellyfish stats -o $OUTF/$LFOLDERSHORT.stats $OUTF/$JF";
                        echo "ROOT :: PROJECT :: $RFOLDER :: $LFOLDERSHORT :: MERGE :: STATS :: $MSTAT"
                        eval "nohup $MSTAT 2>> $LOGFILE &"
			            waitfor $! "$OUTF/$LFOLDERSHORT.stats"
                        echo -e "\n\n\n"


                        if [[ ! -f "$OUTF/$LFOLDERSHORT.stats" ]]; then
                            echo "ROOT :: PROJECT :: $RFOLDER :: $LFOLDERSHORT :: MERGE :: STATS :: FAILED TO CREATE '$OUTF/$LFOLDERSHORT.stats'. ERROR 11"
                            exit 11
                        fi
                    else
                        echo -e "ROOT :: PROJECT :: $RFOLDER :: $LFOLDERSHORT :: MERGE :: STATS :: OUTPUT '$OUTF/$LFOLDERSHORT.stats' EXISTS. SKIPPING\n\n\n"
                    fi


                    #########################
                    ## ROOT :: PROJECT :: MERGE :: HISTOGRAM
                    #########################
                    echo "ROOT :: PROJECT :: $RFOLDER :: $LFOLDERSHORT :: MERGE :: HISTOGRAM"
                    if [[ ! -f "$OUTF/$LFOLDERSHORT.histo" || $REDOPROJHISTO -gt 0 ]]; then
			            if [[ -f "$OUTF/$LFOLDERSHORT.histo" ]]; then
				            rm "$OUTF/$LFOLDERSHORT.histo"
			            fi
                        MHIS="nohup jellyfish histo --low=1 --high=$PROJHISTOHIGH --increment=1 --buffer-size=$HASHSIZE --threads=$THREADS -o $OUTF/$LFOLDERSHORT.histo $OUTF/$JF";
                        echo "ROOT :: PROJECT :: $RFOLDER :: $LFOLDERSHORT :: MERGE :: HISTOGRAM :: $MHIS"
                        eval "$MHIS 2>> $LOGFILE &"
			            waitfor $! "$OUTF/$LFOLDERSHORT.histo"
                        echo -e "\n\n\n"


                        if [[ ! -f "$OUTF/$LFOLDERSHORT.histo"  ]]; then
                            echo "ROOT :: PROJECT :: $RFOLDER :: $LFOLDERSHORT :: MERGE :: HISTOGRAM :: FAILED TO CREATE '$OUTF/$LFOLDERSHORT.histo'. ERROR 10"
                            exit 10
                        fi
                    else
                        echo -e "ROOT :: PROJECT :: $RFOLDER :: $LFOLDERSHORT :: MERGE :: HISTOGRAM :: OUTPUT '$OUTF/$LFOLDERSHORT.histo' EXISTS. SKIPPING\n\n\n"
                    fi


                    #########################
                    ## ROOT :: PROJECT :: MERGE :: HISTOGRAM :: GRAPH
                    #########################
                    echo "ROOT :: PROJECT :: $RFOLDER :: $LFOLDERSHORT :: PROJECT :: MERGE :: HISTOGRAM :: GRAPH"
                    export MKPLOTLINES=$PROJGRAPHLENGTH
                    if [[ ! -f "$OUTF/$LFOLDERSHORT.histo.png" || $REDOPROJHISTOGRAPH -gt 0 ]]; then
			            if [[ -f "$OUTF/$LFOLDERSHORT.histo.png" ]]; then
				            rm "$OUTF/$LFOLDERSHORT.histo.png"
			            fi
                        PLOT="$MKPLOT $OUTF/$LFOLDERSHORT.histo"
                        echo "ROOT :: PROJECT :: $RFOLDER :: $LFOLDERSHORT :: MERGE :: HISTOGRAM :: GRAPH :: $PLOT"
                        eval "$PLOT 2>> $LOGFILE &"
			            waitfor $! "$OUTF/$LFOLDERSHORT.histo.png"
                        echo -e "\n\n\n"


                        if [[ ! -f "$OUTF/$LFOLDERSHORT.histo.png" ]]; then
                            echo "ROOT :: PROJECT :: $RFOLDER :: $LFOLDERSHORT :: MERGE :: HISTOGRAM :: GRAPH :: FAILED TO CREATE '$OUTF/$LFOLDERSHORT.histo.png'. ERROR 09"
                            exit 9
                        fi
                    else
                        echo -e  "ROOT :: PROJECT :: $RFOLDER :: $LFOLDERSHORT :: MERGE :: HISTOGRAM :: GRAPH :: OUTPUT '$OUTF/$LFOLDERSHORT.histo.png' EXISTS. SKIPPING\n\n\n"
                    fi
                fi

            else #if [ -d "$LFOLDER" ]; then
                echo "ROOT :: PROJECT :: $RFOLDER :: $LFOLDERSHORT :: MERGE :: HISTOGRAM :: GRAPH :: $BASE/$RFOLDER/"$RFOLDER"_Illumina/$LFOLDER IS NOT A FOLDER OR HASNT CHILDREN. ERROR 08"
                exit 8
            fi
        done # for LFOLDER in $BASE/$RFOLDER/$RFOLDER\_Illumina/$RFOLDER\_Illumina*



        if [[ $DOPARENT -gt 0 ]]; then
            ###############################
            ## ROOT :: PARENT
            ###############################
            echo "ROOT :: PARENT"
            echo "ROOT :: PARENT :: $RFOLDER"
            echo "ROOT :: PARENT :: $RFOLDER :: MERGE"
            echo "ROOT :: PARENT :: $RFOLDER :: MERGE :: "$RFOLDER"_Illumina"
            PARENT=$RFOLDER"_Illumina"
            PJF="$PARENT.jf"


            ###############################
            ## ROOT :: PARENT :: MERGE
            ###############################
            if [[ ! -f "$OUTF/$PJF" || $REDOPARENTMERGE -gt 0 ]]; then
		        if [[ -f "$OUTF/$PJF" ]]; then
			        rm "$OUTF/$PJF"
		        fi
                SOURCES=`ls $OUTF/"$PARENT"_*.jf | grep -v ".fastq.jf" | perl -ne 'chomp; print; print " "'`
                MERGE="jellyfish merge --buffer-size=$HASHSIZE -o $OUTF/$PJF $SOURCES"
                echo "ROOT :: PARENT :: $RFOLDER :: MERGE :: BASENAME $PARENT"
                echo "ROOT :: PARENT :: $RFOLDER :: MERGE :: $MERGE"
                eval "nohup $MERGE 2>> $LOGFILE &"
		        waitfor $! "$SOURCES  $OUTF/$PJF"
		        echo -n "\n\n\n"

                if [[ ! -f "$OUTF/$PJF" ]]; then
                    echo "ROOT :: PARENT :: $RFOLDER :: MERGE :: FAILED TO CREATE '$OUTF/$PJF'. ERROR 72"
                    exit 72
                fi


                if [[ $PARENTDELETETEMPMER -gt 0 ]]; then
                    #DELPARENTFILE="$OUTF/$PARENT*.jf"
                    DELPARENTFILE=$SOURCES
                    echo "ROOT :: PARENT :: $RFOLDER :: MERGE :: DELETE PROJ TEMP $DELPARENTFILE"
                    rm $DELPARENTFILE 2>> $LOGFILE
                else
                    echo "ROOT :: PARENT :: $RFOLDER :: MERGE :: NOT DELETING PROJ TEMP"
                fi

                if [[ ! -f "$OUTF/$PJF" ]]; then
                    echo "ROOT :: PARENT :: $RFOLDER :: MERGE :: FAILED TO CREATE '$OUTF/$PJF'. ERROR 71"
                    exit 71
                fi

            else
                echo -e "ROOT :: PARENT :: $RFOLDER :: MERGE :: OUTPUT '$OUTF/$PJF' EXISTS. SKIPPING\n\n\n"
            fi



            ###############################
            ## ROOT :: PARENT :: MERGE :: STAT
            ###############################
            echo "ROOT :: PARENT :: $RFOLDER :: MERGE :: STAT"

            if [[ ! -f "$OUTF/$PARENT.stats" || $REDOPARENTSTAT -gt 0 ]]; then
		        if [[ -f "$OUTF/$PARENT.stats" ]]; then
			        rm "$OUTF/$PARENT.stats"
		        fi
                MSTAT="jellyfish stats -o $OUTF/$PARENT.stats $OUTF/$PJF";
                echo "ROOT :: PARENT :: $RFOLDER :: MERGE :: STAT :: $MSTAT"
                eval "nohup $MSTAT 2>> $LOGFILE &"
		        waitfor $! "$OUTF/$PARENT.stats"
                echo -e "\n\n\n"


                if [[ ! -f "$OUTF/$PARENT.stats" ]]; then
                    echo "ROOT :: PARENT :: $RFOLDER :: MERGE :: STAT :: FAILED TO CREATE '$OUTF/$PARENT.stats'. ERROR 06"
                    exit 6
                fi
            else
                echo "ROOT :: PARENT :: $RFOLDER :: MERGE :: STAT :: OUTPUT '$OUTF/$PARENT.stats' EXISTS. SKIPPING"
            fi



            #########################
            ## ROOT :: PARENT :: MERGE :: HISTOGRAM
            #########################
            echo "ROOT :: PARENT :: $RFOLDER :: MERGE :: HISTOGRAM"
            if [[ ! -f "$OUTF/$PARENT.histo" || $REDOPARENTHISTO -gt 0 ]]; then
		        if [[ -f "$OUTF/$PARENT.histo" ]]; then
			        rm "$OUTF/$PARENT.histo"
		        fi
                MHIS="jellyfish histo --low=1 --high=$PARENTHISTOHIGH --increment=1 --buffer-size=$HASHSIZE --threads=$THREADS -o $OUTF/$PARENT.histo $OUTF/$PJF";
                echo "ROOT :: PARENT :: $RFOLDER :: MERGE :: HISTOGRAM :: $MHIS"
                eval "nohup $MHIS 2>> $LOGFILE &"
		        waitfor $! "$OUTF/$PARENT.histo"
                echo -e "\n\n\n"


                if [[ ! -f "$OUTF/$PARENT.histo" ]]; then
                    echo "ROOT :: PARENT :: $RFOLDER :: MERGE :: HISTOGRAM :: FAILED TO CREATE '$OUTF/$PARENT.histo'. ERROR 05"
                    exit 5
                fi
            else
                echo -e "ROOT :: PARENT :: $RFOLDER :: MERGE :: HISTOGRAM :: OUTPUT '$OUTF/$PARENT.histo' EXISTS. SKIPPING\n\n\n"
            fi



            #########################
            ## ROOT :: PARENT :: MERGE :: HISTOGRAM :: GRAPH
            #########################
            export MKPLOTLINES=$PARENTGRAPHLENGTH
            echo "ROOT :: PARENT :: $RFOLDER :: MERGE :: HISTOGRAM :: GRAPH"
            if [[ ! -f "$OUTF/$PARENT.histo.png" || $REDOPARENTHISTOGRAPH -gt 0 ]]; then
		        if [[ -f "$OUTF/$PARENT.histo.png" ]]; then
			        rm "$OUTF/$PARENT.histo.png"
		        fi
                PLOT="$MKPLOT $OUTF/$PARENT.histo"
                echo "ROOT :: PARENT :: $RFOLDER :: MERGE :: HISTOGRAM :: GRAPH :: $PLOT"
                eval "nohup $PLOT 2>> $LOGFILE &"
		        waitfor $! "$OUTF/$PARENT.histo"
                echo -e "\n\n\n"


                if [[ ! -f "$OUTF/$PARENT.histo.png" ]]; then
                    echo  "ROOT :: PARENT :: $RFOLDER :: MERGE :: HISTOGRAM :: GRAPH :: FAILED TO CREATE '$OUTF/$PARENT.histo.png'. ERROR 04"
                    exit 4
                fi
            else
                echo -e  "ROOT :: PARENT :: $RFOLDER :: MERGE :: HISTOGRAM :: GRAPH :: OUTPUT '$OUTF/$PARENT.histo.png' EXISTS. SKIPPING\n\n\n"
            fi
        fi
    else
        echo "ROOT :: '$BASE/$RFOLDER/"$RFOLDER"_Illumina' IS NOT A FOLDER OR HASNT CHILDREN. ERROR 03"
        exit 3
    fi
else
    echo "ROOT :: '$BASE/$RFOLDER' IS NOT A FOLDER OR HASNT CHILDREN. ERROR 02"
    exit 2
fi

echo "COMPLETED SUCCESSFULLY"

exit 0
