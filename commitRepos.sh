#!/bin/bash

if [[ ! -d ".git" ]]; then
    rm -rf * .*
    ln ../git_accessories/commitRepos.sh .
    ln ../git_accessories/repos .
    ln ../git_accessories/gitCommitHome .
    ln ../git_accessories/gitCommitHomeJgit .
    ln ../git_accessories/sync .

    git init
    git add .
    git commit -am "initial commit"
fi



BASE=/home/aflit001
DATESTR=`date --rfc-3339=seconds | tr " |,|:|\+" _`

#git config --global user.email sauloal@yahoo.com.br
#git config --global user.name "saulo"
#git remote add github  git@github.com:sauloal/pythonscripts.git
#git push github master:gamma

echo "UPDATING INDIVIDUAL REPO"
for repo in `cat repos | grep -v \#`
do
    cd $repo
    base=`basename $repo`
    echo "UPDATING INDIVIDUAL REPO :: $base ($repo)"

    LOG=$BASE/tmp/gitLog/git_$DATESTR\_$base.log


    if [[ ! -d ".git" ]]; then
        echo "UPDATING INDIVIDUAL REPO :: $base :: INITING GIT"
        git init
    else
        echo "UPDATING INDIVIDUAL REPO :: $base :: ALREADY UNDER GIT"
        #rm -rf .git
        #git init
    fi


    git config pack.window 25M           2>&1 | tee -a $LOG
    git config pack.windowMemory 25M     2>&1 | tee -a $LOG
    git config pack.compression 1        2>&1 | tee -a $LOG
    git config pack.packSizeLimit 25M    2>&1 | tee -a $LOG
    git config pack.deltaCacheSize 25M   2>&1 | tee -a $LOG
    git config pack.deltaCacheLimit 25M  2>&1 | tee -a $LOG
    git config core.bigFileThreshold 25M 2>&1 | tee -a $LOG


    if [[ -f ".gitadd" ]]; then
        echo "UPDATING INDIVIDUAL REPO :: $base :: USING GITADD"
        adds=`cat .gitadd`
        git add -v $adds 2>&1 | tee -a $LOG
    else
        echo "UPDATING INDIVIDUAL REPO :: $base :: ADDING ALL"
        git add -v .     2>&1 | tee -a $LOG
    fi

    echo "UPDATING INDIVIDUAL REPO :: $base :: STATUS"
    git status                        2>&1 | tee -a $LOG

    STATUSN=`git status -s | grep -v \?\? | wc -l`
    STATUSN=$((STATUSN-1))
    STATUST=`git status -s | grep -v \?\? | perl -MFile::Basename -ane 'BEGIN { %p } END { map { my $k = $_; print $k, " => ", (scalar keys %{$p{$k}}), "\n"; } sort keys %p; } $d=dirname $F[1]; $p{$F[0]}{$d}++;'`
    STATUSL=`git status -s | grep -v \?\? | perl -MFile::Basename -ane 'BEGIN { %p } END { map { my $k = $_; print $k, " => ", (scalar keys %{$p{$k}}), "\n"; } sort keys %p; foreach my $k (sort keys %p) { foreach my $kk ( sort keys %{$p{$k}} ) { print "\t$kk => $p{$k}{$kk}\n"; } } } $d=dirname $F[1]; $p{$F[0]}{$d}++;'`

    if [[ ! -z $(git ls-files --deleted) ]]; then
        git rm $(git ls-files --deleted)  2>&1 | tee -a $LOG
    fi


    echo "STATUS $STATUS ($STATUSN)"
    echo "STATUS $STATUS ($STATUST)"
    git commit -am "$DATESTR :: $STATUSN :: $STATUST"  2>&1 | tee -a $LOG

    cd -
done



LOG=$BASE/tmp/gitLog/git_$DATESTR.log

echo "UPDATING GLOBAL"
echo "UPDATING GLOBAL :: $PWD"
for repo in `cat repos | grep -v \#`
do
    base=`basename $repo`
    echo "UPDATING GLOBAL :: $PWD :: $base ($repo)"

    #http://jasonkarns.com/blog/merge-two-git-repositories-into-one/

    if [[ ! -d "$base" ]]; then
        git config pack.window 25M           2>&1 | tee -a $LOG
        git config pack.windowMemory 25M     2>&1 | tee -a $LOG
        git config pack.compression 1        2>&1 | tee -a $LOG
        git config pack.packSizeLimit 25M    2>&1 | tee -a $LOG
        git config pack.deltaCacheSize 25M   2>&1 | tee -a $LOG
        git config pack.deltaCacheLimit 25M  2>&1 | tee -a $LOG
        git config core.bigFileThreshold 25M 2>&1 | tee -a $LOG

        echo "UPDATING GLOBAL :: $base :: FIRST TIME"
        #mkdir $base

        git commit -v -am "first commit of $base"

        echo "UPDATING GLOBAL :: $base :: FIRST TIME :: ADDING"
        git remote add -f $base $repo/

        #echo "UPDATING GLOBAL :: $base :: FIRST TIME :: MERGING $base/master"
        #git merge -s ours --no-commit $base/master

        echo "UPDATING GLOBAL :: $base :: FIRST TIME :: READING TREE"
        git read-tree --prefix=$base/ -u $base/master

        echo "UPDATING GLOBAL :: $base :: FIRST TIME :: COMMITING"
        git commit -v -am "merging $repo INTO $base. $DATESTR"

    else
        echo "UPDATING GLOBAL :: $base :: UPDATE $base FOR SUBTREE"
        git config pack.window 25M           2>&1 | tee -a $LOG
        git config pack.windowMemory 25M     2>&1 | tee -a $LOG
        git config pack.compression 1        2>&1 | tee -a $LOG
        git config pack.packSizeLimit 25M    2>&1 | tee -a $LOG
        git config pack.deltaCacheSize 25M   2>&1 | tee -a $LOG
        git config pack.deltaCacheLimit 25M  2>&1 | tee -a $LOG
        git config core.bigFileThreshold 25M 2>&1 | tee -a $LOG

        git pull -v -s subtree $base master
    fi
done


if [[ ! -z $(git ls-files --deleted) ]]; then
    git rm $(git ls-files --deleted)  2>&1 | tee -a $LOG
fi
git commit -v -am "COMMITING BASE $base. $DATESTR"
