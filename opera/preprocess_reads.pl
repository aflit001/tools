#!/usr/bin/perl -w
use strict;

my $bowtiePath = "";

# preprocess of reads and map them using bowtie

if( @ARGV != 4 || $ARGV[ 0 ] eq "-h" )
{
	print "Usage:\n";
	print "\tpreprocess.pl  <cf> <rf1> <rf2> <output> \n";
	print "\t\t<cf>: Multi-fasta contig file\n";
	print "\t\t<rf1>: Fasta/Fastq file for first reads\n";
	print "\t\t<rf2>: Fasta/Fastq file for second reads\n";
	print "\t\t<output>: Name for output file\n";
	exit( 1 );
}

my ( $contigFile, $readFile1, $readFile2, $outputFile ) = @ARGV;

die "CONTIG FILE $contigFile DOES NOT EXISTS" if ! -f $contigFile;
die "READ FILE 1 $readFile1  DOES NOT EXISTS" if ! -f $readFile1;
die "READ FILE 2 $readFile2  DOES NOT EXISTS" if ! -f $readFile2;

die "CONTIG FILE $contigFile IS EMPTY"        if ! -s $contigFile;
die "READ FILE 1 $readFile1  IS EMPTY"        if ! -s $readFile1;
die "READ FILE 2 $readFile2  IS EMPTY"        if ! -s $readFile2;

#die "OUTPUT FILE $outputFile EXISTS. DELETE TO CONTINUE" if -f $outputFile;

if( $bowtiePath !~ "/\$" and $bowtiePath ne "" )
{
	$bowtiePath .= "/";
}


# create output folder
#`touch $outputFile 2> err`;

my $folder = "./";
if( $outputFile =~ m/(.*)\/(.*)/ )
{
	$folder     = $1."/";
	$outputFile = $2;
}
mkdir($folder);


if( $? == -1 || $? == 256 )
{
	my $time = localtime;
	print "[$time] ";
	print "ERROR: Directory \"".$folder."\" does not exist.\n";
	`rm err`;
	exit( 2 );
}
`rm err`;

my $fasta = "";

open( READ, "$readFile1" ) or die $!;
while( my $line = <READ> )
{
	if( $line =~ "^>" )
	{ 
        $fasta = "-f"; 
        print "Fasta format is recognized\n"; 
        last; 
    }
	elsif( $line =~ "^@" )
	{ 
        $fasta = "-q"; 
        print "Fastq format is recognized\n"; 
        last; 
    }
	else
	{ 
        print "reads format is not correct\n"; 
        exit( 3 ); 
    }
}

my $type = "fs"; #fasta
my $line = <READ>;
if( $line =~ m/(0|1|2|3)/ )
{
    $type = "cs"; #color space
}

close READ;


my $readFile1Base = `basename $readFile1`;
my $readFile2Base = `basename $readFile2`;
chomp($readFile1Base);
chomp($readFile2Base);

my $readFile = "${folder}combined_$readFile1Base\_$readFile2Base.fasta";

if ( ! -f $readFile )
{
    # combine the reads together and change the name
    my $time = localtime;
    print "[$time] ";
    print "Combining read files for bowtie mapping [$readFile]...\n";

    open( F,      "<$readFile1" ) or die $!;
    open( S,      "<$readFile2" ) or die $!;
    open( OUTPUT, ">$readFile"  ) or die $!;
    my $num = 1;
    while( my $first = <F> )
    {
        die if ! defined $first;

        if( $fasta eq "-f" )
        { # fasta
        	print OUTPUT ">r$num".".1\n";
	        $first = <F>;
            die if ! defined $first;
		
        	my $second = <S>;
            die if ! defined $second;
	        print OUTPUT ">r$num".".2\n";

        	$second = <S>;
            die if ! defined $second;
	        print OUTPUT $second;
        }
        else
        { # fastaq
	        print OUTPUT "\@r$num".".1\n";

	        $first = <F>;
            die if ! defined $first;
    	    print OUTPUT $first;

	        $first = <F>;
            die if ! defined $first;
	        print OUTPUT "+r$num".".1\n";

    	    $first = <F>;
            die if ! defined $first;
	        print OUTPUT $first;

    	    my $second = <S>;
            die if ! defined $second;
	        print OUTPUT "\@r$num".".2\n";

	        $second = <S>;
            die if ! defined $second;
    	    print OUTPUT $second;

	        $second = <S>;
            die if ! defined $second;
	        print OUTPUT "+r$num".".2\n";

    	    $second = <S>;
            die if ! defined $second;
	        print OUTPUT $second;
        }
        $num++;
    }
    close S;
    close F;
    close OUTPUT;
} else {
    my $time = localtime;
    print "[$time] ";
    print "Combined file already exists... skipping ... delete $readFile to force combination\n";
}


# build the index 
my $time = localtime;
print "[$time] ";
print "Building bowtie index ...\n";
my @contigName = split( "/", $contigFile );
if ( ! -f "$folder$contigName[ -1 ].1.ebwt" )
{
    my $command = "${bowtiePath}bowtie-build " . (  $type eq "cs" ? "-C " : "" ) . "$contigFile $folder$contigName[ -1 ]";
    `$command`;
    if( $? == -1 )
    {
        my $time = localtime;
	    print "[$time] ";
    	print "ERROR: Running bowtie-build error with the following command.\n";
	    print $command."\n";
    	print "Please make sure both bowtie-build and input files do exist!\n";
    	exit( 4 );
    }

    if ( -f "$folder$outputFile" )
    {
        `rm $folder$outputFile`;
    }
} else {
    my $time = localtime;
    print "[$time] ";
    print "  Bowtie index already exists... skipping... delete $folder$contigName[ -1 ].* to force rebuild index\n";
}

# map with bowtie
$time = localtime;
print "[$time] ";
print "Mapping reads using bowtie ...\n";

if ( ! -f "$folder$outputFile" )
{
    #my $command = "${bowtiePath}bowtie -v 3 -a -m 1 -t " . (  $type eq "cs" ? "-C " : "" ) . "$fasta -p 5 $folder$contigName[ -1 ] $readFile 2>${folder}bowtie.err | sort --compress-program=gzip -T ./ -n > $folder$outputFile";
    my $command = "${bowtiePath}bowtie -v 3 -a -m 1 -t " . (  $type eq "cs" ? "-C " : "" ) . "$fasta -p 5 $folder$contigName[ -1 ] $readFile 2>/dev/null | sort --compress-program=gzip -T ./ -n > $folder$outputFile";

    `$command`;

    if( $? == -1 )
    {
        my $time = localtime;
	    print "[$time] ";
    	print "ERROR: Running bowtie error with the following command.\n";
	    print $command."\n";
    	print "Please make sure both bowtie and input files do exist!\n";
        unlink("$folder$outputFile");
	    exit( 5 );
    }
} else {
    my $time = localtime;
    print "[$time] ";
    print "  Bowtie map already exists... skipping... delete $folder$outputFile to force remap\n";
}

# finalize
$time = localtime;
print "[$time] ";
print "Preprocessing done!\n";
