#!/usr/bin/perl -w
use strict;
use Date::Format;
use lib "/home/aflit001/lib/perl/lib/perl5/";
use HTML::Entities;
use XML::Dumper;
use File::Basename;
use File::MimeInfo;
use File::stat;
use Cwd 'abs_path';
#use Time::localtime;
use Time::ParseDate;

use CGI qw(:standard);
use CGI::Debug;

#export REMOTE_USER=pig

my $verbose        = 0;
my $debug          = 0;
   $ENV{ERROR}     = '';
my $ERROR          = \$ENV{ERROR};

my $setupBase      = "/home/aflit001/public_html_acessories";
my $setupFile      = "$setupBase/db.xml";
my $accessFile     = "$setupBase/access.xml";
my $REMOTE_USER    = $ENV{REMOTE_USER};
die if ! defined $REMOTE_USER;
#REMOTE_USER=ab
#AUTH_TYPE=Basic

print "Content-type: text/html\n\n" if $debug;

my $setup          = loadSetup($setupFile);
my $access         = loadSetup($accessFile);
my $userPerms      = loadPerms($REMOTE_USER, $access);
exit if ! defined $userPerms;

my $HTMLBASE       = exists ${$setup}{HTMLBASE}    ? $setup->{HTMLBASE}    : &ndef( print "HTML BASE PATH NOT DEFINED" );
my $progs          = exists ${$setup}{progs}       ? $setup->{progs}       : &ndef( print "programs not defined" );
my $speciesBase    = exists ${$setup}{speciesBase} ? $setup->{speciesBase} : &ndef( print "species Base not defined" );

my @allowedDirs    = ('/mnt/nexenta/aflit001/nobackup/filter/Data_0_mer/', $setupBase); # far from ideal
my @allowedSubDirs = ('src', 'db');
my %converters     = (
    fn => {
        'fastqc_report.html.*' => {
            #regex    => [ 'img src="', 'img src="/~aflit001/cgi-bin/getter.cgi?src=' ],
            function => \&fixFastqc
        }
    }
);


my $src;
if (param() && param('src')) {
    $src = param('src');
} else {
    $$ERROR =  "no src\n";
    print $$ERROR if $debug;
    exit 0;
}



$$ERROR =  "SRC1 $src\n";
print $$ERROR if $debug;
$src = $setupBase . "/$src"; #DANGEROUS
$src =~ s/\/\//\//g;

my $aldSubCount = 0;
for my $allowed (@allowedSubDirs) {
    $$ERROR =  "CHECKING $allowed\n";
    print $$ERROR if $debug;

    if ( $src =~ /^$setupBase\/$allowed/ )
    {   

        if ( $src =~ /^$setupBase\/$allowed\/(\S+?)\/(\S+?)\// ) {
            my $prog = $1;
            my $spp  = $2;

            if ( $spp !~ /$userPerms/ ) {
                $$ERROR = "SPECIES $spp NOT ALLOWED USING $userPerms";
                exit 0;
            }
            
            $aldSubCount++;
            last;
        } else {
            $$ERROR = "NOT ABLE TO DETERMINE SPECIES FOR FILE $src";
            exit 0;
        }
    }
}

if ( ! $aldSubCount ) {
    $$ERROR =  "src subdir $src out of allowed folder\n";
    print $$ERROR if $debug;

    exit 0;
}



$$ERROR =  "SRC2 $src\n";
print $$ERROR if $debug;
$src = abs_path($src);
#$src = File::Spec->rel2abs( $src );
#$src = File::Spec::Link->linked($src);

if ( ! defined $src )
{
    $$ERROR =  "src returned invalid path\n";
    print $$ERROR if $debug;
    exit 0;
}
$$ERROR =  "SRC3 $src\n";
print $$ERROR if $debug;


my $aldCount = 0;
for my $allowed (@allowedDirs) {
    $$ERROR =  "CHECKING $allowed\n";
    print $$ERROR if $debug;

    if ( $src =~ /^$allowed/ )
    {
        $aldCount++;
        last;
    }
}

if ( ! $aldCount ) {
    $$ERROR =  "src out of base folder\n";
    print $$ERROR if $debug;

    exit 0;
}


if ( ! -f $src ) {
    $$ERROR =  "src does not exists\n";
    print $$ERROR if $debug;
    exit 0;
}




my $mime_type   = mimetype($src);
my $name        = basename($src);
my $template    = '%a, %d %b %Y %T %Z';

my $stat        = stat($src);
my $file_size   = $stat->size;


my @ctime       = localtime($stat->ctime);
my @mtime       = localtime($stat->mtime);

my $ctime       = strftime($template, @ctime);
my $mtime       = strftime($template, @mtime);

                        
#my $datetime = '2004-05-07 18:00:48';
#$ctime = time2str($template, parsedate($datetime));

#http://www.codingforums.com/archive/index.php/t-10452.html
#print "Content-type: $mime_type\n\n";
print "Content-type: $mime_type\n";
print "Last-Modified: $mtime\n";
print "ETag: $src$mtime\n";
print "Cache-Control: public\n";
print "Content-transfer-encodig: binary\n";

#print "Content-disposition: attachment; filename=$name; size=$file_size; creation-date=\"$date_string\"; modification-date=\"$date_string\"\n\n";

my @needConv;
foreach my $conv ( keys %{$converters{fn}} ) {
    #print "TEST CONV $conv\n";
    if ( $src =~ /$conv/ ) {
        #print "SRC $src HAS $conv\n";
        push(@needConv, $conv);
    }
}

if ( ! scalar @needConv ) {
    print "Content-length: $file_size\n";
    print "Content-disposition: inline; filename=\"$name\"; size=$file_size; creation-date=\"$ctime\"; modification-date=\"$mtime\"\n\n";
    open FI, "<$src" or die "";
    binmode(FI);
    print  <FI>;
    close   FI;
} else {
    my $lines_size = 0;
    open FI, "<$src" or die "";
    #print "NEED CONV\n";
    my @lines;
    while ( my $line = <FI> ) {
        foreach my $conv ( @needConv ) {
            #print "CONV $conv\n";
            #if ( exists ${ $converters{fn}{$conv}}{regex} ) {
            #    my $regex = $converters{fn}{$conv}{regex};
            #    my $from  = $regex->[0];
            #    my $to    = $regex->[1];
            #    #print "  FROM $from TO $to\n";
            #    $line     =~ s/$from/$to/g;
            #}
        
            if ( exists ${ $converters{fn}{$conv}}{function} ) {
                my $func  = $converters{fn}{$conv}{function};
                $line     = $func->($src, $line);
            }
        }
        $lines_size += length($line);
        push(@lines, $line);
    }

    close FI;
    
    print "Content-length: $lines_size\n";
    print "Content-disposition: inline; filename=\"$name\"; size=$lines_size; creation-date=\"$ctime\"; modification-date=\"$mtime\"\n\n";
    print join("", @lines);
}


sub loadSetup      {
    my $file = shift;
    my %hash;
    if ( -f "$file" )
    {
        print "LOCAL DB '$file' EXISTS\n" if $verbose;
        
        my $dump = new XML::Dumper;    

        my $ref = \$dump->xml2pl( $file );
        
        if ( $ref ) {
    #        print Dumper($ref);
            #print $$ref;
    #        print Dumper($ds);
            %hash = %{${$ref}};
    #        print Dumper($ds);
            return \%hash;
        } else {
            return \%hash;
        }
    } else {
        print "LOCAL DB '$file' DOES NOT EXISTS\n";
        exit 1;
        return \%hash;
    }
}


sub loadPerms {
    my $un = shift;
    my $st = shift;
    #my %access = (
    #ab    => '.+',
    #pig   => 'Pig',
    #three => 'Pennellii|AllRound|F5',
    #heinz => 'Heinz'

    if ( exists ${$st}{$un} ) {
        return $st->{$un};
    } else {
        return;
    }
}

sub fixFastqc {
    my $srcl = shift;
    my $line = shift;
       $srcl =~ s/$setupBase//g;
       $srcl =~ s/fastqc_report.html//g;
       $srcl = substr($srcl, 1) if ( substr($srcl, 0, 1) eq "/");
    my $from = 'img(.*?)src="';
    my $to   = 'img $1 src="/~aflit001/cgi-bin/getter.cgi?src=' . $srcl;
       $line =~ s/$from/$to/g;
    return $line;
}

1;


