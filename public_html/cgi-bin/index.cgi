#!/usr/bin/perl -w
use strict;

my $qcdir = ["/home/aflit001/public_html/fastqc", "fastqc", "/fastqc_report.html"];
my $BASE  = "/~aflit001";

print "Content-type: text/html\n\n";
print <<HTML
<html>
<head></head>
    <body>
        <h1>Dynamic Content of Aflit0001 @ dev1.ab.wur.nl</h1>
        <a href="$BASE/cgi-bin/INGSQR.cgi">Integrated NGS Quality Report</a><br/>
        <table>
HTML
;


#my $qctable = &genFolderList($qcdir);
#print $qctable;


print <<HTML
        </table>
    </body>
</html>
HTML
;


sub genFolderList () {
    my $indir = $_[0][0];
    my $oudir = $_[0][1];
    my $index = defined $_[0][2] ? $_[0][2] : "";
    
    if ( -d $indir ) {
        if (opendir(my $dh, $indir)) {
            my @folders = grep { ! /^\./ && -d "$indir/$_" } readdir($dh);
            closedir $dh;
            
            my $table  = "<table>\n";
               $table .= "\t<th><td>$oudir</td></th>";
            
            foreach my $folder ( sort @folders )
            {
                $folder =~ s/^$indir//;
                $table .= "\t<tr><td><a href='$BASE/$oudir/$folder$index'>$folder</a></td></tr>\n";
            }
            
            $table .= "</table>\n";
            return $table;
        } else {
            return "<table></table>\n";
        }
    } else {
        return "<table></table>\n";
    }
}

1;

