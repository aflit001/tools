#!/usr/bin/perl -w
use HTML::Entities;
use lib "/home/aflit001/lib/perl/lib/perl5/";
use XML::Dumper;
use File::Basename;
use CGI qw(:standard);
use strict;
#export REMOTE_USER=pig



my $verbose     = 0;
my $debug       = 0;

my $setupBase   = "/home/aflit001/public_html_acessories";
my $setupFile   = "$setupBase/db.xml";
my $accessFile  = "$setupBase/access.xml";
my $REMOTE_USER = $ENV{REMOTE_USER};
#REMOTE_USER=ab
#AUTH_TYPE=Basic


my $setup       = loadSetup($setupFile);
my $access      = loadSetup($accessFile);
my $userPerms   = loadPerms($REMOTE_USER, $access);


my $HTMLBASE    = exists ${$setup}{HTMLBASE}    ? $setup->{HTMLBASE}    : &ndef( print "HTML BASE PATH NOT DEFINED" );
my $progs       = exists ${$setup}{progs}       ? $setup->{progs}       : &ndef( print "programs not defined" );
my $speciesBase = exists ${$setup}{speciesBase} ? $setup->{speciesBase} : &ndef( print "species Base not defined" );

my $htmlBase    = "$HTMLBASE/cgi-bin/" . basename($0);
my $getter      = "$HTMLBASE/cgi-bin/getter.cgi";
my $s           = "\t";
my $dbgArea     = "";

#my $titleHight = 215;
my $imgWidth    = 640;#480; #320; #640;
my $imgHeight   = 480;#360; #240; #480;
#my $divWidth   = $imgWidth  * 1.1;
#my $divHeight  = $imgHeight * 1.1;
my $span_width  = 60;
my $span_1      = "span-" . $span_width;
my $span_2      = "span-" . int($span_width / 2);
my $span_3      = "span-" . int($span_width / 3);
my $span_4      = "span-" . int($span_width / 4);
my $span_5      = "span-" . int($span_width / 5);
my $span_6      = "span-" . int($span_width / 6);
my $span_u      = "span-" . 1;
my $span_1_1    = "span-" . $span_width -1;
my $span_l      = "span-" . int($span_width / 2.5);



my $htmlTitle   = "Integrated NGS Quality Report :: Aflit001 @ assembly.ab.wur.nl";
my $titleCols   =  5;
my $sumCols     =  0;
my $titleHight  = 10;
my $maxLenCol   =  0;
foreach my $prog ( keys %$progs )
{
    my $cols = $progs->{$prog}{cols};
    $sumCols += scalar keys %{$cols};
    foreach my $col ( keys %{$cols} )
    {
        $maxLenCol = length($col) if (length($col) > $maxLenCol);
    }
}

$titleHight = $maxLenCol * 7;#6.5;

#use Data::Dumper;
#print Dumper($struct);
#exit;

my $JS         = &getJs();
my $CSS        = &getCss();
my $reqSpp;
if (param() && param('spp')) {
    $reqSpp = param('spp');
}




















if ( $debug ) {
    $dbgArea = '
    <div id="area" style="display: inline; float: right; position: absolute; right: 0px; top: 30px; width: 500px; z-index: 1001;">
        <!-- <textarea name="comments" cols="60" rows="9" id="area"></textarea> -->
    </div>
    <script type="text/javascript" language="JavaScript">
        function scriptDebugger(debugText) {
            ar           = document.getElementById(\'area\');
            ar.innerHTML = "<small><pre>"+debugText+"</pre></small>";
            ad           = document.getElementById(\'area\');
            ad.style.top = cYA + "px";
        }
        scriptDebugger("debugText\n")
    </script>' . "\n";
} else {
$dbgArea = '
    <script type="text/javascript" language="JavaScript">
        function scriptDebugger(debugText) {
        }
    </script>' . "\n";
}








my $allowedSpps = &getSpecies($speciesBase, $userPerms);
my @tabs        = ( 
    ["Quality Check - Pre Cleaning",  \&getTab1], 
    ["Cleaning",                      \&getTab2], 
    ["Quality Check - Post Cleaning", \&getTab3],
    ["Assembly",                      \&getTab3]
);


my ($fixed, $fixedSize) = exists $allowedSpps->{$reqSpp} ? &getFixed("$setupBase/src/zip", $reqSpp) : undef;
my $fixedStr            = $fixed ? "<br><div class='download'><a href='$getter?src=src/zip/$reqSpp/$fixed'><img class='icon' src='$HTMLBASE/images/download_box.png'/></a><small>$fixedSize Mb</small></div>\n" : '';





print "Content-type: text/html\n\n";
print <<HTML
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>$htmlTitle</title>
</head>
    <body>
        <div class="container">
        <div class="h2"><h2>$htmlTitle</h2></div>
        <div class="h4"><h4>USER</h4><br>$REMOTE_USER<small><a href="$HTMLBASE/EXIT">[logout]</a></small><br><h4 class="alt">FILTERS</h4><br>$userPerms$fixedStr</div>
HTML
;

&struct2Table($speciesBase, $HTMLBASE, $progs, $reqSpp, $userPerms, $allowedSpps, 5);

print <<HTML
        $CSS
        $JS
        $dbgArea
    </body>
</html>
HTML
;











sub struct2Table   {
    my $stpPath = shift;
    my $base    = shift;
    my $filters = shift;
    my $req     = shift;
    my $perms   = shift;
    my $spps    = shift;
    my $start   = shift;

    my $level   = 0;

        #my $data    = shift;
        #my $setupFile  = "$setupBase/db.xml";
        #my $struct     = defined $setup ? $setup->{struct} : exit 0;

        #$struct{$spp}{type}                                        = "Species";
        ##$struct{$spp}{info}                                        = getInfo($indir, $spp);
        #
        #$struct{$spp}{CHILD}{$tec}{type}                           = "Technology";
        #$struct{$spp}{CHILD}{$tec}{info}                           = getInfo($indir, $spp, $tec);
        #
        #$struct{$spp}{CHILD}{$tec}{CHILD}{$lib}{type}              = "Library";
        #$struct{$spp}{CHILD}{$tec}{CHILD}{$lib}{info}              = getInfo($indir, $spp, $lib);
        #
        #$struct{$spp}{CHILD}{$tec}{CHILD}{$lib}{CHILD}{$run}{type} = "Run";
        #$struct{$spp}{CHILD}{$tec}{CHILD}{$lib}{CHILD}{$run}{kmer} = getInfo($indir, $spp, $run);
        #$struct->{$spp}{CHILD}{$tec}{CHILD}{$lib}{CHILD}{$run}{fasq} = $status;
        #print "ADDING $key1 -> $key2 -> $key3\n" if $verbose;








    print $s x ($start + 0) . "<table id='results' class='$span_1' cellpadding=0 cellspacing=0 cellwidth=0 border=0>\n";
    my $class      = "grandtitle";
    print $s x ($start + 0) . "<thead class='$span_1'>\n";
    print $s x ($start + 0) . "<tr class=\"$class $span_1\">\n";
    print $s x ($start + 1) . "<td class=\"grandtitle $span_1\" colspan=\"" . ($sumCols+1). "\">\n";
    print $s x ($start + 2) . "TOTAL SPECIES: ", scalar keys %$spps, "\n";
    print $s x ($start + 1) . "</td>\n";
    print $s x ($start + 0) . "</tr>\n";






    print $s x ($start + 0) . "<tr class=\"$class $span_1\">\n";
    print $s x ($start + 1) . "<td class=\"$class $class\_list $span_1\" colspan=\"" . ($sumCols+1). "\">\n";

    print $s x ($start + 2) . "<form action=\"$htmlBase\" method=\"get\">\n";
    foreach my $spp ( sort keys %$spps )
    {
        print $s x ($start + 3) . "<input type=\"submit\" name=\"spp\" value=\"$spp\">\n";
    }
    print $s x ($start + 2) . "</form>\n";

    print $s x ($start + 1) . "</td>\n";
    print $s x ($start + 0) . "</tr>\n";

    return if ( ! defined $reqSpp );
    return if ( ! exists ${$spps}{$reqSpp} );
    





    print $s x ($start + 0) . "<tr class=\"$class $class\_list $span_1\">\n";
    print $s x ($start + 1) . "<td class=\"$class $class\_list $span_1\" colspan=\"" . ($sumCols+1). "\">\n";
    print $s x ($start + 1) . ":: $reqSpp ::\n";
    print $s x ($start + 1) . "</td>\n";
    print $s x ($start + 0) . "</tr>\n";







    print <<TABS
    <tr>
        <td>
        <div id="tabs" class="$span_1" style='padding:0; border: 0'>
            <ul class="$span_1_1"  style='padding:0; border: 0'>
TABS
;


    for (my $c = 0; $c < @tabs; ++$c ) { 
        my $name = $tabs[$c][0];
        my $func = $tabs[$c][1];        
        print $s x ($start + 3) . "<li class='span-". (int($span_width / @tabs) - 1 )."' style='padding:0; border: 0'>
            <a href='#tabs-".($c+1)."'>$name</a>
        </li>\n";
    }

    print $s x ($start + 2) . "</ul>\n";

    for (my $c = 0; $c < @tabs; ++$c ) {
        my $name = $tabs[$c][0];
        my $func = $tabs[$c][1];
        print $s x ($start + 3) . "<div id='tabs-".($c+1)."' class='$span_1' style='padding:0; border: 0'>\n";
        $func->($stpPath, $base, $filters, $req, $perms, $spps, $start, $level);
        print $s x ($start + 3) . "</div>\n";
    }   


    print <<TABS
        </td>
    </tr>
TABS
;

    print $s x ($start + 0) . "</table id='results'>\n";
}





sub getTab1()
{   
    my $stpPath = shift;
    my $base    = shift;
    my $filters = shift;
    my $req     = shift;
    my $perms   = shift;
    my $spps    = shift;
    my $start   = shift;
    my $level   = shift;

    print $s x ($start + 0) . "<table id='results2' class='$span_1' cellpadding=0 cellspacing=0 cellwidth=0 border=0>\n";
    print $s x ($start + 0) . "<tbody class='$span_1'>\n";
    if ( exists ${$spps}{$reqSpp} ) {
        my $spp    = $reqSpp;
        my $sppNfo = &getSppNfo($stpPath, $spp);
        
        #use Data::Dumper;
        #print Dumper($struct);
        #print Dumper($sppNfo);
        
        
        ++$level;
        my $class      = "titles titles_label";
        
        
        my $titles1 = "";
        $titles1 .= $s x ($start + 0) . "<tr class=\"$class $span_1\">\n";
        $titles1 .= $s x ($start + 1) . "<td class='rowtitle $span_l' colname='rowtitle'></td>\n";
        foreach my $prog ( sort keys %$filters )
        {
            my $cols    = $filters->{$prog}{cols};
            my $progLen = scalar keys %{$cols};
            my $help    = $filters->{$prog}{help};
            my $hlpIcon = '';
            if ( defined $help && $help ) {
                $hlpIcon = "<a class='help' href='$help' target='_blank'><img class='question' src='$HTMLBASE/images/question.png'/></a>";
            }
            $titles1 .= $s x ($start + 1) . "<td colspan='$progLen' class='span-".($progLen-1)."'>$prog$hlpIcon</td>\n";
        }
        $titles1 .= $s x ($start + 0) . "</tr>\n";






        $class      = "titles";
        my $titles2 = "";
        $titles2 .= $s x ($start + 0) . "<tr class=\"$class $span_1\">\n";
        $titles2 .= $s x ($start + 1) . "<td class='rowtitle $span_l' colname='rowtitle'></td>\n";

        foreach my $prog ( sort keys %$filters )
        {
            my $cols = $filters->{$prog}{cols};
            foreach my $title ( sort keys %{$cols})
            {
                my $titleS = lc($title); $titleS =~ s/ //g;
                $titles2 .= $s x ($start + 1) . "<td class='coltitle $span_u' colname='$titleS' valign='bottom' align='center' style='valign=bottom; align=left; text-align=center'>\n";
                $titles2 .= $s x ($start + 2) . "<script type='image/svg+xml'>\n";
                $titles2 .= $s x ($start + 3) . "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' height='".$titleHight."px'>\n";
                $titles2 .= $s x ($start + 4) . "<text x='-".$titleHight."' y='10' font-family='sans-serif' font-size='14' transform='rotate(-90)' text-rendering='optimizeSpeed' fill='#FFF'>$title</text>\n";
                $titles2 .= $s x ($start + 3) . "</svg>\n";
                $titles2 .= $s x ($start + 2) . "</script>\n";
                $titles2 .= $s x ($start + 1) . "</td>\n";
            }
        }
        $titles2 .= $s x ($start + 0) . "</tr>\n";
        
        print $titles1;
        print $titles2;
        print $s x ($start + 0) . "</thead>\n";
        print $s x ($start + 0) . "<tbody class='$span_1'>\n";


        
        my $sppType = $sppNfo->{type};
        my $techs   = $sppNfo->{CHILD};
        die "NO TYPE TO SPP $spp" if ! defined $sppType;
        
        $class      = "species";
        
        
        print $s x ($start + 0)       . "<tr class=\"$class $span_1\">\n";
        print $s x ($start + 1)       . "<td class='rowtitle $span_l' colname='rowtitle'>\n";
        print $s x ($start + 2)       . uc($sppType) . " $spp (TECHS: ",(scalar keys %$techs),")\n";
        print $s x ($start + 1)       . "</td>\n";
        
        &parser(($start + 1), $spp, $filters, $sppNfo);
        #print "" . ($s x ($start + 1) . "<td></td>\n") x $sumCols;
        
        print $s x ($start + 0) . "</tr>\n";
        
        foreach my $tech ( sort keys %$techs )
        {
            ++$level;
            my $techNfo  = $techs->{$tech};
            my $techType = $techNfo->{type};
            my $libs     = $techNfo->{CHILD};
            $class       = "techs";
            
            print $s x ($start + 0)       . "<tr class=\"$class $span_1\">\n";
            print $s x ($start + 1)       . "<td class='rowtitle $span_l' colname='rowtitle'>\n";
            print $s x ($start + 2)       . uc($techType) . " $tech (LIBS: ",(scalar keys %$libs),")\n";
            print $s x ($start + 1)       . "</td>\n";

            
            
            &parser(($start + 1), "$spp :: $tech", $filters, $techNfo);
            #print "" . ($s x ($start + 1) . "<td></td>\n") x $sumCols;
            
            print $s x ($start + 0) . "</tr>\n";
            
            foreach my $lib ( sort keys %$libs )
            {
                ++$level;
                my $libNfo  = $libs->{$lib};
                my $libType = $libNfo->{type};
                my $runs    = $libNfo->{CHILD};
                $class      = "libs";
                
                print $s x ($start + 0)       . "<tr class=\"$class $span_1\">\n";
                print $s x ($start + 1)       . "<td class='rowtitle $span_l' colname='rowtitle'>\n";
                print $s x ($start + 2)       . uc($libType) . " $lib (RUNS: ",(scalar keys %$runs),")\n";
                print $s x ($start + 1)       . "</td>\n";
                
                &parser(($start + 1), "$spp :: $tech :: $lib", $filters, $libNfo);
                #print "" . ($s x ($start + 1) . "<td></td>\n") x $sumCols;
                
                print $s x ($start + 0)       . "</tr>\n";
                
                foreach my $run ( sort keys %$runs )
                {
                    ++$level;
                    my $runNfo  = $runs->{$run};
                    my $runType = $runNfo->{type};
                    $class      = "run";
                    
                    #use Data::Dumper;
                    #print Dumper($runNfo);
                    #exit 0;
                    
                    print $s x ($start + 0)       . "<tr class=\"$class $span_1\">\n";
                    print $s x ($start + 1)       . "<td class='rowtitle $span_l' colname='rowtitle'>\n";
                    print $s x ($start + 2)       . uc($runType) . " $run\n";
                    print $s x ($start + 1)       . "</td>\n";
                    
                    &parser(($start + 1), "$spp :: $tech :: $lib :: $run", $filters, $runNfo);
                    
                    print $s x ($start + 0)       . "</tr>\n";
                    --$level;
                }
                --$level;
            }
            --$level;
        }
        --$level;

        print $s x ($start + 0) . "</tbody>\n";
        print $s x ($start + 0) . "<tfoot class='$span_1'>\n";
        print $titles2;
        print $titles1;
        print $s x ($start + 0) . "</tfoot>\n";
    }
    print $s x ($start + 0) . "</table id='results2'>\n";
}




sub getTab2()
{   
    my $stpPath = shift;
    my $base    = shift;
    my $filters = shift;
    my $req     = shift;
    my $perms   = shift;
    my $spps    = shift;
    my $start   = shift;
    my $level   = shift;

    print "not implemented yet\n";
}




sub getTab3()
{   
    my $stpPath = shift;
    my $base    = shift;
    my $filters = shift;
    my $req     = shift;
    my $perms   = shift;
    my $spps    = shift;
    my $start   = shift;
    my $level   = shift;

    print "not implemented yet\n";
}


sub getSpecies {
    my $stpPath = shift;
    my $perms   = shift;
    my %spps;
    
    if (opendir(my $dh, $stpPath)) {
        #print "folder $stpPath exists\n";
        my @spps;
        push(@spps, grep { ! /^\./ && /\.xml$/ && !/db.xml/ && -f "$stpPath/$_" && -s "$stpPath/$_" } readdir($dh));
        closedir $dh;
        #print @spps;
        foreach my $spp ( @spps )
        {
            $spp =~ s/\.xml$//; 
            $spps{$spp}++ if $spp =~ /$perms/;
        }
        #print %spps;
        return \%spps;
    } else {
        #print "folder $stpPath does not exists\n";
        return \{};
    }
}




sub getSppNfo {
    my ($stpPath, $spp) = @_;

    my $setupFile  = "$stpPath/$spp.xml";
    #print "SETUP FILE $setupFile\n";
    
    if ( -f "$setupFile" ) {
        #print "  FILE EXISTS\n";
        my $setup      = loadSetup($setupFile);
        #use Data::Dumper;
        #print Dumper($setup);
        return $setup;
    } else {
        #print "  FILE DOESNT EXISTS\n";
        return \{};
    }
}





sub parser         {
    my $space   = shift;
    my $name    = shift;
    my $filters = shift;
    my $nfo     = shift;

    #$res{$name}{content} = $content;
    #$res{$name}{link}    = $link;
    #$res{$name}{logo}    = $logo;
    #$res{$name}{path}    = $path;
    #$res{$name}{src}     = $file;
    #$res{$name}{type}    = $type;

    #print "NAME $name\n";

    foreach my $filter ( sort keys %$filters )
    {
        #print "  FILTER $filter\n";
        
        if ( exists ${$nfo}{$filter} && ${$nfo}{$filter} )
        {
            my $cols       = $filters->{$filter}{cols};
            my $filterSize = scalar keys %{$cols};
            my $data       = $nfo->{$filter};

            #print "FILTER $filter\n";
            #print "DATA '$data'\n";
            #use Data::Dumper;
            #print Dumper($data);
            #print Dumper($nfo);
            #exit 0;
            
            foreach my $key ( sort keys %$data )
            {

                #print "    KEY $key\n";
                my $hash    = $data->{$key};
                my $type    = exists $hash->{type}    ? $hash->{type}    : &ndef( "$name => $filter :: $key :: type not defined"    );
                #print "      TYPE $type\n";
                my $logo    = exists $hash->{logo}    ? $hash->{logo}    : &ndef( "$name => $filter :: $key :: logo not defined"    );
                
                #print "      LOGO $logo\n";

                if ( ( $type eq "image" ) || ( $type eq "text" ) )
                {
                    my $path    = exists $hash->{path}    ? $hash->{path}    : &ndef( "$name => $filter :: $key :: path not defined"    );
                    my $file    = exists $hash->{src}     ? $hash->{src}     : &ndef( "$name => $filter :: $key :: src not defined"     );
                    my $content = exists $hash->{content} ? $hash->{content} : &ndef( "$name => $filter :: $key :: content not defined" );
                    my $link    = exists $hash->{link}    ? $hash->{link}    : &ndef( "$name => $filter :: $key :: link not defined"    );
                    &makeLogo(type => $type, key => $key, logo => $logo, space=> $space, name => "$name > $key", link => $link, content => $content);
                }
                elsif ( $type eq "link" )
                {
                    my $link    = exists $hash->{link}    ? $hash->{link}    : &ndef( "$name => $filter :: $key :: link not defined"    );
                    &makeLogo(type => $type, key => $key, logo => $logo, space=> $space, name => "$name > $key", link => $link);
                }
                elsif ( $type eq "empty" )
                {
                    &makeLogo(type => $type, key => $key, logo => $logo, space=> $space, name => "$name > $key" );
                } else {
                    print "unknown type: $type\n";
                    exit 1;
                }
            }
            #print "" . ($s x $space . "<td></td>\n") x ($filterSize - (scalar keys %$data));
        } else {
            #print "      NO DATA\n";
            my $names= $filters->{$filter}{cols};
            foreach my $name ( sort keys %$names )
            {
                my $titleS = lc($name); $titleS =~ s/ //g;
                print "" . ( $s x $space ). "<td class=\"icons\" colname=\"$titleS\"></td>\n";
            }
        }
    }
}





sub makeLogo       {
    my %data  = @_;
    my $type  = $data{type};
    my $logo  = $data{logo};
    my $space = $data{space};
    my $alt   = $data{name};
    my $key   = $data{key};
    my $txt   = '';
    
    my $icon  = "<img class='icon' src=\"$logo\" alt=\"$alt\"/>\n";
    if ( exists $data{link} )
    {
        my $lnk = $data{link};
        my $a = "";
        
        if ( exists $data{content} )
        {
            my $txtC  = $data{content}{text};
            my $imgC  = $data{content}{image};

            my $iw    = $imgWidth;
            my $ih    = $imgHeight;
            
            if ( exists ${$data{content}}{size} )
            {
                my $sizeC = $data{content}{size};
    
                if ( defined $sizeC )
                {
                    if ( scalar @$sizeC == 2 )
                    {
                        $iw = $sizeC->[0];
                        $ih = $sizeC->[1];
                    }
                }
            }
            
            
            if ( defined $txtC && $imgC )
            {
                #print "TEXT C 1 $txtC\n";
                #print "IMG  C 1 $imgC\n";
                #$txtC    = $txtC;

                $imgC    = "<img class=\"imgdsp\" width=$iw height=$ih src=\"$getter?src=$imgC\"/>";
                $txtC    = $imgC . $txtC;
            }
            elsif ( defined $txtC )
            {
                #print "TEXT C 2 $txtC\n";
                #$txtC    = $txtC;
            }
            elsif ( defined $imgC )
            {
                #print "IMG  C 2 $imgC\n";
                $imgC    = "<img class=\"imgdsp\" width=$iw height=$ih src=\"$getter?src=$imgC\"/>";
                $txtC    = $imgC;
            }
            $a       = &getA($txtC);
        }

        $txt  = ($s x ($space+1)) . "<a href=\"$getter?src=$lnk\" target=\"_blank\" alt=\"$alt\" $a>\n";
        $txt .= ($s x ($space+2)) . $icon;
        $txt .= ($s x ($space+1)) . "</a>\n";
    } else {
        #$txt  = ($s x ($space+1)) . $icon;
    }
    

    #my @lines = <FI>;
    #if (scalar @lines > 30 ) {
    #    &splitLines(\@lines);
    #}
    #$webPath = "\n<pre>\n". join("<br/>\n", @lines) . "</pre>\n";
    #close FI;
    
    my $titleS = lc($key); $titleS =~ s/ //g;
    print "" . ($s x $space ) . "<td class=\"icons\" colname=\"$titleS\">\n";
    print $txt;
    print "" . ($s x $space ) . "</td>\n";
}

sub ndef           {
    my $s = shift;
    print $s;
    exit 1;
}





sub getA           {
    my $content = shift;
    #print "CONTENT B $content\n";
    $content = encode_entities($content);
    $content =~ s/(?:\n\r|\n|\r)/\\n/g;
    #print "CONTENT A $content\n\n";
    my $A = "onmouseover=\"ModifyContent('graph', '$content'); ShowContent('graph'); return true;\"  onmouseout=\"HideContent('graph'); return true;\"";
   #href="javascript:ShowContent('graph">

    return $A;
}







sub loadSetup      {
    my $file = shift;
    my %hash;
    if ( -f "$file" )
    {
        print "LOCAL DB '$file' EXISTS\n" if $verbose;
        
        my $dump = new XML::Dumper;    

        my $ref = \$dump->xml2pl( $file );
        
        if ( $ref ) {
    #        print Dumper($ref);
            #print $$ref;
    #        print Dumper($ds);
            %hash = %{${$ref}};
    #        print Dumper($ds);
            return \%hash;
        } else {
            return \%hash;
        }
    } else {
        print "LOCAL DB '$file' DOES NOT EXISTS\n";
        exit 1;
        return \%hash;
    }
}




sub getCss         {
    my $css = <<CSS
        <link type="text/css" href="$HTMLBASE/js/jquery/css/vader/jquery-ui-1.8.17.custom.css" rel="stylesheet"/>   

        <!-- Framework CSS -->  
        <link rel="stylesheet" type="text/css" media="screen, projection" href="$HTMLBASE/css/blueprint/screen.css"/>  
        <!--    <link rel="stylesheet" type="text/css" media="print"              href="$HTMLBASE/css/blueprint/print.css"/> -->

        <!--[if IE]>
        <link rel="stylesheet" type="text/css" media="screen, projection" href="$HTMLBASE/css/blueprint/ie.css"/>
        <![endif]-->  
  
        <!-- Import fancy-type plugin. -->  
        <!-- <link rel="stylesheet" type="text/css" media="screen, projection" href="$HTMLBASE/css/blueprint/plugins/fancy-type/screen.css"/> -->

        <link rel="stylesheet" type="text/css" href="$HTMLBASE/css/style.css" /> 
CSS
;
    return $css;
}




sub getJs          {

    my $dbgToggle = $debug ? "true" : "false";
    
    my @jss;
        #<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>-->
        #<!-- <script type="text/javascript" src="$HTMLBASE/js/jquery-1.7.1.min.js"></script> -->
        #<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    
        #my $js01 = "<script type='text/javascript' language='JavaScript' src='$HTMLBASE/js/toggle.js'      ></script>\n'; push(@jss, $js01);



    my $js02 = "<script type='text/javascript' language='JavaScript' src='$HTMLBASE/js/svgweb/src/svg.js' data-path='$HTMLBASE/js/svgweb/src/'></script>\n"; push(@jss, $js02);



    my $js03 = <<JQRY
        <script type="text/javascript" src="$HTMLBASE/js/jquery/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="$HTMLBASE/js/jquery/js/jquery-ui-1.8.17.custom.min.js"></script>
JQRY
; push(@jss, $js03);



    my $js04 = "<script type='text/javascript' language='JavaScript' src='$HTMLBASE/js/modify.js'          ></script>\n"; push(@jss, $js04);
    my $js05 = "<script type='text/javascript' language='JavaScript' src='$HTMLBASE/js/position.js'        ></script>\n"; push(@jss, $js05);
    my $js06 = "<script type='text/javascript' language='JavaScript' src='$HTMLBASE/js/mouse.js'           ></script>\n"; push(@jss, $js06);
    my $js07 = "<script type='text/javascript' language='JavaScript' src='$HTMLBASE/js/highlight.js'       ></script>\n"; push(@jss, $js07);
    my $js08 = "<script type='text/javascript' language='JavaScript' src='$HTMLBASE/js/fixedmenu.js'       ></script>\n"; push(@jss, $js08);
    #my $js09 = "<script type='text/javascript' language='JavaScript' src='$HTMLBASE/js/stickyTable.js' ></script>\n"; push(@jss, $js09);
    #my $js10 = "<script type='text/javascript' language='JavaScript' src='$HTMLBASE/js/floating-1.7.js'    ></script>\n"; push(@jss, $js10);

        #<script type='text/javascript' language='JavaScript' src='$HTMLBASE/js/jquery/jquery-1.7.1.min.js'></script>
        #<script type='text/javascript' language='JavaScript' src='$HTMLBASE/js/jquery/jquery.ui.core.js'></script>
        #<script type='text/javascript' language='JavaScript' src='$HTMLBASE/js/jquery/jquery.ui.widget.js'></script>
        #<script type='text/javascript' language='JavaScript' src='$HTMLBASE/js/jquery/jquery.ui.tabs.js'></script>

        #\$(function(){
        #    \$("#results").stickyTableHeaders();
        #});
    



    my $js11 = <<JS
    <script type="text/javascript" language="JavaScript">
        floatingMenu.add('graph',  
        {  
            // Represents distance from left or right browser window  
            // border depending upon property used. Only one should be  
            // specified.  
            // targetLeft: 0,  
            targetRight: 10,  
  
            // Represents distance from top or bottom browser window  
            // border depending upon property used. Only one should be  
            // specified.  
            targetTop: 10,  
            // targetBottom: 0,  
  
            // Uncomment one of those if you need centering on  
            // X- or Y- axis.  
            // centerX: true,  
            // centerY: true,  
  
            // Remove this one if you don't want snap effect  
            snap: false
        });  
    </script>
JS
; #push(@jss, $js11);




    my $js12 = <<JS
        <script type="text/javascript" language="JavaScript">
            \$(function() {
                \$( "#tabs" ).tabs();
            });
        </script>
JS
; push(@jss, $js12);




    my $js00 = <<JS
        <script type="text/javascript" language="JavaScript">
            fixedMenuId = 'graph';
        </script>
JS
;

    unshift(@jss, $js00);

    my $div = "
    <div id='graph' style='  
        position:absolute;  
        top:10px;left:10px;  
        padding:16px;background:#FFFFFF;  
        border:2px solid #2266AA;  
        z-index:100'>
    </div>\n"; unshift(@jss, $div);

    return join("", @jss);
}


sub loadPerms {
    my $un = shift;
    my $st = shift;
    #my %access = (
    #ab    => '.+',
    #pig   => 'Pig',
    #three => 'Pennellii|AllRound|F5',
    #heinz => 'Heinz'

    if ( exists ${$st}{$un} ) {
        return $st->{$un};
    } else {
        return;
    }
}

sub getFixed {
    my $path  = shift;
    my $spp   = shift;
    my $fPath = "$path/$spp";
    my $fix;
    my $size;

    my $fver = 0;
    print "PATH $path SPP $spp FPATH $fPath\n" if $fver;
    if ( -d $fPath )
    {
        print "  PATH $fPath EXISTS\n" if $fver;
        if (opendir(my $dh, $fPath)) 
        {
            foreach my $file ( sort readdir($dh) ) 
            {
                print "    FIXED: CHECKING FILE $file\n" if $fver;
                next if $file =~ /^\./;
                print "      NAME NOT DOT : $file\n" if $fver;
                next if $file !~ /\.zip$/;
                print "      NAME ZIP     : $file\n" if $fver;
                next if $file !~ /^$spp/;
                print "      NAME SPECIES : $file\n" if $fver;
                next if ! -f "$fPath/$file";
                print "      NAME FILE    : $file\n" if $fver;
                my $name = $file;
                $name =~ s/_\d{4}_\d{2}_\d{2}_\d{2}_\d{2}_\d{2}\.zip$//;
                           #2012_02_   13_   16_   10_   53
                print "      NAME SMALL   : $name\n" if $fver;
                $fix  =    $file;
                $size = -s "$fPath/$file";
            }
            closedir $dh;
        } else {
            return ($fix, $size);
        }
    } else {
        return ($fix, $size);
    }

    if ( $size ) {
        $size = sprintf("%0.1f", $size / 1024 /1024);
    }
    #print "FIX $fix\n";
    return ($fix, $size);
}


1;


