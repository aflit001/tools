function toggle_visibility(id, content) {
    var e = document.getElementById(id);
    if(e.style.display == 'block') {
        e.style.display = 'none';
    } else {
        e.innerHTML     = content;
        e.style.display = 'block';
    }
        
}
