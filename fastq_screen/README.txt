Fastq Screen - Contmaination Screening for large data sets
==========================================================

Introduction
------------

Fastq screen is a simple application which allows you to 
search a large sequence dataset against a panel of different
databases to build up a picture of where the sequences 
in your data originate. It was built as a QC check for
sequencing pipelines but may also have uses in metagenomics
studies where mixed samples are expected.

Although the program wasn't built with any particular
technology in mind it is probably only really suitable for
processing short reads due to the use of bowtie as the 
searching application.

The program generates both text and graphical output to
tell you what proportion of your library was able to map, 
either uniquely or in more than one location, against each
of the databases in your search set.

Installation
------------

Before installing fastq screen there are a few prerequisites
which will need to be installed.  These are:

1) Bowtie - this can be downloaded from bowtie-bio.sourceforge.net
   It's easier if you can put the bowtie binary in your path, but
   if not you can configure its location in the config file.

2) Perl - will already be present on OSX or Linux. If you're on 
   windows you can get this from www.activestate.com/activeperl

3) GD::Graph Fastq screen uses the GD::Graph module to draw the
   results graph so this will need to be installed on your 
   system.

   Windows ActivePerl users can install this using;

   ppm install GD-Graph

   Other platforms can use the built in CPAN shell to install
   this:

   perl -MCPAN -e "install GD"

   Because GD graph uses GD this will be brought in as a 
   dependecy.  GD may be easier to install using a pacakge
   manager on many linux distributions.  On Fedora for example
   you can install GD using:

   yum install perl-GD

   ..before doing the CPAN install of GD::Graph

Actually installing Fastq Screen is very simple. Download the
tar.gz distribution file and then do:

  tar xzf fastq_screen_v0.1.tar.gz

You will see a folder called FastQScreen be created and the
program is inside that.  You can add the program to your 
path either by linking the program into /usr/local/bin or 
by adding the program installation directory to your search
path.


Configuration
-------------

In order to use fastq screen you will need to configure some
databases for the program to search.  This will involve 
downloading the sequences for the databases in FastA format
and then using the bowtie-build program which comes with
bowtie to build an index for the sequences.

Once you have built your indices you can configure the
fastq_screen program.  You do this by editing the 
fastq_screen.conf file which is distributed with the program.
This shows an example set of database configurations which 
you will need to change to reflect the actual databases
you have set up.

The other options you can set in the config file are 
the location of the bowtie binary (if it's not in your path),
and the number of threads you want to allocate to bowtie when 
performing your screen.  The number of threads will be the number 
of CPU cores the code will run on so you shouldn't set this value
higher than the number of physical cores you have in your
machine. The more threads you can allow the faster the searching
part of the screen will run.


Running the program
-------------------

Full documentation for the options for actually running the
fastq_screen program can be found by running:

fastq_screen --help

An example command is shown below.  This would process two
fastq files and would create the screen output in the same
directory as the original files.  The files in this case
would be single end fastq files with Illumina v1.3 quality
scores. Only 500,000 sequence out of the original file will
be searched.

fastq_screen --subset 500000 --illumina fastq1.txt fastq2.txt


Problems?
---------

If you have any problems running this program you can either
open them as bugs in our bug tracking system at:

www.bioinformatics.bbsrc.ac.uk/bugzilla/

Or you can email then to:

simon.andrews@bbsrc.ac.uk
