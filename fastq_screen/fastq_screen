#!/usr/bin/perl
use warnings;
use strict;
use GD;
use GD::Graph::bars;
use Getopt::Long;
use FindBin qw($RealBin);

our $VERSION = "O.2.1";

###########################################################################
###########################################################################
##                                                                       ##
## Copyright 2011, Simon Andrews  (simon.andrews@bbsrc.ac.uk)            ##
##                 Mark Fiers     (Plant & Food Research, NZ)            ##
##                 Steven Wingett (steven.wingett@bbsrc.ac.uk)           ##
##                                                                       ##
## This program is free software: you can redistribute it and/or modify  ##
## it under the terms of the GNU General Public License as published by  ##
## the Free Software Foundation, either version 3 of the License, or     ##
## (at your option) any later version.                                   ##
##                                                                       ##
## This program is distributed in the hope that it will be useful,       ##
## but WITHOUT ANY WARRANTY; without even the implied warranty of        ##
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         ##
## GNU General Public License for more details.                          ##
##                                                                       ##
## You should have received a copy of the GNU General Public License     ##
## along with this program.  If not, see <http://www.gnu.org/licenses/>. ##
###########################################################################
###########################################################################


## Option variables
my $subset_count;
my $outdir;
my $illumina;
my $quiet;
my $help;
my $version;
my $paired;
my $threads;
my $color;
my $conf;
my $bowtie_opts;
my $multi_library;


my $config_result = GetOptions("subset=i" => \$subset_count,
			       "outdir=s" => \$outdir,
			       "illumina" => \$illumina,
			       "quiet" => \$quiet,
			       "help" => \$help,
			       "version" => \$version,
			       "paired" => \$paired,
			       "conf=s" => \$conf,
			       "color" => \$color,
			       "bowtie" => \$bowtie_opts,
			       "threads=i" => \$threads,
			       "multilib" => \$multi_library,
			      );

die "Could not parse options" unless ($config_result);

if ($help) {
  print while (<DATA>);
  exit;
}

if ($version) {
  print "fastq_screen v$VERSION\n";
  exit;
}

if ($color) {
  $color = '-C';
}
else {
  $color = '';
}

$bowtie_opts = '' unless ($bowtie_opts); # Get undef warning otherwise


# Configuration
my $number_of_threads = 1;
my $path_to_bowtie = 'bowtie';
my @libraries;

load_configuration($conf);

# Override the configuration default if they've
# manually specified a number of threads to use
$number_of_threads = $threads if ($threads);

unless (@libraries) {
  die "No search libraries were configured";
}



my @files = @ARGV;

die "No files to process" unless (@files);

my $index = 0;
while ($index <= $#files) {
  if ($paired) {
    process_file($files[$index],$files[$index+1]);
    $index+=2;
  }
  else {
    process_file($files[$index]);
    $index++;
  }
}

sub process_file {

  my ($file,$file2) = @_;

    #Variables required should multilib option be selected
    my %seq_genomes; # Stores the hits to each genome
    my $readsprocessed = 0; # Stores the total number of sequences searched


  if ($file2) {
    warn "Processing $file and $file2\n" unless ($quiet);
  }
  else {
    warn "Processing $file\n" unless ($quiet);
  }

  # Check that we can actually find the files we're working with
  unless (-e $file) {
    warn "Couldn't locate file $file - skipping\n";
    return;
  }
  if ($file2) {
    unless (-e $file2) {
      warn "Couldn't locate file $file2 - skipping\n";
      return;
    }
  }

  # Work out the output file name we're going to use
  my $outfile = $file;
  $outfile =~ s/\.(txt|seq|fastq)$//i;

  # We can try to remove the end specific part of the name
  # if they're using standard Illumina naming. It doesn't
  # really matter if this fails
  if ($paired) {
    $outfile =~ s/s_(\d)_1_sequence/s_${1}_sequence/;
  }

  $outfile .= "_screen.txt";

  if ($outdir) {
    $outfile = (split(/\//,$outfile))[-1];
    $outfile = $outdir."/".$outfile;
  }

  if (-e $outfile) {
    warn "Output file $outfile already exists - skipping\n";
    return;
  }

  open (OUT,'>',$outfile) or do {
    warn "Couldn't write to $outfile: $!";
    return;
  };

  #Print the headers to the output file
  if ($multi_library){
    print OUT join ("\t",('Library','Unmapped','Mapped_One_library', 'Mapped_Multiple_Libraries')),"\n"; 	
  }
  else{
    print OUT join("\t",('Library','Unmapped','Mapped','Multi_mapped')),"\n";
  }

  my $temp_file;
  my $temp_file2;

  my $read_length = get_read_length($file);

  if ($read_length < 0) {
    warn "Failed to calculate read length from $file";
    return;
  }

  if ($read_length < 20) {
    warn "Ignoring reads shorter than 20bp\n";
    $read_length = 20;
  }

  # We don't use a seed of >40 even if the reads are that long
  $read_length = 40 if ($read_length > 40);


  # See if we need to take only a subset of this file.  If we're doing
  # multi_lib then we need to do this anyway since we need to count the
  # sequences in the file.

  if ($subset_count or $multi_library) {
    # We need to make a subset of these sequences

    # First we need to count how many sequences are in the original file
    my $seqcount = 0;

    warn "Counting sequences in $file\n" unless ($quiet);

    open (IN,$file) or do {
      warn "Can't read $file: $!";
      return;
    };


    ++$seqcount while (<IN>);

    $seqcount = int($seqcount/4);

    if ($subset_count and $seqcount > $subset_count*2) {
      # We actually need to do the reduction

      $temp_file = $file;
      $temp_file .= "_temp_subset.txt";

      # Since we're writing we need to do it in
      # the directory we know we're allowed to write
      # to.

      if ($outdir) {
	$temp_file = (split(/\//,$temp_file))[-1];
	$temp_file = $outdir."/".$temp_file;
      }


      open (TEMP,'>',$temp_file) or do {
	warn "Can't write temp subset file: $!";
	return;
      };

      my $interval = sprintf("%.0f",($seqcount/$subset_count));

      warn "Making reduced seq file with ratio $interval:1\n" unless ($quiet);

      seek(IN,0,0);

      my $current_count = 0;
      while (<IN>) {
	if (/^@/) {
	  my $record = $_;
	  $record .= scalar <IN>;
	  $record .= scalar <IN>;
	  $record .= scalar <IN>;

	  if ($current_count % $interval == 0) {
	    print TEMP $record;
	    ++$readsprocessed;
	  }
	  ++$current_count;
	}
      }

      close (TEMP) or do {
	warn "Can't write temp subset file: $!";
	return;
      };

      $file = $temp_file;

      if ($file2) {
	# We need to do the reduction in file 2 as well
	
	$temp_file2 = $file2;
	$temp_file2 .= "_temp_subset.txt";

	# Since we're writing we need to do it in
	# the directory we know we're allowed to write
	# to.

	if ($outdir) {
	  $temp_file2 = (split(/\//,$temp_file2))[-1];
	  $temp_file2 = $outdir."/".$temp_file2;
	}


	open (TEMP,'>',$temp_file2) or do {
	  warn "Can't write temp2 subset file: $!";
	  return;
	};


	warn "Making reduced seq file 2 with ratio $interval:1\n" unless ($quiet);

	open (IN,$file2) or do {
	  warn "Failed to read $file2: $!";
	  unlink ($temp_file) or warn "Couldn't delete temp file $temp_file: $!";
	  return;
	};

	my $current_count = 0;
	while (<IN>) {
	  if (/^@/) {
	    my $record = $_;
	    $record .= scalar <IN>;
	    $record .= scalar <IN>;
	    $record .= scalar <IN>;

	    print TEMP $record if ($current_count % $interval ==0);
	    ++$current_count;
	  }
	}

	close (TEMP) or do {
	  warn "Can't write temp2 subset file: $!";
	  return;
	};

	$file2 = $temp_file2;

      }

    }

    else {
      $readsprocessed = $seqcount;
      if ($subset_count) {
	warn "Not making subset of $subset_count since $seqcount actual seqs is too low or close enough" unless ($quiet);
      }
    }

  }


  my $library_index = -1; # Used in multi-lib to make lists in the same order as @libraries

  foreach my $library (@libraries) {

    warn "Searching $file against $library->[0]\n" unless ($quiet);

    my $illumina_flag = '';
    $illumina_flag = "--solexa1.3-quals" if ($illumina);

    my $bowtie_command;

    if ($multi_library){

      #Count the index of the library being used
      $library_index++;

      if ($file2) {
	$bowtie_command = "$path_to_bowtie $bowtie_opts $illumina_flag $color -l $read_length -k 1 --maxins 1000 --chunkmbs 512 -p $number_of_threads $library->[1] -1 $file -2 $file2 2>&1 |";
      }
      else {
	$bowtie_command = "$path_to_bowtie $bowtie_opts $illumina_flag $color -l $read_length -k 1 --chunkmbs 512  -p $number_of_threads $library->[1] $file 2>&1 |";
      }

      open (BOWTIE,$bowtie_command) or die "Failed to launch bowtie command '$bowtie_command': $!";

      while (<BOWTIE>) {


	#Split the sequences into an array
	chomp;
	my ($seqname,$is_alignment) =  split(/\t/);

	unless (defined $is_alignment) {
	  # This is a progress message coming from stderr
	  unless (/^\#/ or /^Reported/ or /^No alignments/) {
	    warn $_;
	  };

	  next;
	}


	# Initialise an array of zero values the same length as @libraries
	unless (exists $seq_genomes{$seqname}){
	  for my $position(0..$#libraries){
	    $seq_genomes{$seqname}->[$position] = 0;
	  }
	}

	# Set the value for this library to 1 since we have a hit
	$seq_genomes{$seqname}->[$library_index] = 1;
      }
    }

    else { # Not using multi-lib option

      if ($file2) {
	$bowtie_command = "$path_to_bowtie $bowtie_opts $illumina_flag $color -l $read_length -m 1 --maxins 1000 --chunkmbs 512 -p $number_of_threads $library->[1] -1 $file -2 $file2 /dev/null 2>&1 |";
      }
      else {
	$bowtie_command = "$path_to_bowtie $bowtie_opts $illumina_flag $color -l $read_length -m 1 --chunkmbs 512  -p $number_of_threads $library->[1] $file /dev/null 2>&1 |";
      }

      open (BOWTIE,$bowtie_command) or die "Failed to launch bowtie command '$bowtie_command': $!";

      my $mapped = 0;
      my $unmapped = 0;
      my $multi_mapped = 0;

      while (<BOWTIE>) {

	# Don't show expected messages from bowtie, but
	# warn about anything else
	unless (/^\#/ or /^Reported/ or /^No alignments/) {
	  warn $_;
	};

	if (/\# reads with at least one reported alignment:\s+\d+\s+\(([\d\.]+)/) {
	  $mapped = $1;
	}

	if (/\# reads that failed to align:\s+\d+\s+\(([\d\.]+)/) {
	  $unmapped = $1;
	}

	if (/\# reads with alignments suppressed due to -m:\s+\d+\s+\(([\d\.]+)/) {
	  $multi_mapped = $1;
	}

      }

      print OUT join("\t",($library->[0],$unmapped,$mapped,$multi_mapped)),"\n";
    }
  }

  if($multi_library){
    # We need to collate the hit results we put together
    # in the bowtie searches - for the non multi-lib these
    # were written out to the output file as we went along.

    my @one_library;
    my @multiple_libraries;

    foreach my $seq_id(keys %seq_genomes){
      my $numlib = 0;
	
      # Count up the number of libraries where this sequence
      # had a hit

      foreach (@{$seq_genomes{$seq_id}}) {
	$numlib++ if ($_);
      }


      # Is this a single or a multi-hit?
      if($numlib > 1){
	foreach my $index (0..$#{$seq_genomes{$seq_id}}){
	  $multiple_libraries[$index]+= $seq_genomes{$seq_id}->[$index];
	}
      }
      else{
	foreach my $index (0..$#{$seq_genomes{$seq_id}}){
	  $one_library[$index]+= $seq_genomes{$seq_id}->[$index];
	}
      }
    }

    # If we're doing a paired end search then all our counts will
    # be double what they should be since we'll get hits for both
    # of the sequences in the pair with different ids (bowtie will
    # make them different even if they weren't originally).  We
    # therefore need to halve the counts before calculating percentages.
    # The easiest way to do this is to double the expected total
    # count

    $readsprocessed *= 2 if ($file2);
	
    # Summarise the single/multiple counts and write the text report

    foreach my $index (0..$#libraries){
      my $library = $libraries[$index];
      my $percent_unmapped = 100 * (($readsprocessed - $one_library[$index]) - $multiple_libraries[$index])/($readsprocessed);
      $percent_unmapped = sprintf("%.2f",$percent_unmapped);
      my $percent_mapped_one_library = ($one_library[$index] / $readsprocessed) * 100;
      $percent_mapped_one_library = sprintf("%.2f", $percent_mapped_one_library);
      my $percent_mapped_multiple_libraries = ($multiple_libraries[$index]/$readsprocessed) * 100;
      $percent_mapped_multiple_libraries = sprintf("%.2f", $percent_mapped_multiple_libraries);
      print OUT join("\t",($library->[0],$percent_unmapped,$percent_mapped_one_library,$percent_mapped_multiple_libraries)),"\n";
    }
  }

  close OUT or die "Coudn't write to $outfile: $!";

  unlink($temp_file) or warn "Unable to delete temp file '$temp_file'" if ($temp_file);

  unlink($temp_file2) or warn "Unable to delete temp file '$temp_file'" if ($temp_file2);

  make_graph($outfile);

}

sub load_configuration {

  # Find the config file

  my ($conf_file) = @_;

  # If they haven't specified a conf file then look
  # in the directory containing the program.
  $conf_file = "$RealBin/fastq_screen.conf" unless ($conf_file);

  unless (-e $conf_file) {
    die "Couldn't find fastq_screen.conf at '$conf_file'";
  }

  warn "Reading configuration from '$conf_file'\n" unless ($quiet);

  open (CONF,$conf_file) or die "Can't read $conf_file : $!";

  while (<CONF>) {
    chomp;
    s/^\s+//;
    s/\s+$//;

    next if (/^\#/);
    next unless ($_);

    my ($name) = split(/\s+/);

    if ($name eq 'BOWTIE') {
      $path_to_bowtie = (split(/\s+/,$_,2))[1];

      die "Empty value set for BOWTIE config parameter" unless ($path_to_bowtie);

      warn "Using '$path_to_bowtie' as bowtie path\n" unless ($quiet);
    }


    elsif ($name eq 'THREADS') {
      $number_of_threads = (split(/\s+/))[1];
      unless ($number_of_threads =~ /^\d+$/) {
	die "Invalid number of threads '$number_of_threads set in conf file";
      }
      warn "Using $number_of_threads threads for searches\n" unless ($quiet);
    }


    elsif ($name eq 'DATABASE') {
      my (undef,$db_name,$db_path) = split(/\s+/,$_,3);

      # Check to see that there's a bowtie index at that location
      unless (-e "$db_path.1.ebwt") {
	warn "Skipping DATABASE '$db_name' since no bowtie index was found at '$db_path'\n";
	next;
      }

      warn "Adding database $db_name\n" unless ($quiet);
      push @libraries, [$db_name,$db_path];

    }

  }
  close CONF;

}


sub get_read_length {

  my ($file) = @_;

  open (IN,$file) or do {
    warn "Failed to read $file: $!";
    return 0;
  };

  my $shortest_length = -1;

  my $current_count = 0;
  while (<IN>) {
    if (/^@/) {
      my $seq = scalar <IN>;
      chomp $seq;
      if ($shortest_length < 0 or length $seq < $shortest_length) {
	$shortest_length = length $seq;
      }

      # Skip the rest of the record
      $_ = scalar <IN>;
      $_ = scalar <IN>;

      ++$current_count;
      last if ($current_count > 1000);
    }
  }

  return $shortest_length;

}

sub make_graph {

  my ($file) = @_;

  my $outfile = $file;
  $outfile =~ s/\.txt$//;

  open (IN,$file) or die "Can't open data file '$file' to create graph";

  my @data;

  $_ = <IN>; # Header;

  while (<IN>) {
    chomp;
    my ($name,$unmapped,$mapped,$multi) = split(/\t/);

    push @{$data[0]},$name;
    push @{$data[1]},$mapped;
    push @{$data[2]},$multi;

  }

  my $imgSizePerSpp = 150;
  my $imgWidth      = $imgSizePerSpp * @{$data[0]};
  my $imgHeight     = 0.6211180124223602 * $imgWidth; # golden

  my $graph         = GD::Graph::bars->new($imgWidth,$imgHeight);

  $graph -> set (
		 y_label      => '% Mapped (single/multi)',
		 title        => (split(/\//,$outfile))[-1],
		 y_max_value  => 100,
		 bar_spacing  => 15,
		 shadow_depth => 2,
		 transparent  => 0,
		 overwrite    => 1,
		 cumulate     => 1,
		 dclrs        => [qw(dblue dgreen)]
		);


  $graph->set_legend(qw(mapped multi));
  $graph->set_title_font(gdGiantFont);
  $graph->set_legend_font(gdGiantFont);
  $graph->set_x_label_font(gdGiantFont);
  $graph->set_x_axis_font(gdMediumBoldFont);
  $graph->set_y_axis_font(gdMediumBoldFont);

  my $gd = $graph -> plot (\@data);

  $outfile .= ".png";

  open (IMG,'>',$outfile) or die "Can't write graph to $outfile: $!";
  binmode IMG;
  print IMG $gd->png();
  close IMG or die "Can't write graph to $outfile: $!";

}


__DATA__

Fastq Screen - Screen sequences against a panel of databases

Synopsis

  fastq_screen [OPTION]... [FastQ FILE]...

Function

  Fastq Screen is intended to be used as part of a QC pipeline.
  It allows you to take a sequence dataset and search it
  against a set of bowtie databases.  It will then generate
  both a text and a graphical summary of the results to see if
  the sequence dataset contains the kind of sequences you expect
  or not.

Options

  --help -h      Print program help and exit

  --subset       Don't use the whole sequence file to search, but
                 create a temporary dataset of this size. The
                 dataset created will be of approximately (within
                 a factor of 2) of this size. If the real dataset
                 is smaller than twice the specified size then the
                 whole dataset will be used. Subsets will be taken
                 evenly from throughout the whole original dataset

  --paired       Files are paired end. Files must be specified in
                 the correct order with pairs of files coming
                 immediately after one another. Results files will
                 be named after the first file in the pair if the
                 names differ between the two files.

  --outdir       Specify a directory in which to save output files.
                 If no directory is specified then output files
                 are saved into the same directory as the input
                 file.

  --illumina     Assume that the quality values are in encoded in
                 Illumina v1.3+ format. Defaults to Sanger format
                 if this flag is not specified

  --quiet        Supress all progress reports on stderr and only
                 report errors

  --version      Print the program version and exit

  --threads      Specify across how many threads bowtie will be
                 allowed to run. Overrides the default value set in
                 the conf file

  --conf         Manually specify a location for the configuration
                 file to be used for this run. If not specified then
                 the file will be taken from the same directory as
                 the fastq_screen program

  --color        FastQ files are in colorspace. This requires that the
                 libraries configures in the config file are colorspace
                 indices.

  --bowtie       Specify extra parameters to be passed to bowtie. These
                 parameters should be quoted to clearly delimit bowtie
                 parameters from fastq_screen parameters. You should not
                 try to use this option to override the normal search or
                 reporting options for bowtie which are set automatically
                 but it might be useful to allow reads to be trimmed before
                 alignment etc.

  --multilib     Records the pattern of hits for every sequence searched
                 and produces a graph which is split into hits specific to
                 the current library, and hits which hit the current library
                 and at least one other library. This is useful to see if
                 you are generating low complexity sequences which map to
                 many species, or if you have sequences which map uniquely
                 to a species you weren't expecting.
