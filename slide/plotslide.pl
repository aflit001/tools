#!/usr/bin/perl -w
use strict;
use File::Basename;
use Storable;

#TODO:
#	CHOOSE BETWEEN Q / PVALUE / PERCENTIL

#########################################################
## SETTINGS
#########################################################
my $verbose      =     0; # print every line
my $printQTable  =     0; # print quality conversion table and leave
my $exportTab    =     1; # export dat file or not
my $sizeX        =   640; # size of image in pixels
my $sizeY        =  1978;
my $percentil    =    .3; # lower pencentile to show on the graphic
my $maxPThr      =    50; # max N proportion threshold. shows in graphic all spots until this proportion of N
my $maxPGrp      =   100; # group anything above with this value
my $readEvery    =     0; # read every Nth read ( 0 to all )
my $max          =    -1; # max number of reads to process ( -1 to all )
#  1_038_865  1%
# 10_388_652 10%
#103_886_521

my $redoReadFile =     0;
my $redoGrouping =     0;
my $redoMatrix   =     0;
my $doPlot       =     1;

my $area         = $sizeX * $sizeY;
my $areaProp     = $sizeY / $sizeX;

#my $pThreshold = 0.018; # error level(0.018 = 98.2% confidence Q = 17)
#my $maxQThr    = 17; # max quality threshold. shows in graphic all spots until this quality
#my $maxQGrp    = 20; # group everything above threshold here



print  "SETUP\n";
print  "    QUALITY\n";
printf "        PERCENTILE   %15.3f\n", $percentil;
printf "        MAX ERR REP  %15d\n",  $maxPThr;
printf "        HI QUAL GRP  %15d\n",  $maxPGrp;
print  "    IMAGE\n";
printf "    AREA             %15d\n",  $area;
printf "    SIZE X           %15d\n",  $sizeX;
printf "    SIZE Y           %15d\n",  $sizeY;






#INPUT FILE READ [103 886 521]
#    READS                  103886521
#    READS/SPOT                    82 (reads/area)
#    READS/SPOT X                  33 (max reads X / sizeX )
#    READS/SPOT Y                 101 (max reads Y / sizeY )
#    NUM BASES:           10388652100
#
#    X        :: Min            1000 Max           21336 DIFF           20336
#    Y        :: Min            1743 Max          200815 DIFF          199072



&printQTable() if $printQTable;
#total 103_886_521
#########################################################
## CHECK FILES
#########################################################
my $infile     = $ARGV[0];
die "NO INPUT FILE DEFINED"      if ! defined $infile;
die "INPUT FILE DOES NOT EXISTS" if !      -f $infile;
my $basename = basename($infile);
my $oufile   = "$basename.dat";
print "OUTFILE  $oufile\n";

unlink("$oufile.plot");
unlink("$oufile.png");
unlink("$oufile.jpg");
unlink("$oufile.gif");
unlink("$oufile.ascii");
#unlink("$oufile.emf");
#unlink("$oufile.svg");
#unlink("$oufile.canvas");
my $fileSize      = -s $infile;



if ( $redoReadFile || $redoGrouping || $redoMatrix ) {
	unlink("$oufile");
	unlink("$oufile.data3.info.dump");
	if ( $redoReadFile || $redoGrouping ) {
		unlink("$oufile.data2.info.dump");
		unlink("$oufile.data2.data.dump");
		if ( $redoReadFile ) {
			unlink("$oufile.data1.info.dump");
			unlink("$oufile.data1.data.dump");	
		}
	}
}


print  "READING INPUT FILE\n";
printf "    FILESIZE         %15d\n", $fileSize;





#########################################################
## READ INPUT FILE
#########################################################
my $data;
my $qualHisto;

my $len    = undef;   # read length
my $reads  = 0;       # number of reads
my $maxX   = 0;       # max X coordinate
my $maxY   = 0;       # max Y coordinate
my $minX   = 10**100; # min X coordinate
my $minY   = 10**100; # min Y coordinate
my $minQ   = 10000;   # min sum of ascii found
my $maxQ   = 0;       # max sum of ascii found
my $minP   = 10000;   # min P (proportion of N) found
my $maxP   = 0;       # max P (proportion of N) found
my $foundX = 0;
my $foundY = 0;



if ( ! -f "$oufile.data1.data.dump" ) {
	open FI, "<$infile"  or die "COULD NOT OPEN INPUT FILE $infile: $!";
	my $register;
	my $seq;
	my $name2;
	my $qual;
	my $skipped = $readEvery ? 1 : 0;
	my %foundX;
	my %foundY;
	
	$| = 1;
	while ( $register = <FI> ) {
		if ( $readEvery && ( $skipped++ <= ($readEvery*4))) { next;         };
		if ( $readEvery && ( $skipped   >= ($readEvery*4))) { $skipped = 1; };
		
		chomp $register;
		$seq   = <FI>; chomp $seq;
		$name2 = <FI>; chomp $name2;
		$qual  = <FI>; chomp $qual;

			#HWI-ST132_0429:2:1:1570:1747#TAAGNT/1
			#NATGAAATGTATTTGTATTTACGTCNGAAATTTGNNNNNNNNNNNTGAAGCACNNNTNNNNANNNNNNNNNNNNNNNNNNNNCNNTNNNNNNNANNNNNN
			#+HWI-ST132_0429:2:1:1570:1747#TAAGNT/1
			#BXX[X[YYY[^^^^^_____BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
			#@
	
		if ( $register =~ /(\d+):(\d+)\#/ ) {
			my $x = $1;
			my $y = $2;

			$foundX{$x}++;
			$foundY{$y}++;
			$reads++;
		
			#print "SKIPPED $skipped... ";
			print "\n\n\n[$reads] REGISTER $register\nSEQ $seq\nNAME $name2\nQUAL $qual\n" if $verbose;
			printf "%15d", $reads if ( $reads % ((  1_000_000 / ( $readEvery || 1 ) ) ||  10 ) == 0 );
			print  "\n"           if ( $reads % (( 10_000_000 / ( $readEvery || 1 ) ) || 100 ) == 0 );
	
			#print "\tX $x Y $y S $seq Q $qual\n";
	
			if ( ! defined $len ) {
				$len = length $seq; # get sequence length from the first sequence
			}
	
			my $qual2 = $qual;
			my $n     = $seq   =~ tr/N//; # count number of N
			my $n2    = $qual2 =~ tr/B//; # count number of N
			my $p     = (((($n > $n2) ? $n : $n2)/ $len)*100); # get Proportion
			my $Q     = 0;
			
				#map { my $ql = 10 * (log(1 + 10 ** (ord($_) - 64) / 10.0)) / log(10); print "\t\tQL $_ => $ql\n";  $Q += $ql; } split(//, $qual);
				#$map { my $ql = ord($_) - 66; $Q += $ql; } split(//, $qual);
				
			#for (my $p = 0; $p < $len; $p++) {
			#	my $char = substr($qual, $p, 1); # get char at position P
			#	my $ql   = ord($char); # get its ascii number
				#print " ",substr($qual, $p, 1), " => ", ord(substr($qual, $p, 1)), " => ", $ql, " \n";
			#	$Q += $ql; # add
			#	$qualHisto{$char}++;
			#}
			
			
			map { $Q += ord($_); $qualHisto->{$_}++ } split(//, $qual);
			
			
				#print "\t\tQ: $Q\n";
			my $qq = int( $Q / ( $len - 1 ) ); # get average ascii number
	
			$maxX  = $x  if $x  > $maxX;
			$maxY  = $y  if $y  > $maxY;
			$minX  = $x  if $x  < $minX;
			$minY  = $y  if $y  < $minY;
	
			$minQ  = $qq if $qq < $minQ;
			$maxQ  = $qq if $qq > $maxQ;
	
			$minP  = $p  if $p  < $minP;
			$maxP  = $p  if $p  > $maxP;
	
				#printf "X %06d Y %06d FX %06d FY %06d N %2d P %3d%% [%3d] Q %3d S %s\n", $x, $y, $fx, $fy, $n, $p, ,$len, $qq, $seq if ($verbose);
			printf "    X %06d Y %06d [%3d] Q %3d P %3d S %s\n", $x, $y, $len, $qq, $p, $seq if ($verbose);
	
			#$data{$x}{$y} = {'qq' => $qq, 'p' => $p}; hhh
			#$data{$x}{$y} = [$qq, $p]; #hha
			#$data->[$x]{$y} = [$qq, $p]; #aha
			#$data[$x]{$y} = {'qq' => $qq, 'p' => $p}; #ahh
			#$data[$x][$y] = [$qq, $p]; #aaa
			$data->{"$x.$y"} = [$qq, $p]; #aha
			#$dataX->[$x]      = $dataY; #aha
		} else {
			print "\n\n\n";
			print "READS    : $reads\n";
			print "REGISTER : $register\n";
			print "SEQUENCE : $seq\n";
			print "NAME 2   : $name2\n";
			print "QUALITY  : $qual\n";
			die "error parsing data";
		}
		last if ( $max != -1 && $reads >= $max);
	}
	print "\n";
	close FI;
	$| = 0;

	$foundX   = scalar keys %foundX;
	$foundY   = scalar keys %foundY;


	print "\tDUMPING INTERMEDIATE FILE\n";
	my %dump = (
		'qualHisto' => \$qualHisto,
		'len'       => $len,
		'reads'     => $reads,
		'maxX'      => $maxX,
		'maxY'      => $maxY,
		'minX'      => $minX,
		'minY'      => $minY,
		'minQ'      => $minQ,
		'maxQ'      => $maxQ,
		'minP'      => $minP,
		'maxP'      => $maxP,
		'foundX'    => $foundX,
		'foundY'    => $foundY,
	);
	store \%dump, "$oufile.data1.info.dump";
	store  $data, "$oufile.data1.data.dump";
	%dump = ();
} else {
	print "\tREADING FROM DUMP\n";
	if ( ! -f "$oufile.data2.data.dump" ) {
		print "\t\tLOADING DATA DUMP\n";
		$data  = retrieve("$oufile.data1.data.dump");
		print "\t\tLOADING DATA DUMP COMPLETED\n";
	}

	my $dump = retrieve("$oufile.data1.info.dump");
	print "\t\tLOADING VARIABLES\n";	
	$qualHisto = ${$dump->{'qualHisto'}};
	$len       = $dump->{'len'};
	$reads     = $dump->{'reads'};
	$maxX      = $dump->{'maxX'};
	$maxY      = $dump->{'maxY'};
	$minX      = $dump->{'minX'};
	$minY      = $dump->{'minY'};
	$minQ      = $dump->{'minQ'};
	$maxQ      = $dump->{'maxQ'};
	$minP      = $dump->{'minP'};
	$maxP      = $dump->{'maxP'};
	$foundX    = $dump->{'foundX'};
	$foundY    = $dump->{'foundY'};
	print "\t\tLOADING VARIABLES COMPLETED\n";
	$dump = undef;
	print "\tREADING FROM DUMP COMPLETED\n";	
}
die "NO READS" if ! $reads;

print  "INPUT FILE READ\n";
print  "    READS : $reads\n";
print  "    DATA  : ", scalar keys %$data, "\n" if ( ! -f "$oufile.data2.data.dump" );
print  "    HISTO : ", scalar keys %$qualHisto, "\n";
printf "    FOUND X %15d Y %15d\n\n", $foundX, $foundY;



#########################################################
## PRINT INPUT FILE STATS
#########################################################
my $diffX         = ( $maxX - $minX );
my $diffY         = ( $maxY - $minY );
my $diffQ         = ( $maxQ - $minQ );
my $diffP         = ( $maxP - $minP );


#TODO: MAY BE USE DIFFX INSTEAD OF FOUNDX
my $readsPerSpot  = int( $reads  / $area  ) ? (int( $reads  / $area  )+1) : 1; # general packing factor
my $readsPerSpotX = int( $foundX / $sizeX ) ? (int( $foundX / $sizeX )+1) : 1; # packing factor on X
my $readsPerSpotY = int( $foundY / $sizeY ) ? (int( $foundY / $sizeY )+1) : 1; # packing factor on Y


my @characters    = sort {$a cmp $b} keys %$qualHisto; 
my $charactersMin = $characters[0];  #smaller ascii character found
my $charactersMax = $characters[-1]; #biggest ascii character found
my $basesCount    = 0;               # total number of bases
map { $basesCount += $qualHisto->{$_} } @characters;



printf "    READS            %15d\n"                             , $reads;
printf "    READS/SPOT       %15d (reads/area)\n"                , $readsPerSpot;
printf "    READS/SPOT X     %15d (max found reads X [%15d]/ sizeX [%15d]) {%15d}\n", $readsPerSpotX, $foundX, $sizeX, (int($foundX / $readsPerSpotX));
printf "    READS/SPOT Y     %15d (max found reads Y [%15d]/ sizeY [%15d]) {%15d}\n", $readsPerSpotY, $foundY, $sizeY, (int($foundY / $readsPerSpotY));
printf "    NUM BASES:       %15d\n\n"                           , $basesCount;

printf "    X        :: Min %15d Max %15d DIFF %15d\n", $minX, $maxX, $diffX;
printf "    Y        :: Min %15d Max %15d DIFF %15d\n", $minY, $maxY, $diffY;
printf "    mean Q   :: Min %15d Max %15d DIFF %15d\n", $minQ, $maxQ, $diffQ;
printf "    mean P   :: Min %15d Max %15d DIFF %15d\n", $minP, $maxP, $diffP;
printf "    Base MIN :: %s [ %3d => %12d ] MAX %s [ %3d => %12d ]\n\n",
$charactersMin, ord($charactersMin), $qualHisto->{$charactersMin},
$charactersMax, ord($charactersMax), $qualHisto->{$charactersMax};





#########################################################
## FIND WHICH ILLUMINA VERSION IS BEING RUN
#########################################################
my $class       = "UNKNOWN";
my $subtraction = 0;
my $minOrd      = ord($charactersMin);
if ( $minOrd < 64 ) {
	if ( $minOrd < 59 ) {
		$class       = "SANGER";
		$subtraction = 33;
	} else {
		$class = "ILL 1.0";
		$subtraction = 64;
	}
} else {
	if ( $minOrd < 66 ) {
		$class = "ILL 1.3";
		$subtraction = 64;
	} else {
		$class = "ILL 1.5";
		$subtraction = 64;
	}
}

die "UNKOWN ILLUMINA QUALITY SYSTEM" if ( $class eq "UNKNOWN");

printf "    CLASS %s\n", $class;
printf "    mean CQ  :: Min %15d ['%s'] Max %15d ['%s'] DIFF %15d\n\n", # corrected quality
($minQ - $subtraction), chr($minQ), ($maxQ - $subtraction), chr($maxQ), $diffQ;
	#ASCII      33   34   35   36   37   38   39   40   41   42   43   44   45   46   47   48   49   50   51 
	#ASCII       !    "    #    $    %    &    '    (    )    *    +    ,    -    .    /    0    1    2    3 
	#SANGER      0    1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17   18 
	#SANGER P 1.00 0.79 0.63 0.50 0.40 0.32 0.25 0.20 0.16 0.13 0.10 0.08 0.06 0.05 0.04 0.03 0.03 0.02 0.02 
	#Q1.0                                                                                                    
	#Q1.3                                                                                                    
	#Q1.5                                                                                                    
	
	#ASCII      52   53   54   55   56   57   58   59   60   61   62   63   64   65   66   67   68   69   70 
	#ASCII       4    5    6    7    8    9    :    ;    <    =    >    ?    @    A    B    C    D    E    F 
	#SANGER     19   20   21   22   23   24   25   26   27   28   29   30   31   32   33   34   35   36   37 
	#SANGER P 0.01 0.01 0.01 0.01 0.01 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 
	#Q1.0                                          -5   -4   -3   -2   -1    0    1    2    3    4    5    6 
	#Q1.3                                                                    0    1    2    3    4    5    6 
	#Q1.5                                                                    0    1    2    3    4    5    6 





#########################################################
## PRINT QUALITY TABLE
#########################################################
print "\n    ";
my @cumQ;
my $percentilQ;
my $prevCcv = 0;

for ( my $c = 0; $c < @characters; $c++) {
	my $cv  = $characters[$c];
	my $ccv = ord($cv) - $subtraction;
	my $cc  = $qualHisto->{$cv};
	my $per = (int(($cc/$basesCount)*100000) / 1000);
	$cumQ[$ccv] = ( $c ? $cumQ[$prevCcv] : 0 ) + $cc;
	my $cum  = $cumQ[$ccv];
	my $pCum = ( $c ? $cumQ[$prevCcv] : 0 );
	if (($pCum / $basesCount <= $percentil ) && ( $cum / $basesCount >= $percentil )) { $percentilQ = $ccv };
	printf "'%s' [%3d:%3d] = %12d {%12d} (%8.4f%%)  |  ", $cv, $ccv, ord($cv), $cc, $cum, $per;
	print "\n    " if ($c+1) % 3 == 0;
	$prevCcv = $ccv;
}
print "\n\n";









#########################################################
## CALCULATE THRESHOLD
#########################################################
#my $qTable     = &getQfromP($pThreshold);
#my $maxQThr    = $qTable->{$class};      # max quality threshold. shows in graphic all spots until this quality
#my $maxQGrp    = $maxQThr+2;             # group everything above threshold here
#while ( $maxQGrp % 5 != 0 ) { $maxQGrp++ };

my $maxPWhite  = 110; # group anything above with this value

my $maxQThr    = $percentilQ;
my $thrPTable  = &getPTable($percentilQ);
my $maxEThr    = $thrPTable->{$class};

my $maxQGrp    = $maxQThr+2;             # group everything above threshold here
while (( $maxQGrp % 5 ) != 0 ) { $maxQGrp++    };
if    (  $maxQGrp > 40       ) { $maxQGrp = 40 };
my $maxQWhite  = $maxQGrp + 5;

my $grpPTable  = &getPTable($maxQGrp);
my $maxEGrp    = $grpPTable->{$class};

my $sumTilQ    = $cumQ[$maxQThr];
my $totalQ     = $cumQ[-1];
my $sumAfterQ  = ($totalQ - $sumTilQ);

printf "    PERCENTILE       %15.5f\n",         $percentil;
printf "    PERCENTILE Q     %15d\n",           $percentilQ;
print  "    Q THRESHOLD\n";
printf "        MAX VALUE    %15d\n",           $maxQThr;
printf "        ALFA         %15.5f\n",         ( 1 - $maxEThr );
printf "        ERR          %15.5f\n",         $maxEThr;
printf "        TOTAL        %15d (%5.3f%%)\n", $sumTilQ, (int(($sumTilQ / $totalQ) * 10000)/100);
print  "    Q ROUPING\n";
printf "        VALUE        %15d\n",           $maxQGrp;
printf "        WHITE        %15d\n",           $maxQWhite;
printf "        ALFA VALUE   %15.5f\n",         (1-$maxEGrp);
printf "        ERR  VALUE   %15.5f\n",         $maxEGrp;
printf "        TOTAL        %15d (%5.3f%%)\n", $sumAfterQ, (int(($sumAfterQ / $totalQ) * 10000)/100);






#########################################################
## GROUPING SPOTS AND FIXING Q
#########################################################
print "GROUPING SPOTS\n";
my $nData;
my $nDataY;
my $maxFX    = 0;
my $maxFY    = 0;
my $minFX    = 10**100;
my $minFY    = 10**100;
my $foundFX  = {};
my $foundFY  = {};

my $d        = 0;
my $th       = 1;
my $rCountTh = int( $maxX / 100 ) + 1;

if ( ! -f "$oufile.data2.data.dump" ) {
	$| = 1;
	print "    ";
	for (my $x = $minX; $x <= $maxX; $x++ ) {
		$d++;
		#next if ! defined ${$dataX}[$x];
		
		printf "[%12d/%12d]  ",$x,$maxX  if ( $d %  $rCountTh      == 0 );
		print  "\n    "                  if ( $d % ($rCountTh * 5) == 0 );
		
		#my $ys      = $dataX->[$x];
	
		#foreach my $yt ( keys %$ys ) {
		for (my $y = $minY; $y <= $maxY; $y++) {
			#next if $yt !~ /^$x\.(\d+)$/;
			my $yt = "$x.$y";
			next if ! exists $data->{$yt};
			
			
			#my $fx   = int( $x / $readsPerSpotX );
			#my $fy   = int( $y / $readsPerSpotY );
			my $fx   = int( $x / $readsPerSpotX );
			my $fy   = int( $y / $readsPerSpotY );
	
			$foundFX->{$fx}++;
			$foundFY->{$fy}++;	
	
			$maxFX   = $fx if $fx > $maxFX;
			$maxFY   = $fy if $fy > $maxFY;
			$minFX   = $fx if $fx < $minFX;
			$minFY   = $fy if $fy < $minFY;

			my $lArr = $data->{$yt};
			my $Q    = $lArr->[0];
			my $P    = $lArr->[1];
			die "NO Q DEFINED FOR X $x AND Y $y [", scalar @$lArr, " : " ,join(",", @$lArr),"]\n" if ! defined $Q;
			die "NO P DEFINED FOR X $x AND Y $y\n" if ! defined $P;
			my $QQ = ( $Q - $subtraction );
	
			printf "    X %06d Y %06d FX %06d FY %06d Q %06d QQ %06d P %06d\n", $x, $y, $fx, $fy, $Q, $QQ, $P if ($verbose);
	
			$nDataY->{"$fx.$fy"}[0] += $QQ;
			$nDataY->{"$fx.$fy"}[1] += $P;
			$nDataY->{"$fx.$fy"}[2]++;
			
			$nData->[$fx] = $nDataY;
		}
	}
	print "\n";
	$| = 0;

	
	print "\tDUMPING INTERMEDIATE FILE\n";
	my %dump = (
		'maxFX'   => $maxFX,
		'maxFY'   => $maxFY,
		'minFX'   => $minFX,
		'minFY'   => $minFY,
		'foundFX' => $foundFX,
		'foundFY' => $foundFY,
	);
	my %dataDump = ('nData' => $nData, 'nDataY' => $nDataY);
	
	store \%dump,     "$oufile.data2.info.dump";
	store \%dataDump, "$oufile.data2.data.dump";
	%dump = ();
} else {
	print "\tREADING FROM DUMP\n";
	
	if ( ! -f "$oufile" ) {
		print "\t\tLOADING DATA DUMP\n";
		my $dataDump = retrieve("$oufile.data2.data.dump");
		$nData  = $dataDump->{nData};
		$nDataY = $dataDump->{nDataY};
		print "\t\tLOADING DATA DUMP COMPLETED\n";
	}

	print "\t\tLOADING VARIABLES\n";	
	my $dump   = retrieve("$oufile.data2.info.dump");
	$maxFX     = $dump->{'maxFX'};
	$maxFY     = $dump->{'maxFY'};
	$minFX     = $dump->{'minFX'};
	$minFY     = $dump->{'minFY'};
	$foundFX   = $dump->{'foundFX'};
	$foundFY   = $dump->{'foundFY'};



	print "\t\tLOADING VARIABLES COMPLETED\n";
	$dump      = undef;
	print "\tREADING FROM DUMP COMPLETED\n";
}

$data = undef;
print "SPOTS GROUPED\n";

my $diffFX = ($maxFX - $minFX );
my $diffFY = ($maxFY - $minFY );
printf "    FX MIN %15d MAX %15d DIFF %15d\n", $minFX, $maxFX, $diffFX;
printf "    FY MIN %15d MAX %15d DIFF %15d\n", $minFY, $maxFY, $diffFY;
printf "    FOUND FX %15d FY %15d\n", scalar keys %$foundFX, scalar keys %$foundFY;

die "NO FX/FY FOUND" if ( ! scalar keys %$foundFX || ! scalar keys %$foundFY );
die "ERROR GETTING GROUPED TABLE x" if ! -f $oufile && ! scalar      @$nData;
die "ERROR GETTING GROUPED TABLE y" if ! -f $oufile && ! scalar keys %$nDataY;


my %fx2cx  = ();
my $fx2cxC = 0;
my %fy2cy  = ();
my $fy2cyC = 0;
my $sideXQ;
my $sideXP;
my $sideYQ;
my $sideYP;

map { $fx2cx{$_} = ++$fx2cxC } sort {$a <=> $b} keys %$foundFX;
map { $fy2cy{$_} = ++$fy2cyC } sort {$a <=> $b} keys %$foundFY;


my $showGaps = 0;
#########################################################
## EXPORTING MATRIX TO PLOT
#########################################################
print "EXPORTING MATRIX\n\t";
my $avgQMin   = 100000;
my $avgQMax   = 0;
my $avgPMin   = 100000;
my $avgPMax   = 0;
my $countX    = 0;

if ( ! -f $oufile ) {
	my $rCountThF = int( $maxFX / 100 ) + 1;
	open FO, ">$oufile"  or die "COULD NOT OPEN OUTPUT FILE $oufile: $!";
	for ( my $fx = $minFX; $fx <= $maxFX; $fx++ ) {
		printf "[%7d/%7d] ",$fx,$maxFX if ( $fx %  $rCountThF      == 0 );
		print  "\n\t"                  if ( $fx % ($rCountThF * 5) == 0 );

		die if ! exists $fx2cx{$fx};
		my $fxS = $fx2cx{$fx};
	
		if ( ! defined ${$nData}[$fx] ) {
			next;
			#next if ! $showGaps;
			# IF ROW DOESN'T EXISTS, FILL WITH DEFAULT (GROUPING NUMBER)

			$sideXQ->[$fxS][0]++;
			$sideXQ->[$fxS][1] += $maxQWhite;
			$sideXP->[$fxS][0]++;
			$sideXP->[$fxS][1] += $maxPWhite;

			for ( my $fy = $minFY; $fy <= $maxFY; $fy++ ) {
				my $fyS = $fy2cy{$fy};

				$sideYQ->[$fyS][0]++;
				$sideYQ->[$fyS][1] += $maxQWhite;
				$sideYP->[$fyS][0]++;
				$sideYP->[$fyS][1] += $maxPWhite;

				print FO $fxS,"\t",$fyS,"\t",$maxQWhite,"\t",$maxPWhite,"\n"; # print the post-threshold grouping number
				#$maxQGrp "x( $diffFY - 1 ), "$maxQGrp\n"; 
				$avgQMax  = $maxQWhite if $maxQWhite > $avgQMax;
				$avgPMax  = $maxPWhite if $maxPWhite > $avgPMax;
			}
			print FO " \n";
			next;
		}
		
		$countX++;
		my $ys    = $nData->[$fx];
		
		for ( my $fy = $minFY; $fy <= $maxFY; $fy++ ) {

			if ( ! exists ${$ys}{"$fx.$fy"} ) {
				next if ! exists $fx2cx{$fx};
				next if ! exists $fy2cy{$fy};

				die "ERROR GETTING CORRELATION FOR $fx" if ! exists $fx2cx{$fx};
				die "ERROR GETTING CORRELATION FOR $fy" if ! exists $fy2cy{$fy};

				my $fyS = $fy2cy{$fy};

				$sideXQ->[$fxS][0]++;
				$sideXQ->[$fxS][1] += $maxQWhite;
				$sideXP->[$fxS][0]++;
				$sideXP->[$fxS][1] += $maxPWhite;

				$sideYQ->[$fyS][0]++;
				$sideYQ->[$fyS][1] += $maxQWhite;
				$sideYP->[$fyS][0]++;
				$sideYP->[$fyS][1] += $maxPWhite;
				
				#next if ! $showGaps;
				# IF COLUMN DOESN'T EXISTS, FILL WITH DEFAULT
				print FO $fxS,"\t",$fyS,"\t",$maxQWhite,"\t",$maxPWhite,"\n";
				
				$avgQMax  = $maxQWhite if $maxQWhite > $avgQMax;
				$avgPMax  = $maxPWhite if $maxPWhite > $avgPMax;
				
				#print a new line if last value in row
				#if ( $fy == ( $maxFY )) { print FO "\n" }
				next;
			}

			die if ! exists $fy2cy{$fy};
			my $fyS  = $fy2cy{$fy};

			my $p    = $ys->{"$fx.$fy"};
	
			my $sumQ = $p->[0];
			my $sumP = $p->[1];
			my $num  = $p->[2];
			#printf "X %06d Y %06d SUMQ %06d SUMP %06d NUM %06d\n", $fx, $fy, $sumQ, $sumP, $num;
			die "NO SUM FOUND" if ( ! defined $sumQ || ! defined $sumP || ! $num );
			
			my $avgQ = int($sumQ/(($num - 1) || 1)); # get average Q FOR PIXEL
			my $avgP = int($sumP/(($num - 1) || 1)); # get average P FOR PIXEL

			$avgQMin = $avgQ if $avgQ < $avgQMin;
			$avgQMax = $avgQ if $avgQ > $avgQMax;
			$avgPMin = $avgP if $avgP < $avgPMin;
			$avgPMax = $avgP if $avgP > $avgPMax;
			
			printf "FX %06d FY %06d SUMQ %06d SUMP %06d NUM %06d AVGQ %06d AVGP %06d\n", $fx, $fy, $sumQ, $sumP, $num, $avgQ, $avgP if ($verbose);
			die if $avgQ == 0;
			die if $avgP  < 0;
			
			#print FO $fx,"\t",$fy,"\t", ( $avgQ > $maxQThr ? $maxQGrp : $avgQ ), "\t",( $avgP > $maxPThr ? $maxPGrp : $avgP ),"\n";
			#print FO $countX,"\t",$fy,"\t", ( $avgQ > $maxQThr ? $maxQGrp : $avgQ ), "\t",( $avgP > $maxPThr ? $maxPGrp : $avgP ),"\n";
			print FO $fxS, "\t", $fyS, "\t", ( $avgQ > $maxQThr ? $maxQGrp : $avgQ ), "\t",( $avgP > $maxPThr ? $maxPGrp : $avgP ),"\n";

			$sideXQ->[$fxS][0]++;
			$sideXQ->[$fxS][1] += ( $avgQ > $maxQThr ? $maxQGrp : $avgQ );
			$sideXP->[$fxS][0]++;
			$sideXP->[$fxS][1] += ( $avgP > $maxPThr ? $maxPGrp : $avgP );

			$sideYQ->[$fyS][0]++;
			$sideYQ->[$fyS][1] += ( $avgQ > $maxQThr ? $maxQGrp : $avgQ );
			$sideYP->[$fyS][0]++;
			$sideYP->[$fyS][1] += ( $avgP > $maxPThr ? $maxPGrp : $avgP );

		}
		print FO "\n";
	}
	close FO;
	print "\n";

	open FOX, ">$oufile.x.dat"  or die "COULD NOT OPEN OUTPUT FILE $oufile.y.dat: $!";
	for (my $x = 1; $x < @$sideXQ; $x++) {
		my $couXQ = $sideXQ->[$x][0];
		my $sumXQ = $sideXQ->[$x][1];

		my $couXP = $sideXP->[$x][0];
		my $sumXP = $sideXP->[$x][1];

		my $avgXQ = int( $sumXQ / ($couXQ - 1) );
		my $avgXP = int( $sumXP / ($couXP - 1) );

		print FOX $x, "\t", $avgXQ, "\t", $avgXP, "\n";
	}
	close FOX;

	open FOY, ">$oufile.y.dat"  or die "COULD NOT OPEN OUTPUT FILE $oufile.x.dat: $!";
	for (my $y = 1; $y < @$sideYQ; $y++) {
		my $couYQ = $sideYQ->[$y][0];
		my $sumYQ = $sideYQ->[$y][1];

		my $couYP = $sideYP->[$y][0];
		my $sumYP = $sideYP->[$y][1];

		my $avgYQ = int( $sumYQ / ($couYQ - 1) );
		my $avgYP = int( $sumYP / ($couYP - 1) );

		print FOY $y, "\t", $avgYQ, "\t", $avgYP, "\n";
	}
	close FOY;
	
	print "\tDUMPING INTERMEDIATE FILE\n";
	my %dump = (
		'avgQMin' => $avgQMin,
		'avgQMax' => $avgQMax,
		'avgPMin' => $avgPMin,
		'avgPMax' => $avgPMax,
		'sideXQ'  => $sideXQ,
		'sideXP'  => $sideXP,
		'sideYQ'  => $sideYQ,
		'sideYP'  => $sideYP
	);
	store \%dump, "$oufile.data3.info.dump";
	%dump = ();	
} else {
	print "\tREADING FROM DUMP\n";
	print "\t\tLOADING VARIABLES\n";

	my $dump   = retrieve("$oufile.data3.info.dump");
	$avgQMin   = $dump->{'avgQMin'};
	$avgQMax   = $dump->{'avgQMax'};
	$avgPMin   = $dump->{'avgPMin'};
	$avgPMax   = $dump->{'avgPMax'};
	$sideXQ    = $dump->{'sideXQ'};
	$sideXP    = $dump->{'sideXP'};
	$sideYQ    = $dump->{'sideYQ'};
	$sideYP    = $dump->{'sideYP'};
	print "\tLOADING VARIABLES COMPLETED\n";
	$dump = undef;
	print "\tREADING FROM DUMP COMPLETED\n";	
}
$nData  = undef;
$nDataY = undef;

die "ERROR EXPORTING MATRIX" if ! -s $oufile;

print "MATRIX EXPORTED\n";

#$avgQMax = 20;
my $diffAvgQDiff = ( $avgQMax - $avgQMin );
my $diffAvgPDiff = ( $avgPMax - $avgPMin );
printf "    Q AVG MIN %15d MAX %15d DIFF %15d\n", $avgQMin, $avgQMax, $diffAvgQDiff;
printf "    P AVG MIN %15d MAX %15d DIFF %15d\n", $avgPMin, $avgPMax, $diffAvgPDiff;

my $maxQThr1 = $maxQThr + 1; # defined boundary to force change of color on z axis





#########################################################
## GENERATING PLOT SCRIPT
#########################################################
	print "EXPORTING PLOT\n";
	# IF MINIMUM FOUND IS GREATER THAN THRESHOLD, PUT HALF THRESHOLD AS MINIMUM LIMIT
	my $zQMin = ( $avgQMin >= $maxQThr ) ? ( $maxQThr / 2.0 ) : $avgQMin;

	# GET MIDDLE POINT BETWEEN MINIMUM AND AVERAGE FOR Z-SCALE COLORING
	my $zQMid = int( ( $avgQMin + $maxQThr ) / 2 );
	
	my $zPMin = ( $avgPMin >= $maxPThr ) ? ( $maxPThr / 2.0 ) : $avgPMin;
	my $zPMid = int( ( $avgQMin + $maxQThr ) / 2 );

	# GET PLOT TITLE
	my $plotTitle  = $basename;
	   $plotTitle  =~ s/_/\\_/g;
	   $plotTitle  =~ s/\.fastq//g;
	   
	my $maxError = sprintf("%.5f", $maxEThr);
	my $maxTotal = sprintf("%.3f", (int(($sumTilQ / $totalQ) * 10000)/100));
	
	my $maxQGrp1 = $maxQGrp+1;
	my $sizeX2   = 2 * $sizeX;
	print "ZQMIN $zQMin ZQMID $zQMid ZQTHR $maxQThr ZQGRP $maxQGrp ZQWHITE $avgQMax ZPMIN $zPMin ZPMID $zPMid ZPTHR $maxPThr ZPGRP $maxPGrp\n";
	
	my $plot = <<PLOT
unset key
set xtics out
set ytics out
set ztics out

set lmargin   0 # at screen 0
set bmargin   0 # at screen 0
set rmargin   0 # at screen 0
set tmargin   0 # at screen 0


#set origin    -0.05, -0.070 # 1024 x 768
#set size       1.07,  1.225 # 
#set origin   -0.10, -0.09 # 3000 x 1000
#set size      1.16,  1.48


set border    0
set noborder


set view map
set terminal pngcairo crop truecolor enhanced size $sizeY,$sizeX2
set output "$oufile.png"

set yrange  [1:$fx2cxC]
set xrange  [1:$fy2cyC]

set multiplot title 'Illumina QC test\\n[ $reads Reads | Area $area | $readsPerSpot reads/spot ]'

#####################
## Q PLOT
#####################
set title '$plotTitle [ $class ] :: Percentil $percentil (Q $percentilQ) :: Threshold Q $maxQThr (e $maxError) :: Total $sumTilQ ($maxTotal%)' offset 0,-8
set cblabel "Quality Q"

set zrange  [$zQMin:$maxQWhite]
set cbrange [$zQMin:$maxQWhite]

set palette defined (  $zQMin 0 0 0, $zQMid 0 .5 0, $maxQThr 0 1 0, $maxQThr1 1 1 1, $maxQGrp 1 1 1, $maxQGrp1 1 1 1, $maxQWhite 1 1 1 )
#                      black         darkGreen      green           white            white

set size   0.95,0.48
set origin 0.01,0.60


splot "$oufile" using 2:1:3 with image



#####################
## P PLOT
#####################
set title '$plotTitle Threshold P $maxPThr'
set cblabel "% N"

set zrange  [0:$maxPWhite]
set cbrange [0:$maxPWhite]

set size   0.95,0.48
set origin 0.01,0.22

#set palette defined (  0 0 0 0, 50 0 .5 0, 75 0 1 0, 80 1 1 1, 81 1 1 1, 90 1 1 1, 100 1 1 1 )
#set palette defined (  0 1 1 1, 80 0 0 0, 81 0 1 0, 90 0 0.5 0, 100 0 0 0 )
set palette defined (  0 1 1 1, 10 1 1 1, 11 0 1 0, 110 0 0 0 )

splot "$oufile" using 2:1:4 with image





#####################
## HORIZONTAL PLOTS
#####################
# Q
unset title
unset xtics
set xrange  [1:$fy2cyC]
set yrange  [1:$maxQWhite]
set zrange  [*:*]
set cbrange [*:*]
set ytics   1,15,45
set size    0.95,0.10
set origin  0.01,0.53
splot "$oufile.y.dat" using 1:2:'0' with lines lc rgb 'red'

# P
set xrange  [1:$fy2cyC]
set yrange  [1:$maxPWhite]
set zrange  [*:*]
set cbrange [*:*]
set ytics   1,40,110
set size    0.95,0.10
set origin  0.01,0.15
splot "$oufile.y.dat" using 1:3:'0' with lines lc rgb 'red'


unset ytics
set xtics
unset ytics



#####################
## VERTICAL PLOTS
#####################
# Q
set xrange  [1:$maxQWhite]
set yrange  [1:$fx2cxC]
set zrange  [*:*]
set cbrange [*:*]
set xtics   1,15,45
set size    0.10,0.35
set origin  0.03,0.60
splot "$oufile.x.dat" using 2:1:'0' with lines lc rgb 'green'

#P
set xrange  [1:$maxPWhite]
set yrange  [1:$fx2cxC]
set zrange  [*:*]
set cbrange [*:*]
set xtics   1,40,110
set size    0.10,0.35
set origin  0.03,0.22
splot "$oufile.x.dat" using 3:1:'0' with lines lc rgb 'green'





##set terminal svg enhanced  size 1978,640 dynamic
##set output "110126_SN132_B_s_2_1_seq_GOG-17.fastq.dat.svg"
##replot

#set terminal jpeg large crop size 1978,640
#set output "110126_SN132_B_s_2_1_seq_GOG-17.fastq.dat.jpg"
#replot

#set terminal gif giant enhanced optimize crop size 1978,640
#set output "110126_SN132_B_s_2_1_seq_GOG-17.fastq.dat.gif"
#replot

#set terminal emf enhanced size 1978,640
#set output "110126_SN132_B_s_2_1_seq_GOG-17.fastq.dat.emf"
#replot

#set terminal dumb enhanced size 1978,640
#set output "110126_SN132_B_s_2_1_seq_GOG-17.fastq.dat.ascii"
#replot

##set terminal canvas enhanced size 1978,640
##set output "110126_SN132_B_s_2_1_seq_GOG-17.fastq.dat.canvas"
##replot

quit()

PLOT
;















;






#########################################################
## EXPORTING PLOT SCRIPT
#########################################################
if ( -f $oufile ) {
	open FP, ">$oufile.plot"  or die "COULD NOT OPEN OUTPUT PLOT FILE $oufile.plot: $!";
	print FP $plot;
	close FP;
	print "EXPORTING PLOT COMPLETE\n";
	
	if ( -f "$oufile.plot" && $doPlot ) {
		print "GENERATING IMAGE\n";
		print `gnuplot $oufile.plot 2>&1`;

		if ( -f "$oufile.png" ) {
			print "\n\tIMAGE GENERATED\n";
		} else {
			die "\n\tERROR GENERATING IMAGE\n";
		}
		
		print "GENERATING IMAGE COMPLETE\n";
	}
} else {
	die "NO MATRIX FOUND\n";
}

print "FINISHED\n";
exit 0;







sub getPTable {
	my $query = shift;
	my %Q;
	for ( my $p = 0; $p <= 0.8; $p += 0.00001 ) {
		my $lp         = $p ? log($p)        : $p;
		my $lpmp       = $p ? log($p/(1-$p)) : $p;
		my $q14        = -10 * ($lpmp/log(10));
		my $Qphred     = -10 * ($lp  /log(10));
		last if ( int($q14) == -6 && int($Qphred) ==  0 );
		next if ( int($q14) >  40 && int($Qphred) >  40 );
		$Q{14}{int($q14)}{min}    = $p if ( ! exists $Q{14} || ! exists ${$Q{14}}{int($q14)}    || ! exists ${$Q{14}{int($q14)}}{min}    || $p < $Q{14}{int($q14)}{min}    );
		$Q{14}{int($q14)}{max}    = $p if ( ! exists $Q{14} || ! exists ${$Q{14}}{int($q14)}    || ! exists ${$Q{14}{int($q14)}}{max}    || $p > $Q{14}{int($q14)}{max}    );
		$Q{ph}{int($Qphred)}{min} = $p if ( ! exists $Q{ph} || ! exists ${$Q{ph}}{int($Qphred)} || ! exists ${$Q{ph}{int($Qphred)}}{min} || $p < $Q{ph}{int($Qphred)}{min} );
		$Q{ph}{int($Qphred)}{max} = $p if ( ! exists $Q{ph} || ! exists ${$Q{ph}}{int($Qphred)} || ! exists ${$Q{ph}{int($Qphred)}}{max} || $p > $Q{ph}{int($Qphred)}{max} );
		#printf "%3f => %3d => %3d\n", $p, int($q14), int($Qphred);
	}
	
	my %pKeys;
	map { $pKeys{$_}++ } keys %{$Q{14}};
	map { $pKeys{$_}++ } keys %{$Q{ph}};
	
	my %res;
	foreach my $Q ( sort { $a <=> $b } keys %pKeys ) {
		
		my $avg14 = ((($Q{14}{$Q}{min} || 0) + ($Q{14}{$Q}{max} || 0) ) / 2);
		my $avgph = ((($Q{ph}{$Q}{min} || 0) + ($Q{ph}{$Q}{max} || 0) ) / 2);
		
		$res{$Q}{'ILL 1.0'} = $avg14;
		$res{$Q}{'ILL 1.3'} = $avg14;
		$res{$Q}{'ILL 1.5'} = $avg14;
		$res{$Q}{SANGER}    = $avgph;
	}
	
	return $res{$query};
}

sub getQfromP {
	my $p = shift;
	my %res;
	
	my $lp         = $p ? log($p)        : $p;
	my $lpmp       = $p ? log($p/(1-$p)) : $p;
	my $q14        = -10 * ($lpmp/log(10));
	my $Qphred     = -10 * ($lp  /log(10));

	$res{'ILL 1.0'} = int($q14);
	$res{'ILL 1.3'} = int($q14);
	$res{'ILL 1.5'} = int($q14);
	$res{SANGER}    = int($Qphred);

	return \%res;
}


sub printQTable {

	my %Q;
	for ( my $p = 0; $p <= 0.8; $p += 0.00001 ) {
		my $lp         = $p ? log($p)        : $p;
		my $lpmp       = $p ? log($p/(1-$p)) : $p;
		my $q14        = -10 * ($lpmp/log(10));
		my $Qphred     = -10 * ($lp  /log(10));
		last if ( int($q14) == -6 && int($Qphred) ==  0 );
		next if ( int($q14) >  40 && int($Qphred) >  40 );
		$Q{14}{int($q14)}{min}    = $p if ( ! exists $Q{14} || ! exists ${$Q{14}}{int($q14)}    || ! exists ${$Q{14}{int($q14)}}{min}    || $p < $Q{14}{int($q14)}{min}    );
		$Q{14}{int($q14)}{max}    = $p if ( ! exists $Q{14} || ! exists ${$Q{14}}{int($q14)}    || ! exists ${$Q{14}{int($q14)}}{max}    || $p > $Q{14}{int($q14)}{max}    );
		$Q{ph}{int($Qphred)}{min} = $p if ( ! exists $Q{ph} || ! exists ${$Q{ph}}{int($Qphred)} || ! exists ${$Q{ph}{int($Qphred)}}{min} || $p < $Q{ph}{int($Qphred)}{min} );
		$Q{ph}{int($Qphred)}{max} = $p if ( ! exists $Q{ph} || ! exists ${$Q{ph}}{int($Qphred)} || ! exists ${$Q{ph}{int($Qphred)}}{max} || $p > $Q{ph}{int($Qphred)}{max} );
		#printf "%3f => %3d => %3d\n", $p, int($q14), int($Qphred);
	}
	
	my %pKeys;
	map { $pKeys{$_}++ } keys %{$Q{14}};
	map { $pKeys{$_}++ } keys %{$Q{ph}};
	
	my %res;
	my $keysQ; my $valsPmin; my $valsPavg; my $valsPmax; my $vals14min; my $vals14avg; my $vals14max;
	foreach my $Q ( sort { $a <=> $b } keys %pKeys ) {
		$keysQ     .= sprintf("%5d " , $Q);
		$vals14min .= defined $Q{14}{$Q}{min} ? sprintf("%.3f ", $Q{14}{$Q}{min}) : sprintf("%.3f ", 0.0);
		$vals14max .= defined $Q{14}{$Q}{max} ? sprintf("%.3f ", $Q{14}{$Q}{max}) : sprintf("%.3f ", 0.0);
		$valsPmin  .= defined $Q{ph}{$Q}{min} ? sprintf("%.3f ", $Q{ph}{$Q}{min}) : sprintf("%.3f ", 0.0);
		$valsPmax  .= defined $Q{ph}{$Q}{max} ? sprintf("%.3f ", $Q{ph}{$Q}{max}) : sprintf("%.3f ", 0.0);
		
		my $avg14 = sprintf("%.3f", ((($Q{14}{$Q}{min} || 0) + ($Q{14}{$Q}{max} || 0) ) / 2));
		my $avgph = sprintf("%.3f", ((($Q{ph}{$Q}{min} || 0) + ($Q{ph}{$Q}{max} || 0) ) / 2));
		$vals14avg .= "$avg14 ";
		$valsPavg  .= "$avgph ";
		
		$res{$Q}{Ill}   = $avg14;
		$res{$Q}{Phred} = $avgph;
	}
	
	my $s = 96;
	for ( my $p = 0; $p < length($keysQ); $p += $s) {
		print "Q         ", substr($keysQ    ,   $p, $s), "\n";
		print "Phred MIN ", substr($valsPmin ,   $p, $s), "\n";
		print "Phred AVG ", substr($valsPavg ,   $p, $s), "\n";
		print "Phred MAX ", substr($valsPmax ,   $p, $s), "\n";
		print "Ill   MIN ", substr($vals14min,   $p, $s), "\n";
		print "Ill   AVG ", substr($vals14avg,   $p, $s), "\n";
		print "Ill   MAX ", substr($vals14max,   $p, $s), "\n";
		print "\n";
	}
	
	
	
	
	
	
	my $san; my $pSan; my $ascD; my $ascII; my $q10; my $q13; my $q15;;
	for ( my $asc = 33; $asc <= 126; $asc++ ) {
		if ( $asc >= 64 ) {
			$q13   .= sprintf("%5d ", $asc-64 );
			$q15   .= sprintf("%5d ", $asc-64 );
		} else {
			$q13   .= "      ";
			$q15   .= "      ";
		}
	
		if ( $asc >= 59 ) {
			$q10   .= sprintf("%5d ", $asc-64 );
		} else {
			$q10   .= "      ";
		}
		$ascD  .= sprintf("%5d ", $asc);
		$ascII .= sprintf("%5s ", chr($asc));
		$san   .= sprintf("%5d ", $asc - 33);
		$pSan  .= sprintf("%.3f ", (10 ** ((($asc - 33)*-1) / 10) ));
	}
	print "\n";
	
	$s = 96;
	for ( my $p = 0; $p < length($ascD); $p += $s) {
		while ($p+$s > length($ascD)) { $s-- };
		print "ASCII    ", substr($ascD ,   $p, $s), "\n";
		print "ASCII    ", substr($ascII,   $p, $s), "\n";
		print "SANGER   ", substr($san  ,   $p, $s), "\n";
		print "SANGER P ", substr($pSan ,   $p, $s), "\n";
		print "Q1.0     ", substr($q10  ,   $p, $s), "\n";
		print "Q1.3     ", substr($q13  ,   $p, $s), "\n";
		print "Q1.5     ", substr($q15  ,   $p, $s), "\n";
		print "\n";
	}
	print "\n";
	
	exit 0;
	#Q            -5    -4    -3    -2    -1     0     1     2     3     4     5     6     7     8     9    10 
	#Phred MIN 0.000 0.000 0.000 0.000 0.000 0.000 0.631 0.501 0.398 0.316 0.251 0.200 0.158 0.126 0.100 0.079 
	#Phred AVG 0.000 0.000 0.000 0.000 0.000 0.400 0.713 0.566 0.450 0.357 0.284 0.225 0.179 0.142 0.113 0.090 
	#Phred MAX 0.000 0.000 0.000 0.000 0.000 0.799 0.794 0.631 0.501 0.398 0.316 0.251 0.200 0.158 0.126 0.100 
	#Ill   MIN 0.760 0.715 0.666 0.613 0.557 0.000 0.387 0.334 0.285 0.240 0.201 0.166 0.137 0.112 0.091 0.074 
	#Ill   AVG 0.779 0.737 0.691 0.640 0.585 0.279 0.415 0.360 0.309 0.263 0.221 0.184 0.152 0.124 0.101 0.082 
	#Ill   MAX 0.799 0.760 0.715 0.666 0.613 0.557 0.443 0.387 0.334 0.285 0.240 0.201 0.166 0.137 0.112 0.091 
	#
	#Q            11    12    13    14    15    16    17    18    19    20    21    22    23    24    25    26 
	#Phred MIN 0.063 0.050 0.040 0.032 0.025 0.020 0.016 0.013 0.010 0.008 0.006 0.005 0.004 0.003 0.003 0.002 
	#Phred AVG 0.071 0.057 0.045 0.036 0.028 0.023 0.018 0.014 0.011 0.009 0.007 0.006 0.004 0.004 0.003 0.002 
	#Phred MAX 0.079 0.063 0.050 0.040 0.032 0.025 0.020 0.016 0.013 0.010 0.008 0.006 0.005 0.004 0.003 0.003 
	#Ill   MIN 0.059 0.048 0.038 0.031 0.025 0.020 0.016 0.012 0.010 0.008 0.006 0.005 0.004 0.003 0.003 0.002 
	#Ill   AVG 0.066 0.054 0.043 0.034 0.028 0.022 0.018 0.014 0.011 0.009 0.007 0.006 0.004 0.004 0.003 0.002 
	#Ill   MAX 0.074 0.059 0.048 0.038 0.031 0.024 0.020 0.016 0.012 0.010 0.008 0.006 0.005 0.004 0.003 0.003 
	#
	#Q            27    28    29    30    31    32    33    34    35    36    37    38    39    40 
	#Phred MIN 0.002 0.001 0.001 0.001 0.001 0.001 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 
	#Phred AVG 0.002 0.001 0.001 0.001 0.001 0.001 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 
	#Phred MAX 0.002 0.002 0.001 0.001 0.001 0.001 0.001 0.000 0.000 0.000 0.000 0.000 0.000 0.000 
	#Ill   MIN 0.002 0.001 0.001 0.001 0.001 0.001 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 
	#Ill   AVG 0.002 0.001 0.001 0.001 0.001 0.001 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 
	#Ill   MAX 0.002 0.002 0.001 0.001 0.001 0.001 0.001 0.000 0.000 0.000 0.000 0.000 0.000 0.000 
	#
	#
	#ASCII       33    34    35    36    37    38    39    40    41    42    43    44    45    46    47    48 
	#ASCII        !     "     #     $     %     &     '     (     )     *     +     ,     -     .     /     0 
	#SANGER       0     1     2     3     4     5     6     7     8     9    10    11    12    13    14    15 
	#SANGER P 1.000 0.794 0.631 0.501 0.398 0.316 0.251 0.200 0.158 0.126 0.100 0.079 0.063 0.050 0.040 0.032 
	#Q1.0                                                                                                     
	#Q1.3                                                                                                     
	#Q1.5                                                                                                     
	#
	#ASCII       49    50    51    52    53    54    55    56    57    58    59    60    61    62    63    64 
	#ASCII        1     2     3     4     5     6     7     8     9     :     ;     <     =     >     ?     @ 
	#SANGER      16    17    18    19    20    21    22    23    24    25    26    27    28    29    30    31 
	#SANGER P 0.025 0.020 0.016 0.013 0.010 0.008 0.006 0.005 0.004 0.003 0.003 0.002 0.002 0.001 0.001 0.001 
	#Q1.0                                                                    -5    -4    -3    -2    -1     0 
	#Q1.3                                                                                                   0 
	#Q1.5                                                                                                   0 
	#
	#ASCII       65    66    67    68    69    70    71    72    73    74    75    76    77    78    79    80 
	#ASCII        A     B     C     D     E     F     G     H     I     J     K     L     M     N     O     P 
	#SANGER      32    33    34    35    36    37    38    39    40    41    42    43    44    45    46    47 
	#SANGER P 0.001 0.001 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 
	#Q1.0         1     2     3     4     5     6     7     8     9    10    11    12    13    14    15    16 
	#Q1.3         1     2     3     4     5     6     7     8     9    10    11    12    13    14    15    16 
	#Q1.5         1     2     3     4     5     6     7     8     9    10    11    12    13    14    15    16 
	#
	#ASCII       81    82    83    84    85    86    87    88    89    90    91    92    93    94    95    96 
	#ASCII        Q     R     S     T     U     V     W     X     Y     Z     [     \     ]     ^     _     ` 
	#SANGER      48    49    50    51    52    53    54    55    56    57    58    59    60    61    62    63 
	#SANGER P 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 
	#Q1.0        17    18    19    20    21    22    23    24    25    26    27    28    29    30    31    32 
	#Q1.3        17    18    19    20    21    22    23    24    25    26    27    28    29    30    31    32 
	#Q1.5        17    18    19    20    21    22    23    24    25    26    27    28    29    30    31    32 
	#
	#ASCII       97    98    99   100   101   102   103   104   105   106   107   108   109   110   111   112 
	#ASCII        a     b     c     d     e     f     g     h     i     j     k     l     m     n     o     p 
	#SANGER      64    65    66    67    68    69    70    71    72    73    74    75    76    77    78    79 
	#SANGER P 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 
	#Q1.0        33    34    35    36    37    38    39    40    41    42    43    44    45    46    47    48 
	#Q1.3        33    34    35    36    37    38    39    40    41    42    43    44    45    46    47    48 
	#Q1.5        33    34    35    36    37    38    39    40    41    42    43    44    45    46    47    48 
	#
	#ASCII      113   114   115   116   117   118   119   120   121   122   123   124   125   126 
	#ASCII        q     r     s     t     u     v     w     x     y     z     {     |     }     ~ 
	#SANGER      80    81    82    83    84    85    86    87    88    89    90    91    92    93 
	#SANGER P 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 0.000 
	#Q1.0        49    50    51    52    53    54    55    56    57    58    59    60    61    62 
	#Q1.3        49    50    51    52    53    54    55    56    57    58    59    60    61    62 
	#Q1.5        49    50    51    52    53    54    55    56    57    58    59    60    61    62

}






	#my $z1   = (($zThr - $zMin) * 0.33 );
	#my $z2   = (($zThr - $zMin) * 0.66 );
	#my $z3   = (($zMax - $zThr) * 0.25 ) + $zThr;
	#my $z4   = (($zMax - $zThr) * 0.50 ) + $zThr;
	#my $z5   = (($zMax - $zThr) * 0.75 ) + $zThr;
	#print "\tZ MIN $zMin Z1 $z1 Z2 $z2 ZTHR $zThr Z3 $z3 Z4 $z4 Z5 $z5 MAX $zMax\n";
	
	
	

	#set palette gray negative
	
	#set palette defined ( 0 1 1 1, 25 0 0 0, 30 0 0 1,  32.5 0 1 1, 35 0 1 0, 37.5 1 1 0, 40 1 0 0 )
	#set palette defined ( 0 1 1 1, 7.5 .5 .5 .5, 27.5 0 0 0, 30 0 0 1,  32.5 0 1 1, 35 0 1 0, 37.5 1 1 0, 40 1 0 0 )
	#set palette negative defined ( $zMin 1 1 1, $z1 .5 .5 .5, $z2 0 0 0, $zThr 0 0 1, $z3 0 1 1, $z4 0 1 0, $z5 1 1 0, $zMax 1 0 0 )
	#set palette defined ( $zMin 1 0 0 , $z1 1 1 0, $z2 0 1 0, $zThr 0 0 1, $z3 0 1 1, $z4 0 0 0, $z5 .5 .5 .5, $zMax 1 1 1)
	#set palette defined ( $zMin 1 0 0 , $z1 1 1 0, $z2 0 1 0, $zThr 0 0 1, $z3 0 1 1, $z4 0 0 0,                $zMax 1 1 1)
	#                      red          yellow     green      blue         cyan       black      grey          white
	#set palette defined (  0 0 0 0  ,    25 0 .5 0,    30 0 1 0, $zMax 1 1 1)
	#                      black         darkGreen      green        
	
	
	
	
	
	#http://mng.iop.kcl.ac.uk/site/node/292
	#Chars 37-58 are only seen in standard fastq:
	#1.& ' ( ) * + , - . / 0 2 3 4 5 6 7 8 9 :
	#If none of the above characters are present, data is probably Illumina so:
	#Chars 59-68 are not seen in the Illumina files post v1.3:
	#1.? @ A B C D
	#I haven't checked the above is actually true yet. Use with caution :)
	#chr(33 + 10* ( log(1+10**((ord($_)-64)/10)) / log(10) ))
	
	#http://maq.sourceforge.net/qual.shtml
	# $Q = -10 * log($e) / log(10);
	# For example, if the quality of a base call is 30, the probability that it is wrong is 0.001. In other words, given 1000 base calls with Q=30, one of them is wrong in average.
	#error probability of a base is $e, the Solexa quality $sQ is:
	#    $sQ = -10 * log($e / (1 - $e)) / log(10);
	#Solexa quality $sQ can be converted to Phred quality $Q with this formula:
	#    $Q = 10 * log(1 + 10 ** ($sQ / 10.0)) / log(10);
	#Phred quality can never be negative, but Solexa quality can be negative. This is the most effective way to tell what type of quality is used. 
	
	
	
	#http://maq.sourceforge.net/fq_all2std.pl
	#my @conv_table;
	#for my $ord (-64..64) {
	#	my $letter = chr(int(33 + 10*log(1+10**($ord/10.0))/log(10)+.499));
	#	print "ORD $ord CHR ".($ord + 64)." [".(chr($ord + 64))."] LETTER ".$letter."\n";
	#	$conv_table[$ord + 64] = $letter;
	#}
	
	#print $exam_sol;
	
	
	
	
	#solexa <= 1.3 Q = -10 * log10(p/(1-p))
	#solexa >  1.3 Q = -10 * log10(p)
	
	
	#$q = chr(($Q<=93? $Q : 93) + 33);
	#$Q = ord($q) - 33;r
	#$Q = 10 * log(1 + 10 ** (ord($sq) - 64) / 10.0)) / log(10);
	#SOLEXA 1.0 -5 TO 62 OFF 33 ASCII 59(;) < = > ? @  126(~)
	#SOLEXA 1.3  0 TO 62 OFF 64 ASCII            64(@) 126(~) [0-40]
	
	
	#ASCII  59  60  61  62  63  64  65  66  67  68  69  70  71  72  73  74  75  76  77  78  79  80  81  82  83  84  85  86  87  88  89  90  91  92  93  94  95  96  97  98  99 100 101 102 103 104 
	#ASCII   ;   <   =   >   ?   @   A   B   C   D   E   F   G   H   I   J   K   L   M   N   O   P   Q   R   S   T   U   V   W   X   Y   Z   [   \   ]   ^   _   `   a   b   c   d   e   f   g   h 
	#Q1.0   -5  -4  -3  -2  -1   0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31  32  33  34  35  36  37  38  39  40 
	#Q1.3                        0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31  32  33  34  35  36  37  38  39  40 
	#Q1.5                        0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31  32  33  34  35  36  37  38  39  40 
	#                                                                                       ^- threshold








	#415546084 lines
	#29665746364 size
	#71.389786852
	
	# X = 202
	# Y = 680
	# X / Y = 0.2970
	# Y / X = 3.09
	# 1024 x 3165 = 3.240.960
	#  304 x 1024 =   311.488
	#  640 x 1978 = 1.265.920 <-- 423796376 / 1265920 = 334
	# 1024 x  768 =   786.432 <-- 423796376 /  786432 =  538
	
	
	
	
	
	
	#my $register2;
	#my $rCount2 = 0;
	#my $lineC   = 1;
	#my %A;
	#my $sTime   = time;
	#open FI, "<$infile"  or die "COULD NOT OPEN INPUT FILE $infile: $!";
	#while ($register2 = <FI>) {
	#	next if ( $lineC++ % 4 != 0 );
	#	$rCount2++;
	#	chomp $register2;
	#
	#	map{ $A{$_}++ } split(//, $register2);
	#	last if ( $max != -1 && $rCount2 >= $max);
	#}
	#my @B      = sort {$a cmp $b} keys %A;
	#my $Bmin   = $B[0];
	#my $Bmax   = $B[-1];
	#my $bCount = 0;
	#map { $bCount += $A{$_} } @B;
	#
	#print "NUM SEQS : $rCount2\n";
	#print "NUM BASES: $bCount\n";
	#print "MIN $B[0] [",ord($Bmin)," => ", $A{$Bmin},"] MAX $B[-1] [",ord($Bmax)," => ", $A{$Bmax},"]\n";
	#for ( my $b = 0; $b < @B; $b++) {
	#	my $bv  = $B[$b];
	#	my $bc  = $A{$bv};
	#	my $per = (int(($bc/$bCount)*100000) / 1000);
	#	printf "%s [%3d] = %15d (%8.4f%%)\t", $bv, ord($bv), $bc, $per;
	#	print "\n" if ($b+1) % 5 == 0;
	#}
	#print "\n";
	#close FI;
	#my $eTime = time - $sTime;
	#print "ELA $eTime\n";

