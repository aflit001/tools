#define PRG_NAME "fastqCount"
#define CREATOR  "Saulo Aflitos"
#define INSTITU  "CBSG/PRI/WUR"
#define LICENSE  "Apache License 2.0"
#define KMERLEN  31

#ifndef VERSION
#define VERSION __DATE__" "__TIME__
#endif

#include "fastqCount.hpp"
namespace po = boost::program_options;
namespace fs = boost::filesystem;
using namespace std;





int main(int argc,char **argv) {
    string inFile;
    string ouFile;
    bool   compressed = false;
    int    kmerLen    = KMERLEN;
    
    getParams(argc, argv, inFile, ouFile, compressed, kmerLen);
    long int fileSize = fs::file_size( inFile );
    stats stats = {inFile, ouFile, 0, fileSize, 0, 0, 0, 0, 0, kmerLen, 0};
//                                 numSeq       sizeData sizeRead       kmerCount
//                                    sizeFile     sizeSeqs countN
//                                                    sizeSeqsClean
//                                                             kmerLen
    
    if ( compressed ) {
        ifstream in;
        in.open((char *)inFile.c_str(), ios_base::in | ios_base::binary);
        try {
            boost::iostreams::filtering_istream strea;
            strea.push(boost::iostreams::gzip_decompressor());
            strea.push(in);
            process(strea, stats);
        }
        catch(const boost::iostreams::gzip_error& e) {
             cout << e.what() << '\n';
        }
        in.close();
    } else {
        ifstream in((char*)inFile.c_str(), ios::in);

        //cout << "CHECKING IF IT IS OPEN" << endl;

        if(!in.is_open()) {
            cerr << "COULD NOT OPEN FILE" << endl;
            return EXIT_FAILURE;
        } else {
            //cout << "FILE IS OPEN" << endl;
            cerr << "PROCESSING" << endl;
            process(in, stats);
        }
        //cout << "READING FILE" << endl;
        in.close();
    }

    printStats(stats);
    //cout << "FINISHED READING" << endl;

    return 0;
}

void printStats(stats &stats) {
    string outFile = stats.outFile;
    ofstream out( (char*)outFile.c_str(), ios_base::out );
    
    if(!out.is_open()) {
        cerr << "COULD NOT OPEN FILE" << endl;
        exit(EXIT_FAILURE);
    } else {
        //cout << "FILE IS OPEN" << endl;
        std::stringstream outS;
        outS << "INFILE:"         << stats.inFile        <<
                "|OUTFILE:"       << stats.outFile       <<
                "|SIZEFILE:"      << stats.sizeFile      <<
                "|SIZEDATA:"      << stats.sizeData      <<
                "|SIZEREAD:"      << stats.sizeRead      <<
                "|SIZESEQS:"      << stats.sizeSeqs      <<
                "|SIZESEQSCLEAN:" << stats.sizeSeqsClean <<
                "|COUNTNs:"       << stats.countN        <<
                "|KMERLEN:"       << stats.kmerLen       <<
                "|KMERCOUNT:"     << stats.kmerCount     << endl;
        cout << outS.str();
        out  << outS.str();
    }
    

    out.close();
    if ( ! fs::exists(outFile) ){
        cerr << "    error creating output file " << outFile << endl;
        exit(1);
    }    
    
}

template <class T>
void process(T &in, stats &stats) {
    long int&    numSeq        = stats.numSeq;
    long int&    sizeData      = stats.sizeData;
    long int&    sizeSeqs      = stats.sizeSeqs;
    long int&    sizeSeqsClean = stats.sizeSeqsClean;
    long int&    sizeRead      = stats.sizeRead;
    long int&    countN        = stats.countN;
         int&    kmerLen       = stats.kmerLen;
    long int&    kmerCount     = stats.kmerCount;

    string name, seq, name2, qual;
    while (!in.eof()) {
        ++numSeq;
        if(!getline(in,name ,'\n')) {
            if (in.eof()) {
                //cerr << "EOF " << numSeq << endl;
                break;
            } else {
                cerr << "ERROR READING :: " << numSeq << " " << endl;
                exit(1);
            };
        };

        if(!getline(in,seq  ,'\n')) { cerr << "ERROR READING :: " << numSeq << " " << name  << endl; break; };
        if(!getline(in,name2,'\n')) { cerr << "ERROR READING :: " << numSeq << " " << seq   << endl; break; };
        if(!getline(in,qual ,'\n')) { cerr << "ERROR READING :: " << numSeq << " " << name2 << endl; break; };

//        cout << "NAME  " << name  << endl;
//        cout << "SEQ   " << seq   << endl;
        transform(seq.begin(), seq.end(), seq.begin(), ::tolower);
//        cout << "SEQ   " << seq   << endl;
//        cout << "NAME2 " << name2 << endl;
//        cout << "QUAL  " << qual  << endl;

        int lSize = seq.size();
        sizeSeqs      += lSize;
        sizeSeqsClean += lSize;
        sizeData      += name.size() + 1 + seq.size() + 1 + name2.size() + 1 + qual.size() + 1;
        if ( sizeRead == 0 ) {
            sizeRead = lSize;
        }

        for (int i = 0 ; i < lSize ; ++i) {
            char letter = seq[i];
            if ( letter == 'n' ) {
                ++countN;
                --sizeSeqsClean;
            }
        }

//        cout << "CHECKING KMER " << merLen << " IN SEQ " << seq << " LSIZE " << lSize << endl;
        for ( int s = 0; s <= lSize; ++s )
        {
            int endPos = (s+kmerLen);
            if ( endPos > lSize ) { break; };
            string sub = seq.substr(s, kmerLen);
//            cout << "  POS " << s << " SUB " << s << " " << sub << " " << endPos << endl;
            int p = sub.find("n");
            if ( p != (signed) string::npos ) {
//                cout << "    first 'needle' found at: " << int(p) << endl;
                s += int(p);
            } else {
                ++kmerCount;
            }
        }
    }
}


void getParams(int argc, char **argv, string &inFile, string &outFile, bool &compressed, int &kmerLen) {
    bool   force      = false;
    try {
        po::options_description comp("Compulsory options");
        comp.add_options()
            ("input,i",  po::value<string>(), "input fastq(.gz) file")
        ;
        
        po::options_description opt("Optional options");
        opt.add_options()
            ("output,o",  po::value<string>(), "output nfo file")
            ("kmerlen,k", po::value<int>(),    "output nfo file")
            ("force,f",                        "force replace output")    
        ;
        
        po::options_description inf("Information");
        inf.add_options()
            ("help,h",                        "produce help message")
            ("version,v",                     "print version")
        ;
        
        po::options_description cmd_line;
        cmd_line.add(comp).add(opt).add(inf);
    
        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, cmd_line), vm);
        po::notify(vm);
        
        if ( vm.count("help") ) {
            cout << "PROGRAM NAME: " << PRG_NAME << endl;
            cout << "VERSION     : " << VERSION  << endl;
            cout << "CREATOR     : " << CREATOR  << endl;
            cout << "FUNDING     : " << INSTITU  << endl;
            cout << "LICENSE     : " << LICENSE  << endl;
            cout << cmd_line << endl;
            exit(0);
        }

        if ( vm.count("version") ) {
            cout << "VERSION: " << VERSION << endl;
            exit(0);
        }
        
        if ( vm.count("input")) {
            inFile = vm["input"].as<string>();
            cout << "  input file: '" << inFile << "'" << endl;
            
            bool found =   (( inFile.find(".fastq") != string::npos  ) ||
                            ( inFile.find(".fq")    != string::npos  ) ||
                            ( inFile.find(".FASTQ") != string::npos  ) ||
                            ( inFile.find(".FQ")    != string::npos  ));
            if ( !found ) {
                cerr << "    error: not .fastq or .fq file" << endl;
                exit(1);
            }
            
            if((inFile.substr(inFile.find_last_of(".") + 1) == "gz") ||
               (inFile.substr(inFile.find_last_of(".") + 1) == "GZ")) {
                compressed = true;
                cerr << "    Compressed..." << endl;
            }
            
            if ( ! fs::exists(inFile) ){
                cerr << "    Input file " << inFile << " does not exists" << endl;
                exit(1);
            }
            
            cout << "size of " << inFile << " is " << fs::file_size( inFile ) << endl;
            
        } else {
            cerr << "no input file defined" << endl;
            exit(1);
        }

        
        
        if ( vm.count("force")) {
            force = true;
            cout << "  force: '" << force << "'" << endl;
        }
        
        
        
        if ( vm.count("output")) {
            outFile = vm["output"].as<string>();
            cout << "  output file: '" << outFile << "'" << endl;
        } else {
            cerr << "  no output file defined" << endl;
            outFile = inFile + ".nfo";
            cout << "    output file: '" << outFile << "'" << endl;
        }
        
        checkOutputFile(outFile, force);
        
        if ( vm.count("kmerlen")) {
            kmerLen = vm["kmerlen"].as<int>();
        } else {
            cerr << "  no k-mer length defined" << endl;
        }
        cout << "  k-mer length: '" << kmerLen << "'" << endl;
    }
    catch(exception& e) {
        cerr << "error: " << e.what() << endl;
        exit(1);
    }
    catch(...){
        cerr << "exception of unknown type!" << endl;
        exit(1);
    }
}

void checkOutputFile(string outFile, bool force) {
    if ( fs::exists(outFile) ) {
        if ( ! force ) {
            cerr << "    Output file " << outFile << " exists. to replace use --force" << endl;
            exit(1);
        } else {
            cerr << "    Output file " << outFile << " exists. deleting" << endl;
            fs::remove(outFile);
            if ( fs::exists(outFile) ) {
                cerr << "    error removing output file" << endl;
                exit(1);
            }
        }
    }
}
