#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <algorithm>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
//#include <tr1/unordered_set> // use <unordered_set> for more recent g++
//#include <unordered_set> //for more recent g++
using namespace std;

struct stats {
    string inFile;
    string outFile;
    long int    numSeq;
    long int    sizeFile;
    long int    sizeData;
    long int    sizeSeqs;
    long int    sizeSeqsClean;
    long int    sizeRead;
    long int    countN;
         int    kmerLen;
    long int    kmerCount;
};

void printStats(stats &stats);
template <class T>
void process(T &in, stats &stats);
void checkOutputFile(string outFile, bool force);
void getParams(int argc, char **argv, string &inFile, string &outFile, bool &compressed, int &kmerLen);
int main(int argc,char **argv);
