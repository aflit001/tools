#!/usr/bin/perl -w
use XML::Dumper;
use strict;

my $selSpp       = shift;

my $maxLines     = 35;
my $maxWidth     = 80;
my $HTMLBASE     = "/~aflit001";

my $accessories  = "/home/aflit001/public_html_acessories";

my $kmr_qcdir    = "$accessories/src/Data_0_mer";
my $kmr_imgdir   = "$HTMLBASE/images/Data_0_mer";
my $kmr_BASE     = "src/Data_0_mer";

my $fqc_qcdir    = "$accessories/src/fastqc";
my $fqc_imgdir   = "$HTMLBASE/images/fastqc";
my $fqc_BASE     = "src/fastqc";

my $sqa_qcdir    = "$accessories/src/solexaqa";
my $sqa_imgdir   = "$HTMLBASE/images/solexaqa";
my $sqa_BASE     = "src/solexaqa";

my $verbose      = 1;
my $baseOut      = "$accessories/db";
my $sumCols      = 0;

my %structH;
my %progs = (
    Kmer     => { qcdir  => $kmr_qcdir, imgdir => $kmr_imgdir, BASE   => $kmr_BASE, func   => \&getKmer,     help   => "$HTMLBASE/help/Kmer"     },
    FastQC   => { qcdir  => $fqc_qcdir, imgdir => $fqc_imgdir, BASE   => $fqc_BASE, func   => \&getFastQc,   help   => "$HTMLBASE/help/FastQC"   },
    SolexaQA => { qcdir  => $sqa_qcdir, imgdir => $sqa_imgdir, BASE   => $sqa_BASE, func   => \&getSolexaQa, help   => "$HTMLBASE/help/SolexaQA" }
);

foreach my $prog ( sort keys %progs ) {
    my $data   = $progs{$prog};
    my $qcdir  = $data->{qcdir};
    my $imgdir = $data->{imgdir};
    my $base   = $data->{BASE};
    my $func   = $data->{func};
    $func->($prog, $qcdir, $imgdir, $base, \%structH, $selSpp);
}

#&getKmer(    "Kmer",     $kmr_qcdir, $kmr_imgdir, $kmr_BASE, \%structH);
#&getFastQc(  "FastQC",   $fqc_qcdir, $fqc_imgdir, $fqc_BASE, \%structH);
#&getSolexaQa("SolexaQA", $sqa_qcdir, $sqa_imgdir, $sqa_BASE, \%structH);


#use Data::Dumper;
#print Dumper(\%structH);
#exit;

foreach my $spp ( sort keys %structH )
{
    my $osp  = "$baseOut/$spp.xml";
    my $data = $structH{$spp};

    unlink($osp) if ( -f "$osp" );
    my $dump    = new XML::Dumper;
    $dump->dtd;
    $dump->pl2xml($data, $osp);


    if ( -f "$osp" )
    {
        print "$osp exported successfully\n\n";
    } else {
        print "failed to export $osp\n\n";
    }

}


my $outFile      = "$accessories/db.xml";

my %setup   = (
    'progs'       => \%progs   ,
    'HTMLBASE'    => $HTMLBASE ,
    'speciesBase' => $baseOut 
);


if ( ! defined $selSpp || ! -f $outFile ) {
    unlink($outFile) if ( -f "$outFile" );
    my $dump    = new XML::Dumper;
    $dump->dtd;
    $dump->pl2xml(\%setup, $outFile);
}


if ( -f "$outFile" )
{
    print "$outFile exported successfully\n\n";
} else {
    print "failed to export $outFile\n\n";
}






#######################
### SOLEXAQA
#######################
sub getSolexaQa      () {
    my $progName = shift;
    my $indir    = shift;
    my $imdir    = shift;
    my $badir    = shift;
    my $struct   = shift;
    my $sSpecie  = shift;

    #./Heinz/Heinz_Illumina_pairedend_368/3050CAAXX_3_r.fastq.png
    #./Heinz/Heinz_Illumina_pairedend_368/3050CAAXX_3_r.fastq.quality.pdf
    #./Heinz/Heinz_Illumina_pairedend_368/3050CAAXX_3_r.fastq.segments.hist.pdf



    if ( -d $indir ) {
        my %spps;
        if (opendir(my $dh, $indir)) {
            my @spps;
            push(@spps, grep { ! /^\./ && -d "$indir/$_" } readdir($dh));
            #print @spps;
            closedir $dh;
            
            foreach my $spp ( sort @spps )
            {
                #next if $spp ne "Pimpinellifolium";
                next if defined $sSpecie && $spp ne $sSpecie;
                print "  SOLEXAQA SPP $spp\n";
                my @libs;
                if (opendir(my $dh, "$indir/$spp")) {
                    push(@libs, grep { ! /^\./ && -d "$indir/$spp/$_" } readdir($dh));
                    #print @libs;
                    closedir $dh;
                }
                
                foreach my $lib ( sort @libs )
                {
                    print "    SOLEXAQA LIB $lib\n";
                    #./Heinz/Heinz_Illumina_pairedend_368/3050CAAXX_3_r.fastq.png
                    #./Heinz/Heinz_Illumina_pairedend_368/3050CAAXX_3_r.fastq.quality.pdf
                    #./Heinz/Heinz_Illumina_pairedend_368/3050CAAXX_3_r.fastq.segments.hist.pdf
                    
                    my @runs;
                    if (opendir(my $dh, "$indir/$spp/$lib")) {
                        push(@runs, grep { ! /^\./ && -f "$indir/$spp/$lib/$_" && $_ =~ /\.fastq[\.gz]*\.png$/} readdir($dh));
                        #print @runs;
                        closedir $dh;
                    }

                    foreach my $run ( sort @runs )
                    {
                        print "      SOLEXAQA RUN $run\n";
                        my $runName = $run;
                        #$runName =~ s/-|\.//g;
                        $runName =~ s/\.fastq[\.gz]*\.png//g;
                        $run     =~ s/\.png//g;
                        my $tec  = "none";
                           $tec  = $1 if ( $lib =~ /($spp\_\w+?)\_/);


                        $struct->{$spp}{type}                                                 = "Species";

                        $struct->{$spp}{CHILD}{$tec}{type}                                    = "Technology";

                        $struct->{$spp}{CHILD}{$tec}{CHILD}{$lib}{type}                       = "Library";
                        
                        $struct->{$spp}{CHILD}{$tec}{CHILD}{$lib}{CHILD}{$runName}{type}      = "Run";

                        $struct->{$spp}{CHILD}{$tec}{CHILD}{$lib}{CHILD}{$runName}{$progName} = &getSolexaQa_Status($progName, $run, $imdir, "$indir/$spp/$lib/$run", "$badir/$spp/$lib/$run");
                        print "ADDING FASTQC $spp -> $tec -> $lib -> $runName\n" if $verbose;
                    }
                }
            }
        }
    }
}

sub getSolexaQa_Status () {
    my $progName = shift;
    my $src      = shift;
    my $imdir    = shift;
    my $folder   = shift; #local folder
    my $link     = shift; #weblink
    my $tiles    = "$link.png";
    my $qual     = "$link.quality.pdf.png";
    my $qualP    = "$link.quality.pdf";
    my $hist     = "$link.segments.hist.pdf.png";
    my $histP    = "$link.segments.hist.pdf";
    my $tilesF   = "$folder.png";
    my $qualF    = "$folder.quality.pdf.png";
    my $histF    = "$folder.segments.hist.pdf.png";
    my %data;


    #.png
    my $name = "Tiles";
    $data{$name}{link}           = "$tiles";
    $data{$name}{logo}           = "$imdir/tiles.png";
    $data{$name}{path}           = "$tilesF";
    $data{$name}{src}            = "$src";
    $data{$name}{type}           = "image";
    $data{$name}{content}{image} = "$tiles";
    if ( -f $tilesF && -s $tilesF ) {
        my @sizes = split(/\s+/, `identify -format '%w %h\n' $tilesF`);
        $data{$name}{content}{size} = \@sizes if ( scalar @sizes );
    }
    
    print "        SOLEXAQA :: $name\n";
    print "          LINK   ",$data{$name}{link},"\n";
    print "          LOGO   ",$data{$name}{logo},"\n";
    print "          PATH   ",$data{$name}{path},"\n";
    print "          SRC    ",$data{$name}{src} ,"\n";
    print "          TYPE   ",$data{$name}{type},"\n";
    print "          CONT I ",$data{$name}{content}{image},"\n" if exists $data{$name}{content}{image};
    print "          CONT S ",$data{$name}{content}{size} ,"\n" if exists $data{$name}{content}{size};
    $progs{$progName}{cols}{$name}++;
    
    
    
    $name = "Quality per Pos";
        #.quality.pdf
    $data{$name}{link}           = "$qual";
    $data{$name}{logo}           = "$imdir/qual.png";
    $data{$name}{path}           = "$qualF";
    $data{$name}{src}            = "$src";
    $data{$name}{type}           = "image";
    if ( -f $qualF ) {
        $data{$name}{content}{image} = "$qual";
        my @qSizes = split(/\s+/, `identify -format '%w %h\n' $qualF`);
        $data{$name}{content}{size} = \@qSizes if ( scalar @qSizes );
    } else {
        $data{$name}{type} = 'empty';
    }
    print "      SOLEXAQA :: $name\n";
    print "          LINK   ",$data{$name}{link},"\n";
    print "          LOGO   ",$data{$name}{logo},"\n";
    print "          PATH   ",$data{$name}{path},"\n";
    print "          SRC    ",$data{$name}{src} ,"\n";
    print "          TYPE   ",$data{$name}{type},"\n";
    print "          CONT I ",$data{$name}{content}{image},"\n" if exists $data{$name}{content}{image};
    print "          CONT S ",$data{$name}{content}{size} ,"\n" if exists $data{$name}{content}{size};
    $progs{$progName}{cols}{$name}++;
        
        
    #.segments.hist.pdf
    $name = "Segment Length Dist";
    $data{$name}{link}           = "$hist";
    $data{$name}{logo}           = "$imdir/hist.png";
    $data{$name}{path}           = "$histF";
    $data{$name}{src}            = "$src";
    $data{$name}{type}           = "image";
    if ( -f $histF ) {
        $data{$name}{content}{image} = "$hist";
        my @hSizes = split(/\s+/, `identify -format '%w %h\n' $histF`);
        $data{$name}{content}{size} = \@hSizes if ( scalar @hSizes );
    } else {
        $data{$name}{type} = 'empty';
    }
    print "      SOLEXAQA :: $name\n";
    print "          LINK   ",$data{$name}{link},"\n";
    print "          LOGO   ",$data{$name}{logo},"\n";
    print "          PATH   ",$data{$name}{path},"\n";
    print "          SRC    ",$data{$name}{src} ,"\n";
    print "          TYPE   ",$data{$name}{type},"\n";
    print "          CONT I ",$data{$name}{content}{image},"\n" if exists $data{$name}{content}{image};
    print "          CONT S ",$data{$name}{content}{size} ,"\n" if exists $data{$name}{content}{size};
    $progs{$progName}{cols}{$name}++;
    
    return \%data;
}




















############################
###### KMERPLOT
############################

sub getKmer          () {
    my $progName = shift;
    my $indir    = shift;
    my $imdir    = shift;
    my $badir    = shift;
    my $struct   = shift;
    my $sSpecie  = shift;

    if ( -d $indir ) {
        my %dbs;
        
        if (opendir(my $dp, $indir)) {
            my @projects = grep { ! /^\./ && -d "$indir/$_" } readdir($dp);
            foreach my $pf ( @projects )
            {
                if (opendir(my $dh, "$indir/$pf")) {
                    my @fil;
                    push(@fil, grep { ! /^\./ && /\.jf$/ && -f "$indir/$pf/$_" } readdir($dh));
                    push(@{$dbs{$pf}}, @fil);
                    closedir $dh;
                } else {
                    print "unable to read project folder $pf\n";
                    exit;
                }
                #last;
            }
        } else {
            print "unable to read base folder $indir\n";
            exit;
        }
        
        #map { $struct{$_} = {}; } @dbs;
        foreach my $pf ( sort keys %dbs )
        {
            my $db = $dbs{$pf};
            foreach my $key1 ( sort @$db ) {
                $key1 =~ s/\.jf$//;
                foreach my $key2 ( sort @$db ){
                    $key2 =~ s/\.jf$//;
                    next if $key1 eq $key2;
                    if ( $key2 =~ /^$key1/ ) {
                        # $k2 is either child or grandchild
                        foreach my $key3 ( sort @$db ) {
                            $key3 =~ s/\.jf$//;
                            next if $key1 eq $key3;
                            next if $key2 eq $key3;
                            if ( $key3 =~ /^$key2/ ) {
                                #has (grand) child
                                my $spp     = $pf;
                                my $tec     = $key1;
                                my $lib     = $key2;
                                my $run     = $key3;
                                my $runName = $run;
                                next if defined $sSpecie && $spp ne $sSpecie;

                                $runName =~ s/$lib\_//;
                                $runName =~ s/\.fastq\.gz//;
                                $runName =~ s/\.fastq//;

                                print "\nADDING KMER  $spp -> $tec -> $lib -> $run\n" if $verbose;
                                
                                $struct->{$spp}{type}                                                 = "Species";
                                #$struct->{$spp}{kmer}                                                 = getKmer_Info($indir, $spp);

                                $struct->{$spp}{CHILD}{$tec}{type}                                    = "Technology";
                                $struct->{$spp}{CHILD}{$tec}{$progName}                               = getKmer_Info($progName, $indir, $imdir, $badir, $spp, $tec) if ( ! exists $struct->{$spp}{CHILD}{$tec}{$progName} );

                                $struct->{$spp}{CHILD}{$tec}{CHILD}{$lib}{type}                       = "Library";
                                $struct->{$spp}{CHILD}{$tec}{CHILD}{$lib}{$progName}                  = getKmer_Info($progName, $indir, $imdir, $badir, $spp, $lib) if ( ! exists $struct->{$spp}{CHILD}{$tec}{CHILD}{$lib}{$progName} );
                                
                                $struct->{$spp}{CHILD}{$tec}{CHILD}{$lib}{CHILD}{$runName}{type}      = "Run";
                                $struct->{$spp}{CHILD}{$tec}{CHILD}{$lib}{CHILD}{$runName}{$progName} = getKmer_Info($progName, $indir, $imdir, $badir, $spp, $run);
                            } else {
                                #print "NOT A MATCH $key1 -> $key2 -> $key3\n" if $verbose;
                            }
                        }
                    }
                }
            }
        }

        if ($verbose) {
            foreach my $spp (sort keys %$struct) {
                print STDERR "SPECIES $spp\n";
                my $techs = $struct->{$spp}{CHILD};
                
                foreach my $tech (sort keys %{$techs}) {
                    print STDERR "  `->TECHNOLOGY $tech\n";
                    my $libs = $techs->{$tech}{CHILD};
                    
                    foreach my $lib (sort keys %{$libs}) {
                        print STDERR "    `->LIBRARY $lib\n";
                        my $runs = $libs->{$lib}{CHILD};
                        
                        foreach my $run (sort keys %{$runs}) {
                            print STDERR "      `->RUN $run\n"
                        }
                    }
                }
            }
        }
    } else {
        die "INPUT DIR $indir DOESNT EXISTS\n";
    }
}




sub getKmer_Info     () {
    #etKmer_Info($indir, $imdir, $badir, $spp, $run);
    my $progName = shift;
    my $indir    = shift;
    my $imdir    = shift;
    my $badir    = shift;
    my $spp      = shift;
    my $file     = shift;
    my %res;

    print "  getting info dir $indir spp $spp file $file\n";
    my @exts    = (".histo.png",           ".histo",                 ".histo.desc",      ".stats",                 ".nfo");
    my @name    = ("Histogram Image",      "Histogram Text",         "Description",      "Statistics",             "Info");
    my @type    = ("image",                "text",                   "text",             "text",                   "text");
    my @conv1   = ("",                     "",                       "",                 "",                       "");#(?:\.fastq\.gz|\.fastq)\$
    my @conv2   = ("",                     "",                       "",                 "",                       "");
    my @parser  = (undef,                  undef,                    undef,              undef,                    \&getKmer_parseNfo);
    my @logo    = ("$imdir/graph.png",     "$imdir/graphBlack.png",  "$imdir/desc.png",  "$imdir/statistics.png",  "$imdir/number.png");
    #my @cells   = ("Histogram\&nbspImage", "Histogram\&nbspText", "Description", "Statistics");

    for (my $a = 0; $a < @exts; $a++) {
        my $name   = $name[$a];
        my $type   = $type[$a];
        my $ext    = $exts[$a];
        my $logo   = $logo[$a];
        my $conv1  = $conv1[$a];
        my $conv2  = $conv2[$a];
        my $fpars  = $parser[$a];
        
        if ( $conv1 ne "" )
        {
            #print "CONVERTING $conv1 TO $conv2 ON $file\n";
            $file =~ s/$conv1/$conv2/g;
            #print "  FILE $file\n";
        }
        
        my $path  = "$indir/$spp/$file$ext";
        my $link  = "$badir/$spp/$file$ext";
        $progs{$progName}{cols}{$name}++;

        $res{$name}{logo} = $logo;
        
        if ( -f "$path" )
        {
            print "    getting info $path\n";

            $res{$name}{link} = $link;
            $res{$name}{path} = $path;
            $res{$name}{src}  = $file;
            $res{$name}{type} = $type;

            if ( $type eq "text" )
            {
                my $content = &getKmer_Content($path);
                if ( defined $fpars )
                {
                    $fpars->(\$content);
                }
                $content    = splitLines($content);
                $res{$name}{content}{text} = "<pre>$content</pre>\n";
                #print "CONTENT <pre>\n$content\n</pre>\n";
            }
            elsif ( $type eq "image" )
            {
                $res{$name}{content}{image} = $link;
                if ( -f $file && -s $file ) {
                    my @sizes = split(/\s+/, `identify -format '%w %h\n' $file`);
                    $res{$name}{content}{size} = \@sizes if ( scalar @sizes );
                }
            }
        } else {
            $res{$name}{type} = 'empty';
            print "    could not find path $path\n";
            #exit 1;
        }
    }
    
    return \%res;
}

sub getKmer_parseNfo () {
    #print "PARSING NFO $$s\n";
    my $s     = shift;
    my @lines = split(/\|/, $$s);
    my $siz   = 0;
    
    for ( my $p = 0; $p < @lines; $p++ )
    {
        my $l = $lines[$p];
        my ( $a, $b ) = split(/\:/, $l);
        $siz = length($a) if length($a) > $siz;
    }
    
    for ( my $p = 0; $p < @lines; $p++ )
    {
        my $l = $lines[$p];
        my ( $a, $b ) = split(/\:/, $l);
        $lines[$p] = sprintf("%-".$siz."s: %s", $a, $b);
    }
    #$$s =~ s/\|/\n/g;
    $$s = join("\n", @lines);
    
    #print "  NFO PARSED\n$$s\n\n";
}

sub getKmer_Content  () {
    my $path     = shift;

    if (open FI, "<$path") {
        my @lines = <FI>;
        close FI;
        return join("", @lines);
    } else {
        print "failed to open $path\n";
        exit 1;
    }
}

sub splitLines () {
    my $lines = shift;
    #print "LINES B $lines\n";
    
    my @arr = split(/\n/, $lines);
    my @bar;
    
    #print "ARR ";
    #print join("\n", @arr);
    #print "\n";
    
    for ( my $a = 0; $a < @arr ; ++$a )
    {
        my $l = $arr[$a];
        next if ! $l || $l eq "";
        if ( length $l > $maxWidth )
        {
            while ( length $l > $maxWidth )
            {
                push(@bar, substr( $l, 0, $maxWidth));
                $l = substr( $l, ($maxWidth + 1));
            }
        } else {
            push(@bar, $l);
        }
    }
    
    #print "BAR ";
    #print join("\n", @bar);
    #print "\n";
    
    @arr = @bar;
    
    my $total = scalar @bar;
    
    map { chomp } @bar;

    for ( my $p = $maxLines; $p < $total; $p++ ) {
        my $pos = ($p % $maxLines);
        $bar[$pos] .= "    " . $bar[$p];
        #print "POS $pos P $p\n";
        delete($bar[$p]);
    }

    #map { $_ .= "\n" } @arr;
    
    #foreach my $l ( @arr )
    #{
    #    chomp $l;
    #    if ( $l ne "" )
    #    {
    #        $lines = $l . "<br/>\n";
    #    }
    #}
    my $res = "";
    map { $res .= "" . $_ . "\n" if $_ && $_ ne "" } @bar;
    #print "LINES A $res\n";
    return $res;
}

































#######################
### FASTQC
#######################

sub getFastQc        () {
    my $progName = shift;
    my $indir    = shift;
    my $imdir    = shift;
    my $badir    = shift;
    my $struct   = shift;
    my $sSpecie  = shift;

    
    #Heinz/Heinz_Illumina_matepair_3000/3018DAAXX_2_f_fastqc/summary.txt
    if ( -d $indir ) {
        my %spps;
        if (opendir(my $dh, $indir)) {
            my @spps;
            push(@spps, grep { ! /^\./ && -d "$indir/$_" } readdir($dh));
            #print @spps;
            closedir $dh;
            
            foreach my $spp ( sort @spps )
            {
                next if defined $sSpecie && $spp ne $sSpecie;
                #next if $spp ne "Pimpinellifolium";
                my @libs;
                if (opendir(my $dh, "$indir/$spp")) {
                    push(@libs, grep { ! /^\./ && -d "$indir/$spp/$_" } readdir($dh));
                    #print @libs;
                    closedir $dh;
                }
                
                foreach my $lib ( sort @libs )
                {
                    my @runs;
                    if (opendir(my $dh, "$indir/$spp/$lib")) {
                        push(@runs, grep { ! /^\./ && -d "$indir/$spp/$lib/$_" } readdir($dh));
                        #print @runs;
                        closedir $dh;
                    }

                    foreach my $run ( sort @runs )
                    {
                        my $runName = $run;
                        #$runName =~ s/-|\.//g;
                        $runName =~ s/_fastqc//g;
                        my $tec  = "none";
                        $tec = $1 if ( $lib =~ /($spp\_\w+?)\_/);


                        $struct->{$spp}{type}                                                 = "Species";

                        $struct->{$spp}{CHILD}{$tec}{type}                                    = "Technology";

                        $struct->{$spp}{CHILD}{$tec}{CHILD}{$lib}{type}                       = "Library";
                        
                        $struct->{$spp}{CHILD}{$tec}{CHILD}{$lib}{CHILD}{$runName}{type}      = "Run";

                        $struct->{$spp}{CHILD}{$tec}{CHILD}{$lib}{CHILD}{$runName}{$progName} = &getFastQc_Status($progName, $run, $imdir, "$indir/$spp/$lib/$run", "$badir/$spp/$lib/$run");
                        print "ADDING FASTQC $spp -> $tec -> $lib -> $runName\n" if $verbose;
                    }
                }
            }
        }
    }
}

sub getFastQc_Status () {
    my $progName = shift;
    my $src      = shift;
    my $imdir    = shift;
    my $folder   = shift;
    my $link     = shift;
    my $indexF   = "$folder/fastqc_report.html";
    my $indexL   = "$link/fastqc_report.html";
    my %data;

    $data{Zip}{link}  = "$link.zip";
    $data{Zip}{logo}  = "$imdir/zip.png";
    $data{Zip}{path}  = "$folder.zip";
    $data{Zip}{src}   = "$src";
    $data{Zip}{type}  = "link";

    $data{Text}{link} = "$link/fastqc_data.txt";
    $data{Text}{logo} = "$imdir/text.png";
    $data{Text}{path} = "$folder/fastqc_data.txt";
    $data{Text}{src}  = "$src";    
    $data{Text}{type} = "link";
    $progs{$progName}{cols}{Zip}++;
    $progs{$progName}{cols}{Text}++;

    
    if (( -d "$folder" ) && ( -f "$indexF" )) {
        if (open FI, "<$indexF") {
            my @ms;
            while (my $line = <FI>) {
                if ( $line =~ /^\<div class="summary"\>/) {
                    my $lineC = 0;
                    while ( my $iline = <FI> ) {
                        if ( $iline !~ /^\<\/div\>/ ) { 
                            next if $iline !~ /li/;
                            chomp $iline;

                            if ( $iline =~ /\<a href\=\"\#M(\d+?)\"\>(.+?)\<\/a\>/ ) {
                                print STDOUT "  ADDING NAME $2 IMAGE $1 :: $iline\n";
                                my $lnkNum = $1;
                                my $name   = $2;

                                $progs{$progName}{cols}{$name}++;
                                $ms[$lnkNum] = $name;
                            
                                #$iline =~ s/<\/*li>//g;
                                #$iline =~ s/src=\"/src=\"$fqc_imgdir\//;
                                #$iline =~ s/a href\=\"/a href\=\"$indexL/;
                                #$iline =~ s/(\<img.+?)(\<a.+?\>).+\<\/a/$2$1\<\/a/;
                                #$iline =~ s/\<img/\<img width=20 height=20/;
                                
                                if ( $iline =~ /src\=\"(\S+)\"/ )
                                {
                                    my $logo = $1;
                                    print "    ADDING LOGO $logo\n";
                                    $data{$name}{link} = "$indexL#M$lnkNum";
                                    $data{$name}{logo} = "$imdir/$logo";
                                    $data{$name}{path} = "$indexL";
                                    $data{$name}{src}  = $src;
                                    $data{$name}{type} = "text";
                                    print "      LINK  ",$data{$name}{link},"\n";
                                    print "      LOGO  ",$data{$name}{logo},"\n";
                                    print "      PATH  ",$data{$name}{path},"\n";
                                    print "      SRC   ",$data{$name}{src} ,"\n";
                                    print "      TYPE  ",$data{$name}{type},"\n";
                                    
                                    #$data{Text}{link} = "$link/fastqc_data.txt";
                                    #$data{Text}{logo} = "zip.png";
                                    #$data{Text}{path} = "$folder.zip";
                                    #$data{Text}{src}  = "$folder";    
                                    #$data{Text}{type} = "link";
                                } else {
                                    print "NOT $iline\n";
                                    exit 1;
                                };
                            }
                        } else {
                            last;
                        }
                    } # end while iline
                } # end if div summary
                
                if ( $line =~ /\<div class\=\"module\"\>\<h2 id\=\"M(\d+)\"\>/ ) {
                    my $M    = $1;
                    my $name = $ms[$M];
                    next if ! defined $name;
                    while ( my $iline = <FI> ) {
                        if ( $iline !~ /^\<\/div\>/ ) {
                            chomp $iline;
                            #$iline =~ s/src=\"/src=\"$link\//;
                            #$iline =~ s/\<img/\<img width=640 height=480/;
                            if ($iline =~ /src=\"(\S+)\"/ )
                            {
                                my $img = $1;
                                $data{$name}{type}           = "image";
                                $data{$name}{content}{image} = "$link/$img";
                                my @sizes = split(/\s+/, `identify -format '%w %h\n' $folder/$img`);
                                $data{$name}{content}{size} = \@sizes if ( scalar @sizes );
                                
                                #print "    NAME    ",$name,"\n";
                                #print "      TYPE  ",$data{$name}{type}   ,"\n";
                                #print "      CONT  ",$data{$name}{content}{image},"\n";
                                #print "ADDING $iline TO LC $lc\n";
                            } else {
                                $data{$name}{content}{text} .= "$iline\n";
                            }
                        } else {
                            last;
                        }
                        
                        if ( exists $data{$name} && exists ${$data{$name}}{content} && exists ${$data{$name}{content}}{text} )
                        {
                            #print "    NAME    ",$name,"\n";
                            #print "      TYPE  ",$data{$name}{type}   ,"\n";
                            #print "      CONT  ",$data{$name}{content}{text},"\n";
                        }
                    } # end while iline
                } # end if div module h2
            }
            close FI;
            
            return \%data;
        } else {
            print "COULD NOT OPEN FILE $folder/fastqc_report.html: $!\n";
            return "";
        }
    } else {
        print "FOLDER $folder OR FILE $folder/fastqc_report.html DOES NOT EXISTS\n";
        return "";
    }
    
    
    #<div class="summary">
    #<h2>Summary</h2>
    #<ul>
    #<li><img src="Icons/tick.png" alt="[PASS]"> <a href="#M0">Basic Statistics</a></li>
    #<li><img src="Icons/error.png" alt="[FAIL]"> <a href="#M1">Per base sequence quality</a></li>
    #<li><img src="Icons/tick.png" alt="[PASS]"> <a href="#M2">Per sequence quality scores</a></li>
    #<li><img src="Icons/error.png" alt="[FAIL]"> <a href="#M3">Per base sequence content</a></li>
    #<li><img src="Icons/warning.png" alt="[WARNING]"> <a href="#M4">Per base GC content</a></li>
    #<li><img src="Icons/tick.png" alt="[PASS]"> <a href="#M5">Per sequence GC content</a></li>
    #<li><img src="Icons/tick.png" alt="[PASS]"> <a href="#M6">Per base N content</a></li>
    #<li><img src="Icons/tick.png" alt="[PASS]"> <a href="#M7">Sequence Length Distribution</a></li>
    #<li><img src="Icons/tick.png" alt="[PASS]"> <a href="#M8">Sequence Duplication Levels</a></li>
    #<li><img src="Icons/tick.png" alt="[PASS]"> <a href="#M9">Overrepresented sequences</a></li>
    #<li><img src="Icons/warning.png" alt="[WARNING]"> <a href="#M10">Kmer Content</a></li>
    #</ul>
    #</div>


    #<div class="module"><h2 id="M1"><img src="Icons/error.png" alt="[FAIL]"> Per base sequence quality</h2>
    #<p><img class="indented" src="Images/per_base_quality.png" alt="Per base quality graph"></p>
    #</div>


}


1;


#sub loadLocalDb
#{
#    if ( -f "$dbNameLocal" )
#    {
#        print "LOCAL DB '$dbNameLocal' EXISTS\n";
#        if ( loadXML($dbNameLocal, \%localDb) )
#        {
#            print "  LOCAL DB '$dbNameLocal' READ\n";
#        } else {
#            print "  COULD NOT READ LOCAL DB\n";
#            #do nothing
#        }
#    } else {
#        print "LOCAL DB '$dbNameLocal' DOES NOT EXISTS\n";
#    }
#}

#sub saveXML
#{
#    my $fn   = shift;
#    my $ds   = shift;
#    my $dump = new XML::Dumper;
#    
#    unlink($fn);
#
#    $dump->dtd;
#    $dump->pl2xml( $ds, $fn );
#
#    if ( ! -f "$fn" )
#    {
#        die "NOT ABLE TO CREATE XML '$fn'";
#    } else {
#        return 1;
#    }
#}
#
#sub loadXML
#{
#    my $fn   = shift;
#    my $ds   = shift;
#    my $dump = new XML::Dumper;    
#    
#    if ( -f "$fn" )
#    {
#        open XML, "<$fn" or die;
#        my @xml = <XML>;
#
#        close XML;
#        return 0 if ! length @xml;
#        
#        my $xml = join("", @xml);
#        return 0 if ! length $xml;
#
#        my $ref = \$dump->xml2pl( $xml );
##        print Dumper($ref);
##        print Dumper($ds);
#        %{$ds} = %{${$ref}};
##        print Dumper($ds);
#        return 1;
#    } else {
#        return 0;
#    }
#}
#


    #Pennellii_Illumina.histo
    #Pennellii_Illumina.histo.desc
    #Pennellii_Illumina.histo.png
    #Pennellii_Illumina.jf
    #Pennellii_Illumina.jf.sort
    #Pennellii_Illumina.stats
    #Pennellii_Illumina_matepair_5000.histo
    #Pennellii_Illumina_matepair_5000.histo.desc
    #Pennellii_Illumina_matepair_5000.histo.png
    #Pennellii_Illumina_matepair_5000.jf
    #Pennellii_Illumina_matepair_5000.stats
    #Pennellii_Illumina_matepair_5000_s_5_1_sequence.fastq.histo
    #Pennellii_Illumina_matepair_5000_s_5_1_sequence.fastq.histo.desc
    #Pennellii_Illumina_matepair_5000_s_5_1_sequence.fastq.histo.png
    #Pennellii_Illumina_matepair_5000_s_5_1_sequence.fastq.jf
    #Pennellii_Illumina_matepair_5000_s_5_1_sequence.fastq_mer_counts_0
    #Pennellii_Illumina_matepair_5000_s_5_1_sequence.fastq.nfo
    #Pennellii_Illumina_matepair_5000_s_5_1_sequence.fastq.stats
    #Pennellii_Illumina_matepair_5000_s_5_2_sequence.fastq_mer_counts_0
    #Pennellii_Illumina_matepair_5000_s_5_2_sequence.fastq.nfo
    #Pennellii_Illumina_matepair_5000_s_5_2_sequence.fastq.stats
    #Pennellii_Illumina_matepair_5000_s_6_1_sequence.fastq.histo
    #Pennellii_Illumina_matepair_5000_s_6_1_sequence.fastq.histo.desc
    #Pennellii_Illumina_matepair_5000_s_6_1_sequence.fastq.histo.png
    #Pennellii_Illumina_matepair_5000_s_6_1_sequence.fastq.jf
    #Pennellii_Illumina_matepair_5000_s_6_1_sequence.fastq_mer_counts_0
    #Pennellii_Illumina_matepair_5000_s_6_1_sequence.fastq.nfo
    #Pennellii_Illumina_matepair_5000_s_6_1_sequence.fastq.stats
    #Pennellii_Illumina_matepair_5000_s_6_2_sequence.fastq_mer_counts_0
    #Pennellii_Illumina_matepair_5000_s_6_2_sequence.fastq.nfo
    #Pennellii_Illumina_matepair_5000_s_6_2_sequence.fastq.stats
