#!/bin/bash
SPP=$1
DATE=`date +%Y_%m_%d_%H_%M_%S`
BASE=/home/aflit001/public_html_acessories
TMP=$BASE/get
SRC=$BASE/src
ZIP=$SRC/zip
DST=$ZIP/$SPP

if [[ -z "$SPP" ]]; then
    echo "NO SPECIES DEFINED"
    exit 9
fi

if [[ ! -d "$TMP" ]]; then
    echo "NO TMP FOLDER $TMP"
    exit 8
fi

cd $TMP

if [[ -d "$SPP" ]]; then
    rm -rf $SPP
fi

mkdir $SPP

if [[ ! -d  "$SPP" ]]; then
    echo "ERROR CREATING DIR $SPP"
    exit 9
fi

if [[ ! -d "$SRC" ]]; then
    mkdir $SRC
fi

if [[ ! -d "$ZIP" ]]; then
    mkdir $ZIP
fi

if [[ ! -d "$DST" ]]; then
    mkdir $DST
fi

        #--continue \
        #--tries=4 \
        #--timeout=4 \
        #--read-timeout=6 \
        #--ignore-length \

cd $SPP
wget    --convert-links \
        --page-requisites \
        --recursive \
        --http-user=ab --http-password=quality \
        --adjust-extension \
        --restrict-file-names=windows \
        --no-clobber \
        --no-parent \
        --force-html \
        --reject rejlist "*.zip" \
        --base="http://127.0.0.1/~aflit001/cgi-bin/" \
        --cut-dirs=1 \
        --input-file="http://127.0.0.1/~aflit001/cgi-bin/INGSQR.cgi?spp=$SPP"

#     --no-host-directories  \
#     --no-directories \

if [[ ! -d '127.0.0.1' ]]; then
    echo "ERROR DOWNLOADING. no base folder"
    exit 2
fi

cd 127.0.0.1/cgi-bin

if [[ ! -f "INGSQR.cgi@spp=$SPP.html" ]]; then
    echo "ERROR DOWNLOADING. NO INDEX FILE"
    exit 3
fi
mv "INGSQR.cgi@spp=$SPP.html" index_orig.html

cat index_orig.html | perl -ne 's/\/\~aflit001\/cgi-bin\///; print' > index_fix1.html

cat index_fix1.html | perl -ne 'BEGIN { $s=0 } $s=1 if (/\<tr class\=\"grandtitle\"\>/);  $s=0 if (/\<tr class\=\"grandtitle grandtitle_list\"\>/); next if $s; next if /class\=\"h4\"/; print' > index_fix2.html

#                   getter.cgi@src=src%2Fsolexaqa%2FPennellii%2FPennellii_Illumina_pairedend_300%2FEAS517_0028_PEFC42U9FAAXX_s_4_2_sequence.fastq.png
#/~aflit001/cgi-bin/getter.cgi?src=src/fastqc/Pennellii/Pennellii_Illumina_matepair_3000/ILLUMINA-3A188F-110725_reads_lane7_1_fastqc/Images/per_base_n_content.png
cat index_fix2.html | perl -ne 'if ( /(.+?)getter.cgi\?(.+?)\&quot\;(.+)/ ) { print "$1"; my $s = $2; my $t=$3; $s =~ s/\//\%252F/g; $s =~ s/\//\@/g; print "getter.cgi\@$s\&quot\;"; print $t } else { print }' > index.html

#cat index_fix3.html | perl -ne 's/(\<a href\=\"(\S+?)\".+src\=\&quot\;)\S+?(\&quot\;)/\1\2\3/; print' > index.html

cat index.html | perl -ne 's/(src=.)\.\.\//\1/; s/getter\.cgi\@/cgi-bin\/getter\.cgi\@/; s/(href=.)\.\.\//\1/; s/src=\&quot\;getter\.cgi/src=\&quot\;cgi-bin\/getter\.cgi/; print' > ../index.html

#exit 0
cd ..
BASEZIP=$SPP\_$DATE.zip
OUTZIP=$TMP/$BASEZIP
zip  -x \*.zip --move --recurse-paths $OUTZIP *

if [[ ! -f "$OUTZIP" ]]; then
    echo "ERROR GENERATING OUTPUT ZIP $OUTZIP"
    exit 4
fi

if [[ ! -s "$OUTZIP" ]]; then
    echo "ERROR GENERATING OUTPUT ZIP $OUTZIP. SIZE 0"
    exit 5
fi





cd ../..
rm -rf $SPP/

if [[ ! -f "$OUTZIP" ]]; then
    echo "ERROR GENERATING OUTPUT ZIP $OUTZIP"
    exit 4
fi

if [[ ! -s "$OUTZIP" ]]; then
    echo "ERROR GENERATING OUTPUT ZIP $OUTZIP. SIZE 0"
    exit 5
fi




mv $BASEZIP $DST

if [[ ! -f "$DST/$BASEZIP" ]]; then
    echo "ERROR MOVING FILE TO DESTINY $DST";
    exit 8
fi



echo "finished successfully saved to $DST/$BASEZIP"
