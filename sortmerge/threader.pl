#!/usr/bin/perl -w
use strict;
#reset; ./threader.pl 2 dna 'wc -l'           sampleinput.sort.bz2
#reset; ./threader.pl 2 dna './kmerDb.py put' sampleinput.sort.bz2
my $runForReal =   1;
my $sleepTime  = 120;

my @inputMapper = (
                   #['\.fastq$'     , \&fastq],
                   #['\.tar\.bz2$'  , \&tarbz2],
                   #['\.fastq\.bz2$', \&fastqbz2],
                   #['\.fastq\.gz$' , \&fastqgz],
                   ['\.sort\.gz$'  , \&Sortgz],
                   ['\.sort\.bz2$' , \&Sortbz2],
                   ['\.sort$'      , \&Sort],
                   #['\.gz$'        , \&gz],
                   #['\.bz2$'       , \&bz2],
                  );

my %bases = (
    'bin' => [0, 1], #binary
    'dec' => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], #decimal
    'dna' => ['A', 'C', 'G', 'T'], #dna
    'dnD' => ['A', 'C', 'G', 'T', 'R', 'Y', 'M', 'K', 'W', 'S', 'B', 'D', 'H', 'V', 'N'], #dna degenerated
    'hex' => ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'], #hexadecimal
    'pro' => ['A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V', 'B', 'Z', 'X'], #protein
);

if ( @ARGV == 0 ) {
    die "NO INPUT GIVEN. eg: reset; ./threader.pl 2 dna './kmerDb.py put' sampleinput.sort.bz2";
}

if ( @ARGV != 4 ) {
    die "WRONG NUMBER OF INPUT. eg: reset; ./threader.pl 2 dna './kmerDb.py put' sampleinput.sort.bz2";
}

my $mapLength  = $ARGV[0]; # NUMBER OF MAP KEYS
my $alphabet   = $ARGV[1]; # WHAT ALPHABET
my $program    = $ARGV[2]; # WHICH PROGRAM TO CALL
my $inputFile  = $ARGV[3]; # INPUT FILE


if ( ! exists $bases{$alphabet} ) {
    die "WRONG ALPHABET GIVEN: valids are: ", join(" ,", keys %bases),"\n";
}

if ( ! -f $inputFile ) {
    die "INPUT FILE $inputFile DOES NOT EXISTS\n";
}

if ( $mapLength !~ /^(\d+)$/ ) {
    die "MAP LENGTH ($mapLength) NOT A NUMBER\n";
}

if ( $mapLength < 1 ) {
    die "MAP LENGTH ($mapLength) TOO SMALL\n";
}

my $alphaDict     = $bases{$alphabet};
my $mapBase       = int(@$alphaDict);
my $splitOptions  = $mapBase ** $mapLength;

my $mapping       = undef;
my $func          = undef;

foreach my $mapper (@inputMapper) {
    my $re  = $mapper->[0];
    my $fun = $mapper->[1];
    if ( $inputFile =~ /$re/i) {
        print "  TESTING RE ",$re," [",$fun,"]\n";
        $mapping = $re;
        $func    = $fun;
        last;
    }
}

if (( ! defined $mapping ) || ( ! defined $func )) {
    die "COULD NOT FIND A SUITABLE MAPPER TO FILETYPE $mapping $func";
}


print "  MAPPING LENGHT : $mapLength\n";
print "  ALPHABET       : $alphabet\n";
print "  MAPPING BASE   : $mapBase\n";

print "  SPLIT OPTIONS  : $splitOptions\n";

print "  PROGRAM        : $program\n";
print "  INPUT FILE     : $inputFile\n";
print "  FILE MAPPING   : $mapping\n";

print "  GENERATING DICTIONARY\n";

my $opts = [];
$opts = &appendItem($opts, $alphaDict, $mapLength);
print "    OPTS: ", scalar(@$opts), " OPTIONS\n\n";

my @cmds;
my $code = $func->($inputFile);


print "  GENERATING RULES\n";
for my $opt ( @$opts ) {
    #my $cmd = "$code | grep -E '^$opt' | $program";
        #real	1m38.227s
        #user	0m57.150s
        #sys	0m31.740s
    #my $cmd = "$code | grep -P '^$opt' | $program";
        #real	1m43.331s
        #user	1m00.710s
        #sys	0m31.620s
    #my $cmd = "$code | grep --mmap -E '^$opt' | $program";
        #real	1m16.262s sample short
        #user	1m00.630s
        #sys	0m33.530s
        #real	8m8.273s sample
        #user	5m34.400s
        #sys	1m41.950s
    my $cmd = "$code | pv -i 20 -w 40 -c -l -N \"$opt B\"| egrep --mmap '^$opt' | pv -i 20 -w 40  -c -l -N \"$opt A\" | $program";
        #real	2m59.246s sample
        #user	5m40.840s
        #sys	1m28.490s
    #my $cmd = "$code | grep --mmap -P '^$opt' | $program";
        #real	1m26.241s
        #user	1m08.380s
        #sys	0m39.540s

    #my $cmd = "$code | perl -ne 'print if /^$opt/' | $program";
        #real	2m54.212s
        #user	1m03.370s
        #sys	0m34.300s

    #--mmap If  possible,  use  the  mmap(2)  system  call  to read input, instead of the default
    #          read(2) system call.  In some situations, --mmap yields better performance.  However,
    #          --mmap  can  cause undefined behavior (including core dumps) if an input file shrinks
    #          while grep is operating, or if an I/O error occurs.
    
    #-E, --extended-regexp
    #          Interpret PATTERN as an  extended  regular  expression  (ERE,  see  below).   (-E  is
    #          specified by POSIX.)
    
    #-P, --perl-regexp
    #          Interpret PATTERN as a Perl regular expression.  This is highly experimental and grep
    #          -P may warn of unimplemented features.

    
    print "    $cmd\n";
    push(@cmds, [$opt, $cmd]); 
}

use threads;
my @thrs;

for my $cmd ( @cmds ) {
    my $thr = threads->create('runCmdAndPrint', @$cmd);
    push @thrs, [$cmd->[0], $cmd->[1], $thr];
}


while ( threads->list() > 0 ) {
    foreach my $thrD (@thrs) {
        my $thrOpt = $thrD->[0];
        my $thrCmd = $thrD->[1];
        my $thr    = $thrD->[2];

        # Check thread's state
        if ($thr->is_running()) {
            print "    THREAD $thrOpt STILL RUNNING\n";
        } else {
            print "    THREAD $thrOpt HAS FINISHED\n";
            if ( ! $thr->is_joinable() ) {
                #print "      THREAD $thrOpt NOT JOINABLE\n";
                sleep(1);
            } else {
                #print "      THREAD $thrOpt JOINABLE\n";
                $thr->join();
                #print "        THREAD $thrOpt JOINED\n";

                print "  REMAINING RUNNING THREADS ",scalar threads->list(),"/",scalar @thrs,"\n";
            }
        }
    }
    print "\n";
    sleep($sleepTime);
}
print "FINISHED SUCCESSFULLY\n";

sub runCmdAndPrint {
    my $opt = shift;
    my $cmd = shift;
    print "  RUNNING OPTION $opt CMD $cmd\n";
    
    if ( $runForReal ) {
        my $FH;
        open ($FH, "$cmd 2>&1 |") or die "COULD NOT RUN $cmd: $!";
        while (<$FH>) {
            print "    ", $opt, " :: ", $_;
        }
        close $FH;
    }
    
    print "  FINISHED OPTION $opt CMD $cmd\n";
}


sub Sortbz2 {
    my $file = shift;
    print "  SORT BZ2 :: $file\n";
    return "pbzip2 -dck $file";
}

sub Sortgz {
    my $file = shift;
    print "  SORT GZ  :: $file\n";
    return "pigz -dc $file";
}



sub Sort {
    my $file = shift;
    print "  SORT     :: $file\n";
    return "cat $file";
}

sub appendItem {
    my $arr  = shift;
    my $dict = shift;
    my $iter = shift;

    my $tmpArr = [];

    if ( $iter > 0 ) {
        print "    ITER ",$iter,"\n";
        if (@$arr == 0)
        {
            for my $d (@$dict) {
                #print "        DICT ",$d,"[ZERO]\n";
                #print "          ARR $d\n";
                push(@$tmpArr, $d);
            }
        } else {
            for my $d (@$dict) {
                #print "          NON ZERO\n";
                #print "        DICT ",$d,"\n";
                for ( my $a = 0; $a < @$arr; $a++ ) {
                    #print "          ARR ",$a," => '",$arr->[$a],"'\n";
                    push(@$tmpArr, $d.$arr->[$a]);
                }
            }
        }
        $tmpArr = &appendItem($tmpArr, $dict, $iter - 1);
        return $tmpArr;
    } else {
        return $arr
    }
}


