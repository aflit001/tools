#ifndef READCONFIG_H
#define READCONFIG_H
// external files
// developed by: RenÃ© Nyffenegger's
// adapted by me
#include "configfile.hpp" //http://www.adp-gmbh.ch/cpp/config_file.html
#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <vector>
#include <list>
using namespace std;

struct configRes
{
    // MERGER
    int    maxLines;      // # MAX NUMBER OF LINES TO ANALYZE (-1 to infinit)
    int    minKmerCount;  // # MINIMUM NUMBER OF APPEARANCES A KMER HAS TO HAVE IN ORDER TO BE INCLUDED
    bool   printLine;     // # PRINT MERGED LINE OR NOT
    int    printEvery;    // # PRINT STATUS EVERY n LINES
    
    // BUCKETIER
    int    kmerLen;       // # KMER LENGTH
    int    numKeys;       // # NUMBER OF KEYS TO SPLIT KMER
    int    lenKeys;       // # LENGHT OF EACH KEY TO SPLIT KMER
    int    maxOpenFiles;  // # MAX NUMBER OF BUCKETS IN MEMORY BEFORE FLUSHING TO FILE
    string baseFolderName;// # FOLDER WHERE TO STORE DATABASE
    string dbName;        // # DATABASE NAME
    string outFolder;     // # OUTPUT FOLDER
    string sppIndexFile;  // # SPECIES INDEX FILE
};


configRes readConfig();

#endif