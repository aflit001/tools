#ifndef VERSION_H
    #define VERSION_H
    
#define PRG_NAME "kmerdb"
#define CREATOR  "Saulo Aflitos"
#define INSTITU  "CBSG/PRI/WUR"
#define LICENSE  "Apache License 2.0"
#define KMERLEN  31

#ifndef VERSION
#define VERSION __DATE__" "__TIME__
#endif

#endif