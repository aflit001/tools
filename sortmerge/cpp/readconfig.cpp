#include "readconfig.hpp"
using namespace std;

configRes readConfig()
{
    string sConfigFile = "kmerdb.cfg";

    ifstream myfile;
    myfile.open(sConfigFile.c_str());

    if ( myfile ) { // file exists
        myfile.close();

        ConfigFile cf(sConfigFile);

        vector<string> sections = cf.getSections();
        int iNumSections        = sections.size();

        configRes config;
        cout << "###########################" << endl;
        cout << "###########SETUP###########" << endl;
        cout << "###########################" << endl;
        for ( int s = 0; s < iNumSections; ++s )
        {
            string sSection = sections[s];
            cout << "  SECTION : " << sSection << endl;

            if ( sSection == "merger" )
            {
                config.maxLines       = (int)    cf.Value(sSection, "maxLines");
                config.minKmerCount   = (int)    cf.Value(sSection, "minKmerCount");
                config.printLine      = (bool)   cf.Value(sSection, "printLine");
                config.printEvery     = (int)    cf.Value(sSection, "printEvery");

                if ( sizeof(config.maxLines     ) == 0 ) { cout << "MAX LINES NOT DEFINED"      << endl; exit(1); }     else { cout << "    MAX LINES        : " << config.maxLines     << endl; };
                if ( sizeof(config.minKmerCount ) == 0 ) { cout << "MIN KMER COUNT NOT DEFINED" << endl; exit(1); }     else { cout << "    MIN KMER COUNT   : " << config.minKmerCount << endl; };
                if ( sizeof(config.printLine    ) == 0 ) { cout << "PRINT LINE NOT DEFINED"     << endl; exit(1); }     else { cout << "    PRINT LINE       : " << config.printLine    << endl; };
                if ( sizeof(config.printEvery   ) == 0 ) { cout << "PRINT EVERY NOT DEFINED"    << endl; exit(1); }     else { cout << "    PRINT EVERY      : " << config.printEvery   << endl; };
            }
            else if ( sSection == "bucketier" )
            {
                config.kmerLen        = (int)    cf.Value(sSection, "kmerLen");
                config.numKeys        = (int)    cf.Value(sSection, "numKeys");
                config.lenKeys        = (int)    cf.Value(sSection, "lenKeys");
                config.maxOpenFiles   = (int)    cf.Value(sSection, "maxOpenFiles");
                config.baseFolderName = (string) cf.Value(sSection, "baseFolderName");
                config.dbName         = (string) cf.Value(sSection, "dbName");
                config.outFolder      = (string) cf.Value(sSection, "outFolder");
                config.sppIndexFile   = (string) cf.Value(sSection, "sppIndexFile");

                if ( sizeof(config.kmerLen        ) == 0 ) { cout << "KMER LENGTH NOT DEFINED"      << endl; exit(1); } else { cout << "    KMER LENGTH      : " << config.kmerLen        << endl; };
                if ( sizeof(config.numKeys        ) == 0 ) { cout << "NUMBER OF KEYS NOT DEFINED"   << endl; exit(1); } else { cout << "    NUMBER OF KEYS   : " << config.numKeys        << endl; };
                if ( sizeof(config.lenKeys        ) == 0 ) { cout << "LENGTH KEYS NOT DEFINED"      << endl; exit(1); } else { cout << "    LENGTH KEYS      : " << config.lenKeys        << endl; };
                if ( sizeof(config.maxOpenFiles   ) == 0 ) { cout << "MAX OPEN FILES NOT DEFINED"   << endl; exit(1); } else { cout << "    MAX OPEN FILES   : " << config.maxOpenFiles   << endl; };
                if ( sizeof(config.baseFolderName ) == 0 ) { cout << "BASE FOLDER NAME NOT DEFINED" << endl; exit(1); } else { cout << "    BASE FOLDER NAME : " << config.baseFolderName << endl; };
                if ( sizeof(config.dbName         ) == 0 ) { cout << "DB NAME NOT DEFINED"          << endl; exit(1); } else { cout << "    DB NAME          : " << config.dbName         << endl; };
                if ( sizeof(config.outFolder      ) == 0 ) { cout << "OUTPUT FOLDER NOT DEFINED"    << endl; exit(1); } else { cout << "    OUTPUT FOLDER    : " << config.outFolder      << endl; };
                if ( sizeof(config.sppIndexFile   ) == 0 ) { cout << "SPP INDEX FILE NOT DEFINED"   << endl; exit(1); } else { cout << "    SPP INDEX FILE   : " << config.sppIndexFile   << endl; };
            } else {
                //string sFirst4 = sSection.substr(0, 4);
                //
                //if ( sFirst4.compare("Task") == 0 ) // starts with task
                //{
                //        cout << "  TAST FOUND: " << sSection << endl;
                //        thresholdData matrixJob;
                //        try {
                //                matrixJob.sName                       = cf.Value(sSection, "sName");                       // pretty name for task
                //                matrixJob.dTaskInitialThreshold       = cf.Value(sSection, "dTaskInitialThreshold");       // initial threshold of worker to perform this job
                //                matrixJob.dTaskStimuliInitialInflow   = cf.Value(sSection, "dTaskStimuliInitialInflow");   // initial ammount of task to be performed
                //                matrixJob.dTaskStimuliInflow          = cf.Value(sSection, "dTaskStimuliInflow");          // increase per unity of time
                //                matrixJob.dTaskWorkerStimuliReduction = cf.Value(sSection, "dTaskWorkerStimuliReduction"); // ammount of reduction each worker induces when working per unity of time
                //                matrixJob.dTaskLearningRate           = cf.Value(sSection, "dTaskLearningRate");           // ammount of learning per unity of time worked
                //                matrixJob.dTaskForgetingRate          = cf.Value(sSection, "dTaskForgetingRate");          // ammount of forgetting per unity of time not worked
                //        }
                //        catch( std::string str ) {
                //                cerr << "Exception raised: " << str << '\n';
                //                exit(1);
                //        }
                //        cout << "ADDING TASK:" << endl;
                //        cout << matrixJob << endl;
                //        config.tMatrix.push_back(matrixJob); // will have id 1
                //}
            }
        }
        return config;
    } else {
        //TODO
        // export standard config file
        cerr << "CONFIG FILE " << sConfigFile << " COULD NOT BE FOUND" << endl;
        exit(1);
    }

    // SAMPLE CONFIG FILE
    /*
    [main]
    iColonySize        =  10
    iNumIterations     = 100
    
    [Task 1]
    # pretty name for task
    sName                       = digging
    dTaskInitialThreshold       =  50       # initial threshold of worker to perform this job
    dTaskStimuliInitialInflow   = 100       # initial ammount of task to be performed
    dTaskStimuliInflow          =  50       # increase per unity of time
    dTaskWorkerStimuliReduction =  10       # ammount of reduction each worker induces when working per unity of time
    dTaskLearningRate           =  10       # ammount of learning per unity of time worked
    dTaskForgetingRate          =  10       # ammount of forgetting per unity of time not worked
    
    [Task 2]
    sName                       = cleaning
    dTaskInitialThreshold       = 100
    dTaskStimuliInitialInflow   = 200
    dTaskStimuliInflow          = 100
    dTaskWorkerStimuliReduction =  20
    dTaskLearningRate           =  20
    dTaskForgetingRate          =  20
    */

}

