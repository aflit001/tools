#ifndef __CONFIG_FILE_H__
#define __CONFIG_FILE_H__
//http://www.adp-gmbh.ch/cpp/config_file.html
#include <string>
#include <map>
#include <vector>
#include <list>


#include "Chameleon.hpp"

class ConfigFile {
  std::map<std::string,Chameleon> content_;
  std::list<std::string> sections_;

public:
  ConfigFile(std::string const& configFile);
  std::vector<std::string> getSections();

  Chameleon const& Value(std::string const& section, std::string const& entry) const;

  Chameleon const& Value(std::string const& section, std::string const& entry, double value);
  Chameleon const& Value(std::string const& section, std::string const& entry, std::string const& value);
};

#endif
