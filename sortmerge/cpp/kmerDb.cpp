#include "kmerDb.hpp"
using namespace std;



// Highly scalable db for short kmer indexing and arbitraryly adding data
// it converts the kmer to binary (2bits/nuc), from binary to decimal and
//   from decimal to hexadecimal as hashing algorithm for storage
// An arbitrary number of charachteres from the hexadecimal representation
//   can then be used as partitioning rule (defined by NUMKEYS and LENKEYS)
// Each file will be inside folders (as defined by NUMKEYS) which (each) 
//   already is the first characters (as defined by LENKEYS) of the sequence
// Inside each file the remaining characters not present in the folder names
//   will be the key for each vaule. The whole sequence can be retrieved by
//   merging the full path + key name, converting from hexadecimal to decimal,
//   from decimal to binary and from binary to sequence
// The value is a base64 representation of the compressed (by snappy) (c)
//   pickle of the array containing the pertinent information.
//
//
//reset; rm -rf  merger/*; ln spp.idx merger/;
//lsof 2>/dev/null | (head -1 && grep kmerDb.py)
//reset; ./threader.pl 2 dna './kmerDb.py put' sampleinput.sort.bz2
//reset; rm -rf  merger/*; ln spp.idx merger/; pbzip2 -dc sym.sort.bz2 | head | ./kmerDb.py put; find merger/db -type f | ./kmerDb.py get; pbzip2 -dc sym.sort.bz2 | head -30 | ./kmerDb.py put; find merger/db -type f | ./kmerDb.py get; pbzip2 -dc sym.sort.bz2 | head | ./kmerDb.py put;find merger/db -type f | ./kmerDb.py get;
//find merger/db -type f -exec ls -l {} \; | perl -nae 'BEGIN { my $s=0; my $c = 0; } END { print "SUM $s COUNT $c AVG ",int($s/$c),"\n" } $c++; $s += $F[4]; if (0) {print $F[4], " sum $s\n";} '
//   SUM 103518233 COUNT 223345 AVG 463
//




int main(int argc, char **argv) {
    string function;
    cout << "getting params" << endl;
    
    getParams(argc, argv, function);
    
    cout << "function defined " << function << endl;
    
    cout << "get config" << endl;
    
    configRes config     = readConfig();
    
    cout << "finished" << endl;
    
    //colony *colonyInst = new colony(config.iColonySize , config.tMatrix);
    
    //CONSTANTS (YOU CAN CHANGE)
    // MERGER
    int    maxLines      = config.maxLines;    // # MAX NUMBER OF LINES TO ANALYZE (-1 to infinit)
    int    minKmerCount  = config.minKmerCount;// # MINIMUM NUMBER OF APPEARANCES A KMER HAS TO HAVE IN ORDER TO BE INCLUDED
    bool   printLine     = config.printLine;   // # PRINT MERGED LINE OR NOT
    int    printEvery    = config.printEvery;  // # PRINT STATUS EVERY n LINES
    
    // BUCKETIER
    int    kmerLen        = config.kmerLen;       // # KMER LENGTH
    int    numKeys        = config.numKeys;       // # NUMBER OF KEYS TO SPLIT KMER
    int    lenKeys        = config.lenKeys;       // # LENGHT OF EACH KEY TO SPLIT KMER
    int    maxOpenFiles   = config.maxOpenFiles;  // # MAX NUMBER OF BUCKETS IN MEMORY BEFORE FLUSHING TO FILE
    string baseFolderName = config.baseFolderName;// # FOLDER WHERE TO STORE DATABASE
    string dbName         = config.dbName;        // # DATABASE NAME
    string outFolder      = config.outFolder;     // # OUTPUT FOLDER
    string sppIndexFile   = config.sppIndexFile;  // # SPECIES INDEX FILE

    //#lenkey 3 numkeys 3 = 4096 folders / level * 2 levels = 16.777.216 (16m) folders.
    //#each folder containing 4096 files = 68.719.476.736 (68b) files
    //#4^31 = 2^62 = 4.611.686.018.427.387.904 possiblilitis
    //#4.611.686.018.427.387.904 / 68.719.476.736 = 67.108.864 (67m) registers per file
    //
    //#lenkey 3 numkey 2 = 4096 folder / level * 1 levels =  4096 (4k) folders
    //# each folder contains 4096 files = 16.777.216 (16m) files
    //#4^31 = 2^62 = 4.611.686.018.427.387.904 possiblilitis
    //#4.611.686.018.427.387.904 / 16.777.216 = 274.877.906.944 (274b) registers per file
    
    if (( numKeys * lenKeys ) > kmerLen ) {
        cout << "invalid set of parameters" << endl;
        cout << "number of keys ("<< numKeys <<") x length of keys ("<< lenKeys <<")" << endl;
        cout << "  is bigger ("<< ( numKeys * lenKeys ) <<") than length of kmer ("<< kmerLen <<")" << endl;
        exit(1);
    }
    
    

}



