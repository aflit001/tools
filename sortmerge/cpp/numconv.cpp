#include "numconv.hpp"

#define BITS 64
using namespace std;

int bin2dec( string binNum ){
    //http://www.codeguru.com/forum/showthread.php?t=372718
   return bitset<BITS>(binNum).to_ulong();
}

string bin2hex ( string binNum ) {
    //http://www.cplusplus.com/forum/beginner/31502/
    stringstream ss;
    ss << hex << bin2dec( binNum );
    return ss.str();
}

string num2hex ( int num ) {
    stringstream ss;
    ss << hex << num;
    return ss.str();
}

template <typename T>  // convert from number to string
string ntos(const T num)
{
        ostringstream ss("");
        ss << num;
        return ss.str();
};

template <typename T> // convert from string to number
T ston(const string str)
{
        T dSub;
        istringstream iss(str);
        iss >> dSub;
        return dSub;
};
