#ifndef KMERDB_H
    #define KMERDB_H

#include <algorithm>
#include <cstdlib>
#include <iomanip>   // formating [p85]
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/filter/gzip.hpp>

//#include <tr1/unordered_set> // use <unordered_set> for more recent g++
//#include <unordered_set> //for more recent g++

#include "readconfig.hpp"
#include "getparams.hpp"


#endif