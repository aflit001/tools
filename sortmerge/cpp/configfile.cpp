#include "configfile.hpp"
//http://www.adp-gmbh.ch/cpp/config_file.html
#include <fstream>
#include <vector>
#include <list>

std::string trim(std::string const& source, char const* delims = " \t\r\n") {
  std::string result(source);
  std::string::size_type index = result.find_last_not_of(delims);
  if(index != std::string::npos)
    result.erase(++index);

  index = result.find_first_not_of(delims);
  if(index != std::string::npos)
    result.erase(0, index);
  else
    result.erase();
  return result;
}

ConfigFile::ConfigFile(std::string const& configFile) {
  std::ifstream file(configFile.c_str());

  std::string line;
  std::string name;
  std::string value;
  std::string inSection;
  int posEqual;
  while (std::getline(file,line)) {

    if (! line.length()) continue;

    if (line[0] == '#') continue;
    if (line[0] == ';') continue;

    if (line[0] == '[') {
      inSection=trim(line.substr(1,line.find(']')-1));
      continue;
    }

    posEqual=line.find('=');
    name  = trim(line.substr(0,posEqual));
    value = trim(line.substr(posEqual+1));

    content_[inSection+'/'+name]=Chameleon(value);
        sections_.push_back(inSection);;
  }
        sections_.sort();
        sections_.unique();
}

Chameleon const& ConfigFile::Value(std::string const& section, std::string const& entry) const {

    try{
        std::map<std::string,Chameleon>::const_iterator ci = content_.find(section + '/' + entry);
        if (ci == content_.end())
        {
          std::string sErrMsg = "either section '" + section + "' does not exist or entry '" + entry + "' does not exists";
          throw sErrMsg;
        } else {
            return ci->second;
        }
    }
    catch( std::string str ) {
        throw str;
    }
}

Chameleon const& ConfigFile::Value(std::string const& section, std::string const& entry, double value) {
  try {
    return Value(section, entry);
  } catch(const char *) {
    return content_.insert(std::make_pair(section+'/'+entry, Chameleon(value))).first->second;
  }
}

Chameleon const& ConfigFile::Value(std::string const& section, std::string const& entry, std::string const& value) {
  try {
    return Value(section, entry);
  } catch(const char *) {
    return content_.insert(std::make_pair(section+'/'+entry, Chameleon(value))).first->second;
  }
}


std::vector<std::string> ConfigFile::getSections()
{
        std::vector<std::string> sKeys;

        //int iNumKeys = (int) sections_.size();

        for (std::list<std::string>::iterator it=sections_.begin(); it!=sections_.end(); ++it)
        {
                sKeys.push_back(*it);
        }
        return sKeys;
}
