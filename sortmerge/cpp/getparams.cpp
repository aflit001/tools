#include "getparams.hpp"

static const int MAX_LINE_SIZE    = 1000;  // max 1000 char per line

using namespace std;
namespace po = boost::program_options;
namespace fs = boost::filesystem;

void checkOutputFile(string outFile, bool force) {
    if ( fs::exists(outFile) ) {
        if ( ! force ) {
            cerr << "    Output file " << outFile << " exists. to replace use --force" << endl;
            exit(1);
        } else {
            cerr << "    Output file " << outFile << " exists. deleting" << endl;
            fs::remove(outFile);
            if ( fs::exists(outFile) ) {
                cerr << "    error removing output file" << endl;
                exit(1);
            }
        }
    }
}


void getParams(int argc, char **argv, string &function) {
    cout << "get params " << argc << endl;
    try {
        boost::program_options::options_description options("Information");
        options.add_options()
            ("help,h",    "Use -h or --help to list all arguments")
            ("version,v", "Use -v or --version to check the version")
            ("function", boost::program_options::value<string>(),
                "Provide function to be executed. either put get or query");
        
        boost::program_options::variables_map vmap;
        boost::program_options::positional_options_description poptd;
        poptd.add("function", 1);
        
        boost::program_options::store(
            boost::program_options::command_line_parser(argc, argv).
            options(options).positional(poptd).run(), vmap);
        boost::program_options::notify(vmap);
        

        if ( vmap.count("help") ) {
            cout << "HELP" << endl;
            cout << "PROGRAM NAME: " << PRG_NAME << endl;
            cout << "VERSION     : " << VERSION  << endl;
            cout << "CREATOR     : " << CREATOR  << endl;
            cout << "FUNDING     : " << INSTITU  << endl;
            cout << "LICENSE     : " << LICENSE  << endl;
            cout << options << endl;
            exit(0);
        } else {
            cout << "no help" << endl;
        }
        
        if ( vmap.count("version") ) {
            cout << "VERSION: " << VERSION << endl;
            exit(0);
        } else {
            cout << "no version" << endl;
        }
        
        if ( vmap.count("function")) {
            function = vmap["function"].as<string>();
            cout << "function " << function << endl;
        } else {
            cerr << "  .no function defined" << endl;
            exit(1);
        }
        cout << "  function    : '" << function << "'" << endl;
    }
    catch(exception& e) {
        cerr << "error: " << e.what() << endl;
        exit(1);
    }
    catch(...){
        cerr << "exception of unknown type!" << endl;
        exit(1);
    }
}

