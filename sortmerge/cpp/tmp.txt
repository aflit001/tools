

//## CHECKING INPUT
if (argc < 1 ){
    print "insuficient parameters";
    print " please inform if get put querySeq";
    sys.exit(2);
}


string function;
if ( sys.argv[1] != None ){
    function = sys.argv[1];
} else {
    print "insuficient parameters";
    print " please inform if get put querySeq";
    sys.exit(3);
}



//## VARIABLES
//# merger
string keyPrev       = '';
string valPrev       = '';
int    gCount        = 0;
int    sCount        = 1;
counterNormal = {};
counterNo1    = {};
patternW      = re.compile("(\w+):(\d+)");
patternW      = re.compile("(?P<spp>\w+):(?P<count>\d+)");
patternR      = re.compile("(?P<sppNum>\d+):(?P<count>\d+)");
infoMapping   = { 'kmerCount': 0 }; // # index mapping for fields

//bucketier
int numLen             = len(str(hex(2**(kmerLen*2))))-1; //# FINAL LENGTH OF NUMERIC REPRESENTATION STRING (hexadecimal)
    //numLen            = len(str(2**(kmerLen*2)))-1;     //# FINAL LENGTH OF NUMERIC REPRESENTATION STRING (decimal)
int strKeys            = (lenKeys*numKeys);               //# TOTAL NUMBER OF KEYS (FOLDERS + FILE)
int cleanOpenFilesNum  = int( maxOpenFiles * .9 );        //# WHEN THERSHOLD OF MAX NUMBER OF BUCKETS IN MEMORY IS ACHIEVED, FLUSH 90% OF THE FILES AT ONCE
string baseFolderPath  = os.path.abspath(os.path.expanduser(baseFolderName));
string baseFolder      = os.path.join(baseFolderPath, dbName);
string outFolder       = os.path.abspath(os.path.join(baseFolder, "db"));
string sppIndexFile    = os.path.abspath(os.path.join(baseFolder, "spp.idx"));


if not os.path.isdir(baseFolder) or not os.path.isdir(outFolder):
    try:
        os.makedirs(outFolder)
    except OSError as (errno, strerror):
        if errno != 17:
            sys.stderr.write("COULD NOT CREATE FOLDER {0} [ ERR # {1} :: {2}][{3} :: makedirs]\n".format(outFolder, errno, strerror, function))
            sys.exit(4)

if not os.path.isdir(outFolder):
    sys.stderr.write("FAILED ON CREATING FOLDER "+outFolder+"[ ERR # "+str(errno)+" : "+strerror+"["+function+" :: checkMakedirs]\n")
    sys.exit(5)

if False:
    sys.stderr.write("{0} :: MAX NUMBER SIZE : {1}\n".format(function, numLen)) # 19
    sys.stderr.write("{0} :: NUMBER KEYS     : {1}\n".format(function, numKeys))
    sys.stderr.write("{0} :: LENGTH KEYS     : {1}\n".format(function, lenKeys))
    sys.stderr.write("{0} :: STRING KEYS     : {1}\n".format(function, strKeys))
    sys.stderr.write("{0} :: MAX OPEN FILES  : {1}\n".format(function, maxOpenFiles))
    sys.stderr.write("{0} :: CLEAN OPEN FILES: {1}\n".format(function, cleanOpenFilesNum))
    sys.stderr.write("{0} :: BASE FOLDER     : {1}\n".format(function, baseFolder))
    sys.stderr.write("{0} :: OUT FOLDER      : {1}\n".format(function, outFolder))
    sys.stderr.write("{0} :: SPP INDEX FILE  : {1}\n".format(function, sppIndexFile))


# RULES TO CONVERT FROM NUCLEOTIDE TO BINARY AND BACK
tb   = { 'A' : '00', 'C' : '01', 'G' : '10', 'T' : '11' }
bt   = { '00':  'A', '01':  'C', '10':  'G', '11':  'T' }


hashKeys     = {}
sppIndex     = {}
sppRevIndex  = {}








def getDbFile(line):
    #line = line.replace("\n", "") # chomp
    line = line.rstrip("\n")
    
    #print function+" :: getdbFile : '"+line+"'"
    keys = line.split("/") #3 not platform safe
    keys = keys[len(keys)-numKeys:]
    #print str(keys)
    
    jointkeys = "".join(keys)
    
    #for itemp in range(0, len(keys)):
    #    item = "".join(keys[0:itemp+1])
    #    print "  ITEM ["+str(itemp)+"] "+item+" SEQ "+getSequenceBack(item + (numLen - ((itemp+1) * lenKeys))*"0")
    
    
    #                          AATAAT TACATG TACCAA ATCAGAATCAAAC		Pimpinelifolium_Illumina:31;AllRound_Illumina:24
    #ITEM [0] 0220         SEQ AATAAT CGCGAT-CCCGCG-GGCCGAAAAAAAA
    #ITEM [1] 02204165     SEQ AATAAT TACATG GATGCA-CAGAGGAGAAAAA
    #ITEM [2] 022041655878 SEQ AATAAT TACATG TACCAA AGTAGAGCTAAAA
    
    if os.path.isfile(line):
        #sys.stderr.write("[o]\n")
        dbI = {}
        fh = open(line, 'r')
        for fline in fh:
            #print "  FLINE '"+fline+"'"
            k      = fline.index("\t")
            fkey   = fline[:k]
            fval   = fline[k+1:-1]
            seqNum = fkey
            #seq    = getSequenceBack(jointkeys+seqNum)
            #seq2   = getSequenceBcat ackOrig(jointkeys+seqNum)
            #print "    SEQNUM "+seqNum+ " JOINT KEYS "+jointkeys
            #print "      SEQ1 '"+seq+"'"
            #print "      SEQ2 "+seq2
            #print "      VAL  '"+fval+"'"
            valNew = cPickle.loads(snappy.uncompress(decodestring(fval)))
            #print "      VALN '"+str(valNew)+"'"
            
            #TODO: REPLACE SPP NUMBER BY NAME
            dbI[seqNum] = valNew
        fh.close()
        return dbI

    else:
        sys.stderr.write("input file " +line+ " does not exists ["+function+"::"+"readLine]\n")
        sys.exit(7);





# figures out the correct partitioning for the requested kmer
# @def genHashKeys(key)
# @param key The key to have the bucket figured out
# @return fileContents The content of the bucket or a empty array
def genHashKeys(key):
    #print function+" :: genHashKeys :: key :"+key
    closeOpenDbs(False)

    Ks = []
    
    for k in range(0, strKeys, lenKeys):
        Ks.append(key[k:k+lenKeys])

    hier = outFolder
    for k in range(0, len(Ks)-1):
        hier = os.path.join(hier, Ks[k])
            
    if not os.path.isdir(hier):
        try:
            os.makedirs(hier)
        except OSError:
            sys.stderr.write("COULD NOT CREATE FOLDER "+hier+"[ ERR # "+errno+" : "+strerror+"["+function+" :: genHashKeys]\n")
            sys.exit(6)
    
    hier = os.path.join(hier, key[strKeys-lenKeys:])
    #print key+" = [PATH: "+hier+"]"
    
    #TODO: READ FILE CONTENTS
    dbi         = {}
    if os.path.isfile(hier):
        #print "READING FILE CONTENT"
        #fileContents = file(hier).read()
        #TODO: perform a binary search to find key instead of loading whole file in memory
        dbi = getDbFile(hier)
        
    hashKeys[key] = [hier, dbi]
    return dbi




# converts from hexadecimal representation to nucleotides
# @def getSequenceBack(nnum)
# @param key The hexadecimal representation of the whole kmer
# @return seq The nucleotide kmer sequence
def getSequenceBack(nnum):
    #print "  NNUM "+nnum
    binLen = (kmerLen*2)
    bina   = bin(int(nnum, 16))[2:]
    bina   = "0"*(binLen-len(bina)) + bina
    #print "  BINA "+bina
    #print "  DEC  "+str(int(nnum, 16))
        
        #===============================================================================================================
        #FORMAT | REPRESENTATION                                                 | STR LENGTH | MEM SIZE | EFF MEM SIZE 
        #---------------------------------------------------------------------------------------------------------------
        #DNA    | AATAATTACATCCTTAAAAAGCCACAATCAC                                | 31         | 248      |              
        #BIN    | 00001100001111000100110101111100000000001001010001000011010001 | 62         |   7      |              
        #DEC    | 220416205509693649                                             | 19         | 152      |              
        #HEXA   | 30f135f002510d1                                                | 16         | 128      |              
        #===============================================================================================================
        #
        #=========================================================================
        #FORMAT | MAX                                                            
        #-------------------------------------------------------------------------
        #HEXA   |     4 000 000 000 000 000
        #DEC    | 4 611 686 018 427 387 904
        #BIN    | 11111111111111111111111111111111111111111111111111111111111111
        #=========================================================================

    seq = ''
    for i in range(0, len(bina), 2): seq += bt[bina[i:i+2]]

    #if verbose: print "\tDECODE6 NUM '%020d' NEWNUM '%020d' SEQ '%31s' NSEQ '%31s' NUC '%31s'" % (nnum, nnum, seq, '', seq)
    return seq





#load species number indexing file
# @def loadSppIndex()
def loadSppIndex():
    #sys.stderr.write("LOADING SPECIES INDEX: '"+sppIndexFile+"'\n")
    if os.path.isfile(sppIndexFile):
        f = open(sppIndexFile, 'r')
        for line in f:
            line = line.rstrip("\n")
            #print line
            if len(line) == 0   : continue
            if line[0]   == '#' : continue
            vals = line.split("\t")
            if len(vals) != 2:
                sys.stderr.write("ERROR LOADING SPECIES INDEX: '"+sppIndexFile+"'\n")
                sys.stderr.write("  ERROR IN LINE: '"+line+"'\n")
                sys.exit()
            #sys.stderr.write("  SPP: "+vals[0]+" ID:"+vals[1]+"\n")
            sppIndex[vals[0]]    = vals[1]
            sppRevIndex[vals[1]] = vals[0]
        f.close()


#save species number indexing file
# @def saveSppIndex()
#def saveSppIndex():
#    sys.stderr.write("SAVING SPECIES INDEX: "+sppIndexFile+"\n")
#    f = open(sppIndexFile, 'w')
#    for k,v in sppIndex.iteritems():
#        f.write("%s\t%s\n" % (k,v))
#    f.close()






def closeOpenDbs(last):
    if ((len(hashKeys) >= maxOpenFiles) or (last)):
        #sys.stderr.write("[c]\n")
        keys = hashKeys.keys()
        #TODO: POSSIBLY SLOW
        if not last:
            keys.sort()
        #sys.stderr.write(function+" :: cleaning remaining open files\n")
        #print function+" :: KEYS: "+str(keys)
        for k in range(0, len(keys)):
            closeK = keys[k]
            #print "  "+function+" :: CLOSING KEY: "+closeK
            fn  = hashKeys[closeK][0]
            dbi = hashKeys[closeK][1]
            #print "    "+function+" :: FN: "+fn
            del hashKeys[closeK]
            
            dbikeys = dbi.keys()
            dbikeys.sort()
            fh = open(fn, 'w')
            for key in dbikeys:
                val = dbi[key]
                valC = encodestring(snappy.compress(cPickle.dumps(val, -1))).replace("\n", "")
                #print function+" :: dbi key  : "+key
                #print function+" :: dbi val  : "+str(val)
                #print function+" :: dbi valC : "+valC
                fh.write("%s\t%s\n" % (key, valC))
            fh.close()







#sys.stderr.write(function+" :: waiting for stdin\n\n")
lineCount = 0
loadSppIndex()
#        fh.close()

if   (function == "get"):
    for line in sys.stdin:
        #print function+" :: line : "+line
        #line    = line.replace("\n", "")
        line    = line.rstrip("\n")
        #print function+" :: line : "+line
        line    = line.replace("/", "") # not portable
        #print function+" :: line     : "+line
        #print function+" :: length   : "+str(lenKeys * numKeys)
        #print function+" :: len Line : "+str(len(line))
        #print function+" :: start    : "+str(len(line) - (lenKeys * numKeys) + 1)
        key     = line[len(line) - (lenKeys * numKeys):]
        #print function+" :: key      : "+key
        
        dbi     = None
        if key not in hashKeys:
            dbi = genHashKeys(key)
        else:
            dbi = hashKeys[key][1]

        dbikeys = dbi.keys()
        dbikeys.sort();
        for seq in dbikeys:
            wholeSeq = key+seq
            seqOrig  = getSequenceBack(wholeSeq)
            val = dbi[seq]
            print "%s\t%s" % (seqOrig, str(val))
            
    closeOpenDbs(True)
    
    
    
    
    
elif (function == "put"):
    start    = time.time()
    timeLast = start
    for line in sys.stdin:
        if printLine == True:
            lineCount += 1
            #print "put :: line : "+line
            k         = line.index("\t")
            keyO      = line[:k]
            val       = line[k+1:-1]
            key       = ''.join( tb[ch] for ch in keyO[0:] )
            key       = str(hex(int(key, 2))[2:]).zfill(numLen)

            if ( not (lineCount % printEvery) ):
                curTime   = time.time()
                ela       = curTime - start
                elaLast   = curTime - timeLast
                timelast  = curTime
                speed     = lineCount  / ela
                speedLast = printEvery / elaLast
                sys.stderr.write("[%12d] (%s) %s %30s GLOBAL {%6d s | %8.4f lines/s} LOCAL {%6d s | %8.4f lines/s}\n" % (lineCount, key, keyO, val, ela, speed, elaLast, speedLast))
            
            #TODO: TEST! ALLOWS PARTIAL MATCH
            #TODO: NEEDS TO FIX GETBACK
            #hk        = ''
            #print function+" :: key B      : "+key
            #for p in range(0, len(key), 4):
            #    piece = key[p:p+4]
            #    print function+" ::   piece    : "+str(piece)
            #    h   = hex(int(piece, 2))
            #    print function+" ::   h        : "+str(h)
            #    hk += str(h[2:])
            #    print function+" ::   hk       : "+hk
            #key       = hk.zfill(numLen)
            

            
            fileKey   = key[0:strKeys]
            keyRemain = key[strKeys:]
            dbi       = None

            #print function+" :: key orig   : "+keyO
            #print function+" :: key bin    : "+''.join( tb[ch] for ch in keyO[0:] )
            #print function+" :: key dec    : "+str(int(''.join( tb[ch] for ch in keyO[0:] ), 2))
            #print function+" :: key hex    : "+key
            #print function+" :: file key   : "+fileKey
            #print function+" :: key remain : "+keyRemain
            #print function+" :: val        : "+val
            #print function+" :: val back   : "+getSequenceBack(key)+"\n"
            if fileKey not in hashKeys:
                dbi = genHashKeys(fileKey)
            else:
                dbi = hashKeys[fileKey][1]
            #print function+" :: in dbi    : "+str(dbi)
            
            if keyRemain not in dbi:
                dbi[keyRemain] = {}
            dbL = dbi[keyRemain]
            
            for frag in patternW.finditer(line):
                spp = frag.group('spp')
                cou = frag.group('count')
                #print "FRAG SPP "+spp+" COUNT "+cou
                if (spp in sppIndex):
                    sppNum  = sppIndex[spp]
                    #if len(valNew) > 0: valNew += ";"
                    #valNew += str(sppNum)+":"+cou
                    valL = []
                    valL.insert(infoMapping['kmerCount'], cou)
                    dbL[sppNum] = valL
                else:
                    sys.stderr.write("UNKNOWN SPECIES " +spp+ "\n")
                    sys.exit()
                    #print "NEW SPP "+spp
                    #sppNum              = len(sppRevIndex)
                    ##print "  SPP NUM "+str(sppNum)
                    #sppIndex[spp]       = sppNum
                    #sppRevIndex.append(spp)
                    ##if len(valNew) > 0: valNew += ";"
                    ##valNew += str(sppNum)+":"+cou
                    #valL = []
                    #valL.insert(infoMapping['kmerCount'], cou)
                    #dbL[sppNum] = valL
                #valPrev = replace(valPrev, k, v)
            #print function+" :: dbl       : "+str(dbL)
            #print function+" :: out dbi   : "+str(dbi)
            
            #print valNewC
            #fa.append("%s\t%s\n" % (keyRemain, valNewC))
            
    closeOpenDbs(True)
    ela   = time.time() - start
    speed = lineCount / ela
    sys.stderr.write("FINISHED %12d REGISTERS in %6d s WITH AVG SPEED %8.4f lines/s\n" % (lineCount, ela, speed))
        
        
        
        
        
elif (function == "querySeq"):
    #TODO: ACCEPT FILE WITH SEQUENCES
    if len(sys.argv) < 3:
        sys.stderr.write("no sequence given\n")
        sys.exit(8);
        
    seqs = sys.argv[2:]
    for seq in seqs:
        if len(seq) != kmerLen:
            sys.stderr.write("wrong sequence length\n")
            sys.exit(9);
            
        key       = ''.join( tb[ch] for ch in seq[0:] )
        key       = str(hex(int(key, 2))[2:]).zfill(numLen)
        
        fileKey   = key[0:strKeys]
        keyRemain = key[strKeys:]
        dbi       = None
        
        if fileKey not in hashKeys:
            dbi = genHashKeys(fileKey)
        else:
            dbi = hashKeys[fileKey][1]
        #print function+"  :: in dbi    : "+str(dbi)
        
        if keyRemain not in dbi:
            sys.stderr.write("no record for "+seq+"\n")
        else:
            dbL      = dbi[keyRemain]
            seqOrig  = getSequenceBack(key)
            print "%s\t%s" % (seqOrig, str(dbL))
        
    closeOpenDbs(True)
    
    
    
    
    
    
elif (function == "queryFrag"):
    #TODO: all
    pass
    if len(sys.argv) < 3:
        sys.stderr.write("no sequence given\n")
        sys.exit(8);
        
    seq = sys.argv[2]

    if len(seq) != kmerLen:
        sys.stderr.write("wrong sequence length\n")
        sys.exit(9);
        
    key       = ''.join( tb[ch] for ch in seq[0:] )
    key       = str(hex(int(key, 2))[2:]).zfill(numLen)
    
    fileKey   = key[0:strKeys]
    keyRemain = key[strKeys:]
    dbi       = None
    
    if fileKey not in hashKeys:
        dbi = genHashKeys(fileKey)
    else:
        dbi = hashKeys[fileKey][1]
    #print function+"  :: in dbi    : "+str(dbi)
    
    if keyRemain not in dbi:
        sys.stderr.write("no record for "+seq+"\n")
    else:
        dbL      = dbi[keyRemain]
        seqOrig  = getSequenceBack(key)
        print "%s\t%s" % (seqOrig, str(dbL))
        
    closeOpenDbs(True)
elif (function == "querySpp"):
    pass


else:
    sys.stderr.write("invalid option ["+function+"::"+"checkFunction]\n")
    sys.stderr.write("valid options are get put querySeq\n")
    sys.exit(10);















def getSequenceBackOrig(nnum):
    binLen = (kmerLen*2)
    bina   = bin(int(nnum))[2:]
    bina   = "0"*(binLen-len(bina)) + bina

    seq = ''
    for i in range(0, binLen, 2): seq += bt[bina[i:i+2]]

    #if verbose: print "\tDECODE6 NUM '%020d' NEWNUM '%020d' SEQ '%31s' NSEQ '%31s' NUC '%31s'" % (nnum, nnum, seq, '', seq)
    return seq

