#ifndef GETPARAMS_H
    #define GETPARAMS_H

#include <bitset>
#include <string>
#include <sstream>

#include <string>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include "version.hpp"

using namespace std;

void checkOutputFile(string outFile, bool force);
void getParams(int argc, char **argv, string &function);

#endif