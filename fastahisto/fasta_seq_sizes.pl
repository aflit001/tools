#!/usr/bin/perl -w
use strict;
use File::Basename;
use Cwd 'abs_path';
use FindBin;
use lib $FindBin::Bin;
use fasta;


die "NO INPUT FASTA" if ( ! @ARGV );
my $inFasta    = $ARGV[0];
my $dstFolder  = $ARGV[1];
#./fasta_seq_sizes.pl cbs7750_DE_NOVO_LAST.fasta

die "INPUT FASTA $inFasta DOESNT EXISTS" if ( ! -f $inFasta );

$dstFolder = "./" if ! defined $dstFolder;
die "DESTINATION FOLDER DOES NOT EXISTS" if defined $dstFolder && ! -d $dstFolder;
my $baseName = (fileparse($inFasta))[0];
$dstFolder = abs_path($dstFolder);
my $outTmp = "$dstFolder/$baseName.tmp";
my $outPng = "$dstFolder/$baseName.png";

print STDERR "  INPUT FASTA $inFasta\n";
print STDERR "  OUTPUT TMP  $outTmp\n";
print STDERR "  OUTPUT PNG  $outPng\n";
my $n50 = -1;
my $nIn = -1;

if ( ! -f $outTmp )
{
	print STDERR "    GENERATING CHROMOSSOMES TABLE...\n";
	my $fasta  = fasta->new($inFasta);
	print STDERR "    GENERATING CHROMOSSOMES TABLE...done\n";
	my $stats = $fasta->getStat();

	print STDERR "    READING CHROMOSSOMES\n";
	my $gTotal;
	my $gSize;
	my @sizes;
	foreach my $chrom ( sort keys %$stats )
	{
		my $size    = $stats->{$chrom}{size};
		#print "\tCHROMOSSOME $chrom SIZE $size\n";
		push(@sizes, $size);
		$gTotal++;
		$gSize  += $size;
	}
	print STDERR "    TOTAL :: CHR $gTotal SIZE $gSize\n";

	open  TMP, ">$outTmp" or die "$!";
    
    my $ts = 0;
    my $tc = 0;
    for my $s (sort { $a <=> $b } @sizes)
    {
        $tc++;
        if (( $ts <= ($gSize / 2)) && (($ts + $s) >= ($gSize  / 2)))
        {
            $n50 = $s;
            $nIn = $tc;
        }
        print TMP "$s\n";
        $ts += $s;
    }
    
	close TMP;
} else {
	print STDERR "    CHROMOSOMES SIZES ALREADY IN FILE $outTmp. DELETE IT TO RE-CALCULATE\n";
}


print STDERR "    RENAMING\n";
my $n50Text = <<N50
set label "N50       =$n50" at graph 0.15,0.90
set label "N50 Index =$nIn" at graph 0.15,0.85
N50
;
my @script = <DATA>;
map { s/histogram.png/$outPng/g } @script;
map { s/histogram.dat/$outTmp/g } @script;
map { s/\#n50label/$n50Text/g   } @script if $n50 > 0;

print STDERR "    RENAMING... done\n";

die "TMP FILE $outTmp DOES NOT EXISTS" if ( ! -f $outTmp );
die "TMP FILE $outTmp IS EMPTY"        if ( ! -s $outTmp );#;;;
unlink ($outPng) if ( -f "$outPng" );

print STDERR "    PLOTTING\n";
open (GNUPLOT, "|gnuplot") or die $!;
print GNUPLOT @script;
close GNUPLOT;
print STDERR "    PLOTTING... done\n";

die "PNG FILE $outPng WAS NOT CREATED" if ( ! -f $outPng );
die "PNG FILE $outPng IS EMPTY"        if ( ! -s $outPng );#;;;

unlink($outTmp);

1;

__DATA__
#http://gnuplot-surprising.blogspot.com/2011/09/statistic-analysis-and-histogram.html
reset
# setup
xticks   = 5    # number of ticks in X
minBins  = 35   # minimum number of bins
logbase  = 1.04 # base in which to calculate number of bins ((logN/logB) + 1)
longsize = 2048

# general configuration
set term png enhanced size longsize, (longsize/2)   #output terminal and file
set output "histogram.png"
set title "fasta stats"
set style fill solid 0.5    #fillstyle
set tics out nomirror
set offset graph 0.05,0.05,0.05,0.0

set multiplot layout 1,2
set size   0.3, 1.0
set origin 0.0, 0.0

#function used to map a value to the intervals
c(x)     = ((count=count+1), 0)        #calculate count
s(x)     = ((sum=sum+x),     0)        #calculate sum
ismin(x) = (x<minx)?minx=x:0           #calculate min
ismax(x) = (x>maxx)?maxx=x:0           #calculate max

#variables initialization
count =  0    #initialize count
sum   =  0
maxx  = -1e38 #initialize max
minx  =  1e38 #initialize min




#plot sizes
set xlabel "Sequences"
set ylabel "Length (bp)"
set format y "%0.1te%L"
set xtics rotate
#set xtics 0,200
set xrange[0:*]

#n50label

set title "Sequence Lengths"
plot '<sort -n histogram.dat' using 0:((c($1)*s($1)*ismin($1)*ismax($1))+$1) with boxes lc rgb"green" notitle




sumfar  = 0

#calculate new variables
#logn          = ((log(count)/log(2))+1)         #calculate logn
logn           = ((log(count)/log(logbase))+1)   #calculate logn
nbins          = (logn<minBins)?minBins:(logn)   #number of intervals
width          = (maxx-minx)/nbins               #interval width
hist(x,widt)   = widt*floor(x/widt)+widt/2.0     #calculate histogram bin
sumsofar(x,sf) = ((sumfar=sf+x),   (sumfar/sum*100))   #calculate sum so far

#to put an empty boundary around the
#data inside an autoscaled graph.
#plot histogram
unset label
set size 0.7, 1.0
set origin 0.3, 0.0
#set xtics hist(minx,width),(maxx-minx)/xticks,(hist(maxx,width)+width)
set xlabel  "Size (bp)"
set ylabel  "Frequency"    textcolor rgbcolor "green"
set format x "%0.1te%L"
set yrange  [0:*]
set y2label "Cumm Sizes %" textcolor rgbcolor "red"
set y2range [0:*]
set y2tics textcolor rgbcolor "red"

set boxwidth width*0.9

set label "count=%.0f",count   at graph 0.65,0.90
set label "sum  =%.0f",sum     at graph 0.65,0.85
set label "bins =%.0f",nbins   at graph 0.65,0.80
set label "min  =%.0f",minx    at graph 0.65,0.75
#set label "max  =%.0f",maxx    at graph 0.65,0.70
set label "max  =%.0f",GPVAL_DATA_Y_MAX    at graph 0.65,0.70



set title "Sequence Lengths Histogram"
plot "histogram.dat"            using (hist($1,width)):(1.0)          smooth frequency  axes x1y1 with boxes lc rgb"green" notitle, \
     '<sort -n "histogram.dat"' using (hist($1,width)):(sumsofar($1,sumfar)) smooth bezier axes x1y2 with lines lc rgb"red" title "%Cumm"

#     "" using (sumsofar($1)):(1.0)    smooth cumulative lc rgb"red"
#     ""              using ($1):(1.0)             smooth cumulative axes x1y2 lc rgb"red",\

#plot "histogram.dat" u (hist($1,width)):(1.0) smooth freq w boxes lc rgb"green" notitle
#plot "histogram.dat" u (hist($1,width)):(1.0) smooth freq w histeps lc rgb"green" notitle



