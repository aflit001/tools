#!/usr/bin/perl -w
use strict;
use Getopt::Long;
use Cwd qw(realpath);
use lib "./lib";
use countRaw;
use xml qw( dumper );

my $unwanted;
my $indir;
my $outdir;
my $outfileFolder;
#my $outfileSpp;
#my $outFileTechs;
my $cc;

GetOptions(
           'unwanted:s'      => \$unwanted,
           'u:s'             => \$unwanted,
           'indir:s'         => \$indir,
           'i:s'             => \$indir,
           'outdir:s'        => \$outdir,
           'o:s'             => \$outdir,
           'outfilefolder:s' => \$outfileFolder,
           'f:s'             => \$outfileFolder,
           #'outfilespp:s'    => \$outfileSpp,
           #'s:s'             => \$outfileSpp,
           #'outfiletech:s'   => \$outFileTechs,
           #'t:s'             => \$outFileTechs,
           );


$indir         = "/home/aflit001/nobackup/Data" if ( ! defined $indir         );
$outdir        = "./"                           if ( ! defined $outdir        );
$outfileFolder = "file.xml"                     if ( ! defined $outfileFolder );
#$outfileSpp    = "spp.xml"                      if ( ! defined $outfileSpp    );
#$outFileTechs  = "techs.xml"                    if ( ! defined $outFileTechs  );

die "NO INPUT DIR :: $indir"   if ( ! -d $indir  );
die "NO OUTPUT DIR :: $outdir" if ( ! -d $outdir );

$indir   = realpath($indir);
$outdir  = realpath($outdir);
my $outFileFullFolder = "$outdir/$outfileFolder";
#my $outFileFullSpp    = "$outdir/$outfileSpp";
#my $outFileFullTechs  = "$outdir/$outFileTechs";

die "NO INPUT DIR :: $indir"   if ( ! -d $indir  );
die "NO OUTPUT DIR :: $outdir" if ( ! -d $outdir );

die "COULD NOT FIND UNWANTED FILE $unwanted" if (( defined $unwanted ) && ( ! -f $unwanted ));
$unwanted = '' if ! defined $unwanted;

print "USING
  UNWANTED          : '$unwanted'
  INPUT DIR         : '$indir'
  OUTPUT DIR        : '$outdir'
  OUTPUT FILE FOLDER: '$outFileFullFolder'
";

my $unW          = &readUnwanted($indir, $unwanted);
my $folders      = &getFolders($indir, $unW);
#my $folderXmlOut = &genXmlFolder($indir, $folders);
#&exportXml($outFileFullFolder, $folderXmlOut);

my $techs       = &getTechs($folders);
my $techsXmlOut = &genXmlTech($indir, $techs);
&exportXml($outFileFullFolder,  $techsXmlOut);


#dumper($spps);

print "COMPLETED\n";



sub genXmlFolder
{
    my $dir     = shift;
    my $folderH = shift;
    
    my $xml;
    
    $xml .= '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
    $xml .= "<dataFolders>\n";
    foreach my $folder ( sort keys %$folderH )
    {
        my $nfo       = $folderH->{$folder};
        my $files     = $nfo->{files};
        my $sumSize   = $nfo->{sumSize};
        my $sumReads  = $nfo->{sumReadsCount};
        my $fileTotal = @$files;
        
        $xml .= 
"\t<folder id=\"$folder\" size=\"$sumSize\" reads=\"$sumReads\" files=\"$fileTotal\">
\t\t<folder>$folder</folder>
\t\t<files files=\"$fileTotal\">\n";

        my $fileCount = 0;
        foreach my $file ( sort @$files )
        {
            my $fileName  = $file->{fileName};
            my $fileSize  = $file->{fileSize};
            my $fileReads = $file->{readsCount};
            $xml .= "\t\t\t<file id=\"$fileName\" count=\"" . $fileCount++ . "\" size=\"$fileSize\" reads=\"$fileReads\"/>\n";
        }

        $xml .= "\t\t</files>
\t</folder>\n"
    }
    $xml .= "</dataFolders>\n";
    
    #print $xml;
    return $xml;
}


sub genXmlTech
{
    my $dir          = shift;
    my $techH        = shift;
    my $printGenTech = 0;

    my $xml    =    '<?xml version="1.0" encoding="UTF-8"?>' . "\n" .
                    "<species";
    
    
    
    my $nfos = $techH->{ROOT_NFO};
    my $SPP  = $techH->{SPP};
    foreach my $nfo ( sort keys %$nfos )
    {
        my $nfoData = $nfos->{$nfo};
        $xml .= " $nfo=\"$nfoData\"";
    }
    $xml .= ">\n";
    
    foreach my $spp ( sort keys %$SPP )
    {
        print "SPP: $spp\n" if $printGenTech;
        $xml .= "\t<specie id=\"$spp\"";
        my $TECH    = $SPP->{$spp}{TECH};
        my $sppNfos = $SPP->{$spp}{SPP_NFO};
        
        foreach my $sppNfo ( sort keys %$sppNfos )
        {
            $sppNfos->{$sppNfo} = scalar keys %{$sppNfos->{$sppNfo}} if ( ref $sppNfos->{$sppNfo} eq 'HASH' );
            my $sppNfoData = $sppNfos->{$sppNfo};
            print "  SPP NFO: $sppNfo = $sppNfoData\n" if $printGenTech;
            $xml .= " $sppNfo=\"$sppNfoData\"";
        }
        
        $xml .= ">\n";
        
        foreach my $tech ( sort keys %$TECH )
        {
            my $FOLDER   = $TECH->{$tech}{FOLDER};
            my $techNfos = $TECH->{$tech}{TECH_NFO};
            print "    TECH: $tech\n" if $printGenTech;
            $xml .= "\t\t<technology id=\"$tech\"";
            
            foreach my $techNfo ( sort keys %$techNfos )
            {
                $techNfos->{$techNfo} = scalar keys %{$techNfos->{$techNfo}} if ( ref $techNfos->{$techNfo} eq 'HASH' );
                my $techNfoData = $techNfos->{$techNfo};
                print "      TECH NFO: $techNfo = $techNfoData\n" if $printGenTech;
                $xml .= " $techNfo=\"$techNfoData\"";
            }
            $xml .= ">\n";
            
            foreach my $folder ( sort keys %$FOLDER )
            {
                print "        FOLDER $folder\n" if $printGenTech;
                #my $folderData = $folderHash->{$folder};
                my $folderData  = $FOLDER->{$folder};
                
                my $technology = $folderData->{technology};
                my $library    = $folderData->{library};
                my $libType    = $folderData->{libType};
                my $insertSize = $folderData->{insertSize};
                my $nfo        = $folderData->{files};
                
                my $sumSize       = $nfo->{sumSize};
                my $sumReadsCount = $nfo->{sumReadsCount};
                my $files         = $nfo->{files};
                my $fileTotal     = @$files;
                
                $xml .= 
                "\t\t\t<folder id=\"$folder\" size=\"$sumSize\" reads=\"$sumReadsCount\" files=\"$fileTotal\">\n";
                #"\t\t\t\t<folder>$folder</folder>\n" .
                #"\t\t\t\t<files files=\"$fileTotal\">\n";
    
                my $fileCount = 0;
                foreach my $file ( sort @$files )
                {
                    my $fileName  = $file->{fileName};
                    my $fileSize  = $file->{fileSize};
                    my $fileReads = $file->{readsCount};
                    $xml .= "\t\t\t\t<file id=\"$fileName\" count=\"" . $fileCount++ . "\" size=\"$fileSize\" reads=\"$fileReads\"/>\n";
                }
        
                #$xml .= "\t\t\t\t</files>\n";
                $xml .= "\t\t\t</folder>\n";
            }
            $xml .= "\t\t</technology>\n";
        }
        $xml .= "\t</specie>\n";
    }
    $xml .= "</species>\n";
    
   
    #print $xml;
    return $xml;
}
sub getTechs
{
    my $fol               = shift;
    my $printGetTech      = 0;
    my $printGetTechParse = 0;

    my %tech;
    my %techs;
    my %libTypes;
    my %insertSizes;
    my %libraries;
    my %species;

    foreach my $folder ( sort keys %$fol )
    {
        print "  TECHS :: FOLDER ", $folder, "\n" if $printGetTech;

        #spp          ^(.+?)_.+?(?:_*.*?)_(?:\d+|WGS|PE)
        #pipeline     ^.+?_(.+?)(?:_.+?)*_(?:\d+|WGS|PE)
        #insertSize   ^.+?_.+?(?:_.+?)*_(\d+|WGS|PE)
        #library      ^.+?_.+?_(.+?)_(?:\d+|WGS|PE)

        my $spp;
        my $technology;
        my $libType;
        my $insertSize;
        my $library;
        
        if ( $folder =~ /^(.+?)_(454|Illumina)_(matepair|pairedend|WGS)_(\d+|WGS|PE)/ )
        {
            $spp        = $1;
            $technology = $2;
            $libType    = $3;
            $insertSize = $4;
            print "    TECHS :: SPP: $spp TECHNOLOGY: $technology LIBRARY TYPE: $libType INSERT SIZE: $insertSize\n" if $printGetTech;
        }
        elsif ( $folder =~ /^(.+?)_(454|Illumina)_(.+?)_(matepair|pairedend|WGS)_(\d+|WGS|PE)/ )
        {
            $spp        = $1;
            $technology = $2;
            $library    = $3;
            $libType    = $4;
            $insertSize = $5;
            print "    TECHS :: SPP: $spp TECHNOLOGY: $technology LIBRARY: $library LIBRARY TYPE: $libType INSERT SIZE: $insertSize\n" if $printGetTech;
        }
        elsif ( $folder =~ /^(.+?)_(454|Illumina)_(.+?)_(\d+|WGS|PE)/ )
        {
            $spp        = $1;
            $technology = $2;
            $library    = $3;
            $insertSize = $4;
            print "    TECHS :: SPP: $spp TECHNOLOGY: $technology LIBRARY: $library INSERT SIZE: $insertSize\n" if $printGetTech;
        } else {
            die "COULD NOT PARSE FOLDER NAME $folder";
        }

        my $nfo       = $fol->{$folder};
        my $files     = $nfo->{files};
        my $sumSize   = $nfo->{sumSize};
        my $sumReads  = $nfo->{sumReadsCount};
        my $fileTotal = @$files;

        $tech{SPP}                                                   = {} if ( ! exists   $tech{SPP}                                                      );
        $tech{SPP}{$spp}                                             = {} if ( ! exists ${$tech{SPP}}{$spp}                                               );
        $tech{SPP}{$spp}{TECH}                                       = {} if ( ! exists ${$tech{SPP} {$spp}}{TECH}                                        );
        $tech{SPP}{$spp}{TECH}    {$technology}                      = {} if ( ! exists ${$tech{SPP} {$spp} {TECH}}   {$technology}                       );
        $tech{SPP}{$spp}{TECH}    {$technology}{TECH_NFO}            = {} if ( ! exists ${$tech{SPP} {$spp} {TECH}    {$technology}}{TECH_NFO}            );
        $tech{SPP}{$spp}{TECH}    {$technology}{FOLDER}              = {} if ( ! exists ${$tech{SPP} {$spp} {TECH}    {$technology}}{FOLDER}              );
        $tech{SPP}{$spp}{TECH}    {$technology}{FOLDER}    {$folder} = {} if ( ! exists ${$tech{SPP} {$spp} {TECH}    {$technology} {FOLDER}}   {$folder} );
        $tech{SPP}{$spp}{SPP_NFO}                                    = {} if ( ! exists ${$tech{SPP} {$spp}}{SPP_NFO}                                     );
        
        my $techSpp     = $tech{SPP}{$spp};
        my $techTech    = $techSpp->{TECH};
        my $sppNfo      = $techSpp->{SPP_NFO};
        
        my $techFolder  = $techTech->{$technology}{FOLDER}{$folder};
        my $techSppNfo  = $techTech->{$technology}{TECH_NFO};
        
        if ( defined $technology ) { $techFolder->{technology} = $technology; $techs{$spp}{$technology}{$technology}++       };
        if ( defined $library    ) { $techFolder->{library}    = $library;    $libraries{$spp}{$technology}{$library}++      };
        if ( defined $libType    ) { $techFolder->{libType}    = $libType;    $libTypes{$spp}{$technology}{$libType}++       };
        if ( defined $insertSize ) { $techFolder->{insertSize} = $insertSize; $insertSizes{$spp}{$technology}{$insertSize}++ };
        if ( defined $spp        ) { $techFolder->{species}    = $spp;        $species{$spp}{$technology}{$spp}++            };
        $techFolder->{files} = $nfo;

        map {
            $tech{ROOT_NFO}{technologies}{$_}++;
            $sppNfo->{technologies}{$_}++;
            $techSppNfo->{technologies}{$_}++;
            } keys %{$techs{$spp}{$technology}      };
        map {
            $tech{ROOT_NFO}{libTypes}{    $_}++;
            $sppNfo->{libTypes}{    $_}++;
            $techSppNfo->{libTypes}{    $_}++;
            } keys %{$libTypes{$spp}{$technology}   };
        map {
            $tech{ROOT_NFO}{insertSizes}{ $_}++;
            $sppNfo->{insertSizes}{ $_}++;
            $techSppNfo->{insertSizes}{ $_}++;
            } keys %{$insertSizes{$spp}{$technology}};
        map {
            $tech{ROOT_NFO}{libraries}{   $_}++;
            $sppNfo->{libraries}{   $_}++;
            $techSppNfo->{libraries}{   $_}++;
            } keys %{$libraries{$spp}{$technology}  };
        map {
            $tech{ROOT_NFO}{species}{   $_}++;
            $sppNfo->{species}{   $_}++;
            $techSppNfo->{species}{   $_}++;
            } keys %{$species{$spp}{$technology}  };


        $sppNfo->{files}                  += scalar @$files;
        $sppNfo->{size}                   += $sumSize;
        $sppNfo->{sumReads}               += $sumReads;
        
        $techSppNfo->{files}              += scalar @$files;
        $techSppNfo->{size}               += $sumSize;
        $techSppNfo->{sumReads}           += $sumReads;

        $tech{ROOT_NFO}{files}            += scalar @$files;
        $tech{ROOT_NFO}{size}             += $sumSize;
        $tech{ROOT_NFO}{sumReads}         += $sumReads;
    }

    my $nfos = $tech{ROOT_NFO};
    my $SPP  = $tech{SPP};
    foreach my $nfo ( sort keys %$nfos )
    {
        $nfos->{$nfo} = scalar keys %{$nfos->{$nfo}} if ( ref $nfos->{$nfo} eq 'HASH' );
        my $nfoData = $nfos->{$nfo};
        print "ROOT NFO: $nfo = $nfoData\n" if $printGetTechParse;
    }
    
    foreach my $spp ( sort keys %$SPP )
    {
        print "SPP: $spp\n" if $printGetTechParse;
        my $TECH    = $SPP->{$spp}{TECH};
        my $sppNfos = $SPP->{$spp}{SPP_NFO};
        
        foreach my $sppNfo ( sort keys %$sppNfos )
        {
            $sppNfos->{$sppNfo} = scalar keys %{$sppNfos->{$sppNfo}} if ( ref $sppNfos->{$sppNfo} eq 'HASH' );
            my $sppNfoData = $sppNfos->{$sppNfo};
            print "  SPP NFO: $sppNfo = $sppNfoData\n" if $printGetTechParse;
        }
        
        foreach my $tech ( sort keys %$TECH )
        {
            my $FOLDER   = $TECH->{$tech}{FOLDER};
            my $techNfos = $TECH->{$tech}{TECH_NFO};
            print "    TECH: $tech\n" if $printGetTechParse;
            
            foreach my $techNfo ( sort keys %$techNfos )
            {
                $techNfos->{$techNfo} = scalar keys %{$techNfos->{$techNfo}} if ( ref $techNfos->{$techNfo} eq 'HASH' );
                my $techNfoData = $techNfos->{$techNfo};
                print "      TECH NFO: $techNfo = $techNfoData\n" if $printGetTechParse;
            }
            
            foreach my $folder ( sort keys %$FOLDER )
            {
                print "        FOLDER $folder\n" if $printGetTechParse;
            }
        }
    }
    
    return \%tech;
}





sub getFiles
{
    my $folder        = shift;
    my $printGetFiles = 0;
    
    opendir(my $DIR, $folder) or die "could not open dir '$folder': $!";
    
    my %done;
    my %bad;
    my $doneFile = "$outFileFullFolder.done.tmp";
    my $badFile  = "$outFileFullFolder.bad.tmp";

    if ( -f $doneFile ) { &readTmp($doneFile, \%done); };
    if ( -f $badFile  ) { &readTmp($badFile,  \%bad ); };
    
    open DONE, ">>$doneFile" or die "COULD NOT OPEM TMP FILE $doneFile";
    open BAD , ">>$badFile"  or die "COULD NOT OPEM TMP FILE $badFile";
    
    my @files;
    my $sumSize;
    my $sumReadsCount;
    my $counter = countRaw->new();
    my $formats = $counter->getFormatsStr();
    
    foreach my $file ( sort readdir($DIR) )
    {
        next if $file =~ /^\./;
        my $fileName   = "$folder/$file";
        if (( ! exists $done{$fileName} ) && ( ! exists $bad{$fileName} ))
        {
            print "      FILE $file\n" if $printGetFiles;
            if (( -f "$folder/$file" ) && ( $file =~ /$formats/ ))
            {
                my $fileSize   = -s "$folder/$file";
                my $reads      = $counter->count("$folder/$file");
                
                if ( ! defined $reads )
                {
                    print BAD "$fileName\n";
                    next;
                };
                
                my $readsName  = $reads->{name};
                my $readsSize  = $reads->{size};
                my $readsCount = $reads->{count};
                die "NO NAME"  if ! defined $readsName;
                die "SIZE = 0" if ! defined $readsSize;
                die "NO READS" if ! defined $readsCount;
                
                print "      FILE $file NAME $readsName SIZE $readsSize COUNT $readsCount\n" if $printGetFiles;
                
                if ( $reads )
                {
                    die "ERROR IN LOGIC. FILE SIZE NOT EQUAL $fileSize != $readsSize" if ( $fileSize ne $readsSize );
                    
                    push(@files, {fileName   => $fileName,
                                  fileSize   => $fileSize,
                                  readsCount => $readsCount});
                    $sumSize       += $fileSize;
                    $sumReadsCount += $readsCount;
                    
                } else {
                    die "FILE '$file' NOT A VALID FILE";
                }
                
                $done{$file}++;
                print DONE "$fileName\t$fileSize\t$readsCount\n";
            }
        } else {
            if ( exists $bad{$fileName} )
            {
                print "      SKIPPING FILE $fileName :: BAD OMEN\n" if $printGetFiles;
            } else {
                my $nfo        = $done{$fileName};
                my $fileName2  = $nfo->[0];
                my $fileSize   = $nfo->[1];
                my $readsCount = $nfo->[2];
                
                die if $fileName ne $fileName2;
                die if ! defined $fileName;
                die if ! defined $fileSize;
                die if ! defined $readsCount;

                print "      FILE $file SIZE $fileSize COUNT $readsCount\n" if $printGetFiles;
                
                push(@files, {  fileName   => $fileName,
                                fileSize   => $fileSize,
                                readsCount => $readsCount});
                $sumSize       += $fileSize;
                $sumReadsCount += $readsCount;
            }
        }
    }
    
    closedir $DIR;
    close    DONE;
    close    BAD;
    
    die "NO FILES FOUND" if ! @files;
    
    return {files => \@files, sumSize => $sumSize, sumReadsCount => $sumReadsCount};
}




sub getFolders
{
    my $dir             = shift;
    my $not             = shift;
    my $printGetFolders = 0;
    
    opendir(my $DIR, $indir) or die "could not open dir $indir: $!";
    my %folders;
    foreach my $folder ( sort readdir($DIR) )
    {
        print "    CHECKING FOLDER '$folder' " if $printGetFolders;
        if (( $folder !~ /^\./ ) && ( -d "$indir/$folder" ) &&  ( ! exists ${$not}{$folder} ))
        {
            print "OK\n" if $printGetFolders;
            my $files = &getFiles("$indir/$folder");
            $folders{$folder} = $files;
        } else {
            print "NO\n" if $printGetFolders;
        }
    }
     
    close $DIR;
    
    #print "\t", join("\n\t", @folders), "\n";
    return \%folders;
}



sub readTmp
{
    my $file = shift;
    my $hash = shift;

    open FIL, "<$file" or die "COULD NOT OPEN TMP FILE '$file': $!";
    
    foreach my $line (<FIL>)
    {
        #print $line;
        chomp $line;
        my @fields = split(/\t/, $line);
        #print "\t", join("::", @fields), "\n";
        #die if @fields != 3;
        $hash->{$fields[0]} = \@fields;
    }
    
    close FIL;
    
    #print "        ", ( scalar keys %$hash ), " LINES RETRIEVED FROM TEMPORARY FILE $file\n";
}

sub readUnwanted
{
    my $dir  = shift;
    my $file = shift;
    
    my %hash;
    return \%hash if (( ! defined $file ) || ( $file eq '' ));
    
    open FILE, "<$file" or die "COULD NOT OPEN UNWANTED FILE $file: $!";
    while (my $line = <FILE>)
    {
        chomp $line;
        next if ( substr($line, 0, 1) eq "#" );
        next if ! defined $line;
        next if $line eq '';
        
        if ( -d "$dir/$line" )
        {
            print "  ADDING UNWANTED FOLDER '$dir/$line'\n";
            $hash{$line} = 1;
        } else {
            warn "INVALID ENTRY ON UNWANTED FILE '$file' :: '$line'\n"
        }
    }
    close FILE;
    
    return \%hash;
}

sub exportXml
{
    my $file = shift;
    my $xml  = shift;
    
    print "  REPLACING '$file'\n" if ( -f $file );
    
    open  FILE, ">$file" or die "COULD NOT EXPORT XML TO '$file'";
    print FILE $xml;
    close FILE;
    
    print "  EXPORTED\n";
}1;
