package xml;
require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw( checkMd5 readMd5 getMd5 printOnFile cleanXml parseAtts mergeHash dumper getCurrNodeNfo getParentNfo );

use strict;
use warnings;
use Data::Dumper;
use Digest::MD5 qw(md5);


sub checkMd5
{
    my $cnf = shift;
    my $bkp = shift;
    
    return 0 if ( ! -f $cnf       );
    return 0 if ( ! -f "$cnf.md5" );
    return 0 if ( ! -f $bkp       );
    return 0 if ( ! -f "$bkp.md5" );
    
    my $cnfmd5 = &getMd5($cnf);
    my $cnfold = &readMd5("$cnf.md5");
    my $bkpmd5 = &getMd5($bkp);
    my $bkpold = &readMd5("$bkp.md5");
    
    my $len = length($cnf) > length($bkp) ? length($cnf) : length($bkp);
    $len = $len > 32 ? $len : 32;
    my $lens = "%".$len."s";
    
    printf "        $lens  $lens  $lens\n", 'NAME', 'MD5 FILE', 'MD5 STORE',;
    printf "    CNF $lens  $lens  $lens\n", $cnf,    $cnfmd5,   $cnfold;
    printf "    BKP $lens  $lens  $lens\n", $bkp,    $bkpmd5,   $bkpold;

    
    return 0 if ( $cnfmd5 ne $cnfold );
    return 0 if ( $bkpmd5 ne $bkpold );
    
    return 1;
}

sub readMd5
{
    my $fn = shift;
    return undef if ! -f $fn;
    open(FN, $fn) or die "Can't open '$fn': $!";
    my $md5 = <FN>;
    chomp($md5);
    close FN;
    return $md5;
}

sub getMd5
{
    my $fn = shift;
    return undef if ! -f $fn;
    
    open(FN, $fn) or die "Can't open '$fn': $!";
    binmode(FN);
    my $md5 = Digest::MD5->new->addfile(*FN)->hexdigest;
    #print "\t\tFN $fn MD5 $md5\n";
    close FN;
    return $md5;
}

sub printOnFile
{
    my $file = shift;
    
    open FH, ">$file" or die "COULD NOT OPEN FILE $file: $!";
    print FH @_;
    close FH;    
}

sub cleanXml
{
    $_[0] =~ s/\n+//g;
    $_[0] =~ s/^\s+//g;
    $_[0] =~ s/^\t+//g;
    $_[0] =~ s/\s+$//g;
    $_[0] =~ s/\t+$//g;
    $_[0] =~ s/^\s+$//g;
    return $_[0];
}


sub parseAtts
{
    my $atts     = shift;
    my $thisItem = 0;
    my %hash;
    
    while ($atts->item($thisItem))
    {
        my $att      = $atts->item($thisItem);
        my $attName  = $att->getNodeName();
        my $attValue = &cleanXml($att->getNodeValue());
        
        #print "    ATT: '$attName' = \"$attValue\"\n";
        $hash{"_$attName"} = $attValue;
        $thisItem++;
    }
    return \%hash;
}


sub getCurrNodeNfo
{
    my $cnf        = shift;
    my $nodeAdd    = shift;
    my $doc        = $cnf->getDoc();
    my $rootName   = $cnf->getRootName();
    
    my @nodes      = $doc->findnodes($rootName.$nodeAdd);
    my @nfo;
    
    foreach my $node ( @nodes )
    {
        my $name = $node->getNodeName();
        my $att  = $node->getAttributes();
        my $atts     = &parseAtts($att);
        
        #print "NAME $name\n";
        my %data = ( NAME => $name );
        
       
        foreach my $key ( sort keys %$atts )
        {
            my $value = $atts->{$key};
            $key =~ s/^_//;
            #print " ATTS ", uc($key), " = ", $value, "\n";
            $data{ATT}{$key} = $value;
            #$pipelines{$technologyName}{$pipelineName}{INFO}{$key} = $value;
        }
        
        my $parentNfo = &getParentNfo($node);
        #unshift(@$parentNfo, \%data);
        push(@nfo, $parentNfo);
    }
    #dumper(\@nfo);
    return \@nfo;
}

sub getParentNfo
{
    my $node    = shift;
    my $array   = defined $_[0] ? $_[0] : [];
    my $level   = defined $_[1] ? $_[1] : 1;
    
    if ( ! scalar @$array )
    {
        my $nName = $node->getNodeName();
        return undef if $nName eq '#document';
        my $att   = $node->getAttributes();
        my $atts  = &parseAtts($att) if defined $att;
        $array->[0]{NAME} = $nName;
        
        foreach my $key ( sort keys %$atts )
        {
            my $value = $atts->{$key};
            $key =~ s/^_//;
            #print uc($key), " = ", $value, "; ";
            $array->[0]{ATT}{$key} = $value;
            #$out{ATT}{$key} = $value;
            #$pipelines{$technologyName}{$pipelineName}{INFO}{$key} = $value;
        }
    }
    
    my $aLen    = scalar @$array;
    my @parents = $node->getParentNode;
    my $parent  = $parents[0];
    return undef if ! $parent;
    #my %out;
        
    my $pname  = $parent->getNodeName();
    return undef if $pname eq '#document';
    
    my $patt   = $parent->getAttributes();
    my $patts  = &parseAtts($patt) if defined $patt;
    $array->[$aLen]{NAME} = $pname;
    #$out{NAME} = $pname;
    #$out{LINK} = $parent;
    #print "  "x$level . "p"x$level. "NAME $pname ATTS ";
   
    foreach my $key ( sort keys %$patts )
    {
        my $value = $patts->{$key};
        $key =~ s/^_//;
        #print uc($key), " = ", $value, "; ";
        $array->[$aLen]{ATT}{$key} = $value;
        #$out{ATT}{$key} = $value;
        #$pipelines{$technologyName}{$pipelineName}{INFO}{$key} = $value;
    }
    
    #print "\n";
    
    #$out{PARENT} = &getParentNfo($parent, $level + 1);
    &getParentNfo($parent, $array, $level + 1);
    
    #return \%out;
    return $array;
}


sub mergeHash
{
    my $one = $_[0];
    my $two = $_[1];
    
    map { $one->{$_} = $two->{$_} } keys %$two;
}




sub dumper
{
    my $hash = $_[0];
    my $d = Data::Dumper->new([$hash]);
    $d->Purity(1)->Terse(1)->Deepcopy(1);
    print $d->Dump;
}

1;
