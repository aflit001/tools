package ngs;
require Exporter;
our @ISA       = qw{ Exporter };
our @EXPORT_OK = qw{ new getAvailableTechnologies addFile clipping };
use strict;
use warnings;
use lib "./ngs";
use ngs::illumina;
use ngs::fourfivefour;

my $verbose = 0;

use constant {
    NAME     => 'ngs'
};


sub new {
    my $class = shift;
    my $self  = bless {}, $class;

    my %vars        = @_;
    foreach my $var ( keys %vars )
    {
        $self->{$var} = $vars{$var};
    }

    die if ! exists $self->{specie};
    my $spp       = $self->{specie};      # spp name
    $verbose      = exists $self->{verbose} ?  $self->{verbose} : $verbose;

    print "  ", NAME, " :: CREATING NEW NGS: SPECIE $spp\n" if $verbose;


    $self->{_techs_}      = $self->_initializeTechs();
    $self->{_assemblers_} = $self->_genAssembliesList();

    return $self;
}

sub _initializeTechs
{
    my $self = shift;
    my $spp  = $self->{specie};
    
    my %techs = (
        "454"      => ngs::fourfivefour->new( parent => $self, specie => $spp ),
        "Illumina" => ngs::illumina->new(     parent => $self, specie => $spp  )
    );
    
    return \%techs;
}

sub _genAssembliesList
{
    my $self       = shift;
    
    my %assemblies =
    (
        wgs    => \&wgs,
        filter => \&filter
    );
    
    #$self->dumper(\%assemblies);exit;
    
    return \%assemblies;
}

sub getAvailableTechnologies
{
    my $self = shift;
    my %k;
    
    map { $k{$_}++ } keys %{$self->{_techs_}};
    
    return \%k ;
}

sub getTechnology
{
    my $self = shift;
    my $tech = shift;
    my $techs = $self->{_techs_};
    
    if ( exists ${$techs}{$tech} )
    {
        my $techMod  = $techs->{$tech};
        return $techMod;
    } else {
        die;
    }
}



sub getAvailableAssemblies
{
    my $self = shift;
    my %k;
    
    map { $k{$_}++ } keys %{$self->{_assemblers_}};
    
    return \%k;
}

sub addAssembly
{
    my $self = shift;
    my $in   = shift;
    my $ass  = $self->{_assemblers_};
    
    print "  "x2, $self->getName("ADDING ASSEMBLY STEP") . "\n" if $verbose;
    
    foreach my $pair ( @$in )
    {
        my $stepId    = $pair->[0];
        my $stepParam = $pair->[1];
        print "  "x3, NAME, " :: ID $stepId -> PARAM $stepParam\n" if $verbose;
        die "NO ASSEMBLY '$stepId' DEFINED" if ! exists ${$ass}{$stepId};
        #my $func = $ass->{$assId};
        #$func->($self, $assParam);
        push(@{ $self->{assembly} }, { id => $stepId, param => $stepParam});
    }
    

}

sub getAssembliesSteps
{
    my $self = shift;
    my $assemblies = $self->{assembly};
    
    foreach my $assembly ( @$assemblies )
    {
        print "  "x2 . $self->getName('GET ASSEMBLIES STEPS');
        foreach my $k ( sort keys %$assembly )
        {
            print " " . uc($k) . " = " . $assembly->{$k};
        }
        print "\n";
    }
}

sub compile
{
    my $self = shift;
    my $assemblies = $self->{assembly};
    my $assemblers = $self->{_assemblers_};
    
    foreach my $assembly ( @$assemblies )
    {
        my $assId    = $assembly->{id};
        my $assParam = $assembly->{param};
        
        my $func = $assemblers->{$assId};
        my $res  = $func->($self, $assParam);
        $assembly->{result} = $res;
    }
}

sub exportAssembly
{
    my $self         = shift;
    my $assemblyName = shift;
}

sub exportAssemblies
{
    my $self         = shift;
    
}


sub dumper
{
    my $self = shift;
    my $target = defined $_[0] ? $_[0] : $self;
    
    my $d = Data::Dumper->new([$target]);
    $d->Purity(1)->Terse(1)->Deepcopy(1);
    print $d->Dump;
}






sub getName
{
    my $self = shift;
    my $func = shift;
    if ( defined $func )
    {
        $func = " | $func ::"
    } else {
        $func = "";
    };
    
    return "" . NAME . " | " . $self->{specie} . $func;
}






# *********************************************************
#
# ASSEMBLIES STRATEGIES
#
# *********************************************************
sub wgs
{
    my $self  = shift;
    my $param = shift;
    
    #die if ! defined $param;
    
    print "        WGS PARAM $param\n";# if $verbose;
    return undef;
}

sub filter
{
    my $self  = shift;
    my $param = shift;
    
    #die if ! defined $param;
    
    print "        FILTER PARAM $param\n";# if $verbose;
    return undef;
}

1;
