package xmldereference;
require Exporter;
our @ISA       = qw{Exporter};
our @EXPORT_OK = qw{new getConfFile};

use warnings;
use strict;

our $VERSION   = 1.0;
use XML::DOM;
use XML::DOM::XPath;
use Switch;
use xml qw( checkMd5 readMd5 getMd5 printOnFile cleanXml parseAtts mergeHash dumper );

my $rBackup      = 1; #LOAD COMILED XML IF IT EXISTS
my $printDEF0    = 0; #ORIGINAL
my $printDEF1    = 0; #AFTER FIRST DEREFERENCE
my $printDEF2    = 0; #NONE
my $printDEF3    = 0; #AFTER SECOND DEREFERENCE
my $printDEF4    = 0; #AFTER THIRD DEREFERENCE
my $printDEF5    = 0; #BEFORE MAKING NFO
my $printMEM     = 0; #AFTER MAKING NFO
my $printREAD    = 0; #FINAL

my $printEl      = 0; # print &createElement
my $printSet     = 0; # print &setValue
my $printSetNode = 0; # print &setValueNode
my $printDeRef   = 0; # print &dereference
my $printRun     = 0; # print &runCmd
my $printInsert  = 0; # print &insertXml
my $printCopy    = 0; # print &copyXml
my $printRegex   = 0; # print &runRegex
my $printReplace = 0; # print &replacePairsTags
my $printRec     = 0; # print &recursiveReplaceTags
my $printGetRef  = 0; # print & getReferenceValue
my $printExport  = 0; # print &export
my $printParse   = 0; # print &parse

use constant {
    LOADED => 0,
    DEF1   => 1,
    DEF2   => 2,
    DEF3   => 3,
    DEF4   => 4,
    MEMORY => 5,
    READY  => 6,
};



sub new
{
    my $class       = shift;
    my $self        = bless {}, $class;

    my %vars        = @_;
    
    my $config      = exists $vars{config}     ? $vars{config}     : 'config.xml';
    my $export      = exists $vars{export}     ? $vars{export}     : 1;
    my $exportFile  = exists $vars{exportFile} ? $vars{exportFile} : undef;
    my $readBackup  = exists $vars{readBackup} ? $vars{readBackup} : $rBackup;

    if (( defined $export ) || ( defined $exportFile ))
    {
        $exportFile = defined $exportFile ? $exportFile : "$config.new.xml";
        $export     = 1;
    }

    my $loadNew      = 0;
    if ( $readBackup && &checkMd5($config, $exportFile) )
    {
        print "XML DEREF :: MD5 FOUND. READING PARSED FILE\n";
        $config             = $exportFile;
        $loadNew            = 1;
        $self->{deRef}      = 0;
        $self->load($config);
    } else {
        print "XML DEREF :: MD5 NOT FOUND. READING RAW FILE\n";
        $self->{deRef}      = 1;
        $self->load($config);
    }



    $self->{config}     = $exportFile;
    $self->{exportFile} = $exportFile;

    #print "The root element is '", $root->getNodeName(), "'\n";
    #my @children = $root->getChildNodes();
    #print "There are ", scalar(@children), " child elements.\n";


    foreach my $var ( keys %vars )
    {
        $self->{$var} = $vars{$var};
    }

    $self->{exportFile} = $exportFile;
    $self->{export}     = $export;

    
    if ( $loadNew )
    {
        #print "XML LOADED INSIDE\n";

        $self->{status}     = MEMORY;
        
        $self->traverse() if $printDEF0 || $printDEF1 || $printDEF2 || $printDEF3;
        
        #print "NFO LOADED\n";
        $self->traverse() if $printDEF4 || $printDEF5 || $printMEM || $printREAD;
        $self->{status}     = READY;
        return $self;
    } else {
        #print "XML LOADED INSIDE\n";

        $self->{status}     = LOADED;
        
        $self->traverse() if $printDEF0;
        #FIRST DEREF
        if ( $self->{deRef} )
        {
            $self->{status}     = DEF1;
            dereference($self);
            $self->traverse() if $printDEF1;
        }


        $self->{status}     = DEF2;
        #$self->traverse() if $printDEF2;

        if ( $self->{deRef} )
        {
            $self->{status}     = DEF3;
            dereference($self);
            $self->traverse() if $printDEF3;
        }

        #SECOND DEREF
        if ( $self->{deRef} )
        {
            $self->{status}     = DEF4;
            dereference($self);
            $self->traverse() if $printDEF4;
        }


        # MAKE NFO
        $self->traverse() if $printDEF5;

        $self->{status}     = MEMORY;
        $self->traverse() if $printMEM;

        #EXPORTING
        if ( $export )
        {
            $self->export();
        }

        $self->{status}     = READY;
        $self->traverse() if $printREAD;
    }
    
    die if ! defined $self;
    die if ! exists $self->{status};
    die if ( $self->{status} != READY );
    #$self->traverse();
    return $self;
}

sub getDoc
{
    my $self = shift;
    return $self->{doc};
}

sub getRootName
{
    my $self = shift;
    return $self->{rootName};
}

sub getRoot
{
    my $self = shift;
    return $self->{root};
}

sub load
{
    my $self     = shift;
    my $cfg      = shift;
    my $parser   = new XML::DOM::Parser;
    my $doc      = $parser->parsefile($cfg);
    my $root     = $doc->getDocumentElement();
    my $rootName = $root->getNodeName();
    
    $self->{parser}     = $parser;
    $self->{doc}        = $doc;
    $self->{root}       = $root;
    $self->{rootName}   = $rootName;
}

sub createElement
{
    my $self     = shift;
    my $parent   = shift;
    my $child    = shift;
    
    my $doc      = $self->{doc};
    my $rootName = $self->{rootName};
    
    print "CREATE ELEMENT :: CRETING NODE :: NAME '$child' ON PARENT '$parent'\n" if $printEl;
    
    if ( $parent !~ /$rootName/ )
    {
        $parent = "$rootName/$parent";
    };

    print "  CREATE ELEMENT :: SETTING '$parent/$child'\n" if $printEl;
    
    if ( $doc->exists("$parent") )
    {
        my @nodes = $doc->findnodes("$parent");
        if ( ! @nodes )
        {
            warn "CREATE ELEMENT :: PARENT '$parent' DOES NOT EXISTS\n" ;
            return 0;
        }
        print "    CREATE ELEMENT :: ", scalar @nodes, " NODES AFFECTED\n" if $printEl;
        
        my $success = 0;
        foreach my $node ( @nodes )
        {
            if ( ! $node->exists("$child") )
            {
                print "  CREATE ELEMENT :: CHILD '$child' DOES NOT EXISTS. CREATING.\n" if $printEl;
                my $cEl = $doc->createElement($child);

                $node->appendChild($cEl);
                
                if ( ! $node->exists("$child") )
                {
                    warn "CREATE ELEMENT :: FAILED CREATING NODE '$child' : $!\n\n";
                    #return 0;
                } else {
                    print "CREATE ELEMENT :: CHILD NODE '$child' CREATED SUCCESSIFULLY\n" if $printEl;
                    #return 1;
                    $success++;
                }
    
            } else {
                print "  CREATE ELEMENT :: NAME '$child' EXISTS. SKIPPING.\n" if $printEl;
                #return 0;
            }
        }
        
        return 1 if $success == scalar @nodes;
    } else {
        warn "CREATE ELEMENT :: PARENT '$parent' DOES NOT EXISTS";
        return 0;
    }
}


sub setValue
{
    my $self     = shift;
    my $name     = shift;
    my $value    = shift;
    my $doc      = $self->{doc};
    my $rootName = $self->{rootName};
    
    print "SET VALUE NAME :: SETTING DATA :: NAME '$name' VALUE '$value'\n" if $printSet;
    
    if (( ! $doc->exists($name) ) && ( $doc->exists("$rootName/$name") ))
    {
        $name = "$rootName/$name";
    };

    print "  SET VALUE NAME :: SETTING '$name'\n" if $printSet;
    
    if ( $doc->exists("$name") )
    {
        print "  SET VALUE NAME :: NAME '$name' EXISTS. UPDATING.\n" if $printSet;
    } else {
        print "  SET VALUE NAME :: ERROR : NAME '$name' DOES NOT EXISTS\n";
        return 0;
    }
    
    my @nodes = $doc->findnodes($name);
    if ( ! @nodes ) { print "  SET VALUE NAME :: ERROR : NAME '$name' DOES NOT EXISTS\n" ; return 0 };
    
    
    my $successNode = 0;
    foreach my $node ( @nodes )
    {
        $successNode++ if $self->setValueNode($node, $value);
    }

    if ( $successNode != @nodes )
    {
        print "  SET VALUE NAME :: ERROR : $successNode OUT OF ",scalar @nodes," SUCCESSES\n";
        return 0;
    } else {
        print "  SET VALUE NAME :: SUCCESS IN $successNode OUT OF ",scalar @nodes,"\n" if $printSet;
        return 1;
    }    
}

sub setValueNode
{
    my $self         = shift;
    my $node         = shift;
    my $value        = shift;
    my $doc          = $self->{doc};
    my $rootName     = $self->{rootName};
    
    my $name = $node->getNodeName();
    print "SET VALUE NODE :: SETTING DATA :: NAME '$name' VALUE '$value'\n" if $printSetNode;
    
    if ( $node->getNodeType == ELEMENT_NODE )
    {
        my @children = $node->getChildNodes();

        my $successChildren = 0;
        if ( ! @children )
        {
            my $txt = $doc->createTextNode($value);
            $node->appendChild($txt);
            
            if ( $node->getChildNodes() == 0 )
            {
                print "  SET VALUE NODE :: ERROR : ERROR SETTING DATA: NO CHILD. $!\n";
            }
            elsif ( $self->parseVal($node->getChildNodes()->[0]) ne $value )
            {
                print "  SET VALUE NODE :: ERROR : ERROR SETTING DATA: NO VALUE\n";
            } else {
                print "  SET VALUE NODE :: SUCCESS SETTING DATA\n" if $printSetNode;
                $successChildren++;
            }
        } else {
            for ( my $c = 0; $c < @children; $c++ )
            {
                my $child = $children[$c];
                if ( ! defined $child ) { $successChildren++; next; };
                
                if ( $child->getNodeType == TEXT_NODE )
                {
                    $child->setData($value);
                    if ( $self->parseVal($child) eq $value )
                    {
                        print "  SET VALUE NODE :: SUCCESS SETTING DATA\n" if $printSetNode;
                        $successChildren++;
                    } else {
                        print "  SET VALUE NODE :: ERROR : ERROR SETTING DATA: UPDATE FAILED$!\n";
                    }
                } else {
                    print "  SET VALUE NODE :: ERROR : CHILD NOT A TEXT NODE\n";
                }
            }
        }
        
        if ( $successChildren < @children )
        {
            print "  SET VALUE NODE :: ERROR : $successChildren OUT OF ",scalar @children," SUCCESSES : NODE $name VALUE $value\n";
            return 0;
        } else {
            print "  SET VALUE NODE :: SUCCESS IN $successChildren OUT OF ",scalar @children,"\n" if $printSetNode;
            return 1;
        }
    } else {
        print "  SET VALUE NODE :: ERROR : NODE '$name' NOT A ELEMENT NODE\n";
        return 0;
    }
}


sub dereference
{
    my $self       = shift;
    my $root       = $self->{root};

    
    my($node, $level);
    if    ( @_ == 2 ) { ( $node, $level) = ( $_[0],  $_[1] ); }
    elsif ( @_ == 1 ) { ( $node, $level) = ( $_[0],  0     ); }
    elsif ( @_ == 0 ) { ( $node, $level) = ( \$root, 0     ); }
    else              { die };
    

    if ( $$node->getNodeType == ELEMENT_NODE )
    {
        print "\n", "  "x$level, "[DEF :: $level] <", $$node->getNodeName if $printDeRef;
        my $thisItem = 0;
        my $atts     = $$node->getAttributes();
        
        while ($atts->item($thisItem))
        {
            my $att      = $atts->item($thisItem);
            my $attName  = $att->getNodeName();
            my $attValue = &cleanXml($att->getNodeValue());
            print " [DEF :: ATT] '$attName=\"$attValue\"'" if $printDeRef;
            $thisItem++
        }
        print ">" if $printDeRef;
        
        my $el = 0;
        foreach my $child ( $$node->getChildNodes() )
        {
            $el++ if ( $child->getNodeType != TEXT_NODE );
            dereference($self, \$child, $level+1);
        }
        print "\n", "  "x$level, "[$level] " if ($el && $printDeRef);
        print "</", $$node->getNodeName, ">" if $printDeRef;
    }
    elsif ($$node->getNodeType() == TEXT_NODE)
    {
        my $data = &cleanXml($self->parseVal($$node));
        
        if ( defined $data && ($data ne '') )
        {
            print "[DEF :: DATA] '$data'" if  $data && $printDeRef;
            
            #print "\n" if $printDeRef;
            
            my $newData = $self->replacePairsTags($data, $node);
            
            if ( $data ne $newData )
            {
                $$node->setData($newData);
                print "\n", "  "x($level+1), "\n[DEF :: DATA NEW] '$newData'" if  $printDeRef;
            } else {
                print "\n", "  "x($level+1), "\n[DEF :: DATA SAME $data] '$newData'" if  $printDeRef;
            }
        }
    }
    elsif ($$node->getNodeType() == PROCESSING_INSTRUCTION_NODE)
    {
        #print "\n" . "!"x20 . "GOT A PROCESSING INSTRUCTION" . "!"x20 . "\n";
        my $target = $$node->getTarget;
        my $data   = $self->parseVal($$node);
        print "\n", "  "x$level, "[DEF :: $level] <?", $target if $printDeRef;
        print " $data?>" if $printDeRef;
        
        if ( $target eq "CMD" )
        {
            my $cmdRes = &runCmd($self, $data);
            $$node->getParentNode()->removeChild($$node) if $cmdRes;
        }
    }
    
    
    print "\n" if ! $level && $printDeRef;
}

sub runCmd
{
    my $self     = shift;
    my $data     = shift;
    my $status   = $self->{status};
    my $doc      = $self->{doc};
    my $rootName = $self->{rootName};
    
    my @opts = qw (rule type value when);
    my %opts;
    my $opt      = join('|', @opts);
    my $qry      = '('.$opt.')="(.+?)"(?=\s+'.join('=|\s+', @opts).'=|$)';
    #print "OPT '$opt'\nQRY '$qry'\n";
    print"CMD :: DATA '$data'\n" if $printRun;

    while ( $data =~ /$qry/g )
    {
        #print "    WORK :: OPT '$1' VAL '$2'\n" if $printRun;
        $opts{$1} = $2;
    }
    
    $opts{when} = 'begin' if ! exists $opts{when};
    
    if ( lc($opts{when}) eq 'end' )
    {
        if ( $status < DEF4 )
        {
            print "    CMD :: RUNNING LATER : $opts{when}\n" if $printRun;
            return 0 ;
        } else {
            print "    CMD :: RUNNING NOW : $opts{when}\n" if $printRun;
        }
    }
    
    if ( scalar keys %opts == scalar @opts ) 
    {
        print "    CMD :: RUNNING COMAND :: RULE '$opts{rule}' VALUE '$opts{value}' TYPE '$opts{type}' WHEN '$opts{when}'\n" if $printRun;

        switch ( $opts{type} )
        {
            case 'createNode' { return 1 if $self->createElement( $opts{rule}, $opts{value} ); }
            case 'createText' { return 1 if $self->setValue     ( $opts{rule}, $opts{value} ); }
            case 'regex'      { return 1 if $self->runRegex     ( $opts{rule}, $opts{value} ); }
            case 'insert'     { return 1 if $self->insertXml    ( $opts{rule}, $opts{value} ); }
            case 'copy'       { return 1 if $self->copyXml      ( $opts{rule}, $opts{value} ); }
            else              { return 0; }
        }
    }
}

sub insertXml
{
    my $self        = shift;
    my $destiny     = shift;
    my $command     = shift;
    #http://ubuntuforums.org/showthread.php?t=299381
    print "INSERT XML :: DESTINY '$destiny' SOURCE '$command'\n" if $printInsert;
    
    return 0 if ! -f $command;
    return 0 if      $command !~ /\.xml$/i;
    
    eval {
        my $parser      = new XML::DOM::Parser;
        my $doc         = $parser->parsefile($command) or die "$@";
        my $root        = $doc->getDocumentElement()   or die "$@";
        my $rootName    = $root->getNodeName()         or die "$@";
        my $srcChildren = $root->getChildNodes()       or die "$@";
    
        print "\tRUNNING DESTINY '$destiny' COMMAND '$command' ROOT '$root' [$rootName]\n" if $printInsert;
    
        my $dstNodes = $self->getNodes($destiny) or die "$@";
        
        return 0 if ! scalar @$srcChildren;
        return 0 if ! scalar @$dstNodes;
    
        foreach my $srcNode ( @$srcChildren )
        {
            #$srcNode = $srcNode->{link};
            my $srcName = $srcNode->getNodeName() or die "$@";
            print "\tSRC NODE NAME $srcName [$srcNode]\n" if $printInsert;
    
            foreach my $dstNode ( @$dstNodes )
            {
                my $cloneSrcNode = $srcNode->cloneNode('deep');
                if ( ! defined $cloneSrcNode ) { warn "$@"; die; };
                my $dstOwner     = $dstNode->getOwnerDocument();
                if ( ! defined $dstOwner     ) { warn "$@"; die; };
                $cloneSrcNode->setOwnerDocument($dstOwner)     ;# or sub { warn "$@"; die; };
                
                my $dstName = $dstNode->getNodeName() or die "$@";
                print "\t\t\tDST NODE NAME $dstName\n" if $printInsert;
    
                $dstNode->appendChild($cloneSrcNode) or die "$@";
                #print "\t\t\t\tAPPENDING $cloneSrcNode\n";
            }
        }
    };
    
    if ( $@ )
    {
        die "COULD NOT INSERT FILE: $@";
        return 0;
    }

    return 1;    
 
 
    #exit;
    
}

sub copyXml
{
    my $self      = shift;
    my $destiny   = shift;
    my $command   = shift;
    my $doc       = $self->{doc};
    
    print "COPY XML :: DESTINY '$destiny' SOURCE '$command'\n" if $printCopy;
    
    my $srcNodes = $self->getChildren($command);
    my $dstNodes = $self->getNodes($destiny);
    
    return 0 if ! scalar @$srcNodes;
    return 0 if ! scalar @$dstNodes;

    foreach my $srcNode ( @$srcNodes )
    {
        $srcNode = $srcNode->{link};
        my $srcName = $srcNode->getNodeName();
        print "\tSRC NODE NAME $srcName [$srcNode]\n" if $printCopy;

        foreach my $dstNode ( @$dstNodes )
        {
            my $cloneSrcNode = $srcNode->cloneNode('deep');
            my $dstName = $dstNode->getNodeName();
            print "\t\t\tDST NODE NAME $dstName\n" if $printCopy;
            $dstNode->appendChild($cloneSrcNode);
            #print "\t\t\t\tAPPENDING $cloneSrcNode\n";
        }
    }

    return 1;    
}

sub runRegex
{
    my $self       = shift;
    my $destiny    = shift;
    my $command    = shift;
    
    my ($src, $rex) = split(/\<\=\>/, $command);
    if ( ! defined $src || ! defined $rex )
    {
        print "      REGEX :: FAILED RUNNING REGEX DESTINY '$destiny' COMMAND '$command' SRC '$src' REX '$rex'\n";
        return 0;
    };
    
    print "      REGEX :: RUNNING REGEX DESTINY '$destiny' COMMAND '$command' SRC '$src' REX '$rex'\n" if $printRegex;

    my $nodes = $self->getNodes($destiny);
    if ( ! $nodes )
    {
        print "        REGEX :: DESTINY '$destiny' DOES NOT EXISTS\n";
        return 0;
    }
    
    my $nodeSucc = 0;
    foreach my $node ( @$nodes )
    {
        my $srcL = $self->replacePairsTags($src, \$node);
        my $rexL = $self->replacePairsTags($rex, \$node);
        
        if ( ! defined $srcL || ! defined $rexL )
        {
            print "      REGEX :: COULD NOT APPLY REGEX '$rex' TO SOURCE '$src'";
            return 0;
        };
        
        print "        REGEX :: SRC '$srcL' REX '$rexL'\n" if $printRegex;
        
        my $res;
        if ($srcL =~ /$rexL/)
        {
            $res = $1;
            #print "          REGEX :: RES $res\n" if $printRegex;
            $nodeSucc++;
            print "          REGEX :: DESTINY '$destiny' SRC '$srcL' REX '$rexL' RES '$res'\n" if $printRegex;
            $self->setValueNode($node, $res);
        } else {
            print "      REGEX :: COULD NOT APPLY REGEX DESTINY '$destiny' '$rex' TO SOURCE '$src' : SRC '$srcL' REX '$rexL' RES ''\n";
        }
    }
    
    return 1 if $nodeSucc == @$nodes;
    
    print "      REGEX :: FAILED RUNNING REGEX DESTINY '$destiny' COMMAND '$command' SRC '$src' REX '$rex' : DID NOT SUCCESS IN ALL NODES [$nodeSucc == ", (scalar @$nodes),"]\n";
    return 0;
    #foreach my $child ( $node->findnodes($src) )
    #{
    #    my $link   = $child->{link};
    #    
    #    $self->setValue();
    #    my $name   = $link->getName();
    #    my $gChild = $link->getChildNodes()->[0];
    #    next if ! defined $gChild;
    #    
    #    my $dest   = $link->findNodes($src);
    #    my $value  = $self->parseVal($gChild);
    #    print "    NAME $name VALUE $value\n" if $printRegex;
    #    
    #    #return 1 if $self->setValue($dest, $value);
    #    return 1;
    #}
    

    
}

sub replacePairsTags
{
    my $self         = shift;
    my $dataOrig     = shift;
    my $node         = shift;
    my $level        = defined $_[0] ? $_[0] : 0;
    my $data         = $dataOrig;

    for (my $n = 5; $n > 1; $n--)
    {
        my $q = '$'x$n;
        my $d = '#'x$n;
        #print "$q $d\n";
        $data =~ s/\Q$q\E/$d/g;
    }
    
    my $nodeName     = $$node->getNodeName();
    print "\n" if ($printReplace && ! $level);
    print "  "x($level+2) . "[$level] REPLACE PAIR TAGS :: [DATA ORIG] '$data' [NODE NAME $nodeName]\n" if $printReplace;

    my @matches = $data =~ m/\$(.+)\$/g;
    
    if ( ! @matches )
    {
        print "  "x($level+2) . "[$level] REPLACE PAIR TAGS :: TAG NOT FOUND '$data'\n" if $printReplace;
        print "\n" if ($printReplace && ! $level);
        #$data =~ s/\#\#/\$/g;
        
        for (my $n = 5; $n > 1; $n--)
        {
            my $q = '#'x$n;
            my $d = '$'x($n-1);
            #print "$q $d\n";
            $data =~ s/\Q$q\E/$d/g;
        }
        
        return $data
    };
    
    foreach my $ref ( $data =~ m/\$(.+)\$/g )
    {
        print "  "x($level+2) . "[$level] REPLACE PAIR TAGS :: TAG FOUND '$ref'\n" if $printReplace;
        
        my $newValue = $self->recursiveReplaceTags($ref, $node, $level+1);
        
        print "  "x($level+2) . "[$level] REPLACE PAIR TAGS :: DATA '$data' :: REF '$ref' VAL '$newValue'\n" if $printReplace;
        
        $data =~ s/\$\Q$ref\E\$/$newValue/;
        
        print "  "x($level+2) . "[$level] REPLACE PAIR TAGS :: DATA '$data' :: REF '$ref' VAL '$newValue'\n" if $printReplace;
    }
    
    #$data =~ s/\#\#/\$/g;
    for (my $n = 5; $n > 1; $n--)
    {
        my $q = '#'x$n;
        my $d = '$'x($n-1);
        #print "$q $d\n";
        $data =~ s/\Q$q\E/$d/g;
    }
    
    if ( $printReplace )
    {
        if ( $data ne $dataOrig )
        {
            print "  "x($level+2) . "[$level] REPLACE PAIR TAGS :: [DATA NEW ] '$dataOrig' => '$data'\n";
            print "\n" if ($printReplace && ! $level);
        } else {
            print "  "x($level+2) . "[$level] REPLACE PAIR TAGS :: [DATA SAME ] '$dataOrig' => '$data'\n";
            print "\n" if ($printReplace && ! $level);
        }
    }

    return $data;
}

sub recursiveReplaceTags
{
    my $self     = shift;
    my $ref      = shift;
    my $node     = shift;
    my $level    = shift;
    
    #$ref =~ s/\$\$/\#\#/g;
    
    my $refNew   = replacePairsTags( $self, $ref,    $node, $level);
    foreach my $reff ( $refNew =~ m/\$(.+)\$/g )
    {
       $refNew   = recursiveReplaceTags( $self, $reff,    $node, $level); 
    }
    my $refValue = getReferenceValue($self, $refNew, $node);
    
    #$ref =~ s/\#\#/\$/g;
    
    return $refValue;
}

sub getReferenceValue
{
    my $self        = shift;
    my $ref         = shift;
    my $node        = shift;
    my $qry         = $ref;

    my $doc      = $self->{doc};
    my $rootName = $self->{rootName};

    my $val;
    print "        GET REFERENCE VALUE :: GETTING REFERENCE '$qry'\n" if $printGetRef;
    
    eval {
        if ( $$node->exists($qry) )
        {
            print "          GET REFERENCE VALUE :: NODE EXISTS :: $qry\n" if $printGetRef;
            foreach ( $$node->findnodes($qry) )
            {
                $val .= ":" if defined $val;
                $val .= $self->parseVal($_);
                #if ( $_->getNodeType == TEXT_NODE )
                #{
                #    #$val .= $_->getValue();
                #    $val .= $_->getData();
                #}
                #elsif ( $_->getNodeType == ELEMENT_NODE )
                #{
                #    $val .= $self->parseVal($_);
                #}
                #elsif ( $_->getNodeType == ATTRIBUTE_NODE )
                #{
                #    $val .= $_->getValue();
                #} else {
                #    print "UNKNOWN NODE TYPE: ",$_->getNodeType,"\n";
                #}
            }
            print "            GET REFERENCE VALUE :: NODE: '", (defined $val ? $val : ''), "'\n" if $printGetRef;
            $val = $ref if ! defined $val;
        } else {
            print "          GET REFERENCE VALUE :: NODE DOESNT EXISTS\n" if $printGetRef;
        
            if (( ! $doc->exists("$qry") ) || ( ! $$node->exists($qry) ))
            {
                if ( $$node->exists($qry) )
                {
                   #print "RELATIVITY\n"; 
                }
                elsif ( $doc->exists("$rootName/$qry") )
                {
                    $qry = "$rootName/$qry";
                } else {
                    #print " LOOKS BAD\n";
                }
            } else {
                #print " LOOKS GOOD\n";
            }
            
            if ( $doc->exists("$qry") )
            {
                print "          GET REFERENCE VALUE :: REFERENCE EXISTS :: '$qry'\n" if $printGetRef;
                my $res = $self->getValueAsText("$qry");
                print "      GET REFERENCE VALUE :: RES '$res'\n" if $printGetRef;
                if ( defined $res )
                {
                    $val = $res;
                    print "          GET REFERENCE VALUE :: VAL '$val' RES '$res'\n" if $printGetRef;
                } else {
                    $val = $ref;
                }
            } else {
                print "          GET REFERENCE VALUE :: REFERENCE DOES NOT EXISTS\n" if $printGetRef;
                #die;
                $val = $ref
            }
        }
    };
    
    if ($@)
    {
        print "        GET REFERENCE VALUE :: REFERENCE '$qry' DOES NOT EXISTS: $@\n" if $printGetRef;
        $val = $qry;
    }
    
    die if ! defined $val;
    print "          GET REFERENCE VALUE :: REFERENCE '$ref' => '$qry' => '$val'\n" if $printGetRef;
    
    return $val;
}








sub export
{
    my $self        = shift;
    my $config      = $self->{config};
    my $fn          = $self->{exportFile};

    print "EXPORT :: EXPORTING TO $fn\n" if $printExport;
    #open  XML, ">$fn.tmp" or die "COULD NOT OPEN $fn.tmp: $!";
    #print XML $self->toString();
    #close XML;

    $self->toFile("$fn.tmp");


    if ( -f "$fn.tmp" )
    {
        `xmllint --format $fn.tmp > $fn`;
        unlink("$fn.tmp");
    }

    my $cnfMd5 = &getMd5($config);
    my $bkpMd5 = &getMd5($fn);

    &printOnFile("$config.md5", $cnfMd5);
    &printOnFile("$fn.md5",     $bkpMd5);
}











sub toString
{
    my $self    = shift;
    my $doc     = $self->{root};
    
    my $out     = $doc->toString;
 
    return $out;
}


sub toFile
{
    my $self  = shift;
    my $fn    = shift;
    my $doc   = $self->{root};
    $doc->printToFile($fn) or die "COULD NOT EXPORT XML TO '$fn': $!";
}


sub getValue
{
    my $self     = shift;
    my $name     = shift;
    #print "GETTING VALUE SELF $self NAME $name\n";
    my $doc      = $self->{doc};
    my $rootName = $self->{rootName};
    
    my $hash     = $self->parse($name);
   
    #&dump($hash);
    
    exists ${$hash}{val} ? return ${$hash}{val} : return [];
}

sub getValueAsText
{
    my $self     = shift;
    my $name     = shift;
    
    my $text     = join("", @{$self->getValue($name)});
    
    return $text;
}

sub getNodeValue
{
    my $self = shift;
    my $node = shift;
    
    my $doc      = $self->{doc};
    my $rootName = $self->{rootName};
    
    my $hash = $self->getChildNfo($node);
    
    exists ${$hash}{val} ? return ${$hash}{val} : return '';
}


sub getXpath
{
    my $self     = shift;
    my $req      = shift;
    my $doc      = $self->{doc};
    my $rootName = $self->{rootName};
    
    return $doc->findnodes("$rootName/$req");
}

sub getChildrenIds
{
    my $self = shift;
    my $name = shift;
    
    my $nodes = $self->getChildren($name);
    
    #&dump($nodes);
    
    my @arr = map { $_->{_id} } @$nodes;
    
    #&dump(\@arr);
    
    return \@arr;
}


sub getNodes
{
    my $self     = shift;
    my $name     = shift;
    my %out;
    
    my $hash     = $self->parse($name);
    #&dump($hash);
    return $hash->{nodes}     if ( exists $hash->{nodes}     );
}

sub getChildren
{
    my $self     = shift;
    my $name     = shift;
    my %out;
    
    my $hash     = $self->parse($name);
    #&dump($hash);
    return $hash->{children} if ( exists $hash->{children} );
}

sub parse
{
    my $self       = shift;
    my $name       = shift;
    my $doc        = $self->{doc};
    my $rootName   = $self->{rootName};
    my %out;
    
    print "GETTING $name\n" if $printParse;
    
    if (( ! $doc->exists("$name") ) && ( $doc->exists("$rootName/$name") ))
    {
        $name = "$rootName/$name";
        #print "   GETTING $name\n";
    };
    
    return undef if ( ! $doc->exists("$name") );
    
    my @nodes = $doc->findnodes("$name");
    
    print "  NODES: ", scalar @nodes, "\n" if $printParse;
    foreach my $node ( @nodes )
    {
        my $name     = $node->getNodeName();
        print "    NAME: '$name'\n" if $printParse;
        
        if ( $node->getNodeType == ELEMENT_NODE )
        {
            &mergeHash( \%out, &parseAtts($node->getAttributes()));
            
            my @childs = $node->getChildNodes();
            
            for ( my $c = 0; $c < @childs; $c++ )
            {
                my $child = $childs[$c];
                
                if ( $child->getNodeType == TEXT_NODE )
                {
                    my $val   = $self->parseVal($child);
                    next if (( ! defined $val ) || ( $val eq '' ));
                    push(@{$out{val}}, $val);
                    print "    cVAL: [$c]'$val'\n" if $printParse;
                } else {
                    my $val = $self->getChildNfo($child);
                    push(@{$out{children}}, $val);
                    print "    aVAL: [$c]'". (exists $val->{name} ? $val->{name} : '[TEXT]')."'\n" if $printParse;
                }
            }
        }
        elsif ( $node->getNodeType() == TEXT_NODE )
        {
            my $val   = $self->parseVal($node);
            push(@{$out{val}}, $val);
            print "    nVAL: '$val'\n" if $printParse;
        }
    }
    
    $out{nodes} = \@nodes;
    
    return \%out;
}

#sub getNode
#{
#    my $self     = shift;
#    my $name     = shift;
#    my $doc      = $self->{doc};
#    my $rootName = $self->{rootName};
#    print "NAME: $name\n";
#    my $res      = $doc->findnodes("$name") or die "ERROR: $!";
#    return $res;
#}


sub getChildNfo
{
    my $self     = shift;
    my $node     = shift;
    my %hash;

    $hash{link} = $node;
    if ( $node->getNodeType == ELEMENT_NODE )
    {
        $hash{name} = $node->getNodeName();
        &mergeHash( \%hash, &parseAtts($node->getAttributes()));
    }
    elsif ( $node->getNodeType == TEXT_NODE )
    {
        $hash{val}  = $self->parseVal($node);
    }
    
    return \%hash;
}



sub parseVal
{
    my $self = shift;
    my $node = shift;
    
    my $val = '';
    return undef if ! defined $node;
    
    if ( $node->getNodeType == TEXT_NODE )
    {
        $val .= &cleanXml($node->getData());
    }
    elsif ( $node->getNodeType == ELEMENT_NODE )
    {
        $val .= &cleanXml($node->getData());
    }
    elsif ( $node->getNodeType == ATTRIBUTE_NODE )
    {
        $val .= &cleanXml($node->getValue());
    }
    elsif ( $node->getNodeType == PROCESSING_INSTRUCTION_NODE )
    {
        $val .= &cleanXml($node->getData());
    } else {
        print "UNKNOWN NODE TYPE: ",$node->getNodeType,"\n";
    }
    
    return $val;
}


sub traverse
{
    my $self     = shift;
    my $doc      = $self->{doc};
    my $root     = $self->{root};
    my $rootName = $self->{rootName};
    
    my($node, $level);
    if    ( @_ == 2 ) { ( $node, $level) = ( shift,  shift ); }
    elsif ( @_ == 1 ) { ( $node, $level) = ( shift,  0     ); }
    elsif ( @_ == 0 ) { ( $node, $level) = ( \$root, 0     ); }
    else              { die };

    if ($$node->getNodeType == ELEMENT_NODE) {
        print "\n", "  "x$level, "[$level] <", $$node->getNodeName;
        my $thisItem = 0;
        my $atts = $$node->getAttributes();
        while ($atts->item($thisItem)){
            my $att      = $atts->item($thisItem);
            my $attName  = $att->getNodeName();
            my $attValue = &cleanXml($att->getNodeValue());
            print " [ATT] '$attName=\"$attValue\"'";
            $thisItem++
        }
        print ">";
        
        my $el = 0;
        foreach my $child ( $$node->getChildNodes() )
        {
            $el++ if ( $child->getNodeType != TEXT_NODE );
            $self->traverse(\$child, $level+1);
        }
        print "\n", "  "x$level, "[$level] " if $el;
        print "</", $$node->getNodeName, ">";
    } elsif ($$node->getNodeType() == TEXT_NODE) {
        my $data = $self->parseVal($$node);
        print "[DATA] '$data'" if  $data;
    }
    
    print "\n" if ! $level;
}






1;
