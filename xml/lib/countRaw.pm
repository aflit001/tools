package countRaw;
use strict;
use warnings;

our @ISA       = qw{Exporter};
our @EXPORT_OK = qw{new count getFormats getFormatsStr};

my %formats = (
        sff   => '~/bin/sffCount',
        fastq => '~/bin/fastqCount'
);

sub new {
    my $class = shift;
    my $self = bless {}, $class;


    my $opts;
    map { $opts .= "|" if defined $opts; $opts .= "$_\$"; } sort keys %formats;

    $self->{options} = $opts;

    return $self;
}

sub getFormats
{
    my $self = shift;
    return \%formats;
}

sub getFormatsStr
{
    my $self = shift;
    return $self->{options};
}

sub count
{
    my $self = shift;
    my $file = shift;
    my $opts = $self->{options};
    my $res;
    
    if ( $file =~ /\.($opts)/ )
    {
        my $fmt = $1;
        my $cmd = $formats{$fmt};
        print "      VALID FILE :: FORMAT $fmt COMMAND $cmd\n";
        $res = `$cmd $file 2>&1`;
    } else {
        print "UNKNOWN FORMAT :: $file\n";
        return undef;
    }
    
    return undef if ! defined $res;
    
    #FILE ./Pennellii_Illumina_pairedend_600/EAS67_0093_PEFC30600AAXX_s_5_1_sequence.fastq SIZE 3645924630 COUNT 17205619
    #Error:  Unable to read SFF file:  ./Heinz_454_matepair_20000/20_France_20kb_7000011429.sff
    if ( $res =~ /Error\: /)
    {
        print "ERROR: $res\n";
        return undef;
    } else {
        if ( $res =~ /FILE (.+) SIZE (\d+) COUNT (\d+)/ )
        {
            my $name  = $1;
            my $size  = $2;
            my $count = $3;
            print "        FILE $name SIZE $size COUNT $count\n";
            return {name => $name, size=> $size, count=>$count};
        } else {
            return undef;
        }
    }
}

1;
