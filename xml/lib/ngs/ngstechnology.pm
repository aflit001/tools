package ngs::ngstechnology;
require Exporter;
our @ISA       = qw{ Exporter };
our @EXPORT_OK = qw{ new addFile addNfo addPipeline addPipelineArray dumper  };

use strict;
use warnings;

use constant {
    NAME    => "  "x4 . 'ngstechnology'
};

my $verbose = 0;

sub new {
    my $class = shift;
    my $self  = bless {}, $class;
    
    my %nfo   = @_;
    
    die if ( ! exists $nfo{parent} );
    die if ( ! exists $nfo{specie} );

    die if ( ! defined $nfo{parent} );
    die if ( ! defined $nfo{specie} );

    $self->{parent} = $nfo{parent};
    $self->{specie} = $nfo{specie};

    $verbose      = exists $nfo{verbose} ?  $nfo{verbose} : $verbose;
    
    print "  ", $self->getName('CREATING NEW TECHNOLOGY'), " SPP = ",
                $self->{specie},
                " PARENT = " .
                $self->{parent} . "\n" if $verbose;
    
    return $self;
}


sub addFile
{
    my $self       = shift;
    my %nfo        = @_;
    
    print "  "x2 . $self->getName('ADDING FILE') . " ". $nfo{file} ."\n" if $verbose;
    die if ! exists $nfo{file};
    $self->{_FILES_}{$nfo{file}} = exists $nfo{atts} ? $nfo{atts} : {};

    return 1;
}


sub addNfo
{
    my $self       = shift;
    my %nfo        = @_;
    
    map {
        $self->{_NFO_}{$_} = $nfo{$_};
        print "  "x2, $self->getName('ADDING NFO'), " $_\n" if $verbose;
    } keys %nfo;
}


sub addPipeline
{
    my $self      = shift;
    my $pipeSteps = shift;

    #[ $stepsAtts->{_id} , $stepsAtts->{_parameters} ]
    print "  "x2, $self->getName('ADDING PIPELINE'), "\n" if $verbose;
    
    foreach my $pair ( @$pipeSteps )
    {
        my $id    = $pair->[0];
        my $param = $pair->[1];
        print "  "x3, $self->getName('ADDING PIPELINE'), " ID $id -> PARAM $param\n" if $verbose;
    }
    
    push( @{ $self->{pipeline} }, $pipeSteps);
}

sub addPipelineArray
{
    my $self      = shift;
    my $pipeArray = shift;

    #[ $stepsAtts->{_id} , $stepsAtts->{_parameters} ]
    #print "  "x2, $self->getName(), " ADDING PIPELINE\n" if $verbose;
    foreach my $arr ( @$pipeArray )
    {
        $self->addPipeline($arr);
    }
}

sub dumper
{
    my $self = shift;
    my $d = Data::Dumper->new([$self]);
    $d->Purity(1)->Terse(1)->Deepcopy(1);
    print $d->Dump;
}

1;
