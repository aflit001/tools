package ngs::illumina;
use lib "../";
use ngs::ngstechnology;

our @ISA       = qw{ ngs::ngstechnology };
our @EXPORT_OK = qw{ new add  };
use strict;
use warnings;

use constant {
    NAME    => "  "x4 . 'Illumina'
};


sub new {
    my $class   = shift;
    
    my $self    = $class->SUPER::new(@_);
    bless $self, $class;
    
    return $self;
}


sub getAvailableSteps
{
    my $self       = shift;
    my %steps = {
        clipping             => \&clipping,
        kmerCount            => \&kmerCount,
        kmerDelete           => \&kmerDelete,
        deleteIdenticalReads => \&deleteIdenticalReads
    };
    
    return \%steps;
}


sub getName
{
    my $self = shift;
    my $func = shift;
    if ( defined $func )
    {
        $func = " | $func ::"
    } else {
        $func = "";
    };
    
    return "" . NAME . " | " . $self->{specie} . $func;
}





# *********************************************************
#
# PIPELINES STRATEGIES
#
# *********************************************************

sub clipping
{
    my $self = shift;
    
    return 1;
}

sub kmerCount
{
    my $self = shift;
    
    return 1;
}

sub kmerDelete
{
    my $self = shift;
    
    return 1;
}

sub deleteIdenticalReads
{
    my $self = shift;
    
    return 1;
}

1;
