package readxml;
require Exporter;
our @ISA       = qw{Exporter};
our @EXPORT_OK = qw{ new getValueAsText };
#our @EXPORT_OK = qw{ new toString getValue getXpath getChildrenIds getNodes parse getChildNfo parseVal traverse };

use warnings;
use strict;
our $VERSION   = 1.0;
use XML::DOM;
use XML::DOM::XPath;
use xml qw( cleanXml parseAtts mergeHash dump );

#http://www.tutorialspoint.com/perl/perl_oo_perl.htm

#my $cnf = loadconf->new();
#my $cnf = loadconf->new(config => config_file_name.xml);
#my $inputMergedTabFile = $cnf->get('reader.inputExpTab');




sub new
{
    my $print    = 0;

    my $class    = shift;
    my $self     = bless {}, $class;

    my %vars     = @_;

    my $config   = exists $vars{config} ? $vars{config} : 'config.xml';

    my $parser   = new XML::DOM::Parser;
    my $doc      = $parser->parsefile($config);
    my $root     = $doc->getDocumentElement();
    my $rootName = $root->getNodeName();
    

    #print "The root element is '", $root->getNodeName(), "'\n";
    #my @children = $root->getChildNodes();
    #print "There are ", scalar(@children), " child elements.\n";
    

    $self->{config}   = $config;
    $self->{parser}   = $parser;
    $self->{doc}      = $doc;
    $self->{root}     = $root;
    $self->{rootName} = $rootName;

    $self->traverse() if $print;
    
    return $self;
}

sub getDoc
{
    
}


sub toString
{
    my $self    = shift;
    my $doc     = $self->{root};
    
    my $out     = $doc->toString;
 
    return $out;
}


sub toFile
{
    my $self  = shift;
    my $fn    = shift;
    my $doc   = $self->{root};
    $doc->printToFile($fn) or die "COULD NOT EXPORT XML TO '$fn': $!";
}


sub getValue
{
    my $self     = shift;
    my $name     = shift;
    #print "GETTING VALUE SELF $self NAME $name\n";
    my $doc      = $self->{doc};
    my $rootName = $self->{rootName};
    
    my $hash     = $self->parse($name);
   
    #&dump($hash);
    
    exists ${$hash}{val} ? return ${$hash}{val} : return [];
}

sub getValueAsText
{
    my $self     = shift;
    my $name     = shift;
    
    my $text     = join("", @{$self->getValue($name)});
    
    return $text;
}

sub getNodeValue
{
    my $self = shift;
    my $node = shift;
    
    my $doc      = $self->{doc};
    my $rootName = $self->{rootName};
    
    my $hash = $self->getChildNfo($node);
    
    exists ${$hash}{val} ? return ${$hash}{val} : return '';
}


sub getXpath
{
    my $self     = shift;
    my $req      = $_[0];
    my $doc      = $self->{doc};
    my $rootName = $self->{rootName};
    
    return $doc->findnodes("$rootName/$req");
}

sub getChildrenIds
{
    my $self = shift;
    my $name = $_[0];
    
    my $nodes = $self->getChildren($name);
    
    #&dump($nodes);
    
    my @arr = map { $_->{_id} } @$nodes;
    
    #&dump(\@arr);
    
    return \@arr;
}


sub getNodes
{
    my $self     = shift;
    my $name     = shift;
    my %out;
    
    my $hash     = $self->parse($name);
    #&dump($hash);
    return $hash->{nodes}     if ( exists $hash->{nodes}     );
}

sub getChildren
{
    my $self     = shift;
    my $name     = shift;
    my %out;
    
    my $hash     = $self->parse($name);
    #&dump($hash);
    return $hash->{children} if ( exists $hash->{children} );
}

sub parse
{
    my $self       = shift;
    my $name       = shift;
    my $doc        = $self->{doc};
    my $rootName   = $self->{rootName};
    my $printParse = 0;
    my %out;
    
    print "GETTING $name\n" if $printParse;
    
    if (( ! $doc->exists("$name") ) && ( $doc->exists("$rootName/$name") ))
    {
        $name = "$rootName/$name";
        #print "   GETTING $name\n";
    };
    
    return undef if ( ! $doc->exists("$name") );
    
    my @nodes = $doc->findnodes("$name");
    
    print "  NODES: ", scalar @nodes, "\n" if $printParse;
    foreach my $node ( @nodes )
    {
        my $name     = $node->getNodeName();
        print "    NAME: '$name'\n" if $printParse;
        
        if ( $node->getNodeType == ELEMENT_NODE )
        {
            &mergeHash( \%out, &parseAtts($node->getAttributes()));
            
            my @childs = $node->getChildNodes();
            
            for ( my $c = 0; $c < @childs; $c++ )
            {
                my $child = $childs[$c];
                
                if ( $child->getNodeType == TEXT_NODE )
                {
                    my $val   = $self->parseVal($child);
                    next if (( ! defined $val ) || ( $val eq '' ));
                    push(@{$out{val}}, $val);
                    print "    cVAL: [$c]'$val'\n" if $printParse;
                } else {
                    my $val = $self->getChildNfo($child);
                    push(@{$out{children}}, $val);
                    print "    aVAL: [$c]'". (exists $val->{name} ? $val->{name} : '[TEXT]')."'\n" if $printParse;
                }
            }
        }
        elsif ( $node->getNodeType() == TEXT_NODE )
        {
            my $val   = $self->parseVal($node);
            push(@{$out{val}}, $val);
            print "    nVAL: '$val'\n" if $printParse;
        }
    }
    
    $out{nodes} = \@nodes;
    
    return \%out;
}




sub getChildNfo
{
    my $self     = shift;
    my $node     = shift;
    my %hash;

    $hash{link} = $node;
    if ( $node->getNodeType == ELEMENT_NODE )
    {
        $hash{name} = $node->getNodeName();
        &mergeHash( \%hash, &parseAtts($node->getAttributes()));
    }
    elsif ( $node->getNodeType == TEXT_NODE )
    {
        $hash{val}  = $self->parseVal($node);
    }
    
    return \%hash;
}



sub parseVal
{
    my $self = shift;
    my $node = shift;
    
    my $val = '';
    return undef if ! defined $node;
    
    if ( $node->getNodeType == TEXT_NODE )
    {
        $val .= &cleanXml($node->getData());
    }
    elsif ( $node->getNodeType == ELEMENT_NODE )
    {
        $val .= &cleanXml($node->getData());
    }
    elsif ( $node->getNodeType == ATTRIBUTE_NODE )
    {
        $val .= &cleanXml($node->getValue());
    }
    elsif ( $node->getNodeType == PROCESSING_INSTRUCTION_NODE )
    {
        $val .= &cleanXml($node->getData());
    } else {
        print "UNKNOWN NODE TYPE: ",$node->getNodeType,"\n";
    }
    
    return $val;
}


sub traverse
{
    my $self     = shift;
    my $doc      = $self->{doc};
    my $root     = $self->{root};
    my $rootName = $self->{rootName};
    
    my($node, $level);
    if    ( @_ == 2 ) { ( $node, $level) = ( shift,  shift ); }
    elsif ( @_ == 1 ) { ( $node, $level) = ( shift,  0     ); }
    elsif ( @_ == 0 ) { ( $node, $level) = ( \$root, 0     ); }
    else              { die };

    if ($$node->getNodeType == ELEMENT_NODE) {
        print "\n", "  "x$level, "[$level] <", $$node->getNodeName;
        my $thisItem = 0;
        my $atts = $$node->getAttributes();
        while ($atts->item($thisItem)){
            my $att      = $atts->item($thisItem);
            my $attName  = $att->getNodeName();
            my $attValue = &cleanXml($att->getNodeValue());
            print " [ATT] '$attName=\"$attValue\"'";
            $thisItem++
        }
        print ">";
        
        my $el = 0;
        foreach my $child ( $$node->getChildNodes() )
        {
            $el++ if ( $child->getNodeType != TEXT_NODE );
            $self->traverse(\$child, $level+1);
        }
        print "\n", "  "x$level, "[$level] " if $el;
        print "</", $$node->getNodeName, ">";
    } elsif ($$node->getNodeType() == TEXT_NODE) {
        my $data = $self->parseVal($$node);
        print "[DATA] '$data'" if  $data;
    }
    
    print "\n" if ! $level;
}







1;

#sub dereferenceOld
#{
#    my $self     = shift;
#    my $val      = $_[0];
#    my $doc      = $self->{doc};
#    my $rootName = $self->{rootName};
#    
#    #&cleanXml(
#    
#    while ( $val =~ /\$(.+?)\$/g )
#    {
#        my $reference = $1;
#        my $query     = $reference;
#        print "REFERENCE $val => $reference\n";
#        
#        if (( ! $doc->exists("$query") ) && ( $doc->exists("$rootName/$query") )) { $query = "$rootName/$query"; };
#        
#        if ( $doc->exists("$query") )
#        {
#            print "  REFERENC EXISTS\n";
#            my $tr = $self->getValue("$query");
#            
#            if ( defined $tr )
#            {
#                print "    TR '$tr'\n";
#                $val =~ s/\$\Q$reference\E\$/$tr/g;
#                print "    VAL '$val'\n";
#            }
#        }
#    }
#
#    return $val;
#}



