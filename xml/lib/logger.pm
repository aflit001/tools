package logger;

require Exporter;

@ISA       = qw{Exporter};
@EXPORT_OK = qw{new log close};

sub new {
    my $class = shift;
    my $self = bless {}, $class;

    my %vars        = @_;

    my $TS          = &getTimeStamp();
    my $epitope     = exists $vars{epitope} ? $vars{epitope} : "log";
    my $level       = exists $vars{level}   ? $vars{level}   : 0;
    my $projLogFile = "$epitope\_$TS.log";
    
    if ( open ($PROJLOG, ">$projLogFile") )
    {
        $self->{fh}      = $PROJLOG;
        $self->{file}    = $projLogFile;
        $self->{TS}      = $TS;
        $self->{epitope} = $epitope;
        $self->{level}   = $level;
        
        return $self;
    } else {
        warn "COULD NOT OPEN LOG FILE :: $projLogFile : $!";
        return undef;
    }
}

sub printLog
{
    my $self   = shift;
    my $levelR = $_[0];
    my $msg    = join("",@_[1 .. (scalar(@_)-1)]);
    
    my $fh     = $self->{fh};
    my $levelD = $self->{level};
    
    if ( defined $fh )
    {
        return 1 if ! defined $levelR;
        return 1 if ! defined $msg;
        return 1 if ( $levelR !~ /^\d+$/ );
        
        if ( $levelD >= $levelR )
        {
            print " "x($levelR+1), $msg;
        }
        if ( ($levelR+1) >= $levelR)
        {
            print $fh " "x($levelR+1), &getTimeStamp(), " ", $msg;
        }
        
        return 0;
    } else {
        return 1;
    }

    my $v = $_[0];
    my $line = join("",@_[1 .. (scalar(@_)-1)]);
    
}


sub getTimeStamp
{
    my ($gSec, $gMin, $gHour, $gMday, $gMon, $gYear, $gWday, $gYday, $gIsdst) = localtime(time);
    $gYear += 1900;
    $gMon  += 1;
    return sprintf "%04d_%02d_%02d_%02d_%02d", $gYear, $gMon, $gMday, $gHour, $gMin;
}

sub close
{
    my $self = shift;
    
    my $fh = $self->{fh};
    if ( defined $fh )
    {
        close $fh;
        return 0;
    } else {
        return 1;
    }    
}

1;
