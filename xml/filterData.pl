#!/usr/bin/perl -w
use strict;
use Data::Dumper;

use lib "./lib";
use logger;
use xmldereference;
use ngs;
use xml qw( cleanXml parseAtts mergeHash dumper getCurrNodeNfo getParentNfo );

my $verbose     = 0;
my $conf        = xmldereference->new(config => 'setup/config.xml');
my $inputFolder = $conf->getValueAsText('InputFolder');
my $destiFolder = $conf->getValueAsText('DestinationFolder');

die "NO INPUT FOLDER DEFINED"  if ( ! defined $inputFolder );
die "NO OUTPUT FOLDER DEFINED" if ( ! defined $destiFolder );

die "INPUT FOLDER $inputFolder DOES NOT EXISTS"  if ( ! -d $inputFolder );
die "OUTPUT FOLDER $destiFolder DOES NOT EXISTS" if ( ! -d $destiFolder );

my $log = logger->new(epitope => "./log/filter", level => $verbose);
$log->printLog(0, "INPUT FOLDER $inputFolder\n");
$log->printLog(0, "DEST  FOLDER $destiFolder\n");

my %NGS;
my $assemblies  = &loadAssemblies( $conf);
my $pipelines   = &loadPipelines(  $conf, \%NGS);
my $dataFolders = &loadDataFolders($conf, \%NGS);
my $projects    = &loadProjects(   $conf, $assemblies, $pipelines, $dataFolders, \%NGS);

foreach my $projectName ( sort keys %NGS )
{
    print "  PROJECT: $projectName\n";
    my $ngs = $NGS{$projectName};
    #$ngs->dumper();
    $ngs->getAssembliesSteps();
    $ngs->compile();
    
}

$log->close;
print "EXITING\n";










sub loadDataFolders
{
    my $cnf        = shift;
    my $ngs        = shift;
    my $doc        = $cnf->getDoc();
    my $rootName   = $cnf->getRootName();
    my $printLoadD = 0;

    my %dataFolders;
    
    my $nfo = &getCurrNodeNfo($cnf, '/dataFolders/specie/technology/folder/file');
    #dumper($nfo);
    #exit;
    foreach my $data ( @$nfo )
    {
        my $file        = $data->[0]{ATT};
        my $folder      = $data->[1]{ATT};
        my $technology  = $data->[2]{ATT};
        my $specie      = $data->[3]{ATT};
        
        my $sppName        = $specie->{id};
        my $folderName     = $folder->{id};
        my $technologyName = $technology->{id};
        my $fileName       = $file->{id};
        
        $dataFolders{$sppName}{TECH}{$technologyName}{FOLDER}{$folderName}{FILE}{$fileName} = $file;
        $dataFolders{$sppName}{TECH}{$technologyName}{FOLDER}{$folderName}{FOLDER_NFO} = $folder;
        $dataFolders{$sppName}{TECH}{$technologyName}{TECH_NFO} = $technology;
        $dataFolders{$sppName}{SPP_NFO} = $specie;

        #                        {SPP_NFO}{$key} = $value    
        #$dataFolders{$sppName}  {TECH}{$technologyName}{FOLDER}{$folderName}{FILE}{$fileName}{$key} = $value
        #                              {$technologyName}{TECH_NFO}{$key} = $value;
        #                                                       {$folderName}{FOLDER_NFO}{$key} = $value;
    }
    
    #dumper(\%dataFolders);
    #exit;
    
    return \%dataFolders;
    
    #datafolder
    #{
    #    $sppName
    #    {
    #        TECH
    #        {
    #            $technologyName
    #            {
    #                FOLDER
    #                {
    #                    $folderName
    #                    {
    #                        FILE
    #                        {
    #                            $filename
    #                            {
    #                                $key => $val
    #                            }
    #                        }
    #                        FOLDER_NFO
    #                        {
    #                            $key => $val
    #                        }
    #                    }
    #                }
    #                TECH_NFO
    #                {
    #                    $key => $val
    #                }
    #            }
    #        }
    #        SPP_NFO
    #        {
    #            $key => $val
    #        }
    #    }
    #}
    #                        {SPP_NFO}{$key} = $value    
    #$dataFolders{$sppName}  {TECH}{$technologyName}{FOLDER}{$folderName}{FILE}{$fileName}{$key} = $value
    #                              {$technologyName}{TECH_NFO}{$key} = $value;
    #                                                       {$folderName}{FOLDER_NFO}{$key} = $value;


    #<dataFolders>
    #  <specie id="AllRound" files="25" insertSizes="5" libTypes="1" libraries="4" size="166747907487" species="7" sumReads="525398492" technologies="2">
    #    <technology id="454" files="17" insertSizes="3" libTypes="1" size="26490756927" species="3" sumReads="8453276" technologies="1">
    #      <folder id="AllRound_454_matepair_20000" size="6750882266" reads="2176751" files="4">
    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_20000/20000_3_2_XLR56_GNKKXC002.sff" count="0" size="1334626250" reads="468249"/>
    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_20000/20000_1_1_XLR54_GMFT65F01.sff" count="1" size="1944004554" reads="568823"/>
    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_20000/20000_2_1_XLR55_GMNA3LJ01.sff" count="2" size="1982751460" reads="612608"/>
    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_20000/20000_4_1_XLR58_GO17ZGT01.sff" count="3" size="1489500002" reads="527071"/>
    #      </folder>
    #      <folder id="AllRound_454_matepair_3000" size="12191349699" reads="3952186" files="8">
    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_3000/3000_2_4_XLR69_GX9RM5204.sff" count="0" size="749214238" reads="239320"/>
    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_3000/3000_1_1_XLR71_GZGLCTR01.sff" count="1" size="1871983295" reads="613058"/>
    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_3000/3000_1_3_XLR69_GX9RM5203.sff" count="2" size="560239206" reads="183304"/>
    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_3000/3000_1,2_1_XLR72_GZJ7FCV01.sff" count="3" size="1837587086" reads="599624"/>
    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_3000/3000_1,2_2_XLR70_GYSE4CQ02.sff" count="4" size="1886125686" reads="607336"/>
    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_3000/3000_1_1_XLR70_GYSE4CQ01.sff" count="5" size="1561248322" reads="511121"/>
    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_3000/3000_2_2_XLR72_GZJ7FCV02.sff" count="6" size="1870655031" reads="601952"/>
    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_3000/3000_2_2_XLR71_GZGLCTR02.sff" count="7" size="1854296835" reads="596471"/>
    #      </folder>
    #      <folder id="AllRound_454_matepair_8000" size="7548524962" reads="2324339" files="5">
    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_8000/8000_4_4_XLR64_GR3I8FS04.sff" count="0" size="792116302" reads="248444"/>
    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_8000/8000_3_2_XLR63_GQ92EL202.sff" count="1" size="1851425682" reads="569925"/>
    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_8000/8000_4_3_XLR64_GR3I8FS03.sff" count="2" size="802537642" reads="252121"/>
    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_8000/8000_2_1_XLR63_GQ92EL201.sff" count="3" size="2094255134" reads="637836"/>
    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_8000/8000_1_2_XLR62_GQWYU1N02.sff" count="4" size="2008190202" reads="616013"/>
    #      </folder>
    #    </technology>
    #  </species>
    #</dataFolders>

}


sub loadPipelines
{
    my $cnf        = shift;
    my $doc        = $cnf->getDoc();
    my $rootName   = $cnf->getRootName();
    my $printLoadP = 0;

    my %pipelines;
    
    
    my $nfo = &getCurrNodeNfo($cnf, '/pipelines/technology/pipeline/step');
    foreach my $pipeline ( @$nfo )
    {
        #print "PIPELINE $pipeline\n";
        #foreach my $line ( @$pipeline )
        #{
        #    print "  LINE $line\n";
        #    foreach my $key ( sort keys %$line )
        #    {
        #        my $val = $line->{$key};
        #        if ( $key eq 'ATT' )
        #        {
        #            foreach my $att ( sort keys %$val )
        #            {
        #                my $attV = $val->{$att};
        #                print "    ATT $att VAL $attV\n";
        #            }
        #        } else {
        #            #print "    KEY $key VAL $val\n";
        #        }
        #    }
        #}
        my $stepId       = $pipeline->[0]{ATT}{id};
        my $stepParam    = $pipeline->[0]{ATT}{parameters};
        my $pipelineId   = $pipeline->[1]{ATT}{id};
        my $technologyId = $pipeline->[2]{ATT}{id};
        #print " STEP ID $stepId STEP PARAM $stepParam PIPELINE $pipelineId TECHNOLOGY $technologyId\n";
        push(@{ $pipelines{$technologyId}{$pipelineId} }, [ $stepId , $stepParam ]);
    }

    return \%pipelines;
    #<pipelines>
    #    <technology id="454">
    #        <pipeline id="454">
    #            <step id="check"    parameters="checkParam4"/>
    #            <step id="clipping" parameters="clipParam4"/>
    #            <step id="kmer"     parameters="kmarParam4"/>
    #        </pipeline>
    #        <pipeline id="454_2">
    #            <step id="check"    parameters="checkParam42"/>
    #            <step id="clipping" parameters="clipParam42"/>
    #            <step id="kmer"     parameters="kmarParam42"/>
    #        </pipeline>
    #    </technology>
    #    <technology id="Illumina">
    #        <pipeline id="Illumina">
    #            <step id="check"    parameters="checkParamI"/>
    #            <step id="clipping" parameters="clipParamI"/>
    #            <step id="kmer"     parameters="kmarParamI"/>
    #            <step id="check"    parameters="checkParamI2"/>
    #        </pipeline>
    #    </technology>
    #</pipelines>

}

sub loadAssemblies
{
    my $cnf        = shift;
    my $doc        = $cnf->getDoc();
    my $rootName   = $cnf->getRootName();
    my $printLoadP = 0;

    my %assemblies;
    
    my $nfo = &getCurrNodeNfo($cnf, '/assemblies/assembly/step');
    #dumper($nfo);
    #exit;
    foreach my $assembly ( @$nfo )
    {
        my $stepId       = $assembly->[0]{ATT}{id};
        my $stepParam    = $assembly->[0]{ATT}{parameters};
        my $assemblyId   = $assembly->[1]{ATT}{id};
        #print " STEP ID $stepId STEP PARAM $stepParam PIPELINE $pipelineId TECHNOLOGY $technologyId\n";
        push(@{ $assemblies{$assemblyId}}, [ $stepId , $stepParam ]);
    }
    #dumper(\%assemblies);
    #exit;

    return \%assemblies;
    #<assemblies>
    #    <assembly id="mixed1">
    #        <step id="wgs" parameters="standard"/>
    #    </assembly>
    #    <assembly id="mixed2">
    #        <step id="wgs" parameters="tweaked"/>
    #    </assembly>
    #    <assembly id="mixedAndFiltered">
    #        <step id="filter" parameters="standard"/>
    #        <step id="wgs"    parameters="standard"/>
    #    </assembly>
    #</assemblies>

}

sub loadProjects
{
    my $cnf        = shift;
    my $ass        = shift;
    my $pipe       = shift;
    my $data       = shift;
    my $ngs        = shift;
    my $doc        = $cnf->getDoc();
    my $rootName   = $cnf->getRootName();
    my $printLoadP = 0;

    #push(@{ $pipelines{$technologyId}{$pipelineId} }, [ $stepId , $stepParam ]);
    #push(@{ $assemblies{$assemblyId}}, [ $stepId , $stepParam ]);
    
    my %projects;
    
    my $nfo2 = &getCurrNodeNfo($cnf, '/projects/project/assemblies/assembly');
    #dumper($nfo2);
    #exit;
    foreach my $project ( @$nfo2 )
    {
        my $assemblyId    = $project->[0]{ATT}{id};
        my $projectName   = $project->[2]{ATT}{title};
        my $projectSpecie = $project->[2]{ATT}{specie};
        #print " STEP ID $stepId STEP PARAM $stepParam PIPELINE $pipelineId TECHNOLOGY $technologyId\n";
        $projects{$projectSpecie}{$projectName}{ASSEMBLY} = $assemblyId;
        
        # *********************
        # CREATES NGS PROJECT FOR SPECICES
        # *********************
        $ngs->{$projectName} = ngs->new( specie => $projectSpecie );
        my $ngsProj = $ngs->{$projectName};
        
        # *********************
        # CHECK IF THERE IS A ASSEMBLY STRATEGY FOR THE TECHNOLOGY
        # push(@{ $assemblies{$assemblyId}}, [ $stepId , $stepParam ]);
        # *********************
        die "THERE'S NO ASSEMBLER $assemblyId" if ! exists ${$ass}{$assemblyId};
        my $assArr = ${$ass}{$assemblyId};
        $ngsProj->addAssembly($assArr);
    }




    my $nfo = &getCurrNodeNfo($cnf, '/projects/project/pipelines/pipeline');

    #dumper($nfo);
    #exit;
    foreach my $project ( @$nfo )
    {
        my $pipelineTechnology = $project->[0]{ATT}{technology};
        my $pipelineName       = $project->[0]{ATT}{pipeline};
        my $projectName        = $project->[2]{ATT}{title};
        my $projectSpecie      = $project->[2]{ATT}{specie};
        #print " STEP ID $stepId STEP PARAM $stepParam PIPELINE $pipelineId TECHNOLOGY $technologyId\n";
        $projects{$projectSpecie}{$projectName}{PIPELINE}{$pipelineTechnology} = $pipelineName;
        
        my $ngsProj = $ngs->{$projectName};
        
        # *********************
        # CHECK IF THERE IS A PIPELINE FOR THE TECHNOLOGY
        # push(@{ $pipelines{$technologyId}{$pipelineId} }, [ $stepId , $stepParam ]);
        # *********************
        die "THERE'S NO PIPELINE FOR TECHNOLOGY $pipelineTechnology" if ! exists ${$pipe}{$pipelineTechnology};
        my $pipeTech    = $pipe->{$pipelineTechnology};
        die "THERE'S NO PIPELINE CALLED $pipelineName"               if ! exists ${$pipeTech}{$pipelineName};
        my $pipeArr     = $pipeTech->{$pipelineName};
        
        # *********************
        # GET TECHNOLOGY MODULE
        # *********************
        my $ngsTechs = $ngsProj->getAvailableTechnologies();
        die "TECHNOLOGY $pipelineTechnology NOT IMPLEMENTE IN NGS" if ( ! exists ${$ngsTechs}{$pipelineTechnology} );
        my $ngsTech = $ngsProj->getTechnology($pipelineTechnology);
        
        
        # *********************
        # ADD PIPELINE TO TECHNOLOGY
        # *********************
        $ngsTech->addPipeline($pipeArr);
        
        
        #$dataFolders{$sppName}{TECH}{$technologyName}{FOLDER}{$folderName}{FILE}{$fileName}{$key} = $value;
        #$dataFolders{$sppName}{TECH}{$technologyName}{FOLDER}{$folderName}{FOLDER_NFO} = $folder;
        #$dataFolders{$sppName}{TECH}{$technologyName}{TECH_NFO} = $technology;
        #$dataFolders{$sppName}{SPP_NFO} = $specie;
        
        if ( exists $data->{$projectSpecie} )
        {
            if ( exists $data->{$projectSpecie}{TECH}{$pipelineTechnology} )
            {
                my $folders = $data->{$projectSpecie}{TECH}{$pipelineTechnology}{FOLDER};
                my $techNfo = $data->{$projectSpecie}{TECH}{$pipelineTechnology}{TECH_NFO};
                my $sppNfo  = $data->{$projectSpecie}{SPP_NFO};
                
                die if ! scalar keys %$folders;
                
                foreach my $folderName ( sort keys %$folders )
                {
                    my $files     = $folders->{$folderName}{FILE};
                    my $folderNfo = $folders->{$folderName}{FOLDER_NFO};
                    
                    die if ! scalar keys %$files;
                    
                    foreach my $fileName ( sort keys %$files )
                    {
                        my $fileAttKeys = $files->{$fileName};
                        
                        
                        # *********************
                        # ADD FILE TO PROJECT
                        # *********************
                        my $add = $ngsTech->addFile( file => $fileName, atts => $fileAttKeys );
                        
                        if ( $add ne 1 )
                        {
                            warn "ERROR ADDING FILE $fileName :: $add\n";
                            die;
                        }
                    }
                    
                    
                    #foreach my $fileName ( sort keys %$FILE )
                    #{
                    #    print "            FILE: $fileName\n" if $printLoadNgs;
                    #    my $fileNfos = $FILE->{$fileName};
                    #    
                    #    # *********************
                    #    # ADD FILE TO PROJECT
                    #    # *********************
                    #    my $add = $ngsTech->addFile( file => $fileName );
                    #    
                    #    if ( $add ne 1 )
                    #    {
                    #        warn "ERROR ADDING FILE $fileName :: $add\n";
                    #        die;
                    #    }
                    #    
                    #    foreach my $fileNfo ( sort keys %$fileNfos )
                    #    {
                    #        my $fileNfoData = $fileNfos->{$fileNfo};
                    #        print "              FILE NFO: FILE$fileNfo = $fileNfoData\n" if $printLoadNgs;
                    #    }
                    #}
                }
            } else {
                die "THERE ARE NO FILES FOR SPECIES $projectSpecie BUT NOT FOR ".
                    "TECHNOLOGY $pipelineTechnology";
            }
        } else {
            die "THERE ARE NO FILES FOR SPECIES $projectSpecie";
        }
        
    }
    #dumper(\%projects);
    #exit;


    return \%projects;
    
    #<projects>
    #    <project specie="AllRound" title="AllRound standard">
    #        <pipelines>
    #            <pipeline technology="454"      pipeline="454"/>
    #            <pipeline technology="illumina" pipeline="Illumina"/>
    #        </pipelines>
    #        <assemblies>
    #            <assembly id="mixed1"/>
    #        </assemblies>
    #    </project>
    #    <project specie="F5" title="F5 standard">
    #        <pipelines>
    #            <pipeline technology="454"      pipeline="454"/>
    #            <pipeline technology="illumina" pipeline="Illumina"/>
    #        </pipelines>
    #        <assemblies>
    #            <assembly id="mixed2"/>
    #        </assemblies>
    #    </project>
    #    <project specie="Heinz" title="Heinz standard">
    #        <pipelines>
    #            <pipeline technology="454"      pipeline="454_2"/>
    #            <pipeline technology="illumina" pipeline="Illumina"/>
    #        </pipelines>
    #        <assemblies>
    #            <assembly id="mixedAndFiltered"/>
    #        </assemblies>
    #    </project>
    #    <project specie="Pennellii" title="Pennellii standard">
    #        <pipelines>
    #            <pipeline technology="454"      pipeline="454"/>
    #            <pipeline technology="illumina" pipeline="Illumina"/>
    #        </pipelines>
    #        <assemblies>
    #            <assembly id="mixed1"/>
    #        </assemblies>
    #    </project>
    #    <project specie="Pennellii" title="Pennellii again">
    #        <pipelines>
    #            <pipeline technology="454"      pipeline="454"/>
    #            <pipeline technology="illumina" pipeline="Illumina"/>
    #        </pipelines>
    #        <assemblies>
    #            <assembly id="mixed2"/>
    #        </assemblies>
    #    </project>
    #</projects>
}



1;



#sub loadNgs
#{
#    my $ngs          = shift;
#    my $data         = shift;
#    my $pipe         = shift;
#    my $printLoadNgs = 0;
#    
#    print " LOAD NGS\n";
#    
#    my $SPP     = $data;
#    
#    foreach my $spp ( sort keys %$SPP )
#    {
#        print "SPP: $spp\n" if $printLoadNgs;
#        my $TECH    = $SPP->{$spp}{TECH};
#        my $sppNfos = $SPP->{$spp}{SPP_NFO};
#        
#
#        # *********************
#        # CREATES NGS PROJECT FOR SPECICES
#        # *********************
#        my $ngsProj;
#        if ( ! exists $ngs->{$spp} )
#        {
#            $ngsProj   = ngs->new( spp => $spp );
#            $ngs->{$spp} = $ngsProj;
#        } else {
#            $ngsProj = $ngs->{$spp};
#        }
#        
#        
#        foreach my $sppNfo ( sort keys %$sppNfos )
#        {
#            my $sppNfoData = $sppNfos->{$sppNfo};
#            print "  SPP NFO: SPP$sppNfo = $sppNfoData\n" if $printLoadNgs;
#        }
#        
#        foreach my $tech ( sort keys %$TECH )
#        {
#            my $FOLDER   = $TECH->{$tech}{FOLDER};
#            my $techNfos = $TECH->{$tech}{TECH_NFO};
#            print "    TECH: $tech\n" if $printLoadNgs;
#            
#            # *********************
#            # CHECK IF THERE IS A PIPELINE FOR THE TECHNOLOGY
#            # push(@{ $pipelines{$technologyName}{$pipelineName} }, [ $stepsAtts->{_id} , $stepsAtts->{_parameters} ]);
#            # *********************
#            die "THERE'S NO PIPELINE FOR TECHNOLOGY $tech" if ! exists ${$pipe}{$tech};
#            my $pipe        = $pipe->{$tech};
#            
#            # *********************
#            # GET TECHNOLOGY MODULE
#            # *********************
#            my $ngsTechs = $ngsProj->getAvailableTechnologies();
#            die "TECHNOLOGY $tech NOT IMPLEMENTE IN NGS" if ( ! exists ${$ngsTechs}{$tech} );
#            my $ngsTech = $ngsProj->getTechnology($tech);
#            
#            # *********************
#            # ADD PIPELINE TO TECHNOLOGY
#            # *********************
#            $ngsTech->addPipeline($tech, $pipe);
#            
#            foreach my $techNfo ( sort keys %$techNfos )
#            {
#                my $techNfoData = $techNfos->{$techNfo};
#                print "      TECH NFO: TECH$techNfo = $techNfoData\n" if $printLoadNgs;
#                $ngsTech->addNfo($techNfo => $techNfoData);
#            }
#            
#            foreach my $folder ( sort keys %$FOLDER )
#            {
#                my $FILE       = $FOLDER->{$folder}{FILE};
#                my $folderNfos = $FOLDER->{$folder}{FOLDER_NFO};
#                print "        FOLDER: $folder\n" if $printLoadNgs;
#                
#                foreach my $folderNfo ( sort keys %$folderNfos )
#                {
#                    my $folderNfoData = $folderNfos->{$folderNfo};
#                    print "          FOLDER NFO: FOLDER$folderNfo = $folderNfoData\n" if $printLoadNgs;
#                }
#                
#                foreach my $fileName ( sort keys %$FILE )
#                {
#                    print "            FILE: $fileName\n" if $printLoadNgs;
#                    my $fileNfos = $FILE->{$fileName};
#                    
#                    # *********************
#                    # ADD FILE TO PROJECT
#                    # *********************
#                    my $add = $ngsTech->addFile( file => $fileName );
#                    
#                    if ( $add ne 1 )
#                    {
#                        warn "ERROR ADDING FILE $fileName :: $add\n";
#                        die;
#                    }
#                    
#                    foreach my $fileNfo ( sort keys %$fileNfos )
#                    {
#                        my $fileNfoData = $fileNfos->{$fileNfo};
#                        print "              FILE NFO: FILE$fileNfo = $fileNfoData\n" if $printLoadNgs;
#                    }
#                }
#            }
#        }
#    }
#}







#sub loadDataFolders
#{
#    my $cnf        = shift;
#    my $doc        = $cnf->getDoc();
#    my $rootName   = $cnf->getRootName();
#    my $printLoadD = 0;
#
#    my %dataFolders;
#    #$dataFolders{SPP} = {};
#    
#    foreach my $sppName ( @{ $cnf->getChildrenIds("dataFolders") } )
#    {
#        next if ! defined $sppName;
#        
#        print "  "x1 ."LOAD DATA :: SPECIE $sppName\n" if $printLoadD;
#
#        my $species      = sprintf('dataFolders/specie[@id="%s"]', $sppName);
#        my $speciesNodes = $cnf->getNodes($species);
#        $dataFolders{$sppName} = {} if ! exists $dataFolders{$sppName};
#        my $dataSpp      = $dataFolders{$sppName};
#        
#        if ( $speciesNodes )
#        {
#            my $speciesAtts     = &parseAtts($speciesNodes->[0]->getAttributes());
#            foreach my $key ( sort keys %$speciesAtts )
#            {
#                my $value = $speciesAtts->{$key};
#                $key =~ s/^_//;
#                print "  "x2 , "SPECIE", uc($key), " = ", $value, "\n" if $printLoadD;
#                $dataSpp->{SPP_NFO}{$key} = $value;
#            }
#        } else {
#            die "NO ATTS";
#        }
#        
#        $dataSpp->{TECH} = {} if ! exists $dataFolders{$sppName}{TECH};
#        my $dataTech = $dataSpp->{TECH};
#        
#        foreach my $technologyName ( @{ $cnf->getChildrenIds($species) } )
#        {
#            next if ! defined $technologyName;
#            
#            print "  "x2 ."LOAD DATA :: TECHNOLOGY $technologyName\n" if $printLoadD;
#            
#            my $technologies     = sprintf('dataFolders/specie[@id="%s"]/technology[@id="%s"]', $sppName, $technologyName);
#            my $technologyNodes  = $cnf->getNodes($technologies);
#            
#            if ( $technologyNodes )
#            {
#                my $technologyAtts     = &parseAtts($technologyNodes->[0]->getAttributes());
#                foreach my $key ( sort keys %$technologyAtts )
#                {
#                    my $value = $technologyAtts->{$key};
#                    $key =~ s/^_//;
#                    print "  "x3 , "TECHNOLOGY", uc($key), " = ", $value, "\n" if $printLoadD;
#                    $dataTech->{$technologyName}{TECH_NFO}{$key} = $value;
#                }
#            } else {
#                die "NO ATTS";
#            }
#            
#            $dataTech->{$technologyName}{FOLDER} = {} if ! exists ${$dataTech->{$technologyName}}{FOLDER};
#            my $dataFolder = $dataTech->{$technologyName}{FOLDER};
#            
#            foreach my $folderName ( @{ $cnf->getChildrenIds($technologies) } )
#            {
#                next if ! defined $folderName;
#            
#                print "  "x3 ."LOAD DATA :: FOLDER $folderName\n" if $printLoadD;
#            
#                my $folders      = sprintf('dataFolders/specie[@id="%s"]/technology[@id="%s"]/folder[@id="%s"]', $sppName, $technologyName, $folderName);
#                my $folderNodes  = $cnf->getNodes($folders);
#                
#                if ( $folderNodes )
#                {
#                    my $folderAtts     = &parseAtts($folderNodes->[0]->getAttributes());
#                    foreach my $key ( sort keys %$folderAtts )
#                    {
#                        my $value = $folderAtts->{$key};
#                        $key =~ s/^_//;
#                        print "  "x4 , "FOLDER", uc($key), " = ", $value, "\n" if $printLoadD;
#                        $dataFolder->{$folderName}{FOLDER_NFO}{$key} = $value;
#                    }
#                } else {
#                    die "NO ATTS";
#                }
#                
#                
#                $dataFolder->{$folderName}{FILE} = {} if ! exists ${$dataFolder->{$folderName}}{FOLDER};
#                my $dataFile = $dataFolder->{$folderName}{FILE};
#                
#                foreach my $fileName ( @{ $cnf->getChildrenIds($folders) } )
#                {
#                    next if ! defined $fileName;
#                
#                    print "  "x4 ."LOAD DATA :: FILE $fileName\n" if $printLoadD;
#                
#                    my $files      = sprintf('dataFolders/specie[@id="%s"]/technology[@id="%s"]/folder[@id="%s"]/file[@id="%s"]', $sppName, $technologyName, $folderName, $fileName);
#                    my $fileNodes  = $cnf->getNodes($files);
#                    
#                    if ( $fileNodes )
#                    {
#                        my $fileAtts     = &parseAtts($fileNodes->[0]->getAttributes());
#                        foreach my $key ( sort keys %$fileAtts )
#                        {
#                            my $value = $fileAtts->{$key};
#                            $key =~ s/^_//;
#                            print "  "x5 , "FILE", uc($key), " = ", $value, "\n" if $printLoadD;
#                            $dataFile->{$fileName}{$key} = $value;
#                        }
#                    } else {
#                        die "NO ATTS";
#                    }
#                }
#            }
#        }
#    }
#
#    #dumper(\%dataFolders);
#
#    return \%dataFolders;
#
#
#    #<dataFolders>
#    #  <specie id="AllRound" files="25" insertSizes="5" libTypes="1" libraries="4" size="166747907487" species="7" sumReads="525398492" technologies="2">
#    #    <technology id="454" files="17" insertSizes="3" libTypes="1" size="26490756927" species="3" sumReads="8453276" technologies="1">
#    #      <folder id="AllRound_454_matepair_20000" size="6750882266" reads="2176751" files="4">
#    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_20000/20000_3_2_XLR56_GNKKXC002.sff" count="0" size="1334626250" reads="468249"/>
#    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_20000/20000_1_1_XLR54_GMFT65F01.sff" count="1" size="1944004554" reads="568823"/>
#    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_20000/20000_2_1_XLR55_GMNA3LJ01.sff" count="2" size="1982751460" reads="612608"/>
#    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_20000/20000_4_1_XLR58_GO17ZGT01.sff" count="3" size="1489500002" reads="527071"/>
#    #      </folder>
#    #      <folder id="AllRound_454_matepair_3000" size="12191349699" reads="3952186" files="8">
#    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_3000/3000_2_4_XLR69_GX9RM5204.sff" count="0" size="749214238" reads="239320"/>
#    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_3000/3000_1_1_XLR71_GZGLCTR01.sff" count="1" size="1871983295" reads="613058"/>
#    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_3000/3000_1_3_XLR69_GX9RM5203.sff" count="2" size="560239206" reads="183304"/>
#    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_3000/3000_1,2_1_XLR72_GZJ7FCV01.sff" count="3" size="1837587086" reads="599624"/>
#    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_3000/3000_1,2_2_XLR70_GYSE4CQ02.sff" count="4" size="1886125686" reads="607336"/>
#    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_3000/3000_1_1_XLR70_GYSE4CQ01.sff" count="5" size="1561248322" reads="511121"/>
#    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_3000/3000_2_2_XLR72_GZJ7FCV02.sff" count="6" size="1870655031" reads="601952"/>
#    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_3000/3000_2_2_XLR71_GZGLCTR02.sff" count="7" size="1854296835" reads="596471"/>
#    #      </folder>
#    #      <folder id="AllRound_454_matepair_8000" size="7548524962" reads="2324339" files="5">
#    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_8000/8000_4_4_XLR64_GR3I8FS04.sff" count="0" size="792116302" reads="248444"/>
#    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_8000/8000_3_2_XLR63_GQ92EL202.sff" count="1" size="1851425682" reads="569925"/>
#    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_8000/8000_4_3_XLR64_GR3I8FS03.sff" count="2" size="802537642" reads="252121"/>
#    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_8000/8000_2_1_XLR63_GQ92EL201.sff" count="3" size="2094255134" reads="637836"/>
#    #        <file id="/home/aflit001/nobackup/Data/AllRound_454_matepair_8000/8000_1_2_XLR62_GQWYU1N02.sff" count="4" size="2008190202" reads="616013"/>
#    #      </folder>
#    #    </technology>
#    #  </species>
#    #</dataFolders>
#
#}

#sub loadPipelines
#{
#    my $cnf        = shift;
#    my $doc        = $cnf->getDoc();
#    my $rootName   = $cnf->getRootName();
#    my $printLoadP = 0;
#
#    my %pipelines;
#    
#    foreach my $technologyName ( @{ $cnf->getChildrenIds('pipelines') } )
#    {
#        next if ! defined $technologyName;
#        
#        print "  "x2 . "LOAD PIPELINES :: TECHNOLOGY $technologyName\n" if $printLoadP;
#
#        my $technologies         = sprintf('pipelines/technology[@id="%s"]', $technologyName);
#        my $technologiesNodes    = $cnf->getNodes($technologies);
#        
#        if ( $technologiesNodes )
#        {
#            my $technologiesAtts     = &parseAtts($technologiesNodes->[0]->getAttributes());
#            foreach my $key ( sort keys %$technologiesAtts )
#            {
#                my $value = $technologiesAtts->{$key};
#                $key =~ s/^_//;
#                print "  "x3 . "LOAD PIPELINES :: TECHNOLOGY : ", uc($key), " = ", $value, "\n" if $printLoadP;
#                #$pipelines{$technologyName}{INFO}{$key} = $value;
#            }
#        } else {
#            die " NO ATTS";
#        }
#
#        foreach my $pipelineName ( @{ $cnf->getChildrenIds($technologies) } )
#        {
#            next if ! defined $pipelineName;
#            
#            print "  "x4 . "LOAD PIPELINES :: PIPELINE $pipelineName\n" if $printLoadP;
#            my $pipelines      = sprintf('pipelines/technology[@id="%s"]/pipeline[@id="%s"]', $technologyName, $pipelineName);
#            my $pipelinesNodes = $cnf->getNodes($pipelines);
#
#            if ( $pipelinesNodes )
#            {
#                my $pipelinesAtts     = &parseAtts($pipelinesNodes->[0]->getAttributes());
#                foreach my $key ( sort keys %$pipelinesAtts )
#                {
#                    my $value = $pipelinesAtts->{$key};
#                    $key =~ s/^_//;
#                    print "  "x5 . "LOAD PIPELINES :: PIPELINE : ", uc($key), " = ", $value, "\n" if $printLoadP;
#                    #$pipelines{$technologyName}{$pipelineName}{INFO}{$key} = $value;
#                }
#            } else {
#                die " NO ATTS";
#            }
#
#            foreach my $stepNode ( @{ $cnf->getChildren($pipelines) } )
#            {
#                next if ! defined $stepNode;
#                
#                $stepNode    = $stepNode->{link};
#                my $stepName = $stepNode->getNodeName();
#
#                print "  "x6 . "LOAD PIPELINES :: STEPS $stepName\n" if $printLoadP;
#        
#                my $stepsAtts     = &parseAtts($stepNode->getAttributes());
#                foreach my $key ( sort keys %$stepsAtts )
#                {
#                    my $value = $stepsAtts->{$key};
#                    $key =~ s/^_//;
#                    print "  "x7 . " LOAD PIPELINES :: STEPS : ", uc($key), " = ", $value, "\n" if $printLoadP;
#                }
#                push(@{ $pipelines{$technologyName}{$pipelineName} }, [ $stepsAtts->{_id} , $stepsAtts->{_parameters} ]);
#                    
#            }
#        }
#    }
#    
#    #dumper(\%pipelines);
#    return \%pipelines;
#}


#sub getPipelines
#{
#    my $cnf = shift;
#    my %hash;
#    
#    foreach my $technology ( @{$cnf->getChildrenIds('pipelines')} )
#    {
#        my $parent = 'pipelines/pipeline[@id="'.$technology.'"]';
#        my $prog   = $cnf->getValueAsText("$parent/prog");
#        my $param  = $cnf->getValueAsText("$parent/parameters");
#    
#        $hash{$technology}{program}    = $prog;
#        $hash{$technology}{parameters} = $param;
#        
#        print "  TECHNOLOGY '$technology'\n";
#        print "    PROG: '$prog'\n";
#        print "    PARA: '$param'\n";
#    }
#
#    return \%hash;
#}

#sub splitter
#{
#    my $self       = shift;
#    my $printSplit = 0;
#
#    my $dataFolders = $self->loadDataFolders();
#
#    foreach my $dataFolder ( sort keys %$dataFolders )
#    {
#        my $fields = $dataFolders->{$dataFolder};
#        my $parent = $fields->{_parent};
#        
#        #split fields
#        if (       exists $fields->{nameSplitter}        && $fields->{nameSplitter}       ne ''
#                && exists $fields->{nameSplitterFields}  && $fields->{nameSplitterFields} ne ''
#                && exists $fields->{folder}              && $fields->{folder}             ne ''
#           )
#        {
#            print "    SPLITTER :: DATAFOLDER '$dataFolder' PARENT '$parent' SPLITTER '$fields->{nameSplitter}' FORMAT '$fields->{nameSplitterFields}' FOLDER '$fields->{folder}'\n" if $printSplit;
#            my @fields = split(/\+/, $fields->{nameSplitterFields});
#            my $ns     = $fields->{nameSplitter};
#            my @datas  = ($fields->{folder} =~ /$ns/);
#            
#            if (scalar @fields != scalar @datas )
#            {
#                print
#                "SPLITTER :: MISMATCHED NUMBER OF FIELDS. SPLITTER '$ns' GENERATES ",
#                (scalar @fields) ,
#                " FIELDS WHILE FORMAT '$fields->{nameSplitterFields}' GENERATES ",
#                (scalar @datas) ," FIELDS";
#                die;
#            };
#                
#            for ( my $f = 0; $f < scalar @fields; $f++ )
#            {
#                my $field = $fields[$f];
#                my $data  = $datas[$f];
#                print "      SPLITTER :: FIELD = '$field' DATA = '$data'\n" if $printSplit;
#                
#                #<!-- fields are: SPP TECH SIZE LIB -->
#                switch ( $field )
#                {
#                    case 'SPP'  { createElement($self, "$parent" ,"spp"       ); setValue($self, "$parent/spp",        $data) }
#                    case 'TECH' { createElement($self, "$parent" ,"technology"); setValue($self, "$parent/technology", $data) }
#                    case 'SIZE' { createElement($self, "$parent" ,"size"      ); setValue($self, "$parent/size",       $data) }
#                    case 'LIB'  { createElement($self, "$parent" ,"library"   ); setValue($self, "$parent/library",    $data) }
#                }
#            }
#            print "\n\n" if $printSplit;
#            
#        } #if has splitter
#    }
#}
#



#sub makeNfo
#{
#    my $self        = shift;
#    my $printNfo    = 0;
#    my $dataFolders = $self->loadDataFolders();
#    
#    foreach my $dataFolder ( sort keys %$dataFolders )
#    {
#        my $fields = $dataFolders->{$dataFolder};
#        
#        if ( ! exists $fields->{technology} ) { die "NO TECHNOLOGY DEFINED"; };
#        if ( ! exists $fields->{spp}        ) { die "NO SPECIES DEFINED"; };
#        if ( ! exists $fields->{size}       ) { die "NO SIZE DEFINED"; };
#        if (   exists $fields->{_parent}    ) { delete ${$fields}{_parent}          } ;
#        #if ( ! exists $localHash->{lib}        ) { print "NO LIBRARY DEFINED"   ; die; };
#        
#        
#        if ( $printNfo )
#        {
#            print "MAKE NFO :: DATAFOLDER '$dataFolder'\n";
#            foreach my $key ( sort keys %$fields )
#            {
#                print "    ", uc($key), " = ", ( defined $fields->{$key} ? $fields->{$key} : '' ), "\n";
#            }
#        }
#    }
#
#    return $dataFolders;
#}


#sub listFiles
#{
#    #my $prjs = &listFiles($outputFolder, ".xml");
#    my $inFolder = $_[0];
#    my $ext = $_[1];
#
#    my $error = 0;
#    opendir(DH, $inFolder) or $error = 1;
#
#    if ($error)
#    {
#        &printLog(0, "\tCOULD NOT OPEN DIR $inFolder\n");
#        die;
#    }
#
#    my $startList = time;
#    my @fls;
#    while (my $igot = readdir(DH)) {
#        next if ($igot =~ /^\.{1,2}$/);
#        next if ( ! ( $igot =~ /$ext$/ ));
#        #print "igot $igot\n";
#        push(@fls, $inFolder."/".$igot);
#    }
#    closedir DH;
#
#    @fls = sort @fls;
#
#    return \@fls;
#}

1;


#my $fullpath    = dirname(realpath($0));
